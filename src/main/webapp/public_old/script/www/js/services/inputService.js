angular.module('starter.inputService', [])
  .factory('inputService', inputService);

function inputService(TYPE_POST, $q, $http, MAIN_CONSTANT,$localStorage) {
$http.defaults.headers.common['Bearer'] = $localStorage.token;
//alert( $localStorage.token);

  return {
    getcategories:getcategories,
    getallproductsbyname:getallproductsbyname,
    getactivecategories:getactivecategories,
    submitAddCategoryDetails: submitAddCategoryDetails,
    getsubcategories:getsubcategories,
    submitAddSubCategoryDetails:submitAddSubCategoryDetails,
    submitAddProductDetails:submitAddProductDetails,
    getsubcategoriesByCategoryId:getsubcategoriesByCategoryId,
    getcustomerreviews:getcustomerreviews,
    getcustomerstatistics:getcustomerstatistics,
    getordersByUserId:getordersByUserId,
    getorderdetailsbyorderId:getorderdetailsbyorderId,
    submitAddCustomerDetails:submitAddCustomerDetails,
    getallcustomersbysearch:getallcustomersbysearch,
    submitAddTaxDetails:submitAddTaxDetails,
    getrelations:getrelations,
    
    submitEditCategoryDetails:submitEditCategoryDetails,
    getallcustomers:getallcustomers,
    gethomedatas:gethomedatas,
    submitAddOfferDetails:submitAddOfferDetails,
   
    getproducts:getproducts
    // categorymanage:categorymanage

  };

   function submitAddOfferDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/addoffer",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

//get home datas
function gethomedatas() {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getalldetails" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }



  //add customer
  function submitAddCustomerDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/savecustomer",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


  // get order by customer

function getordersByUserId(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorders",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  // get orderdetails by order Id

function getorderdetailsbyorderId(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorders",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
 

 function getallcustomers(customers) {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/user/getallcustomers" ,
      data:customers
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
//get customer statistics
function getcustomerstatistics(customers) {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/user/getcustomers" ,
      data:customers
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
  function getactivecategories() {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getcategory?categoryId=0&mainCategoryId=0&status=1" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

 function getcategories() {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getcategory?categoryId=0&mainCategoryId=0&status=2" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }


  // search customer


  function getallcustomersbysearch(customerData) {
    var deffered = $q.defer();
    //adppostData
    console.log(customerData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url +"/api/user/getallcustomers",
      data: customerData
    }).then(function mySucces(response) {
      
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  //search product by name
  function getallproductsbyname(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url +"/api/input/getproducts",
      data: Data
    }).then(function mySucces(response) {
      
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }



// related products

function getrelations(relation) {

  var relations = [{"modeId": "8", "name": "color"},
     {"modeId": "9", "name": "material"},
      {"modeId": "10", "name": "type"},
      {"modeId": "11", "name": "capacity"},
      {"modeId": "12", "name": "size"},
      {"modeId": "13", "name": "length"},
      {"modeId": "14", "name": "width"},
      {"modeId": "15", "name": "height"},
       {"modeId": "16", "name": "weight"},
       ];
console.log( relations);

 // var relations= [
 //      {"modeId": "8", "name": "color"},
 //      {"modeId": "9", "name": "material"},
 //      {"modeId": "10", "name": "type"},
 //      {"modeId": "11", "name": "capacity"},
 //      {"modeId": "12", "name": "size"},
 //      {"modeId": "13", "name": "length"},
 //      {"modeId": "14", "name": "width"},
 //      {"modeId": "15", "name": "height"},
 //       {"modeId": "16", "name": "weight"},
     
 //    ];
 //    console.log( relations);

  }

  // getproducts

function getproducts(products) {
    var deffered = $q.defer();
    console.log(products);
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getproducts" ,
      data:products
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);


          // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }


  // getcustomer reviews

function getcustomerreviews(customerreviews) {
    var deffered = $q.defer();
    console.log(customerreviews);
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getreviews" ,
      data:customerreviews
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);


          // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

  
//add tax

 function submitAddTaxDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/addtax",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


  



// add category


 function submitAddCategoryDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/savecategory",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


// get subcategories



 function getsubcategories() {
  console.log();
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getsubcategory?mainCategoryId=0&categoryId=0&status=2" ,
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);

         // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

  //get subcategory by category

  function getsubcategoriesByCategoryId(catId) {
  console.log();
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getsubcategory?mainCategoryId=0&status=2&categoryId="+ catId ,
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);

         // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

 


  //add subcategory

  // add category


 function submitAddSubCategoryDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
     // alert("hai");
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/savesubcategory",
      data: Data
     
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

  // add product


 function submitAddProductDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/saveproduct",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }




  // edit category


function submitEditCategoryDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/savecategory",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

// get all subcategories



}




