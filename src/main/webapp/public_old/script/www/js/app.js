  // angular.module is a global place for creating, registering and retrieving Angular modules
  // 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
  // the 2nd parameter is an array of 'requires'
  // 'starter.controllers' is found in controllers.js

  angular.module('starter', [

      'ui.router',
      'common.btnLoading',
      'common.loading',
      
      'starter.homeCtrl',
      
      'starter.authCtrl',
      'starter.categoryCtrl',
       'starter.subcategoryCtrl',
       'starter.productCtrl',
       'starter.customerCtrl',
       'starter.auctionCtrl',
       'starter.locationCtrl',
       'starter.sellerCtrl',
        'starter.taxCtrl',
         'starter.orderCtrl',

       'starter.authService',
       'starter.inputService',
       'starter.auctionService',
       'starter.storeCtrl',
       'starter.loadingService',


       'starter.storeService',
       'starter.sellerService',
       'ngStorage',
     
      'ui.bootstrap',
      'ngFileUpload',
      'starter.inputCtrl',
      /*  'angularFileUpload',*/
      'ngTable',
      'constant',
      'toaster',
      



    ])

    .run(function($templateCache, $rootScope, $state, loadingService) {
    })

    .config(function($stateProvider, $urlRouterProvider) {
      $stateProvider


        .state('login', {
          url: '/login',
          templateUrl: 'public/script/www/templates/auth/login.html',
          controller: 'authCtrl'
        })


        .state('app', {
          url: '/app',
          
          templateUrl: 'public/script/www/templates/auth/login.html',
          controller: 'authCtrl'
        })
        .state('admin', {
          url: '/admin',
          abstract: true,
                    
          templateUrl: 'public/script/www/templates/common/sidebar.html',
          controller: 'homeCtrl'
        })

        .state('admin.dashboard', {
          url: '/dashboard',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/dashboard//dashboard.html',
              controller: 'homeCtrl'
            }
          }

        })

        //  .state('admin.advertisement', {
        //   url: '/advertisement',
        //   views: {
        //     'menuContent': {
        //       templateUrl: 'public/script/www/templates/advertisement.html',
        //       controller: 'advertisementCtrl'
        //     }
        //   }

        // })

          .state('admin.category', {
          url: '/category',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/categorymanagement.html',
              controller: 'categoryCtrl'
            }
          }

        })
          .state('admin.addcategory', {
          url: '/addcategory/:categoryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/addcategory.html',
              controller: 'categoryCtrl'
            }
          }

        })
           .state('admin.viewcategory', {
          url: '/viewcategory/:categoryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/viewcategory.html',
              controller: 'categoryCtrl'
            }
          }

        })

           .state('admin.deactivatedcategories', {
          url: '/deactivatedcategories',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/category/deactivatedcategories.html',
              controller: 'categoryCtrl'
            }
          }

        })

           .state('admin.subcategory', {
          url: '/subcategory',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/itemtype.html',
              controller: 'subcategoryCtrl'
            }
          }


        })
        
        .state('admin.addsubcategory', {
          url: '/addsubcategory/:subCategoryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/additemtype.html',
              controller: 'subcategoryCtrl'
            }
          }
          

        })


            .state('admin.viewsubcategory', {
          url: '/viewsubcategory/:subCategoryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/viewsubcategory.html',
              controller: 'subcategoryCtrl'
            }
          }

        })

           .state('admin.deactivatedsubcategories', {
          url: '/deactivatedsubcategories',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/subcategory/deactivatedsubcategories.html',
              controller: 'subcategoryCtrl'
            }
          }

        })


           .state('admin.country', {
          url: '/country',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/country/countrymanagement.html',
              controller: 'locationCtrl'
            }
          }

        })
          .state('admin.addcountry', {
          url: '/addcountry/:countryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/country/addcountry.html',
              controller: 'locationCtrl'
            }
          }

        })

          .state('admin.viewcountry', {
          url: '/viewcountry/:countryId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/country/viewcountry.html',
              controller: 'locationCtrl'
            }
          }

        })




          .state('admin.city', {
          url: '/city',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/city/citymanagement.html',
              controller: 'locationCtrl'
            }
          }

        })
          .state('admin.addcity', {
          url: '/addcity/:cityId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/city/addcity.html',
              controller: 'locationCtrl'
            }
          }

        })

           .state('admin.viewcity', {
          url: '/viewcity/:cityId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/city/viewcity.html',
              controller: 'locationCtrl'
            }
          }

        })
//product
    .state('admin.product', {
          url: '/product',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/itemmanagement.html',
              controller: 'productCtrl'
            }
          }

        })

        .state('admin.addproduct', {
          url: '/addproduct/:productId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/additem.html',
              controller: 'productCtrl'
            }
          }

        })

        .state('admin.viewproduct', {
          url: '/viewproduct/:productId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/product/viewitem.html',
              controller: 'productCtrl'
            }
          }

        })

        //SELLER
         .state('admin.seller', {
          url: '/seller',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/sellermanagement.html',
              controller: 'sellerCtrl'
            }
          }

        })
       


         .state('admin.addseller', {
          url: '/addseller/:sellerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/addseller.html',
              controller: 'sellerCtrl'
            }
          }

        })
          .state('admin.viewseller', {
          url: '/viewseller/:sellerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/viewseller.html',
              controller: 'sellerCtrl'
            }
          }

        })
           .state('admin.editseller', {
          url: '/editseller',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/editseller.html',
              controller: 'sellerCtrl'
            }
          }

        })

          .state('admin.sellerstatistics', {
          url: '/sellerstatistics',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/seller/sellerstatistics.html',
              controller: 'sellerCtrl'
            }
          }

        })
      


// CUSTOMER
 .state('admin.customer', {
          url: '/customer',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/customeraccounts.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.viewcustomer', {
          url: '/viewcustomer/:customerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/viewcustomer.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.addcustomer', {
          url: '/addcustomer/:customerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/addcustomer.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.editcustomer', {
          url: '/editcustomer/:customerId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/editcustomer.html',
              controller: 'customerCtrl'
            }
          }

        })

          .state('admin.customerstatistics', {
          url: '/customerstatistics',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/customerstatistics.html',
              controller: 'customerCtrl'
            }
          }

        })

          .state('admin.customerreview', {
          url: '/customerreview',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customer/customerreview.html',
              controller: 'customerCtrl'
            }
          }

        })
 

           .state('admin.tax', {
          url: '/tax',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/tax/taxmanagement.html',
              controller: 'taxCtrl'
            }
          }

        })
           .state('admin.addtax', {
          url: '/addtax/:taxId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/tax/addtax.html',
              controller: 'taxCtrl'
            }
          }

        })
           .state('admin.viewtax', {
          url: '/viewtax/:taxId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/tax/viewtax.html',
              controller: 'taxCtrl'
            }
          }

        })
//ORDER
.state('admin.order', {
          url: '/order',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order.html',
              controller: 'orderCtrl'
            }
          }

        })
.state('admin.neworder', {
          url: '/neworder',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/order/neworders.html',
              controller: 'orderCtrl'
            }
          }

        })
.state('admin.milestoneorder', {
          url: '/milestoneorder',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/milestoneoforder.html',
              controller: 'orderCtrl'
            }
          }

        })
.state('admin.deliveredorders', {
          url: '/deliveredorders',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/deliveredorders.html',
              controller: 'orderCtrl'
            }
          }

        })
.state('admin.invoice', {
          url: '/invoice/:orderId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/invoice.html',
              controller: 'orderCtrl'
            }
          }

        })
          
       
          .state('admin.itemacceptance', {
          url: '/itemacceptance/:requestItemId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/itemacceptance.html',
              controller: 'storeCtrl'
            }
          }})

            .state('admin.itemdeactivation', {
          url: '/itemdeactivation',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/itemdeactivation.html',
              controller: 'storeCtrl'
            }
          }

        })
          .state('admin.responsedstorerequests', {
          url: '/responsedstorerequests',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/responsedstoreitemrequests.html',
              controller: 'storeCtrl'
            }
          }

        })
          .state('admin.listofcustomerorder', {
          url: '/listofcustomerorder/:userId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/listofordersbycustomer.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.customerorderdetails', {
          url: '/customerorderdetails/:orderId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/customerorderdetails.html',
              controller: 'customerCtrl'
            }
          }

        })
          .state('admin.listofstoreorder', {
          url: '/listofstoreorder/:storeId',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/listofordersbystore.html',
              controller: 'storeCtrl'
            }
          }

        })


        .state('app.signup', {
          url: '/signup',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/auth/signup.html',
              controller: 'authCtrl'
            }
          }

        })

       
       .state('app.home', {
          url: '/home?mode',
          views: {
            'menuContent': {
              templateUrl: 'public/script/www/templates/common/sidebar.html',
              controller: 'homeCtrl'
            }
          }

        })

       
      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/login');
    });
