angular.module('starter.sellerCtrl', [])
  .controller('sellerCtrl', function($scope, $window, $state, inputService,toaster,sellerService, FILE_CONSTANT,toaster, MAIN_CONSTANT, Upload, sellerService, $stateParams, $localStorage,loadingService) {

if (!$localStorage.token||$localStorage.token===0) {
    $window.location.href ='#/login';
    }
    $scope.imageUrl = FILE_CONSTANT.filepath;

     



    if ($state.current.name === 'admin.seller') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

      $scope.sellerPageVo = {
        "sellerId":0,
        "sellerName":"",
        "phoneNumber":"",
        "address":"",
        "status":[0],
        "currentIndex":0,
        "rowPerPage":100
}

// alert(33333333);
      $scope.allseller = function() {
        loadingService.isLoadin=true;

        sellerService.getallsellers($scope.sellerPageVo).then(function(response) {
          loadingService.isLoadin=false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.sellers = response.object;
            console.log($scope.sellers);



          } else {
            alert(response.errorMessage);
          }
        },function(){
          alert('Oops! somthing went wrong');
        });
      }
      $scope.allseller();

    }

   


    
   

    $scope.viewseller = function(seller) {
       loadingService.isLoadin=true;
      console.log(seller);
      $window.location.href = '#/admin/viewseller/' + seller.sellerId;


      if (seller.sellerId) {
        // alert(seller.sellerId);

        $localStorage.seller = seller;
        //


      }
    };
    //view seller

    if ($state.current.name === 'admin.viewseller') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
       loadingService.isLoadin=false;
      $scope.seller = {};
      $scope.seller.imageName = "nothing";
      if ($stateParams.sellerId !== '0') {
        $scope.seller = $localStorage.seller;

      }


console.log( $scope.seller );


    }

    $scope.sellermanage = function(seller) {
       loadingService.isLoadin=true;
      console.log(seller);
      $window.location.href = '#/admin/addseller/' + seller.sellerId;


      if (seller.sellerId) {
        // alert(seller.sellerId);

        $localStorage.seller = seller;
        //


      }
    };


    // add seller

    if ($state.current.name === 'admin.addseller') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.seller = {};

      // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
      $scope.upload = function(file) {
      //$scope.isFileuploadStart=true;



      if (file) {
        

        $scope.clickimg = true;


        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/uploadtoaws",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
          if (resp.data.errorCode === 0) {
            $scope.seller.profileImage = resp.data.object;

            console.log($scope.seller.profileImage);

           
              // if ($scope.country.countryFlag) {
              //   console.log(resp);
              //   $scope.country.countryFlag = resp.data.object;
              //   $scope.country.countryFlag.isLoadin = false;

              // }
          


            // if (!$scope.country.countryFlag) {
            //   $scope.country.countryFlag = "";
            // }
          } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';

          } else {

          }





        }, function(resp) {
          $scope.progressPercentage = 0;
          // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.seller.profileImage) {
              //splice i
              $scope.seller.profileImage;

            }
         

          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };
$scope.deleteimage = function(){
          $scope.seller.profileImage="";
          
          }

    
      
       
      
      if ($stateParams.sellerId !== '0') {
        $scope.seller = $localStorage.seller;
      }

      $scope.addseller = function(getAddsellerDetails) {
        $scope.seller.password = "password";
         $scope.seller.password +=getAddsellerDetails.phoneNumber;
          $scope.seller.workingHours =2; 

         loadingService.isLoadin=true;
        // $scope.featureVo = [{
        //   "featurePhotoId": 1,
        //   "modeType": 1,
        //   "userId": 1,
        //   "extraData": "qwert",
        //   "extraDataNum": "12",
        //   "productOrShop": 1,
        //   "status": 1,
        //   "lastUpdatedTime": ""
        // }]


        console.log(getAddsellerDetails);

        sellerService.submitAddsellerdetails(getAddsellerDetails).then(function(response) {
           loadingService.isLoadin=false;
          console.log(response);

          if (response.data.errorCode === 0) {
            if ($stateParams.sellerId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added new seller!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  seller!");

                             }

            $window.location.href = '#/admin/seller';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {

            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
            toaster.pop('error', "Oops..!", response.data.errorMessage);
          }

        });



      }


    }


    $scope.changeCountry = function(countryId) {
      console.log(countryId);


      $scope.getcityByCountryId(countryId)

    };
    $scope.getcityByCountryId = function(countryId) {

      $scope.cityPageVo = {
        "countryId": countryId,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
        "cityName": "",
        "citusStatus":1,
        "status": []
      }
      $scope.citiesbycountries=[];

      sellerService.getcitiesByCountryId($scope.cityPageVo, countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
    // $scope.getcityByCountryId();







// seller search

// get country


  $scope.getactivecountry = function(country) {
  loadingService.isLoadin=false;
      sellerService.getactivecountries(country).then(function(response) {
        loadingService.isLoadin=false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.countries = response.object;
        }else { 
        loadingService.isLoadin=false;
      


        }
      },function(){
        loadingService.isLoadin=false;


      });
    };
$scope.getactivecountry();;


            
   $scope.changeCountry=function(countryId){
  console.log(countryId);

    
     $scope.getcityByCountryId(countryId)

   };
$scope.getcityByCountryId = function(countryId) {

  $scope.cityPageVo=
                {
                "countryId":countryId,
                "cityId":0,
                "rowPerPage":100,
                "currentIndex":0,
                "cityName":"",
                "citusStatus":1,
                "status":[]
              }
               $scope.citiesbycountries = [];

      sellerService.getcitiesByCountryId($scope.cityPageVo,countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
 $scope.getcityByCountryId();

 $scope.vieworderdetailsofseller = function(order) {
      // alert("01");
      loadingService.isLoadin = true;

      console.log(order);
      $window.location.href = '#/admin/customerorderdetails/' + order.orderId;


      if (order.orderId) {
        // alert(city.cityId);

        $localStorage.order = order;
        //


      }
    };



  });
