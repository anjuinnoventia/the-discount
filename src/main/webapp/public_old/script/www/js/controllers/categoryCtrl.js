angular.module('starter.categoryCtrl', [])
  .controller('categoryCtrl', function($scope,$window, $state,Upload,inputService,toaster,FILE_CONSTANT, MAIN_CONSTANT,$stateParams,$localStorage,loadingService) {
//alert($localStorage.token);
 $scope.imageUrl = FILE_CONSTANT.filepath;
if (!$localStorage.token||$localStorage.token===0) {
    $window.location.href ='#/login';
    }



 if ($state.current.name === 'admin.category') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

     $scope.getcategory = function(maincat) {
   loadingService.isLoadin=true;
      inputService.getcategories(maincat).then(function(response) {
         loadingService.isLoadin= false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.categories = response.object;
          console.log($scope.categories);
        }
      });
    };
 $scope.getcategory();

 $scope.categorymanage=function(category){
   loadingService.isLoadin=true;
  console.log(category);
  $window.location.href='#/admin/addcategory/'+category.categoryId;


  if (category.categoryId) {
    // alert(category.categoryId);

  $localStorage.category=category;
   loadingService.isLoadin=false;
  //


}
};

$scope.viewcategory=function(category){
   loadingService.isLoadin=true;
  console.log(category);
  $window.location.href='#/admin/viewcategory/'+category.categoryId;


  if (category.categoryId) {
    // alert(category.categoryId);

  $localStorage.category=category;
   loadingService.isLoadin=false;
  //


}
};

    }




// add category


    if ($state.current.name === 'admin.addcategory') {
      $scope.category = {};
       $scope.category.color="#0000FF";
       // $scope. category.imageName ="nothing";
         if ($stateParams.categoryId!=='0') {
         $scope.category=$localStorage.category;
     }
     $scope.deleteimage = function(){
          $scope.category.imageName="";
          
          }


      // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
      $scope.upload = function(file) {
      //$scope.isFileuploadStart=true;



      if (file) {
        

        $scope.clickimg = true;


        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/uploadtoaws",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
          if (resp.data.errorCode === 0) {
            $scope.category.imageName = resp.data.object;

            console.log($scope.category.imageName);

           
              // if ($scope.country.countryFlag) {
              //   console.log(resp);
              //   $scope.country.countryFlag = resp.data.object;
              //   $scope.country.countryFlag.isLoadin = false;

              // }
          


            // if (!$scope.country.countryFlag) {
            //   $scope.country.countryFlag = "";
            // }
          } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';

          } else {

          }





        }, function(resp) {
          $scope.progressPercentage = 0;
          // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.category.imageName) {
              //splice i
              $scope.category.imageName;

            }
         

          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };

      
      //loadingService.isLoadin = true;

      $scope.addCategory=function(getAddCategoryDetails){
        

         loadingService.isLoadin=true;

      console.log(getAddCategoryDetails);

                

                inputService.submitAddCategoryDetails(getAddCategoryDetails).then(function(response) {
                   loadingService.isLoadin=false;
                    console.log(response);
                   
                        if (response.data.errorCode === 0) {
                          if ($stateParams.categoryId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added new Category!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  The Category!");

                             }
                           
                           // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
                           // console.log($scope.allProducts);
                            $window.location.href = '#/admin/category';
                        } else  if (response.data.errorCode === 45||response.data.errorCode === 34) {
                        //  console.log( );
                 $window.location.href = '#/login';

                        }else{
                           toaster.pop('error', "Oops..!", response.data.errorMessage);
                          // alert(response.data.errorMessage);
                        }

                });
         

      /*/fd*/
    }


     }


// $scope.getcategory = function(maincat) {
//    loadingService.isLoadin=true;
//       inputService.getcategories(maincat).then(function(response) {
//          loadingService.isLoadin= false;
//       	console.log(response);
//         if (response.errorCode === 0) {
//           $scope.categories = response.object;
//           console.log($scope.categories);
//         }
//       });
//     };
//  $scope.getcategory();
// $scope.viewcategory=function(category){
//    loadingService.isLoadin=true;
//   console.log(category);
//   $window.location.href='#/admin/viewcategory/'+category.categoryId;


//   if (category.categoryId) {
//     // alert(category.categoryId);

//   $localStorage.category=category;
//    loadingService.isLoadin=false;
//   //


// }
// };

// $scope.categorymanage=function(category){
//    loadingService.isLoadin=true;
// 	console.log(category);
// 	$window.location.href='#/admin/addcategory/'+category.categoryId;


// 	if (category.categoryId) {
// 		// alert(category.categoryId);

// 	$localStorage.category=category;
//    loadingService.isLoadin=false;
// 	//


// }
// };


//view category

if ($state.current.name === 'admin.viewcategory') {
  if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.category = {};
       $scope. category.imageName ="nothing";
         if ($stateParams.categoryId!=='0') {
         $scope.category=$localStorage.category;
     }
        

      
    

     }




   //  // add category


   //  if ($state.current.name === 'admin.addcategory') {
   //  	$scope.category = {};
   //  	 $scope. category.imageName ="nothing";
   //       if ($stateParams.categoryId!=='0') {
   //       $scope.category=$localStorage.category;
   //   }
        

    	
   //    //loadingService.isLoadin = true;

   //    $scope.addCategory=function(getAddCategoryDetails){
   //       loadingService.isLoadin=true;

  	// 	console.log(getAddCategoryDetails);

                

   //              inputService.submitAddCategoryDetails(getAddCategoryDetails).then(function(response) {
   //                 loadingService.isLoadin=false;
   //                  console.log(response);
                   
   //                      if (response.data.errorCode === 0) {
   //                        if ($stateParams.categoryId =='0') {
   //                        // toastrMsg(1,'Added new Category!','Successfull..!');
   //                         // $scope.pop = function(){
   //                            toaster.pop('success', "Successfull..!", "Added new Category!");
   //                            // toaster.pop('success', "Added new Category!", "Successfull..!");
   //                             // };
   //                           } else{
   //                             toaster.pop('success', "Successfull..!", "Updated  The Category!");

   //                           }
                           
   //                         // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
   //                         // console.log($scope.allProducts);
   //                          $window.location.href = '#/admin/category';
   //                      } else  if (response.data.errorCode === 45||response.data.errorCode === 34) {
   //                      //	console.log( );
			// 					 $window.location.href = '#/login';

   //                      }else{
   //                         toaster.pop('error', "Oops..!", response.data.errorMessage);
   //                      	// alert(response.data.errorMessage);
   //                      }

   //              });
         

  	// 	/*/fd*/
  	// }


   //   }




 



  });


