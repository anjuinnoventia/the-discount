angular.module('starter.employeeService', [])

    .factory('employeeService', function($http, URL_CONSTANT, $window) {

        return {
            saveOrUpdateDepartment:saveOrUpdateDepartment,
            getAllDepartmentlist:getAllDepartmentlist,
            saveOrUpdateDesignation:saveOrUpdateDesignation,
            addEmployeeRegistration:addEmployeeRegistration,
            getAllDesignation:getAllDesignation,
            getAllEmployee:getAllEmployee,
            getAllTeacherAllocation:getAllTeacherAllocation,
            getAllEmployeeByDesignation:getAllEmployeeByDesignation,
            getEmployeeDetailsByEmpId:getEmployeeDetailsByEmpId,
            addEmployeeAttendence:addEmployeeAttendence,
            getEmployeeDetailsofByEmpId:getEmployeeDetailsofByEmpId


        };




        

         function getEmployeeDetailsofByEmpId(empid) {

            return $http({
                method: 'GET',
                url: URL_CONSTANT.main_url + 'getemployeeprofile?empId='+empid+'&schoolId=0&status=2', 
                headers: { 'Content-Type': 'application/json' ,'Bearer': $window.localStorage.token},               


            });

        }

           function addEmployeeRegistration(data) {
            data = JSON.stringify(data);
            console.log(data);

            return $http({
                method: 'POST',
                url: URL_CONSTANT.main_url + 'addemployee',
                headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },
                data: data


            });

        }


    function getAllEmployee(index,page,id) {

            return $http({
                method: 'GET',
                url: URL_CONSTANT.main_url + 'getallemployee?currentIndex='+index+'&rowPerPage='+page+'&schoolId='+id+'&status=1', 
                headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },               


            });

        }


      function getEmployeeDetailsByEmpId(empid) {

            return $http({
                method: 'GET',
                url: URL_CONSTANT.main_url + 'getallemployeebyid?empId='+empid, 
                headers: { 'Content-Type': 'application/json' ,'Bearer': $window.localStorage.token},               


            });

        }


        function getAllTeacherAllocation(schoolId,status) {

            return $http({
                method: 'GET',
                url: URL_CONSTANT.main_url + 'getallallocatedteacher?schoolId='+schoolId+'&status='+status, 
                headers: { 'Content-Type': 'application/json' ,'Bearer': $window.localStorage.token},               


            });

        }
        function saveOrUpdateDepartment(data) {
            data = JSON.stringify(data);
            console.log(data);

            return $http({
                method: 'POST',
                url: URL_CONSTANT.main_url + 'adddepartment',
                headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },
                data: data


            });

        }



    function getAllDepartmentlist(index,page,id,status) {
            

            return $http({
                method: 'GET',
                url: URL_CONSTANT.main_url + 'getalldepartment?currentIndex='+index+'&rowPerPage='+page+'&schoolId='+id+'&status='+status,
                headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },               


            });

        }



    function getAllDesignation(index,page,id,status) {

            return $http({
                method: 'GET',
                url: URL_CONSTANT.main_url + 'getalldesignation?currentIndex='+index+'&rowPerPage='+page+'&schoolId='+id+'&status='+status, 
                headers: { 'Content-Type': 'application/json' ,'Bearer': $window.localStorage.token},               


            });

        }





function saveOrUpdateDesignation(data) {
            data = JSON.stringify(data);
            console.log(data);

            return $http({
                method: 'POST',
                url: URL_CONSTANT.main_url + 'adddesignation',
                headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },
                data: data


            });

        }

   function getAllEmployeeByDesignation(desigid) {

            return $http({
                method: 'GET',
                url: URL_CONSTANT.main_url + 'getallemployeebydesignation?designationId='+desigid, 
                headers: { 'Content-Type': 'application/json', 'Bearer': $window.localStorage.token},               


            });

        }






            //add student addEmployeeAttendence

                function addEmployeeAttendence(data) {

          
            data = JSON.stringify(data);
            console.log(data);
           

            // console.log(URL_CONSTANT.main_url + 'addstudent');

            return $http({
                method: 'POST',
                url: URL_CONSTANT.main_url + 'addstaffattendence',
                headers: { 'Content-Type': 'application/json', 'Bearer': $window.localStorage.token},
                data: data


            });

        }    


    })