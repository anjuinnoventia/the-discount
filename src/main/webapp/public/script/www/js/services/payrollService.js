angular.module('starter.payrollService', [])

    .factory('payrollService', function($http, $localStorage,MAIN_CONSTANT,$window) {

        return {
            // saveOrUpdatePayheadType: saveOrUpdatePayheadType,
            // getAllPayHead: getAllPayHead,
            // employeesalarycollection: employeesalarycollection,
            // getAllEmployeeSalary:getAllEmployeeSalary,
            // addincomedata:addincomedata,
            // addexpensedata:addexpensedata,
            // getIncome:getIncome,
            // getExpense:getExpense,
            // addincomehead:addincomehead,
            // addexpensehead:addexpensehead,
            // getincomehead:getincomehead,
            // getexpensehead:getexpensehead
            addIncomeOrExpenseHead,
            getAllIncomeOrExpenseHead:getAllIncomeOrExpenseHead,
            addincomedata:addincomedata,
            getAllIncome:getAllIncome,
            getAllExpense:getAllExpense,
            addexpensedata:addexpensedata


        };

        // function addIncomeOrExpenseHead(data) {
        //     data = JSON.stringify(data);
        //     console.log(data);

        //     return $http({
        //         method: 'POST',
        //         url: URL_CONSTANT.main_url + 'addexpensehead',
        //         headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },

        //         data: data


        //     });

        // }


     function addIncomeOrExpenseHead(data) { 
        data = JSON.stringify(data);
        console.log(data);
        return $http({
                method: 'POST',
                url:MAIN_CONSTANT.site_url + "/api/account/addincomeexpensehead",
                headers: { 'Content-Type': 'application/json','Bearer':$localStorage.token},
                data: data


            });
    }
         function getAllIncomeOrExpenseHead(incomeOrExpense) { 
              return $http({
                method: 'GET',
                url:MAIN_CONSTANT.site_url + "/api/account/getincomeexpensehead?incomeOrExpense="+incomeOrExpense+"&incomExpnsHeadId=0&status=1",
                headers: { 'Content-Type': 'application/json','Bearer':$localStorage.token},
              
            });
    }
        //     function addincomedata(data) {
        //     data = JSON.stringify(data);
        //     console.log(data);

        //     return $http({
        //         method: 'POST',
        //         url: URL_CONSTANT.main_url + 'addincome',
        //         headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },

        //         data: data


        //     });

        // }
        function addincomedata(data) { 
        data = JSON.stringify(data);
        console.log(data);
        return $http({
                method: 'POST',
                url:MAIN_CONSTANT.site_url + "/api/account/addincome",
                headers: { 'Content-Type': 'application/json','Bearer':$localStorage.token},
                data: data


            });
    }
    function addexpensedata(data) { 
        data = JSON.stringify(data);
        console.log(data);
        return $http({
                method: 'POST',
                url:MAIN_CONSTANT.site_url + "/api/account/addexpense",
                headers: { 'Content-Type': 'application/json','Bearer':$localStorage.token},
                data: data


            });
    }
        function getAllIncome() { 
              return $http({
                method: 'GET',
                url:MAIN_CONSTANT.site_url + "/api/account/getincome?currentIndex=0&rowPerPage=100&incomeId=0&status=1",
                headers: { 'Content-Type': 'application/json','Bearer':$localStorage.token},
              
            });
         }
         function getAllExpense() { 
              return $http({
                method: 'GET',
                url:MAIN_CONSTANT.site_url + "/api/account/getexpense?currentIndex=0&rowPerPage=100&expenseId=0&status=1",
                headers: { 'Content-Type': 'application/json','Bearer':$localStorage.token},
              
            });
         }

        // function getincomehead() {
        //     return $http({
        //         method: 'GET',
        //         url: URL_CONSTANT.main_url + 'getincomehead?incomeOrExpense=7',
        //         headers: { 'Content-Type': 'application/JSON; charset=UTF-8','Bearer': $window.localStorage.token },


        //     })
        // }
  
    // var deferred = $q.defer();
    //adppostData
  //   console.log(Data);
  //   $http({
  //     method: "POST",
  //     headers: {
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json',
  //       'Bearer':$localStorage.token
  //     },
  //     url: MAIN_CONSTANT.site_url + "/api/account/addincomeexpensehead",
  //     data: Data
  //   }).then(function(data) {
  //      // deferred.resolve(data);
  //       // alert(data);
  //       console.log(data);
  //     },
  //     function myError(response) {
  //       console.log("Please check your internet connection");
  //     });
  //  // return deferred.promise;


  // }





  //  function getAllPayHead(index,page,id){
  //           return $http({
  //           method: 'GET',
  //            url: URL_CONSTANT.main_url+'getallpayhead?currentIndex=' + index + '&rowPerPage=' + page + '&schoolId='+id,
  //           headers: { 'Content-Type': 'application/json', 'Bearer': $window.localStorage.token},
        
  //       })
  //       }




  //       function saveOrUpdatePayheadType(data) {


  //           data = JSON.stringify(data);
  //           console.log(data);

  //           console.log(URL_CONSTANT.main_url + 'addpayhead');

  //           return $http({
  //               method: 'POST',
  //               url: URL_CONSTANT.main_url + 'addpayhead',
  //              headers: { 'Content-Type': 'application/json', 'Bearer': $window.localStorage.token},
  //               data: data


  //           });

  //       }






  //       function employeesalarycollection(data) {
  //           data = JSON.stringify(data);
  //           console.log(data);

  //           return $http({
  //               method: 'POST',
  //               url: URL_CONSTANT.main_url + 'salarycollection',
  //               headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },

  //               data: data


  //           });

  //       }

       
  //       function getAllEmployeeSalary(empid,schoolid) {
  //           return $http({
  //               method: 'GET',
  //               url: URL_CONSTANT.main_url + 'getemployeesalaryempid?empId='+empid+'&schoolId='+ schoolid,
  //               headers: { 'Content-Type': 'application/JSON; charset=UTF-8','Bearer': $window.localStorage.token },


  //           })
  //       }
  //       function addincomehead(data) {
  //           data = JSON.stringify(data);
  //           console.log(data);

  //           return $http({
  //               method: 'POST',
  //               url: URL_CONSTANT.main_url + 'addincomehead',
  //               headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },

  //               data: data


  //           });

  //       }

        // function addexpensehead(data) {
        //     data = JSON.stringify(data);
        //     console.log(data);

        //     return $http({
        //         method: 'POST',
        //         url: URL_CONSTANT.main_url + 'addexpensehead',
        //         headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },

        //         data: data


        //     });

        // }

  //       function addincomedata(data) {
  //           data = JSON.stringify(data);
  //           console.log(data);

  //           return $http({
  //               method: 'POST',
  //               url: URL_CONSTANT.main_url + 'addincome',
  //               headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },

  //               data: data


  //           });

  //       }
  //       function addexpensedata(data) {
  //           data = JSON.stringify(data);
  //           console.log(data);

  //           return $http({
  //               method: 'POST',
  //               url: URL_CONSTANT.main_url + 'addexpense',
  //               headers: { 'Content-Type': 'application/json','Bearer': $window.localStorage.token },

  //               data: data


  //           });

  //       }

  //         function getincomehead() {
  //           return $http({
  //               method: 'GET',
  //               url: URL_CONSTANT.main_url + 'getincomehead?incomeOrExpense=7',
  //               headers: { 'Content-Type': 'application/JSON; charset=UTF-8','Bearer': $window.localStorage.token },


  //           })
  //       }

  //         function getexpensehead() {
  //           return $http({
  //               method: 'GET',
  //               url: URL_CONSTANT.main_url + 'getexpensehead?incomeOrExpense=8',
  //               headers: { 'Content-Type': 'application/JSON; charset=UTF-8','Bearer': $window.localStorage.token },


  //           })
  //       }

  //         function getIncome() {
  //           return $http({
  //               method: 'GET',
  //               url: URL_CONSTANT.main_url + 'getincome?currentIndex=0&rowPerPage=100',
  //               headers: { 'Content-Type': 'application/JSON; charset=UTF-8','Bearer': $window.localStorage.token },


  //           })
  //       }

  // function getExpense() {
  //           return $http({
  //               method: 'GET',
  //               url: URL_CONSTANT.main_url + 'getexpense?currentIndex=0&rowPerPage=100',
  //               headers: { 'Content-Type': 'application/JSON; charset=UTF-8','Bearer': $window.localStorage.token },


  //           })
  //       }




    })