angular.module('starter.inputService', [])
  .factory('inputService', inputService);

function inputService(TYPE_POST, $q, $http, MAIN_CONSTANT,$localStorage) {
$http.defaults.headers.common['Bearer'] = $localStorage.token;
//alert( $localStorage.token);

  return {
    getcategories:getcategories,
    getallproductsbyname:getallproductsbyname,
    getactivecategories:getactivecategories,
    submitAddCategoryDetails: submitAddCategoryDetails,
    getsubcategories:getsubcategories,
    submitAddSubCategoryDetails:submitAddSubCategoryDetails,
    updateCustomerDetails:updateCustomerDetails,
    submitAddProductDetails:submitAddProductDetails,
    getsubcategoriesByCategoryId:getsubcategoriesByCategoryId,
    getcustomerreviews:getcustomerreviews,
    getcustomerstatistics:getcustomerstatistics,
    getordersByUserId:getordersByUserId,
    getorderdetailsbyorderId:getorderdetailsbyorderId,
    submitAddCustomerDetails:submitAddCustomerDetails,
    getallcustomersbysearch:getallcustomersbysearch,
    submitAddTaxDetails:submitAddTaxDetails,
    getrelations:getrelations,
    getallattributes:getallattributes,
    submitAddAttributeDetails:submitAddAttributeDetails,
    getattributesforaddproduct:getattributesforaddproduct,
    
    submitEditCategoryDetails:submitEditCategoryDetails,
    getallcustomers:getallcustomers,
    gethomedatas:gethomedatas,
    changeorderstatus:changeorderstatus,
    submitAddManufactureDetails:submitAddManufactureDetails,
    // getmanufactures:getmanufactures,
    getallmanufactures:getallmanufactures,
    submitAddOfferDetails:submitAddOfferDetails,
    assigningoffertoproduct:assigningoffertoproduct,
    removeingoffertoproduct:removeingoffertoproduct,
    getallsliderimages:getallsliderimages,
    saveAddDetails:saveAddDetails,
    getallbannerimages:getallbannerimages,
    getalloffers:getalloffers,
    Addstocks:Addstocks,
    getallreviews:getallreviews,
    confirmingreturnitem:confirmingreturnitem,
    rejectingreturnitem:rejectingreturnitem,
    returnedreturnitem:returnedreturnitem,
    getallpromocodes:getallpromocodes,
    createPromoCode:createPromoCode,
    savepromocodedetails:savepromocodedetails,
    changepromocodestatus:changepromocodestatus,
    getallaccountsettings:getallaccountsettings,
    submitEditSellerdetails:submitEditSellerdetails,
    updateSellerDetails:updateSellerDetails,
    getallcmsdata:getallcmsdata,
    savecmsdatas:savecmsdatas,
    getlowstockproducts:getlowstockproducts,
    getproductreports:getproductreports,
    gettransactionreports:gettransactionreports,
    getbestpurchasedproductsreports:getbestpurchasedproductsreports,
    savecommentonreview:savecommentonreview,
    getallmonthlyearnings:getallmonthlyearnings,
savePromoterdetails:savePromoterdetails,
getallpromorters:getallpromorters,
getpromocodedetailsbypromoter:getpromocodedetailsbypromoter,
getpromocodedetails:getpromocodedetails,
getsalesreports:getsalesreports,
getallweeklyearnings:getallweeklyearnings,
getsortproducts:getsortproducts,   
    getproducts:getproducts,
    ChangeCustomerStatus:ChangeCustomerStatus,
    rejectreturnitem:rejectreturnitem,
    getAllCommisionReport:getAllCommisionReport,
    getTaxReport:getTaxReport,
    getIncomeReport:getIncomeReport
    // getMonthlyQueryReport:getMonthlyQueryReport
    // categorymanage:categorymanage

  };



   function ChangeCustomerStatus(customerId,status) {
    console.log(customerId,status);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/changecustomerstatus?customerId="+customerId+"&status="+status,
     
        }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }


    function getallattributes(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/getallproductattributes",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  
      function getsalesreports(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/getallsalesreport",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  

      function savecommentonreview(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/addcomments",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  
      function getpromocodedetailsbypromoter(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/getallpromocodereports",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

    function gettransactionreports(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/transactionReport",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

    function getAllCommisionReport(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/commissionreport",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);       
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
      function getTaxReport(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/taxreport",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);       
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  
    function getIncomeReport(Data) {  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/incomeexpencereport",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);       
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
    function getbestpurchasedproductsreports(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/bestpurchasedproducts",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

  
  function getproductreports(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/productreport",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  
  function getlowstockproducts(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/lowstockreport",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


function savecmsdatas(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/savecms",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
function getallcmsdata(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/getallcms",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  // updateShopDetails

function updateSellerDetails(Data) {
  
  
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/addsellerdetails",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  

   function rejectingreturnitem(orderid,orderDetailsIds) {
    console.log(orderid,orderDetailsIds);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/changeorderstatus?orderId="+orderid+"&orderlyStatus=12&orderDetailsIds[]="+orderDetailsIds,
     
        }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

   function confirmingreturnitem(returnItemId) {
    console.log(returnItemId);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/changereturnstatus?returnItemId="+returnItemId+"&status=11",
      
      // /api/order/changeorderstatus?orderId="+orderid+"&orderlyStatus=11&orderDetailsIds[]="+orderDetailsIds
     
        }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
function rejectreturnitem(returnItemId) {
    console.log(returnItemId);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/changereturnstatus?returnItemId="+returnItemId+"&status=12",
      
      // /api/order/changeorderstatus?orderId="+orderid+"&orderlyStatus=11&orderDetailsIds[]="+orderDetailsIds
     
        }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
     function returnedreturnitem(returnItemId) {
    console.log(returnItemId);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/changereturnstatus?returnItemId="+returnItemId+"&status=15",
      
      // /api/order/changeorderstatus?orderId="+orderid+"&orderlyStatus=11&orderDetailsIds[]="+orderDetailsIds
     
        }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

  function getalloffers(offerData) {
    var deffered = $q.defer();
    //adppostData
    console.log(offerData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getalloffer",
      data: offerData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
  

   function getallmonthlyearnings(offerData) {
    var deffered = $q.defer();
    //adppostData
    console.log(offerData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/monthlurevenuereport",
      data: offerData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  function getallweeklyearnings(offerData) {
    var deffered = $q.defer();
    //adppostData
    console.log(offerData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/weeklyreport",
      data: offerData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
    function getallpromocodes(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/user/getpromocodedetails",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

    function getpromocodedetails(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallpromocodereports",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
  function submitAddOfferDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/addoffer",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
     function changeorderstatus(orderId,orderstatus) {
    console.log(orderId,orderstatus);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/changeorderstatus?orderId="+orderId+"&orderlyStatus="+orderstatus+"&orderDetailsIds[]=0" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
//get home datas
function gethomedatas() {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
       headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/report/getalldetails" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

  // get attributes for add product
  function getattributesforaddproduct() {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
       headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/getallattributes?attributeId=0&status=1" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

  // get attribute
  // function getallattributes() {
  //   var deffered = $q.defer();
  //   //adppostData
    
  //   $http({
  //     method: "GET",
  //      headers: {
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json',
  //       'Bearer':$localStorage.token
  //     },
  //     url: MAIN_CONSTANT.site_url + "/api/input/getallattributes?attributeId=0&status=2" ,
  //   }).then(function mySucces(response) {
  //       console.log(response);
  //        deffered.resolve(response.data);
  //     },
  //     function myError(response) {
  //       console.log("Please check your internet connection");
  //     });
  //   return deffered.promise;

  // }

  // add attribute


 function submitAddAttributeDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/saveattribute",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


 //edit customer
  function updateCustomerDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/updatecustomer",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }




  //add customer
  function submitAddCustomerDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/savecustomer",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


  // get order by customer

function getordersByUserId(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorders",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  // get orderdetails by order Id

function getorderdetailsbyorderId(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getallorders",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
 

 function getallcustomers(customers) {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/user/getallcustomers" ,
      data:customers
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
  
   function getallpromorters(customers) {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/user/getallpromoters" ,
      data:customers
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

//get customer statistics
function getcustomerstatistics(customers) {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/user/getcustomers" ,
      data:customers
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
  function getactivecategories() {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getcategory?categoryId=0&mainCategoryId=0&companyId=0&status=1" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

   function getallreviews(productId) {
    console.log(productId);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getreviews?fatherId="+productId+"&firstIndex=0&rowPerPage=100&fromDate=0&toDate=0&cityId=0&countryId=0&customerId=0&sellerId=0" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

 function getcategories() {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getcategory?categoryId=0&mainCategoryId=0&status=2&companyId=0" ,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
  function getallsliderimages(Data) {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getalladdstatus" ,
      data:Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
  //  function getallsliderimages(Data) {
  //   var deffered = $q.defer();
  //   //adppostData
    
  //   $http({
  //     method: "POST",
  //     url: MAIN_CONSTANT.site_url + "/api/report/getalladdstatus" ,
  //   }).then(function mySucces(response) {
  //       console.log(response);
  //        deffered.resolve(response.data);
  //     },
  //     function myError(response) {
  //       console.log("Please check your internet connection");
  //     });
  //   return deffered.promise;

  // }
function getallbannerimages(Data) {
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/report/getalladdstatus" ,
      data:Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
  //    function getallbannerimages() {
  //   var deffered = $q.defer();
  //   //adppostData
    
  //   $http({
  //     method: "GET",
  //     url: MAIN_CONSTANT.site_url + "/api/input/getallads?addId=0&sellerId=0&addStatus=2&modeId=2" ,
  //   }).then(function mySucces(response) {
  //       console.log(response);
  //        deffered.resolve(response.data);
  //     },
  //     function myError(response) {
  //       console.log("Please check your internet connection");
  //     });
  //   return deffered.promise;

  // }
   function assigningoffertoproduct(offerid,productId) {
    console.log(offerid,productId);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/user/assignoffer?offerId="+offerid+"&productId="+productId,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
   function removeingoffertoproduct(productId) {
    console.log(productId);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/user/assignoffer?offerId=0&productId="+productId,
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }removeingoffertoproduct

  // GET MANUFACTURE

// function getmanufactures(manufactureData) {
//     var deffered = $q.defer();
//     //adppostData
//     console.log(manufactureData);
//     $http({
//       method: "POST",
//       url: MAIN_CONSTANT.site_url +"/api/input/getallmanufacture",
//       data: manufactureData
//     }).then(function mySucces(response) {
      
//          deffered.resolve(response.data);
//       },
//       function myError(response) {
//         console.log("Please check your internet connection");
//       });
//     return deffered.promise;
//   }
  function getallmanufactures(cityData) {
    var deffered = $q.defer();
    //adppostData
    console.log(cityData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getallmanufacture",
      data: cityData
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

function getallaccountsettings(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/user/getallsellers",
      data: Data
    }).then(function mySucces(response) {
        console.log(response);
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }
  // search customer


  function getallcustomersbysearch(customerData) {
    var deffered = $q.defer();
    //adppostData
    console.log(customerData);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url +"/api/user/getallcustomers",
      data: customerData
    }).then(function mySucces(response) {
      
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }

  //search product by name
  function getallproductsbyname(Data) {
    var deffered = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url +"/api/input/getproducts",
      data: Data
    }).then(function mySucces(response) {
      
         deffered.resolve(response.data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;
  }



// related products

function getrelations(relation) {

  var relations = [{"modeId": "8", "name": "color"},
     {"modeId": "9", "name": "material"},
      {"modeId": "10", "name": "type"},
      {"modeId": "11", "name": "capacity"},
      {"modeId": "12", "name": "size"},
      {"modeId": "13", "name": "length"},
      {"modeId": "14", "name": "width"},
      {"modeId": "15", "name": "height"},
       {"modeId": "16", "name": "weight"},
       ];
console.log( relations);

 // var relations= [
 //      {"modeId": "8", "name": "color"},
 //      {"modeId": "9", "name": "material"},
 //      {"modeId": "10", "name": "type"},
 //      {"modeId": "11", "name": "capacity"},
 //      {"modeId": "12", "name": "size"},
 //      {"modeId": "13", "name": "length"},
 //      {"modeId": "14", "name": "width"},
 //      {"modeId": "15", "name": "height"},
 //       {"modeId": "16", "name": "weight"},
     
 //    ];
 //    console.log( relations);

  }

  // getproducts

function getproducts(products) {
    var deffered = $q.defer();
    console.log(products);
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getproducts" ,
      data:products
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);


          // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }



function getsortproducts(products) {
    var deffered = $q.defer();
    console.log(products);
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/sortdata" ,
      data:products
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);


          // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

  // getcustomer reviews

function getcustomerreviews(customerreviews) {
    var deffered = $q.defer();
    console.log(customerreviews);
    //adppostData
    
    $http({
      method: "POST",
      url: MAIN_CONSTANT.site_url + "/api/input/getreviews" ,
      data:customerreviews
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);


          // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

  
//add tax

 function submitAddTaxDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/order/addtax",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


  



// add category


 function submitAddCategoryDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/savecategory",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  

   function submitEditSellerdetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/addsellerdetails",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  

 function createPromoCode(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/generatepromocode",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
   function savepromocodedetails(promocodedata) {
  console.log(promocodedata);
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(promocodedata);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/savepromocodedetails",
      data: promocodedata
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }
  

 // function Addstocks(productId,productQuantity) {
  
 //  console.log( $localStorage.token);
 //    var deferred = $q.defer();
 //    //adppostData
 //    console.log(productId,productQuantity);
 //    $http({
 //      method: "POST",
 //      headers: {
 //        'Accept': 'application/json',
 //        'Content-Type': 'application/json',
 //        'Bearer':$localStorage.token
 //      },
 //      url: MAIN_CONSTANT.site_url + "/api/input/updatestock?productId="+productId+"&productQuantity="+productQuantity,
 //      data: Data
 //    }).then(function(data) {
 //        deferred.resolve(data);
 //        // alert(data);
 //        console.log(data);
 //      },
 //      function myError(response) {
 //        console.log("Please check your internet connection");
 //      });
 //    return deferred.promise;


 //  }
   function Addstocks(productId,productQuantity) {
  console.log(productId,productQuantity);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/updatestock?productId="+productId+"&productQuantity="+productQuantity,
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);

         // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }
  
    function changepromocodestatus(codeId,status) {
  console.log(codeId,status);
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/user/changepromocodestatus?codeId="+codeId+"&status="+status,
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);

         // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

   function saveAddDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/saveadd",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


  // ADD MANUFACTURE

 function submitAddManufactureDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/savemanufacture",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }



// get subcategories



 function getsubcategories() {
  console.log();
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getsubcategory?mainCategoryId=0&categoryId=0&companyId=0&status=2" ,
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);

         // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

  //get subcategory by category

  function getsubcategoriesByCategoryId(catId) {
  console.log();
    var deffered = $q.defer();
    //adppostData
    
    $http({
      method: "GET",
      url: MAIN_CONSTANT.site_url + "/api/input/getsubcategory?mainCategoryId=0&status=1&companyId=0&categoryId="+ catId ,
    }).then(function mySucces(response) {
        console.log(response);
        if (response.data.errorCode === 0) {
          deffered.resolve(response.data);
        } else {
          deffered.resolve(response.data);

         // deffered.reject(response.data);
        }
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deffered.promise;

  }

 


  //add subcategory

  // add category


 function submitAddSubCategoryDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
     // alert("hai");
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/savesubcategory",
      data: Data
     
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

  // add product


 function submitAddProductDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/saveproduct",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

 function savePromoterdetails(data) {
  
  console.log( $localStorage.token);
  var content=JSON.stringify(data);
  console.log(content);
    var deferred = $q.defer();
    //adppostData
    console.log(data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/user/savepromoters",
      data: data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }


  // edit category


function submitEditCategoryDetails(Data) {
  
  console.log( $localStorage.token);
    var deferred = $q.defer();
    //adppostData
    console.log(Data);
    $http({
      method: "POST",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Bearer':$localStorage.token
      },
      url: MAIN_CONSTANT.site_url + "/api/input/savecategory",
      data: Data
    }).then(function(data) {
        deferred.resolve(data);
        // alert(data);
        console.log(data);
      },
      function myError(response) {
        console.log("Please check your internet connection");
      });
    return deferred.promise;


  }

// get all subcategories



}




