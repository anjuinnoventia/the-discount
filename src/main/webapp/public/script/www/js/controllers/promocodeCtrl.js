angular.module('starter.promocodeCtrl', [])
  .controller('promocodeCtrl', function($scope, $window, $state, Upload, sellerService, inputService, toaster, FILE_CONSTANT, MAIN_CONSTANT, $stateParams, $localStorage, loadingService) {
    //alert($localStorage.token);
    $scope.imageUrl = FILE_CONSTANT.filepath;
    if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
    $window.scrollTo(0, 0);
    $scope.field1 = 1;
    $scope.field2 = 1;
    $scope.Promoter = {};
    $scope.Promoter.latitude = 15.24;
    $scope.Promoter.longitude = 254.121;
    $scope.displaymsg = 0;
    // $scope.Promoter.password="password123";




    $scope.checkpasswords = function(password1, password2) {

      console.log(password1, password2);
      if (password2) {
        $scope.display = 1;
        console.log($scope.displaymsg);
        if (password1 == password2) {
          $scope.displaymsg = 2;
          // $scope.displaymsg=1;
        } else {
          $scope.displaymsg = 3;
        }

      }
    }

    // PROMOCODE PAGE

    $scope.PromoCode = {};
    $scope.selectone = function(data) {
      console.log(data);
      if (!data) {
        // alert("nodata");
        $scope.field1 = 1;
      };
      var arr = data;
      if (arr.length >= 1) {
        $scope.field1 = 0;
      } else {
        $scope.field1 = 1;
      }

      console.log($scope.field1);

      // (offer.offerPercentage)



    }
    $scope.selectone = function(data) {
      console.log(data);
      if (!data) {
        // alert("nodata");
        $scope.field1 = 1;
      };
      var arr = data;
      if (arr.length >= 1) {
        $scope.field1 = 0;
      } else {
        $scope.field1 = 1;
      }

      console.log($scope.field1);

      // (offer.offerPercentage)



    }
    $scope.selectonetwo = function(data) {
      console.log(data);
      if (!data) {
        // alert("nodata");
        $scope.field2 = 1;
      };
      var arra = data;
      if (arra.length >= 1) {
        $scope.field2 = 0;
      } else {
        $scope.field2 = 1;
      }

      console.log($scope.field2);

      // (offer.offerPercentage)



    }



    $scope.generatePromoCode = function(promocodedata) {
      console.log(promocodedata.promocode);

      console.log(promocodedata);
      promocodedata.validFrom = new Date(promocodedata.validFrom).getTime();
      console.log(promocodedata.validFrom);
      promocodedata.validTo = new Date(promocodedata.validTo).getTime();
      console.log(promocodedata.validTo);
      if (!promocodedata.promocode) {
        promocodedata.promocode = promocodedata.generatedcode;
        console.log(promocodedata);
      }
      console.log(promocodedata.promocode);
      inputService.savepromocodedetails(promocodedata).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);

        if (response.data.errorCode === 1) {

          toaster.pop('success', "Successfull..!", "Successfully Generated Promo Code!");



          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          $window.location.href = '#/admin/promocode';
        } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          toaster.pop('error', "Oops..!", response.data.errorMessage);
          // alert(response.data.errorMessage);
        }

      });

    }
    $scope.getPromoCode = function() {


      $scope.promodatavo = {

      }
      inputService.createPromoCode($scope.promodatavo).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);

        if (response.data.errorCode === 1) {
          $scope.PromoCode.generatedcode = response.data.object;
          // $scope.generatedcode = response.data.object;
          console.log($scope.PromoCode.generatedcode);
          // toaster.pop('success', "Successfull..!", "Successfully Generated New Promo Code!");



          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          // $window.location.href = '#/admin/category';
        } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          toaster.pop('error', "Oops..!", response.data.errorMessage);
          // alert(response.data.errorMessage);
        }

      });

    }
    if ($state.current.name === 'admin.promocode') {
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }


      $scope.promocodePageVo = {
        "currentIndex": 0,
        "rowPerPage": 250,
        "codeId": 0,
        "status": 2
      }


      $scope.getallpromocode = function() {
        loadingService.isLoadin = true;

        inputService.getallpromocodes($scope.promocodePageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);


          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.promocodes = response.object;
            $scope.nooffers = 0;
            console.log($scope.promocodes);

          } else if (response.errorCode === -3) {
            $scope.nooffers = 1;

          } else {
            //alert(response.errorMessage);
          }
        });
      }
      $scope.getallpromocode();

      $scope.promocodemanage = function(code) {
        loadingService.isLoadin = true;
        console.log(code);
        if (code.codeId) {
          // alert(category.categoryId);

          $localStorage.PromoCode = code;
          loadingService.isLoadin = false;
          //


        }
        $window.location.href = '#/admin/addpromocode/' + code.codeId;



      };
      $scope.viewpromocode = function(promocode) {
        loadingService.isLoadin = true;
        console.log(promocode);
        $window.location.href = '#/admin/viewpromocode/' + promocode.codeId;


        if (promocode.codeId) {
          // alert(category.categoryId);

          $localStorage.promocode = promocode;
          loadingService.isLoadin = false;
          //


        }
      };

    }


    $scope.changepromocodestatus = function(promocode) {

      console.log(promocode);

      var codeId = promocode.codeId;
      if (promocode.status == 1) {
        promocode.status = 0;
      } else {
        promocode.status = 1;
      }
      var status = promocode.status;
      //  customer.status = 0;
      //   customer.password = "password123";
      // console.log(customer);
      inputService.changepromocodestatus(codeId, status).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);

        if (response.errorCode === 0) {

          toaster.pop('success', "Successfull..!", "Successfully Updated Promo Code Status!");

          $scope.getallpromocode();

          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          $window.location.href = '#/admin/promocode';
        } else if (response.errorCode === 45 || response.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          toaster.pop('error', "Oops..!", response.errorMessage);
          // alert(response.data.errorMessage);
        }

      });

      // $scope.updateCustomerasdeactivateoractivate(customer);


    };


    $scope.activatepromocode = function(customer) {

      console.log(customer);


      customer.status = 1;
      console.log(customer);
      $scope.updateCustomerasdeactivateoractivate(customer);


    };
    $scope.getactivecountry = function(country) {
      // alert("tfgzdygy");
      loadingService.isLoadin = false;
      sellerService.getactivecountries(country).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.countries = response.object;
        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        } else {
          loadingService.isLoadin = false;



        }
      }, function() {
        loadingService.isLoadin = false;


      });
    };
    $scope.getactivecountry();


    // $scope.changeCountry = function(countryId) {
    //   console.log(countryId);


    //   $scope.getcityByCountryId(countryId)

    // };
    $scope.getcityByCountryId = function(countryId) {
      console.log(countryId);
      $scope.cityPageVo = {
        "countryId": countryId,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
        "cityName": "",
        "citusStatus": 1,
        "status": []
      }

      $scope.citiesbycountries = [];

      sellerService.getcitiesByCountryId($scope.cityPageVo, countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        };
      });
    }

    $scope.getcityByCountryId();


    $scope.upload = function(file) {
      $scope.isFileuploadStart = true;
      if (file) {

        $scope.clickimg = true;
        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/pentalupload",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },

          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          if (resp.data.errorCode === 0) {
            $scope.isFileuploadStart = false;
            $scope.Promoter.profileImage = resp.data.object;

            console.log($scope.Promoter.profileImage);


          } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
            $scope.isFileuploadStart = false;
            $localStorage.token = 0;
            $window.location.href = '#/login';

          } else {

          }





        }, function(resp) {
          $scope.isFileuploadStart = false;
          $scope.progressPercentage = 0;
          // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
          if ($scope.Promoter.profileImage) {
            //splice i
            $scope.Promoter.profileImage;

          }


          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };
    $scope.deleteimage = function() {
      $scope.Promoter.profileImage = "";

    }

    $scope.savePromoter = function(getAddProductDetails) {
      getAddProductDetails.password = "password123";

      if (!getAddProductDetails.customerName) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Name of Promoter ");
          } else {
            toaster.pop('info', "Please Enter Name of Promoter");
          }

        } else if (getAddProductDetails.phoneNo==null) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Phone Number");
          } else {
            toaster.pop('info', "Please Enter Phone Number");
          }
        } else if (!getAddProductDetails.emailId) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Email Id");
          } else {
            toaster.pop('info', "Please Enter Email Id");
          }
        } else if (!getAddProductDetails.address) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Address");
          } else {
            toaster.pop('info', "Please Enter Address");
          }
        }else if (!getAddProductDetails.country) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Choose Country");
          } else {
            toaster.pop('info', "Please Choose Country");
          }
        }else if (!getAddProductDetails.city) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Choose City");
          } else {
            toaster.pop('info', "Please Choose City");
          }
        }else if (getAddProductDetails.zipCode==null) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter zipCode");
          } else {
            toaster.pop('info', "Please Enter zipCode");
          }
        }else if (getAddProductDetails.status != 0 && getAddProductDetails.status != 1) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Choose Status");
          } else {
            toaster.pop('info', "Please Choose Status");
          }

        } else if (!getAddProductDetails.profileImage) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Upload Profile Image");
          } else {
            toaster.pop('info', "Please Upload Profile Image");
          }
        }else{
           loadingService.isLoadin = true;
      console.log(getAddProductDetails);



      inputService.savePromoterdetails(getAddProductDetails).then(function(response) {

        loadingService.isLoadin = false;
        console.log(response);

        if (response.data.errorCode === 0) {
          if ($stateParams.customerId == '0') {
            // toastrMsg(1,'Added new Category!','Successfull..!');
            // $scope.pop = function(){
            toaster.pop('success', "Successfull..!", "Added New Promoter!");
            // toaster.pop('success', "Added new Category!", "Successfull..!");
            // };
          } else {
            toaster.pop('success', "Successfull..!", "Updated  Promoter!");

          }
          // toaster.pop('success', "Successfull..!", "Added New Product!");

          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          $window.location.href = '#/admin/promocode';
        } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          // alert(response.data.errorMessage);
          toaster.pop('error', "Oops..!", response.data.errorMessage);
        }

      });

        }
    }




    $scope.noPages = 1;
    $scope.getNumber = function(num) {
      return new Array(num);
    }


    $scope.pageno = 1;


    $scope.getallpromoters = function(pageno) {
      $scope.promorterPageVo = {
        "customerId": 0,
        "cityId": 0,
        "countryId": 0,
        "userId": 0,
        "fromDate": 0,
        "toDate": 0,
        "currentIndex": 0,
        "rowPerPage": "75",
        "status": 2,
        "customerName": "",
        "phoneNo": "",
        "emailId": ""

      }
      // alert("haai");
      loadingService.isLoadin = true;
      $scope.promorterPageVo.currentIndex = (pageno - 1) * $scope.promorterPageVo.rowPerPage;

      inputService.getallpromorters($scope.promorterPageVo).then(function(response) {
        loadingService.isLoadin = false;
        $scope.noPages = Math.ceil(response.totalRecords / $scope.promorterPageVo.rowPerPage);
        console.log(response);



        if (response.errorCode === 0) {
          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          $scope.promorters = response.object;
          console.log($scope.promorters);



        } else {
          //alert(response.errorMessage);
        }
      }, function() {
       // alert('Oops! somthing went wrong');
      });
    };
    $scope.getallpromoters($scope.pageno);




    if ($state.current.name === 'admin.addpromocode') {
      $scope.PromoCode = {};


      // $scope. category.imageName ="nothing";
      if ($stateParams.codeId !== '0') {
        $scope.PromoCode = $localStorage.PromoCode;
        $scope.catcid = 1;
      } else {
        $scope.catcid = 0;
      }
      if ($scope.PromoCode.validFrom) {
        $scope.PromoCode.validFrom = new Date($scope.PromoCode.validFrom);
      };
      if ($scope.PromoCode.validTo) {
        $scope.PromoCode.validTo = new Date($scope.PromoCode.validTo);
      };



      $scope.getallpromoters = function() {
        $scope.promorterPageVo = {
          "customerId": 0,
          "cityId": 0,
          "countryId": 0,
          "userId": 0,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": "350",
          "status": 1,
          "customerName": "",
          "phoneNo": "",
          "emailId": ""

        }
        // alert("haai");
        loadingService.isLoadin = true;
        // $scope.promorterPageVo.currentIndex = (pageno - 1) * $scope.promorterPageVo.rowPerPage;

        inputService.getallpromorters($scope.promorterPageVo).then(function(response) {
          loadingService.isLoadin = false;
          // $scope.noPages = Math.ceil(response.totalRecords / $scope.promorterPageVo.rowPerPage);
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.activepromorters = response.object;
            console.log($scope.activepromorters);



          } else {
            //alert(response.errorMessage);
          }
        }, function() {
          //alert('Oops! somthing went wrong');
        });
      };
      $scope.getallpromoters();







    }


    $scope.viewpromoterdetails = function(promoter) {
      // alert("haaai");
      loadingService.isLoadin = true;
      console.log(promoter);
      $window.location.href = '#/admin/viewpromoter/' + promoter.customerId;


      if (promoter.customerId) {
        // alert(category.categoryId);

        $localStorage.promoter = promoter;
        loadingService.isLoadin = false;
        //


      }
    };


    if ($state.current.name === 'admin.viewpromoter') {


      loadingService.isLoadin = false;
      $scope.promoter = {};
      // $scope. city.imageName ="nothing";
      if ($stateParams.customerId !== '0') {
        $scope.promoter = $localStorage.promoter;
      }

      $scope.promoterdetailsPageVo =

        {
          "rowPerPage": 100,
          "currentIndex": "0",
          "fromDate": "",
          "toDate": "",
          "codeId": "",
          "userId": $scope.promoter.userId
        }
      console.log($scope.promoterdetailsPageVo);

      $scope.noPagesoder = 1;
      $scope.getNumberforordercount = function(num) {
        return new Array(num);
      }


      $scope.pagenumberfororder = 1;


      $scope.getpromocodedetailsbypromoter = function(pagenumberfororder) {
        console.log(pagenumberfororder);

        console.log($scope.promoterdetailsPageVo);
        loadingService.isLoadin = true;
        $scope.promoterdetailsPageVo.currentIndex = (pagenumberfororder - 1) * $scope.promoterdetailsPageVo.rowPerPage;

        inputService.getpromocodedetailsbypromoter($scope.promoterdetailsPageVo).then(function(response) {
          loadingService.isLoadin = false;
          $scope.noPagesoder = Math.ceil(response.data.totalRecords / $scope.promoterdetailsPageVo.rowPerPage);
          console.log(response);


          if (response.data.errorCode === 0) {


            $scope.listoforders = response.data.object.ORDER;
            $scope.totalamount = response.data.object.PRICE;


            console.log($scope.listoforders);

            $scope.nototalcompletedorders = 0;

          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.data.errorCode === -3) {
            loadingService.isLoadin = false;
            $scope.nototalcompletedorders = 1;




          } else {
            // alert("helllo");
           // alert(response.data.errorMessage);
          }
        });
      }
      $scope.getpromocodedetailsbypromoter($scope.pagenumberfororder);




    }

    $scope.viewpromocodedetails = function(promocode) {
      // alert("haaai");
      loadingService.isLoadin = true;
      console.log(promocode);
      $window.location.href = '#/admin/viewordersbypromocode/' + promocode.codeId;


      if (promocode.codeId) {
        // alert(category.categoryId);

        $localStorage.promocode = promocode;
        loadingService.isLoadin = false;
        //


      }
    };


    //     };
    if ($state.current.name === 'admin.viewordersbypromocode') {
      //  if( $localStorage.roleId !== 7){
      //   $window.location.href ='#/login'

      // }

      // alert(99999)

      loadingService.isLoadin = false;
      $scope.promocode = {};
      // $scope. city.imageName ="nothing";

      $scope.promocode = $localStorage.promocode;
      console.log($scope.promocode);

      $scope.promocodedetailsPageVo =

        {
          "rowPerPage": 100,
          "currentIndex": "0",
          "fromDate": "",
          "toDate": "",
          "codeId": $scope.promocode.codeId
          // "userId": $scope.promoter.customerId
        }
      console.log($scope.promocodedetailsPageVo);

      $scope.noPagesoder = 1;
      $scope.getNumberforordercount = function(num) {
        return new Array(num);
      }


      $scope.pagenumberfororder = 1;


      $scope.getpromocodedetails = function(pagenumberfororder) {
        console.log(pagenumberfororder);

        console.log($scope.promocodedetailsPageVo);
        loadingService.isLoadin = true;
        $scope.promocodedetailsPageVo.currentIndex = (pagenumberfororder - 1) * $scope.promocodedetailsPageVo.rowPerPage;

        inputService.getpromocodedetails($scope.promocodedetailsPageVo).then(function(response) {
          loadingService.isLoadin = false;
          $scope.noPagesoder = Math.ceil(response.totalRecords / $scope.promocodedetailsPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            // $scope.totalcompletedorders = response.object;
            $scope.totalnumberoforder = response.object.totalRecords;

            $scope.listofordersbypromocode = response.object.ORDER;
            $scope.toatlPRICE = response.object.PRICE;
            console.log($scope.listofordersbypromocode);
            console.log($scope.totalorders);
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            // console.log($scope.orderdetails);
            $scope.nototalcompletedorders = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            loadingService.isLoadin = false;
            $scope.nototalcompletedorders = 1;




          } else {
            // alert("helllo");
          //  alert(response.errorMessage);
          }
        });
      }
      $scope.getpromocodedetails($scope.pagenumberfororder);

    }


    if ($state.current.name === 'admin.viewpromocode') {

      $scope.PromoCode = {};


      // $scope. category.imageName ="nothing";
      if ($stateParams.codeId !== '0') {
        $scope.PromoCode = $localStorage.promocode;
        $scope.catcid = 1;
      } else {
        $scope.catcid = 0;
      }

      console.log($scope.PromoCode);
      //      if ( $scope.PromoCode.validFrom ) {
      //   $scope.PromoCode.validFrom=new Date($scope.PromoCode.validFrom);
      // };
      //       if ( $scope.PromoCode.validTo) {
      //          $scope.PromoCode.validTo=new Date($scope.PromoCode.validTo);
      //        };


    }

    $scope.promotermanage = function(promoter) {
      loadingService.isLoadin = true;
      console.log(promoter);
      $window.location.href = '#/admin/savepromoter/' + promoter.customerId;


      if (promoter.customerId) {
        // alert(category.categoryId);

        $localStorage.Promoter = promoter;
        loadingService.isLoadin = false;
        //


      }
    };

    if ($state.current.name === 'admin.savepromoter') {
      $scope.Promoter = {};


      // $scope. category.imageName ="nothing";
      if ($stateParams.customerId !== '0') {
        $scope.Promoter = $localStorage.Promoter;
        $scope.catcid = 1;
      } else {
        $scope.catcid = 0;
      }
      if ($scope.Promoter.phoneNo) {
        $scope.Promoter.phoneNo = parseInt($scope.Promoter.phoneNo);
        console.log($scope.Promoter.phoneNo);

      } else {
        // alert("no");
      }


    }

  });
