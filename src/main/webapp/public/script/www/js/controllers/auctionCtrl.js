angular.module('starter.auctionCtrl', [])
  .controller('auctionCtrl', function(FILE_CONSTANT,MAIN_CONSTANT,Upload,$scope,$window, toaster,$state,inputService,storeService,auctionService,$stateParams,$localStorage,loadingService){

    if (!$localStorage.token||$localStorage.token===0) {
    $window.location.href ='#/login';
    }
    


    $scope.imageUrl = FILE_CONSTANT.filepath;
 if ($state.current.name === 'admin.addauction') {

    $scope.auction = {};
     $scope.auction.featureAndPhotos=[];


    $scope.upload = function(file) {
      //$scope.isFileuploadStart=true;



      if (file) {
        var featurejson = {
          "id": new Date().getTime(),
          "modeId": 5,
           "isLoadin": true,
          "extraDataNum": 0,
          "isLoadin": true


        };


        $scope.auction.featureAndPhotos.push(featurejson);
        var id = featurejson.id;

        $scope.clickimg = true;


        console.log(file);
        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/uploadtoaws",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {

          console.log(resp);

          //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
          if (resp.data.errorCode===0) {

          for (var i = 0; i < $scope.auction.featureAndPhotos.length; i++) {
            if ($scope.auction.featureAndPhotos[i].id == id) {
              console.log(resp);
              $scope.auction.featureAndPhotos[i].data = resp.data.object;
              $scope.auction.featureAndPhotos[i].isLoadin = false;

            }
          }


          if (!$scope.auction.featureAndPhotos) {
            $scope.auction.featureAndPhotos = [];
          }
        }else if (resp.data.errorCode===45||resp.data.errorCode===34) {
             $localStorage.token = 0;
           $window.location.href = '#/login';
          
        }else{

        }





        }, function(resp) {
          $scope.progressPercentage = 0;
          for (var i = 0; i < $scope.auction.featureAndPhotos.length; i++) {
            if ($scope.auction.featureAndPhotos[i].id == id) {
              //splice i
              $scope.auction.featureAndPhotos.splice(i, 1);

            }
          }

          console.log('Error status: ' + resp.status);


        }, function(evt) {
          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;
          if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
            $scope.filecmplt = false;
          } else if ($scope.progressPercentage == 100) {
            $scope.filecmplt = true;

          } else {
            $scope.filenotUplaod = true;
          }
        });
      }
    };

}

// get auctions

        if ($state.current.name === 'admin.auctionmanagement') {
          if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }


          $scope.auctionmanage=function(auction){
             loadingService.isLoadin=true;
            console.log(auction);
            $window.location.href='#/admin/addauction/'+auction.auctionId;


            if (auction.auctionId) {
              // alert(category.categoryId);

            $localStorage.auction=auction;
             loadingService.isLoadin=false;
  //


}
};


          


              $scope.getallauction = function() {
                loadingService.isLoadin=true;
                 $scope.testdata= false;
                $scope.auctionPageVo=
                

                     {
                             "rowPerPage":100,
                             "currentIndex":0,
                             "customerId":0,
                             "fromDate":0,
                             "toDate":0,
                             "auctionId":0,
                             "auctionDetaiilsId":0,
                             "status":[],
                             "cityId":0,
                             "countryId":0
                             
                     }

                auctionService.getallauctions($scope.auctionPageVo).then(function(response) {
                  loadingService.isLoadin=false;
                    console.log(response);

                    //  if (response.errorCode === 0) {
                    //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
                    //       $scope.stores = response.object.Store;
                    // console.log($scope.stores );
                   
                    if (response.errorCode === 0) {
                       // $scope.shopList = $scope.shopList.concat(response.object.Product);

                         $scope.auctions = response.object;

                    } else {
                      auctions=[];
                        alert(response.errorMessage);
                    }
                });
            }
            $scope.getallauction();

        }

        // get auction items 

        if ($state.current.name === 'admin.auction') {

          if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

          


              $scope.getallauction = function() {

                loadingService.isLoadin=true;
                $scope.auctionPageVo=
                

                     {
                             "rowPerPage":100,
                             "currentIndex":0,
                             "customerId":0,
                             "fromDate":0,
                             "toDate":0,
                             "auctionId":0,
                             "auctionDetaiilsId":0,
                             "status":[2],
                             "cityId":0,
                             "countryId":0
                             
                     }

                auctionService.getallauctions($scope.auctionPageVo).then(function(response) {
                  
                  loadingService.isLoadin=false;
                    console.log(response);

                    //  if (response.errorCode === 0) {
                    //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
                    //       $scope.stores = response.object.Store;
                    // console.log($scope.stores );
                   
                    if (response.errorCode === 0) {
                       // $scope.shopList = $scope.shopList.concat(response.object.Product);

                         $scope.auctions = response.object;

                    } else {
                        alert(response.errorMessage);
                    }
                });
            }
            $scope.getallauction();

        }

        //viewauction(auction)
        $scope.viewauctiondetails=function(auction){
          console.log(auction);
          $window.location.href='#/admin/auctionitemdetails/'+auction.auctionId;


          if (auction.auctionId) {
            // alert(auction.auctionId);

          $localStorage.auction=auction;
          //


        }
        };

        // auction item details

        if ($state.current.name === 'admin.auctionitemdetails') {

           if (!$localStorage.token||$localStorage.token===0) {
    $window.location.href ='#/login';
    }

          

              $scope.getallauctiondetails = function(auctionId) {
                loadingService.isLoadin=true;
                $scope.auctiondetailsPageVo=
                
                     {
                     "auctionId":auctionId,
                     "auctionDetaiilsId":0,
                     "customerId":0,
                     "formDate":0,
                     "toDate":0,
                     "rowPerPage":3,
                     "currentIndex":0,
                     "status":[1]

                    }

                auctionService.getallauctiondetails($scope.auctiondetailsPageVo,auctionId).then(function(response) {
                  loadingService.isLoadin=false;
                    console.log(response);

                    //  if (response.errorCode === 0) {
                    //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
                    //       $scope.stores = response.object.Store;
                    // console.log($scope.stores );
                   
                    if (response.errorCode === 0) {
                       // $scope.shopList = $scope.shopList.concat(response.object.Product);

                         $scope.auctiondetails = response.object;

                    } else {
                        alert(response.errorMessage);
                    }
                });
            }
            $scope.getallauctiondetails();

        }






// view auction page

//view category

if ($state.current.name === 'admin.auctionitemdetails') {
  if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.auction = {};
       // $scope. category.imageName ="nothing";
         if ($stateParams.auctionId!=='0') {
         $scope.auction=$localStorage.auction;
     }
   }
// get country


$scope.getcountry = function(country) {
  loadingService.isLoadin=true;
      storeService.getcountries(country).then(function(response) {
        loadingService.isLoadin=false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.countries = response.object;
        }
      });
    };
$scope.getcountry();


// auction search
$scope.auctionSearch= function(search){
  loadingService.isLoadin=true;


  console.log(search);
  if (search.fromDate) {
  $scope.fromDate = new Date(search.fromDate).getTime();
}
  console.log($scope.fromDate);
  $scope.toDate = new Date(search.toDate).getTime();
  console.log($scope.toDate);
  console.log(search);
  $scope.auctionPageVo=
                
                    {
                              "rowPerPage":100,
                             "currentIndex":0,
                             "customerId":0,
                             "fromDate":$scope.fromDate,
                             "toDate":$scope.toDate,
                             "auctionId":0,
                             "auctionDetaiilsId":0,
                             "status":[1],
                             "cityId":search.cityId,
                             "countryId":search.countryId
                                                   };
                                                   // alert("hiii");

                auctionService.getallauctionsbysearch($scope.auctionPageVo).then(function(response) {
                  loadingService.isLoadin=false;
                    console.log(response);

                    // if (response.errorCode === -3){
                    //  $scope.testdata= true;
                    //     $scope.auctions=[];
                    //    // alert()


                    // }else{
                    //   $scope.testdata= false;
                    // }

                    if (response.errorCode === 0) {
                       // $scope.shopList = $scope.shopList.concat(response.object.Product);

                         $scope.auctions = response.object;

                    } else {
                        $scope.auctions=[];
                        alert(response.errorMessage);
                    }
                },function(response){

                    $scope.auctions=[];



                });
            }
            
   $scope.changeCountry=function(countryId){
  console.log(countryId);

    
     $scope.getcityByCountryId(countryId)

   };
$scope.getcityByCountryId = function(countryId) {

  $scope.cityPageVo=
                {
                "countryId":countryId,
                "cityId":0,
                "rowPerPage":100,
                "currentIndex":0,
                "cityName":"",
                "status":[]
              }

      storeService.getcitiesByCountryId($scope.cityPageVo,countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
 $scope.getcityByCountryId();




$scope.getcountry = function(country) {
  loadingService.isLoadin=true;
      storeService.getcountries(country).then(function(response) {
        loadingService.isLoadin=false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.countries = response.object;
        }
      });
    };
$scope.getcountry();

//function for country change
 $scope.changeCountry=function(countryId){
  console.log(countryId);

    
     $scope.getcityByCountryId(countryId)

   };
$scope.getcityByCountryId = function(countryId) {

  $scope.cityPageVo=
                {
                "countryId":countryId,
                "cityId":0,
                "rowPerPage":100,
                "currentIndex":0,
                "cityName":"",
                "status":[]
              }

      storeService.getcitiesByCountryId($scope.cityPageVo,countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
 $scope.getcityByCountryId();

  

  // add auction


    if ($state.current.name === 'admin.addauction') {
      if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
     
       // $scope. category.imageName ="nothing";
         if ($stateParams.auctionId!=='0') {
         $scope.auction=$localStorage.auction;
     }

     $scope.deleteimage=function(name){
        if ($scope.auction.featureAndPhotos.length<=1) {
          $scope.auction.featureAndPhotos.length=[];

        }else{
          for (var i =0; i <  $scope.auction.featureAndPhotos.length ; i++) {
            if($scope.auction.featureAndPhotos[i].data==name){
              $scope.auction.featureOrPhotos.splice(i,1);

            }
          }
        }
            //$scope.product.featureOrPhotos[i].data

      }
        

      
      //loadingService.isLoadin = true;

      $scope.addauction=function(getAddAuctionDetails){
        if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
        console.log(getAddAuctionDetails);
         getAddAuctionDetails.auctionDate = new Date(getAddAuctionDetails.auctionDate).getTime();
          getAddAuctionDetails.auctionExpiryTime = new Date(getAddAuctionDetails.auctionExpiryTime).getTime();
          console.log( getAddAuctionDetails.auctionDate);
           console.log(getAddAuctionDetails.auctionExpiryTime);
        loadingService.isLoadin=true;

      console.log(getAddAuctionDetails);

                

                auctionService.submitAddAuctionDetails(getAddAuctionDetails).then(function(response) {
                  loadingService.isLoadin=false;
                    console.log(response);
                   
                        if (response.data.errorCode === 0) {

                          if ($stateParams.auctionId =='0') {
                          // toastrMsg(1,'Added new Category!','Successfull..!');
                           // $scope.pop = function(){
                              toaster.pop('success', "Successfull..!", "Added new Auction!");
                              // toaster.pop('success', "Added new Category!", "Successfull..!");
                               // };
                             } else{
                               toaster.pop('success', "Successfull..!", "Updated  Auction!");

                             }
                          // toaster.pop('success', "Successfull..!", "Added New Auction!");
                           
                           // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
                           // console.log($scope.allProducts);
                            $window.location.href = '#/admin/auctionmanagement';
                        } else  if (response.data.errorCode === 45||response.data.errorCode === 34) {
                        //  console.log( );
                 $window.location.href = '#/login';

                        }else{
                          toaster.pop('error', "Oops..!", response.data.errorMessage);
                          // alert(response.data.errorMessage);
                        }

                });
         

      /*/fd*/
    }


     }
     $scope.viewauction=function(auction){
   loadingService.isLoadin=true;
  console.log(auction);
  $window.location.href='#/admin/viewauction/'+auction.auctionId;


  if (auction.auctionId) {
    // alert(category.categoryId);

  $localStorage.auction=auctionId;
   loadingService.isLoadin=false;
  //


}
};

//view Auction
if ($state.current.name === 'admin.viewauction') {

  if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
      $scope.auction = {};
       // $scope. auction.imageName ="nothing";
         if ($stateParams.auctionId!=='0') {
         $scope.auction=$localStorage.auction;
     }
        

      
    

     }




  });





