angular.module('starter.orderCtrl', [])
  .controller('orderCtrl', function($scope, $window, $state, inputService, toaster, sellerService, FILE_CONSTANT, toaster, MAIN_CONSTANT, Upload, sellerService, $stateParams, $localStorage, loadingService) {

    if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }

     //#########################################################

    $scope.maxSize = 5;     // Limit number for pagination display number.  
    $scope.totalCount = 0;  // Total number of items in all pages. initialize as a zero  
    $scope.pageIndex = 1;   // Current page number. First page is 1.-->  
    $scope.pageSizeSelected = 10; // Maximum number of items per page. 

         //#########################################################

    $scope.OrderPageContent = {
      "orderId": "رقم الأمر"
    }
    $window.scrollTo(0, 0);
    $scope.imageUrl = FILE_CONSTANT.filepath;
    $scope.sellerId = $localStorage.sellerId;
    console.log($scope.sellerId);

    // start new order page

    if ($state.current.name === 'admin.returns') {
      // if( $localStorage.roleId !== 50){
      //     $window.location.href ='#/login'

      //   }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      // alert("hai");
      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallproductreturns = function(pageno) {
        $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.orderreturnPageVo = {
          "returnItemId": 0,
          "rowPerPage": 25,
          "currentIndex": 0,
          "stat": 10,
          "storeId": 0
        }

        // {
        //    "rowPerPage":25,
        //    "currentIndex":0,
        //    "customerId":0,
        //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
        //    "storeId":$localStorage.currentloginstoreId,
        //   "deliveryDate":"0","serviceOrProduct":"",
        //    "deliveryBoyId":0,"orderListId":"0"
        //  }
        console.log($scope.orderreturnPageVo);
        $scope.orderreturnPageVo.currentIndex = (pageno - 1) * $scope.orderreturnPageVo.rowPerPage;
        loadingService.isLoadin = true;

        sellerService.getallreturns($scope.orderreturnPageVo).then(function(response) {
          loadingService.isLoadin = false;

         
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

        $scope.noPages = Math.ceil(response.totalRecords / $scope.orderreturnPageVo.rowPerPage);
       //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.allreturnorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.allreturnorders);
            $scope.noorderstoship = 0;


          } else if (response.errorCode === 2) {
            $scope.allreturnorders = [];
            loadingService.isLoadin = false;
            $scope.noorderstoship = 1;
             $scope.pageList=[];
             $scope.noPages = 0;




          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            // alert("helllo");
            alert(response.errorMessage);
          }
        });
      }
      $scope.getallproductreturns($scope.pageno);

      // confirmed returns
      $scope.getallconfirmedreturns = function(pageno) {
         $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.orderreturnconfirmPageVo = {
          "returnItemId": 0,
          "rowPerPage": 25,
          "currentIndex": 0,
          "stat": 11,
          "storeId": 0
        }

        // {
        //    "rowPerPage":25,
        //    "currentIndex":0,
        //    "customerId":0,
        //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
        //    "storeId":$localStorage.currentloginstoreId,
        //   "deliveryDate":"0","serviceOrProduct":"",
        //    "deliveryBoyId":0,"orderListId":"0"
        //  }
        console.log($scope.orderreturnconfirmPageVo);
        $scope.orderreturnconfirmPageVo.currentIndex = (pageno - 1) * $scope.orderreturnconfirmPageVo.rowPerPage;
        loadingService.isLoadin = true;

        sellerService.getallreturns($scope.orderreturnconfirmPageVo).then(function(response) {
          loadingService.isLoadin = false;

          
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.noPages = Math.ceil(response.totalRecords / $scope.orderreturnconfirmPageVo.rowPerPage);

         //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.allacceptedreturs = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.allacceptedreturs);
            $scope.noallacceptedreturs = 0;


          } else if (response.errorCode === 2) {
            $scope.allacceptedreturs = [];
            loadingService.isLoadin = false;
            $scope.noallacceptedreturs = 1;
            $scope.noPages = 0;
            $scope.pageList=[];




          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            // alert("helllo");
            alert(response.errorMessage);
          }
        });
      }
      // Rejected returns
      $scope.getallrejectedreturns = function(pageno) {
         $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.orderreturnrejectedPageVo = {
          "returnItemId": 0,
          "rowPerPage": 25,
          "currentIndex": 0,
          "stat": 12,
          "storeId": $localStorage.currentloginstoreId
        }

        // {
        //    "rowPerPage":25,
        //    "currentIndex":0,
        //    "customerId":0,
        //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
        //    "storeId":$localStorage.currentloginstoreId,
        //   "deliveryDate":"0","serviceOrProduct":"",
        //    "deliveryBoyId":0,"orderListId":"0"
        //  }
        console.log($scope.orderreturnrejectedPageVo);
        $scope.orderreturnrejectedPageVo.currentIndex = (pageno - 1) * $scope.orderreturnrejectedPageVo.rowPerPage;
        loadingService.isLoadin = true;

        sellerService.getallreturns($scope.orderreturnrejectedPageVo).then(function(response) {
          loadingService.isLoadin = false;

         
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
             $scope.noPages = Math.ceil(response.totalRecords / $scope.orderreturnrejectedPageVo.rowPerPage);
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

              //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.allrejectedreturs = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.allrejectedreturs);
            $scope.noallallrejectedreturs = 0;


          } else if (response.errorCode === 2) {
             $scope.noPages = 0;
             $scope.pageList=[];
            loadingService.isLoadin = false;
            $scope.noallallrejectedreturs = 1;




          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            // alert("helllo");
            alert(response.errorMessage);
          }
        });
      }

      // COMPLETED RETURNS

      $scope.getallcompletedreturns = function(pageno) {
         $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.orderreturncompletedPageVo = {
          "returnItemId": 0,
          "rowPerPage": 25,
          "currentIndex": 0,
          "stat": 15,
          "storeId": $localStorage.currentloginstoreId
        }

        // {
        //    "rowPerPage":25,
        //    "currentIndex":0,
        //    "customerId":0,
        //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
        //    "storeId":$localStorage.currentloginstoreId,
        //   "deliveryDate":"0","serviceOrProduct":"",
        //    "deliveryBoyId":0,"orderListId":"0"
        //  }
        console.log($scope.orderreturncompletedPageVo);
        $scope.orderreturncompletedPageVo.currentIndex = (pageno - 1) * $scope.orderreturncompletedPageVo.rowPerPage;
        loadingService.isLoadin = true;

        sellerService.getallreturns($scope.orderreturncompletedPageVo).then(function(response) {
          loadingService.isLoadin = false;        
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
              $scope.noPages = Math.ceil(response.totalRecords / $scope.orderreturncompletedPageVo.rowPerPage);

                //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.allcompletedreturs = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.allcompletedreturs);
            $scope.noallcompletedreturs = 0;


          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === 2) {
            loadingService.isLoadin = false;
            $scope.noallcompletedreturs = 1;
             $scope.noPages =0;
             $scope.pageList=[];




          } else {
            // alert("helllo");
            alert(response.errorMessage);
          }
        });
      }
      //        $scope.getallconfirmedreturns= function(pageno) {
      //          $scope.confirmedreturnPageVo ={
      // "returnItemId":0,"rowPerPage":25,"currentIndex":0,"storeId":$localStorage.currentloginstoreId
      // }

      //       // {
      //       //    "rowPerPage":25,
      //       //    "currentIndex":0,
      //       //    "customerId":0,
      //       //    "fromDate":0,"toDate":0,"orderId":0,"status":[2],
      //       //    "storeId":$localStorage.currentloginstoreId,
      //       //   "deliveryDate":"0","serviceOrProduct":"",
      //       //    "deliveryBoyId":0,"orderListId":"0"
      //       //  }
      // $scope.confirmedreturnPageVo.currentIndex = (pageno - 1) * $scope.confirmedreturnPageVo.rowPerPage;
      //          loadingService.isLoadin=true;

      //         sellerService.getallreturns($scope.confirmedreturnPageVo).then(function(response) {
      //            loadingService.isLoadin=false;

      //               $scope.noPages = Math.ceil(response.totalRecords / $scope.confirmedreturnPageVo.rowPerPage);
      //           console.log(response);

      //           //  if (response.errorCode === 0) {
      //           //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
      //           //       $scope.stores = response.object.Store;
      //           // console.log($scope.stores );

      //           if (response.errorCode === 0) {
      //             // $scope.shopList = $scope.shopList.concat(response.object.Product);

      //             $scope.confirmedreturnorders = response.object;
      //              // $scope.orderdetails = response.object;
      //              // [0].orderDetailsVo;
      //              console.log($scope.confirmedreturnorders);
      //              $scope.noorderstoship=0;


      //           } else if (response.errorCode === -3){
      //              loadingService.isLoadin=false;
      //               $scope.noorderstoship=1;




      //           }else {
      //             // alert("helllo");
      //             alert(response.errorMessage);
      //           }
      //         });
      //       }



    }
    //  if ($state.current.name === 'seller.orderstoshipping') {
    //   if (!$localStorage.token || $localStorage.token === 0) {
    //       $window.location.href = '#/login';
    //     }

    // // alert("hai");
    //      $scope.noPages = 1;
    //       $scope.getNumber = function(num) {
    //         return new Array(num);
    //       }


    //       $scope.pageno = 1;

    //       $scope.getallorderstoshipping = function(pageno) {
    //          $scope.orderPageVo =

    //       {
    //          "rowPerPage":25,
    //          "currentIndex":0,
    //          "customerId":0,
    //          "fromDate":0,"toDate":0,"orderId":0,"status":[2],
    //          "storeId":$localStorage.currentloginstoreId,
    //         "deliveryDate":"0","serviceOrProduct":"",
    //          "deliveryBoyId":0,"orderListId":"0"
    //        }
    // $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
    //          loadingService.isLoadin=true;

    //         sellerService.getallorders($scope.orderPageVo).then(function(response) {
    //            loadingService.isLoadin=false;

    //               $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
    //           console.log(response);

    //           //  if (response.errorCode === 0) {
    //           //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
    //           //       $scope.stores = response.object.Store;
    //           // console.log($scope.stores );

    //           if (response.errorCode === 0) {
    //             // $scope.shopList = $scope.shopList.concat(response.object.Product);

    //             $scope.orderstoship = response.object;
    //              // $scope.orderdetails = response.object;
    //              // [0].orderDetailsVo;
    //              console.log($scope.orderstoship);
    //              $scope.noorderstoship=0;


    //           } else if (response.errorCode === -3){
    //              loadingService.isLoadin=false;
    //               $scope.noorderstoship=1;




    //           }else {
    //             // alert("helllo");
    //             alert(response.errorMessage);
    //           }
    //         });
    //       }
    //       $scope.getallorderstoshipping($scope.pageno);


    //   }
    if ($state.current.name === 'admin.neworder') {
    //    $scope.gap = 4;

    // $scope.filteredItems = [];
    // $scope.groupedItems = [];
    // $scope.itemsPerPage = 5;
    // $scope.pagedItems = [];
    // $scope.currentPage = 0;

    //      $scope.range = function (size, start, end) {
    //     var ret = [];
    //     console.log(size, start, end);

    //     if (size < end) {
    //         end = size;
    //         start = size - $scope.gap;
    //     }
    //     for (var i = start; i < end; i++) {
    //         ret.push(i);
    //     }
    //     console.log(ret);
    //     return ret;
    // };

    // $scope.prevPage = function () {
    //     if ($scope.currentPage > 0) {
    //         $scope.currentPage--;
    //         var currentPageNum=$scope.currentPage+1; 
    //         console.log(currentPageNum);
    //         $scope.getallorders(currentPageNum);
    //     }
    // };

    // $scope.nextPage = function () {
    //     if ($scope.currentPage < $scope.pagedItems.length - 1) {
    //         $scope.currentPage++;
    //         var currentPageNum=$scope.currentPage+1; 
    //         console.log(currentPageNum);
    //         $scope.getallorders(currentPageNum);
    //     }
    // };

    // $scope.setPage = function () {
    //     $scope.currentPage = this.n;
    //     var currentPageNum=$scope.currentPage+1; 
    //     console.log(currentPageNum);
    //      $scope.getallorders(currentPageNum);
    // };
      // if( $localStorage.roleId !== 50){
      //     $window.location.href ='#/login'

      //   }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallorders = function(pageno) {
            $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.orderPageVo =

          {
            "rowPerPage": 30,
            "currentIndex":0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [1],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }
        console.log($scope.orderPageVo);
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        // loadingService.isLoadin=true;
        $scope.loading = 1;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;

       
          console.log(response);
         $scope.totalCount=response.totalRecords;
          //$scope.pageSizeSelected=10;
         console.log($scope.totalCount+"totalRecords Count")


          if (response.errorCode === 0) {
              $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
       //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.neworders = response.object;
           //  $scope.pagedItems = response.object;
            console.log($scope.neworders);
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            // console.log($scope.orderdetails);
            $scope.nodata = 0;


          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
             $scope.noPages=0;
             $scope.pageList=[];
               $scope.neworders = [];
            $scope.loading = 0;
            // loadingService.isLoadin=false;
            $scope.nodata = 1;




          } else {
            // alert("helllo");
            alert(response.errorMessage);
          }
        });
      }
      $scope.getallorders($scope.pageno);

      $scope.pageChanged = function () {  
        $scope.getallorders();  
    }; 

      // confirmed orders

      $scope.getallconfirmedorders = function(pageno) {
             $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.orderPageVo =

          {
            "rowPerPage": 30,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [2],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;
         // $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);
          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
           //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.confirmedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.confirmedorders);
            $scope.noconfirmeddata = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.noconfirmeddata = 1;
            $scope.confirmedorders = [];
             $scope.pageList=[];
             $scope.noPages=0;




          } else {
            alert(response.errorMessage);
          }
        });
      }
      // $scope.getallconfirmedorders($scope.pageno);

      

      $scope.getallcancelledorders = function(pageno) {
              $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.orderPageVo =

          {
            "rowPerPage": 30,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [5],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          //$scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);    

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
             $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
           //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.cancelledorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.cancelledorders);
            $scope.nocancelleddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.nocancelleddata = 1;
             $scope.pageList=[];
              $scope.noPages=0;
          } else {
            alert(response.errorMessage);
          }
        });
      }


      $scope.getallrejectedorders = function(pageno) {
        $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.orderPageVo =

          {
            "rowPerPage": 30,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [3],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
         // $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);
          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
           //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.rejectedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.rejectedorders);
            $scope.norejecteddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
             $scope.pageList=[];
            $scope.loading = 0;
            $scope.norejecteddata = 1;
             $scope.noPages = 0;
          } else {
            alert(response.errorMessage);
          }
        });
      }
      // $scope.getallrejectedorders($scope.pageno);

      $scope.getallshippedorders = function(pageno) {
        $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.shippedorderPageVo =

          {
            "rowPerPage": 30,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [10],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.shippedorderPageVo.currentIndex = (pageno - 1) * $scope.shippedorderPageVo.rowPerPage;
        sellerService.getallorders($scope.shippedorderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          // $scope.noPages = Math.ceil(response.totalRecords / $scope.shippedorderPageVo.rowPerPage);
          console.log(response);
          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
             $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
           //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.shippedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.shippedorders);
            $scope.noshippedordersdata = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.noshippedordersdata = 1;
             $scope.pageList=[];
              $scope.noPages = 0;




          } else {
            alert(response.errorMessage);
          }
        });
      }



      $scope.getalldeliveredorders = function(pageno) {
          $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.orderPageVo =

          {
            "rowPerPage": 30,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [4],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          // $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);
          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
           //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.deliveredorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.deliveredorders);
            $scope.nodelivereddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.nodelivereddata = 1;
            console.log($scope.nodelivereddata);
            $scope.pageList=[];
             $scope.noPages = 0;
          } else {
            alert(response.errorMessage);
          }
        });
      }

      $scope.getallpickedorders = function(pageno) {
         $scope.currentPageNumber=pageno;
        console.log($scope.currentPageNumber);
        $scope.pickedorderPageVo =

          {
            "rowPerPage": 30,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": 0,
            "status": [6],
            "storeId": $localStorage.currentloginstoreId,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.pickedorderPageVo.currentIndex = (pageno - 1) * $scope.pickedorderPageVo.rowPerPage;
        sellerService.getallorders($scope.pickedorderPageVo).then(function(response) {
          // $scope.noPages = Math.ceil(response.totalRecords / $scope.pickedorderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);
          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
           //-------- *** PAGINATION -- START *** -----------
          if ( $scope.noPages>0) {             
                    $scope.pageList=[];                 
                     // $scope.itemPagination = response.data.object.pagination;
                     $scope.totalPage = $scope.noPages;
                     console.log( $scope.totalPage);
                  
                     if($scope.currentPageNumber+4> $scope.totalPage){
                        $scope.lastIndex= $scope.totalPage ;

                     }else{
                         $scope.firstIndex=$scope.currentPageNumber;
                           //$scope.lastIndex=$scope.pagevo.pageNo+4;
                           $scope.lastIndex=parseInt($scope.currentPageNumber)+4;
                     }
                        if ($scope.currentPageNumber-4<1) {
                        $scope.firstIndex=1;
                       // $scope.lastIndex=10;
                     }else{
                          //$scope.firstIndex=$scope.pagevo.pageNo-4;
                          $scope.firstIndex=$scope.lastIndex-4;
                     }
                     for (var i = $scope.firstIndex; i <= $scope.lastIndex; i++) {
                       $scope.pageList.push(i);                       
                     }                  
                     console.log($scope.pageList);
                    

          }else{
             $scope.pageList=[];
          }
           //-------- *** PAGINATION -- END *** -----------

            $scope.pickedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.pickedorders);
            $scope.nopickeddata = 0;
          } else if (response.errorCode === -3) {
            $scope.pickedorders = [];
            $scope.nopickeddata = 1;
            $scope.pageList=[];
             $scope.noPages = 0;
               $scope.loading = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            alert(response.errorMessage);
          }
        });
      }
      // $scope.getalldeliveredorders($scope.pageno);




    }



    //end new order page

    if ($state.current.name === 'admin.order') {
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      $scope.orderPageVo = {
        "rowPerPage": 100,
        "currentIndex": 0,
        "customerId": 0,
        "fromDate": 0,
        "toDate": 0,
        "orderId": 0,
        "status": [1],
        "sellerId": 0,
        "deliveryDate": "0",
        "serviceOrProduct": "0",
        "orderListId": "0"
      }

      $scope.getallorders = function() {
        loadingService.isLoadin = true;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.orders = response.object;
            $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.orderdetails);

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            alert(response.errorMessage);
          }
        });
      }
      $scope.getallorders();



    }
    // ########################

    if ($state.current.name === 'admin.deliveredorders') {
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      $scope.orderPageVo = {
        "rowPerPage": 9,
        "currentIndex": 0,
        "customerId": 0,
        "fromDate": 0,
        "toDate": 0,
        "orderId": 0,
        "status": [1],
        "sellerId": 0,
        "deliveryDate": "0",
        "serviceOrProduct": "0",
        "orderListId": "0"
      }

      $scope.getallorders = function() {
        loadingService.isLoadin = true;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.orders = response.object;
            $scope.orderdetails = response.object[0].orderDetailsVo;
            console.log($scope.orderdetails);

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            alert(response.errorMessage);
          }
        });
      }
      $scope.getallorders();



    }
    // ##################
    $scope.pop = function() {
      toaster.pop('info', "title", "text");
    };


    $scope.store = {};
    $scope.store.featureOrPhotos = [];

    $scope.upload = function(file) {

      //$scope.isFileuploadStart=true;
      $scope.storelogUploading = true;


      if (file) {

        Upload.upload({
          url: MAIN_CONSTANT.site_url + "/api/pentalupload",
          headers: {
            "Content-Type": 'multipart/*',
            Bearer: $localStorage.token
          },
          /*data: { file: FILE_CONSTANT.site_url + file.name,
            type: 'image/jpeg' }*/
          data: {
            file: file
          }
        }).then(function(resp) {
          $scope.store.imageName = resp.data.object;
          $scope.storelogUploading = false;

          console.log(resp);




        }, function(resp) {
          $scope.progressPercentage = 0;


        }, function(evt) {

          $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
          console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
          // $scope.file = null;

        });
      }
    };






    // get store statistics*/
    // ViewOrderdetails

    $scope.ViewOrderdetails = function(order) {
      loadingService.isLoadin = true;
      console.log(order);
      console.log(order.orderId);

      $window.location.href = '#/seller/invoice/' + order.orderId;
      $localStorage.order = order;
      loadingService.isLoadin = false;

      // $scope.getallordersbyuserId(customer.userId);


    };
    if ($state.current.name === 'seller.invoice') {
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      console.log($stateParams.orderId);
      //alert($stateParams.orderId);
      $scope.Id = $stateParams.orderId;
      console.log($scope.Id);

      //     $scope.orderPageVo = {
      //       "orderId":$scope.Id,
      //       "orderDetailsId":"0",
      //       "customerId":0,
      //       "fromDate":0,
      //       "toDate":0,"sellerId":0,
      //       "currentIndex":0,
      //       "rowPerPage":20
      // }
      $scope.orderPageVo = {
        "rowPerPage": 100,
        "currentIndex": 0,
        "customerId": 0,
        "fromDate": 0,
        "toDate": 0,
        "orderId": 0,
        "status": [1],
        "sellerId": 0,
        "deliveryDate": "0",
        "serviceOrProduct": "0",
        "orderListId": "0"
      }

      $scope.getallorders = function() {

        loadingService.isLoadin = true;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.orders = response.object;
            $scope.orderdetails = response.object[0];
            $scope.transaction = response.object[0].transaction;
            var customerId = response.object[0].customerId;
            var sellerIdId = response.object[0].sellerId;
            console.log($scope.orderdetails);
            console.log(customerId);
            console.log($scope.transaction);
            $scope.getallcustomers(customerId);
            $scope.getseller(sellerIdId);

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            alert(response.errorMessage);
          }
        });
        // console.log($scope.customerId);

      }
      $scope.getallorders();

      // $scope.getcustomer = function() {





      // }
      //  $scope.getcustomer();

      //  ##########


      //get seller details


      // alert(33333333);
      $scope.getseller = function(sellerIdId) {
        $scope.sellerPageVo = {
          "sellerId": sellerIdId,
          "sellerName": "",
          "phoneNumber": "",
          "address": "",
          "status": [0],
          "currentIndex": 0,
          "rowPerPage": 100
        }
        loadingService.isLoadin = true;

        sellerService.getallsellers($scope.sellerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.sellers = response.object[0];
            console.log($scope.sellers);



          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      }
      // $scope.getseller();




      //end get seller details



      // alert(33333333);
      $scope.getallcustomers = function(customerId) {
        console.log(customerId);
        $scope.customerPageVo = {
          "customerId": 0,
          "cityId": 0,
          "countryId": 0,
          "userId": customerId,
          "orderBy": 0,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": 100,
          "status": 2
        }
        console.log($scope.customerPageVo);

        loadingService.isLoadin = true;

        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.customers = response.object;
            console.log($scope.customers);



          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      };
      $scope.getallcustomers();





    }






    if ($state.current.name === 'seller.listofstoreorder') {
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      $scope.pageno = 1;
      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        console.log(num);
        return new Array(num);
      }
      // alert('323');

      console.log($stateParams.storeId);
      $scope.Id = $stateParams.storeId;
      console.log($scope.Id);
      $scope.orderPageVo = {
        "rowPerPage": 15,
        "currentIndex": 0,
        "customerId": 0,
        "fromDate": 0,
        "toDate": 0,
        "orderId": 0,
        "status": [2, 4, 6],
        "storeId": $scope.Id

      }

      $scope.getallordersbystoreId = function(pageno) {
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        console.log($stateParams.storeId);




        console.log($scope.orderPageVo);
        sellerService.getallordersbystoreId($scope.orderPageVo).then(function(response) {
          loadingService.isLoadin = false;
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);
          if (response.errorCode === 0) {
            $scope.ordersbystoreId = response.object;
            console.log($scope.ordersbystoreId);
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          };
        }, function() {
          loadingService.isLoadin = false;
        });
      };
      $scope.getallordersbystoreId($scope.pageno);

    }





    $scope.changeorderstatus = function(order, changeId) {
      // alert(order);
      loadingService.isLoadin = true;
      console.log(order, changeId);
      $scope.orderId = order.orderId;
      console.log($scope.orderId);
      // $window.location.href = '#/admin/milestoneorder/' + order.orderId;


      // if (order.orderId) {
      //   // alert(country.countryId);

      //   $localStorage.order = order;
      //   //


      // }
      // alert("near55");
      // console.log(storeService);
      sellerService.changeorderstatus($scope.orderId, changeId).then(function(response) {
        // alert("reached1");
        loadingService.isLoadin = false;
        // $scope.noPages=Math.ceil(response.totalRecords/$scope.orderPageVo.rowPerPage);
        console.log(response);
        if (response.errorCode === 0) {
          $scope.ordersbystoreId = response.object;
          console.log($scope.ordersbystoreId);
        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        };
      }, function() {
        loadingService.isLoadin = false;
      });
    };

    // if ($state.current.name === 'admin.milestoneorder') {
    //   if (!$localStorage.token || $localStorage.token === 0) {
    //       $window.location.href = '#/login';
    //     }


    //      $scope.order = {};
    //       // $scope.customer.password ="password123";
    //       // $scope. category.imageName ="nothing";


    //       // if ($stateParams.orderId !== '0') {
    //         $scope.order = $localStorage.order;
    //         console.log( $scope.order);


    //       // }




    // // alert("haaaaaaaai");





    //   }

    // $scope.ordermanage = function(order) {
    //     loadingService.isLoadin=true;
    //    console.log(order);
    //    $window.location.href = '#/seller/vieworder/' + order.orderId;


    //    if (order.orderId) {
    //      // alert(country.countryId);

    //      $localStorage.order = order;
    //      //


    //    }
    //  };
    $scope.vieworderdetails = function(order) {
      // alert("oh");
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/admin/vieworder/' + order.orderId;


      if (order.orderId) {
        // alert(country.countryId);

        $localStorage.order = order;
        //


      }
    };

    $scope.vieworderdetailsbyseller = function(order) {
      // alert("oh");
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/admin/vieworder/' + order.orderId;


      if (order.orderId) {
        // alert(country.countryId);

        $localStorage.order = order;
        //


      }
    };

    // ##########################

    //       $scope.getallorderdetails = function(listid) {
    //     alert("hhhiisd")
    //     console.log(listid);
    //     $scope.order = {};
    //     $scope.orderdetaPageVo =


    //       {
    //         "orderId": ordId,
    //         "orderDetailsId": listid,
    //         "customerId": 0,
    //         "fromDate": 0,
    //         "toDate": 0,
    //         "sellerId": 0,
    //         "currentIndex": 0,
    //         "rowPerPage": 20
    //       }
    //     console.log($scope.orderdetaPageVo);
    //     // $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
    //     loadingService.isLoadin = true;

    //     sellerService.getallorderdetails($scope.orderdetaPageVo).then(function(response) {
    //       loadingService.isLoadin = false;

    //       // $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
    //       console.log(response);

    //       //  if (response.errorCode === 0) {
    //       //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
    //       //       $scope.stores = response.object.Store;
    //       // console.log($scope.stores );

    //       if (response.errorCode === 0) {
    //         // $scope.shopList = $scope.shopList.concat(response.object.Product);

    //         $scope.order = response.object;
    //         console.log($scope.order);
    //         // $scope.orderdetails=
    //         $localStorage.order = $scope.order[0];
    //         console.log($localStorage.order);

    //         // $scope.orderdetails = response.object;
    //         // [0].orderDetailsVo;
    //         // console.log($scope.orderdetails);
    //         var orderId = $scope.order[0].orderId;
    //         console.log(orderId);
    //         $scope.nodata = 0;
    //         $window.location.href = '#/admin/viewreturn/' + orderId;


    //       } else if (response.errorCode === 45 || response.errorCode === 34) {
    //         $localStorage.token = 0;
    //         $window.location.href = '#/login';


    //       } else if (response.errorCode === -3) {
    //         loadingService.isLoadin = false;
    //         $scope.nodata = 1;




    //       } else {
    //         // alert("helllo");
    //         alert(response.errorMessage);
    //       }
    //     });
    //   }
    //   $scope.getallorderdetails(listid);

    // #####################
    $scope.viewreturnorderdetailsbyseller = function(ret) {
      // alert("haaaaaaaai");
      // alert("oh");
      // $scope.returnReason=$localStorage.returnReason;
      $localStorage.returnReason=ret.returnReason;
      loadingService.isLoadin = true;
      console.log(ret);
      var ordId = ret.orderId;
      console.log(ordId);
      // console.log();
      var listid = ret.orderListId;
      console.log(listid);
      var customerId = ret.customerId;
      console.log(customerId);
      $localStorage.customerId = customerId;


      

      $scope.getReturnOrderDetails= function() {
        //alert("hhhiisd")
        console.log(listid);
        $scope.order = {};
        $scope.returnOrderDetailsPageVo = {
        "orderId":ordId,
        "orderDetailsId":listid,
        "customerId": 0,
        "fromDate": 0,
        "toDate": 0,
        "sellerId": 0,
        "currentIndex": 0,
        "rowPerPage": 20
      }
        
        console.log($scope.returnOrderDetailsPageVo);      
        loadingService.isLoadin = true;
        sellerService.getallorderdetails($scope.returnOrderDetailsPageVo).then(function(response) {
          loadingService.isLoadin = false;         
          console.log(response);        

          if (response.errorCode === 0) {
            $scope.returnOrder = response.object[0];
            console.log($scope.returnOrder);
            var orderId =$scope.returnOrder.orderId;
            console.log(orderId);
             $scope.finalPrice=$scope.returnOrder.totalAmountIncludingOffer+$scope.returnOrder.shippingCharge;       
             console.log($scope.finalPrice);  
             $localStorage.returnOrder= $scope.returnOrder;

            // $scope.order = response.object;
            // console.log($scope.order);          
            // $localStorage.order = $scope.order[0];
            // console.log($localStorage.order);            
            // var orderId = $scope.order[0].orderId;
            // console.log(orderId);
            // $scope.nodata = 0;
            $window.location.href = '#/admin/viewreturn/' + orderId;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            loadingService.isLoadin = false;
            $scope.nodata = 1;




          } else {
            // alert("helllo");
            alert(response.errorMessage);
          }
        });
      }
      $scope.getReturnOrderDetails();

    }

      // $scope.getallorderbyorderlistId = function() {
      //   $scope.orderPageVo =

      //     {
      // //       "orderId": ordId,
      // // //       "orderDetailsId": listid,
      //       "rowPerPage": 25,
      //       "currentIndex": 0,
      //       "customerId": 0,
      //       "fromDate": 0,
      //       "toDate": 0,
      //       "orderId": ordId,
      //       "status": [1],
      //       "storeId": 0,
      //       "deliveryDate": "0",
      //       "serviceOrProduct": "",
      //       "deliveryBoyId": 0,
      //       "orderListId": listid
      //     }
      //   console.log($scope.orderPageVo);

      //   // loadingService.isLoadin=true;
      //   $scope.loading = 1;

      //   sellerService.getallorders($scope.orderPageVo).then(function(response) {
      //     // loadingService.isLoadin=false;
      //     $scope.loading = 0;


      //     console.log(response);

      //     if (response.errorCode === 0) {     
      //     alert("sss");  

      //       $scope.neworders = response.object;
      //       console.log($scope.neworders);
      //       $scope.nodata = 0;
      //     } else if (response.errorCode === 45 || response.errorCode === 34) {
      //       $localStorage.token = 0;
      //       $window.location.href = '#/login';
      //     } else if (response.errorCode === -3) {
      //       $scope.loading = 0;       
      //       $scope.neworders = [];
      //       $scope.nodata = 1;

      //     } else {        
      //       alert(response.errorMessage);
      //     }
      //   });
      // }
      // $scope.getallorderbyorderlistId();


      // $scope.getallorderdetails = function(listid) {
      //   alert("hhhiisd")
      //   console.log(listid);
      //   $scope.order = {};
      //   $scope.orderdetaPageVo =


      //     {
      //       "orderId": ordId,
      //       "orderDetailsId": listid,
      //       "customerId": 0,
      //       "fromDate": 0,
      //       "toDate": 0,
      //       "sellerId": 0,
      //       "currentIndex": 0,
      //       "rowPerPage": 20
      //     }
      //   console.log($scope.orderdetaPageVo);
      //   // $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
      //   loadingService.isLoadin = true;

      //   sellerService.getallorderdetails($scope.orderdetaPageVo).then(function(response) {
      //     loadingService.isLoadin = false;

      //     // $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
      //     console.log(response);

      //     //  if (response.errorCode === 0) {
      //     //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
      //     //       $scope.stores = response.object.Store;
      //     // console.log($scope.stores );

      //     if (response.errorCode === 0) {
      //       // $scope.shopList = $scope.shopList.concat(response.object.Product);

      //       $scope.order = response.object;
      //       console.log($scope.order);
      //       // $scope.orderdetails=
      //       $localStorage.order = $scope.order[0];
      //       console.log($localStorage.order);

      //       // $scope.orderdetails = response.object;
      //       // [0].orderDetailsVo;
      //       // console.log($scope.orderdetails);
      //       var orderId = $scope.order[0].orderId;
      //       console.log(orderId);
      //       $scope.nodata = 0;
      //       $window.location.href = '#/admin/viewreturn/' + orderId;


      //     } else if (response.errorCode === 45 || response.errorCode === 34) {
      //       $localStorage.token = 0;
      //       $window.location.href = '#/login';


      //     } else if (response.errorCode === -3) {
      //       loadingService.isLoadin = false;
      //       $scope.nodata = 1;




      //     } else {
      //       // alert("helllo");
      //       alert(response.errorMessage);
      //     }
      //   });
      // }
      // $scope.getallorderdetails(listid);


    //   $scope.getreturnorderdata(listid);

    //   // $window.location.href = '#/seller/vieworder' + ret.orderId;



    //   // if (ret.orderId) {
    //   //   // alert(country.countryId);

    //   //   $localStorage.retdetails = retdetails;
    //   //   //


    //   // }
    // };

    // $scope.getreturnorderdata = function() {

    //   $scope.returnorderdataPageVo =

    //     {
    //       "rowPerPage": 25,
    //       "currentIndex": 0,
    //       "customerId": 0,
    //       "fromDate": 0,
    //       "toDate": 0,
    //       "orderId": 0,
    //       "status": [1, 2, 3, 4, 5, 6],
    //       "storeId": $localStorage.currentloginstoreId,
    //       "deliveryDate": "0",
    //       "serviceOrProduct": "",
    //       "deliveryBoyId": 0,
    //       "orderListId": listid
    //     }


    //   // $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
    //   loadingService.isLoadin = true;

    //   sellerService.getallorders($scope.returnorderdataPageVo).then(function(response) {
    //     loadingService.isLoadin = false;

    //     // $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
    //     console.log(response);

    //     //  if (response.errorCode === 0) {
    //     //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
    //     //       $scope.stores = response.object.Store;
    //     // console.log($scope.stores );

    //     if (response.errorCode === 0) {
    //       // $scope.shopList = $scope.shopList.concat(response.object.Product);

    //       $scope.returnorderdetails = response.object;
    //       console.log($scope.returnorderdetails);
    //       // $scope.orderdetails = response.object;
    //       // [0].orderDetailsVo;
    //       // console.log($scope.orderdetails);
    //       $scope.nodata = 0;


    //     } else if (response.errorCode === 45 || response.errorCode === 34) {
    //       $localStorage.token = 0;
    //       $window.location.href = '#/login';


    //     } else if (response.errorCode === -3) {
    //       loadingService.isLoadin = false;
    //       $scope.nodata = 1;




    //     } else {
    //       // alert("helllo");
    //       alert(response.errorMessage);
    //     }
    //   });
    // }
    if ($state.current.name === 'seller.vieworder') {
      // alert("oh2");
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }


      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';

      }

      $scope.order = {};
      $scope.order = $localStorage.order;
      $scope.orderdetails = $scope.order.orderDetailsVo;
      console.log($scope.orderdetails);
      // if (order.orderId) {
      //   // alert(country.countryId);

      //   $scope.order= $localStorage.order;
      //   //


      // }
      console.log($scope.order);



      // alert(33333333);

      $scope.customerPageVo = {
        "customerId": 0,
        "cityId": 0,
        "countryId": 0,
        "userId": $scope.order.customerId,
        "fromDate": 0,
        "toDate": 0,
        "currentIndex": 0,
        "rowPerPage": "100",
        "status": 2,
        "customerName": "",
        "phoneNo": "",
        "emailId": ""

      }

      $scope.getallcustomers = function() {

        // alert("haai");
        loadingService.isLoadin = true;

        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.customer = response.object[0];
            console.log($scope.customer);



          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      };
      $scope.getallcustomers();



    }
    $scope.editorder = function(order) {
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/admin/editorder/' + order.orderId;


      if (order.orderId) {
        // alert(country.countryId);

        $localStorage.order = order;
        //


      }
    };

    if ($state.current.name === 'admin.editorder') {
      if ($localStorage.roleId !== 7) {
        $window.location.href = '#/login'

      }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';

      }

      $scope.order = {};
      $scope.order = $localStorage.order;
      $scope.orderdetails = $scope.order.orderDetailsVo;
      console.log($scope.orderdetails);
      // if (order.orderId) {
      //   // alert(country.countryId);

      //   $scope.order= $localStorage.order;
      //   //


      // }
      console.log($scope.order);



      // alert(33333333);
      $scope.getallcustomers = function() {
        $scope.customerPageVo = {
          "customerId": $scope.order.customerId,
          "cityId": 0,
          "countryId": 0,
          "userId": 0,
          "orderBy": 0,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": 100,
          "status": 2
        }
        console.log($scope.customerPageVo);
        loadingService.isLoadin = true;

        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.customer = response.object;
            console.log($scope.customer);



          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      };
      $scope.getallcustomers();

    }
    $scope.AllocateDeliverBoy = function(order) {
      // alert("haaai");
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/seller/allocateDeliveBoy/' + order.orderId;

      $localStorage.order = order;
      loadingService.isLoadin = false;
      //



    };

    if ($state.current.name === 'seller.allocateDeliveBoy') {
      // alert("hoo");
      if ($localStorage.roleId !== 50) {
        $window.location.href = '#/login'

      }
      $scope.order = {};


      $scope.order = $localStorage.order;

      $scope.getnearestdeliveryboys = function() {

        // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
        //      "cityId":0,
        //      "countryId":0,
        //      "userId":0,
        //      "orderBy":0,
        //      "fromDate":0,
        //      "toDate":0,
        //      "currentIndex":0,
        //      "rowPerPage":100,
        //      "status":2}
        //      console.log( $scope.deliveryboyPageVo);

        loadingService.isLoadin = true;

        inputService.getallnearestdeliveryboys().then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.nearestdeliveryboys = response.object;
            console.log($scope.nearestdeliveryboys);

            $scope.nearest = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -7) {
            $scope.nearest = 1;

          } else {
            alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      };
      $scope.getnearestdeliveryboys();




      $scope.assignworktodeliveryboy = function(deliveryboy, order) {
        console.log(deliveryboy, order);
        var orderid = order.orderId;
        $scope.deliveryboyPageVo = {
          "deliveryBoyId": deliveryboy.deliveryBoyId,
          "orderId": orderid,
          "deliverStatus": 1
        }

        console.log($scope.deliveryboyPageVo);


        console.log(deliveryboy, orderid);

        var orderId = orderid;
        console.log(orderId);
        var deliveryboyId = deliveryboy.deliveryBoyId;
        console.log(deliveryboyId);
        // console.log(orderId);
        //  console.log(orderstatus);


        loadingService.isLoadin = true;

        inputService.assignworktodeliveryboy($scope.deliveryboyPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {

            toaster.pop('success', "Successfull..!", "Assigned the DeliveryBoy!");

            $scope.Confirmedorder(order);
            $window.location.href = '#/seller/neworder';







          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            toaster.pop('info', "Oops..!", response.errorMessage);
            // alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      };

      // sedfhjklhg

      $scope.assignworktodeliveryboyofreturn = function(deliveryboy, order) {
        console.log(deliveryboy, order);
        var orderid = order.orderId;
        $scope.deliveryboyPageVo = {
          "deliveryBoyId": deliveryboy.deliveryBoyId,
          "orderId": orderid,
          "deliverStatus": 4
        }

        console.log($scope.deliveryboyPageVo);


        console.log(deliveryboy, orderid);

        var orderId = orderid;
        console.log(orderId);
        var deliveryboyId = deliveryboy.deliveryBoyId;
        console.log(deliveryboyId);
        // console.log(orderId);
        //  console.log(orderstatus);


        loadingService.isLoadin = true;

        inputService.assignworktodeliveryboy($scope.deliveryboyPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);



          if (response.errorCode === 0) {

            toaster.pop('success', "Successfull..!", "Assigned the DeliveryBoy!");

            $scope.confirmedreturnorder(order);
            $window.location.href = '#/seller/returns';







          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else {
            toaster.pop('info', "Oops..!", response.errorMessage);
            // alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      };



    }
    // change returns status


    $scope.confirmedreturnorder = function(order) {
      console.log(order);

      var returnItemId = order.returnItemId;
      var orderid = order.orderId;
      var orderDetailsIds = order.orderListId;
      var status = 11;

      console.log(returnItemId);
      console.log(status);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.confirmingreturnitem(returnItemId).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          toaster.pop('success', "Successfull..!", "Confirmed the Return Request!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);
          $scope.getallproductreturns(1);

          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else {
          // alert(response.errorMessage);
        }
      }, function() {
        alert('Oops! somthing went wrong');
      });
    };

    $scope.returedtheorder = function(order) {
      console.log(order);

      var returnItemId = order.returnItemId;
      var orderid = order.orderId;
      var orderDetailsIds = order.orderListId;
      // var status =11;

      console.log(returnItemId);
      // console.log(status);


      loadingService.isLoadin = true;

      inputService.returnedreturnitem(returnItemId).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          toaster.pop('success', "Successfull..!", "Successfully Completed the Return !");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);
          $scope.getallconfirmedreturns(1);

          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else {
          // alert(response.errorMessage);
        }
      }, function() {
        alert('Oops! somthing went wrong');
      });
    };

    // REJECT RETURN ORDER

    $scope.RejectReturn = function(order) {
      console.log(order);

      var returnItemId = order.returnItemId;
      var orderid = order.orderId;
      var orderDetailsIds = order.orderListId;
      // var status =11;

      console.log(returnItemId);
      console.log(status);
   

      loadingService.isLoadin = true;

      inputService.rejectreturnitem(returnItemId).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          toaster.pop('success', "Successfull..!", "Successfully Rejected the Return Request!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);
          $scope.getallproductreturns(1);

          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else {
          // alert(response.errorMessage);
        }
      }, function() {
        alert('Oops! somthing went wrong');
      });
    };


    $scope.AllocateDeliverBoyforreturn = function(order) {
      // alert("haaai");
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/seller/allocateDeliveBoy/' + order.returnItemId;

      $localStorage.order = order;
      loadingService.isLoadin = false;
      //



    };

    $scope.Confirmedorder = function(order) {

      var orderId = order.orderId;
      var orderstatus = 2;
      console.log(orderId);
      console.log(orderstatus);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.changeorderstatus(orderId, orderstatus).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          $scope.getallorders(1);
          toaster.pop('success', "Successfull..!", "Confirmed the Order!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);


          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        } else {
          // alert(response.errorMessage);
        }
      }, function() {
        alert('Oops! somthing went wrong');
      });
    };
    $scope.ChangetoDelivered = function(order) {

      var orderId = order.orderId;
      var orderstatus = 4;
      console.log(orderId);
      console.log(orderstatus);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.changeorderstatus(orderId, orderstatus).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          $scope.getallpickedorders(1);
          toaster.pop('success', "Successfull..!", "Delivered the Order!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);


          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        }
        // else if (response.errorCode=== -3) {


        // }
        else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        } else {
          // alert(response.errorMessage);
        }
      }, function() {
        alert('Oops! somthing went wrong');
      });
    };

    $scope.ChangetoShipping = function(order) {

      var orderId = order.orderId;
      var orderstatus = 6;
      console.log(orderId);
      console.log(orderstatus);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.changeorderstatus(orderId, orderstatus).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          $scope.getallconfirmedorders(1);
          toaster.pop('success', "Successfull..!", "Successfully Shipped the Order!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);


          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else if (response.errorCode === 45 || response.errorCode === 34) {
          $localStorage.token = 0;
          $window.location.href = '#/login';


        } else {
          // alert(response.errorMessage);
        }
      }, function() {
        alert('Oops! somthing went wrong');
      });
    };

    $scope.rejectOrder = function(order) {
      //alert("reject call");

      var orderId = order.orderId;
      var orderstatus = 3;
      console.log(orderId);
      console.log(orderstatus);
      // $scope.deliveryboyPageVo = {"customerId":$scope.order.customerId,
      //      "cityId":0,
      //      "countryId":0,
      //      "userId":0,
      //      "orderBy":0,
      //      "fromDate":0,
      //      "toDate":0,
      //      "currentIndex":0,
      //      "rowPerPage":100,
      //      "status":2}
      //      console.log( $scope.deliveryboyPageVo);

      loadingService.isLoadin = true;

      inputService.changeorderstatus(orderId, orderstatus).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);



        if (response.errorCode === 0) {
          toaster.pop('success', "Successfull..!", "Rejected the Order!");
          //        $scope.getalldeliveredorders(1);
          // $scope.getallrejectedorders(1);
          // $scope.getallconfirmedorders(1);
          $scope.getallorders(1);

          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          // $scope.nearestdeliveryboys = response.object;
          // console.log($scope.nearestdeliveryboys);



        } else {
          // alert(response.errorMessage);
        }
      }, function() {
        alert('Oops! somthing went wrong');
      });
    };
    // $scope.getnearestdeliveryboys(); 




    // vieworderdetails

    $scope.vieworderdetails = function(order) {
      loadingService.isLoadin = true;
      console.log(order);
      $window.location.href = '#/admin/vieworder/' + order.orderId;


      if (order.orderId) {
        // alert(country.countryId);

        $localStorage.order = order;
        //


      }
    };
    if ($state.current.name === 'admin.vieworder') {
      // alert("haaaaaaaai");
      // if( $localStorage.roleId !== 7){
      //   $window.location.href ='#/login'

      // }

       $scope.calculateOrderSubTotalAndShippingAndInstallation=function(index,item){
        console.log(index,item);
        if (index==1) {
          $scope.subTotal=0;
          $scope.totalShippingCharge=0;
          $scope.totalInstallationFee=0;
          if (item.status!=10 && item.status!=5 && item.status!=11 && item.status!=12 && item.status!=15) {
            $scope.subTotal=item.itemNetPrice;
            $scope.totalShippingCharge=item.shippingCharge;
            $scope.totalInstallationFee=item.installationCharge;

          }
           
        }else{
           // $scope.subTotal= $scope.subTotal+itemPrice;
           if (item.status!=10 && item.status!=5 && item.status!=11 && item.status!=12 && item.status!=15) {
            $scope.subTotal+=item.itemNetPrice;
            $scope.totalShippingCharge+=item.shippingCharge;
            $scope.totalInstallationFee+=item.installationCharge;

           }
           
        }
        console.log($scope.subTotal);

       }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      $scope.order = {};
      $scope.order = $localStorage.order;
      console.log($scope.order);
      $scope.orderdetails = $scope.order.orderDetailsVo;
      $scope.shippingDetails = $scope.order.shippingDetails;
      console.log($scope.shippingDetails);


      console.log($scope.orderdetails);
      console.log($scope.order);
      loadingService.isLoadin = false;
      var customerId = $scope.order.customerId;


      $scope.getcustomer = function(customerId) {
        // alert("success1");
        $scope.customerPageVo = {
          "customerId": 0,
          "cityId": 0,
          "countryId": 0,
          "userId": customerId,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": "75",
          "status": 2,
          "customerName": "",
          "phoneNo": "",
          "emailId": ""

        }
        console.log($scope.customerPageVo);

        loadingService.isLoadin = true;


        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;

          console.log(response);



          if (response.errorCode === 0) {

            $scope.customerinfo = response.object[0];
            console.log($scope.customerinfo);



          } else {
            alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      };
      $scope.getcustomer(customerId);


      // DOWNLOAD IN PDF
      $scope.export = function() {
        // alert("haagdgxdgaaaa")
        html2canvas(document.getElementById('exportthis'), {
          onrendered: function(canvas) {
            var data = canvas.toDataURL();
            var docDefinition = {
              content: [{
                image: data,
                width: 500,
              }]
            };
            pdfMake.createPdf(docDefinition).download("Order.pdf");
          }
        });
      }
      // $scope.printDiv = function (div) {
      //   var docHead = document.head.outerHTML;
      //   var printContents = document.getElementById(div).outerHTML;
      //   var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

      //   var newWin = window.open("", "_blank", winAttr);
      //   var writeDoc = newWin.document;
      //   writeDoc.open();
      //   writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
      //   writeDoc.close();
      //   newWin.focus();
      // }
      // $scope.printDiv = function(divName) {
      //   var printContents = document.getElementById(divName).innerHTML;
      //   var popupWin = window.open('', '_blank', 'width=300,height=300');
      //   popupWin.document.open();
      //   popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</body></html>');
      //   popupWin.document.close();
      // }
      // $scope.printDiv = function (divName) {

      //     var printContents = document.getElementById(divName).innerHTML;
      //     var originalContents = document.body.innerHTML;      

      //     if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
      //         var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
      //         popupWin.window.focus();
      //         popupWin.document.write('<!DOCTYPE html><html><head>' +
      //             '<link rel="stylesheet" type="text/css" href="style.css" />' +
      //             '</head><body onload="window.print()"><div class="reward-body">' + printContents + '</div></html>');
      //         popupWin.onbeforeunload = function (event) {
      //             popupWin.close();
      //             return '.\n';
      //         };
      //         popupWin.onabort = function (event) {
      //             popupWin.document.close();
      //             popupWin.close();
      //         }
      //     } else {
      //         var popupWin = window.open('', '_blank', 'width=800,height=600');
      //         popupWin.document.open();
      //         popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + printContents + '</html>');
      //         popupWin.document.close();
      //     }
      //     popupWin.document.close();

      //     return true;
      // }

      // function printDiv() {
      //   var contentToPrint = document.getElementById('#myDiv').innerHTML;
      //   var windowPopup = window.open('', '_blank', 'width=500,height=500');
      //   windowPopup.document.open();
      //   windowPopup.document.write('<html><head><link rel="stylesheet" type="text/css" href="" /></head><body onload="window.print()">' + contentToPrint + '</body></html>');
      //   windowPopup.document.close();
      // } 
      $scope.printDiv = function(div) {
        var docHead = document.head.outerHTML;
        var printContents = document.getElementById(div).outerHTML;
        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

        var newWin = window.open("", "_blank", winAttr);
        var writeDoc = newWin.document;
        writeDoc.open();
        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
        writeDoc.close();
        newWin.focus();
      }
    }
    // VIEW RETURN ORDER
    if ($state.current.name === 'admin.viewreturn') {
      //alert("new")

      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
       $scope.returnOrder={};
       $scope.returnOrder=$localStorage.returnOrder;
        $scope.finalPrice=$scope.returnOrder.totalAmountIncludingOffer+$scope.returnOrder.shippingCharge; 
        $scope.returnReason={};
        $scope.returnReason=$localStorage.returnReason;

        

      // $scope.returnOrder = response.object[0];
      //       console.log($scope.returnOrder);
      //       var orderId =$scope.returnOrder.orderId;
      //       console.log(orderId);
      //        $scope.finalPrice=$scope.returnOrder.totalAmountIncludingOffer+$scope.returnOrder.shippingCharge;       
      //        console.log($scope.finalPrice);  
      //       // $localStorage.returnOrder= $scope.returnOrder;





      var customerId = $localStorage.customerId;
      console.log(customerId);
      $scope.order = {};
      $scope.order = $localStorage.order;
      console.log($scope.order);
      $scope.orderdetails = $scope.order.orderDetailsVo;
      $scope.shippingDetails = $scope.order.shippingDetails;
      console.log($scope.shippingDetails);


      console.log($scope.orderdetails);
      console.log($scope.order);
      loadingService.isLoadin = false;

      $scope.getcustomer = function() {
        // alert("success1");
        $scope.customerPageVo = {
          "customerId": 0,
          "cityId": 0,
          "countryId": 0,
          "userId": customerId,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": "75",
          "status": 2,
          "customerName": "",
          "phoneNo": "",
          "emailId": ""

        }
        console.log($scope.customerPageVo);

        loadingService.isLoadin = true;


        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;

          console.log(response);



          if (response.errorCode === 0) {

            $scope.customerifo = response.object[0];
            console.log($scope.customerifo);



          } else {
            //alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      };
      $scope.getcustomer();


    }

    // VIEW RETURN ORDER
    if ($state.current.name === 'admin.viewreturnorder') {
      // alert("haaaaaaaai");
      // if( $localStorage.roleId !== 7){
      //   $window.location.href ='#/login'

      // }
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      $scope.order = {};
      $scope.order = $localStorage.order;
      console.log($scope.order);
      $scope.orderdetails = $scope.order.orderDetailsVo;
      $scope.shippingDetails = $scope.order.shippingDetails;
      console.log($scope.shippingDetails);


      console.log($scope.orderdetails);
      console.log($scope.order);
      loadingService.isLoadin = false;
      var customerId = $scope.order.customerId;


      // $scope. country.countryFlag ='1';
      // if ($stateParams.cityId !== '0') {
      //   $scope.city = $localStorage.city;
      // }
      // $scope.getcustomer(customerId);
      //  $scope.getallcustomers(customerId);
      // $scope.getallconfirmedorders(1);
      $scope.getcustomer = function(customerId) {
        // alert("success1");
        $scope.customerPageVo = {
          "customerId": 0,
          "cityId": 0,
          "countryId": 0,
          "userId": customerId,
          "fromDate": 0,
          "toDate": 0,
          "currentIndex": 0,
          "rowPerPage": "75",
          "status": 2,
          "customerName": "",
          "phoneNo": "",
          "emailId": ""

        }
        console.log($scope.customerPageVo);
        // alert("haai");
        loadingService.isLoadin = true;
        // $scope.customerPageVo.currentIndex = (pageno - 1) * $scope.customerPageVo.rowPerPage;

        inputService.getallcustomers($scope.customerPageVo).then(function(response) {
          loadingService.isLoadin = false;
          // $scope.noPages = Math.ceil(response.totalRecords / $scope.customerPageVo.rowPerPage);
          console.log(response);



          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);
            $scope.customerifo = response.object[0];
            console.log($scope.customerifo);



          } else {
           // alert(response.errorMessage);
          }
        }, function() {
          alert('Oops! somthing went wrong');
        });
      };
      $scope.getcustomer(customerId);


    }


    $scope.pagereload = function() {
      window.location.reload();

    }
    $scope.orderSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [1],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }
        console.log($scope.orderPageVo);
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        // loadingService.isLoadin=true;
        $scope.loading = 1;

        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;

          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.neworders = response.object;
            console.log($scope.neworders);
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            // console.log($scope.orderdetails);
            $scope.nodata = 0;


          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            $scope.loading = 0;
            // loadingService.isLoadin=false;
            $scope.neworders = [];
            $scope.nodata = 1;




          } else {
            // alert("helllo");
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallorders($scope.pageno);

    }
    // SEARCH Confirmed orders

    $scope.confirmedorderSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallconfirmedorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [2],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.confirmedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.confirmedorders);
            $scope.noconfirmeddata = 0;

          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            $scope.confirmedorders = [];
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.noconfirmeddata = 1;




          } else {
            //alert(response.errorMessage);
          }
        });
      }
      $scope.getallconfirmedorders($scope.pageno);

    }

    // SEARCH REJECTED ORDERS

    $scope.rejectedorderSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallrejectedorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [3],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.rejectedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.rejectedorders);
            $scope.norejecteddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            // loadingService.isLoadin=false;
            $scope.rejectedorders = [];
            $scope.loading = 0;
            $scope.norejecteddata = 1;
          } else {
           // alert(response.errorMessage);
          }
        });
      }
      $scope.getallrejectedorders($scope.pageno);

    }

    // SEARCH PICKED ORDERS

    $scope.pickedorderSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getallpickedorders = function(pageno) {
        $scope.pickedorderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [6],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.pickedorderPageVo.currentIndex = (pageno - 1) * $scope.pickedorderPageVo.rowPerPage;
        sellerService.getallorders($scope.pickedorderPageVo).then(function(response) {
          $scope.noPages = Math.ceil(response.totalRecords / $scope.pickedorderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.pickedorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.pickedorders);
            $scope.nopickeddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            $scope.pickedorders = [];
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.nopickeddata = 1;
            console.log($scope.nopickeddata);
          } else {
            alert(response.errorMessage);
          }
        });
      }
      $scope.getallpickedorders($scope.pageno);

    }

    // SEARCH DELIVERED ORDERS
    $scope.deliveredSearchwithOrderId = function(orderid) {
      console.log(orderid);


      $scope.noPages = 1;
      $scope.getNumber = function(num) {
        return new Array(num);
      }


      $scope.pageno = 1;

      $scope.getalldeliveredorders = function(pageno) {
        $scope.orderPageVo =

          {
            "rowPerPage": 25,
            "currentIndex": 0,
            "customerId": 0,
            "fromDate": 0,
            "toDate": 0,
            "orderId": orderid,
            "status": [4],
            "storeId": 0,
            "deliveryDate": "0",
            "serviceOrProduct": "",
            "deliveryBoyId": 0,
            "orderListId": "0"
          }

        // loadingService.isLoadin=true;
        $scope.loading = 1;
        $scope.orderPageVo.currentIndex = (pageno - 1) * $scope.orderPageVo.rowPerPage;
        sellerService.getallorders($scope.orderPageVo).then(function(response) {
          $scope.noPages = Math.ceil(response.totalRecords / $scope.orderPageVo.rowPerPage);
          // loadingService.isLoadin=false;
          $scope.loading = 0;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.deliveredorders = response.object;
            // $scope.orderdetails = response.object;
            // [0].orderDetailsVo;
            console.log($scope.deliveredorders);
            $scope.nodelivereddata = 0;
          } else if (response.errorCode === 45 || response.errorCode === 34) {
            $localStorage.token = 0;
            $window.location.href = '#/login';


          } else if (response.errorCode === -3) {
            $scope.deliveredorders = [];
            // loadingService.isLoadin=false;
            $scope.loading = 0;
            $scope.nodelivereddata = 1;
            console.log($scope.nodelivereddata);
          } else {
            alert(response.errorMessage);
          }
        });
      }

      $scope.getalldeliveredorders($scope.pageno);

    }



      $scope.gotoFirstPage=function(key){
    // $scope.pagevo.pageNo=1;
    console.log(key);
     if (key==1) {
      $scope.getallorders(1);

        }else if (key==2) {
          $scope.getallconfirmedorders(1);

        }else if (key==3) {
           $scope.getallrejectedorders(1);

        }else if (key==4) {
         $scope.getallpickedorders(1);

        }else if (key==5) {
           $scope.getalldeliveredorders(1);
        }else if(key==6){
          $scope.getallcancelledorders(1);

        }else if(key==7){
         // alert("six")
          $scope.getallproductreturns(1);

        }else if (key==8) {
           $scope.getallconfirmedreturns(1);
        }else if (key==9) {
           $scope.getallrejectedreturns(1);
        }else if (key==10) {
           $scope.getallcompletedreturns(1);
        }


 //    if (key==1) {
 // $scope.getproduct(1);
 //    }else{
 //       $scope.getsortproduct(1); 
 //    }
   
   }
   $scope.gotoLastPage=function(key,lastPageNum){
    console.log(key,lastPageNum);
    var lastPageNumb=lastPageNum;
    // $scope.getAllStockListbypagevo($scope.pagevo);
     // if (key==1) {
     //  $scope.getallorders(lastPageNumb);

     //    }else if (key==2) {
     //       $scope.getallconfirmedorders(lastPageNumb);

     //    }else if (key==3) {
     //       $scope.getallrejectedorders(lastPageNumb);

     //    }else if (key==4) {
     //       $scope.getallpickedorders(lastPageNumb);

     //    }else{
     //       $scope.getalldeliveredorders(lastPageNumb);

     //    }
        if (key==1) {
            //alert("one")
      $scope.getallorders(lastPageNumb);

        }else if (key==2) {
          //alert("two")
          $scope.getallconfirmedorders(lastPageNumb);

        }else if (key==3) {
         /// alert("three")
           $scope.getallrejectedorders(lastPageNumb);

        }else if (key==4) {
        //  alert("four")
         $scope.getallpickedorders(lastPageNumb);

        }else if (key==5) {
          //alert("five")
           $scope.getalldeliveredorders(lastPageNumb);
        }else if(key==6){
         // alert("six")
          $scope.getallcancelledorders(lastPageNumb);

        }else if(key==7){
         // alert("six")
          $scope.getallproductreturns(lastPageNumb);

        }else if (key==8) {
           $scope.getallconfirmedreturns(lastPageNumb);
        }else if (key==9) {
           $scope.getallrejectedreturns(lastPageNumb);
        }else if (key==10) {
           $scope.getallcompletedreturns(lastPageNumb);
        }


    // if (key==1) {
    //   $scope.getproduct(lastPageNumb);
    // }else{
    //    $scope.getsortproduct(lastPageNumb); 
    // }
     
   }
    $scope.gotoNextPage=function(key,currentLastindex){
        console.log(currentLastindex);
        // $scope.pagevo.pageNo=currentLastindex+1;
        var newPageFirstIndex=currentLastindex+1;
        console.log(newPageFirstIndex);
        // $scope.getAllStockListbypagevo($scope.pagevo);
        // if (key==1) {
        //   $scope.getallorders(newPageFirstIndex);

        // }else if (key==2) {
        //   $scope.getallconfirmedorders(newPageFirstIndex);

        // }else if (key==3) {
        //    $scope.getallrejectedorders(newPageFirstIndex);

        // }else if (key==4) {
        //    $scope.getallpickedorders(newPageFirstIndex);

        // }else{
        //   $scope.getalldeliveredorders(newPageFirstIndex);


        // }
   if (key==1) {
            //alert("one")
      $scope.getallorders(newPageFirstIndex);

        }else if (key==2) {
          //alert("two")
          $scope.getallconfirmedorders(newPageFirstIndex);

        }else if (key==3) {
         /// alert("three")
           $scope.getallrejectedorders(newPageFirstIndex);

        }else if (key==4) {
        //  alert("four")
         $scope.getallpickedorders(newPageFirstIndex);

        }else if (key==5) {
          //alert("five")
           $scope.getalldeliveredorders(newPageFirstIndex);
        }else if(key==6){
         // alert("six")
          $scope.getallcancelledorders(newPageFirstIndex);

        }else if(key==7){
         // alert("six")
          $scope.getallproductreturns(newPageFirstIndex);

        }else if (key==8) {
           $scope.getallconfirmedreturns(newPageFirstIndex);
        }else if (key==9) {
           $scope.getallrejectedreturns(newPageFirstIndex);
        }else if (key==10) {
           $scope.getallcompletedreturns(newPageFirstIndex);
        }



         
    //         if (key==1) {
    //   $scope.getproduct(newPageFirstIndex);
    // }else{
    //    $scope.getsortproduct(newPageFirstIndex); 
    // }
    }
     $scope.gotoPreviousPage=function(key,currentFirstindex){
        console.log(currentFirstindex);
        var previousPage=currentFirstindex-1;
        //  if (key==1) {
        //   $scope.getallorders(previousPage);

        // }else if (key==2) {
        //   $scope.getallconfirmedorders(previousPage);

        // }else if (key==3) {
        //    $scope.getallrejectedorders(previousPage);

        // }else if (key==4) {
        //    $scope.getallpickedorders(previousPage);

        // }else{
        //    $scope.getalldeliveredorders(previousPage);
          

        // }

                  if (key==1) {
            //alert("one")
      $scope.getallorders(previousPage);

        }else if (key==2) {
          //alert("two")
          $scope.getallconfirmedorders(previousPage);

        }else if (key==3) {
         /// alert("three")
           $scope.getallrejectedorders(previousPage);

        }else if (key==4) {
        //  alert("four")
         $scope.getallpickedorders(previousPage);

        }else if (key==5) {
          //alert("five")
           $scope.getalldeliveredorders(previousPage);
        }else if(key==6){
         // alert("six")
          $scope.getallcancelledorders(previousPage);

        }else if(key==7){
         // alert("six")
          $scope.getallproductreturns(previousPage);

        }else if (key==8) {
           $scope.getallconfirmedreturns(previousPage);
        }else if (key==9) {
           $scope.getallrejectedreturns(previousPage);
        }else if (key==10) {
           $scope.getallcompletedreturns(previousPage);
        }




       
    //              if (key==1) {
    //   $scope.getproduct(previousPage);
    // }else{
    //    $scope.getsortproduct(previousPage); 
    // }
    }

  $scope.getNextPageItems=function(key,pageNo){
console.log(pageNo);
var pageNum= pageNo;
 // if (key==1) {
 //  $scope.getallorders(pageNum);

 //        }else if (key==2) {
 //          $scope.getallconfirmedorders(pageNum);

 //        }else if (key==3) {
 //           $scope.getallrejectedorders(pageNum);

 //        }else if (key==4) {
 //           $scope.getallpickedorders(pageNum);

 //        }else{
 //           $scope.getalldeliveredorders(pageNum);

 //        }


          if (key==1) {
            //alert("one")
      $scope.getallorders(pageNum);

        }else if (key==2) {
          //alert("two")
          $scope.getallconfirmedorders(pageNum);

        }else if (key==3) {
         /// alert("three")
           $scope.getallrejectedorders(pageNum);

        }else if (key==4) {
        //  alert("four")
         $scope.getallpickedorders(pageNum);

        }else if (key==5) {
          //alert("five")
           $scope.getalldeliveredorders(pageNum);
        }else if(key==6){
         // alert("six")
          $scope.getallcancelledorders(pageNum);

        }else if(key==7){
         // alert("six")
          $scope.getallproductreturns(pageNum);

        }else if (key==8) {
           $scope.getallconfirmedreturns(pageNum);
        }else if (key==9) {
           $scope.getallrejectedreturns(pageNum);
        }else if (key==10) {
           $scope.getallcompletedreturns(pageNum);
        }

        

        

    //            if (key==1) {
    //    $scope.getproduct(pageNum);
    // }else{
    //    $scope.getsortproduct(pageNum); 
    // }
  }



  });
