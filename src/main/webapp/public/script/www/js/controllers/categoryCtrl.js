angular.module('starter.categoryCtrl', [])
  .controller('categoryCtrl', function($scope, $window, $state, Upload, inputService, toaster, storeService, FILE_CONSTANT, MAIN_CONSTANT, $stateParams, $localStorage, loadingService) {
    //alert($localStorage.token);
    $scope.imageUrl = FILE_CONSTANT.filepath;
    if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
    $window.scrollTo(0, 0);
    $scope.field1 = 1;
    $scope.field2 = 1;



    if ($state.current.name === 'admin.category') {
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      $scope.getcategory = function(maincat) {
        loadingService.isLoadin = true;
        inputService.getcategories(maincat).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.categories = response.object;
            console.log($scope.categories);
          }
        });
      };
      $scope.getcategory();

      $scope.categorymanage = function(category) {
        loadingService.isLoadin = true;
        console.log(category);
        $window.location.href = '#/admin/addcategory/' + category.categoryId;


        if (category.categoryId) {
          // alert(category.categoryId);

          $localStorage.category = category;
          loadingService.isLoadin = false;
          //


        }
      };

      $scope.viewcategory = function(category) {
        loadingService.isLoadin = true;
        console.log(category);
        $window.location.href = '#/admin/viewcategory/' + category.categoryId;


        if (category.categoryId) {
          // alert(category.categoryId);

          $localStorage.category = category;
          loadingService.isLoadin = false;
          //


        }
      };

    }




    // add category


    if ($state.current.name === 'admin.addcategory') {
      $scope.category = {};
      $scope.category.color = "#111" + new Date().getTime();
      console.log($scope.category.color);
      // $scope. category.imageName ="nothing";
      if ($stateParams.categoryId !== '0') {
        $scope.category = $localStorage.category;
        $scope.catcid = 1;
      } else {
        $scope.catcid = 0;
      }
      $scope.deleteimage = function() {
        $scope.category.imageName = "";

      }


      // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
      $scope.upload = function(file) {
        $scope.isFileuploadStart = true;



        if (file) {


          $scope.clickimg = true;


          console.log(file);
          Upload.upload({
            url: MAIN_CONSTANT.site_url + "/api/pentalupload",
            headers: {
              "Content-Type": 'multipart/*',
              Bearer: $localStorage.token
            },
            /*data: { file: FILE_CONSTANT.site_url + file.name,
              type: 'image/jpeg' }*/
            data: {
              file: file
            }
          }).then(function(resp) {

            console.log(resp);

            //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
            if (resp.data.errorCode === 0) {
              $scope.isFileuploadStart = false;
              $scope.category.imageName = resp.data.object;

              console.log($scope.category.imageName);


              // if ($scope.country.countryFlag) {
              //   console.log(resp);
              //   $scope.country.countryFlag = resp.data.object;
              //   $scope.country.countryFlag.isLoadin = false;

              // }



              // if (!$scope.country.countryFlag) {
              //   $scope.country.countryFlag = "";
              // }
            } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
              $scope.isFileuploadStart = false;
              $localStorage.token = 0;
              $window.location.href = '#/login';

            } else {

            }





          }, function(resp) {
            $scope.isFileuploadStart = false;
            $scope.progressPercentage = 0;
            // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.category.imageName) {
              //splice i
              $scope.category.imageName;

            }


            console.log('Error status: ' + resp.status);


          }, function(evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
            console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
            // $scope.file = null;
            if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
              $scope.filecmplt = false;
            } else if ($scope.progressPercentage == 100) {
              $scope.filecmplt = true;

            } else {
              $scope.filenotUplaod = true;
            }
          });
        }
      };


      //loadingService.isLoadin = true;

      $scope.addCategory = function(getAddCategoryDetails) {

        getAddCategoryDetails.commision = 20;

        loadingService.isLoadin = true;

        console.log(getAddCategoryDetails);

        if (!getAddCategoryDetails.categoryName) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Category Name In English");
          } else {
            toaster.pop('info', "Please Enter Category Name In English");
          }

        } else if (!getAddCategoryDetails.categoryNameAr) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Category Name In Arabic");
          } else {
            toaster.pop('info', "Please Enter Category Name In Arabic");
          }


        } else if (!getAddCategoryDetails.discription) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Description In English");
          } else {
            toaster.pop('info', "Please Enter Description In English");
          }


        } else if (!getAddCategoryDetails.discriptionAr) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Description In Arabic");
          } else {
            toaster.pop('info', "Please Enter Description In Arabic");
          }

        } else if (!getAddCategoryDetails.imageName) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Upload Image");
          } else {
            toaster.pop('info', "Please Upload Image");
          }


        } else if (getAddCategoryDetails.status != 0 && getAddCategoryDetails.status != 1) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Choose Status");
          } else {
            toaster.pop('info', "Please Choose Status");
          }

        } else {

          inputService.submitAddCategoryDetails(getAddCategoryDetails).then(function(response) {
            loadingService.isLoadin = false;
            console.log(response);

            if (response.data.errorCode === 0) {
              if ($stateParams.categoryId == '0') {
                // toastrMsg(1,'Added new Category!','Successfull..!');
                // $scope.pop = function(){
                toaster.pop('success', "Successfull..!", "Added New Category!");
                // toaster.pop('success', "Added new Category!", "Successfull..!");
                // };
              } else {
                toaster.pop('success', "Successfull..!", "Updated  The Category!");

              }

              // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
              // console.log($scope.allProducts);
              $window.location.href = '#/admin/category';
            } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
              //  console.log( );
              $window.location.href = '#/login';

            } else {
              toaster.pop('error', "Oops..!", response.data.errorMessage);
              // alert(response.data.errorMessage);
            }

          });

        }


        /*/fd*/
      }


    }


    // $scope.getcategory = function(maincat) {
    //    loadingService.isLoadin=true;
    //       inputService.getcategories(maincat).then(function(response) {
    //          loadingService.isLoadin= false;
    //        console.log(response);
    //         if (response.errorCode === 0) {
    //           $scope.categories = response.object;
    //           console.log($scope.categories);
    //         }
    //       });
    //     };
    //  $scope.getcategory();
    // $scope.viewcategory=function(category){
    //    loadingService.isLoadin=true;
    //   console.log(category);
    //   $window.location.href='#/admin/viewcategory/'+category.categoryId;


    //   if (category.categoryId) {
    //     // alert(category.categoryId);

    //   $localStorage.category=category;
    //    loadingService.isLoadin=false;
    //   //


    // }
    // };

    // $scope.categorymanage=function(category){
    //    loadingService.isLoadin=true;
    //  console.log(category);
    //  $window.location.href='#/admin/addcategory/'+category.categoryId;


    //  if (category.categoryId) {
    //    // alert(category.categoryId);

    //  $localStorage.category=category;
    //    loadingService.isLoadin=false;
    //  //


    // }
    // };


    //view category

    if ($state.current.name === 'admin.viewcategory') {
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      $scope.category = {};
      $scope.category.imageName = "nothing";
      if ($stateParams.categoryId !== '0') {
        $scope.category = $localStorage.category;
      }





    }




    //  // add category


    //  if ($state.current.name === 'admin.addcategory') {
    //    $scope.category = {};
    //     $scope. category.imageName ="nothing";
    //       if ($stateParams.categoryId!=='0') {
    //       $scope.category=$localStorage.category;
    //   }



    //    //loadingService.isLoadin = true;

    //    $scope.addCategory=function(getAddCategoryDetails){
    //       loadingService.isLoadin=true;

    //  console.log(getAddCategoryDetails);



    //              inputService.submitAddCategoryDetails(getAddCategoryDetails).then(function(response) {
    //                 loadingService.isLoadin=false;
    //                  console.log(response);

    //                      if (response.data.errorCode === 0) {
    //                        if ($stateParams.categoryId =='0') {
    //                        // toastrMsg(1,'Added new Category!','Successfull..!');
    //                         // $scope.pop = function(){
    //                            toaster.pop('success', "Successfull..!", "Added new Category!");
    //                            // toaster.pop('success', "Added new Category!", "Successfull..!");
    //                             // };
    //                           } else{
    //                             toaster.pop('success', "Successfull..!", "Updated  The Category!");

    //                           }

    //                         // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
    //                         // console.log($scope.allProducts);
    //                          $window.location.href = '#/admin/category';
    //                      } else  if (response.data.errorCode === 45||response.data.errorCode === 34) {
    //                      //  console.log( );
    //           $window.location.href = '#/login';

    //                      }else{
    //                         toaster.pop('error', "Oops..!", response.data.errorMessage);
    //                        // alert(response.data.errorMessage);
    //                      }

    //              });


    //  /*/fd*/
    // }


    //   }


    //  if ($state.current.name === 'admin.manufacture') {
    //       if (!$localStorage.token || $localStorage.token === 0) {
    //       $window.location.href = '#/login';
    //     }

    //      $scope.getmanufacture = function(maincat) {
    //    loadingService.isLoadin=true;
    //       inputService.getmanufactures(maincat).then(function(response) {
    //          loadingService.isLoadin= false;
    //         console.log(response);
    //         if (response.errorCode === 0) {
    //           $scope.manufactures = response.object;
    //           console.log($scope.manufactures);
    //         }
    //       });
    //     };
    //  $scope.getmanufacture();

    //  $scope.manufacturemanage=function(manufacture){
    //    loadingService.isLoadin=true;
    //   console.log(manufacture);
    //   $window.location.href='#/admin/addmanufacture/'+manufacture.manufacturerId;


    //   if (manufacture.manufacturerId) {
    //     // alert(category.categoryId);

    //   $localStorage.manufacture=manufacture;
    //    loadingService.isLoadin=false;
    //   //


    // }
    // };

    // $scope.viewmanufacture=function(manufacture){
    //    loadingService.isLoadin=true;
    //   console.log(manufacture);
    //   $window.location.href='#/admin/viewmanufacture/'+manufacture.manufacturerId;


    //   if (manufacture.manufacturerId) {
    //     // alert(category.categoryId);

    //   $localStorage.manufacture=manufacture;
    //    loadingService.isLoadin=false;
    //   //


    // }
    // };

    //     }

    if ($state.current.name === 'admin.addmanufacture') {
      $scope.manufacture = {};
      // $scope.category.color="#111"+ new Date().getTime();
      // console.log( $scope.category.color);
      // $scope. category.imageName ="nothing";
      if ($stateParams.manufacturerId !== '0') {
        $scope.manufacture = $localStorage.manufacture;
        $scope.manfid = 1;
      } else {
        $scope.manfid = 0;
      }



      //loadingService.isLoadin = true;
      $scope.manufacture.rowPerPage = 100;
      $scope.manufacture.currentIndex = 0;

      $scope.upload = function(file) {
        if (file) {
          $scope.isFileuploadStart = true;
          $scope.clickimg = true;
          console.log(file);
          Upload.upload({
            url: MAIN_CONSTANT.site_url + "/api/pentalupload",
            headers: {
              "Content-Type": 'multipart/*',
              Bearer: $localStorage.token
            },
            /*data: { file: FILE_CONSTANT.site_url + file.name,
              type: 'image/jpeg' }*/
            data: {
              file: file
            }
          }).then(function(resp) {
            console.log(resp);
            //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
            if (resp.data.errorCode === 0) {
              $scope.manufacture.imageName = resp.data.object;
              $scope.isFileuploadStart = false;
              console.log($scope.manufacture.imageName);
            } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
              $localStorage.token = 0;
              $window.location.href = '#/login';
            } else {

            }

          }, function(resp) {
            $scope.progressPercentage = 0;
            // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.manufacture.imageName) {
              //splice i
              $scope.manufacture.imageName;

            }

            console.log('Error status: ' + resp.status);


          }, function(evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
            console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
            // $scope.file = null;
            if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
              $scope.filecmplt = false;
            } else if ($scope.progressPercentage == 100) {
              $scope.filecmplt = true;

            } else {
              $scope.filenotUplaod = true;
            }
          });
        }
      };
      $scope.deleteimage = function() {
        $scope.manufacture.imageName = "";

      }
      // "rowPerPage":"100","currentIndex":0

      $scope.addManufacture = function(getAddManufactureDetails) {
        console.log(getAddManufactureDetails);



        

        console.log(getAddManufactureDetails);
        manufacturerNameAr
        
               if (!getAddManufactureDetails.manufacturerName) {
        if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Manufacture Name In English");
          } else {
            toaster.pop('info', "Please Enter Manufacture Name In English");
          }

        } else if (!getAddManufactureDetails.manufacturerNameAr) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Manufacture Name In Arabic");
          } else {
            toaster.pop('info', "Please Enter Manufacture Name In Arabic");
          }


        } else if (!getAddManufactureDetails.businessType) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Business Type");
          } else {
            toaster.pop('info', "Please Enter Business Type");
          }


        }  else if (!getAddManufactureDetails.imageName) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Upload Image");
          } else {
            toaster.pop('info', "Please Upload Image");
          }


        } else if (getAddManufactureDetails.status != 0 && getAddManufactureDetails.status != 1) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Choose Status");
          } else {
            toaster.pop('info', "Please Choose Status");
          }

        }else{
             loadingService.isLoadin = true;
        inputService.submitAddManufactureDetails(getAddManufactureDetails).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);
          // console.log($scope.manufacturePageVo);

          if (response.data.errorCode === 0) {
            if ($stateParams.manufacturerId == '0') {
              // toastrMsg(1,'Added new Category!','Successfull..!');
              // $scope.pop = function(){
              toaster.pop('success', "Successfull..!", "Successfully Added New Manufacture!");
              // toaster.pop('success', "Added new Category!", "Successfull..!");
              // };
            } else {
              toaster.pop('success', "Successfull..!", " Successfully Updated Manufacture!");

            }

            // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
            // console.log($scope.allProducts);
            $window.location.href = '#/admin/manufacture';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            toaster.pop('error', "Oops..!", response.data.errorMessage);
            // alert(response.data.errorMessage);
          }

        });


        }

 


        /*/fd*/
      }


    }
    if ($state.current.name === 'admin.manufacture') {




      $scope.getallmanufacture = function() {
        loadingService.isLoadin = true;
        $scope.manufacturePageVo = {
          "manufacturerId": 0,
          "manufacturerName": "",

          "status": 2,
          "currentIndex": 0,
          "rowPerPage": 350
        }

        inputService.getallmanufactures($scope.manufacturePageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.manufactures = response.object;

          } else {
            // alert(response.errorMessage);
          }
        });
      }
      $scope.getallmanufacture();

      $scope.manufacturemanage = function(manufacture) {
        loadingService.isLoadin = true;
        console.log(manufacture);
        $window.location.href = '#/admin/addmanufacture/' + manufacture.manufacturerId;


        if (manufacture.manufacturerId) {
          // alert(category.categoryId);

          $localStorage.manufacture = manufacture;
          loadingService.isLoadin = false;
          //


        }
      };

      $scope.viewmanufacture = function(manufacture) {
        loadingService.isLoadin = true;
        console.log(manufacture);
        $window.location.href = '#/admin/viewmanufacture/' + manufacture.manufacturerId;


        if (manufacture.manufacturerId) {
          // alert(category.categoryId);

          $localStorage.manufacture = manufacture;
          loadingService.isLoadin = false;
          //


        }
      };
    }
    if ($state.current.name === 'admin.viewmanufacture') {
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      $scope.manufacture = {};
      // $scope. category.imageName ="nothing";
      //     if ($stateParams.categoryId!=='0') {

      // }
      $scope.manufacture = $localStorage.manufacture;
      console.log($scope.manufacture);




    }

    // PROMOCODE PAGE

    $scope.PromoCode = {};
    $scope.selectone = function(data) {
      console.log(data);
      if (!data) {
        // alert("nodata");
        $scope.field1 = 1;
      };
      var arr = data;
      if (arr.length >= 1) {
        $scope.field1 = 0;
      } else {
        $scope.field1 = 1;
      }

      console.log($scope.field1);

      // (offer.offerPercentage)



    }
    $scope.selectone = function(data) {
      console.log(data);
      if (!data) {
        // alert("nodata");
        $scope.field1 = 1;
      };
      var arr = data;
      if (arr.length >= 1) {
        $scope.field1 = 0;
      } else {
        $scope.field1 = 1;
      }

      console.log($scope.field1);

      // (offer.offerPercentage)



    }
    $scope.selectonetwo = function(data) {
      console.log(data);
      if (!data) {
        // alert("nodata");
        $scope.field2 = 1;
      };
      var arra = data;
      if (arra.length >= 1) {
        $scope.field2 = 0;
      } else {
        $scope.field2 = 1;
      }

      console.log($scope.field2);

      // (offer.offerPercentage)



    }


    $scope.getallcustomers = function() {
      $scope.customerPageVo = {
        "customerId": 0,
        "cityId": 0,
        "countryId": 0,
        "userId": 0,
        "fromDate": 0,
        "toDate": 0,
        "currentIndex": 0,
        "rowPerPage": "75",
        "status": 1,
        "customerName": "",
        "phoneNo": "",
        "emailId": ""

      }
      // alert("haai");
      loadingService.isLoadin = true;
      // $scope.customerPageVo.currentIndex = (pageno - 1) * $scope.customerPageVo.rowPerPage;

      inputService.getallcustomers($scope.customerPageVo).then(function(response) {
        loadingService.isLoadin = false;
        // $scope.noPages = Math.ceil(response.totalRecords / $scope.customerPageVo.rowPerPage);
        console.log(response);



        if (response.errorCode === 0) {
          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          $scope.customers = response.object;
          console.log($scope.customers);



        } else {
          //alert(response.errorMessage);
        }
      }, function() {
        // alert('Oops! somthing went wrong');
      });
    };
    $scope.getallcustomers();

    $scope.generatePromoCode = function(promocodedata) {

      console.log(promocodedata);
      promocodedata.validFrom = new Date(promocodedata.validFrom).getTime();
      console.log(promocodedata.validFrom);
      promocodedata.validTo = new Date(promocodedata.validTo).getTime();
      console.log(promocodedata.validTo);
      promocodedata.promocode = promocodedata.generatedcode;
      console.log(promocodedata);
      inputService.savepromocodedetails(promocodedata).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);

        if (response.data.errorCode === 1) {

          toaster.pop('success', "Successfull..!", "Successfully Generated Promo Code!");



          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          $window.location.href = '#/admin/promocode';
        } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          toaster.pop('error', "Oops..!", response.data.errorMessage);
          // alert(response.data.errorMessage);
        }

      });

    }
    $scope.getPromoCode = function() {


      //   console.log(promocodedata);
      //    promocodedata.validFrom=new Date( promocodedata.validFrom).getTime();
      // console.log(promocodedata.validFrom);
      //  promocodedata.validTo=new Date( promocodedata.validTo).getTime();
      // console.log(promocodedata.validTo);
      // console.log(promocodedata);
      $scope.promodatavo = {

      }
      inputService.createPromoCode($scope.promodatavo).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);

        if (response.data.errorCode === 1) {
          $scope.PromoCode.generatedcode = response.data.object;
          // $scope.generatedcode = response.data.object;
          console.log($scope.PromoCode.generatedcode);
          // toaster.pop('success', "Successfull..!", "Successfully Generated New Promo Code!");



          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          // $window.location.href = '#/admin/category';
        } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          toaster.pop('error', "Oops..!", response.data.errorMessage);
          // alert(response.data.errorMessage);
        }

      });

    }
    if ($state.current.name === 'admin.promocode') {
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }


      $scope.promocodePageVo = {
        "currentIndex": 0,
        "rowPerPage": 100,
        "codeId": 0,
        "status": 2
      }


      $scope.getallpromocode = function() {
        loadingService.isLoadin = true;

        inputService.getallpromocodes($scope.promocodePageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);


          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.promocodes = response.object;
            $scope.nooffers = 0;
            console.log($scope.promocodes);

          } else if (response.errorCode === -3) {
            $scope.nooffers = 1;

          } else {
            // alert(response.errorMessage);
          }
        });
      }
      $scope.getallpromocode();

      //  $scope.offermanage=function(offer){
      //    loadingService.isLoadin=true;
      //   console.log(offer);
      //   $window.location.href='#/admin/addoffer/'+offer.offerId;


      //   if (offer.offerId) {
      //     // alert(category.categoryId);

      //   $localStorage.offer=offer;
      //    loadingService.isLoadin=false;
      //   //


      // }
      // };

      // $scope.viewoffer=function(offer){
      //    loadingService.isLoadin=true;
      //   console.log(offer);
      //   $window.location.href='#/admin/viewoffer/'+offer.offerId;


      //   if (offer.offerId) {
      //     // alert(category.categoryId);

      //   $localStorage.offer=offer;
      //    loadingService.isLoadin=false;
      //   //


      // }
      // };

    }
    if ($state.current.name === 'admin.myaccountsettings') {


      $scope.settingsPageVo = {
        "sellerId": $localStorage.sellerId,
        "sellerName": "",
        "phoneNumber": "",
        "address": "",
        "currentIndex": 0,
        "rowPerPage": 100,
        "stat": 2
      }

      $scope.getaccountsettings = function() {
        loadingService.isLoadin = true;

        inputService.getallaccountsettings($scope.settingsPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);


          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.seller = response.object[0];
            console.log($scope.seller);

            $scope.nooffers = 0;

          } else if (response.errorCode === -3) {
            $scope.nooffers = 1;

          } else {
            // alert(response.errorMessage);
          }
        });
      }
      $scope.getaccountsettings();

    }


    $scope.saveprofileinfo = function(sellerdetails) {

      console.log(sellerdetails);

      inputService.submitEditSellerdetails(sellerdetails).then(function(response) {
        // alert("ddd");
        loadingService.isLoadin = false;
        console.log(response);

        if (response.data.errorCode === 0) {
          if ($stateParams.categoryId == '0') {
            // toastrMsg(1,'Added new Category!','Successfull..!');
            // $scope.pop = function(){
            toaster.pop('success', "Successfull..!", "Added new Category!");
            // toaster.pop('success', "Added new Category!", "Successfull..!");
            // };
          } else {
            toaster.pop('success', "Successfull..!", "Updated  The Category!");

          }

          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          $window.location.href = '#/admin/dashboard';
        } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          toaster.pop('error', "Oops..!", response.data.errorMessage);
          // alert(response.data.errorMessage);
        }

      });
    }
    $scope.getactivecountry = function(country) {
      loadingService.isLoadin = true;
      storeService.getactivecountries(country).then(function(response) {
        loadingService.isLoadin = false;
        console.log(response);
        if (response.errorCode === 0) {
          $scope.activecountries = response.object;
          console.log($scope.activecountries);
        }
      });
    };
    $scope.getactivecountry();

    // $scope.changeCountry = function(countryId) {
    //    alert("1");
    //    // alert("ent55");

    //    console.log(countryId);
    //        // $scope.search.cityId=0;


    //    $scope.getcityByCountryId(countryId);
    //    // console.log( $scope.search);

    //  };
    //   $scope.changeCountry();
    $scope.getcityByCountryId = function(countryId) {
      // alert("2");
      // alert("ent1");
      $scope.citiesbycountries = [];
      $scope.cityPageVo = {
        "countryId": countryId,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
        "cityName": "",
        // "cityStatus": 1,
        "citusStatus": 1,
        "status": []
      }

      storeService.getcitiesByCountryId($scope.cityPageVo, countryId).then(function(response) {
        // alert("ent2");
        console.log(response);
        if (response.errorCode === 0) {
          // alert("ent3");
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
    $scope.getcityByCountryId();
    // $scope.seller={};

    //  $scope.seller.longitude=1233254;
    //  $scope.seller.latitude=5422411;
    // $scope.seller.
    $scope.updateShopasdetails = function(seller) {

      seller.longitude = 123456;
      seller.latitude = 5422411;
      seller.password = "password123";
      loadingService.isLoadin = true;

      console.log(seller);





      inputService.updateSellerDetails(seller).then(function(response) {
        loadingService.isLoadin = false;

        console.log(response);

        if (response.data.errorCode === 0) {
          // $window.location.reload();
          toaster.pop('success', "Successfull..!", "Updated the Store Informations !");
          // $scope.allstore($scope.pageno);

          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          $window.location.href = '#/admin/dashboard';
        } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          // alert(response.data.errorMessage);
          toaster.pop('error', "Oops..!", response.data.errorMessage);
        }

      });

    }

    $scope.addCmsdata = function(cms) {

      console.log(cms);

      inputService.savecmsdatas(cms).then(function(response) {
        // alert("ddd");
        loadingService.isLoadin = false;
        console.log(response);

        if (response.data.errorCode === 0) {

          // toastrMsg(1,'Added new Category!','Successfull..!');
          // $scope.pop = function(){
          toaster.pop('success', "Successfull..!", "Added new Category!");
          // toaster.pop('success', "Added new Category!", "Successfull..!");
          // };


          // $scope.allProducts = $scope.allProducts.concat(response.object.Product);
          // console.log($scope.allProducts);
          $window.location.href = '#/admin/dashboard';
        } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
          //  console.log( );
          $window.location.href = '#/login';

        } else {
          toaster.pop('error', "Oops..!", response.data.errorMessage);
          // alert(response.data.errorMessage);
        }

      });
    }

    // $scope.noPages = 1;
    // $scope.getNumber = function(num) {
    //   return new Array(num);
    // }


    // $scope.pageno = 1;  


    $scope.getallcms = function() {
      $scope.cmsPageVo = {
        "cmsId": 0,
        "currentIndex": 0,
        "rowPerPage": 100
      }
      // alert("haai");
      loadingService.isLoadin = true;
      // $scope.customerPageVo.currentIndex = (pageno - 1) * $scope.customerPageVo.rowPerPage;

      inputService.getallcmsdata($scope.cmsPageVo).then(function(response) {
        loadingService.isLoadin = false;
        // $scope.noPages = Math.ceil(response.totalRecords / $scope.customerPageVo.rowPerPage);
        console.log(response);



        if (response.data.errorCode === 0) {
          // $scope.shopList = $scope.shopList.concat(response.object.Product);
          $scope.cms = response.data.object[0];
          console.log($scope.cms);



        } else {
          // alert(response.data.errorMessage);
        }
      }, function() {
        //alert('Oops! somthing went wrong');
      });
    };
    $scope.getallcms();

    //##########################
    $scope.skilledLabour = {};
    $scope.dataChanged = function() {

      $scope.skilledLabour.location = {};


      if ($scope.details) {
        $scope.wrongPlace = false;
        $scope.details.address_components.forEach(function(element) {
          if (element.types.indexOf("administrative_area_level_2") != (-1)) {
            $scope.skilledLabour.location.district = element.long_name;
          } else if (element.types.indexOf("administrative_area_level_1") != (-1)) {
            $scope.skilledLabour.location.state = element.long_name;

          } else if (element.types.indexOf("country") != (-1)) {
            $scope.skilledLabour.location.country = element.long_name;
          } else if (element.types.indexOf("postal_code") != (-1)) {
            $scope.skilledLabour.location.pincode = element.long_name;
          }
        });

        $scope.skilledLabour.location.latitude = $scope.details.geometry.location.lat();
        $scope.skilledLabour.location.longitude = $scope.details.geometry.location.lng();
      } else {
        $scope.wrongPlace = true;
      }
    }

    //#############################

  });
