angular.module('starter.locationCtrl', [])
  .controller('locationCtrl', function($scope, $window, $state, inputService, toaster, storeService, FILE_CONSTANT, toaster, MAIN_CONSTANT, Upload, storeService, storeService, $stateParams, $localStorage, loadingService) {

    if (!$localStorage.token || $localStorage.token === 0) {
      $window.location.href = '#/login';
    }
    $window.scrollTo(0, 0);
    $scope.imageUrl = FILE_CONSTANT.filepath;

    // $scope.pop = function(){
    //      toaster.pop('info', "title", "text");
    //  };


    //get country

    if ($state.current.name === 'admin.country') {
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }

      // get country


      $scope.getcountry = function(country) {
        loadingService.isLoadin = false;
        storeService.getcountries(country).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.countries = response.object;
          } else {
            loadingService.isLoadin = false;



          }
        }, function() {
          loadingService.isLoadin = false;


        });
      };
      $scope.getcountry();



    }


    // get cities

    if ($state.current.name === 'admin.city') {

      $scope.cityPageVo = {
        "countryId": 0,
        "cityId": 0,
        "rowPerPage": 300,
        "currentIndex": 0,
        "cityName": "",
        "citusStatus": 2,
        "status": []
      }


      $scope.getallcity = function() {
        loadingService.isLoadin = true;

        storeService.getallcities($scope.cityPageVo).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          //  if (response.errorCode === 0) {
          //    // $scope.shopList = $scope.shopList.concat(response.object.Product);
          //       $scope.stores = response.object.Store;
          // console.log($scope.stores );

          if (response.errorCode === 0) {
            // $scope.shopList = $scope.shopList.concat(response.object.Product);

            $scope.cities = response.object;
            $scope.nocities = 0;

          } else if (response.errorCode === -3) {
            $scope.nocities = 1;

          } else {
            alert(response.errorMessage);
          }
        });
      }
      $scope.getallcity();

    }
    $scope.ViewCountry = function(country) {
      loadingService.isLoadin = true;
      console.log(country);
      $window.location.href = '#/admin/viewcountry/' + country.countryId;


      if (country.countryId) {
        // alert(country.countryId);

        $localStorage.country = country;
        //


      }
    };

    //view country

    if ($state.current.name === 'admin.viewcountry') {
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      loadingService.isLoadin = false;
      $scope.country = {};
      // $scope. city.imageName ="nothing";
      if ($stateParams.countryId !== '0') {
        $scope.country = $localStorage.country;
      }


    }


    $scope.ViewCity = function(city) {
      loadingService.isLoadin = true;

      console.log(city);
      $window.location.href = '#/admin/viewcity/' + city.cityId;


      if (city.cityId) {
        // alert(city.cityId);

        $localStorage.city = city;
        //


      }
    };


    //view city

    if ($state.current.name === 'admin.viewcity') {
      loadingService.isLoadin = false;
      $scope.city = {};
      // $scope. city.imageName ="nothing";
      if ($stateParams.cityId !== '0') {
        $scope.city = $localStorage.city;
      }


    }



    //counrty manage
    $scope.countrymanage = function(country) {
      loadingService.isLoadin = true;
      console.log(country);
      $window.location.href = '#/admin/addcountry/' + country.countryId;


      if (country.countryId) {
        // alert(country.countryId);

        $localStorage.country = country;
        //


      }
    };


    $scope.citymanage = function(city) {
      loadingService.isLoadin = true;
      console.log(city);
      $window.location.href = '#/admin/addcity/' + city.cityId;


      if (city.cityId) {
        // alert(city.cityId);

        $localStorage.city = city;
        //


      }
    };




    //add city

    // if ($state.current.name === 'admin.addcity') {
    //   if (!$localStorage.token || $localStorage.token === 0) {
    //     $window.location.href = '#/login';
    //   }
    //   // get country
    //   $scope.getcountry = function(country) {
    //     loadingService.isLoadin = false;
    //     storeService.getactivecountries(country).then(function(response) {
    //       loadingService.isLoadin = false;
    //       console.log(response);
    //       if (response.errorCode === 0) {
    //         $scope.countries = response.object;
    //       } else {
    //         loadingService.isLoadin = false;

    //       }
    //     }, function() {
    //       loadingService.isLoadin = false;
    //     });
    //   };
    //   $scope.getcountry();
    //   $scope.city = {};
    //   if ($stateParams.cityId !== '0') {
    //     $scope.city = $localStorage.city;
    //   }

    //   $scope.addCity = function(getAddCityDetails) {
    //     loadingService.isLoadin = true;
    //     console.log(getAddCityDetails);
    //     storeService.submitAddCityDetails(getAddCityDetails).then(function(response) {
    //       loadingService.isLoadin = false;
    //       console.log(response);
    //       if (response.data.errorCode === 0) {
    //         if ($stateParams.cityId == '0') {
    //           toaster.pop('success', "Successfull..!", "Added New City!");
    //         } else {
    //           toaster.pop('success', "Successfull..!", "Updated  City!");
    //         }

    //         $window.location.href = '#/admin/city';
    //       } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
    //         //  console.log( );
    //         $window.location.href = '#/login';

    //       } else {
    //         // alert(response.data.errorMessage);
    //         toaster.pop('error', "Oops..!", response.data.errorMessage);
    //       }

    //     });
    //   }
    // }

    // add country


    if ($state.current.name === 'admin.addcountry') {
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
      $scope.country = {};

      // $scope.country.countryFlag = 'https://www.iconfinder.com/icons/22899/flag_icon';
      $scope.upload = function(file) {
        if (file) {
          $scope.isFileuploadStart = true;
          $scope.clickimg = true;
          console.log(file);
          Upload.upload({
            url: MAIN_CONSTANT.site_url + "/api/pentalupload",
            headers: {
              "Content-Type": 'multipart/*',
              Bearer: $localStorage.token
            },
            /*data: { file: FILE_CONSTANT.site_url + file.name,
              type: 'image/jpeg' }*/
            data: {
              file: file
            }
          }).then(function(resp) {
            console.log(resp);
            //$scope.product.featureOrPhotos[lastPush-1].data= resp.data.object;
            if (resp.data.errorCode === 0) {
              $scope.country.countryFlag = resp.data.object;
              $scope.isFileuploadStart = false;
              console.log($scope.country.countryFlag);
            } else if (resp.data.errorCode === 45 || resp.data.errorCode === 34) {
              $localStorage.token = 0;
              $window.location.href = '#/login';
            } else {

            }

          }, function(resp) {
            $scope.progressPercentage = 0;
            // for (var i = 0; i < $scope.country.featureOrPhotos.length; i++) {
            if ($scope.country.countryFlag) {
              //splice i
              $scope.country.countryFlag;

            }

            console.log('Error status: ' + resp.status);


          }, function(evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            /// $scope.product.featureOrPhotos[i].progress=$scope.progressPercentage;
            console.log('progress: ' + $scope.progressPercentage + '% ' + evt.config.data.file.name);
            // $scope.file = null;
            if ($scope.progressPercentage < 100 && $scope.progressPercentage != 0) {
              $scope.filecmplt = false;
            } else if ($scope.progressPercentage == 100) {
              $scope.filecmplt = true;

            } else {
              $scope.filenotUplaod = true;
            }
          });
        }
      };


      // $scope. country.countryFlag ='1';
      if ($stateParams.countryId !== '0') {
        $scope.country = $localStorage.country;
        $scope.coid = 1;
      } else {
        $scope.coid = 0;
      }


      $scope.deleteimage = function() {
        $scope.country.countryFlag = "";

      }

      $scope.addCountry = function(getAddCountryDetails) {
        loadingService.isLoadin = true;

        console.log(getAddCountryDetails);

           if (!getAddCountryDetails.countryName) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Country Name In English");
          } else {
            toaster.pop('info', "Please Enter Country Name In English");
          }

        } else if (!getAddCountryDetails.countryNameAr) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Country Name In Arabic");
          } else {
            toaster.pop('info', "Please Enter Country Name In Arabic");
          }


        } else if (!getAddCountryDetails.currency) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter Currency");
          } else {
            toaster.pop('info', "Please Enter Currency");
          }


        }  else if (!getAddCountryDetails.countryFlag) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Upload Flag");
          } else {
            toaster.pop('info', "Please Upload Flag");
          }


        } else if (getAddCountryDetails.countryStatus != 0 && getAddCountryDetails.countryStatus != 1) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Choose Status");
          } else {
            toaster.pop('info', "Please Choose Status");
          }

        }else{
                storeService.submitAddCountryDetails(getAddCountryDetails).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);


          if (response.data.errorCode === 0) {
            if ($stateParams.countryId == '0') {
              // toastrMsg(1,'Added new Category!','Successfull..!');
              // $scope.pop = function(){
              toaster.pop('success', "Successfull..!", "Added New Country!");
              // toaster.pop('success', "Added new Category!", "Successfull..!");
              // };
            } else {
              toaster.pop('success', "Successfull..!", "Updated  Country!");

            }

            $window.location.href = '#/admin/country';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
            toaster.pop('error', "Oops..!", response.data.errorMessage);
          }

        });

        }

  

      }


    }
    // add city


    if ($state.current.name === 'admin.addcity') {
      if (!$localStorage.token || $localStorage.token === 0) {
        $window.location.href = '#/login';
      }
         $scope.getcountry = function(country) {
        loadingService.isLoadin = false;
        storeService.getactivecountries(country).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);
          if (response.errorCode === 0) {
            $scope.countries = response.object;
          } else {
            loadingService.isLoadin = false;

          }
        }, function() {
          loadingService.isLoadin = false;
        });
      };
      $scope.getcountry();
      $scope.city = {};

      // $scope. country.countryFlag ='1';
      if ($stateParams.cityId !== '0') {
        $scope.city = $localStorage.city;
        $scope.ciid = 1;
      } else {
        $scope.ciid = 0;
      }

      $scope.addCity = function(getAddCityDetails) {
        loadingService.isLoadin = true;

        console.log(getAddCityDetails);

            if (!getAddCityDetails.countryId) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Select Country");
          } else {
            toaster.pop('info', "Please Select Country");
          }

        } else if (!getAddCityDetails.cityName) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter City Name In English");
          } else {
            toaster.pop('info', "Please Enter Category Name In English");
          }


        } else if (!getAddCityDetails.cityNameAr) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Enter City Name In Arabic");
          } else {
            toaster.pop('info', "Please Enter City Name In Arabic");
          }


        }  else if (getAddCityDetails.citusStatus != 0 && getAddCityDetails.citusStatus != 1) {
          if ($scope.langval == 1) {
            toaster.pop('info', "Please Choose Status");
          } else {
            toaster.pop('info', "Please Choose Status");
          }

        }else{

                 storeService.submitAddCityDetails(getAddCityDetails).then(function(response) {
          loadingService.isLoadin = false;
          console.log(response);

          if (response.data.errorCode === 0) {
            if ($stateParams.cityId == '0') {
              // toastrMsg(1,'Added new Category!','Successfull..!');
              // $scope.pop = function(){
              toaster.pop('success', "Successfull..!", "Added New City!");
              // toaster.pop('success', "Added new Category!", "Successfull..!");
              // };
            } else {
              toaster.pop('success', "Successfull..!", "Updated  City!");

            }

            $window.location.href = '#/admin/city';
          } else if (response.data.errorCode === 45 || response.data.errorCode === 34) {
            //  console.log( );
            $window.location.href = '#/login';

          } else {
            // alert(response.data.errorMessage);
            toaster.pop('error', "Oops..!", response.data.errorMessage);
          }

        });


        }

 


      }


    }


    $scope.changeCountry = function(countryId) {
      console.log(countryId);


      $scope.getcityByCountryId(countryId)

    };
    $scope.getcityByCountryId = function(countryId) {

      $scope.cityPageVo = {
        "countryId": countryId,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
        "cityName": "",
        "status": []
      }
      $scope.citiesbycountries = [];

      storeService.getcitiesByCountryId($scope.cityPageVo, countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
    // $scope.getcityByCountryId();







    $scope.changeCountry = function(countryId) {
      console.log(countryId);


      $scope.getcityByCountryId(countryId)

    };
    $scope.getcityByCountryId = function(countryId) {

      $scope.cityPageVo = {
        "countryId": countryId,
        "cityId": 0,
        "rowPerPage": 100,
        "currentIndex": 0,
        "cityName": "",
        "status": []
      }
      $scope.citiesbycountries = [];

      storeService.getcitiesByCountryId($scope.cityPageVo, countryId).then(function(response) {
        console.log(response);
        if (response.errorCode === 0) {
          $scope.citiesbycountries = response.object;
          console.log($scope.citiesbycountries);
        }
      });
    };
    // $scope.getcityByCountryId();




  });
