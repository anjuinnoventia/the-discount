package com.innoventiaProject.discount.model.shifting;

import java.util.List;

public class SaveShiftCloseModel{

	/**
	 * 
	 */
	public double creditSale;
	public double diffAmt;
	public long empId;
	public List<CounterInfo> counterList;
	
	
	
	public double getCreditSale() {
		return creditSale;
	}
	public void setCreditSale(double creditSale) {
		this.creditSale = creditSale;
	}
	public double getDiffAmt() {
		return diffAmt;
	}
	public void setDiffAmt(double diffAmt) {
		this.diffAmt = diffAmt;
	}
	public List<CounterInfo> getCounterList() {
		return counterList;
	}
	public void setCounterList(List<CounterInfo> counterList) {
		this.counterList = counterList;
	}
	public long getEmpId() {
		return empId;
	}
	public void setEmpId(long empId) {
		this.empId = empId;
	}
	
	
}
