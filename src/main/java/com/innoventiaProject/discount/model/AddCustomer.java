package com.innoventiaProject.discount.model;

import javax.persistence.Column;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.innoventiaProject.discount.constants.Constants;

public class AddCustomer implements Constants
{
	
public long customerId;
@NotEmpty(message="customer name field is empty")
public String customerName="";
//@NotNull
//@Pattern(regexp=REGEX_EMAIL,message="Email format is incorrect")
public String emailId="";
@NotNull
@Pattern(regexp=REGEX_PHONE)
public String phoneNo="";
public long userId;
public String createdDate="";
@NotNull
@NotEmpty
//@Pattern(regexp=REGEX_PASSWORD,message="password should be minimum eight characters, at least one letter and one number and no special charachtres")
public String password="";
public String oldPassword="";
@Min(value=1,message="country id should be greater than zero")
public long country;
@Min(value=1,message="city id should be greater than zero")
public long city;
@NotEmpty(message="address field is empty")
@Pattern(regexp=REGEX_NOTE,message="Address field exceeded the limit of 250")
public String address="";
public int Status;
@NotNull(message="must provide longitude")
public double longitude;
@NotNull(message="must provide latitude")
public double latitude;
public String profileImage="";
private double totalPay;
public String countryName;
public String cityName;
public long companyId;
public String customerNameAr;
public String addressAr;
public String cityNameAr;
public String countryNameAr;
public String zipCode;
private int isSpecial;
public int countryCode;
public int validate;




public int getValidate() {
	return validate;
}
public void setValidate(int validate) {
	this.validate = validate;

}


public int getCountryCode() {
	return countryCode;
}
public void setCountryCode(int countryCode) {
	this.countryCode = countryCode;

}
public int getIsSpecial() {
	return isSpecial;
}
public void setIsSpecial(int isSpecial) {
	this.isSpecial = isSpecial;
}
public String getZipCode() {
	return zipCode;
}
public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
}
public String getCityNameAr() {
	return cityNameAr;
}
public void setCityNameAr(String cityNameAr) {
	this.cityNameAr = cityNameAr;
}
public String getCountryNameAr() {
	return countryNameAr;
}
public void setCountryNameAr(String countryNameAr) {
	this.countryNameAr = countryNameAr;
}
public String getAddressAr() {
	return addressAr;
}
public void setAddressAr(String addressAr) {
	this.addressAr = addressAr;
}
public String getCustomerNameAr() {
	return customerNameAr;
}
public void setCustomerNameAr(String customerNameAr) {
	this.customerNameAr = customerNameAr;
}
public long getCompanyId() {
	return companyId;
}
public void setCompanyId(long companyId) {
	this.companyId = companyId;
}
public int getStatus() {
	return Status;
}
public void setStatus(int status) {
	Status = status;
}




public String getCountryName() {
	return countryName;
}
public void setCountryName(String countryName) {
	this.countryName = countryName;
}
public String getCityName() {
	return cityName;
}
public void setCityName(String cityName) {
	this.cityName = cityName;
}

public double getTotalPay() {
	return totalPay;
}
public void setTotalPay(double totalPay) {
	this.totalPay = totalPay;
}





public String getProfileImage() {
	return profileImage;
}
public void setProfileImage(String profileImage) {
	this.profileImage = profileImage;
}
public double getLongitude() {
	return longitude;
}
public void setLongitude(double longitude) {
	this.longitude = longitude;
}
public double getLatitude() {
	return latitude;
}
public void setLatitude(double latitude) {
	this.latitude = latitude;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public long getCity() {
	return city;
}
public void setCity(long city) {
	this.city = city;
}
public long getCountry() {
	return country;
}
public void setCountry(long country) {
	this.country = country;
}
public long getCustomerId() {
	return customerId;
}
public void setCustomerId(long customerId) {
	this.customerId = customerId;
}
public String getCustomerName() {
	return customerName;
}
public void setCustomerName(String customerName) {
	this.customerName = customerName;
}


public String getEmailId() {
	return emailId;
}
public void setEmailId(String emailId) {
	this.emailId = emailId;
}
public String getPhoneNo() {
	return phoneNo;
}
public void setPhoneNo(String phoneNo) {
	this.phoneNo = phoneNo;
}
public long getUserId() {
	return userId;
}
public void setUserId(long userId) {
	this.userId = userId;
}
public String getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(String createdDate) {
	this.createdDate = createdDate;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getOldPassword() {
	return oldPassword;
}
public void setOldPassword(String oldPassword) {
	this.oldPassword = oldPassword;
}










}
