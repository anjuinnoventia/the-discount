package com.innoventiaProject.discount.model;
/*package com.innoventiaProject.discount.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.innoventiaProject.discount.constants.Constants;

public class SellerDetailsVo implements Constants
{
	public long sellerId;
	@NotNull(message="seller name is null")
	@NotEmpty(message="seller name is empty")
	@Pattern(regexp=REGEX_NAME,message="Name format is incorrect")
	public String sellerName="";
	@NotNull(message="seller name is null")
	@NotEmpty(message="email value is empty")
	@Pattern(regexp=REGEX_EMAIL,message="Email format is incorrect")
	public String email="";
	@NotNull(message="seller name is null")
	@NotEmpty(message="phone number value is empty")
	@Pattern(regexp=REGEX_PHONE,message="Phone foramt is incorrect")
	public String phoneNo="";
	public String address="";
	public long lastUpdatedTime;
	public long operatingOfficerId;
	public double latitude;
	public double longitude;
	@Min(value=0,message="status minimum value is 0 and maximum is 1")
	@Max(value=1,message="status minimum value is 0 and maximum is 1")
	public int status;
	@NotNull(message="profile image is null")
	@NotEmpty(message="profile image is empty")
	public String profileImage="";
	public String webAddress="";
	public long userId;

	@Pattern(regexp=REGEX_PASSWORD,message="password should be minimum eight characters, at least one letter and one number and no special charachtres")
	public String password="";
	public String oldPassword="";
    public long countryId;
	public long cityId;
	
	
	
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getWebAddress() {
		return webAddress;
	}
	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public long getSellerId() {
		return sellerId;
	}
	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}
	
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
	
	
	
	
}
*/