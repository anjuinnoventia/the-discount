package com.innoventiaProject.discount.model.order;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.innoventiaProject.discount.constants.Constants;

public class AuctionDetailsVo implements Constants
{
	@NotNull(message="")
	@Min(value=0,message="")
	private long auctionId;
	@NotNull(message="")
	@Min(value=0,message="")
	private long userId;;
	@NotNull(message="")
	@Pattern(regexp=REGEX_PRICE,message="")
	private double bidAmount;
	private double lastBidAmount;
	private int status;
	private long lastUpdatedTime;
	private String lastUpdatedTimeString;
	
	
	
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public String getLastUpdatedTimeString() {
		return lastUpdatedTimeString;
	}
	public void setLastUpdatedTimeString(String lastUpdatedTimeString) {
		this.lastUpdatedTimeString = lastUpdatedTimeString;
	}
	public long getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(long auctionId) {
		this.auctionId = auctionId;
	}
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public double getBidAmount() {
		return bidAmount;
	}
	public void setBidAmount(double bidAmount) {
		this.bidAmount = bidAmount;
	}
	public double getLastBidAmount() {
		return lastBidAmount;
	}
	public void setLastBidAmount(double lastBidAmount) {
		this.lastBidAmount = lastBidAmount;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
