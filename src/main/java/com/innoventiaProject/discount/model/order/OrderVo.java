package com.innoventiaProject.discount.model.order;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.innoventiaProject.entity.order.ShippingDetails;
import com.innoventiaProject.entity.order.Transactions;


public class OrderVo {
	
	private long orderId;
	private long lastUpdatedTime;
	private int status;
	private int modeId;
	private long operatingOfficerId;
	private long sellerId;
	private long customerId;
	private String description="";
	private long deliveryDate;
	private String emailId;
	private double tax;
	private long orderStatusUpdateTime;
	private long transaction_id;
	private long  pickUpTime;
	private long  taxId;
	private long expectedDeliveryDate;
	private ShippingDetails shippingDetails;
	private long shippingId;
	@NotNull(message="netprice should not be null")
	private double netPrice;
	private double latitude;
	private double longitude;
	private long numberOfItems;
	private int orderStatus;
	private long  deliveryBoyId;
	@NotNull(message="order details is null")
	@NotEmpty(message="order details is empty")
	private List<OrderDetailsVo> orderDetailsVo=new ArrayList<>();
	private TransactionModel transaction;
	private String customerName="";
	private String customerNameAr="";
	private String phoneNumber="";
	private String address="";
	private String storeNumber="";
	private String storeName="";
	private String orderStatusName;
	private String orderStatusNameAr;
	public long companyId;
	private int orderstatus;
	private String sellerName;
	private String sellerNameAr;
	private String sellerPhoneNumber;
	private String TaxName;
	private String TaxNameAr;
	private Double taxPercentage;
	private int cod;
	private int online;
	private String promocode;
	private long codeId;
	private double minimumAmount;
	private long promocodeId;
	private long validFrom;
	private long validTo;
	private double flatOffer;
	private long percentage;
	private double promoReportPrice;
	private double flatOffer1;
	private long percentage1;
	
	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public double getFlatOffer1() {
		return flatOffer1;
	}
	public void setFlatOffer1(double flatOffer1) {
		this.flatOffer1 = flatOffer1;
	}
	public long getPercentage1() {
		return percentage1;
	}
	public void setPercentage1(long percentage1) {
		this.percentage1 = percentage1;
	}
	public double getPromoReportPrice() {
		return promoReportPrice;
	}
	public void setPromoReportPrice(double promoReportPrice) {
		this.promoReportPrice = promoReportPrice;
	}
	public long getPromocodeId() {
		return promocodeId;
	}
	public void setPromocodeId(long promocodeId) {
		this.promocodeId = promocodeId;
	}
	public long getPercentage() {
		return percentage;
	}
	public void setPercentage(long percentage) {
		this.percentage = percentage;
	}
	public double getMinimumAmount() {
		return minimumAmount;
	}
	public void setMinimumAmount(double minimumAmount) {
		this.minimumAmount = minimumAmount;
	}
	public double getFlatOffer() {
		return flatOffer;
	}
	public void setFlatOffer(double flatOffer) {
		this.flatOffer = flatOffer;
	}
	public long getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(long validFrom) {
		this.validFrom = validFrom;
	}
	public long getValidTo() {
		return validTo;
	}
	public void setValidTo(long validTo) {
		this.validTo = validTo;
	}
	public long getCodeId() {
		return codeId;
	}
	public void setCodeId(long codeId) {
		this.codeId = codeId;
	}
	public String getPromocode() {
		return promocode;
	}
	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}
	public int getCod() {
		return cod;
	}
	public void setCod(int cod) {
		this.cod = cod;
	}
	public int getOnline() {
		return online;
	}
	public void setOnline(int online) {
		this.online = online;
	}
	public String getOrderStatusNameAr() {
		return orderStatusNameAr;
	}
	public void setOrderStatusNameAr(String orderStatusNameAr) {
		this.orderStatusNameAr = orderStatusNameAr;
	}
	public Double getTaxPercentage() {
		return taxPercentage;
	}
	public void setTaxPercentage(Double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}
	public String getTaxName() {
		return TaxName;
	}
	public void setTaxName(String taxName) {
		TaxName = taxName;
	}
	public String getTaxNameAr() {
		return TaxNameAr;
	}
	public void setTaxNameAr(String taxNameAr) {
		TaxNameAr = taxNameAr;
	}
	public long getTaxId() {
		return taxId;
	}
	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}
	public String getSellerPhoneNumber() {
		return sellerPhoneNumber;
	}
	public void setSellerPhoneNumber(String sellerPhoneNumber) {
		this.sellerPhoneNumber = sellerPhoneNumber;
	}
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public String getSellerNameAr() {
		return sellerNameAr;
	}
	public void setSellerNameAr(String sellerNameAr) {
		this.sellerNameAr = sellerNameAr;
	}
	public String getCustomerNameAr() {
		return customerNameAr;
	}
	public void setCustomerNameAr(String customerNameAr) {
		this.customerNameAr = customerNameAr;
	}
	public int getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(int orderstatus) {
		this.orderstatus = orderstatus;
	}
	public ShippingDetails getShippingDetails() {
		return shippingDetails;
	}
	public void setShippingDetails(ShippingDetails shippingDetails) {
		this.shippingDetails = shippingDetails;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public long getOrderStatusUpdateTime() {
		return orderStatusUpdateTime;
	}
	public void setOrderStatusUpdateTime(long orderStatusUpdateTime) {
		this.orderStatusUpdateTime = orderStatusUpdateTime;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStoreNumber() {
		return storeNumber;
	}
	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getOrderStatusName() {
		return orderStatusName;
	}
	public void setOrderStatusName(String orderStatusName) {
		this.orderStatusName = orderStatusName;
	}
	public long getShippingId() {
		return shippingId;
	}
	public void setShippingId(long shippingId) {
		this.shippingId = shippingId;
	}
	public int getModeId() {
		return modeId;
	}
	public void setModeId(int modeId) {
		this.modeId = modeId;
	}
	
	public TransactionModel getTransaction() {
		return transaction;
	}
	public void setTransaction(TransactionModel transaction) {
		this.transaction = transaction;
	}
	public long getTransaction_id() {
		return transaction_id;
	}
	public void setTransaction_id(long transaction_id) {
		this.transaction_id = transaction_id;
	}
	public long getPickUpTime() {
		return pickUpTime;
	}
	public void setPickUpTime(long pickUpTime) {
		this.pickUpTime = pickUpTime;
	}
	
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	
	public long getSellerId() {
		return sellerId;
	}
	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(long deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public long getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}
	public void setExpectedDeliveryDate(long expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	public double getNetPrice() {
		return netPrice;
	}
	public void setNetPrice(double netPrice) {
		this.netPrice = netPrice;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public long getNumberOfItems() {
		return numberOfItems;
	}
	public void setNumberOfItems(long numberOfItems) {
		this.numberOfItems = numberOfItems;
	}
	public int getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}
	public List<OrderDetailsVo> getOrderDetailsVo() {
		return orderDetailsVo;
	}
	public void setOrderDetailsVo(List<OrderDetailsVo> orderDetailsVo) {
		this.orderDetailsVo = orderDetailsVo;
	}
	public long getDeliveryBoyId() {
		return deliveryBoyId;
	}
	public void setDeliveryBoyId(long deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}
	
	
	
	
	
	
	
	
	
}
