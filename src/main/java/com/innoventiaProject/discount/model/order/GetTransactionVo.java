package com.innoventiaProject.discount.model.order;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class GetTransactionVo {
	@NotNull(message="row per page is null")
	@Min(value=1,message="minimum row per page should be 1")
	private int rowPerPage;
	@NotNull(message="row per page is null")
	@Min(value=0,message="minimum current index should be 0")
	private int currentIndex;
	private long fromDate;
	private long toDate;
	private long codeId;
	private int month;
	private double monthlyTotal;
	private long monthyDate;
	private long time;
	private int year;
	private int week;
	
	
	public int getWeek() {
		return week;
	}
	public void setWeek(int week) {
		this.week = week;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public double getMonthlyTotal() {
		return monthlyTotal;
	}
	public void setMonthlyTotal(double monthlyTotal) {
		this.monthlyTotal = monthlyTotal;
	}
	public long getMonthyDate() {
		return monthyDate;
	}
	public void setMonthyDate(long monthyDate) {
		this.monthyDate = monthyDate;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	public long getFromDate() {
		return fromDate;
	}
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	public long getToDate() {
		return toDate;
	}
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	public long getCodeId() {
		return codeId;
	}
	public void setCodeId(long codeId) {
		this.codeId = codeId;
	}
	
	
	
	
}
