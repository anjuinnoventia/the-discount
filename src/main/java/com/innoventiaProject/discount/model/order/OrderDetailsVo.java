package com.innoventiaProject.discount.model.order;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.innoventiaProject.discount.model.input.ProductVo;
import com.innoventiaProject.discount.model.input.SubCategoryVo;
import com.innoventiaProject.entity.input.Product;
import com.innoventiaProject.entity.order.Order;

public class OrderDetailsVo 
{

	private long orderListId;
	private long orderId;
	private long itemQuantity;
	private String itemName;
	private long itemPrice;
	private long productId;
	private long offerId;
	private double itemNetPrice;
	private int status;
	private long lastUpdatedTime;
	private long operatingOfficerId;
	private String description;
	private String returnTime;
	private long deliveryDate;
	private double tax;
	private long totalCount;
	private long taxId;
	private String taxName;
	private String taxNameAr;
	private Double taxPercentage;
	private long totalQtyPurchased;
	private long purchaseProductId;
	private double totalAmountIncludingOffer;
	private double shippingCharge;
	private double itemPurchasePrice;
	private String offerName;
	private String offerNameAr;
	private double offerRate;
	private long offerPercentage;
	private String itemNameAr;
	private String itemStatusName;
	private String itemStatusNameAr;
	private String cancellationDate;
	private double installationCharge;
	private double totalTax;
	private int totalNumberSaled;
	
	
	
	public int getTotalNumberSaled() {
		return totalNumberSaled;
	}
	public void setTotalNumberSaled(int totalNumberSaled) {
		this.totalNumberSaled = totalNumberSaled;
	}
	public double getTotalTax() {
		return totalTax;
	}
	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}
	public double getInstallationCharge() {
		return installationCharge;
	}
	public void setInstallationCharge(double installationCharge) {
		this.installationCharge = installationCharge;
	}
	public String getCancellationDate() {
		return cancellationDate;
	}
	public void setCancellationDate(String cancellationDate) {
		this.cancellationDate = cancellationDate;
	}
	public String getItemStatusName() {
		return itemStatusName;
	}
	public void setItemStatusName(String itemStatusName) {
		this.itemStatusName = itemStatusName;
	}
	public String getItemStatusNameAr() {
		return itemStatusNameAr;
	}
	public void setItemStatusNameAr(String itemStatusNameAr) {
		this.itemStatusNameAr = itemStatusNameAr;
	}
	public String getItemNameAr() {
		return itemNameAr;
	}
	public void setItemNameAr(String itemNameAr) {
		this.itemNameAr = itemNameAr;
	}
	public String getOfferName() {
		return offerName;
	}
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	public String getOfferNameAr() {
		return offerNameAr;
	}
	public void setOfferNameAr(String offerNameAr) {
		this.offerNameAr = offerNameAr;
	}
	public double getOfferRate() {
		return offerRate;
	}
	public void setOfferRate(double offerRate) {
		this.offerRate = offerRate;
	}
	public long getOfferPercentage() {
		return offerPercentage;
	}
	public void setOfferPercentage(long offerPercentage) {
		this.offerPercentage = offerPercentage;
	}
	public double getItemPurchasePrice() {
		return itemPurchasePrice;
	}
	public void setItemPurchasePrice(double itemPurchasePrice) {
		this.itemPurchasePrice = itemPurchasePrice;
	}
	public double getShippingCharge() {
		return shippingCharge;
	}
	public void setShippingCharge(double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}
	public double getTotalAmountIncludingOffer() {
		return totalAmountIncludingOffer;
	}
	public void setTotalAmountIncludingOffer(double totalAmountIncludingOffer) {
		this.totalAmountIncludingOffer = totalAmountIncludingOffer;
	}
	public Double getTaxPercentage() {
		return taxPercentage;
	}
	public void setTaxPercentage(Double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}
	public String getTaxNameAr() {
		return taxNameAr;
	}
	public void setTaxNameAr(String taxNameAr) {
		this.taxNameAr = taxNameAr;
	}
	public String getTaxName() {
		return taxName;
	}
	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}
	public long getTaxId() {
		return taxId;
	}
	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}
	public long getOrderListId() {
		return orderListId;
	}
	public void setOrderListId(long orderListId) {
		this.orderListId = orderListId;
	}
	public long getOrderId() {
		return orderId;
	}
	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public long getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(long itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public long getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(long itemPrice) {
		this.itemPrice = itemPrice;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public long getOfferId() {
		return offerId;
	}
	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}
	public double getItemNetPrice() {
		return itemNetPrice;
	}
	public void setItemNetPrice(double itemNetPrice) {
		this.itemNetPrice = itemNetPrice;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getReturnTime() {
		return returnTime;
	}
	public void setReturnTime(String returnTime) {
		this.returnTime = returnTime;
	}
	public long getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(long deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	
	
	
	public long companyId;
	private int rowPerPage;
	private int currentIndex;
	private long fromDate;
	private long toDate;
	private long customerId;
	private long categoryId;
	private long subCategoryId;
	private long sellerId;








	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public long getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public long getSellerId() {
		return sellerId;
	}
	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	public long getFromDate() {
		return fromDate;
	}
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	public long getToDate() {
		return toDate;
	}
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	
	
	










	public long getTotalQtyPurchased() {
		return totalQtyPurchased;
	}
	public void setTotalQtyPurchased(long totalQtyPurchased) {
		this.totalQtyPurchased = totalQtyPurchased;
	}
	public long getPurchaseProductId() {
		return purchaseProductId;
	}
	public void setPurchaseProductId(long purchaseProductId) {
		this.purchaseProductId = purchaseProductId;
	}
	
	 
	private String categoryName;
	private String subCategoryName;










	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	
	
	
	
	
	
	
	
	
	
	
	/*private long orderId;
	private long orderListId;
	
	
	@NotNull(message="item name is null")
	@NotEmpty(message="item name is empty")
	private String itemName="";
	
	@Min(value=1,message="item price should be greter than 0")
	private double itemPrice;
	
	@Min(value=1,message="item id should be minimum 1")
	private long productId;
	private double itemNetPrice;
	
	private int status;
	private long offerId;
	private long lastUpdatedTime;
	private long returnTime;
	private long operatingOfficerId;
	private String description="";
	private long deliveryDate;
	private String imageName;
	private double tax;
	@Min(value=1,message="seller id should be minimum 1")
	private long sellerId;
	private int modeId;
	private String image;
	public long companyId;
	@Min(value=1,message="item quantity should be greater than one")
	private long itemQuantity;
	private long taxId;
	private double totalAmountIncludingOffer;
	private double totalAmount;
	private long totalQtyPurchased;
	private long purchaseProductId;
	private int rowPerPage;
	private int currentIndex;
	private long orderDetailsId;
	private long fromDate;
	private long toDate;
	private long customerId;
	//private List<ProductVo> subCategoryVos;
	private long categoryId;
	private long subCategoryId;
	private String categoryName;
	private String subCategoryName;
	private long totalCount;
	
	
	
	
	public long getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getSubCategoryName() {
		return subCategoryName;
	}
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public long getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public List<ProductVo> getSubCategoryVos() {
		return subCategoryVos;
	}
	public void setSubCategoryVos(List<ProductVo> subCategoryVos) {
		this.subCategoryVos = subCategoryVos;
	}
	public long getFromDate() {
		return fromDate;
	}
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	public long getToDate() {
		return toDate;
	}
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	public long getOrderDetailsId() {
		return orderDetailsId;
	}
	public void setOrderDetailsId(long orderDetailsId) {
		this.orderDetailsId = orderDetailsId;
	}
	public long getTotalQtyPurchased() {
		return totalQtyPurchased;
	}
	public void setTotalQtyPurchased(long totalQtyPurchased) {
		this.totalQtyPurchased = totalQtyPurchased;
	}
	public long getPurchaseProductId() {
		return purchaseProductId;
	}
	public void setPurchaseProductId(long purchaseProductId) {
		this.purchaseProductId = purchaseProductId;
	}
	public double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public double getTotalAmountIncludingOffer() {
		return totalAmountIncludingOffer;
	}
	public void setTotalAmountIncludingOffer(double totalAmountIncludingOffer) {
		this.totalAmountIncludingOffer = totalAmountIncludingOffer;
	}
	public long getTaxId() {
		return taxId;
	}
	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getModeId() {
		return modeId;
	}
	public void setModeId(int modeId) {
		this.modeId = modeId;
	}


	public String getImageName() {
		return imageName;
	}


	public void setImageName(String imageName) {
		this.imageName = imageName;
	}


	public long getOrderId() {
		return orderId;
	}
	
	
	public double getTax() {
		return tax;
	}


	public void setTax(double tax) {
		this.tax = tax;
	}


	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}
	public long getOrderListId() {
		return orderListId;
	}
	public void setOrderListId(long orderListId) {
		this.orderListId = orderListId;
	}
	
	public long getItemQuantity() {
		return itemQuantity;
	}


	public void setItemQuantity(long itemQuantity) {
		this.itemQuantity = itemQuantity;
	}


	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public double getItemPrice() {
		return itemPrice;
	}
	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}
	
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public double getItemNetPrice() {
		return itemNetPrice;
	}
	public void setItemNetPrice(double itemNetPrice) {
		this.itemNetPrice = itemNetPrice;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getOfferId() {
		return offerId;
	}
	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(long deliveryDate) {
		this.deliveryDate = deliveryDate;
	}



	public long getSellerId() {
		return sellerId;
	}


	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}


	public long getReturnTime() {
		return returnTime;
	}


	public void setReturnTime(long returnTime) {
		this.returnTime = returnTime;
	}
	*/
	
	
	
	
	
}
