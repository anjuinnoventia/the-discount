package com.innoventiaProject.discount.model.order;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class ShippingVo
{
	private long shippingId;
	private long lastUpdatedTime;
	private long countryId;
	private long cityId;
	private int status;
	@NotNull(message="address1 details is null")
	@NotEmpty(message="address1 details is empty")
	private String address1;
	@NotNull(message="address2 details is null")
	@NotEmpty(message="address2 details is empty")
	private String address2;
	@NotNull(message="pincode details is null")
	@NotEmpty(message="pincode details is empty")
	private String pincode;
	private String landMark;
	private long customerId;
	private String countryName;
	private String cityName;
	private double longitude;
	private double latitude;
	private String countryNameAr;
	private String cityNameAr;
	private String customerName;
	private String customerNameAr;
	private String address;
	private String addressAr;
	private String emailId;
	private String phoneNo;
	private String phoneNumber;
	private String landMarkAr;
	private String address2Ar;
	@NotNull(message="addressAr1 details is null")
	@NotEmpty(message="addressAr1 details is empty")
	private String addressAr1;
	private String toDeliver;
	private String deliveryName;
	
		
	
	
	
	
	
	
	
	public String getDeliveryName() {
		return deliveryName;
	}
	public void setDeliveryName(String deliveryName) {
		this.deliveryName = deliveryName;
	}
	public String getToDeliver() {
		return toDeliver;
	}
	public void setToDeliver(String toDeliver) {
		this.toDeliver = toDeliver;
	}
	public String getLandMarkAr() {
			return landMarkAr;
		}
		public void setLandMarkAr(String landMarkAr) {
			this.landMarkAr = landMarkAr;
		}
		public String getAddress2Ar() {
			return address2Ar;
		}
		public void setAddress2Ar(String address2Ar) {
			this.address2Ar = address2Ar;
		}
		public String getAddressAr1() {
			return addressAr1;
		}
		public void setAddressAr1(String addressAr1) {
			this.addressAr1 = addressAr1;
		}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerNameAr() {
		return customerNameAr;
	}
	public void setCustomerNameAr(String customerNameAr) {
		this.customerNameAr = customerNameAr;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddressAr() {
		return addressAr;
	}
	public void setAddressAr(String addressAr) {
		this.addressAr = addressAr;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getCountryNameAr() {
		return countryNameAr;
	}
	public void setCountryNameAr(String countryNameAr) {
		this.countryNameAr = countryNameAr;
	}
	public String getCityNameAr() {
		return cityNameAr;
	}
	public void setCityNameAr(String cityNameAr) {
		this.cityNameAr = cityNameAr;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public long getShippingId() {
		return shippingId;
	}
	public void setShippingId(long shippingId) {
		this.shippingId = shippingId;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getLandMark() {
		return landMark;
	}
	public void setLandMark(String landMark) {
		this.landMark = landMark;
	}
	
	

}
