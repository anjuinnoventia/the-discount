package com.innoventiaProject.discount.model.input;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.innoventiaProject.discount.constants.Constants;

public class CategoryVo implements Constants{

	@NotNull(message="category name is null")
	@NotEmpty(message="category name is empty")
	private String categoryName="";
	@NotNull(message="description is null")
	@NotEmpty(message="description is empty")
	private String discription="";
	private int status;
	@Min(value=0,message="category id should be equal to greater than zero")
	private long categoryId;
	private long subcategoryId;
	private long maincategoryId;
	private String discriptionAr;
	private String mainCategoryName;
	private long createdDate;
	private String lastUpdatedTimeString="";
	private long lastUpdatedTime;
	private String createdDateString="";
	private long userId;
	@NotNull(message="image name is null")
	@NotEmpty(message="image name is empty")
	private String imageName="";
	private int rank;
	private int isSub;
	@NotNull(message="category name in arabic is null")
	@NotEmpty(message="category name in arabic is empty")
	private String categoryNameAr;
	private double commision;
	private String color;
	private List<SubCategoryVo> subCategoryVos;
	public long companyId;
	
	
	
	
	
	public String getDiscriptionAr() {
		return discriptionAr;
	}
	public void setDiscriptionAr(String discriptionAr) {
		this.discriptionAr = discriptionAr;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public List<SubCategoryVo> getSubCategoryVos() {
		return subCategoryVos;
	}
	public void setSubCategoryVos(List<SubCategoryVo> subCategoryVos) {
		this.subCategoryVos = subCategoryVos;
	}
	private long operatingOfficerId;
	private String iconImage;
	
	
	
	
	public String getIconImage() {
		return iconImage;
	}
	public void setIconImage(String iconImage) {
		this.iconImage = iconImage;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getCommision() {
		return commision;
	}
	public void setCommision(double commision) {
		this.commision = commision;
	}
	public String getCategoryNameAr() {
		return categoryNameAr;
	}
	public void setCategoryNameAr(String categoryNameAr) {
		this.categoryNameAr = categoryNameAr;
	}
	public int getIsSub() {
		return isSub;
	}
	public void setIsSub(int isSub) {
		this.isSub = isSub;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public long getSubcategoryId() {
		return subcategoryId;
	}
	public void setSubcategoryId(long subcategoryId) {
		this.subcategoryId = subcategoryId;
	}
	public long getMaincategoryId() {
		return maincategoryId;
	}
	public void setMaincategoryId(long maincategoryId) {
		this.maincategoryId = maincategoryId;
	}
	
	public String getMainCategoryName() {
		return mainCategoryName;
	}
	public void setMainCategoryName(String mainCategoryName) {
		this.mainCategoryName = mainCategoryName;
	}
	public long getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getLastUpdatedTimeString() {
		return lastUpdatedTimeString;
	}
	public void setLastUpdatedTimeString(String lastUpdatedTimeString) {
		this.lastUpdatedTimeString = lastUpdatedTimeString;
	}
	public String getCreatedDateString() {
		return createdDateString;
	}
	public void setCreatedDateString(String createdDateString) {
		this.createdDateString = createdDateString;
	}
	
	
	
	
	
	
	
	
}
