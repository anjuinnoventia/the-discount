package com.innoventiaProject.discount.model.input;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.innoventiaProject.entity.input.Product;

/**
 * @author imraan
 *
 */
public class GetPageVo {
  
	@NotNull(message="row per page is null")
	@Min(value=1,message="minimum row per page should be 1")
	private int rowPerPage;
	@NotNull(message="row per page is null")
	@Min(value=0,message="minimum current index should be 0")
	private int currentIndex;
	private long productId;
	private int stat;
	private long shopId;
    private long orderDetailsId;
	private long subCategoryId;
	private long categoryId;
	private long userId;
	private long orderId;
	private long ratingPercentage;
	private int transactionMode;
	private long returnItemId;	
	private String returnReason;
	private String comments;
	private long shippingId;
	private long customerId;
	private int status[];
	private long modeIds[];
	private int tMode[];
	private int iNoR[];
	private int bankOrCash[];
	private long fromDate;
	private long toDate;
	private long auctionId;
	private long auctionDetaiilsId;
	private long countryId;
	private String countryName;
	private long cityId;
	private String cityName;
	private int citusStatus;
	private String productName;
	private String productNameAr;
	private long deliveryBoyId;
	private String address;
	private String deliveryBoyName;
	private String phoneNumber;
	public long sellerId;
	public String sellerName;
	public String sellerNameAr;
	public String email;
	private long storeId;
	private long deliveryDate;
	private long orderListId;
	private int  serviceOrProduct;	
	private long transactionId;
	private long taxId;
	private String taxName;	
	private int orderstatus;	
	private long companyId;	
	private long numberOfItemsRemaining;
	private long numberStarts;
	private long numberEnds;
	private String productImage;	
	private long offerId;
	private double offerRate;
	private long modeId;
	private int delivaryStatus;
	private long offerPercentage;
	private long offerRatePercentage;
	private String offerStarts;
	private String offerEnds;
	private double offerPrice;
	private long manufacturerId;
	private double priceStarts;
	private double priceEnds;
	private String value;
	private long productAttributeId;
	private long codeId;
	private long lastUpdatedTime;
	private int tranMode;
	private Product product;
	private long promoterId;
	private int data;
	public Map<Long, List<String>> attibutesSearch = new HashMap<Long, List<String>>();
	private double shippingCharge;
	private double taxPercentage;
	private double purchasePrice;
	private String itemNameAr;
	private int featured;
	private int rank;
	private long arabicOrEnglish;

	
	
	
	public int[] getBankOrCash() {
		return bankOrCash;
	}
	public void setBankOrCash(int[] bankOrCash) {
		this.bankOrCash = bankOrCash;
	}
	public int[] getiNoR() {
		return iNoR;
	}
	public void setiNoR(int[] iNoR) {
		this.iNoR = iNoR;
	}
	public long getArabicOrEnglish() {
		return arabicOrEnglish;
	}
	public void setArabicOrEnglish(long arabicOrEnglish) {
		this.arabicOrEnglish = arabicOrEnglish;
	}
	public long[] getModeIds() 
	{
		return modeIds;
	}
	public void setModeIds(long[] modeIds) 
	{
		this.modeIds = modeIds;
	}
	public int getFeatured() {
		return featured;
	}
	public void setFeatured(int featured) {
		this.featured = featured;
	}
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public double getTaxPercentage() {
		return taxPercentage;
	}
	public void setTaxPercentage(double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}
	public String getItemNameAr() {
		return itemNameAr;
	}
	public void setItemNameAr(String itemNameAr) {
		this.itemNameAr = itemNameAr;
	}
	public double getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	public double getShippingCharge() {
		return shippingCharge;
	}
	public void setShippingCharge(double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}
	public Map<Long, List<String>> getAttibutesSearch() {
		return attibutesSearch;
	}
	public void setAttibutesSearch(Map<Long, List<String>> attibutesSearch) {
		this.attibutesSearch = attibutesSearch;
	}
	public int getData() {
		return data;
	}
	public void setData(int data) {
		this.data = data;
	}
	public long getOfferRatePercentage() {
		return offerRatePercentage;
	}
	public void setOfferRatePercentage(long offerRatePercentage) {
		this.offerRatePercentage = offerRatePercentage;
	}
	public int[] gettMode() {
		return tMode;
	}
	public void settMode(int[] tMode) {
		this.tMode = tMode;
	}
	public String getSellerNameAr() {
		return sellerNameAr;
	}
	public void setSellerNameAr(String sellerNameAr) {
		this.sellerNameAr = sellerNameAr;
	}
	public long getPromoterId() {
		return promoterId;
	}
	public void setPromoterId(long promoterId) {
		this.promoterId = promoterId;
	}
	public long getRatingPercentage() {
		return ratingPercentage;
	}
	public void setRatingPercentage(long ratingPercentage) {
		this.ratingPercentage = ratingPercentage;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getTranMode() {
		return tranMode;
	}
	public void setTranMode(int tranMode) {
		this.tranMode = tranMode;
	}
	public int getTransactionMode() {
		return transactionMode;
	}
	public void setTransactionMode(int transactionMode) {
		this.transactionMode = transactionMode;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public String getProductNameAr() {
		return productNameAr;
	}
	public void setProductNameAr(String productNameAr) {
		this.productNameAr = productNameAr;
	}
	public long getCodeId() {
		return codeId;
	}
	public void setCodeId(long codeId) {
		this.codeId = codeId;
	}
	public long getNumberStarts() {
		return numberStarts;
	}
	public void setNumberStarts(long numberStarts) {
		this.numberStarts = numberStarts;
	}
	public long getNumberEnds() {
		return numberEnds;
	}
	public void setNumberEnds(long numberEnds) {
		this.numberEnds = numberEnds;
	}
	public long getProductAttributeId() {
		return productAttributeId;
	}
	public void setProductAttributeId(long productAttributeId) {
		this.productAttributeId = productAttributeId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public double getPriceStarts() {
		return priceStarts;
	}
	public void setPriceStarts(double priceStarts) {
		this.priceStarts = priceStarts;
	}
	public double getPriceEnds() {
		return priceEnds;
	}
	public void setPriceEnds(double priceEnds) {
		this.priceEnds = priceEnds;
	}
	public long getManufacturerId() {
		return manufacturerId;
	}
	public void setManufacturerId(long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}
	public String getOfferStarts() {
		return offerStarts;
	}
	public void setOfferStarts(String offerStarts) {
		this.offerStarts = offerStarts;
	}
	public String getOfferEnds() {
		return offerEnds;
	}
	public void setOfferEnds(String offerEnds) {
		this.offerEnds = offerEnds;
	}
	public double getOfferPrice() {
		return offerPrice;
	}
	public void setOfferPrice(double offerPrice) {
		this.offerPrice = offerPrice;
	}
	public long getOfferPercentage() {
		return offerPercentage;
	}
	public void setOfferPercentage(long offerPercentage) {
		this.offerPercentage = offerPercentage;
	}
	public int getDelivaryStatus() {
		return delivaryStatus;
	}
	public void setDelivaryStatus(int delivaryStatus) {
		this.delivaryStatus = delivaryStatus;
	}
	public long getModeId() {
		return modeId;
	}
	public void setModeId(long modeId) {
		this.modeId = modeId;
	}
	public double getOfferRate() {
		return offerRate;
	}
	public void setOfferRate(double offerRate) {
		this.offerRate = offerRate;
	}
	public long getOfferId() {
		return offerId;
	}
	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}
	public String getProductImage() {
		return productImage;
	}
	public void setProductImage(String productImage) {
		this.productImage = productImage;
	}
	public long getNumberOfItemsRemaining() {
		return numberOfItemsRemaining;
	}
	public void setNumberOfItemsRemaining(long numberOfItemsRemaining) {
		this.numberOfItemsRemaining = numberOfItemsRemaining;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public int getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(int orderstatus) {
		this.orderstatus = orderstatus;
	}
	public long getOrderDetailsId() {
		return orderDetailsId;
	}
	public void setOrderDetailsId(long orderDetailsId) {
		this.orderDetailsId = orderDetailsId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public long getShippingId() {
		return shippingId;
	}
	public void setShippingId(long shippingId) {
		this.shippingId = shippingId;
	}
	public long getTaxId() {
		return taxId;
	}
	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}
	public String getTaxName() {
		return taxName;
	}
	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}
	public long getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}
	public int getServiceOrProduct() {
		return serviceOrProduct;
	}
	public void setServiceOrProduct(int serviceOrProduct) {
		this.serviceOrProduct = serviceOrProduct;
	}
	public long getOrderListId() {
		return orderListId;
	}
	public void setOrderListId(long orderListId) {
		this.orderListId = orderListId;
	}
	public long getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(long deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public long getSellerId() {
		return sellerId;
	}
	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	
	public int getStat() {
		return stat;
	}
	public void setStat(int stat) {
		this.stat = stat;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getDeliveryBoyName() {
		return deliveryBoyName;
	}
	public void setDeliveryBoyName(String deliveryBoyName) {
		this.deliveryBoyName = deliveryBoyName;
	}
	public long getDeliveryBoyId() {
		return deliveryBoyId;
	}
	public void setDeliveryBoyId(long deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	public int getCitusStatus() {
		return citusStatus;
	}
	public void setCitusStatus(int citusStatus) {
		this.citusStatus = citusStatus;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public long getAuctionDetaiilsId() {
		return auctionDetaiilsId;
	}
	public void setAuctionDetaiilsId(long auctionDetaiilsId) {
		this.auctionDetaiilsId = auctionDetaiilsId;
	}
	public long getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(long auctionId) {
		this.auctionId = auctionId;
	}
	public long getFromDate() {
		return fromDate;
	}
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	public long getToDate() {
		return toDate;
	}
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	public int[] getStatus() {
		return status;
	}
	public void setStatus(int[] status) {
		this.status = status;
	}
	public long getOrderId() {
			return orderId;
		}
		public void setOrderId(long orderId) {
			this.orderId = orderId;
		}
		public long getStoreId() {
			return storeId;
		}
		public void setStoreId(long storeId) {
			this.storeId = storeId;
		}
		public long getCustomerId() {
			return customerId;
		}
		public void setCustomerId(long customerId) {
			this.customerId = customerId;
		}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public long getShopId() {
		return shopId;
	}
	public void setShopId(long shopId) {
		this.shopId = shopId;
	}
	public long getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getReturnItemId() {
		return returnItemId;
	}
	public void setReturnItemId(long returnItemId) {
		this.returnItemId = returnItemId;
	}
	public String getReturnReason() {
		return returnReason;
	}
	public void setReturnReason(String returnReason) {
		this.returnReason = returnReason;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	
}
