package com.innoventiaProject.discount.model.input;

import com.innoventiaProject.entity.input.Offer;
import com.innoventiaProject.entity.input.Product;

public class OfferDetailsVo
{
	private long offerDetailsId;
	private long productId;
	private long lastUpdatedTime;
	private int status;
	private Offer offer;
	private Product product;
	
	
	
	
	
	
	
	
	
		public Offer getOffer() {
		return offer;
	}
	public void setOffer(Offer offer) {
		this.offer = offer;
	}
		public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public long getOfferDetailsId() {
		return offerDetailsId;
	}
	public void setOfferDetailsId(long offerDetailsId) {
		this.offerDetailsId = offerDetailsId;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	
	
}
