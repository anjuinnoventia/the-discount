package com.innoventiaProject.discount.model.input;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class OfferVo {
	private long offerId;
	private String offerName;
	private String offerNameAr;
	private int status;
	private long lastUpdatedTime;
	private String descriptionAr;
	private String description;
	private String imageName;
	private long operatingOfficerId;
	private long offerEnds;
	private long offerStarts;
	@NotNull(message="row per page is null")
	@Min(value=1,message="minimum row per page should be 1")
	private int rowPerPage;
	@NotNull(message="row per page is null")
	@Min(value=0,message="minimum current index should be 0")
	private int currentIndex;
	private long offerPercentage;
	private double offerRate;
	
	
	
	
	
	public long getOfferPercentage() {
		return offerPercentage;
	}
	public void setOfferPercentage(long offerPercentage) {
		this.offerPercentage = offerPercentage;
	}
	public double getOfferRate() {
		return offerRate;
	}
	public void setOfferRate(double offerRate) {
		this.offerRate = offerRate;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	
	public long getOfferEnds() {
		return offerEnds;
	}
	public void setOfferEnds(long offerEnds) {
		this.offerEnds = offerEnds;
	}
	public long getOfferStarts() {
		return offerStarts;
	}
	public void setOfferStarts(long offerStarts) {
		this.offerStarts = offerStarts;
	}
	public String getDescriptionAr() {
		return descriptionAr;
	}
	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getOfferId() {
		return offerId;
	}
	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}
	public String getOfferName() {
		return offerName;
	}
	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}
	public String getOfferNameAr() {
		return offerNameAr;
	}
	public void setOfferNameAr(String offerNameAr) {
		this.offerNameAr = offerNameAr;
	}
	
	

}
