package com.innoventiaProject.discount.model.input;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.innoventiaProject.discount.constants.Constants;

public class ReviewVo implements Constants{

	@NotNull(message="data is null")
	@NotEmpty(message="data is empty")
	@Pattern(regexp=REGEX_NOTE,message="data format incorrect")
	private String data="";
	private int currentIndex;
	private int rowPerPage;
	@Min(value=1,message="father id should be greater than zero")
	private long fatherId;//shop or product id 
	private int modeId;// not using now
	private int productOrShop;// 2=shop,1=product
	private String date="";
	@Min(value=1,message="user id should be greater than zero")
	private long userId;
	private String name="";
	private long reviewId;
	private long subReviewId;
	private long likes;
	@Min(value=1,message="rating should be greater than zero")
	private int rating;
	private long companyId;
	private long lastUpdatedTime;
	private String customerName="";
	private String customerNameAr="";
	private double totalRate;
	private double totalRating;
	private String comments;
	private long ratingPercentage;
	
	

	public long getRatingPercentage() {
		return ratingPercentage;
	}
	public void setRatingPercentage(long ratingPercentage) {
		this.ratingPercentage = ratingPercentage;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public double getTotalRating() {
		return totalRating;
	}
	public void setTotalRating(double totalRating) {
		this.totalRating = totalRating;
	}
	public double getTotalRate() {
		return totalRate;
	}
	public void setTotalRate(double totalRate) {
		this.totalRate = totalRate;
	}
	public String getCustomerNameAr() {
		return customerNameAr;
	}
	public void setCustomerNameAr(String customerNameAr) {
		this.customerNameAr = customerNameAr;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	public long getFatherId() {
		return fatherId;
	}
	public void setFatherId(long fatherId) {
		this.fatherId = fatherId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	
	
	public int getModeId() {
		return modeId;
	}
	public void setModeId(int modeId) {
		this.modeId = modeId;
	}
	public int getProductOrShop() {
		return productOrShop;
	}
	public void setProductOrShop(int productOrShop) {
		this.productOrShop = productOrShop;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getReviewId() {
		return reviewId;
	}
	public void setReviewId(long reviewId) {
		this.reviewId = reviewId;
	}
	public long getSubReviewId() {
		return subReviewId;
	}
	public void setSubReviewId(long subReviewId) {
		this.subReviewId = subReviewId;
	}
	public long getLikes() {
		return likes;
	}
	public void setLikes(long likes) {
		this.likes = likes;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	
	
	

	
	}
	
	
	

