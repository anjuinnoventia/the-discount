package com.innoventiaProject.discount.model.input;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class CityVo
{

	private long cityId;
	@NotNull(message="city name is null")
	@NotEmpty(message="city name is empty")
	private String cityName;
	@Min(value=0,message="minimum value for city status is 0`")
	@Max(value=1,message="maximum value for city status is 1")
	private int citusStatus;
	private int cityFlag;
	@Min(value=1,message="mimimum value for the country id should be 1")
	private long countryId;
	private String countryName;
	private String countryNameAr;
	private String cityNameAr;
	
	
	
	
	
	public String getCountryNameAr() {
		return countryNameAr;
	}
	public void setCountryNameAr(String countryNameAr) {
		this.countryNameAr = countryNameAr;
	}
	public String getCityNameAr() {
		return cityNameAr;
	}
	public void setCityNameAr(String cityNameAr) {
		this.cityNameAr = cityNameAr;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	
	public int getCitusStatus() {
		return citusStatus;
	}
	public void setCitusStatus(int citusStatus) {
		this.citusStatus = citusStatus;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public int getCityFlag() {
		return cityFlag;
	}
	public void setCityFlag(int cityFlag) {
		this.cityFlag = cityFlag;
	}
	
	
	
	
}
