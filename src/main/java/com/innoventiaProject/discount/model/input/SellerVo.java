package com.innoventiaProject.discount.model.input;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.innoventiaProject.discount.constants.Constants;

public class SellerVo implements Constants


{
	@Min(value=0,message="seller id should be greater than or equal to 0")
	public long sellerId;
	@NotNull
	@NotEmpty
	
	public String sellerName="";
	public String sellerNameAr;
	public int status;
	public String countryName;
	public String cityName;
	private String addressAr;
	public int stat;
	public long createdDate;
	public String createdDateString;
	public long lastUpdatedTime;
	public String lastUpdatedTimeString;
	public long userId;
	public int  rank;
	@NotNull
	@NotEmpty
    @Pattern(regexp=REGEX_PHONE,message="invalid phone number")
	public String phoneNumber="";
	public String description="";
    public String descriptionAr="";
	@NotNull(message="email id is null")
	@NotEmpty
	@Pattern(regexp=REGEX_EMAIL,message="email format incorrect")
	public String emailId="";
	@NotNull(message="email id is null")
	@NotEmpty
	@Pattern(regexp=REGEX_WEBADDRESS,message="web address format incorrect")
	public String webAddress="";
	@NotNull(message="email id is null")
	@NotEmpty(message="address data empty")
	@Pattern(regexp=REGEX_NOTE,message="address format incorrect")
	public String address="";
	public long mainCategoryId;
	public double rating;
	public long likes;
	public long shares;
	public String priceStarts="0";
	public String workingHours;
	private String returnPolicy;
	private String returnPolicyAr;
	public int delivaryStatus;
	@NotNull
	@Min(value=1,message="latitude should be greater than 0")
	public double latitude;
	@NotNull
	@Min(value=1,message="longitude should be greater than 0")
	public double longitude;
	
	public long noOfReviews;
	@NotNull
	@Min(value=1,message="city id should be greater than 0")
	public long cityId;
	@NotNull
	@Min(value=1,message="country id should be greater than 0")
	public long countryId;
	/*@NotNull(message="Featurs and photos is null")
	@NotEmpty(message="Featurs and photos is empty")
	@Valid
	public List<FeatureVo> featureVo=new ArrayList<FeatureVo>();*/
	

	public int  serviceOrProduct;
	public long servingLocations;
	public long operatingOfficerId;
	public int  isCod;
	public int  isOnline;
	public String profileImage;
	@Pattern(regexp=REGEX_PASSWORD,message="password should be minimum eight characters, at least one letter and one number and no special charachtres")
	public String password="";
	public String oldPassword="";
	@NotEmpty(message="Telephone is incorrect")
	public String telephoneNo="";
	@NotEmpty(message="Contact name is empty")
	public String contactName="";
	public long companyId;
	
	
	
    public String getAddressAr() {
		return addressAr;
	}
	public void setAddressAr(String addressAr) {
		this.addressAr = addressAr;
	}
	public String getReturnPolicy() {
		return returnPolicy;
	}
	public void setReturnPolicy(String returnPolicy) {
		this.returnPolicy = returnPolicy;
	}
	public String getReturnPolicyAr() {
		return returnPolicyAr;
	}
	public void setReturnPolicyAr(String returnPolicyAr) {
		this.returnPolicyAr = returnPolicyAr;
	}
public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
public int getStat() {
		return stat;
	}
	public void setStat(int stat) {
		this.stat = stat;
	}

public String getTelephoneNo() {
		return telephoneNo;
	}
	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}
	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}
	public String getProfileImage() {
		return profileImage;
	}
	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}
	public long getSellerId() {
		return sellerId;
	}
	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}
	public int getIsCod() {
		return isCod;
	}
	public void setIsCod(int isCod) {
		this.isCod = isCod;
	}
	public int getIsOnline() {
		return isOnline;
	}
	public void setIsOnline(int isOnline) {
		this.isOnline = isOnline;
	}
	
	public int getRank() {
		return rank;
	}
	public void setRank(int rank) {
		this.rank = rank;
	}
	public String getDescriptionAr() {
		return descriptionAr;
	}
	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}
	

	public long getServingLocations() {
		return servingLocations;
	}
	public void setServingLocations(long servingLocations) {
		this.servingLocations = servingLocations;
	}
	public int getServiceOrProduct() {
		return serviceOrProduct;
	}
	public void setServiceOrProduct(int serviceOrProduct) {
		this.serviceOrProduct = serviceOrProduct;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) 
	{
		this.countryId = countryId;
	}

	
	/*public List<FeatureVo> getFeatureVo() {
		return featureVo;
	}
	public void setFeatureVo(List<FeatureVo> featureVo) {
		this.featureVo = featureVo;
	}*/
	
	public String getSellerName() {
		return sellerName;
	}
	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}
	public String getSellerNameAr() {
		return sellerNameAr;
	}
	public void setSellerNameAr(String sellerNameAr) {
		this.sellerNameAr = sellerNameAr;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getWebAddress() {
		return webAddress;
	}
	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public long getMainCategoryId() {
		return mainCategoryId;
	}
	public void setMainCategoryId(long mainCategoryId) {
		this.mainCategoryId = mainCategoryId;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public long getLikes() {
		return likes;
	}
	public void setLikes(long likes) {
		this.likes = likes;
	}
	public long getShares() {
		return shares;
	}
	public void setShares(long shares) {
		this.shares = shares;
	}
	public double getPriceStarts() {
		return Double.valueOf(priceStarts);
	}
	public void setPriceStarts(String priceStarts) {
		this.priceStarts = priceStarts;
	}
	public String getWorkingHours() {
		return workingHours;
	}
	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}
	public int getDelivaryStatus() {
		return delivaryStatus;
	}
	public void setDelivaryStatus(int delivaryStatus) {
		this.delivaryStatus = delivaryStatus;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public long getNoOfReviews() {
		return noOfReviews;
	}
	public void setNoOfReviews(long noOfReviews) {
		this.noOfReviews = noOfReviews;
	}
	public String getCreatedDateString() {
		return createdDateString;
	}
	public void setCreatedDateString(String createdDateString) {
		this.createdDateString = createdDateString;
	}
	public String getLastUpdatedTimeString() {
		return lastUpdatedTimeString;
	}
	public void setLastUpdatedTimeString(String lastUpdatedTimeString) {
		this.lastUpdatedTimeString = lastUpdatedTimeString;
	}
	
	
}
