package com.innoventiaProject.discount.model.input;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class FeatureVo  {
    @NotNull(message="data is null")
    @NotEmpty(message="data is empty")
	private String data="";
	private long featurePhotoId;
	private int modeId;
	@NotNull
	private long userId;
	private String extraData="";
	private double extraDataNum;
	 @NotNull
	private int productOrShop;
	private int status;
	private long lastUpdatedTime;
	private long productId;
	private long auctionId;
	private long taxId;
	private int itemDimeansion;
	private int typeOfFeature;
	
	
	
	

	public int getItemDimeansion() {
		return itemDimeansion;
	}
	public void setItemDimeansion(int itemDimeansion) {
		this.itemDimeansion = itemDimeansion;
	}
	public int getTypeOfFeature() {
		return typeOfFeature;
	}
	public void setTypeOfFeature(int typeOfFeature) {
		this.typeOfFeature = typeOfFeature;
	}
	public long getTaxId() {
		return taxId;
	}
	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}
	public long getAuctionId() {
		return auctionId;
	}
	public void setAuctionId(long auctionId) {
		this.auctionId = auctionId;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public long getFeaturePhotoId() {
		return featurePhotoId;
	}
	public void setFeaturePhotoId(long featurePhotoId) {
		this.featurePhotoId = featurePhotoId;
	}
	
	public int getModeId() {
		return modeId;
	}
	public void setModeId(int modeId) {
		this.modeId = modeId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getExtraData() {
		return extraData;
	}
	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}
	public double getExtraDataNum() {
		return extraDataNum;
	}
	public void setExtraDataNum(double extraDataNum) {
		this.extraDataNum = extraDataNum;
	}
	public int getProductOrShop() {
		return productOrShop;
	}
	public void setProductOrShop(int productOrShop) {
		this.productOrShop = productOrShop;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}
	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	
	
	
	
	
	
	
	
}
