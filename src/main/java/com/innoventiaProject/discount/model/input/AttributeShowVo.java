package com.innoventiaProject.discount.model.input;

import com.innoventiaProject.entity.input.AttributeDetails;

public class AttributeShowVo {
   /* private String value;
    private long attributeId;
    
    
    
    public AttributeShowVo(String value, long attributeId) {
		super();
		this.value = value;
		this.attributeId = attributeId;
	}
    
    
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public long getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}*/
	
	
	 private String value;
	 private String valueAr;
	    private AttributeDetails attributeDetails;
	    
	    
	    
	    public AttributeShowVo(String value,String valueAr, AttributeDetails a) {
			super();
			this.value = value;
			this.valueAr=valueAr;
			this.attributeDetails = a;
		}
	    
	    
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}


		public AttributeDetails getAttributeDetails() {
			return attributeDetails;
		}


		public void setAttributeDetails(AttributeDetails attributeDetails) {
			this.attributeDetails = attributeDetails;
		}


		
		
    
    
}
