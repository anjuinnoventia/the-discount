package com.innoventiaProject.discount.model.input;

import com.innoventiaProject.discount.constants.Constants;

public class ReportCommentsVo implements Constants {
	private long reviewId;
	private String comments;
	public long getReviewId() {
		return reviewId;
	}
	public void setReviewId(long reviewId) {
		this.reviewId = reviewId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	
}
