/**
 * 
 */
package com.innoventiaProject.discount.model.input;

import java.util.List;

/**
 * @author Shibina EC
 *@email shibina@sociohub.in
 */
public class LikeAndShareVO {
	public long reviewId;
	public String data;
	public long id;
	public int mode;
	public long userId;
	public List<ReviewVo> reviews;
	@Override
	public String toString() {
		return "LikeAndShareVO [reviewId=" + reviewId + ", data=" + data + ", id=" + id + ", mode=" + mode + ", userId="
				+ userId + ", reviews=" + reviews + "]";
	}
	
	
	

}
