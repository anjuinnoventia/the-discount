package com.innoventiaProject.discount.model.input;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

public class PalaceVo {

    @NotNull
	public String address;
    @NotNull
	public String storeName;
    @NotNull
    @Pattern(regexp="^[0-9\\\\+]{1,}[0-9\\\\-]{2,15}$",message="invalid phone number")
	public String phoneNo;
	public double priceStarts;
	public long shares;
	@NotNull
	public String description;
	public double rating;
	public int delivaryStatus;
	public String webAddress;
	@NotNull(message="email id is null")
	@Pattern(regexp="^[_A-Za-z0-9-]+(\\\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[A-Za-z]{2,})$",message="email format incorrect")
	public String emailId;
	public long likes;
	public long userId;
	public String createdDate;
	public String lastUpdatedDate;
	public int status;
	public long shopId;
	public String workingHours;
	public long shopType;
	public String shopTypeName;
	public String statusName;
	@NotNull
	@Min(value=1,message="latitude should be greater than 0")
	public double latitude;
	@NotNull
	public double longitude;
	@NotNull
	public String city; 
	public long noOfReviews;
	public Map<Long, String> shopCategoryMap;
	@NotEmpty(message="photos should not be empty")
	public List<FeatureVo> featureOrPhotos;
	
	
	
	
	
	
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getStoreName() {
		return storeName;
	}
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public double getPriceStarts() {
		return priceStarts;
	}
	public void setPriceStarts(double priceStarts) {
		this.priceStarts = priceStarts;
	}
	public long getShares() {
		return shares;
	}
	public void setShares(long shares) {
		this.shares = shares;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	public int getDelivaryStatus() {
		return delivaryStatus;
	}
	public void setDelivaryStatus(int delivaryStatus) {
		this.delivaryStatus = delivaryStatus;
	}
	public String getWebAddress() {
		return webAddress;
	}
	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public long getLikes() {
		return likes;
	}
	public void setLikes(long likes) {
		this.likes = likes;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public long getShopId() {
		return shopId;
	}
	public void setShopId(long shopId) {
		this.shopId = shopId;
	}
	public String getWorkingHours() {
		return workingHours;
	}
	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}
	public long getShopType() {
		return shopType;
	}
	public void setShopType(long shopType) {
		this.shopType = shopType;
	}
	public String getShopTypeName() {
		return shopTypeName;
	}
	public void setShopTypeName(String shopTypeName) {
		this.shopTypeName = shopTypeName;
	}
	public String getStatusName() {
		return statusName;
	}
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public long getNoOfReviews() {
		return noOfReviews;
	}
	public void setNoOfReviews(long noOfReviews) {
		this.noOfReviews = noOfReviews;
	}
	public Map<Long, String> getShopCategoryMap() {
		return shopCategoryMap;
	}
	public void setShopCategoryMap(Map<Long, String> shopCategoryMap) {
		this.shopCategoryMap = shopCategoryMap;
	}
	public List<FeatureVo> getFeatureOrPhotos() {
		return featureOrPhotos;
	}
	public void setFeatureOrPhotos(List<FeatureVo> featureOrPhotos) {
		this.featureOrPhotos = featureOrPhotos;
	}
	
	
	
	
	
	
	
	
}
