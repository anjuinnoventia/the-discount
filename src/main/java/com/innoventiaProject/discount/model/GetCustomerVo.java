package com.innoventiaProject.discount.model;

import javax.validation.constraints.Min;

public class GetCustomerVo {
	
	@Min(value=0,message="minimum current index should be 0")
	private int currentIndex;
	@Min(value=1,message="minimum row per page should be 1")
	private int rowPerPage;
	private long userId;
	private long customerId;
	private long fromDate;
	private long toDate;
	private long countryId;
	private String countryName;
	private long cityId;
	private String cityName;
	private int status;
	private int orderBy;
	private String customerName;
	
	private long companyId;
	private String customerNameAr;
	private String phoneNo;
	private String cityNameAr;
	private String countryNameAr;
	private String emailId;
	private String email;
	
	
	
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCityNameAr() {
		return cityNameAr;
	}
	public void setCityNameAr(String cityNameAr) {
		this.cityNameAr = cityNameAr;
	}
	public String getCountryNameAr() {
		return countryNameAr;
	}
	public void setCountryNameAr(String countryNameAr) {
		this.countryNameAr = countryNameAr;
	}
	public String getCustomerNameAr() {
		return customerNameAr;
	}
	public void setCustomerNameAr(String customerNameAr) {
		this.customerNameAr = customerNameAr;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getCurrentIndex() {
		return currentIndex;
	}
	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public long getFromDate() {
		return fromDate;
	}
	public void setFromDate(long fromDate) {
		this.fromDate = fromDate;
	}
	public long getToDate() {
		return toDate;
	}
	public void setToDate(long toDate) {
		this.toDate = toDate;
	}
	public long getCountryId() {
		return countryId;
	}
	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public long getCityId() {
		return cityId;
	}
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public int getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(int orderBy) {
		this.orderBy = orderBy;
	}
	
	
}