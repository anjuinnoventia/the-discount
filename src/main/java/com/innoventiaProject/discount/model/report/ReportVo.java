package com.innoventiaProject.discount.model.report;

import java.util.Map;

public class ReportVo {

	private String fromDate;
	private String toDate;
	private Map<String,String> dataMap;
	private int mode;
	private int reportType;
	private int status;
	private long customerId;
	private long auctionExpiryTime;
	private long auctionTime;
	private int auctionStatus;
	private int firstIndex;
	private int rowPerPage;
	private long auctionDate;
	
	
	
	
	
	public long getAuctionDate() {
		return auctionDate;
	}
	public void setAuctionDate(long auctionDate) {
		this.auctionDate = auctionDate;
	}
	public int getFirstIndex() {
		return firstIndex;
	}
	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}
	public int getRowPerPage() {
		return rowPerPage;
	}
	public void setRowPerPage(int rowPerPage) {
		this.rowPerPage = rowPerPage;
	}
	public int getAuctionStatus() {
		return auctionStatus;
	}
	public void setAuctionStatus(int auctionStatus) {
		this.auctionStatus = auctionStatus;
	}
	public long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}
	public long getAuctionExpiryTime() {
		return auctionExpiryTime;
	}
	public void setAuctionExpiryTime(long auctionExpiryTime) {
		this.auctionExpiryTime = auctionExpiryTime;
	}
	public long getAuctionTime() {
		return auctionTime;
	}
	public void setAuctionTime(long auctionTime) {
		this.auctionTime = auctionTime;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public Map<String, String> getDataMap() {
		return dataMap;
	}
	public void setDataMap(Map<String, String> dataMap) {
		this.dataMap = dataMap;
	}
	public int getMode() {
		return mode;
	}
	public void setMode(int mode) {
		this.mode = mode;
	}
	public int getReportType() {
		return reportType;
	}
	public void setReportType(int reportType) {
		this.reportType = reportType;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	
	
}
