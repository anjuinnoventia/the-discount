package com.innoventiaProject.discount.constants;

public interface ReportConst {

	
	//------------------------SUBSCRIPTION MODES------------------
	public static final int REPORT_MODE_BETWEEN_TWO_DATE=1;
	public static final int REPORT_MODE_ONE_DATE_TO_ALL=2;
	public static final int REPORT_MODE_DAY_SUBSCRIBED=3;
	public static final int REPORT_MODE_TIME=4;
	public static final int REPORT_MODE_PERIOD_TYPE=5;
	public static final int REPORT_MODE_SERVICE=6;
	public static final int REPORT_MODE_BETWEEN_TWO_DATE_WITHOUT_STATUS=7;
	
	
	//-------------------------home constances
	
	public static final int BANNERS=1;
	public static final int SLIDES=2;
	//------------------------sale report by------------------------------
	
		public static final int SALE_REPORT_BY_CATEGORY=1;
		public static final int SALE_REPORT_BY_PRODUCT=2;
		public static final int SALE_REPORT_BY_SUBCATEGORY=3;
		public static final int SALE_REPORT_BY_STORE=4;
		public static final int SALE_REPORT_BY_DELIVERY=5;
		public static final int SALE_REPORT_BY_CUSTOMER=6;
		
}
