package com.innoventiaProject.discount.constants;

public interface InputConst {
	public static final int CLEARANCE_MODE=4;
	public static final int PRICEROLE_MODE=5;
	public static final int ACTIVE=1;
	public static final int INACTIVE=0;
	public static final String DEFAULT_PASSWORD = "mypassword";
	
	
	//---------------------JSP VIEW CONSTANTS------------------------------------------
	
	public static final String VIEW_CATEGORIES="input/categories";
	public static final String VIEW_SERVICES="input/viewservice";
	public static final String VIEW_ADD_SERVICE="input/addservice";
	public static final String VIEW_ADD_CATEGORY="input/addcategory";
	public static final String VIEW_BOOKING_DETAILS="input/viewbookings";
	public static final String VIEW_SUBSCRIPTION="input/viewsubscribtions";
	public static final String VIEW_USERLIST="input/viewusers";
	public static final String VIEW_ADD_USER="input/adduser";
	public static final String VIEW_CUSTOMERLIST="input/viewcustomers";
	public static final String VIEW_ADD_CUSTOMER="input/addcustomer";
	public static final String VIEW_BILL_VIEW="input/paybill";
	public static final String VIEW_BOOKING_REPORT="reports/bookingreport";
	
	//---------------------URL CONSTANTS------------------------------------------
	public static final String ACTIVATE_CUSTOMER_LINK="/activatecustomer";	
	public static final String VALIDATE_CUSTOMER_LINK="/validatecustomer";
	public static final String GET_ALL_CATEGORIES="/getcategory";
	public static final String GET_ALL_MAINCATEGORIES="/getmaincategory";
	public static final String GET_ALL_SUBCATEGORIES="/getsubcategory";
	public static final String GET_ALL_PRODUCTS="/getproducts";
	public static final String GET_ALL_PRODUCTS_WITHOUT_OFFER="/getproductswithoutoffer";
	
	public static final String GET_BOOKED="/getbooked";
	public static final String GET_SUBCATEGORY="/getsubcategory";
	public static final String GET_ALL_USERS="/getusers";
	public static final String GET_ALL_ROLES="/getroles";
	public static final String GET_ALL_SHOPS="/getshops";
	public static final String GET_ALL_SHOPS_BY_SELLER_ID="/allshopbyseller";
	public static final String GET_ALL_PRODUCTS_BY_USER_ID="/allproductsbyuser";
	
	public static final String GET_ALL_REVIEWS="/getreviews";
	public static final String GET_ALL_DETAILS="/getalldetails";
	public static final String LOW_STOCK_REPORT="/lowstockreport";
	public static final String TRANSACTION_REPORT="/transactionReport";
	public static final String INCOME_EXPENCE_REPORT="/incomeexpencereport";
	public static final String TAX_REPORT="/taxreport";
	public static final String PRODUCT_PURCHASE_REPORT="/productreport";
	public static final String  UPDATE_STOCK="/updatestock";
	public static final String GET_ALL_DETAILS_CUST="/getalldetailscust";
	public static final String SAVE_USER="/saveuser";
	public static final String SAVE_MAINCATEGORY="/savemaincategory";
	public static final String SAVE_REVIEW="/savereview";
	public static final String ADD_COMMENTS="/addcomments";
	public static final String GET_ALL_ADDS="/getallads";
	public static final String FORGET_PASSWORD="/forgetpassword";
	public static final String RESEND_OTP="/resendotp";
	
	public static final String FORGET_PASSWORD_OWNER="/forgetpasswordowner";
	public static final String FORGET_OTP_VALIDTE="/forgetotpvalidate";	
	public static final String FORGET_OTP_VALIDTE_OWNER="/forgetotpvalidateowner";	
	
	
	public static final String CHANGE_RETURN_STATUS="/changereturnstatus";
	public static final String  REQUEST_ITEMS="/requestitems";
	public static final String UPDATE_REQUEST_ITEMS="/updaterequestitems";
	public static final String  GET_ALL_ITEM_REQUEST="/getallitemrequest";
	public static final String  SAVE_ADD="/saveadd";
	public static final String DELETE_REVIEW="/deletereview";
	public static final String SAVE_SHOP="/saveshop";
	public static final String SAVE_PRODUCT="/saveproduct";
	public static final String SAVE_CATEGORY_MERCHANT="/savecategoriesmerchant";
	public static final String SAVE_CMS="/savecms";
	public static final String SAVE_MANUFACTURE="/savemanufacture";
	public static final String SEARCH_DATA="/searchdata";
	public static final String FILTER_DATA="/sortdata";
	public static final String UN_SUBSCRIBE="/unsubscribe";
	public static final String SAVE_SUBCATEGORY="/savesubcategory";
	public static final String AUTHENTICATION_USER="/login";
	public static final String ADD_OFFER="/addoffer";
	public static final String UPDATE_CUSTOMER="/updatecustomer";
	public static final String GET_CURRENT_USER="/getcurrentuser";
	public static final String AUTHENTICATION_SOCIAL="/authenticatesocial";
	public static final String GET_USER="/getuser";
	public static final String GET_PROMOCODE_DETAILS="/getpromocodedetails";
	public static final String VALIDATE_PROMOCODE_DETAILS="/validatepromocode";
	public static final String LOGOUT_USER="/logout";
	public static final String GET_ALL_DELIVERY_BOY="/getalldeliveryboy";
	public static final String ASSIGNING_DELIVERY_BOY="/assigningdeliveryboy";
	public static final String ASSIGNING_OFFER="/assignoffer";
	public static final String CHANGE_DELIVERY_STATUS="/changedeliverystatus";
	public static final String CANCELL_DELIVERY_BOY="/cancelldeliveryboy";
	public static final String GET_ALL_SELLERS="/getallsellers";
	public static final String  GET_ALL_NOTIFICATION="/getallnotification";
	public static final String ADD_DELIVERY_BOY="/adddeliveryboy";
	public static final String CHANGE_SELLER_STATUS="/changesellerstatus";
	public static final String ADD_SELLER_DETAILS="/addsellerdetails";
	public static final String UPDATE_SELLER_DETAILS="/updatesellerdetails";
	public static final String ADD_COMPANY_DETAILS="/addcompanydetails";
	public static final String CHANGE_CUSTOMER_STATUS="/changecustomerstatus";
	public static final String CHANGE_PROMOCODE_STATUS="/changepromocodestatus";
	public static final String SELLER_LOGIN="/sellerlogin";
	public static final String GET_DASHBOARD_DETAILS="/getdashdetails";
	public static final String SAVE_LIKE_OR_SHARE="/savelikeorshares";
	public static final String URL_FILE_UPLOAD="/upload";
	public static final String URL_FILE_UPLOAD_PENTAL="pentalupload";
	public static final String URL_FILE_UPLOAD_TO_AWS="/uploadtoaws";
	public static final String ADD_CART="/addcart";
	public static final String ADD_CATEGORY="/addcategory";
	public static final String ADD_AUCTION="/addauction";
	public static final String ADD_COUNTRTY="/addcountry";
	public static final String ADD_CITY="/addcity";
	public static final String ADD_NEW_ORDER="/addneworder";
	public static final String ADD_TAX="/addtax";
	public static final String GET_SHIPPING_BY_ORDERID="/getshippingbyshippingid";
	public static final String GET_ALL_TAX="/getalltax";
	public static final String GET_ALL_SHIPPING_DETAILS="/getallshippingdetails";
	public static final String	GET_CARD_DETAILS="/getcarddetails";
	public static final String  DELETE_TAX="/deletetax";
	public static final String CHANGE_ORDER_STATUS="/changeorderstatus";
	public static final String ADD_SHIPPING_DETAILS="/addshippingdetails";
	public static final String ADD_CARD_DETAILS="/addcarddetails";
	public static final String DELETE_SHIPPING="/deleteshipping";
	public static final String DELETE_CARD="/deletecard";
	public static final String CHANGE_AUCTION_STATUS="/changeauctionstatus";
	public static final String GET_ALL_ORDERS="/getallorders";
	public static final String GET_ALL_ADD_SATUS="/getalladdstatus";
	public static final String GET_ALL_ORDER_DETAILS_BY="/getallorderdetailsby";
	public static final String BEST_PURCHASED_PRODUCTS="/bestpurchasedproducts";
	public static final String COMMISSION_REPORT="/commissionreport";
	public static final String GET_ALL_SALES_REPORT="/getallsalesreport";
	public static final String  GET_ALL_CITY="/getallcity";
	public static final String  GET_ALL_PROMOCODE_REPORT="/getallpromocodereports";
	public static final String   MONTHLY_REVENUE_REPORT="/monthlurevenuereport";
	public static final String  YEARLY_REVENUE_REPORT="/yearlyreport";
	public static final String  WEEKLY_REVENUE_REPORT="/weeklyreport";
	public static final String GET_ALL_OFFER="/getalloffer";
	public static final String GET_ALL_PRODUCT_ATTRIBUTES="/getallproductattributes";
	public static final String GET_ALL_CMS="/getallcms";
	public static final String  GET_ALL_MANUFACTURE="/getallmanufacture";
	//final String uri = "https://www.google.com?key="+key;
	public static final String SAVE_BID="/savebid";
	public static final String DELETE_BID="/deletebid";
	public static final String GET_ALL_COUNTRY="/getallcountry";
	public static final String READ_NOTIFICATION="/readnotification";
	public static final String  GOOGLE_AUTHENTICATION="/googleauthentication";
	public static final String CHANGE_PRODUCT_STATUS="/changeproductstatus";
	public static final String  CHANGE_SHIPPING_STATUS="/changeshippingstatus";
	public static final String RETURN_PRODUCT="/returnproduct";
	public static final String GET_ALL_CUSTOMERS="/getallcustomers";
	public static final String GET_ALL_PROMOTERS="getallpromoters";
	public static final String GET_ALL_RETURN_PRODUCTS="/getallreturnproducts";
	public static final String GET_ALL_AUCTION="/getallauction";
	public static final String GET_ALL_AUCTION_BY="/getallauctionby";
	public static final String GET_ALL_AUCTION_BY_STATUS="/getallauctionbystatus";
	public static final String  GET_ALL_ATTRIBUTES="/getallattributes";
	public static final String  GET_ALL_ATTRIBUTES_VALUES="/getallattributevalues";
	public static final String SAVE_ATTRIBUTE="/saveattribute";
	//--------------------------CALL VIEW FROM FRONTEND----------------------------------
	public static final String GET_VIEW_CATEGORIES="/getcategoryview";
	public static final String GET_VIEW_BILL="/getbillview";
	public static final String GET_VIEW_SERVICES="/getserviceview";
	public static final String GET_VIEW_ADD_CATEGORY="/addcategory";
	public static final String GET_VIEW_ADD_SERVICE="/addservice";
	public static final String GET_VIEW_ADD_USER="/adduser";
	public static final String GET_VIEW_USERS="/getusersview";
	public static final String GET_VIEW_CUSTOMERS="/getcustomersview";
	public static final String GET_VIEW_BOOKING_DETAILS="/getbookingdetails";
	public static final String GET_ALL_CATEGORY_FROM_MAINCATEGORY="/getcateogoryfrommaincategory";
	public static final String GET_VIEW_PERIOD_BOOKING="/getperiodbooking";
	
//----------------------------------ACCOUNT-------------------------------------------
	public static final String ADD_INCOME_EXPENSE_HEAD="/addincomeexpensehead";
	public static final String ADD_INCOME="/addincome";
	public static final String ADD_EXPENSE="/addexpense";
	public static final String GET_INCOME_EXPENSE_HEAD="/getincomeexpensehead";
	public static final String GET_INCOME="/getincome";
	public static final String GET_EXPENSE="/getexpense";
	
	
	//---------------------DATA CONSTANTS------------------------------
	public static final int PRODUCT=1;
	public static final int SERVICE=0;
	public static final int SELLER=2;
	public static final int HOME_DELIVARY=1;
	public static final int NO_HOME_DELIVARY=2;
    public static final int MODE_LIKE_PRODUCT=1;
	public static final int MODE_SHARE_PRODUCT=2;
	public static final int MODE_LIKE_SHOP=3;
	public static final int MODE_SHARE_SHOP=4;
	public static final int MODE_PHOTOS=5;
	public static final int MODE_FEATURES=6;
	public static final int MODE_VEDIO_URL=8;
	public static final int MODE_FEATURES_ARABIC=7;
	
	//----------------------------ATTRIBUTES----------------------------
	public static final int MODE_COLOR=8;
	public static final int MODE_MATERIAL=9;
	public static final int MODE_TYPE=10;
	public static final int MODE_CAPACITY=11;
	public static final int MODE_SIZE=12;
	
	//----------------------------------Diamension---------------------------
	public static final int MODE_LENGTH=13;
	public static final int MODE_WIDTH=14;
	public static final int MODE_HEIGHT=15;
	public static final int MODE_WEIGHT=16;
	
	
	//delivery boy-------------------------------------------------------
	public static final long CANCELL_DELIVERY=0;
	//review table modes---------------------------------------------------
	public static final int MODE_LIKE_OR_SHARE=2;
	public static final int MODE_REVIEW=1;
	public static final int MODE_COUNTRY=1;
	public static final int MODE_STATE=2;
	public static final int MODE_CITY=3;

	public static final String STRING_BOOKED="Booked";
	public static final String STRING_PROCESSING="Processing";
	public static final String STRING_SERVICED="Seviced";
	
	//-------------------------category----------------------------------
	//-------------------------addverstisement constant-----------
		public static final int ADD_STATED=2;
		public static final int ADD_EXPIRED=3;
		public static final int ADD_CANCELED=4;
		public static final long ADD_SLIDER=1;
		
		public static final long ADD_FULL_BANNER_FIRST=2;
		public static final long ADD_FULL_BANNER_LEFT_FIRST=3;
		public static final long ADD_FULL_BANNER_RIGHT_FIRST=4;
		
		public static final long ADD_HALF_BANNER_FIRST=5;
		public static final long ADD_HALF_BANNER_LEFT_FIRST=6;
		public static final long ADD_HALF_BANNER_RIGHT_FIRST=7;
		
		
		
		
	//-------------------------------------search constants------------------------
	public static final Integer SEARCH_BY_COUNTRY=1;
	public static final Integer SEARCH_BY_CITY=2;
	public static final Integer SEARCH_BY_AREA=3;
	public static final Integer SEARCH_BY_DATE=4;
	public static final Integer SEARCH_BY_SECTION=5;
	public static final Integer SEARCH_BY_PRICE=6;
	public static final Integer SEARCH_BY_REVIEWS=8;
	public static final Integer SEARCH_BY_NAME=9;
	public static final Integer SEARCH_BY_CATEGORY=10;
	public static final Integer SEARCH_BY_RANK=11;
	public static final Integer SEARCH_BY_NEW=15;
	public static final Integer SEARCH_BY_RATING=12;
	public static final Integer SEARCH_BY_SUBCATEGORY=13;
	public static final Integer SEARCH_BY_MANUFACTURE=14;
	public static final Integer SEARCH_BY_PRODUCT_OR_SERVICE=15;
	public static final Integer LOW_TO_HIGH=16;
	public static final Integer HIGH_TO_LOW=17;
	public static final Integer POPULARITY=18;
	public static final String ASC="ASC";
	public static final String DESC="DESC";
	
	
	//-------------------income/expense-------------------------------------
	
	public static final int INCOME_HEAD = 7;
	public static final int EXPENSE_HEAD = 8;
	public static final int TRAN_MODE_INCOME = 12;
    public static final int TRAN_MODE_EXPENSE = 13;

}
