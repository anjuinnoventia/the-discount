package com.innoventiaProject.discount.localization;

import org.springframework.beans.factory.annotation.Value;


public class Messages  {
	//*********** LOGIN **********************************
		@Value("${login}")
		public String MESSAGE_KEY_LOGIN;

		@Value("${user_name}")
		public String MESSAGE_KEY_USER_NAME;

		@Value("${password}")
		public String MESSAGE_KEY_PASSWORD;
		
		@Value("${yours}")
		public String MESSAGE_KEY_YOURS;
	//********** HOME ***********************************
		@Value("${home}")
		public String MESSAGE_KEY_HOME;
		
		@Value("${help}")
		public String MESSAGE_KEY_HELP;
		
		@Value("${about_us}")
		public String MESSAGE_KEY_ABOUT_US;
		
		@Value("${contact}")
		public String MESSAGE_KEY_CONTACT;
		
		@Value("${void}")
		public String MESSAGE_KEY_VOID;
	
		@Value("${inventory}")
		public String MESSAGE_KEY_INVENTORY;
		
		@Value("${billing}")
		public String MESSAGE_KEY_BILLING;
		
		@Value("${accounts}")
		public String MESSAGE_KEY_ACCOUNTS;
		
		@Value("${hr}")
		public String MESSAGE_KEY_HR;
	
		@Value("${invoice}")
		public String MESSAGE_KEY_INVOICE;
		//******** ATTENDANCE *********************************	
		@Value("${present_or_absent}")
		public String MESSAGE_KEY_PRESENT_OR_ABSENT;

		@Value("${half_or_full}")
		public String MESSAGE_KEY_HALF_OR_FULL_DAY;
		
		@Value("${absent_tick}")
		public String MESSAGE_KEY_TICK_ABSENT;

		@Value("${tick_if_half_day}")
		public String MESSAGE_KEY_TICK_HALF_DAY;
	//******** HR  &&  EMPLOYEE *********************************	
		@Value("${employee}")
		public String MESSAGE_KEY_EMPLOYEE;

		@Value("${employee_name}")
		public String MESSAGE_KEY_EMPLOYEE_NAME;

		@Value("${employee_edit}")
		public String MESSAGE_KEY_EMPLOYEE_EDIT;

		@Value("${employee_management}")
		public String MESSAGE_KEY_EMPLOYEE_MANAGEMENT;

		@Value("${leave_management}")
		public String MESSAGE_KEY_LEAVE_MANAGEMENT;
		
		@Value("${salary_pay}")
		public String MESSAGE_KEY_SALARY_PAY;
		//******************* FUEL *****************************
		@Value("${terminal_add}")
		public String MESSAGE_KEY_TERMINAL_ADD;
		
		@Value("${dip_close}")
		public String MESSAGE_KEY_DIP_CLOSE;
		
		@Value("${mode}")
		public String MESSAGE_KEY_MODE;
		
		@Value("${barcode}")
		public String MESSAGE_KEY_BARCODE;
		
		@Value("${is}")
		public String MESSAGE_KEY_IS;
		
		@Value("${or}")
		public String MESSAGE_KEY_OR;
		
		@Value("${too}")
		public String MESSAGE_KEY_TOO;
		
		@Value("${code}")
		public String MESSAGE_KEY_CODE;
		
		@Value("${start}")
		public String MESSAGE_KEY_START;
		
		@Value("${end}")
		public String MESSAGE_KEY_END;
		
		@Value("${large}")
		public String MESSAGE_KEY_LARGE;
		
		@Value("${empty}")
		public String MESSAGE_KEY_EMPTY;
 
		@Value("${quantity_obtained}")
		public String MESSAGE_KEY_QUANTITY_OBTAINED;

		@Value("${measured_quantity}")
		public String MESSAGE_KEY_MEASURED_QUANTITY;

		@Value("${tank}")
		public String MESSAGE_KEY_TANK;

		@Value("${vehicle}")
		public String MESSAGE_KEY_VEHICLE;
		
		@Value("${litr}")
		public String MESSAGE_KEY_LITR;
		
		@Value("${machine}")
		public String MESSAGE_KEY_MACHINE;
		
		@Value("${product}")
		public String MESSAGE_KEY_PRODUCT;
		
		@Value("${initial_reading}")
		public String MESSAGE_KEY_INITIAL_READING;
		
		@Value("${nozzle}")
		public String MESSAGE_KEY_NOZZLE;
		
		@Value("${status}")
		public String MESSAGE_KEY_STATUS;
		
		@Value("${active}")
		public String MESSAGE_KEY_ACTIVE;
		
		@Value("${inactive}")
		public String MESSAGE_KEY_INACTIVE;
		//******************* INVOICE *****************************
		@Value("${invoice_id}")
		public String MESSAGE_KEY_INVOICE_ID;
		
		@Value("${head}")
		public String MESSAGE_KEY_HEAD;
		
		@Value("${invoice_add}")
		public String MESSAGE_KEY_INVOICE_ADD;
		
		@Value("${date}")
		public String MESSAGE_KEY_DATE;
		
		@Value("${supplier_code}")
		public String MESSAGE_KEY_SUPPLIER_CODE;
				
		@Value("${invoice_no}")
		public String MESSAGE_KEY_INVOICE_NO;
		
		@Value("${total_amount}")
		public String MESSAGE_KEY_TOTAL_AMOUNT;
		
		@Value("${receiving}")
		public String MESSAGE_KEY_RECEIVING;
		// ********** STOCK **********************************************
		@Value("${all_stock}")
		public String MESSAGE_KEY_ALL_STOCK_LIST;
		
		@Value("${count}")
		public String MESSAGE_KEY_COUNT;
		
		@Value("${list}")
		public String MESSAGE_KEY_LIST;
		
		@Value("${exchange}")
		public String MESSAGE_KEY_EXCHANGE;
		
		@Value("${selling}")
		public String MESSAGE_KEY_SELLING;
		
		@Value("${stock_report}")
		public String MESSAGE_KEY_STOCK_REPORT;
		
		@Value("${stock_data}")
		public String MESSAGE_KEY_STOCK_DATA;
		
		@Value("${stock}")
		public String MESSAGE_KEY_STOCK;
		
		@Value("${clearance}")
		public String MESSAGE_KEY_CLEARENCE;
		
		@Value("${stock_type}")
		public String MESSAGE_KEY_STOCK_TYPE;
		
		@Value("${item_stock}")
		public String MESSAGE_KEY_ITEM_STOCK;
		
		@Value("${stock_add}")
		public String MESSAGE_KEY_STOCK_ADD;
		
		@Value("${added_stock}")
		public String MESSAGE_KEY_ADDED_STOCK;
		
		@Value("${stock_add_for}")
		public String MESSAGE_KEY_ADD_STOCK_FOR;
		
		@Value("${stock_history}")
		public String MESSAGE_KEY_STOCK_HISTORY;
		
		@Value("${stock_id}")
		public String MESSAGE_KEY_STOCK_ID;
		
		@Value("${sku_head}")
		public String MESSAGE_KEY_SKU;
		
		@Value("${update_stock}")
		public String MESSAGE_KEY_UPDATE_STOCK;

		@Value("${new_stock}")
		public String MESSAGE_KEY_NEW_STOCK;
		
		@Value("${sku}")
		public String MESSAGE_KEY_STOCK_KEEPING_UNIT;		
		
		@Value("${shelf_id}")
		public String MESSAGE_KEY_SHELF_ID;		
		
		@Value("${purchase_price}")
		public String MESSAGE_KEY_PURCHASED_PRICE;

		@Value("${batch_code}")
		public String MESSAGE_KEY_BATCH_CODE;
		
		@Value("${tax_persentage}")
		public String MESSAGE_KEY_TAX_PERCENTAGE;
		
		@Value("${quantity}")
		public String MESSAGE_KEY_QUANTITY;
		
		@Value("${unit}")
		public String MESSAGE_KEY_UNIT;

		@Value("${add_price}")
		public String MESSAGE_KEY_ADD_PRICE;
		
		@Value("${view_price}")
		public String MESSAGE_KEY_VIEW_PRICE;

		@Value("${view}")
		public String MESSAGE_KEY_VIEW;
		//*********** CASH  TRANSFER  **************************************
		@Value("${account_to}")
		public String MESSAGE_KEY_ACCOUNT_TO;
		
		@Value("${account_from}")
		public String MESSAGE_KEY_ACCOUNT_FROM;
		
		@Value("${cash_transfer}")
		public String MESSAGE_KEY_CASH_TRANSFER;
	
		@Value("${amount}")
		public String MESSAGE_KEY_AMOUNT;

		@Value("${open_blnce}")
		public String MESSAGE_KEY_OPENING_BALANCE;
		
		@Value("${note}")
		public String MESSAGE_KEY_NOTE;
		//*********** BANK DEPOSITE **************************************
		@Value("${account_no}")
		public String MESSAGE_KEY_ACCOUNT_NO;
	
		@Value("${bank_deposit}")
		public String MESSAGE_KEY_BANK_DEPOSIT;
		
		@Value("${credit_party_payment_by_bank}")
		public String MESSAGE_KEY_CREDIT_PARTY_PAYMENT_BANK;
		
		@Value("${receipt_no}")
		public String MESSAGE_KEY_RECEIPT_NO;
		
		@Value("${depositor}")
		public String MESSAGE_KEY_DEPOSITOR;
		//*********** BANK ACCOUNT ADD ***********************************
		@Value("${cash_lift}")
		public String MESSAGE_KEY_CASH_LIFT;
		
		@Value("${transfers}")
		public String MESSAGE_KEY_TRANSFERS;
		
		@Value("${create}")
		public String MESSAGE_KEY_CREATE;
		
		@Value("${bank}")
		public String MESSAGE_KEY_BANK;
		
		@Value("${branch}")
		public String MESSAGE_KEY_BRANCH;
		
		@Value("${account_holer_1}")
		public String MESSAGE_KEY_ACCOUNT_HOLDER_1;
		
		@Value("${account_holer_2}")
		public String MESSAGE_KEY_ACCOUNT_HOLDER_2;
		
		@Value("${account_holer_3}")
		public String MESSAGE_KEY_ACCOUNT_HOLDER_3;

		@Value("${ifc_code}")
		public String MESSAGE_KEY_IFC_CODE;
		
		@Value("${balance_amount}")
		public String MESSAGE_KEY_BALANCE_AMOUNT;
		
		@Value("${bank_accounts}")
		public String MESSAGE_KEY_BANK_ACCOUNTS;
		
		@Value("${no}")
		public String MESSAGE_KEY_NO;
		// ********** BILLING ********************************************
		@Value("${voucher}")
		public String MESSAGE_KEY_VOUCHER;
		
		@Value("${hold}")
		public String MESSAGE_KEY_HOLD;
		
		@Value("${held}")
		public String MESSAGE_KEY_HELD;
		
		@Value("${new_bill}")
		public String MESSAGE_KEY_NEW_BILL;
		
		@Value("${delete}")
		public String MESSAGE_KEY_DELETE;
		
		@Value("${price_override}")
		public String MESSAGE_KEY_PRICE_OVERRIDE;
		
		@Value("${offer}")
		public String MESSAGE_KEY_OFFER;
		
		@Value("${payment_out}")
		public String MESSAGE_KEY_PAYMENT_OUT;
		
		@Value("${expenses}")
		public String MESSAGE_KEY_EXPENSES;
		
		@Value("${value_can_not_empty}")
		public String VALUE_CAN_NOT_EMPTY;
		
		@Value("${invalid_amt}")
		public String INVALID_AMT;
		
		@Value("${add_credit}")
		public String MESSAGE_KEY_ADD_CREDIT;
		
		@Value("${clear_rb}")
		public String MESSAGE_KEY_BT_CLEAR;
		
		@Value("${issuing}")
		public String MESSAGE_KEY_ISSUING;
		
		@Value("${notclear_rb}")
		public String MESSAGE_KEY_BT_NOT_CLEAR;
		
		@Value("${all}")
		public String MESSAGE_KEY_ALL ; 

		@Value("${cleared_or_not}")
		public String MESSAGE_KEY_CLEARED_OR_NOT;

		@Value("${supplier_name}")
		public String MESSAGE_KEY_SUPPLIER_NAME;
	
		@Value("${supplier_id}")
		public String MESSAGE_KEY_SUPPLIER_ID;

		@Value("${supplier_payment}")
		public String 	MESSAGE_KEY_SUPPLIER_PAYMENT;
		
		@Value("${credit_payment}")
		public String MESSAGE_KEY_CREDIT_PAYMENT;
		
		@Value("${balance}")
		public String MESSAGE_KEY_BALANCE;
		
		@Value("${uncleared}")
		public String MESSAGE_KEY_UNCLEARED;
		
		@Value("${uncleared_paid_amount}")
		public String MESSAGE_KEY_UNCLEARED_PAID_AMOUNT;
		
		@Value("${cleared_paid_amount}")
		public String MESSAGE_KEY_CLEARED_PAID_AMOUNT;
		
		@Value("${cheque}")
		public String MESSAGE_KEY_CHEQUE;
		
		@Value("${employee_deploy}")
		public String MESSAGE_KEY_EMPLOYEE_DEPLOY;
		
		@Value("${transaction_date}")
		public String MESSAGE_KEY_TRANSACTION_DATE;
		
		@Value("${cheque_date}")
		public String MESSAGE_KEY_CHEQUE_DATE;
		
		@Value("${name_on_cheque}")
		public String MESSAGE_KEY_NAME_ON_CHEQUE;
		
		@Value("${cleared}")
		public String MESSAGE_KEY_CLEARED;
		
		@Value("${clear}")
		public String MESSAGE_KEY_CLEAR;
		
		@Value("${pay}")
		public String MESSAGE_KEY_PAY;
		
		@Value("${party_id}")
		public String MESSAGE_KEY_PARTY_ID;

		@Value("${credit_pay}")
		public String MESSAGE_KEY_CREDIT_PAY;
		
		@Value("${supplier_entry}")
		public String MESSAGE_KEY_SUPPLIER_ENTRY_PANE;
		
		@Value("${party}")
		public String MESSAGE_KEY_PARTY;
		
		@Value("${party_mode}")
		public String MESSAGE_KEY_PARTY_MODE;

		
		@Value("${payment_mode}")
		public String MESSAGE_KEY_PAYMENT_MODE;
		
		@Value("${proceed}")
		public String MESSAGE_KEY_PROCEED;
		
		@Value("${add_price_role}")
	    public String MESSAGE_KEY_ADD_PRICE_ROLE;
		
		@Value("${price_role}")
	    public String MESSAGE_KEY_PRICE_ROLE;
		
		@Value("${shift_head_add}")
	    public String MESSAGE_KEY_SHIFT_HEAD_ADD;
		
		@Value("${role_id}")
	    public String MESSAGE_KEY_ROLE_ID;
		
		@Value("${selling_percentage}")
	    public String MESSAGE_KEY_SELLING_PERCENTAGE;

		@Value("${item_list}")
		public String MESSAGE_KEY_ITEM_LIST;
		
		@Value("${sales}")
		public String MESSAGE_KEY_SALES;

		@Value("${purchase}")
		public String MESSAGE_KEY_PURCHASE;

		@Value("${total_purchase}")
		public String MESSAGE_KEY_TOTAL_PURCHASE;

		@Value("${expense}")
		public String MESSAGE_KEY_EXPENSE;
		
		@Value("${income}")
		public String MESSAGE_KEY_INCOME;

		@Value("${create_expense}")
		public String MESSAGE_KEY_CREATE_EXPENSE;
		
		@Value("${create_income}")
		public String MESSAGE_KEY_CREATE_INCOME;

		@Value("${payment_in}")
		public String MESSAGE_KEY_PAYMENT_IN;
		
		@Value("${add_Credit_party}")
		public String MESSAGE_KEY_ADD_CREDIT_PARTY;
		
		@Value("${item_code}")
		public String MESSAGE_KEY_ITEM_CODE;

		@Value("${selling_rate}")
		public String MESSAGE_KEY_SELLING_RATE;

		@Value("${mrp_rate}")
		public String MESSAGE_KEY_MRP_RATE;

		@Value("${no_of_items}")
		public String MESSAGE_KEY_NO_OF_ITEMS;

		@Value("${purchased_rate}")
		public String MESSAGE_KEY_PURCHASED_RATE;

		@Value("${tax}")
		public String MESSAGE_KEY_TAX;

		@Value("${profit}")
		public String MESSAGE_KEY_PROFIT;

		@Value("${discount}")
		public String MESSAGE_KEY_DISCOUNT;

		@Value("${max_discount}")
		public String MESSAGE_KEY_MAX_DISCOUNT;
		
		@Value("${sale_qty}")
		public String MESSAGE_KEY_SALE_QTY;

		@Value("${particulars}")
		public String MESSAGE_KEY_PARTICULARS;

		@Value("${rate}")
		public String MESSAGE_KEY_RATE;

		@Value("${total_rate}")
		public String MESSAGE_KEY_TOTAL_RATE;

		@Value("${expense_input}")
		public String MESSAGE_KEY_EXPENSE_INPUT;
		
		@Value("${income_input}")
		public String MESSAGE_KEY_INCOME_INPUT;

		@Value("${billing_head}")
		public String MESSAGE_KEY_BILLING_HEAD;

		@Value("${paid_to}")
		public String MESSAGE_KEY_PAID_TO;
		
		@Value("${paid_from}")
		public String MESSAGE_KEY_PAID_FROM;
		
		@Value("${received_from}")
		public String MESSAGE_KEY_RECEIVED_FROM;
		
		@Value("${service_code}")
		public String MESSAGE_KEY_SERVICE_CODE;
		
		@Value("${service_bill}")
		public String MESSAGE_KEY_SERVICE_BILL;

		@Value("${item_bill}")
		public String MESSAGE_KEY_ITEM_BILL;
		
		@Value("${credit_item_bill}")
		public String MESSAGE_KEY_CREDIT_ITEM_BILL;
		
		@Value("${petrol_card_cash_deposite}")
		public String MESSAGE_KEY_PETROL_CARD_CASH_DEPOSITE;
		
		@Value("${cash_as_credit}")
		public String MESSAGE_KEY_CASH_AS_CREDIT;
		
		@Value("${credit_as_cash_return}")
		public String MESSAGE_KEY_CREDIT_AS_CASH_RETURN;
		
		@Value("${cash_balance_statement}")
		public String MESSAGE_KEY_CASH_BALANCE_STATEMENT;
		
		@Value("${total_credit_report}")
		public String MESSAGE_KEY_TOTAL_CREDIT_REPORT;
		
		
		@Value("${remark}")
		public String MESSAGE_KEY_REMARK;

		@Value("${card_entry}")
		public String MESSAGE_KEY_CARD_ENTRY;

		@Value("${cheque_entry}")
		public String MESSAGE_KEY_CHEQUE_ENTRY;
		
		@Value("${cheque_payment}")
		public String MESSAGE_KEY_CHEQUE_PAYMENT;
		
		@Value("${bank_payment}")
		public String MESSAGE_KEY_BANK_PAYMENT;

		@Value("${item_entry}")
		public String MESSAGE_KEY_ITEM_ENTRY;

		@Value("${credit_amt}")
		public String MESSAGE_KEY_CREDIT_AMOUNT;
		
		@Value("${existing_credit}")
		public String MESSAGE_KEY_EXISTING_CREDIT;
		
		@Value("${credit_amt_add}")
		public String MESSAGE_KEY_CREDIT_AMOUNT_ADD;
		
		@Value("${cms}")
		public String MESSAGE_KEY_CMS;
		
		@Value("${cash_entry}")
		public String MESSAGE_KEY_CASH_ENTRY;

		@Value("${cash_amt}")
		public String MESSAGE_KEY_CASH_AMOUNT;

		@Value("${service_tax}")
		public String MESSAGE_KEY_SERVICE_TAX;

		@Value("${date_from}")
		public String MESSAGE_KEY_DATE_FROM;

		@Value("${all_credit}")
		public String mESSAGE_KEY_ALL_CREDIT;
		
		@Value("${day_report}")
		public String MESSAGE_KEY_DAY_REPORT;
		
		
		@Value("${automatic}")
		public String MESSAGE_KEY_AUTOMATIC;
		
		@Value("${manual}")
		public String MESSAGE_KEY_MANUAL;
		
		@Value("${multiple_day_report}")
		public String MESSAGE_KEY_MULTIPLE_DAY_REPORT;
		
		
		@Value("${date_to}")
		public String MESSAGE_KEY_DATE_TO;

		@Value("${employee_id}")
		public String MESSAGE_KEY_EMPLOYEE_ID;
		
		@Value("${paid}")
		public String MESSAGE_KEY_PAID;
		
		@Value("${paid_amount}")
		public String MESSAGE_KEY_PAID_AMOUNT;
		
		@Value("${net_rate}")
		public String MESSAGE_KEY_NET_RATE;
		
		@Value("${return}")
		public String MESSAGE_KEY_RETURN;
		// ----------------------ACCOUNT DATA-----------------------------
		@Value("${shift_close}")
		public String MESSAGE_KEY_SHIFT_CLOSE;
		
		@Value("${get}")
		public String MESSAGE_KEY_GET;
		
		@Value("${loss_amount}")
		public String MESSAGE_KEY_LOSS_AMOUNT;
		
		@Value("${shift_enter}")
		public String MESSAGE_KEY_SHIFT_ENTER;
		
		@Value("${in}")
		public String MESSAGE_KEY_IN;

		@Value("${out}")
		public String MESSAGE_KEY_OUT;

		@Value("${transaction_id}")
		public String MESSAGE_KEY_TRANSACTION_ID;

		@Value("${credit_details}")
		public String MESSAGE_KEY_CREDIT_DETAILS;

		@Value("${check_details}")
		public String MESSAGE_KEY_CHECK_DETAILS;

		@Value("${check}")
		public String MESSAGE_KEY_CHECK;

		@Value("${card}")
		public String MESSAGE_KEY_CARD;

		@Value("${card_details}")
		public String MESSAGE_KEY_CARD_DETAILS;

		@Value("${cash_in}")
		public String MESSAGE_KEY_CASH_IN;

		@Value("${cash_out}")
		public String MESSAGE_KEY_CASH_OUT;

		@Value("${day_book}")
		public String MESSAGE_KEY_DAY_BOOK;

		@Value("${cheque_no}")
		public String MESSAGE_KEY_CHEQUE_NO;

		@Value("${card_no}")
		public String MESSAGE_KEY_CARD_NO;
		
		@Value("${total_debit_amount}")
		public String MESSAGE_KEY_TOTAL_DEBIT_AMOUNT;

		@Value("${total_credit_amount}")
		public String MESSAGE_KEY_TOTAL_CREDIT_AMOUNT;

		@Value("${cash}")
		public String MESSAGE_KEY_CASH;
		
		@Value("${bank_entry}")
		public String MESSAGE_KEY_BANK_ENTRY;
		
		@Value("${cash_book}")
		public String MESSAGE_KEY_CASH_BOOK;
		//------------------------INPUT DATA-------------------------------------
		@Value("${input}")
		public String MESSAGE_KEY_INPUT;
		
		@Value("${identification}")
		public String MESSAGE_KEY_IDENTIFICATION;
		
		@Value("${tag}")
		public String MESSAGE_KEY_TAG;
		
		@Value("${assign}")
		public String MESSAGE_KEY_ASSIGN;
		
		@Value("${defect}")
		public String MESSAGE_KEY_DEFECT;
		
		@Value("${add_item}")
		public String MESSAGE_KEY_ADD_ITEM;
		
		@Value("${shift_change}")
		public String MESSAGE_KEY_SHIFT_CHANGE;
		
		@Value("${report}")
		public String MESSAGE_KEY_REPORT;
		
		@Value("${collect}")
		public String MESSAGE_KEY_COLLECT;
		
		@Value("${unit_price}")
		public String MESSAGE_KEY_UNIT_PRICE;
		
		@Value("${new}")
		public String MESSAGE_KEY_NEW;
		
		@Value("${morning}")
		public String MESSAGE_KEY_MORNING;
		
		@Value("${evening}")
		public String MESSAGE_KEY_EVENING;
		
		@Value("${attendance}")
		public String MESSAGE_KEY_ATTENDANCE;
		
		@Value("${attendance_entry}")
		public String MESSAGE_KEY_ATTENDANCE_ENTRY;
		
		@Value("${manufacture_name}")
		public String MESSAGE_KEY_MANUFACTURE_NAME;

		@Value("${manufacture_id}")
		public String MESSAGE_KEY_MANUFACTURE_ID;

		@Value("${manufacture}")
		public String MESSAGE_KEY_MANUFACTURE;

		@Value("${category}")
		public String MESSAGE_KEY_CATEGORY;

		@Value("${category_id}")
		public String MESSAGE_KEY_CATEGORY_ID;

		@Value("${subcategory}")
		public String MESSAGE_KEY_SUBCATEGORY;

		@Value("${item}")
		public String MESSAGE_KEY_ITEM;
		
		@Value("${item_type}")
		public String MESSAGE_KEY_ITEM_TYPE;

		@Value("${input_data}")
		public String MESSAGE_KEY_INPUT_DATA;

		@Value("${submit}")
		public String MESSAGE_KEY_SUBMIT;

		@Value("${id}")
		public String MESSAGE_KEY_ID;

		@Value("${credit}")
		public String MESSAGE_KEY_CREDIT;

		@Value("${credit_party_id}")
		public String MESSAGE_KEY_CREDIT_PARTY_ID;

		@Value("${no_id}")
		public String MESSAGE_KEY_NO_ID;

		@Value("${add}")
		public String MESSAGE_KEY_ADD;

		@Value("${setting}")
		public String MESSAGE_KEY_SETTING;

		@Value("${category_name}")
		public String MESSAGE_KEY_CATEGORY_NAME;

		@Value("${subcategory_name}")
		public String MESSAGE_KEY_SUBCATEGORY_NAME;

		@Value("${subcategory_id}")
		public String MESSAGE_KEY_SUBCATEGORY_ID;

		@Value("${item_id}")
		public String MESSAGE_KEY_ITEM_ID;

		@Value("${item_name}")
		public String MESSAGE_KEY_ITEM_NAME;

		@Value("${service_id}")
		public String MESSAGE_KEY_SERVICE_ID;

		@Value("${service_name}")
		public String MESSAGE_KEY_SERVICE_NAME;

		@Value("${service}")
		public String MESSAGE_KEY_SERVICE;

		@Value("${to}")
		public String MESSAGE_KEY_TO;
		
		@Value("${ing}")
		public String MESSAGE_KEY_ING;

		@Value("${of}")
		public String MESSAGE_KEY_OF;
		
		@Value("${number}")
		public String MESSAGE_KEY_NUMBER;
		

		@Value("${expense_id}")
		public String MESSAGE_KEY_EXPENSE_ID;
		
		@Value("${income_id}")
		public String MESSAGE_KEY_INCOME_ID;

		@Value("${expense_name}")
		public String MESSAGE_KEY_EXPENSE_NAME;
		
		@Value("${income_name}")
		public String MESSAGE_KEY_INCOME_NAME;

		@Value("${expense_head}")
		public String MESSAGE_KEY_EXPENSE_HEAD;
		
		@Value("${income_head}")
		public String MESSAGE_KEY_INCOME_HEAD;

		@Value("${credit_party_name}")
		public String MESSAGE_KEY_CREDIT_PARTY_NAME;
		
		@Value("${related_person}")
		public String MESSAGE_KEY_RELATED_PERSON;

		@Value("${paired_id}")
		public String MESSAGE_KEY_PAIRED_ID;

		@Value("${Credit_party_add}")
		public String MESSAGE_KEY_CREDIT_PARTY_ADD;

		@Value("${Credit_add}")
		public String MESSAGE_KEY_CREDIT_ADD;

		@Value("${input_head}")
		public String MESSAGE_KEY_INPUT_HEAD;

		@Value("${payable}")
		public String MESSAGE_KEY_PAYABLE;
		
		@Value("${receivable}")
		public String MESSAGE_KEY_RECEIVABLE;
		
		@Value("${recall}")
		public String MESSAGE_KEY_RECALL;

		@Value("${bill}")
		public String MESSAGE_KEY_BILL;

		@Value("${phone}")
		public String MESSAGE_KEY_PHONE;

		@Value("${address}")
		public String MESSAGE_KEY_ADDRESS;

		@Value("${credit_period}")
		public String MESSAGE_KEY_CREDIT_PERIOD;

		@Value("${email}")
		public String MESSAGE_KEY_EMAIL;

		@Value("${collection_day}")
		public String MESSAGE_KEY_COLLECTION_DAY;

		@Value("${credit_limit}")
		public String MESSAGE_KEY_CREDIT_LIMIT;
		
		@Value("${first_name}")
		public String MESSAGE_KEY_FIRST_NAME;
		
		@Value("${last_name}")
		public String MESSAGE_KEY_LAST_NAME;
		
		@Value("${sur_name}")
		public String MESSAGE_KEY_SUR_NAME;
		
		@Value("${roll_name}")
		public String MESSAGE_KEY_ROLL_NAME;
		
		@Value("${address1}")
		public String MESSAGE_KEY_ADDRESS_1;
		
		@Value("${address2}")
		public String MESSAGE_KEY_ADDRESS_2;
		
		@Value("${city}")
		public String MESSAGE_KEY_CITY;
		
		@Value("${pincode}")
		public String MESSAGE_KEY_PINCODE;
		
		@Value("${date_of_birth}")
		public String MESSAGE_KEY_DATE_OF_BIRTH;
		
		@Value("${male}")
		public String MESSAGE_KEY_MALE;
		
		@Value("${female}")
		public String MESSAGE_KEY_FEMALE;
		
		@Value("${gender}")
		public String MESSAGE_KEY_GENDER;
		
		@Value("${phone_no}")
		public String MESSAGE_KEY_PHONE_NO;
		
		@Value("${guardian_name}")
		public String MESSAGE_KEY_GUARDIAN_NAME;
		
		@Value("${qualification}")
		public String MESSAGE_KEY_QUALIFICATION;
		
		@Value("${guardian_phone_no}")
		public String MESSAGE_KEY_GUARDIAN_PHONE_NO;
		
		@Value("${proof}")
		public String MESSAGE_KEY_PROOF;
		
		@Value("${browse}")
		public String MESSAGE_KEY_BROWSE;
		
		@Value("${continue}")
		public String MESSAGE_KEY_CONTINUE;
		
		@Value("${salary_mode}")
		public String MESSAGE_KEY_SALARY_MODE;
		
		@Value("${allowed_leave}")
		public String MESSAGE_KEY_ALLOWED_LEAVE;
		
		@Value("${da}")
		public String MESSAGE_KEY_DA;
		
		@Value("${ta}")
		public String MESSAGE_KEY_TA;
		
		@Value("${basic_salary}")
		public String MESSAGE_KEY_BASIC_SALARY;
		
		@Value("${bonus}")
		public String MESSAGE_KEY_BONUS;
		
		@Value("${others}")
		public String MESSAGE_KEY_OTHERS;
		
		@Value("${save}")
		public String MESSAGE_KEY_SAVE;
		
		@Value("${payroll}")
		public String MESSAGE_KEY_PAYROLL;
		
		@Value("${dialy}")
		public String MESSAGE_KEY_DAILY;
		
		@Value("${weakily}")
		public String MESSAGE_KEY_WEEKLY;
		
		@Value("${monthly}")
		public String MESSAGE_KEY_MONTHLY;
		
		@Value("${yearly}")
		public String MESSAGE_KEY_YEARLY;
		
		@Value("${employee_list}")
		public String MESSAGE_KEY_EMPLOYEE_LIST;
		
		@Value("${employee_add}")
		public String MESSAGE_KEY_EMPLOYEE_ADD;
		
		@Value("${user_id}")
		public String MESSAGE_KEY_USER_ID;
		
		@Value("${company}")
		public String MESSAGE_KEY_COMPANY;
		
		@Value("${registered_date}")
		public String MESSAGE_KEY_REGISTERED_DATE;
		
		@Value("${monday}")
		public String MESSAGE_KEY_MONDAY;
		
		@Value("${tuesday}")
		public String MESSAGE_KEY_TUESDAY;
		
		@Value("${wednesday}")
		public String MESSAGE_KEY_WEDNESDAY;
		
		@Value("${thursday}")
		public String MESSAGE_KEY_THURSDAY;
		
		@Value("${friday}")
		public String MESSAGE_KEY_FRIDAY;
		
		@Value("${saturday}")
		public String MESSAGE_KEY_SATURDAY;
		
		@Value("${sunday}")
		public String MESSAGE_KEY_SUNDAY;
		
		@Value("${old_password}")
		public String MESSAGE_KEY_OLD_PASSORD;
		
		@Value("${new_password}")
		public String MESSAGE_KEY_NEW_PASSORD;
		
		@Value("${retype_password}")
		public String MESSAGE_KEY_RETYPE_PASSORD;
		
		@Value("${activation_key}")
		public String MESSAGE_KEY_ACTIVATION_KEY;
		
		@Value("${licensekey_label}")
		public String MESSAGE_KEY_LICENSE_KEY;
		
		@Value("${trial_version}")
		public String MESSAGE_KEY_TRIAL_VERSION;
		
		@Value("${your_trial_expires_in}")
		public String MESSAGE_KEY_YOUR_TRIAL;
		
		@Value("${days}")
		public String MESSAGE_KEY_DAYS;
	
		@Value("${renew}")
		public String MESSAGE_KEY_RENEW;
	
		@Value("${renew_your_license_now}")
		public String MESSAGE_KEY_RENEW_YOUR_LICENSE;
	
		
		@Value("${old_pin}")
		public String MESSAGE_KEY_OLD_PIN;
		
		@Value("${new_pin}")
		public String MESSAGE_KEY_NEW_PIN;
		
		@Value("${retype_pin}")
		public String MESSAGE_KEY_RETYPE_PIN;
		
		@Value("${change}")
		public String MESSAGE_KEY_CHANGE;
		
		@Value("${password_change}")
		public String MESSAGE_KEY_PASSWORD_CHANGE;
		
		@Value("${pin_change}")
		public String MESSAGE_KEY_PIN_CHANGE;
		
		@Value("${remove}")
		public String MESSAGE_KEY_REMOVE;
		
		@Value("${terminal_used}")
		public String MESSAGE_KEY_TERMINAL_USED;
		
		@Value("${final_reading}")
		public String MESSAGE_KEY_FINAL_READING;
		
		@Value("${holder_name}")
		public String MESSAGE_KEY_HOLDER_NAME;
		
		@Value("${net_reading}")
		public String MESSAGE_KEY_NET_READING;
		//-------------------COMMON DATA---------------------------------
		@Value("${an}")
		public String MESSAGE_KEY_AN;
		
		@Value("${a}")
		public String MESSAGE_KEY_A;
		
		@Value("${no_of_rows}")
		public String MESSAGE_KEY_NO_OF_ROWS;
		
		@Value("${send}")
		public String MESSAGE_KEY_SEND;
		
		@Value("${message}")
		public String MESSAGE_KEY_MESSAGE;
		
		@Value("${contact_info}")
		public String MESSAGE_KEY_CONTACT_INFO;
		
		@Value("${search}")
		public String MESSAGE_KEY_SEARCH;

		@Value("${updated_time}")
		public String MESSAGE_KEY_UPDATED_TIME;
		
		@Value("${added_time}")
		public String MESSAGE_KEY_ADDED_TIME;

		@Value("${edit}")
		public String MESSAGE_KEY_EDIT;

		@Value("${print}")
		public String MESSAGE_KEY_PRINT;

		@Value("${name}")
		public String MESSAGE_KEY_NAME;

		@Value("${enter}")
		public String MESSAGE_KEY_ENTER;
		
		@Value("${back}")
		public String MESSAGE_KEY_BACK;

		@Value("${total}")
		public String MESSAGE_KEY_TOTAL;

		@Value("${total_paid}")
		public String MESSAGE_KEY_TOTAL_PAID;

		@Value("${view_details}")
		public String MESSAGE_KEY_VIEW_DETAILS;

		@Value("${some_mantotary_value_is_missing}")
		public String SOME_MANDATORY_VALUE_IS_MISSING;

		@Value("${invalid_amount_is_entered}")
		public String INVALID_AMOUNT_IS_ENTERED;

		@Value("${tran_note}")
		public String TRAN_NOTE;

		@Value("${payment_cash}")
		public String PAYMENT_CASH;

		@Value("${payment_check}")
		public String PAYMENT_CHECK;

		@Value("${payment_card}")
		public String PAYMENT_CARD;

		@Value("${payment_bank}")
		public String PAYMENT_BANK;

		@Value("${error_message_no_error}")
		public String ERROR_MESSAGE_NO_ERROR;

		@Value("${check_info_missing}")
		public String CHECK_INFO_MISSING;
		
		@Value("${check_date_invalid}")
		public String CHECK_DATE_INVALID;
		
		@Value("${cheque_account_no_missing}")
		public String CHEQUE_ACCOUNT_NO_MISSING;

		@Value("${no_data_to_show}")
		public String NO_DATA_TO_SHOW;
		
		@Value("${check_no_missing}")
		public String CHECK_NO_MISSING;
		
		@Value("${check_amt_missing}")
		public String CHECK_AMT_MISSING;
		
		@Value("${password_is_not_matching}")
		public String PASSWORD_IS_NOT_MATCHING;
		
		@Value("${check_name_missing}")
		public String CHECK_NAME_MISSING;
		
		@Value("${zero_value_entered}")
		public String ZERO_VALUE_ENTERED;
		
		@Value("${sales}")
		public String SALES;
	
		@Value("${ok}")
		public String MESSAGE_KEY_OK;
		
		@Value("${total_in}")
		public String MESSAGE_KEY_TOTAL_IN;
		
		@Value("${total_out}")
		public String MESSAGE_KEY_TOTAL_OUT;
		
		@Value("${cash_in_hand}")
		public String MESSAGE_KEY_CASH_IN_HAND;
		
		@Value("${keep_in_shelf}")
		public String MESSAGE_KEY_KEEP_IN_SHELF;
		
		@Value("${reset}")
		public String MESSAGE_KEY_RESET;
		
		@Value("${transation_types}")
		public String MESSAGE_KEY_TRANSACTION_TYPES;
		
		@Value("${credit_sale}")
		public String CREDIT_SALE;
		
		@Value("${sales_bill}")
		public String SALES_BILL;
		
		@Value("${credit_sale}")
		public String CREDIT_SALE_BILL;

		@Value("${notification}")
		public String MESSAGE_KEY_NOTIFICATION;
		
		@Value("${item_remains}")
		public String MESSAGE_KEY_ITEM_REMAINS;
		//---------------------------------------ERROR MESSAGE------------------------------------------------------
		@Value("${empty_string}")
		public String EMPTY_STRING;
		
		@Value("${dublicate_entry}")
		public String DUBLICATE_ENTRY;
		
		@Value("${null_value}")
		public String NULL_VALUE;
		
		@Value("${saving_failed}")
		public String SAVING_FAILED;
		
		@Value("${saved_succsessfully}")
		public String SAVED_SUCCESSFULLY;
		
		@Value("${update_failed}")
		public String UPDATE_FAILED;
		
		@Value("${updated_succsessfully}")
		public String UPDATED_SUCCESSFULLY;
		
		@Value("${valid_invoice_no}")
		public String ENTER_VALID_INVOICE_NO;
		
		@Value("${success}")
		public String SUCCESS;
		
		@Value("${not_found}")
		public String NOT_FOUND;
		
		@Value("${not_implement}")
		public String NOT_IMPLEMENT;
		
		@Value("${enter_all_data}")
		public String ENTER_ALL_DATA;
		
		@Value("${enter_all_data_or_invalid_data}")
		public String ENTER_ALL_DATA_OR_INVALID_DATA;
		
		@Value("${number_of_item_less_to_add}")
		public String NUMBER_OF_ITEM_LESS_TO_ADDED;
		
		@Value("${successfully_changed}")
		public String SUCCESSFULLY_CHANGED;
		
		@Value("${both_cannot_be_empty}")
		public String BOTH_CANNOT_BE_EMPTY;
		
		@Value("${a_confirm_dialog}")
		public String A_CONFIRM_DIALOG;
		
		@Value("${are_you_ok_with_this}")
		public String ARE_YOU_OK_WITH_THIS;
		
		@Value("${confirm_dialog}")
		public String CONFIRM_DIALOG;
		
		@Value("${do_you_want_to_continue}")
		public String DO_YOU_WANT_TO_CONTNUE;
		
		@Value("${error_dialog}")
		public String ERROR_DIALOG;
		
		@Value("${an_error_dialog}")
		public String AN_ERROR_DAILOG;
		
		@Value("${warning_dialog}")
		public String WARNING_DIALOG;
		
		@Value("${a_warning_dialog}")
		public String A_WARNING_DIALOG;
		
		@Value("${information_dailog}")
		public String INFORMATION_DIALOG;
		
		@Value("${enter_no_of_items}")
		public String ENTER_NO_OF_ITEMS;
		
		@Value("${select_a_item}")
		public String SELECT_A_ITEM;
		
		@Value("${could_not_insert}")
		public String COULD_NOT_INSERT;
		
		@Value("${not_possible}")
		public String NOT_POSSIBLE;

		@Value("${personal}")
		public String PERSONAL;
		
		@Value("${other}")
		public String OTHER;
		
		@Value("${checkPayment}")
		public String CHECKPAYMENT;
		
		@Value("${cardPayment}")
		public String CARDPAYMENT;
		
		@Value("${bankPayment}")
		public String BANKPAYMENT;
		
		@Value("${cashPayment}")
		public String CASHPAYMENT;
		//----------------------------SUPPLIER-----------------------------------------
				@Value("${supplier}")
				public String MESSAGE_KEY_HR_SUPPLIER;
				
				@Value("${add_supplier}")
				public String MESSAGE_KEY_HR_ADD_SUPPLIER;
				
				@Value("${purchase_limit}")
				public String MESSAGE_KEY_PURCHASE_LIMIT;
				
				@Value("${supplier_add}")
				public String MESSAGE_KEY_HR_HEAD_ADD_SUPPLIER;
	//----------------------------CREDIT PARTY-----------------------------------------
				@Value("${creditParty}")
				public String MESSAGE_KEY_HR_CREDIT_PARTY;
				
				@Value("${add_creditParty}")
				public String MESSAGE_KEY_HR_ADD_CREDIT_PARTY;
	//----------------------------SHIFTING-----------------------------------------
				@Value("${shifting}")
				public String MESSAGE_KEY_SHIFTING;	
				
				@Value("${credit_amount_add}")
				public String FXML_CTRL_CREDIT_AMOUNT_ADD_PANE;	
				
				@Value("${day_close}")
				public String MESSAGE_KEY_DAY_CLOSE;
	//-----------------------SETTINGS----------------------		
				@Value("${task_data}")
				public String MESSAGE_KEY_TASK_DATA;
				
				@Value("${pending_task}")
				public String MESSAGE_KEY_PENDING_TASK;
				
				@Value("${price_change}")
				public String MESSAGE_KEY_PRICE_CHANGE;
				
				@Value("${item_sale_report}")
				public String MESSAGE_KEY_ITEM_SALE_REPORT;
				
				@Value("${debit_quantity}")
				public String MESSAGE_KEY_DEBIT_QUANTITY;
				
				@Value("${cash_quantity}")
				public String MESSAGE_KEY_CASH_QUANTITY;
				
				@Value("${credit_quantity}")
				public String MESSAGE_KEY_CREDIT_QUANTITY;
				
				@Value("${debit_amount}")
				public String MESSAGE_KEY_DEBIT_AMOUNT;
				
				@Value("${expected_amount}")
				public String MESSAGE_KEY_EXPECTED_AMOUNT;
				
				@Value("${operating_officer}")
				public String MESSAGE_KEY_ADMIN;
				
				@Value("${expense_report}")
				public String MESSAGE_KEY_EXPENSE_REPORT;
				
				@Value("${transaction_report}")
				public String MESSAGE_KEY_TRANSACTION_REPORT;
				
				@Value("${transaction_report_table}")
				public String MESSAGE_KEY_TRANSACTION_REPORT_TABLE;
				

				@Value("${bill_to_be_clear}")
				public String MESSAGE_KEY_BILL_TO_BE_CLEAR;
				
				@Value("${all_terminal_table}")
				public String MESSAGE_KEY_ALL_TERMINAL_TABLE;
				
				@Value("${terminal}")
				public String MESSAGE_KEY_TERMINAL;
				
				@Value("${all_terminal}")
				public String MESSAGE_KEY_ALL_TERMINAL;
				
				@Value("${time}")
				public String MESSAGE_KEY_TIME;
				
				@Value("${stock_level_report}")
				public String MESSAGE_KEY_STOCK_LEVEL_REPORT;
				
				@Value("${select_location}")
				public String MESSAGE_KEY_SELECT_LOCATON;
				
				@Value("${select_file}")
				public String MESSAGE_KEY_SELECT_FLE;
				
				@Value("${backup}")
				public String MESSAGE_KEY_BACKUP;
				
				@Value("${restore}")
				public String MESSAGE_KEY_RESTORE;
				
				@Value("${backup_or_restore}")
				public String MESSAGE_KEY_BACKUP_OR_RESTORE;
				
				@Value("${select}")
				public String MESSAGE_KEY_SELECT;
				
				@Value("${please_wait}")
				public String MESSAGE_KEY_PLEASE_WAIT;
				
				@Value("${restoring}")
				public String MESSAGE_KEY_RESTORNG;
				
				@Value("${data_entry_settings}")
				public String MESSAGE_KEY_DATA_ENTRY_SETTINGS;
				
				@Value("${data}")
				public String MESSAGE_KEY_DATA;
//---------------------------- PRICE  CHANGE-------------------------------------
				@Value("${close_all_terminal}")
				public String MESSAGE_KEY_CLOSE_ALL_TERMINAL;
				
				@Value("${actual_volume_stock}")
				public String MESSAGE_KEY_ACTUAL_VOLUME_OF_STOCK;
				
				@Value("${price_change_history}")
				public String MESSAGE_KEY_PRICE_CHANGE_HISTORY;
				
				@Value("${actual_volume}")
				public String MESSAGE_KEY_ACTUAL_VOLUME;
	//------------------------ LIABILITY ---------------------------------
				@Value("${liability}")
				public String MESSAGE_KEY_LIABILITY;
				
				@Value("${principle_amount}")
				public String MESSAGE_KEY_PRINCIPLE_AMOUNT;
				
				@Value("${liability_from}")
				public String MESSAGE_KEY_LIABILITY_FROM;
				
				@Value("${monthly_payment}")
				public String MESSAGE_KEY_MONTHLY_PAYMENT;
				
				@Value("${duration}")
				public String MESSAGE_KEY_DURATION;
				
				@Value("${installment_period}")
				public String MESSAGE_KEY_INSTALLMENT_PERIOD;
				
				@Value("${interest}")
				public String MESSAGE_KEY_INTEREST;
				
				@Value("${margin}")
				public String MESSAGE_KEY_MARGIN;
				
				@Value("${identify}")
				public String MESSAGE_KEY_IDENTIFY;
				
				@Value("${transfering_type}")
				public String MESSAGE_KEY_TRANSFERING_TYPE;
				
				@Value("${all_liability}")
				public String MESSAGE_KEY_ALL_LIABILITY_LIST;
				
				@Value("${liability_pay}")
				public String MESSAGE_KEY_ALL_LIABILITY_PAY;
				
				@Value("${liability_id}")
				public String MESSAGE_KEY_LIABILITY_ID;
				
				@Value("${paid_date}")
				public String MESSAGE_KEY_PAID_DATE;
//---------------------------INVENTORY SALE REPORT--------------------------------------				
				@Value("${inventory_sale_report}")
				public String MESSAGE_KEY_INVENTORY_SALE_REPORT;
				
				@Value("${inventory_sale}")
				public String MESSAGE_KEY_INVENTORY_SALE;
				
				@Value("${report_name}")
				public String MESSAGE_KEY_REPORT_NAME;
				
				@Value("${summary_report}")
				public String MESSAGE_KEY_SUMMARY_REPORT;
				
				@Value("${detailed_report}")
				public String MESSAGE_KEY_DETAILED_REPORT;
//-----------------------------TEAM ADD-----------------------------------------
				@Value("${team_name}")
				public String MESSAGE_KEY_TEAM_NAME;
				
				@Value("${team}")
				public String MESSAGE_KEY_TEAM;
				
				@Value("${allocate_team}")
				public String MESSAGE_KEY_ALLOCATE_TEAM;
				
//--------------------------------------p and l account-----------------------
				
				
				@Value("${cost_of_goods_sold}")
				public String MESSAGE_KEY_COST_OF_GOODS_SOLD;
				
				@Value("${total_sales}")
				public String MESSAGE_KEY_TOTAL_SALES;
				
				@Value("${total_cost_of_goods_sold}")
				public String MESSAGE_KEY_TOTAL_COST_OF_GOODS_SOLD;
				
				@Value("${gross_profit_margin}")
				public String MESSAGE_KEY_GROSS_PROFIT_MARGIN;
				
				@Value("${operating_expenses}")
				public String MESSAGE_KEY_OPERATING_EXPENSES;
				
				@Value("${p_and_l_report}")
				public String MESSAGE_KEY_P_AND_L_REPORT;

				@Value("${credit_as_cash_statement}")
				public String MESSAGE_KEY_CREDIT_AS_CASH_STATEMENT;
				
				@Value("${total_operating_expenses}")
				public String MESSAGE_KEY_TOTAL_OPERATING_EXPENSES;
				
				@Value("${net_profit_or_lose}")
				public String MESSAGE_KEY_NET_PROFIT_OR_LOSS;
				
				@Value("${percentage}")
				public String MESSAGE_KEY_PERCENTAGE;
				
				@Value("${select_duration}")
				public String MESSAGE_KEY_SELECT_DURATION;
				
				@Value("${party_accounts}")
				public String MESSAGE_KEY_PARTY_ACCOUNTS;
				
				@Value("${rtgs_neft_transfer}")
				public String MESSAGE_KEY_RTGS_NEFT_TRANSFER;
				
				@Value("${add_purchase_invoice}")
				public String MESSAGE_KEY_ADD_PURCHASE_INVOICE;
				
				@Value("${different}")
				public String MESSAGE_KEY_DIFFERENT;
				
				@Value("${lookup}")
				public String MESSAGE_KEY_LOOKUP;
				
				@Value("${logout}")
				public String MESSAGE_KEY_LOGOUT;
				
				@Value("${customer_name}")
				public String MESSAGE_KEY_CUSTOMER_NAME;
				
				@Value("${found}")
				public String MESSAGE_KEY_FOUND;
}
