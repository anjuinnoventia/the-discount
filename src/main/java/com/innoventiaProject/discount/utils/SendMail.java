package com.innoventiaProject.discount.utils;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {
	
	
	
	
	public static void sendMailTo(String toAddress,String messageData,String subject) {
		Properties props = new Properties();
		props.put("mail.smtp.host", "mail.qnoft.com");
		props.put("mail.smtp.socketFactory.port", "587");
//		props.put("mail.smtp.socketFactory.class",
//				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "587");

		Session session = Session.getDefaultInstance(props,
			new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication("support@afrahbooking.com","Hidile@321");
				}
			});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("support@afrahbooking.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(toAddress));
			message.setSubject(subject);
			message.setText(messageData);

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
}