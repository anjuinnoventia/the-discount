package com.innoventiaProject.discount.service;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innoventiaProject.dao.GeneralDao;
import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.mail.MailVo;
import com.innoventiaProject.discount.model.AddCustomer;
import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.discount.model.CompanyVo;
import com.innoventiaProject.discount.model.DeliveryBoyVo;
import com.innoventiaProject.discount.model.GetPromocodeDetails;
import com.innoventiaProject.discount.model.LoggedSession;
import com.innoventiaProject.discount.model.NotificationVo;
import com.innoventiaProject.discount.model.PromocodeDetailsVo;
import com.innoventiaProject.discount.model.input.SellerVo;
import com.innoventiaProject.discount.service.input.InputService;
import com.innoventiaProject.discount.utils.Misceleneous;
import com.innoventiaProject.discount.utils.TimeUtils;
import com.innoventiaProject.entity.input.Attributes;
import com.innoventiaProject.entity.input.Offer;
import com.innoventiaProject.entity.input.SellerDetails;
import com.innoventiaProject.entity.order.TaxManagement;
import com.innoventiaProject.entity.user.CompanyDetails;
import com.innoventiaProject.entity.user.CustomerDetails;
import com.innoventiaProject.entity.user.DeliveryBoy;
import com.innoventiaProject.entity.user.LoginDetails;
import com.innoventiaProject.entity.user.NotificationDetails;
import com.innoventiaProject.entity.user.PromocodeDetails;


@Service("generalService")
public class GeneralServiceImpl implements GeneralService, Constants 
{
	private static Logger logger = Logger.getLogger(GeneralServiceImpl.class);

	@Autowired
	private GeneralDao generalDao;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private InputService inputService;
	
	/*@Autowired
	private GeneralService userService;*/
	
	@Autowired
	public OtpService otpService;
	
	
	
	

	@Override
	public Map<Integer, String> getAllrolls() {
		Map<Integer, String> rollMap = new HashMap<Integer, String>();
		rollMap.put(ROLL_ID_ADMIN, ROLL_NAME_ADMIN);
		rollMap.put(ROLL_ID_CUSTOMER, ROLL_NAME_CUSTOMER);
		rollMap.put(ROLL_ID_SELLER, ROLL_NAME_SELLER);

		// rollMap.put(ROLL_ID_DELIVERY_BOY, ROLL_NAME_DELIVERY_BOY);
		return rollMap;
	}

	@Override
	public BasicResponse getUserFromUserId(long userId) {
		BasicResponse res = null;
		Map<String, Object> map = generalDao.getAllUsers(userId);
		res = new BasicResponse();
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			List<LoggedSession> loggedSessions = new ArrayList<>();
			List<LoginDetails> list = (List<LoginDetails>) map.get(DATA);
			LoggedSession loSession = null;
			for (LoginDetails login : list) {

				loSession = modelMapper.map(login, LoggedSession.class);
				loggedSessions.add(loSession);
			}
			res = new BasicResponse();
			if (userId > 0) {
				res.object = loggedSessions.get(0);
			} else {
				res.object = loggedSessions;
			}
		}
		return res;
	}

	@Override
	public Map<Integer, String> getSalaryModes() {
		Map<Integer, String> salModeMap = new HashMap<Integer, String>();
		salModeMap.put(SUBSCRIBE_MODE_DAILY, SUBSCRIBE_MODE_NAME_DAILY);
		salModeMap.put(SUBSCRIBE_MODE_WEEKLY, SUBSCRIBE_MODE_NAME_WEEKLY);
		salModeMap.put(SUBSCRIBE_MODE_TWO_WEEKS, SUBSCRIBE_MODE_NAME_TWO_WEEKS);
		salModeMap.put(SUBSCRIBE_MODE_THREE_WEEKS, SUBSCRIBE_MODE_NAME_THREE_WEEKS);
		salModeMap.put(SUBSCRIBE_MODE_MONTHLY, SUBSCRIBE_MODE_NAME_MONTHLY);
		salModeMap.put(SUBSCRIBE_MODE_TWO_MONTHS, SUBSCRIBE_MODE_NAME_TWO_MONTHS);
		salModeMap.put(SUBSCRIBE_MODE_THREE_MONTHS, SUBSCRIBE_MODE_NAME_THREE_MONTHS);
		salModeMap.put(SUBSCRIBE_MODE_YEARLY, SUBSCRIBE_MODE_NAME_YEARLY);
		return salModeMap;
	}

	@Override
	public BasicResponse saveAdmin(LoggedSession logSession) {
		LoginDetails login = new LoginDetails();
		LoginDetails login2 = new LoginDetails();
		SellerDetails sellerDetails = new SellerDetails();
		Attributes atttri= new Attributes();
				Attributes atttri1= new Attributes();
				Attributes atttri2= new Attributes();
				Attributes atttri3= new Attributes();
		List<Attributes> attriList=new ArrayList<>();
		atttri.setAttributeName("COLOUR");	
		atttri.setAttributeNameAr("اللون");
		atttri.setStatus(1);
		atttri.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		
		
		
		atttri1.setAttributeName("TYPE");	
		atttri1.setAttributeNameAr("اكتب");
		atttri1.setStatus(1);
		atttri1.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		
		
		atttri2.setAttributeName("SIZE");	
		atttri2.setAttributeNameAr("بحجم");
		atttri2.setStatus(1);
		atttri2.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		
		
		atttri3.setAttributeName("WEIGHT");	
		atttri3.setAttributeNameAr("وزن");
		atttri3.setStatus(1);
		atttri3.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		
		
		
        attriList.add(atttri);
        attriList.add(atttri1);
        attriList.add(atttri2);
        attriList.add(atttri3);
        
		
		logger.info(attriList.size()+"attriList________________attriList______________attriList");
		
		login.setEmail(logSession.email);
		login.setPhoneNo(logSession.phone);
		login.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		login.setPassword(logSession.password);
		//login.setRole(ROLL_ID_ADMIN);
		login.setRoleId(ROLL_ID_ADMIN);
		login.setStatus(ACTIVE);
		login.setUserId(0);
		login.setEmailVerified(VERIFIED_EMAIL);
		login.setCompanyId(logSession.companyId);

		login2.setEmail(logSession.email);
		login2.setPhoneNo(logSession.phone);
		login2.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		login2.setPassword(logSession.password);
		//login2.setRole(ROLL_ID_SELLER);
		login2.setRoleId(ROLL_ID_SELLER);
		login2.setStatus(ACTIVE);
		login2.setUserId(0);
		login2.setEmailVerified(VERIFIED_EMAIL);
		login2.setCompanyId(logSession.companyId);

		sellerDetails.setEmailId(logSession.email);
		sellerDetails.setSellerNameAr("أخفى");
		sellerDetails.setSellerName("HIDILE");
		sellerDetails.setSellerNameAr("أخفى");
		sellerDetails.setAddress("kozhikode");
		sellerDetails.setOperatingOfficerId(0);
		sellerDetails.setProfileImage("hidile123");
		sellerDetails.setPhoneNumber(logSession.phone);
		sellerDetails.setStatus(ACTIVE);
		sellerDetails.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		sellerDetails.setCityId(1);
		sellerDetails.setCountryId(1);
		sellerDetails.setDelivaryStatus(0);
		sellerDetails.setIsCod(1);
		sellerDetails.setIsOnline(1);
		sellerDetails.setDescription("good");
		sellerDetails.setDescriptionAr("good");
		sellerDetails.setLatitude(0);
		sellerDetails.setLongitude(0);
		sellerDetails.setMainCategoryId(0);
		sellerDetails.setNoOfReviews(0);
		sellerDetails.setPriceStarts(0);
		// sellerDetails.setUserId(userId);
		sellerDetails.setWebAddress("www.hidile.com");
		sellerDetails.setRating(0);
		sellerDetails.setRank(0);
		sellerDetails.setWorkingHours("2");
		sellerDetails.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));

		Map<String, Object> map = generalDao.saveAdmin(login, sellerDetails, login2,attriList);
		BasicResponse res = new BasicResponse();
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		if (map.get(ERROR_MESSAGE_ARABIC) != null) {
			res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse changePassword(String oldPass, String newPass, long userId) {
		Map<String, Object> map = generalDao.changePassword(oldPass, newPass, userId);
		BasicResponse basic = new BasicResponse();
		basic.errorCode = (int) map.get(ERROR_CODE);
		basic.errorMessage = (String) map.get(ERROR_MESSAGE);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			LoginDetails loginReturn = (LoginDetails) map.get("LOGIN");

			LoggedSession log = null;
			log = modelMapper.map(loginReturn, LoggedSession.class);
			log.phone = loginReturn.getPhoneNo();
			basic.object = log;

		}
		if (map.get(ERROR_MESSAGE_ARABIC) != null) {
			basic.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		}
		return basic;
	}
	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public BasicResponse getAllCustomers(long customerId, long cityId,
	 * long countryId, long userId, int status, long fromDate, long toDate, int
	 * currentIndex, int rowPerPage,String customerName) { BasicResponse
	 * basicResponse = null; Map<String, Object> map =
	 * generalDao.getAllCustomers(customerId, userId, fromDate, toDate,cityId,
	 * countryId, status, currentIndex, rowPerPage,customerName); if ((int)
	 * map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) { basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = 2; basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); return basicResponse; } basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = ERROR_CODE_NO_ERROR;
	 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
	 * basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC); if
	 * ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) { Map<Long,String>
	 * countryMap=inputService.getAllCountry(1); Map<Long,String>
	 * cityMap=inputService.getAllCities(0,1); List<AddCustomer> customerVoList =
	 * new ArrayList<>(); List<Object[]> customerList = (List<Object[]>)
	 * map.get("customerList"); AddCustomer customerVo = null; for (Object[] cat :
	 * customerList) { customerVo = modelMapper.map(cat[1], AddCustomer.class);
	 * String totalPay = Misceleneous.instance().convertFromScientificNotation(
	 * Math.round((double) cat[0] * ROUND_OFF_DECMAL_TO) / ROUND_OFF_DECMAL_TO);
	 * customerVo.setTotalPay(Double.valueOf(totalPay)); customerVo.setCity((long)
	 * cat[2]); customerVo.setCountry((long) cat[3]);
	 * customerVo.setCityName(cityMap.get(customerVo.getCity()));
	 * customerVo.setCountryName(countryMap.get(customerVo.getCountry()));
	 * 
	 * customerVoList.add(customerVo); } if (customerId > 0) { basicResponse.object
	 * = customerVoList.get(0); } else { basicResponse.object = customerVoList; }
	 * long totalRecords=(long) map.get(TOTAL_COUNT);
	 * basicResponse.totalRecords=totalRecords; } return basicResponse; }
	 */

	@Override
	public BasicResponse saveCustomer(AddCustomer signup) 
	{
		BasicResponse res = new BasicResponse();
		Map<String, Object> returnMap = new HashMap<>();
		CustomerDetails customerSignup = new CustomerDetails();
		customerSignup.setPhoneNo(signup.phoneNo);
		if (signup.customerName == null) {
			customerSignup.setCustomerName("NA");
		} else {
			customerSignup.setCustomerName(signup.customerName);
		}
		customerSignup.setDeviceId("0");
		customerSignup.setEmailId(signup.emailId);
		customerSignup.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		customerSignup.setStatus(ACTIVE);
		customerSignup.setCustomerId(0);
		customerSignup.setAddress(signup.address);
		customerSignup.setUserId(0);
		customerSignup.setProfileImage(signup.profileImage);
		customerSignup.setLatitude(signup.latitude);
		customerSignup.setLongitude(signup.longitude);
		customerSignup.setPhoneNo(signup.phoneNo);
		customerSignup.setProfileImage(signup.profileImage);
		customerSignup.setCompanyId(signup.companyId);
		customerSignup.setAddressAr(signup.addressAr);
		customerSignup.setCustomerNameAr(signup.customerNameAr);
		customerSignup.setZipCode(signup.zipCode);

		customerSignup.setValidate(INACTIVE);

		customerSignup.setCountryCode(signup.countryCode);


		LoginDetails login = new LoginDetails();
		login.setEmail(signup.emailId);
		login.setCompanyId(signup.companyId);
		login.setCountryCode(signup.countryCode);
		login.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));

		if (signup.password != null || !signup.equals("")) {
			login.setPassword(signup.password);
		} else {
			login.setPassword("NA");
		}
		login.setPhoneNo(signup.phoneNo);
		if (signup.getCustomerId() > 0) {
			customerSignup.setCustomerId(signup.customerId);
			login.setUserId(customerSignup.getUserId());

		}
		login.setCountry(signup.getCountry());
		login.setCity(signup.getCity());
	//	login.setRole(ROLL_ID_CUSTOMER);
		login.setRoleId(ROLL_ID_CUSTOMER);
		login.setStatus(ACTIVE);
		login.setValidate(INACTIVE);
		if (TESTING)
		{
			login.setEmailVerified(VERIFIED_EMAIL);
		} else {
			login.setEmailVerified(NON_VERIFIED_EMAIL);
		}
		login.setToken("NA");
		Map<String, Object> map = generalDao.saveCustomer(customerSignup, login);
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) 
		{
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
			customerSignup = (CustomerDetails) map.get("CUSTMOER");
			if (signup.customerId <= 0) 
			
			{
				signup.userId = customerSignup.getUserId();
			}
			LoginDetails loginReturn = (LoginDetails) map.get("LOGIN");

			LoggedSession deliveryBoyVo = null;
			deliveryBoyVo = modelMapper.map(loginReturn, LoggedSession.class);
			deliveryBoyVo.phone=loginReturn.getPhoneNo();
			res.object = deliveryBoyVo;
			
			//added for sms otp
			
			String mail=customerSignup.getPhoneNo();
			int phoneOrWeb=1;
			BasicResponse res1 = checkWeatherEmailPresent(mail,51,PHONE);
			String link=null;
			if (res1.errorCode == ERROR_CODE_NO_ERROR) 
			{
				LoggedSession session = (LoggedSession) res1.object;
				res1.object=null;
				logger.info("556uooooooooooooooooooooooooooooooo");
				int otp = otpService.generateOTP(mail);
				logger.info(otp);
				MailVo mailVo = new MailVo();
				mailVo.setMailFrom("support@pentalsaudi.com");
				mailVo.setMailSubject("Password reset link");
				mailVo.setMailTo(mail);
				Map<String, Object> model = new HashMap<String, Object>();
				model.put("firstName", session.userName);
				model.put("location", "Riyad,Saudi arabia");
				model.put("signature", "www.pentalsaudi.com");
				if(phoneOrWeb == PHONE) 
				{
					StringBuffer textData = new StringBuffer("Dear customer,your otp for activation is ");
					textData.append(otp);
					logger.info(otp);
					//final String uri = "http://my.otpsmsapi.com/vendorsms/pushsms.aspx?user=NAWAS&password=Nawas786&msisdn="+customerSignup.getCountryCode()+""
							//+ mail + "&sid=Pental&msg=" + textData + "&fl=0";
					
	final String uri =	"http://my.forwardvaluesms.com/vendorsms/pushsms.aspx?apiKey=20e90577-df92-4381-b37f-ae62c5fca6d7&clientid=43dfaf3a-b2eb-479e-88cd-fe0b733dafe5&msisdn="+customerSignup.getCountryCode() + ""+ mail +"&sid=Pental&msg=" + textData + "&fl=0";	
	logger.info(uri);
					RestTemplate restTemplate = new RestTemplate();
					String result = restTemplate.getForObject(uri, String.class);
					ObjectMapper mapper = new ObjectMapper();
					try {
						Map<String,Object> map1 = mapper.readValue(result, Map.class);
						logger.info(map.get("ErrorCode"));
						if(!map1.get("ErrorCode").equals("000")) 
						{
							res.errorCode=0;
							res.errorMessage=(String) map1.get("ErrorMessage");
							return res;
						}
					} catch (JsonParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JsonMappingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			
		} else {
			res.errorCode = (int) map.get(ERROR_CODE);
			res.errorMessage = (String) map.get(ERROR_MESSAGE);
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
		}

		
	}
		return res;
	}
	
	@Override
	public BasicResponse  validateCustomer(long userId,int status)
	{
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = generalDao.validateCustomer(userId, ROLL_ID_CUSTOMER, status);
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		if (map.get(ERROR_MESSAGE_ARABIC) != null) {
			res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}
	
	
	@Override
	public BasicResponse activateCustomer(long userId, int status) {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = generalDao.activateAccount(userId, ROLL_ID_CUSTOMER, status);
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		if (map.get(ERROR_MESSAGE_ARABIC) != null) 
		{
			res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			res.object=map.get("UserList");
			logger.info(res.object+"res.objectres.objectres.objectres.objectres.objectres.objectres.object");
		}
		return res;
	}
	
	@Override
	public BasicResponse checkWeatherEmailPresent(String email,int role,int phoneOrWeb) {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = generalDao.checkWeatherEmailPresent(email,role,phoneOrWeb);
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		if (map.get(ERROR_MESSAGE_ARABIC) != null) {
			res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		}
		if (res.errorCode == ERROR_CODE_NO_ERROR) {
			LoggedSession lSession = modelMapper.map(map.get(DATA), LoggedSession.class);
			res.object = lSession;
		}
		return res;
	}
	
	
	@Override
	public BasicResponse  savePromoters(AddCustomer signup)
	{
		BasicResponse res = new BasicResponse();
		Map<String, Object> returnMap = new HashMap<>();
		CustomerDetails customerSignup = new CustomerDetails();
		customerSignup.setPhoneNo(signup.phoneNo);
		if (signup.customerName == null) 
		{
			customerSignup.setCustomerName("NA");
		} else {
			customerSignup.setCustomerName(signup.customerName);
		}
		customerSignup.setDeviceId("0");
		customerSignup.setEmailId(signup.getEmailId());
		customerSignup.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		customerSignup.setStatus(signup.getStatus());

		customerSignup.setCustomerId(0);
		customerSignup.setAddress(signup.address);
		customerSignup.setUserId(0);
		customerSignup.setProfileImage(signup.profileImage);
		customerSignup.setLatitude(signup.latitude);
		customerSignup.setLongitude(signup.longitude);
		customerSignup.setPhoneNo(signup.phoneNo);
		customerSignup.setProfileImage(signup.profileImage);
		customerSignup.setCompanyId(signup.companyId);
		customerSignup.setAddressAr(signup.addressAr);
		customerSignup.setCustomerNameAr(signup.customerNameAr);
		customerSignup.setZipCode(signup.zipCode);

		LoginDetails login = new LoginDetails();
		login.setEmail(signup.emailId);
		login.setCompanyId(signup.companyId);
		login.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));

		if (signup.password != null || !signup.equals("")) {
			login.setPassword(signup.password);
		} else {
			login.setPassword("NA");
		}
		login.setPhoneNo(signup.phoneNo);
		if (signup.getCustomerId() > 0) 
		{
			customerSignup.setCustomerId(signup.customerId);
			customerSignup.setStatus(signup.getStatus());
			login.setUserId(customerSignup.getUserId());
			

		}
		login.setCountry(signup.getCountry());
		login.setCity(signup.getCity());
		login.setRoleId(ROLL_ID_PROMOTER);
		login.setStatus(signup.getStatus());
		if (TESTING) {
			login.setEmailVerified(VERIFIED_EMAIL);
		} else {
			login.setEmailVerified(NON_VERIFIED_EMAIL);
		}
		login.setToken("NA");
		Map<String, Object> map = generalDao.savePromoters(customerSignup, login);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			res.errorCode = ERROR_CODE_NO_ERROR;
			res.errorMessage = MESSAGE_SUCSESS;
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
			customerSignup = (CustomerDetails) map.get("CUSTMOER");
			if (signup.customerId <= 0) {
				signup.userId = customerSignup.getUserId();
			}
			/*LoginDetails loginReturn = (LoginDetails) map.get("LOGIN");
logger.info(loginReturn+"loginReturn---------------------------------------------------------------------------------");
			LoggedSession deliveryBoyVo = null;
			deliveryBoyVo = modelMapper.map(loginReturn, LoggedSession.class);
			res.object = deliveryBoyVo;*/
		} else {
			res.errorCode = (int) map.get(ERROR_CODE);
			res.errorMessage = (String) map.get(ERROR_MESSAGE);
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
		}

		return res;
	}
		@Override
	public BasicResponse generatePromocode(PromocodeDetailsVo addUser) {
		BasicResponse basicResponse = new BasicResponse();
		PromocodeDetails promoDetails = new PromocodeDetails();
		promoDetails.setFlatOffer(addUser.getFlatOffer());
		promoDetails.setValidFrom(addUser.getValidFrom());
		promoDetails.setValidTo(addUser.getValidFrom());
		promoDetails.setPercentage(addUser.getPercentage());

		promoDetails.setUserId(addUser.getUserId());
		logger.info(TimeUtils.instance().getStartOfCurrentDay() + "TimeUtils.instance().getStartOfCurrentDay()");
		logger.info(addUser.getValidFrom() + "addUser.getValidFrom()");
		/*
		 * if (addUser.getValidFrom() <= TimeUtils.instance().getStartOfCurrentDay()) {
		 * basicResponse = new BasicResponse(); basicResponse.errorCode = 7;
		 * basicResponse.errorMessage = " promocode  is only valid from today onwards ";
		 * return basicResponse; }
		 */
		int codeLength = 10;
		char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new SecureRandom();
		for (int i = 0; i < codeLength; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		String output = sb.toString();
		String pental = "PENTAL" + output;
		logger.info(pental + "promocode-----------------------------------------------------------");
		/*
		 * if(pental.length() > 0) { promoDetails.setPromocode(pental); Map<String,
		 * Object> map = generalDao.generatePromocode(promoDetails);
		 * 
		 * basicResponse.errorCode = (int) map.get(ERROR_CODE);
		 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE); return
		 * basicResponse; }
		 */
		basicResponse = new BasicResponse();
		basicResponse.errorCode = 1;
		basicResponse.errorMessage = "success";
		basicResponse.errorMessageArabic = "نجاح";
		basicResponse.object = pental;

		return basicResponse;
	}

	public BasicResponse savePromocode(PromocodeDetailsVo addUser) {
		BasicResponse basicResponse = new BasicResponse();
		PromocodeDetails promoDetails = new PromocodeDetails();
		promoDetails.setFlatOffer(addUser.getFlatOffer());
		promoDetails.setValidFrom(addUser.getValidFrom());
		promoDetails.setValidTo(addUser.getValidTo());
		promoDetails.setPercentage(addUser.getPercentage());
		promoDetails.setPromocode(addUser.getPromocode());
		promoDetails.setStatus(addUser.getStatus());
		promoDetails.setCodeId(addUser.getCodeId());
		promoDetails.setUserId(addUser.getUserId());
		promoDetails.setPromocode(addUser.getPromocode());
		promoDetails.setMinimumAmount(addUser.getMinimumAmount());


		Map<String, Object> map = generalDao.savePromocode(promoDetails);

		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) 
		{
			basicResponse.errorCode = ERROR_CODE_NO_ERROR;
			basicResponse.errorMessage = MESSAGE_SUCSESS;
			basicResponse.errorCode = 1;
			basicResponse.errorMessageArabic = "نجاح";
		} 
		else 
		{
			basicResponse.errorCode = (int) map.get(ERROR_CODE);
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		}

		

		return basicResponse;
	}

	@Override
	public Map<Long, String> getAllCustomerMap() {
		List<CustomerDetails> serviceList = null;
		Map<Long, String> allserviceMap = null;
		Map<String, Object> map = generalDao.getAllCustomers(0, 0, 0, 0, ACTIVE, 0, 0);
		serviceList = (List<CustomerDetails>) map.get("customerList");
		if (serviceList != null && !serviceList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (CustomerDetails item : serviceList) {

				allserviceMap.put(item.getUserId(), item.getEmailId());
			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, String[]> getAllCustomerMap1() {
		List<CustomerDetails> placeList = null;
		Map<Long, String[]> allplaceMap = null;
		Map<String, Object> map = generalDao.getAllCustomers(0, 0, 0, 0, ACTIVE, 0, 0);
		placeList = (List<CustomerDetails>) map.get("customerList");
		if (placeList != null && !placeList.isEmpty()) {
			allplaceMap = new HashMap<>();
			for (CustomerDetails c : placeList) {
				String[] str = { c.getCustomerName(), c.getCustomerNameAr() };
				allplaceMap.put(c.getUserId(), str);
			}

		}
		return allplaceMap;
	}

	@Override
	public BasicResponse saveOrUpdateSellerDetails(SellerVo sellerDetailsVo) {

		BasicResponse res = new BasicResponse();

		SellerDetails sellerDetails = new SellerDetails();
		sellerDetails.setPhoneNumber(sellerDetailsVo.phoneNumber);
		if (sellerDetailsVo.sellerName == null) {
			sellerDetails.setSellerName("NA");
		} else {
			sellerDetails.setSellerName(sellerDetailsVo.sellerName);
		}
		sellerDetails.setCompanyId(sellerDetailsVo.companyId);
		sellerDetails.setEmailId(sellerDetailsVo.emailId);
		sellerDetails.setDescriptionAr(sellerDetailsVo.descriptionAr);
		sellerDetails.setDescription(sellerDetailsVo.description);
		sellerDetails.setAddress(sellerDetailsVo.address);
		sellerDetails.setProfileImage(sellerDetailsVo.profileImage);
		sellerDetails.setWorkingHours(sellerDetailsVo.workingHours);
		sellerDetails.setWebAddress(sellerDetailsVo.webAddress);
		sellerDetails.setLatitude(sellerDetailsVo.latitude);
		sellerDetails.setDelivaryStatus(sellerDetailsVo.delivaryStatus);
		// sellerDetails.setServiceOrProduct(sellerDetailsVo.serviceOrProduct);
		sellerDetails.setCityId(sellerDetailsVo.cityId);
		sellerDetails.setIsCod(sellerDetailsVo.isCod);
		sellerDetails.setIsOnline(sellerDetailsVo.isOnline);
		sellerDetails.setCountryId(sellerDetailsVo.countryId);
		sellerDetails.setContactName(sellerDetailsVo.contactName);
		sellerDetails.setRank(sellerDetailsVo.rank);
		sellerDetails.setStatus(sellerDetailsVo.status);
		sellerDetails.setTelephoneNo(sellerDetailsVo.telephoneNo);
		sellerDetails.setLongitude(sellerDetailsVo.longitude);
		sellerDetails.setOperatingOfficerId(1);
		sellerDetails.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		sellerDetails.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		LoginDetails login = new LoginDetails();
		login.setEmail(sellerDetailsVo.emailId);
		login.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		if (sellerDetailsVo.password != null || !sellerDetailsVo.equals("")) {
			login.setPassword(sellerDetailsVo.password);
		} else {
			login.setPassword("NA");
		}
		login.setPhoneNo(sellerDetailsVo.phoneNumber);
		if (sellerDetailsVo.getSellerId() > 0) {
			sellerDetails.setSellerId(sellerDetailsVo.sellerId);
			login.setUserId(sellerDetails.getSellerId());

		}
		login.setCompanyId(sellerDetailsVo.companyId);
		login.setCountry(sellerDetailsVo.getCountryId());
		login.setCity(sellerDetailsVo.getCityId());
		login.setEntryId(1);
		login.setRoleId(ROLL_ID_SELLER);
		login.setStatus(ACTIVE);
		if (TESTING) {
			login.setEmailVerified(VERIFIED_EMAIL);
		} else {
			login.setEmailVerified(NON_VERIFIED_EMAIL);
		}

		Map<String, Object> map = generalDao.saveOrUpdateSellerDetails(sellerDetails, login);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			res.errorCode = ERROR_CODE_NO_ERROR;
			res.errorMessage = MESSAGE_SUCSESS;
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
			sellerDetails = (SellerDetails) map.get("SELLER");
			if (sellerDetailsVo.sellerId <= 0) {
				sellerDetailsVo.sellerId = sellerDetails.getSellerId();

			}
		} else {
			res.errorCode = (int) map.get(ERROR_CODE);
			res.errorMessage = (String) map.get(ERROR_MESSAGE);
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
		}

		return res;
	}

	@Override
	public BasicResponse saveComapnay(CompanyVo companyVo) {

		BasicResponse res = new BasicResponse();

		CompanyDetails companyDetails = new CompanyDetails();
		companyDetails.setPhone(companyVo.phone);
		if (companyVo.companyName == null) {
			companyDetails.setCompanyName("NA");
		} else {
			companyDetails.setCompanyName(companyVo.companyName);
		}

		companyDetails.setEmail(companyVo.email);
		companyDetails.setCityId(companyVo.cityId);
		companyDetails.setCountryId(companyVo.countryId);
		companyDetails.setCompanyAddress(companyVo.companyAddress);
		companyDetails.setCompanyType(companyVo.companyType);
		companyDetails.setFirstName(companyVo.firstName);
		companyDetails.setGstin(companyVo.gstin);
		companyDetails.setLastName(companyVo.lastName);
		companyDetails.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		companyDetails.setLogo(companyVo.logo);
		companyDetails.setMobile(companyVo.mobile);
		companyDetails.setPan(companyVo.pan);
		companyDetails.setPhone(companyVo.phone);
		companyDetails.setPincode(companyVo.pincode);
		companyDetails.setStatus(ACTIVE);
		companyDetails.setWebsite(companyVo.website);
		companyDetails.setYear(companyVo.year);
		companyDetails.setUserId(1);

		LoginDetails login = new LoginDetails();
		login.setEmail(companyVo.email);
		login.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		if (companyVo.password != null || !companyVo.equals("")) {
			login.setPassword(companyVo.password);
		} else {
			login.setPassword("NA");
		}
		login.setPhoneNo(companyVo.phone);
		if (companyVo.getCompanyId() > 0) {
			companyVo.setCompanyId(companyVo.companyId);
			login.setUserId(companyVo.getCompanyId());

		}

		login.setCountry(companyVo.getCountryId());
		login.setCity(companyVo.getCityId());
		login.setEntryId(1);
		login.setRoleId(ROLL_ID_COMPANY);
		login.setStatus(ACTIVE);
		if (TESTING) {
			login.setEmailVerified(VERIFIED_EMAIL);
		} else {
			login.setEmailVerified(NON_VERIFIED_EMAIL);
		}

		Map<String, Object> map = generalDao.saveComapnay(companyDetails, login);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			res.errorCode = ERROR_CODE_NO_ERROR;
			res.errorMessage = MESSAGE_SUCSESS;
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
			companyDetails = (CompanyDetails) map.get("COMPANY");
			if (companyVo.companyId <= 0) {
				companyVo.companyId = companyVo.getCompanyId();

			}
		} else {
			res.errorCode = (int) map.get(ERROR_CODE);
			res.errorMessage = (String) map.get(ERROR_MESSAGE);
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
		}

		return res;
	}

	/*
	 * @Override public BasicResponse saveOrUpdateDeliveryBoy(DeliveryBoyVo
	 * deliveryBoyVo) {
	 * 
	 * BasicResponse res = new BasicResponse();
	 * 
	 * DeliveryBoy deliveryBoy = new DeliveryBoy();
	 * deliveryBoy.setPhoneNumber(deliveryBoyVo.phoneNumber);
	 * if(deliveryBoyVo.deliveryBoyName==null) {
	 * deliveryBoy.setDeliveryBoyName("NA"); } else {
	 * deliveryBoy.setDeliveryBoyName(deliveryBoyVo.deliveryBoyName); }
	 * deliveryBoy.setEmailId(deliveryBoyVo.emailId);
	 * 
	 * deliveryBoy.setStatus(ACTIVE); deliveryBoy.setIsAvailable(0);
	 * deliveryBoy.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE));
	 * 
	 * deliveryBoy.setAddress(deliveryBoyVo.address);
	 * 
	 * deliveryBoy.setPhoneNumber(deliveryBoyVo.phoneNumber);
	 * 
	 * deliveryBoy.setProfileImage(deliveryBoyVo.profileImage);
	 * 
	 * LoginDetails login=new LoginDetails(); login.setEmail(deliveryBoyVo.emailId);
	 * login.setLast_updated_time(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); if(deliveryBoyVo.password != null ||
	 * !deliveryBoyVo.equals("")) { login.setPassword(deliveryBoyVo.password); }else
	 * { login.setPassword("NA"); } login.setPhoneNo(deliveryBoyVo.phoneNumber);
	 * if(deliveryBoyVo.getDeliveryBoyId() > 0){
	 * deliveryBoy.setDeliveryBoyId(deliveryBoyVo.deliveryBoyId);
	 * login.setUserId(deliveryBoy.getDeliveryBoyId());
	 * 
	 * 
	 * } login.setEntryId(1); login.setCountry(deliveryBoyVo.getCountry());
	 * login.setCity(deliveryBoyVo.getCity()); login.setRole(ROLL_ID_DELIVERY_BOY);
	 * login.setStatus(ACTIVE); if(TESTING) {
	 * login.setEmailVerified(VERIFIED_EMAIL); }else {
	 * login.setEmailVerified(NON_VERIFIED_EMAIL); }
	 * 
	 * Map<String, Object> map =
	 * generalDao.saveOrUpdateDeliveryBoy(deliveryBoy,login); if
	 * ((int)map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) { res.errorCode =
	 * ERROR_CODE_NO_ERROR; res.errorMessage = MESSAGE_SUCSESS;
	 * if(map.get(ERROR_MESSAGE_ARABIC) != null) { res.errorMessageArabic=(String)
	 * map.get(ERROR_MESSAGE_ARABIC); } deliveryBoy=(DeliveryBoy)
	 * map.get("DELIVERYBOY"); if(deliveryBoyVo.deliveryBoyId<= 0) {
	 * deliveryBoyVo.deliveryBoyId=deliveryBoy.getDeliveryBoyId();
	 * 
	 * } } else { res.errorCode = (int) map.get(ERROR_CODE); res.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); if(map.get(ERROR_MESSAGE_ARABIC) != null) {
	 * res.errorMessageArabic=(String) map.get(ERROR_MESSAGE_ARABIC); } }
	 * 
	 * return res;
	 * 
	 * 
	 * }
	 */

	/*
	 * @Override public BasicResponse sellerLogin(String email,String password) {
	 * 
	 * SellerDetails sellerDetails=new SellerDetails(); Map<String, Object> map =
	 * generalDao.sellerLogin(email,password); BasicResponse res = new
	 * BasicResponse(); if ((int)map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
	 * res.errorCode = ERROR_CODE_NO_ERROR; res.errorMessage = MESSAGE_SUCSESS;
	 * if(map.get(ERROR_MESSAGE_ARABIC) != null) { res.errorMessageArabic=(String)
	 * map.get(ERROR_MESSAGE_ARABIC); } sellerDetails=(SellerDetails)
	 * map.get("SELLER");
	 * 
	 * } else { res.errorCode = (int) map.get(ERROR_CODE); res.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); if(map.get(ERROR_MESSAGE_ARABIC) != null) {
	 * res.errorMessageArabic=(String) map.get(ERROR_MESSAGE_ARABIC); } }
	 * 
	 * return res; }
	 */

	@Override
	public BasicResponse getCustomerFrom(String userName, String phoneNo) {
		BasicResponse basicResponse = null;
		if (userName.trim().equals("") || "".equals(phoneNo.trim())) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 9;
			basicResponse.errorMessage = MESSAGE_NULL_VALUE;
			return basicResponse;
		}
		CustomerDetails user = generalDao.getCustomerFrom(userName, phoneNo);
		if (user == null) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = MESSAGE_NOT_FOUND;
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.object = user;
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = MESSAGE_SUCSESS;
		return basicResponse;
	}

	@Override
	public BasicResponse authenticate(String userName, String passWord, int role) {
		BasicResponse res = null;
		if ("".equals(userName) || "".equals(passWord)) {
			res = new BasicResponse();
			res.errorCode = 4;
			res.errorMessage = MESSAGE_NULL_VALUE;
			return res;
		} else {
			Map<String, Object> map = generalDao.aunthenticate(userName, passWord, role);
			res = new BasicResponse();
			if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
				LoginDetails user = (LoginDetails) map.get("loggedUser");
				LoggedSession logSession = new LoggedSession();
				logSession.email = user.getEmail();
				logSession.password = user.getPassword();
				logSession.phone = user.getPhoneNo();
				logSession.roleId = user.getRoleId();
				logSession.status = user.getStatus();
				logSession.userId = user.getUserId();
				logSession.entryId = user.getEntryId();
				logSession.companyId = user.getCompanyId();
				logSession.countryCode = user.getCountryCode();
				logSession.validate= user.getValidate();
				logSession.token = user.getToken();
				// logSession.cityId=user.getCity();
				// logSession.countryId=user.getCountry();
				res.object = logSession;
			}
			res.errorCode = (int) map.get(ERROR_CODE);
			res.errorMessage = (String) map.get(ERROR_MESSAGE);
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
			return res;
		}
	}

	@Override
	public Long getIdFromName(String name, Map<Long, String> map) {

		if (map != null && name != null && !"".equals(name.trim())) {

			Long itemId = null;
			Set<Entry<Long, String>> entrySet = map.entrySet();
			for (Entry<Long, String> entry : entrySet) {
				if (name.equals(entry.getValue())) {
					itemId = entry.getKey();
					break;
				}
			}
			return itemId;
		} else {

			return null;
		}
	}

	@Override
	public CustomerDetails getCustomerFromUserId(long customerId) {
		if (customerId <= 0) {
			return null;
		}
		logger.info(customerId + "customerId___________________**************_______________");
		return generalDao.getCustomerFromId(customerId);
	}

	@Override
	public TaxManagement getTax(long taxId) {
		if (taxId <= 0) {
			return null;
		}
		logger.info(taxId + "taxId__________________________________");
		return generalDao.getTax(taxId);
	}
	
	
	
	 public Offer getOffer(long offerId)
	 {
		 if (offerId <= 0) {
				return null;
			}
			logger.info(offerId + "offerId__________________________________");
			return generalDao.getOffer(offerId);
	 }
	
	
	
	
	

	@Override
	public SellerDetails getSellerFromUserId(long customerId) {
		if (customerId <= 0) {
			return null;
		}
		logger.info(customerId + "customerId__________________________________");
		return generalDao.getSellerFromId(customerId);

	}

	/*
	 * @Override public BasicResponse getAllCustomers(long customerId,long
	 * userId,long fromDate,long toDate,int status,int firstIndex,int rowPerPage) {
	 * BasicResponse res = null; List<AddCustomer> userVos = null; Map<String,
	 * Object> map = generalDao.getAllCustomers(customerId,userId, fromDate,
	 * toDate,status,firstIndex,rowPerPage); if ((int) map.get(ERROR_CODE) ==
	 * ERROR_CODE_NO_ERROR) { userVos = new ArrayList<>(); List<CustomerDetails>
	 * customerDetails = (List<CustomerDetails>) map.get("customerList");
	 * AddCustomer addCus = null; for (CustomerDetails user : customerDetails) {
	 * addCus = new AddCustomer(); addCus.phoneNo = user.getPhoneNo();
	 * addCus.customerId = user.getCustomerId(); addCus.userId=user.getUserId();
	 * addCus.customerName = user.getCustomerName(); addCus.createdDate =
	 * TimeUtils.instance().getDateStrFromMilSec(user.getCreatedDate());
	 * userVos.add(addCus); } res = new BasicResponse(); res.errorCode =
	 * ERROR_CODE_NO_ERROR; res.errorMessage = MESSAGE_SUCSESS;
	 * if(map.get(ERROR_MESSAGE_ARABIC) != null) { res.errorMessageArabic=(String)
	 * map.get(ERROR_MESSAGE_ARABIC); } if (customerId > 0 || userId > 0) {
	 * res.object = userVos.get(0); } else { res.object = userVos; res.totalRecords=
	 * (long) map.get("TotalCounts"); } return res; } res = new BasicResponse();
	 * res.errorCode = (int) map.get(ERROR_CODE); res.errorMessage = (String)
	 * map.get(ERROR_MESSAGE); if(map.get(ERROR_MESSAGE_ARABIC) != null) {
	 * res.errorMessageArabic=(String) map.get(ERROR_MESSAGE_ARABIC); } return res;
	 * }
	 */

	@Override
	public BasicResponse getDashDetails() {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = generalDao.getGeneralDatas();
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.object = map.get("DATA");
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllDeliveryBoy(String address, long deliveryBoyId, String deliveryBoyName, int firstIndex,
			int rowPerPage, int status[], String phoneNumber) {
		BasicResponse res = null;

		List<DeliveryBoyVo> deliveryBoyVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = generalDao.getAllDeliveryBoy(address, deliveryBoyId, deliveryBoyName, firstIndex,
				rowPerPage, status, phoneNumber);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			List<DeliveryBoy> deliveryBoyList = (List<DeliveryBoy>) dataMap.get("DeliveryBoyList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			DeliveryBoyVo deliveryBoyVo = null;
			for (DeliveryBoy deliveryBoy : deliveryBoyList) {
				deliveryBoyVo = modelMapper.map(deliveryBoy, DeliveryBoyVo.class);
				deliveryBoyVos.add(deliveryBoyVo);
			}
			returnMap.put("DeliveryBoy", deliveryBoyVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = deliveryBoyVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;

	}

	@Override
	public BasicResponse assignOffer(long offerId, long productId) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = generalDao.assignOffer(offerId, productId);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@Override
	public BasicResponse changeDeliveryStatus(long orderId, long deliveryBoyId, int status) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = generalDao.changeDeliveryStatus(orderId, deliveryBoyId, status);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;

	}

	@Override
	public BasicResponse cancellDeriveryBoy(long orderId) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = generalDao.cancellDeriveryBoy(orderId);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@Override
	public BasicResponse changeSellerStatus(long sellerId, int status) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = generalDao.changeSellerStatus(sellerId, status);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;

	}

	@Override
	public BasicResponse changeCustomerStatus(long customerId, int status) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = generalDao.changeCustomerStatus(customerId, status);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;

	}

	@Override
	public BasicResponse changePromocodeStatus(long codeId, int status) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = generalDao.changePromocodeStatus(codeId, status);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllNotification(int firstIndex, int rowPerPage,int orderStatus[],int readStatus,long userId,long notificationId)
	{
		BasicResponse res = null;

		List<NotificationVo> sellerDetailsVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = generalDao.getAllNotification( firstIndex,  rowPerPage, orderStatus, readStatus, userId, notificationId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) 
		{
			List<NotificationDetails> sellerList = (List<NotificationDetails>) dataMap.get("SellerList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			NotificationVo sellerDetailsVo = null;
			Map<Long, String[]> countryMap = inputService.getAllCountry(1);
			Map<Long, String[]> cityMap = inputService.getAllCities(0, 1);
			for (NotificationDetails sellerDetails : sellerList) {
				sellerDetailsVo = modelMapper.map(sellerDetails, NotificationVo.class);
				

				sellerDetailsVos.add(sellerDetailsVo);
			}
			returnMap.put("SellerDetails", sellerDetailsVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = sellerDetailsVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;	
	}
	
	

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllSeller(long sellerId, int firstIndex, int rowPerPage, int stat, String sellerName,
			String phoneNumber, long companyId) {
		BasicResponse res = null;

		List<SellerVo> sellerDetailsVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = generalDao.getAllSeller(sellerId, firstIndex, rowPerPage, stat, sellerName,
				phoneNumber, companyId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			List<SellerDetails> sellerList = (List<SellerDetails>) dataMap.get("SellerList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			SellerVo sellerDetailsVo = null;
			Map<Long, String[]> countryMap = inputService.getAllCountry(1);
			Map<Long, String[]> cityMap = inputService.getAllCities(0, 1);
			for (SellerDetails sellerDetails : sellerList) {
				sellerDetailsVo = modelMapper.map(sellerDetails, SellerVo.class);
				sellerDetailsVo.setIsCod(sellerDetails.getIsCod());
				sellerDetailsVo.setIsOnline(sellerDetails.getIsOnline());
				sellerDetailsVo.setContactName(sellerDetails.getContactName());
				sellerDetailsVo.setLatitude(sellerDetails.getLatitude());
				sellerDetailsVo.setLongitude(sellerDetails.getLongitude());
				sellerDetailsVo.setTelephoneNo(sellerDetails.getTelephoneNo());
				/*
				 * sellerDetailsVo.setCityName(cityMap.get(sellerDetailsVo.getCityId()));
				 * sellerDetailsVo.setCountryName(countryMap.get(sellerDetailsVo.getCountryId())
				 * );
				 */

				if (countryMap != null) {
					String names[] = countryMap.get(sellerDetailsVo.getCountryId());
					sellerDetailsVo.setCountryName(names[0]);
					// sellerDetailsVo.setCountryNameAr(names[1]);
				}
				if (cityMap != null) {
					String names[] = cityMap.get(sellerDetailsVo.getCityId());
					sellerDetailsVo.setCityName(names[0]);
					// sellerDetailsVo.setCityNameAr(names[1]);
				}
				sellerDetailsVos.add(sellerDetailsVo);
			}
			returnMap.put("SellerDetails", sellerDetailsVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = sellerDetailsVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}


	@Override
	public BasicResponse updateCustomer(AddCustomer signup, int role) {
		BasicResponse res = new BasicResponse();
		CustomerDetails customerSignup = new CustomerDetails();
		customerSignup.setPhoneNo(signup.phoneNo);
		if (signup.customerName == null) {
			customerSignup.setCustomerName("NA");
		} else {
			customerSignup.setCustomerName(signup.customerName);
		}
		customerSignup.setDeviceId("0");
		customerSignup.setPhoneNo(signup.phoneNo);
		customerSignup.setEmailId(signup.emailId.toLowerCase());
		customerSignup.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		logger.info(signup.Status+"status____________________");
		customerSignup.setStatus(signup.Status);
		customerSignup.setCustomerId(0);
		customerSignup.setLatitude(signup.latitude);
		customerSignup.setLongitude(signup.longitude);
		customerSignup.setProfileImage(signup.profileImage);
		customerSignup.setAddress(signup.address);
		customerSignup.setZipCode(signup.zipCode);
		customerSignup.setUserId(0);
		
		
		
		
		
		if (signup.getCustomerId() > 0) {
			customerSignup.setCustomerId(signup.customerId);
		}
		LoginDetails login=null;
		/*if(role == ROLL_ID_CUSTOMER)
		{
		login = new LoginDetails();
		login.setEmail(signup.emailid);
		login.setLast_updated_time(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		if (signup.password != null || !signup.equals("")) {
			login.setPassword(signup.password);
		} else {
			login.setPassword("NA");
		}
		login.setPhoneNo(signup.phoneNo);
		if (signup.getCustomerId() > 0) {
			login.setUserId(customerSignup.getUserId());
		}
		login.setCountry(signup.getCountry());
	    login.setCity(signup.getCity());
		login.setRole(ROLL_ID_CUSTOMER);
		login.setStatus(signup.getStatus());
		if (TESTING) {
			login.setEmailVerified(VERIFIED_EMAIL);
		} else {
			login.setEmailVerified(NON_VERIFIED_EMAIL);
		}
		login.setToken("NA");
		}*/
		Map<String, Object> map = generalDao.saveCustomer(customerSignup, null);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			res.errorCode = ERROR_CODE_NO_ERROR;
			res.errorMessage = MESSAGE_SUCSESS;
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
			customerSignup = (CustomerDetails) map.get("CUSTMOER");
			if (signup.customerId <= 0) {
				signup.userId = customerSignup.getUserId();
			}
		} else {
			res.errorCode = (int) map.get(ERROR_CODE);
			res.errorMessage = (String) map.get(ERROR_MESSAGE);
			if (map.get(ERROR_MESSAGE_ARABIC) != null) {
				res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			}
		}
		return res;
	}
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllCustomers(long customerId, long cityId, long countryId, long userId, int status,
			long fromDate, long toDate, int currentIndex, int rowPerPage, long companyId, String customerName,
			String emailid, String phoneNo, String customerNameAr) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = generalDao.getAllCustomers(customerId, userId, fromDate, toDate, cityId, countryId,
				status, currentIndex, rowPerPage, companyId, customerName, emailid, phoneNo, customerNameAr);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) 
		{
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) 
		{
			Map<Long, String> countryMap = inputService.getAllCountry1(1);
			Map<Long, String> cityMap = inputService.getAllCities1(0, 1);

			Map<Long, String> countryMapAr = inputService.getAllCountryAr(1);
			Map<Long, String> cityMapAr = inputService.getAllCitiesAr(0, 1);
			List<AddCustomer> customerVoList = new ArrayList<>();
			List<Object[]> customerList = (List<Object[]>) map.get("customerList");

			AddCustomer customerVo = null;
			for (Object[] cat : customerList) 
			{
				customerVo = modelMapper.map(cat[1], AddCustomer.class);
				String totalPay = Misceleneous.instance().convertFromScientificNotation(
						Math.round((double) cat[0] * ROUND_OFF_DECMAL_TO) / ROUND_OFF_DECMAL_TO);
				customerVo.setTotalPay(Double.valueOf(totalPay));
				customerVo.setCity((long) cat[2]);
				customerVo.setCountry((long) cat[3]);
				customerVo.setCityName(cityMap.get(customerVo.getCity()));
				customerVo.setCountryName(countryMap.get(customerVo.getCountry()));
				customerVo.setCityNameAr(cityMapAr.get(customerVo.getCity()));
				customerVo.setCountryNameAr(countryMapAr.get(customerVo.getCountry()));
				
				customerVoList.add(customerVo);
			}
			if (customerId > 0) {
				basicResponse.object = customerVoList.get(0);
			} else {
				basicResponse.object = customerVoList;
			}
			long totalRecords = (long) map.get("TOTALCOUNT");
			
			basicResponse.totalRecords = totalRecords;
		}
		return basicResponse;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllPromoters(long customerId,long cityId,long countryId,long userId,int status,long fromDate,long toDate,int currentIndex,int rowPerPage,long companyId,String customerName,String emailid,String phoneNo,String customerNameAr)
	{
		BasicResponse basicResponse = null;
		Map<String, Object> returnMap = new HashMap<>();
		Map<String, Object> map = generalDao.getAllPromoters( customerId, cityId, countryId, userId, status, fromDate, toDate, currentIndex, rowPerPage, companyId, customerName, emailid, phoneNo, customerNameAr);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Long, String> countryMap = inputService.getAllCountry1(1);
			Map<Long, String> cityMap = inputService.getAllCities1(0, 1);

			Map<Long, String> countryMapAr = inputService.getAllCountryAr(1);
			Map<Long, String> cityMapAr = inputService.getAllCitiesAr(0, 1);
			List<AddCustomer> customerVoList = new ArrayList<>();
			List<CustomerDetails> customerList = (List<CustomerDetails>) map.get("customerList");
logger.info(customerList.size()+"customerList size________________________________customerList size");
			AddCustomer customerVo = null;
			
			for (CustomerDetails cat : customerList) 
			{
				logger.info(customerList+"customerList________________________________customerList");
				customerVo = modelMapper.map(cat, AddCustomer.class);
				customerVo.setEmailId(cat.getEmailId());
				
			
				//-----------------------------------
				
				Map<String, Object> map1 = generalDao.getAllUsers(customerVo.getUserId());
				if ((int) map1.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) 
				{
					List<LoggedSession> loggedSessions = new ArrayList<>();
					
					List<LoginDetails> list = (List<LoginDetails>) map1.get(DATA);
					
					logger.info(list+"list________________________________list");
					customerVo.setCity(list.get(0).getCity());
					customerVo.setCountry(list.get(0).getCountry());
					customerVo.setCityName(cityMap.get(customerVo.getCity()));
					customerVo.setCityNameAr(cityMapAr.get(customerVo.getCity()));
					customerVo.setCountryName(countryMap.get(customerVo.getCountry()));
					customerVo.setCountryNameAr(countryMapAr.get(customerVo.getCountry()));
				
				}
				
				customerVoList.add(customerVo);
				///---------------------------
				
			}
			if (customerId > 0) {
				basicResponse.object = customerVoList.get(0);
			} else {
				basicResponse.object = customerVoList;
			}
			long totalRecords = (long) map.get(TOTAL_COUNT);
			logger.info(totalRecords+"totalCounts-----------------------------------------3333");
		basicResponse.totalRecords = totalRecords;
		}
		return basicResponse;
	}
	/*
	 * @Override public BasicResponse getAllCustomers(long customerId){
	 * BasicResponse basicResponse = null; Map<String, Object> map =
	 * generalDao.getAllCustomers(customerId); long totalCounts = (long)
	 * map.get("TotalCounts"); if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR)
	 * { basicResponse = new BasicResponse(); basicResponse.errorCode = 2;
	 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
	 * basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
	 * return basicResponse; } basicResponse = new BasicResponse();
	 * basicResponse.errorCode = ERROR_CODE_NO_ERROR; basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); basicResponse.totalRecords = totalCounts;
	 * 
	 * @SuppressWarnings("unchecked") List<CustomerDetails> customerList =
	 * (List<CustomerDetails>) map.get("CustomerList"); List<AddCustomer>
	 * customerVoList = new ArrayList<>(); AddCustomer customerVo = null; for
	 * (CustomerDetails cat : customerList) { customerVo =
	 * modelMapper.map(cat,AddCustomer.class);
	 * //categoryVo.setCreatedDateString(TimeUtils.instance().getDateStrFromMilSec(
	 * cat.getCreatedDate()));
	 * //categoryVo.setLastUpdatedTimeString(TimeUtils.instance().
	 * getDateStrFromMilSec(cat.getLastUpdatedTime()));
	 * customerVoList.add(customerVo); } if (customerId > 0) { basicResponse.object
	 * = customerVoList.get(0); } else { basicResponse.object = customerVoList; }
	 * return basicResponse; }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllCustomers(long customerId, long cityId, long countryId, long userId, int orderBy,
			long fromDate, long toDate, int currentIndex, int rowPerPage, int status) {
		BasicResponse basicResponse = null;

		Map<String, Object> map = generalDao.getAllCustomers(customerId, cityId, userId, fromDate, toDate, countryId,
				orderBy, currentIndex, rowPerPage, status);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			/*
			 * Map<Long,String> countryMap=inputService.getAllCountry(1); Map<Long,String>
			 * cityMap=inputService.getAllCities(0,1);
			 */
			List<AddCustomer> customerVoList = new ArrayList<>();
			List<Object[]> customerList = (List<Object[]>) map.get("customerList");
			AddCustomer customerVo = null;
			for (Object[] cat : customerList) {
				customerVo = modelMapper.map(cat[1], AddCustomer.class);
				String totalPay = Misceleneous.instance().convertFromScientificNotation(
						Math.round((double) cat[0] * ROUND_OFF_DECMAL_TO) / ROUND_OFF_DECMAL_TO);
				customerVo.setTotalPay(Double.valueOf(totalPay));
				customerVo.setCity((long) cat[2]);
				customerVo.setCountry((long) cat[3]);

				// customerVo.setCityName(cityMap.get(customerVo.getCity()));
				// customerVo.setCountryName(countryMap.get(customerVo.getCountry()));
				customerVoList.add(customerVo);
			}
			if (customerId > 0) {
				basicResponse.object = customerVoList.get(0);
			} else {
				basicResponse.object = customerVoList;
			}
			long totalRecords = (long) map.get(TOTAL_COUNT);
			basicResponse.totalRecords = totalRecords;
		}
		return basicResponse;
	}

	@Override
	public BasicResponse getPromocodeDetails(long codeId, long userId, int status, long validFrom, long validTo,
			int currentIndex, int rowPerPage,String promocode) {
		BasicResponse res = null;

		List<GetPromocodeDetails> offVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = generalDao.getPromocodeDetails(codeId, userId, status, validFrom, validTo,
				currentIndex, rowPerPage, promocode);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			@SuppressWarnings("unchecked")
			List<PromocodeDetails> citylist = (List<PromocodeDetails>) dataMap.get("promoList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			GetPromocodeDetails ofVo = null;
			for (PromocodeDetails off : citylist) {
				ofVo = modelMapper.map(off, GetPromocodeDetails.class);

				logger.info(off.getUserId() + "userId------------------------------------");
				long u = off.getUserId();
				CustomerDetails cust = getCustomerFromUserId(u);
				logger.info(
						cust + "cust------------------------------------------------------------------------------");

				if (cust != null) {
					ofVo.setCustomerName(cust.getCustomerName());
					ofVo.setCustomerNameAr(cust.getCustomerNameAr());
					/*
					 * ordervo.setAddress(cust.getAddress());
					 * ofVo.setPhoneNumber(cust.getPhoneNo());
					 */
					// ordervo.setAddressAr(cust.getAddressAr());
				}
				offVos.add(ofVo);
			}

			returnMap.put("City", offVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = offVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}
	@Override
	public BasicResponse validatePromocode(String promocode,double orderPrice)
	{
		BasicResponse res = null;

		List<GetPromocodeDetails> offVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = generalDao.validatePromocode(promocode, orderPrice);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			@SuppressWarnings("unchecked")
			List<PromocodeDetails> citylist = (List<PromocodeDetails>) dataMap.get("promoList");
			double promocodeamount=(double) dataMap.get("promocodeamount");
			logger.info(promocodeamount+"promocodeamount-----------------------------------------------------promocodeamount--------");
			long totalCounts = (long) dataMap.get("TotalCounts");
			GetPromocodeDetails ofVo = null;
			for (PromocodeDetails off : citylist) {
				ofVo = modelMapper.map(off, GetPromocodeDetails.class);
				ofVo.setPromocodeamount(promocodeamount);
				/*if(totalCounts > 0)
				{
				logger.info(off.getValidFrom()+"valid from--------------------");
				logger.info(off.getValidTo()+"valid to--------------------");
				long currentDte=TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
				
				}*/

				/*logger.info(off.getUserId() + "userId------------------------------------");
				long u = off.getUserId();
				CustomerDetails cust = getCustomerFromUserId(u);
				logger.info(
						cust + "cust------------------------------------------------------------------------------");*/

				/*if (cust != null) {
					ofVo.setCustomerName(cust.getCustomerName());
					ofVo.setCustomerNameAr(cust.getCustomerNameAr());
					
					 * ordervo.setAddress(cust.getAddress());
					 * ofVo.setPhoneNumber(cust.getPhoneNo());
					 
					// ordervo.setAddressAr(cust.getAddressAr());
				}*/
				offVos.add(ofVo);
			}

			returnMap.put("City", offVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = offVos;
		
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;	
	}

}
