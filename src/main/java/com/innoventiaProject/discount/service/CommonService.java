package com.innoventiaProject.discount.service;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.discount.model.LoggedSession;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;



public class CommonService implements Constants {

	private Logger logger = Logger.getLogger(CommonService.class);
	
	public static ObjectMapper modelMapper = new ObjectMapper();

/*	protected void initSession(HttpServletRequest req, LoggedSession user) {
		if (user != null && req != null) {
			System.out.println("not null");
			// session.ip = req.getRemoteAddr();
			req.getSession(true).setAttribute(SESSION_USER, user);
		}
	}

	public LoggedSession getLogedUserFromSession(HttpServletRequest req) {
		LoggedSession LOGEDUSER = null;
		if (req != null) {
			try {
				LOGEDUSER = (LoggedSession) req.getSession().getAttribute(
						SESSION_USER);
				if (LOGEDUSER == null) {
					logger.debug("User session is null");
				} else {
					logger.debug("User session not null");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		// Test
		LOGEDUSER =new LoggedSession();
		 LOGEDUSER.setUserId(13);

		return LOGEDUSER;
	}
	
	
	public LoggedSession setLogedUserFromSession(HttpServletRequest req) {
		
		if (req != null) {
			req.getSession(true).setAttribute(SESSION_USER, null);
		}
		LoggedSession LOGEDUSER = getLogedUserFromSession(req);
		// Test
		// LOGEDUSER =new LoggedSession();
		// LOGEDUSER.setUserId(13);

		return LOGEDUSER;
	}*/
	
	public static String getJson(Object obj) {
		String json="";
	    Gson gson=new Gson();
	    json = gson.toJson(obj);
		return json;
	}
	
	
	public static String createToken(LoggedSession loggedSession) {
		String compactJws = Jwts.builder()
				  .setSubject(getJson(loggedSession))
				  .signWith(SignatureAlgorithm.HS512, key)
				  .compact();
		return compactJws;
	}
	
	
	public static BasicResponse checkTokenOfUser(String headerToken) {
		 BasicResponse res=new BasicResponse();
		 if(headerToken == null || headerToken.isEmpty()) {
			 res.errorCode=34;
			 res.errorMessage=TOKEN_ERROR;
			 res.errorMessageArabic=TOKEN_ERROR_AR;
			 return res;
		 }
			try {
			    Jwts.parser().setSigningKey(key).parseClaimsJws(headerToken);
			} catch (Exception e) {
				res.errorCode=45;
				res.errorMessage=e.getMessage();//"You are not allowed to access token is incorrect";
				res.errorMessageArabic=JWT_SIGNATURE_ERROR_AR;
				return res;
			}
			String jsonInString=Jwts.parser().setSigningKey(key).parseClaimsJws(headerToken).getBody().getSubject();
			ObjectMapper mapper = new ObjectMapper();
			LoggedSession obj=null;
			try {
				obj = mapper.readValue(jsonInString, LoggedSession.class);
			} catch (Exception e) {
				res.errorCode=46;
				res.errorMessage=e.getMessage();//"You are not allowed to access token is incorrect";
				res.errorMessageArabic=JSON_TO_OBJECT_ERROR;
				return res;
			}
			res.errorCode=ERROR_CODE_NO_ERROR;
			res.errorMessage=MESSAGE_SUCSESS;
			res.errorMessageArabic=MESSAGE_SUCSESS_AR;
			res.object=obj;
			return res;
	}
	
	

}
