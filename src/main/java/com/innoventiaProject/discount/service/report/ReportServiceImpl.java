package com.innoventiaProject.discount.service.report;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.omg.CORBA.TRANSACTION_MODE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innoventiaProject.dao.GeneralDao;
import com.innoventiaProject.dao.input.InputDao;
import com.innoventiaProject.dao.order.OrderDao;
import com.innoventiaProject.dao.report.ReportDao;
import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.mail.MailService;
import com.innoventiaProject.discount.mail.MailVo;
import com.innoventiaProject.discount.model.AddCustomer;
import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.discount.model.GetPromocodeDetails;
import com.innoventiaProject.discount.model.LoggedSession;
import com.innoventiaProject.discount.model.input.AddVo;
import com.innoventiaProject.discount.model.input.CategoryVo;
import com.innoventiaProject.discount.model.input.CityVo;
import com.innoventiaProject.discount.model.input.ConnectedProductsVo;
import com.innoventiaProject.discount.model.input.CountryVo;
import com.innoventiaProject.discount.model.input.FeatureVo;
import com.innoventiaProject.discount.model.input.OfferVo;
import com.innoventiaProject.discount.model.input.ProductAttributeVo;
import com.innoventiaProject.discount.model.input.ProductVo;
import com.innoventiaProject.discount.model.input.ReviewVo;
import com.innoventiaProject.discount.model.input.SubCategoryVo;
import com.innoventiaProject.discount.model.order.AuctionDetailsVo;
import com.innoventiaProject.discount.model.order.AuctionVo;
import com.innoventiaProject.discount.model.order.GetTransactionVo;
import com.innoventiaProject.discount.model.order.OrderDetailsVo;
import com.innoventiaProject.discount.model.order.OrderNewVos;
import com.innoventiaProject.discount.model.order.OrderVo;
import com.innoventiaProject.discount.model.order.TaxVo;
import com.innoventiaProject.discount.model.order.TransactionModel;
import com.innoventiaProject.discount.service.GeneralService;
import com.innoventiaProject.discount.service.input.InputService;
import com.innoventiaProject.discount.service.order.OrderService;
import com.innoventiaProject.discount.utils.Misceleneous;
import com.innoventiaProject.discount.utils.TimeUtils;
import com.innoventiaProject.entity.input.Advertisement;
import com.innoventiaProject.entity.input.Category;
import com.innoventiaProject.entity.input.City;
import com.innoventiaProject.entity.input.ConnectedProducts;
import com.innoventiaProject.entity.input.Country;
import com.innoventiaProject.entity.input.FeaturesAndPhotosProduct;
import com.innoventiaProject.entity.input.Offer;
import com.innoventiaProject.entity.input.Product;
import com.innoventiaProject.entity.input.ProductAttributes;
import com.innoventiaProject.entity.input.Review;
import com.innoventiaProject.entity.input.SellerDetails;
import com.innoventiaProject.entity.input.SubCategory;
import com.innoventiaProject.entity.order.Order;
import com.innoventiaProject.entity.order.OrderDetails;
import com.innoventiaProject.entity.order.TaxManagement;
import com.innoventiaProject.entity.order.Transactions;
import com.innoventiaProject.entity.user.CustomerDetails;
import com.innoventiaProject.entity.user.LoginDetails;
import com.innoventiaProject.entity.user.PromocodeDetails;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;

@Transactional
@Service("reportService")
public class ReportServiceImpl implements ReportService, Constants {
	private static final Logger logger = Logger.getLogger(ReportServiceImpl.class);

	@Autowired
	private OrderDao orderDao;

	@Autowired
	private ReportDao reportDao;

	@Autowired
	private InputDao inputDao;

	@Autowired
	private GeneralDao generalDao;

	@Autowired
	private GeneralService generalService;

	@Autowired
	private InputService inputService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	public MailService mailService;
	
	@Override
	public Map<Integer, String> getAllTransMap() {
		Map<Integer, String> rollMap = new HashMap<Integer, String>();
		rollMap.put(TRAN_MODE_DEBIT_SALE, TRAN_MODE_DEBIT_SALE_STR);
		rollMap.put(TRAN_MODE_DEBIT_CASH_PAY, TRAN_MODE_DEBIT_CASH_PAY_STR);
		rollMap.put(TRAN_MODE_DEBIT_ONLINE_PAY, TRAN_MODE_DEBIT_ONLINE_PAY_STR);
		rollMap.put(TRANS_MODE_SALE_RETURN, TRANS_MODE_SALE_RETURN_STR);
		// rollMap.put(TRAN_, STRING_REJECTED);
		return rollMap;
	}
	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public BasicResponse productPurchaseReport(int firstIndex,int
	 * rowPerPage) { BasicResponse res = null; List<OrderDetailsVo> productVos = new
	 * ArrayList<>(); Map<String, Object> returnMap = new HashMap<>();
	 * modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT)
	 * ; Map<String, Object> dataMap = reportDao.productPurchaseReport( firstIndex,
	 * rowPerPage); if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
	 * Map<Long, String> subcategoryMap = inputService.getAllSubCategoryNames();
	 * Map<Long, String> subcategoryMapAr = inputService.getAllSubCategoryNamesAr();
	 * Map<Long, String> offerMap = getAllOffers(); Map<Long, Long> offerMap1 =
	 * getAllOffers1(); Map<Long, String> offerMapAr = getAllOffersAr(); Map<Long,
	 * String> manufactureMap = inputService.getallManufactures(); Map<Long, String>
	 * manufactureMapAr = inputService.getallManufacturesAr(); Map<Long, String>
	 * categoryMap = inputService.getAllCategoryNames(); Map<Long, String> sub
	 * =inputService.getAllSubCategoryNames(); Map<Long, String> subAr
	 * =inputService.getAllSubCategoryNamesAr(); Map<Long, String> categoryMapAr =
	 * inputService.getAllCategoryNamesAr(); Map<Long, String[]> countryMap =
	 * inputService.getAllCountry(1); Map<Long, String[]> cityMap =
	 * inputService.getAllCities(0, 1); Map<Long, String[]> attributeMap =
	 * inputService.getAllAttribute(1); List<OrderDetails> products =
	 * (List<OrderDetails>) dataMap.get("ProudctList"); //long totalCounts = (long)
	 * dataMap.get("TotalCounts");
	 * 
	 * //returnMap.put("TOTALCOUNTS", totalCounts); OrderDetailsVo prVo = null; for
	 * (OrderDetails prod : products) { prVo = modelMapper.map(prod,
	 * OrderDetailsVo.class); prVo = modelMapper.map(prod, ProductVo.class);
	 * prVo.setDescription(prod.getDescription());
	 * prVo.setDescriptionAr(prod.getDescriptionAr()); FeatureVo fVo = null;
	 * prVo.setFeatureOrPhotos(new ArrayList<>()); //-------------------------
	 * BasicResponse resp = inputService.getAllOfferProduct(prod.getOfferId()); if
	 * (resp.getErrorCode() == ERROR_CODE_NO_ERROR) { if(prod.getOfferId()>0) {
	 * List<OfferVo> exTimeList = (List<OfferVo>) resp.getObject();
	 * prVo.getOfferVos().addAll(exTimeList); } }
	 * 
	 * 
	 * boolean flag = true; for (FeaturesAndPhotosProduct fea :
	 * prod.getFeatureAndPhotos()) {
	 * 
	 * fVo = modelMapper.map(fea, FeatureVo.class);
	 * 
	 * if (flag && fea.getModeId() == MODE_PHOTOS) {
	 * logger.info(fea.getData()+"image name-------------------------"); String
	 * image=fea.getData(); prVo.setImageName(image); //prVo.setProductImage(image);
	 * logger.info(prVo.getImageName()
	 * +"get image name---------------------------------------------------"); flag =
	 * false; } fVo.setLastUpdatedTime(fea.getLastUpdatedTime());
	 * 
	 * prVo.getFeatureOrPhotos().add(fVo); }
	 * 
	 * 
	 * ProductAttributeVo pVo = null; for (ProductAttributes pa :
	 * prod.getProudctAttributes()) { pVo = modelMapper.map(pa,
	 * ProductAttributeVo.class); logger.info(pa.getAttributeId()); String[]
	 * names=attributeMap.get(pa.getAttributeId()); if(names!= null && names.length
	 * != 0) { pVo.setAttributeName(names[0]); pVo.setAttributeNameAr(names[1]); }
	 * prVo.getProductAttributeVos().add(pVo); } ConnectedProductsVo cVo = null; for
	 * (ConnectedProducts pa : prod.getConnectedProducts()) { cVo =
	 * modelMapper.map(pa, ConnectedProductsVo.class);
	 * //logger.info(pa.getAttributeId()); String[]
	 * names=attributeMap.get(pa.getConnectedProductsId()); if(names!= null &&
	 * names.length != 0) { pVo.setAttributeName(names[0]);
	 * pVo.setAttributeNameAr(names[1]); } prVo.getConnectedProductsVo().add(cVo); }
	 * 
	 * 
	 * prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(prod.
	 * getLastUpdatedTime())); prVo.setSellerAddress(prod.getSeller().getAddress());
	 * prVo.setPhone(prod.getSeller().getPhoneNumber());
	 * prVo.setContactName(prod.getSeller().getContactName());
	 * prVo.setTelephoneNo(prod.getSeller().getTelephoneNo());
	 * 
	 * prVo.setCategoryName(categoryMap.get(prod.getCategoryId()));
	 * prVo.setCategoryNameAr(categoryMapAr.get(prod.getCategoryId()));
	 * 
	 * 
	 * prVo.setManufacturerName(manufactureMap.get(prod.getManufacturerId()));
	 * prVo.setManufacturerNameAr(manufactureMapAr.get(prod.getManufacturerId()));
	 * prVo.setBusinessType(manufactureMap.get(prod.getManufacturerId())); if
	 * (prod.getSubCategoryId() != 0) {
	 * prVo.setSubcategoryName(sub.get(prod.getSubCategoryId()));
	 * logger.info(subAr.get(prod.getSubCategoryId())+
	 * "arabic!!!!!!!!!!!!!!!!!!!!_______________________1__________");
	 * prVo.setSubcategoryNameAr(subAr.get(prod.getSubCategoryId())); }
	 * prVo.setSellerName(prod.getSeller().getSellerName());
	 * //prVo.setCityName(cityMap.get(prod.getSeller().getCityId()));
	 * //prVo.setCountryName(countryMap.get(prod.getSeller().getCountryId()));
	 * prVo.setLatitude(prod.getSeller().getLatitude());
	 * prVo.setLongitude(prod.getSeller().getLongitude()); if (countryMap != null) {
	 * String names[] = countryMap.get(prod.getSeller().getCountryId());
	 * prVo.setCountryName(names[0]); prVo.setCountryNameAr(names[1]); } if (cityMap
	 * != null) { String names[] = cityMap.get(prod.getSeller().getCityId());
	 * prVo.setCityName(names[0]); prVo.setCityNameAr(names[1]); }
	 * productVos.add(prVo); } returnMap.put("Product", productVos); if (productId >
	 * 0) { returnMap.put("Product", productVos.get(0)); List<Review> reviews =
	 * (List<Review>) dataMap.get("Reviews"); List<ReviewVo> reviewVos = new
	 * ArrayList<>(); ReviewVo revVo = null; List<OfferDetails> offer =
	 * (List<OfferDetails>) dataMap.get("Offer"); List<OfferDetailsVo>
	 * offerDetailsVos = new ArrayList<>(); OfferDetailsVo offerDetailsVo =null; for
	 * (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class);
	 * prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.
	 * getLastUpdatedTime())); reviewVos.add(revVo); }
	 * 
	 * 
	 * returnMap.put("REVIEWS", reviewVos);
	 * 
	 * } if (productId > 0) { returnMap.put("Product", productVos.get(0));
	 * List<OfferDetails> offer = (List<OfferDetails>) dataMap.get("Offer");
	 * List<OfferDetailsVo> offerDetailsVos = new ArrayList<>(); OfferDetailsVo
	 * offerDetailsVo =null;
	 * 
	 * for (OfferDetails of : offer) { offerDetailsVo = modelMapper.map(of,
	 * OfferDetailsVo.class);
	 * //prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.
	 * getLastUpdatedTime())); offerDetailsVos.add(offerDetailsVo); }
	 * 
	 * //returnMap.put("REVIEWS", reviewVos); returnMap.put("OFFER",
	 * offerDetailsVos); } if (productId > 0) { returnMap.put("Product",
	 * productVos.get(0)); List<ConnectedProducts> connectedProducts =
	 * (List<ConnectedProducts>) dataMap.get("ConnectedProducts");
	 * List<ConnectedProductsVo> connectedVos = new ArrayList<>();
	 * ConnectedProductsVo connectedVo = null;
	 * 
	 * for (ConnectedProducts re : connectedProducts) { connectedVo =
	 * modelMapper.map(re, ConnectedProductsVo.class);
	 * prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.
	 * getLastUpdatedTime())); connectedVos.add(connectedVo); }
	 * returnMap.put("CONNECTEDPRODUCTS", connectedVos); } res = new
	 * BasicResponse(); res.errorCode = (int) dataMap.get(ERROR_CODE);
	 * res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
	 * res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
	 * res.object = returnMap; //res.totalRecords = totalCounts; } else { res = new
	 * BasicResponse(); res.errorCode = (int) dataMap.get(ERROR_CODE);
	 * res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
	 * res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC); } return
	 * res; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllProduct(long productId, long sellerId, long userId, long mainCategoryId, long categoryId,
			long subcategoryId, long countryId, long cityId, int firstIndex, int rowPerPage, int status, long companyId,
			long numberOfItemsRemaining, long offerId, long manufacturerId, double priceStarts, double priceEnds,
			long productAttributeId) {
		BasicResponse res = null;
		List<ProductVo> productVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> dataMap = reportDao.getAllProducts(productId, sellerId, userId, mainCategoryId, categoryId,
				countryId, cityId, subcategoryId, firstIndex, rowPerPage, status, companyId, numberOfItemsRemaining,
				offerId, manufacturerId, priceStarts, priceEnds, productAttributeId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Long, String> subcategoryMap = inputService.getAllSubCategoryNames();
			Map<Long, String> subcategoryMapAr = inputService.getAllSubCategoryNamesAr();
			/*
			 * Map<Long, String> offerMap = getAllOffers(); Map<Long, Long> offerMap1 =
			 * getAllOffers1(); Map<Long, String> offerMapAr = getAllOffersAr();
			 */
			Map<Long, String> manufactureMap = inputService.getallManufactures();
			Map<Long, String> manufactureMapAr = inputService.getallManufacturesAr();
			Map<Long, String> categoryMap = inputService.getAllCategoryNames();
			Map<Long, String> sub = inputService.getAllSubCategoryNames();
			Map<Long, String> subAr = inputService.getAllSubCategoryNamesAr();
			Map<Long, String> categoryMapAr = inputService.getAllCategoryNamesAr();
			Map<Long, String[]> countryMap = inputService.getAllCountry(1);
			Map<Long, String[]> cityMap = inputService.getAllCities(0, 1);
			Map<Long, String[]> attributeMap = inputService.getAllAttribute(1);
			List<Product> products = (List<Product>) dataMap.get("ProudctList");
			long totalCounts = (long) dataMap.get("TotalCounts");

			returnMap.put("TOTALCOUNTS", totalCounts);
			ProductVo prVo = null;
			for (Product prod : products) {

				prVo = modelMapper.map(prod, ProductVo.class);
				prVo.setDescription(prod.getDescription());
				prVo.setDescriptionAr(prod.getDescriptionAr());
				logger.info(prod.getNumberOfItemsRemaining() + "numbr of items remaining---------------------^");
				logger.info(prod.getProductQuantity() + "product quantity---------------------^");
				long totalQtySale = prod.getProductQuantity() - prod.getNumberOfItemsRemaining();
				logger.info(totalQtySale + "totalQtySale---------------------44^");
				prVo.setTotalSaledItems(totalQtySale);
				FeatureVo fVo = null;
				prVo.setFeatureOrPhotos(new ArrayList<>());
				// -------------------------
				BasicResponse resp = inputService.getAllOfferProduct(prod.getOfferId());
				if (resp.getErrorCode() == ERROR_CODE_NO_ERROR) {
					if (prod.getOfferId() > 0) {
						List<OfferVo> exTimeList = (List<OfferVo>) resp.getObject();
						prVo.getOfferVos().addAll(exTimeList);
					}
				}

				boolean flag = true;
				for (FeaturesAndPhotosProduct fea : prod.getFeatureAndPhotos()) {

					fVo = modelMapper.map(fea, FeatureVo.class);

					if (flag && fea.getModeId() == MODE_PHOTOS) {
						logger.info(fea.getData() + "image name-------------------------");
						String image = fea.getData();
						prVo.setImageName(image);
						// prVo.setProductImage(image);
						logger.info(prVo.getImageName()
								+ "get image name---------------------------------------------------");
						flag = false;
					}
					fVo.setLastUpdatedTime(fea.getLastUpdatedTime());

					prVo.getFeatureOrPhotos().add(fVo);
				}

				ProductAttributeVo pVo = null;
				for (ProductAttributes pa : prod.getProudctAttributes()) {
					pVo = modelMapper.map(pa, ProductAttributeVo.class);
					logger.info(pa.getAttributeId());
					String[] names = attributeMap.get(pa.getAttributeId());
					if (names != null && names.length != 0) {
						pVo.setAttributeName(names[0]);
						pVo.setAttributeNameAr(names[1]);
					}
					prVo.getProductAttributeVos().add(pVo);
				}
				ConnectedProductsVo cVo = null;
				for (ConnectedProducts pa : prod.getConnectedProducts()) {
					cVo = modelMapper.map(pa, ConnectedProductsVo.class);
					// logger.info(pa.getAttributeId());
					String[] names = attributeMap.get(pa.getConnectedProductsId());
					if (names != null && names.length != 0) {
						pVo.setAttributeName(names[0]);
						pVo.setAttributeNameAr(names[1]);
					}
					prVo.getConnectedProductsVo().add(cVo);
				}

				prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(prod.getLastUpdatedTime()));
				prVo.setSellerAddress(prod.getSeller().getAddress());
				prVo.setPhone(prod.getSeller().getPhoneNumber());
				prVo.setContactName(prod.getSeller().getContactName());
				prVo.setTelephoneNo(prod.getSeller().getTelephoneNo());

				prVo.setCategoryName(categoryMap.get(prod.getCategoryId()));
				prVo.setCategoryNameAr(categoryMapAr.get(prod.getCategoryId()));

				prVo.setManufacturerName(manufactureMap.get(prod.getManufacturerId()));
				prVo.setManufacturerNameAr(manufactureMapAr.get(prod.getManufacturerId()));
				prVo.setBusinessType(manufactureMap.get(prod.getManufacturerId()));
				if (prod.getSubCategoryId() != 0) {
					prVo.setSubcategoryName(sub.get(prod.getSubCategoryId()));
					logger.info(subAr.get(prod.getSubCategoryId())
							+ "arabic!!!!!!!!!!!!!!!!!!!!_______________________1__________");
					prVo.setSubcategoryNameAr(subAr.get(prod.getSubCategoryId()));
				}
				prVo.setSellerName(prod.getSeller().getSellerName());
				// prVo.setCityName(cityMap.get(prod.getSeller().getCityId()));
				// prVo.setCountryName(countryMap.get(prod.getSeller().getCountryId()));
				prVo.setLatitude(prod.getSeller().getLatitude());
				prVo.setLongitude(prod.getSeller().getLongitude());
				/*
				 * if (countryMap != null) { String names[] =
				 * countryMap.get(prod.getSeller().getCountryId());
				 * prVo.setCountryName(names[0]); prVo.setCountryNameAr(names[1]); } if (cityMap
				 * != null) { String names[] = cityMap.get(prod.getSeller().getCityId());
				 * prVo.setCityName(names[0]); prVo.setCityNameAr(names[1]); }
				 */
				productVos.add(prVo);
			}
			returnMap.put("Product", productVos);
			if (productId > 0) {
				returnMap.put("Product", productVos.get(0));
				List<Review> reviews = (List<Review>) dataMap.get("Reviews");
				List<ReviewVo> reviewVos = new ArrayList<>();
				ReviewVo revVo = null;
				/*
				 * List<OfferDetails> offer = (List<OfferDetails>) dataMap.get("Offer");
				 * List<OfferDetailsVo> offerDetailsVos = new ArrayList<>(); OfferDetailsVo
				 * offerDetailsVo =null;
				 */
				for (Review re : reviews) {
					revVo = modelMapper.map(re, ReviewVo.class);
					prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.getLastUpdatedTime()));
					reviewVos.add(revVo);
				}

				returnMap.put("REVIEWS", reviewVos);

			}

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = returnMap;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse lowStockReport(int firstIndex, int rowPerPage, int status, long numberOfItemsRemaining,
			long numberStarts, long numberEnds) {
		BasicResponse res = null;
		List<ProductVo> productVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> dataMap = reportDao.lowStockReport(firstIndex, rowPerPage, status, numberOfItemsRemaining,
				numberStarts, numberEnds);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Long, String> subcategoryMap = inputService.getAllSubCategoryNames();
			Map<Long, String> subcategoryMapAr = inputService.getAllSubCategoryNamesAr();
			/*
			 * Map<Long, String> offerMap = getAllOffers(); Map<Long, Long> offerMap1 =
			 * getAllOffers1(); Map<Long, String> offerMapAr = getAllOffersAr();
			 */
			Map<Long, String> manufactureMap = inputService.getallManufactures();
			Map<Long, String> manufactureMapAr = inputService.getallManufacturesAr();
			Map<Long, String> categoryMap = inputService.getAllCategoryNames();
			Map<Long, String> sub = inputService.getAllSubCategoryNames();
			Map<Long, String> subAr = inputService.getAllSubCategoryNamesAr();
			Map<Long, String> categoryMapAr = inputService.getAllCategoryNamesAr();
			Map<Long, String[]> countryMap = inputService.getAllCountry(1);
			Map<Long, String[]> cityMap = inputService.getAllCities(0, 1);
			Map<Long, String[]> attributeMap = inputService.getAllAttribute(1);
			List<Product> products = (List<Product>) dataMap.get("ProudctList");
			long totalCounts = (long) dataMap.get("TotalCounts");

			returnMap.put("TOTALCOUNTS", totalCounts);
			ProductVo prVo = null;
			for (Product prod : products) {

				prVo = modelMapper.map(prod, ProductVo.class);
				prVo.setDescription(prod.getDescription());
				prVo.setDescriptionAr(prod.getDescriptionAr());
				FeatureVo fVo = null;
				prVo.setFeatureOrPhotos(new ArrayList<>());
				// -------------------------
				BasicResponse resp = inputService.getAllOfferProduct(prod.getOfferId());
				if (resp.getErrorCode() == ERROR_CODE_NO_ERROR) {
					if (prod.getOfferId() > 0) {
						List<OfferVo> exTimeList = (List<OfferVo>) resp.getObject();
						prVo.getOfferVos().addAll(exTimeList);
					}
				}

				boolean flag = true;
				for (FeaturesAndPhotosProduct fea : prod.getFeatureAndPhotos()) {

					fVo = modelMapper.map(fea, FeatureVo.class);

					if (flag && fea.getModeId() == MODE_PHOTOS) {
						logger.info(fea.getData() + "image name-------------------------");
						String image = fea.getData();
						prVo.setImageName(image);
						// prVo.setProductImage(image);
						logger.info(prVo.getImageName()
								+ "get image name---------------------------------------------------");
						flag = false;
					}
					fVo.setLastUpdatedTime(fea.getLastUpdatedTime());

					prVo.getFeatureOrPhotos().add(fVo);
				}

				ProductAttributeVo pVo = null;
				for (ProductAttributes pa : prod.getProudctAttributes()) {
					pVo = modelMapper.map(pa, ProductAttributeVo.class);
					logger.info(pa.getAttributeId());
					String[] names = attributeMap.get(pa.getAttributeId());
					if (names != null && names.length != 0) {
						pVo.setAttributeName(names[0]);
						pVo.setAttributeNameAr(names[1]);
					}
					prVo.getProductAttributeVos().add(pVo);
				}
				ConnectedProductsVo cVo = null;
				for (ConnectedProducts pa : prod.getConnectedProducts()) {
					cVo = modelMapper.map(pa, ConnectedProductsVo.class);
					// logger.info(pa.getAttributeId());
					String[] names = attributeMap.get(pa.getConnectedProductsId());
					if (names != null && names.length != 0) {
						pVo.setAttributeName(names[0]);
						pVo.setAttributeNameAr(names[1]);
					}
					prVo.getConnectedProductsVo().add(cVo);
				}

				prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(prod.getLastUpdatedTime()));
				prVo.setSellerAddress(prod.getSeller().getAddress());
				prVo.setPhone(prod.getSeller().getPhoneNumber());
				prVo.setContactName(prod.getSeller().getContactName());
				prVo.setTelephoneNo(prod.getSeller().getTelephoneNo());

				prVo.setCategoryName(categoryMap.get(prod.getCategoryId()));
				prVo.setCategoryNameAr(categoryMapAr.get(prod.getCategoryId()));

				prVo.setManufacturerName(manufactureMap.get(prod.getManufacturerId()));
				prVo.setManufacturerNameAr(manufactureMapAr.get(prod.getManufacturerId()));
				prVo.setBusinessType(manufactureMap.get(prod.getManufacturerId()));
				if (prod.getSubCategoryId() != 0) {
					prVo.setSubcategoryName(sub.get(prod.getSubCategoryId()));
					logger.info(subAr.get(prod.getSubCategoryId())
							+ "arabic!!!!!!!!!!!!!!!!!!!!_______________________1__________");
					prVo.setSubcategoryNameAr(subAr.get(prod.getSubCategoryId()));
				}
				prVo.setSellerName(prod.getSeller().getSellerName());
				// prVo.setCityName(cityMap.get(prod.getSeller().getCityId()));
				// prVo.setCountryName(countryMap.get(prod.getSeller().getCountryId()));
				prVo.setLatitude(prod.getSeller().getLatitude());
				prVo.setLongitude(prod.getSeller().getLongitude());
				/*
				 * if (countryMap != null) { String names[] =
				 * countryMap.get(prod.getSeller().getCountryId());
				 * prVo.setCountryName(names[0]); prVo.setCountryNameAr(names[1]); }
				 */
				/*
				 * if (cityMap != null) { String names[] =
				 * cityMap.get(prod.getSeller().getCityId()); prVo.setCityName(names[0]);
				 * prVo.setCityNameAr(names[1]); }
				 */
				productVos.add(prVo);
			}
			returnMap.put("Product", productVos);
			/*
			 * if (productId > 0) { returnMap.put("Product", productVos.get(0));
			 * List<Review> reviews = (List<Review>) dataMap.get("Reviews"); List<ReviewVo>
			 * reviewVos = new ArrayList<>(); ReviewVo revVo = null; List<OfferDetails>
			 * offer = (List<OfferDetails>) dataMap.get("Offer"); List<OfferDetailsVo>
			 * offerDetailsVos = new ArrayList<>(); OfferDetailsVo offerDetailsVo =null; for
			 * (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class);
			 * prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); reviewVos.add(revVo); }
			 * 
			 * 
			 * returnMap.put("REVIEWS", reviewVos);
			 * 
			 * }
			 */
			/*
			 * if (productId > 0) { returnMap.put("Product", productVos.get(0));
			 * List<OfferDetails> offer = (List<OfferDetails>) dataMap.get("Offer");
			 * List<OfferDetailsVo> offerDetailsVos = new ArrayList<>(); OfferDetailsVo
			 * offerDetailsVo =null;
			 * 
			 * for (OfferDetails of : offer) { offerDetailsVo = modelMapper.map(of,
			 * OfferDetailsVo.class);
			 * //prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); offerDetailsVos.add(offerDetailsVo); }
			 * 
			 * //returnMap.put("REVIEWS", reviewVos); returnMap.put("OFFER",
			 * offerDetailsVos); }
			 */
			/*
			 * if (productId > 0) { returnMap.put("Product", productVos.get(0));
			 * List<ConnectedProducts> connectedProducts = (List<ConnectedProducts>)
			 * dataMap.get("ConnectedProducts"); List<ConnectedProductsVo> connectedVos =
			 * new ArrayList<>(); ConnectedProductsVo connectedVo = null;
			 * 
			 * for (ConnectedProducts re : connectedProducts) { connectedVo =
			 * modelMapper.map(re, ConnectedProductsVo.class);
			 * prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); connectedVos.add(connectedVo); }
			 * returnMap.put("CONNECTEDPRODUCTS", connectedVos); }
			 */
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = returnMap;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse commissionReport(int firstIndex,int rowPerPage,int status,long lastUpdatedTime,long orderId,long userId, double priceStarts,double priceEnds, long fromDate, long toDate,int transactionMode,int tMode[])
	{
		BasicResponse res = null;
		double totalCommission=0;
		AddCustomer customerVo = null;
		List<TransactionModel> transVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> dataMap = reportDao.commissionReport(firstIndex, rowPerPage, status, lastUpdatedTime,
				orderId, userId, priceStarts, priceEnds, fromDate, toDate, transactionMode, tMode);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			List<Transactions> transactions = (List<Transactions>) dataMap.get("TranList");
			long totalCounts = (long) dataMap.get("TotalCounts");

			returnMap.put("TOTALCOUNTS", totalCounts);
			TransactionModel trVo = null;
			for (Transactions tran : transactions) 
			{

				
				trVo = modelMapper.map(tran, TransactionModel.class);
                totalCommission=+trVo.getNetAmount();
                Map<String, Object> map = generalDao.getAllCustomers(0, trVo.getToWhoId(), 0, 0, 0, 0,
        				1, 0, 100, 0, null, null, null, null);  
                if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
                	List<Object[]> customerList = (List<Object[]>) map.get("customerList");

        			
        			for (Object[] cat : customerList) 
        			{
        				customerVo = modelMapper.map(cat[1], AddCustomer.class);
        			}
                }
                trVo.setCustomerName(customerVo.getCustomerName());
				transVos.add(trVo);
			}
			returnMap.put("Transactions", transVos);
			returnMap.put("TOTAL COMMISSION", totalCommission);
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = returnMap;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse taxReport(int firstIndex,int rowPerPage,int status,long lastUpdatedTime,long orderId, double priceStarts,double priceEnds, long fromDate, long toDate,long taxId)
{
		double totalTax=0;
		int count=0;
		BasicResponse res = null;
		List<OrderDetailsVo> transVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> dataMap = reportDao.taxReport(firstIndex, rowPerPage, status, lastUpdatedTime,
				orderId, priceStarts, priceEnds, fromDate, toDate,taxId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) 
		{

			List<OrderDetails> transactions = (List<OrderDetails>) dataMap.get("TaxList");
			long totalCounts = (long) dataMap.get("TotalCounts");

			returnMap.put("TOTALCOUNTS", totalCounts);
			OrderDetailsVo trVo = null;
			for (OrderDetails tran : transactions)
			{
                count++;
				trVo = modelMapper.map(tran, OrderDetailsVo.class);
				logger.info(trVo.getTax()+"trVo.getTax()-----------------------trVo.getTax()--------------");
			    totalTax+=trVo.getTax();
			  
			    transVos.add(trVo);
			}
			double a = totalTax;
		    double roundOff = Math.round(a*100)/100;
			returnMap.put("Transactions", transVos);
			returnMap.put("TOTALTAX", roundOff);
			
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = returnMap;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse incomeExpenceReport(int firstIndex,int rowPerPage,int status,long lastUpdatedTime,long orderId,long userId, double priceStarts,double priceEnds, long fromDate, long toDate,int transactionMode,int tMode[])
	{
		BasicResponse res = null;
		double totalIncome=0;
		List<TransactionModel> transVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> dataMap = reportDao.incomeExpenceReport(firstIndex, rowPerPage, status, lastUpdatedTime,
				orderId,userId, priceStarts, priceEnds, fromDate, toDate, transactionMode,tMode);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			List<Transactions> transactions = (List<Transactions>) dataMap.get("TranList");
			long totalCounts = (long) dataMap.get("TotalCounts");

			returnMap.put("TOTALCOUNTS", totalCounts);
			TransactionModel trVo = null;
			for (Transactions tran : transactions) {

				trVo = modelMapper.map(tran, TransactionModel.class);
				totalIncome+=trVo.getNetAmount();
				transVos.add(trVo);
			}
			returnMap.put("Transactions", transVos);
			returnMap.put("TOTAL_EXPENCE_OR_INCOME", totalIncome);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = returnMap;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse transactionReport(int firstIndex, int rowPerPage, int status, long lastUpdatedTime,
			long orderId, long userId, double priceStarts, double priceEnds, long fromDate, long toDate,
			int transactionMode, int tMode[],int iNoR[],int bankOrCash[]) {	
		BasicResponse res = null;
			List<TransactionModel> transVos = new ArrayList<>();
			Map<String, Object> returnMap = new HashMap<>();
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			Map<String, Object> dataMap = reportDao.transactionReport(firstIndex, rowPerPage, status, lastUpdatedTime,
					orderId,userId, priceStarts, priceEnds, fromDate, toDate, transactionMode,tMode, iNoR, bankOrCash);
			if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

				List<Transactions> transactions = (List<Transactions>) dataMap.get("TranList");
				long totalCounts = (long) dataMap.get("TotalCounts");

				returnMap.put("TOTALCOUNTS", totalCounts);
				TransactionModel trVo = null;
				for (Transactions tran : transactions) {

					trVo = modelMapper.map(tran, TransactionModel.class);

					transVos.add(trVo);
				}
				returnMap.put("Transactions", transVos);

				res = new BasicResponse();
				res.errorCode = (int) dataMap.get(ERROR_CODE);
				res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
				res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
				res.object = returnMap;
				res.totalRecords = totalCounts;
			} else {
				res = new BasicResponse();
				res.errorCode = (int) dataMap.get(ERROR_CODE);
				res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
				res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			}
			return res;
		}

	@Override
	public BasicResponse addOrder(OrderNewVos orderVo) throws JSONException {
		List<OrderDetails> oList = null;
		long timeNow = TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
		double totalAmt = 0;
		double totalTax = 0;
		double totalPricePromo=0;
		BasicResponse res = null;
		Map<Long, List<OrderDetails>> orMap = new HashMap<>();
		Map<String, Object> tranMap = new HashMap<String, Object>();
		Map<String, Object> onlineMap = new HashMap<String, Object>();
		Map<String, Object> commissinMap = new HashMap<String, Object>();
		Map<Long, Order> orders = new HashMap<>();
		String output = null;
		double totalShipping = 0;
		double finalShipping = 0;
		double finaInstallation = 0;
		double instalationCharge = 0;
		double offer = 0;
		double promo = 0;
		double finalTax = 0;
		Order order = null;
		InputStream is = null;
		TaxVo taxVo = null;
		OfferVo ofVo = null;
		double amt = 0;
		Transactions transaction = new Transactions();
		Transactions transaction1 = new Transactions();
		Transactions transaction3 = new Transactions();
		StringBuffer msgData = new StringBuffer("Thank you very much ");
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> mapCustomer = generalDao.getAllCustomers(0, orderVo.getCustomerId(), 0, 0, 0, 0,
				1, 0, 100, 0);
		List<Object[]> customerList = (List<Object[]>) mapCustomer.get("customerList");
		List<AddCustomer> customerVoList = new ArrayList<>();
		AddCustomer customerVo = null;
		for (Object[] cat : customerList) 
		{
			customerVo = modelMapper.map(cat[1], AddCustomer.class);
			String totalPay = Misceleneous.instance().convertFromScientificNotation(
					Math.round((double) cat[0] * ROUND_OFF_DECMAL_TO) / ROUND_OFF_DECMAL_TO);
			
			customerVoList.add(customerVo);
		}
		
		logger.info(customerVoList.size()+"");
		
		for (OrderDetailsVo orVo : orderVo.getOrderDetailsVo()) {

			if (orders.get(orVo.getSellerId()) == null) {

				order = modelMapper.map(orderVo, Order.class);
				order.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
				order.setStatus(ACTIVE);
				order.setOrderstatus(ORDERED);
				msgData.append("  for shopping at Pental.We Hope You are satisfied with our service.");
				order.setSellerId(orVo.getSellerId());
				OrderDetails orderdet = null;
				oList = new ArrayList<>();

				int noOfItems = 0;
				double netTax = 0;
				double commission = 0;
				long item = 0;
				for (OrderDetailsVo fVo : orderVo.getOrderDetailsVo()) {
					if (orVo.getSellerId() == fVo.getSellerId()) {
						orderdet = modelMapper.map(fVo, OrderDetails.class);

						Map<String, Object> dataMap = inputDao.getAllProducts(fVo.getProductId(), 0, 0, 0, 0, 0, 0, 0,
								0, 100, 1, null, 0, 0, 0, 0, 0, 0, 0, null, null, 0, null, 0, 0);
						if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
							List<Product> products = (List<Product>) dataMap.get("ProudctList");
							ProductVo prVo = null;
							for (Product prod : products) {

								prVo = modelMapper.map(prod, ProductVo.class);
								commission = +(prVo.getCommission()*fVo.getItemQuantity());
								logger.info(commission + "commissioncommissioncommissioncommission");
								if (prVo.getTaxId() > 0 && prVo.getOfferId() > 0) {
									Map<String, Object> taxMap = orderDao.getTax(0, 0, prVo.getTaxId(), 100, 0, 1);
									Map<String, Object> offerMap = inputDao.getAllOffer(prVo.getOfferId(), 0, 0, 1,
											null, null, 0, 100);
									List<TaxManagement> taxList = (List<TaxManagement>) taxMap.get("TaxList");
									List<Offer> offerList = (List<Offer>) offerMap.get("CityList");

									for (TaxManagement tax : taxList) {
										taxVo = modelMapper.map(tax, TaxVo.class);
										prVo.setTaxPercentage(taxVo.getTaxPercentage());
										// logger.info(prVo.getTaxPercentage());

										double total = prVo.getPrice();
										// logger.info(total);
										prVo.setTotalAmount(total);

										for (Offer off : offerList) {
											ofVo = modelMapper.map(off, OfferVo.class);
											if (ofVo.getOfferRate() > 0) {

												prVo.setTotalOfferAmount(total - ofVo.getOfferRate());
												offer = ofVo.getOfferRate();
												double ofP = (double) (ofVo.getOfferRate() / total * 100);
												prVo.setTaxAmount(ofP * (taxVo.getTaxPercentage() / 100));
												prVo.setOfferPercentage(ofP);
												prVo.setOfferRatePercentage(ofP);
											}
											if (ofVo.getOfferPercentage() > 0) {
												double p = ((double) ofVo.getOfferPercentage() / 100);
												offer = p * prVo.getPrice();
												double pOfferRate = total - (total * (Double.valueOf(p)));
												prVo.setTaxAmount(pOfferRate * (taxVo.getTaxPercentage() / 100));
												prVo.setTotalOfferAmount(pOfferRate);
												prVo.setOfferPercentage(ofVo.getOfferPercentage());
												prVo.setOfferRatePercentage(ofVo.getOfferPercentage());
											}

										}

									}

								}

								else if (prVo.getTaxId() > 0) {
									Map<String, Object> dataMap3 = orderDao.getTax(0, 0, prVo.getTaxId(), 100, 0, 1);
									if ((int) dataMap3.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
										List<TaxManagement> taxList = (List<TaxManagement>) dataMap3.get("TaxList");

										for (TaxManagement tax : taxList) {
											taxVo = modelMapper.map(tax, TaxVo.class);
											prVo.setTaxPercentage(taxVo.getTaxPercentage());
											offer = 0;
											double total = prVo.getPrice();
											//prVo.setTaxAmount(total * (taxVo.getTaxPercentage() / 100));
											prVo.setTaxAmount((total *100)/(100+taxVo.getTaxPercentage()));
											prVo.setTotalAmount(total);
											prVo.setTotalOfferAmount(total);

										}

									}

								}

								else if (prVo.getTaxId() == 0 && prVo.getOfferId() == 0) {
									offer = 0;
									prVo.setTaxAmount(0);
									prVo.setTotalAmount(prVo.getPrice());
									prVo.setTotalOfferAmount(prVo.getPrice());
								} else if (prVo.getOfferId() > 0) {
									Map<String, Object> offerMap1 = inputDao.getAllOffer(prVo.getOfferId(), 0, 0, 1,
											null, null, 0, 100);
									List<Offer> offerList = (List<Offer>) offerMap1.get("CityList");
									for (Offer off : offerList) {
										ofVo = modelMapper.map(off, OfferVo.class);
										if (ofVo.getOfferRate() > 0) {
											offer = ofVo.getOfferRate();
											prVo.setTotalAmount(prVo.getPrice());
											prVo.setTotalOfferAmount(prVo.getPrice() - ofVo.getOfferRate());
											double ofP = (double) (ofVo.getOfferRate() / prVo.getPrice() * 100);
											//prVo.setTaxAmount(ofP * (taxVo.getTaxPercentage() / 100));
											prVo.setTaxAmount((ofP *100)/(100+taxVo.getTaxPercentage()));
											prVo.setOfferPercentage(ofP);
											prVo.setOfferRatePercentage(ofP);
										}
										if (ofVo.getOfferPercentage() > 0) {
											double p = ((double) ofVo.getOfferPercentage() / 100);
											offer = p * prVo.getPrice();
											double pOfferRate = prVo.getPrice()
													- (prVo.getPrice() * (Double.valueOf(p)));
											prVo.setTotalAmount(prVo.getPrice());
											prVo.setTotalOfferAmount(pOfferRate);
											//prVo.setTaxAmount(pOfferRate * (taxVo.getTaxPercentage() / 100));
											prVo.setTaxAmount((pOfferRate *100)/(100+taxVo.getTaxPercentage()));
											prVo.setOfferPercentage(ofVo.getOfferPercentage());
											prVo.setOfferRatePercentage(ofVo.getOfferPercentage());
										}

									}

								}

								orderdet.setItemPurchasePrice(prVo.getPrice());
								orderdet.setShippingCharge(prVo.getShippingCharge());
								orderdet.setItemNameAr(prVo.getProductNameAr());
								orderdet.setTaxId(prVo.getTaxId());
								orderdet.setOfferId(prVo.getOfferId());

								orderdet.setCancellationDate(0);
								orderdet.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
								orderdet.setStatus(ACTIVE);

								orderdet.setTotalAmountIncludingOffer(fVo.getTotalAmountIncludingOffer());
								orderdet.setOperatingOfficerId(orderVo.getCustomerId());
								orderdet.setDeliveryDate(0);
								orderdet.setProduct(null);
								totalShipping = prVo.getShippingCharge() * fVo.getItemQuantity();
								instalationCharge = prVo.getInstallationCharge() * fVo.getItemQuantity();
								totalAmt += prVo.getTotalOfferAmount() * fVo.getItemQuantity();
								orderdet.setItemPrice(prVo.getTotalOfferAmount());
								finalShipping += totalShipping;
								finaInstallation += instalationCharge;
								noOfItems += fVo.getItemQuantity();
								totalTax = prVo.getTaxAmount() * fVo.getItemQuantity();
								finalTax += totalTax;
								oList.add(orderdet);
								orderdet.setTax(totalTax);
								orderdet.setShippingCharge(totalShipping);
								orderdet.setInstallationCharge(instalationCharge);

								
								if (orderVo.getPromocode().length() > 5) 
								{
									logger.info("qwertyuiopasdfghjklzxcvbnm-----------------qwertyuiopasdfghjklzxcvbnm");
									totalAmt = 0;

									Map<String, Object> promoMap = generalDao.getPromocodeDetails(0, 0, 1, 0, 0, 0, 0,
											orderVo.getPromocode());

									Object tab = 0;
									Object tab1 = -3;
									for (Map.Entry<String, Object> entry : promoMap.entrySet()) {
										String key = entry.getKey();
										tab = entry.getValue();

									}

									if (tab == tab1) {
										BasicResponse res1 = new BasicResponse();
										res1.errorCode = 1;
										res1.errorMessage = "no such promocode";
										res1.errorMessageArabic = "لا يوجد مثل الكودات";
										return res1;
									} else

									{
										{
											List<PromocodeDetails> data = (List<PromocodeDetails>) promoMap.get("promoList");
											if (data.size() > 0) 
											{
												orderVo.setPromorterId(data.get(0).getUserId());
												orderVo.setCodeId(data.get(0).getCodeId());
												orderVo.setPercentage(data.get(0).getPercentage());
												if (orderVo.getPercentage() > 0) {
													promo = prVo.getPrice() * orderVo.getPercentage() / 100;
													logger.info(promo + "||||||||||||||||||||||||||||||||||||||||||||");
												}
												orderVo.setFlatOffer(data.get(0).getFlatOffer());
												if (orderVo.getFlatOffer() > 0) {
													promo = orderVo.getFlatOffer();
													logger.info(
															promo + "|||||||||||||||||||||||||||||||||||||||||||||||");
												}
												orderVo.setValidFrom(data.get(0).getValidFrom());
												orderVo.setValidTo(data.get(0).getValidTo());
												orderVo.setMinimumAmount(data.get(0).getMinimumAmount());

											}
										}

									}
									
									// include code
									if (offer > prVo.getPrice() || (offer > prVo.getMaximumPromoOffer()
											&& promo > prVo.getMaximumPromoOffer())) 
									{

										totalAmt = ((prVo.getPrice() - offer) * fVo.getItemQuantity());
										orderdet.setItemNetPrice(totalAmt);

									}

									else if (promo < prVo.getMaximumPromoOffer()) {

										totalAmt = (prVo.getPrice() - promo) * fVo.getItemQuantity();
										orderdet.setItemNetPrice(totalAmt);
									} else 
									{

										totalAmt = ((prVo.getPrice() - prVo.getMaximumPromoOffer())
												* fVo.getItemQuantity());
										orderdet.setItemNetPrice(totalAmt);
									}
									logger.info(orderdet.getItemNetPrice()+"orderdet.getItemNetPrice()");
									
									//totalAmt += orderdet.getItemNetPrice();
									totalPricePromo += totalAmt;
									logger.info(totalPricePromo+"totalPricePromo-------------totalPricePromo");	
									if (order.getCod() == 0)
									{
									transaction3.setBankOrCash(TRAN_BANK);
									}
									else
									{
										transaction3.setBankOrCash(TRAN_CASH);
									}
									transaction3.setTransactionMode(TRAN_MODE_COMMISSION_PAY);
									transaction3.setCmd(1);
									transaction3.setOperatingOfficer(order.getOperatingOfficerId());
									transaction3.setToWhoId(orderVo.getPromorterId());
									transaction3.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
									logger.info(commission+"----------xxxxx-------------------------------------------------");
									transaction3.setNetAmount(commission);
									transaction3.setNote("commission credits");
									transaction3.setInOrOut(CASH_IN);
									transaction3.setIsCleared(NOT_CLEAR_TRAN);
									transaction3.setStatus(INACTIVE);
									transaction3.setCompanyId(order.getCompanyId());
									commissinMap.put("commissinMap", transaction3);
									
									
									
									
									
									
								}
								else {
									logger.info("xxxxx");
									commission=0;
								
								//commissinMap.put("commissinMap", null);
								
								}
							}
						}

					}
				} // for loop

				order.setTax(finalTax);
				order.setTotalInstallationCharge(finaInstallation);
				order.setTotalShippingCharge(finalShipping);
				
				order.setNetPrice(totalAmt + order.getTotalInstallationCharge() + order.getTotalShippingCharge());
				transaction.setNetAmount(order.getNetPrice());
				

				item = orVo.getItemQuantity();

				

				order.setCod(orderVo.getCod());
				order.setOnline(orderVo.getOnline());
				if (orderVo.getCod() == 2) {
					order.setPaymentId("0");
					order.setPaymentStatus(10);
				} else {
					order.setPaymentId("1234");
					order.setPaymentStatus(12);

				}

				order.setPromocodeId(orderVo.getCodeId());
				order.setNumberOfItems(noOfItems);
				orders.put(orVo.getSellerId(), order);
				orMap.put(orVo.getSellerId(), oList);

			}
		}
		if (order.getCod() == 0)
		{
		transaction.setBankOrCash(TRAN_BANK);
		}
		else
		{
			transaction.setBankOrCash(TRAN_CASH);	
		}
		transaction.setTransactionMode(TRAN_MODE_DEBIT_SALE);
		transaction.setCmd(1);
		transaction.setOperatingOfficer(order.getOperatingOfficerId());
		transaction.setToWhoId(order.getCustomerId());
		transaction.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));

		transaction.setNote("sale of item");
		transaction.setInOrOut(CASH_IN);
		transaction.setIsCleared(NOT_CLEAR_TRAN);
		transaction.setStatus(INACTIVE);
		transaction.setCompanyId(order.getCompanyId());
		transaction.setPairedId(order.getOrderId());
		tranMap.put("ORDER_TRAN", transaction);
		// -added online

		if (order.getCod() == 0)
		{

			transaction1.setBankOrCash(TRAN_BANK);
			transaction1.setCmd(1);
			transaction1.setCompanyId(0);
			transaction1.setInOrOut(CASH_IN);
			transaction1.setDiscount(0);
			transaction.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
			transaction1.setTransactionMode(TRAN_MODE_DEBIT_ONLINE_PAY);
			transaction1.setNetAmount(transaction.getNetAmount());
			transaction1.setNote("ONLINE PAY ");
			transaction1.setIsCleared(NOT_CLEAR_TRAN);
			transaction1.setStatus(INACTIVE);
			onlineMap.put("PAYMENT_TRAN", transaction1);

		} 

		Map<String, Object> map = reportDao.addOrder(orMap, orders, tranMap, onlineMap,commissinMap);
		Map<String, Object> returnMap = new HashMap<>();

		res = new BasicResponse();
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		long orderIds = 0;
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			try {
				Map<Long, Order> ordersnew = (Map<Long, Order>) map.get("order");

				for (Entry<Long, Order> entry : orders.entrySet()) {

					logger.info(entry.getValue().getOrderId());
					orderIds = entry.getValue().getOrderId();

				}

				msgData.append("Your order ID is :PNT00" + orderIds);
				/*msgData.append("  شكرًا جزيلًا يا   سيد‎   لتسوقك من بنتل." + 
						"نرجوا أن تحوز خدماتنا على رضاكم." + 
						"رقم هوية طلبكم هو PNT00"+orderIds+".");*/
				
				msgData.append("شكرًا جزيلًا يا سيد لتسوقك من بنتل." +
				"نرجوا أن تحوز خدماتنا على رضاكم." +
				"رقم هوية طلبكم هو  PNT00" +orderIds+".");


				URL url = new URL("https://test.oppwa.com/v1/checkouts");

				HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
				conn.setRequestMethod("POST");
				conn.setDoInput(true);
				conn.setDoOutput(true);
				double totalPrice = Math.round(order.getNetPrice() * 100) / 100;
				String priceData = String.valueOf(totalPrice);
				String[] val = priceData.split("\\.");

				if (val[1].length() == 1) {
					priceData = String.valueOf(totalPrice) + "0";
				}

				if (orderVo.getCod() != 2) 
				{
					String data = "" + "authentication.userId=8a82941865fc22f80166071f8e710f2b"
							+ "&authentication.password=yrkE5s3FRF"
							+ "&authentication.entityId=8a82941865fc22f80166071ff3fa0f2f" + "&amount=" + priceData
							+ "&currency=SAR" + "&paymentType=DB";

					DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

					wr.writeBytes(data);
					wr.flush();
					wr.close();
					int responseCode = conn.getResponseCode();

					if (responseCode >= 400)
						is = conn.getErrorStream();
					else
						is = conn.getInputStream();

					output = IOUtils.toString(is);
					// logger.info(output +
					// "output_____________________output_________________output______________");
					JSONObject jsonObj = new JSONObject(output);
					JSONObject jsonObj2 = (JSONObject) jsonObj.get("result");
					if (jsonObj2.get("code").equals("000.200.100")) {

						String id = (String) jsonObj.get("id");
						Map<String, Object> updateOrder = reportDao.updateOrder(orderIds, id, PAYMENT_PENDING);

					}

				}

			} catch (IOException e) {

				e.printStackTrace();
			}

			int phoneOrWeb = 1;

			//if (phoneOrWeb == PHONE) 
			//{
				if (orderVo.getCod() == 2) 
				{
				final String uri = "http://my.forwardvaluesms.com/vendorsms/pushsms.aspx?apiKey=20e90577-df92-4381-b37f-ae62c5fca6d7&clientid=43dfaf3a-b2eb-479e-88cd-fe0b733dafe5&msisdn="
						+ orderVo.getCountryCode() + orderVo.getCustomerPhone() + "&sid=Pental&msg=" + msgData
						+ "&fl=0" +"&dc=8";
				

				logger.info(uri);

				logger.info(uri);
				RestTemplate restTemplate = new RestTemplate();
				String result = restTemplate.getForObject(uri, String.class);
				ObjectMapper mapper = new ObjectMapper();
				try 
				{
					Map<String, Object> map1 = mapper.readValue(result, Map.class);
					logger.info(map1.get("ErrorCode"));

				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				}
				//
				
				/*MailVo mailVo = new MailVo();
				mailVo.setMailFrom("info@leensouq.com");
				mailVo.setMailSubject("Purchase Invoive Details");
				mailVo.setMailTo(customerVoList.get(0).getEmailId());
				Map<String, Object> model = new HashMap<String, Object>();
				// model.put("firstName", session.firstName);
				model.put("location", "Jeddah,Saudi Arabia");
				model.put("signature", "Leensouq");
				
				String str="";
				
				
				
				//
				
				Map<String, Object> dataMap=reportDao.getAllOrdersDetails(orderIds, 0, 0, 0, 0, 0, 0, 0, 0);
				//
				double subTotal=0;
				if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) 
				{
					List<OrderDetails> orderlist = (List<OrderDetails>) dataMap.get(DATA);
					long totalCounts = (long) dataMap.get("TotalCounts");
					OrderDetailsVo ordervo = null;
				double price=0;	
				double offer1=0;
				double taxIncludePrice=0;
				double tax=0;
					for (OrderDetails orderDet : orderlist) 
					{
						price=0;
						ordervo = modelMapper.map(orderDet, OrderDetailsVo.class);
						//orderVos.add(ordervo);
						if(orderDet.getOfferId() == 0)
						{
						price=((ordervo.getTax()/100*ordervo.getItemPrice())+ordervo.getItemPrice())*orderDet.getItemQuantity();
						}
						else
						{
							Map<String, Object> offerMap = inputDao.getAllOffer(orderDet.getOfferId(), 0, 0, 1, null, null, 0, 100);
							List<Offer> offerList = (List<Offer>) offerMap.get("CityList");
							if(offerList.get(0).getOfferPercentage() >0)
							{
								offer1=offerList.get(0).getOfferPercentage();
								
							double p = ((double) offerList.get(0).getOfferPercentage()/100);
							logger.info(p + "p___________________________________________________________");
							 price = (ordervo.getItemPrice() - (ordervo.getItemPrice() * (Double.valueOf(p))))*ordervo.getItemQuantity();
							 logger.info(price);
							
							 tax=(price*ordervo.getTax()/100);
							 taxIncludePrice=tax+price;
							 price=taxIncludePrice;
							
							//logger.info(price);
							}
							else
							{
								 offer1 = (long) (offerList.get(0).getOfferRate()/ ordervo.getItemPrice() * 100);
								//offer=offerList.get(0).getOfferRate();
								logger.info(offerList.get(0).getOfferRate());
								logger.info(ordervo.getItemPrice());
								price=	(ordervo.getItemPrice()-offerList.get(0).getOfferRate())*ordervo.getItemQuantity();
								logger.info(price);
								 tax=(price*ordervo.getTax()/100);
								 taxIncludePrice=tax+price;
								 price=taxIncludePrice;
							}
							
							
							
							}
						subTotal+=price;
						
						str +=" <tr><td>1</td> <td>"+ordervo.getItemName()+"</td><td>"+ordervo.getItemQuantity()+"</td><td>"+ordervo.getItemPurchasePrice()+"</td><td>"+offer1+"%</td><td>"+ordervo.getTax()+"%</td><td>"+price+"SAR</td></tr>";
					
					}
					
				//} 
				
				
				
				
				
				
				
				////double value = netPrice;
			    //double valueRounded = Math.round(value * 100D) / 100D;
			   // logger.info(value);	
				//logger.info(valueRounded);	
				
			    double value1 = ordervo.getTax();
			   double taxRounded = Math.round(value1 * 100D) / 100D;
			//logger.info(value1);	
			//logger.info(valueRounded1);	
				
				model.put("orderId", "L00"+orderIds);
				model.put("orderdetails", str);
			model.put("totalTax",taxRounded);
				//model.put("grandTotal", valueRounded);
				//model.put("subTotal", valueRounded1);
				String link = null;
				link = REDIRECT_URL + "?email=" + customerVoList.get(0).getEmailId() + "&key=0";
				model.put("link", link);
				mailVo.setTemplateURL("/templates/invoice.vm");
				mailVo.setModel(model); 
				mailService.sendEmail(mailVo);
				
				//
				
				}*/
			//}

			returnMap.put("output", output);
			returnMap.put("orderId", orderIds);

			res.object = returnMap;

		}
		
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllOrder(long orderId, long storeId, long customerId, int firstIndex, int rowPerPage,
			int status[], long fromDate, long toDate, long deliveryDate, int serviceOrProduct, long deliveryBoyId,
			long orderListId) throws IOException {

		BasicResponse res = null;
		long to = 0;
		long from = 0;
		List<OrderVo> orderVos = new ArrayList<>();
		if (fromDate != 0) {
			from = TimeUtils.instance().getStartTimeOfGiveTime(fromDate);
			if (toDate != 0) {
				to = TimeUtils.instance().getEndTimeOfGiveTime(toDate);
			} else {
				to = TimeUtils.instance().getEndTimeOfGiveTime(fromDate);
			}
		}
		logger.info("hi here");
		Map<String, Object> dataMap = reportDao.getAllOrder(orderId, storeId, customerId, firstIndex, rowPerPage,
				status, from, to, deliveryDate, serviceOrProduct, deliveryBoyId, orderListId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Integer, String> orderStatus = orderService.getAllOrderStatus();
			Map<Integer, String> orderStatusAr = orderService.getAllOrderStatusAr();
			List<Order> orderlist = (List<Order>) dataMap.get("OrderList");

			long totalCounts = (long) dataMap.get("TotalCounts");
			OrderVo ordervo = null;
			for (Order order : orderlist) {
				ordervo = new OrderVo();
				ordervo = modelMapper.map(order, OrderVo.class);
				ordervo.setOrderStatusName(orderStatus.get(order.getOrderstatus()));
				ordervo.setOrderStatusNameAr(orderStatusAr.get(order.getOrderstatus()));
				ordervo.setOrderstatus(order.getOrderstatus());
				double value = order.getNetPrice();
				double valueRounded = Math.round(value * 100D) / 100D;
				ordervo.setNetPrice(valueRounded);
				OrderDetailsVo ovo = null;
				TransactionModel trvo = null;
				for (OrderDetails orDet : order.getOrderDetails()) {

					ovo = modelMapper.map(orDet, OrderDetailsVo.class);
					ovo.setItemStatusName(orderStatus.get(orDet.getStatus()));
					ovo.setItemStatusNameAr(orderStatusAr.get(orDet.getStatus()));
					if (orDet.getCancellationDate() != 0) {
						ovo.setCancellationDate(TimeUtils.instance().getDateStrFromMilSec(orDet.getCancellationDate()));
					}
					if (orDet.getReturnTime() != 0) {
						ovo.setReturnTime(TimeUtils.instance().getDateStrFromMilSec(orDet.getReturnTime()));
					}
					TaxManagement tm = generalService.getTax(ovo.getTaxId());
					if (tm != null) {
						ovo.setTaxName(tm.getTaxName());
						ovo.setTaxNameAr(tm.getTaxNameAr());
						ovo.setTaxPercentage(tm.getTaxPercentage());
					}
					Offer offer = generalService.getOffer(ovo.getOfferId());
					if (offer != null) {
						ovo.setOfferName(offer.getOfferName());
						ovo.setOfferNameAr(offer.getOfferNameAr());
						if (offer.getOfferRate() > 0) {
							ovo.setOfferRate(offer.getOfferRate());
						}

						if (offer.getOfferPercentage() > 0) {
							ovo.setOfferPercentage(offer.getOfferPercentage());
						}

					}

					ordervo.getOrderDetailsVo().add(ovo);
				}
				long userId = order.getCustomerId();
				CustomerDetails cust = generalService.getCustomerFromUserId(userId);
				if (cust != null) {
					ordervo.setCustomerName(cust.getCustomerName());
					ordervo.setCustomerNameAr(cust.getCustomerNameAr());
					ordervo.setEmailId(cust.getEmailId());
					ordervo.setAddress(cust.getAddress());
					ordervo.setPhoneNumber(cust.getPhoneNo());

				}

				orderVos.add(ordervo);
				logger.info(ordervo.toString());
			}
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = orderVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllAdd(int status[], int firstIndex, int rowPerPage, long companyId, long modeId,
			long modeIds[], long arabicOrEnglish, int stat) {
		BasicResponse res = null;
		long to = 0;
		long from = 0;
		List<AddVo> addVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = reportDao.getAllAdd(status, firstIndex, rowPerPage, companyId, modeId, modeIds,
				arabicOrEnglish, stat);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			List<Advertisement> orderlist = (List<Advertisement>) dataMap.get("AdvList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			AddVo ordervo = null;
			for (Advertisement order : orderlist) {
				ordervo = modelMapper.map(order, AddVo.class);

				addVos.add(ordervo);

			}

			returnMap.put("Add", addVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = addVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse getAllTransBy(long mode, long fromDate, long toDate, long toWhoId, int cashType, int tranType,
			int currentIndex, int rowPerPage) {
		BasicResponse res = null;
		Map<String, Object> map = reportDao.getAllTransBy(mode, fromDate, toDate, toWhoId, cashType, tranType,
				currentIndex, rowPerPage);
		res = new BasicResponse();
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			List<TransactionModel> transactionModels = new ArrayList<>();
			Map<Integer, String> tranMap = getAllTransMap();
			List<Transactions> transactions = (List<Transactions>) map.get(DATA);
			TransactionModel trVo = null;
			for (Transactions tran : transactions) {
				trVo = modelMapper.map(tran, TransactionModel.class);
				trVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(tran.getLastUpdatedTime()));
				if (tran.getBankOrCash() == TRAN_CASH) {
					trVo.setCashOrBank("CASH");
				} else if (tran.getBankOrCash() == TRAN_BANK) {
					trVo.setCashOrBank("BANK");
				}
				trVo.setTranMode(tranMap.get(tran.getTransactionMode()));
				transactionModels.add(trVo);
			}
			Map<String, Object> returnMap = new HashMap<>();
			returnMap.put("TRANS", transactionModels);
			returnMap.put("TOATALAMT", map.get("TOTALAMOUNT"));
			returnMap.put("DEBITAMT", map.get("DEBITAMOUNT"));
			returnMap.put("ONLINEAMT", map.get("ONLINEAMOUNT"));
			returnMap.put("CODAMT", map.get("CODAMOUNT"));
			returnMap.put("RETURNAMT", map.get("RETURNAMOUNT"));
			res.object = returnMap;
			res.totalRecords = (long) map.get(TOTAL_COUNT);
		}
		return res;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public BasicResponse bestPurchasedProducts(long orderId,long
	 * orderListId,long customerId,long fromDate,long toDate,long sellerId,int
	 * firstIndex,int rowPerPage,long companyId, long categoryId,long subCategoryId)
	 * { BasicResponse res = null; long to=0; long from=0; List<OrderDetailsVo>
	 * orderVos = new ArrayList<>(); Map<String, Object> returnMap = new
	 * HashMap<>(); List<OrderDetailsVo> odVoList = new ArrayList<>(); if(fromDate
	 * !=0) { from =TimeUtils.instance().getStartTimeOfGiveTime(fromDate);
	 * if(toDate!=0) { to=TimeUtils.instance().getEndTimeOfGiveTime(toDate); }else {
	 * to=TimeUtils.instance().getEndTimeOfGiveTime(fromDate); } } Map<String,
	 * Object> dataMap = reportDao.bestPurchasedProducts(orderId, orderListId,
	 * customerId, from, to, sellerId, firstIndex, rowPerPage, companyId,
	 * categoryId, subCategoryId); if ((int) dataMap.get(ERROR_CODE) ==
	 * ERROR_CODE_NO_ERROR) {logger.info(
	 * "dataMap1------------------------------------------------------------------------------------"
	 * ); List<Object[]> cust = (List<Object[]>) dataMap.get("qtyList");
	 * modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT)
	 * ; logger.info(cust+"cust--------------------------------cust.size()-----");
	 * logger.info(cust.size()+
	 * "cust.size()---------------------------------cust.size()-----");
	 * OrderDetailsVo odVo = null; for (Object[] cat : cust) {
	 * 
	 * odVo = modelMapper.map(cat[0], OrderDetailsVo.class);
	 * odVo.setTotalQtyPurchased((long) cat[0]); odVo.setPurchaseProductId((long)
	 * cat[1]);
	 * 
	 * Map<String, Object> dataMap1 =
	 * inputDao.getAllProducts(odVo.getPurchaseProductId(), 0, 0, 0, 0, 0, 0, 0, 0,
	 * 100, 2, null, 0, 0, 0, 0, 0, 0, 0, null, null); logger.info(dataMap1+
	 * "dataMap1------------------------------------------------------------------------------------"
	 * ); List<Product> products = (List<Product>) dataMap1.get("ProudctList");
	 * logger.info(products+
	 * "products----------------------------------products*-------------------------------------------------"
	 * ); if ((int) dataMap1.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) { Map<Long,
	 * String> categoryMap = inputService.getAllCategoryNames(); Map<Long, String>
	 * sub =inputService.getAllSubCategoryNames(); ProductVo prVo = null; for
	 * (Product prod : products) {
	 * 
	 * prVo = modelMapper.map(prod, ProductVo.class);
	 * odVo.setItemName(prVo.getProductName());
	 * odVo.setCategoryId(prVo.getCategoryId());
	 * odVo.setCategoryName(categoryMap.get(prod.getCategoryId()));
	 * odVo.setSubCategoryName(sub.get(prod.getSubCategoryId()));
	 * odVo.setSubCategoryId(prVo.getSubCategoryId()); } } odVoList.add(odVo);
	 * 
	 * 
	 * }
	 * 
	 * if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) { List<Product>
	 * products = (List<Product>) dataMap.get("ProudctList"); ProductVo prVo = null;
	 * for (Product prod : products) {
	 * 
	 * prVo = modelMapper.map(prod, ProductVo.class);
	 * odVo.setItemName(prVo.getProductName()); } returnMap.put("Report", odVoList);
	 * 
	 * List<OrderDetails> orderlist = (List<OrderDetails>) dataMap.get(DATA);
	 * 
	 * long totalCounts = (long) dataMap.get("TotalCounts"); OrderDetailsVo ordervo
	 * = null; for (OrderDetails order : orderlist) { ordervo =
	 * modelMapper.map(order, OrderDetailsVo.class);
	 * 
	 * 
	 * orderVos.add(ordervo); } returnMap.put("Order", orderVos); if (orderId > 0) {
	 * returnMap.put("Order", orderVos.get(0)); List<Review> reviews =
	 * (List<Review>) dataMap.get("Reviews"); List<ReviewVo> reviewVos = new
	 * ArrayList<>(); ReviewVo revVo = null;
	 * 
	 * for (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class); //
	 * ordervo.setLastUpdatedTime(TimeUtils.instance().getDateStrFromMilSec(re.
	 * getLastUpdatedTime())); reviewVos.add(revVo); } returnMap.put("REVIEWS",
	 * reviewVos); } res = new BasicResponse(); res.errorCode = (int)
	 * dataMap.get(ERROR_CODE); res.errorMessage = (String)
	 * dataMap.get(ERROR_MESSAGE); res.errorMessageArabic = (String)
	 * dataMap.get(ERROR_MESSAGE_ARABIC); res.totalRecords = (long)
	 * dataMap.get("TotalCounts"); res.object = odVoList;
	 * 
	 * } else { res = new BasicResponse(); res.errorCode = (int)
	 * dataMap.get(ERROR_CODE); res.errorMessage = (String)
	 * dataMap.get(ERROR_MESSAGE); res.errorMessageArabic = (String)
	 * dataMap.get(ERROR_MESSAGE_ARABIC); } return res; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse bestPurchasedProducts(long orderId, long orderListId, long customerId, long fromDate,
			long toDate, long sellerId, int firstIndex, int rowPerPage, long companyId, long categoryId,
			long subCategoryId) {
		BasicResponse res = null;
		long to = toDate;
		long from = fromDate;
		List<OrderDetailsVo> orderVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		List<OrderDetailsVo> odVoList = new ArrayList<>();
		/*
		 * if(fromDate !=0) { from
		 * =TimeUtils.instance().getStartTimeOfGiveTime(fromDate); if(toDate!=0) {
		 * to=TimeUtils.instance().getEndTimeOfGiveTime(toDate); }else {
		 * to=TimeUtils.instance().getEndTimeOfGiveTime(fromDate); } }
		 */
		Map<String, Object> dataMap = reportDao.bestPurchasedProducts(orderId, orderListId, customerId, from, to,
				sellerId, firstIndex, rowPerPage, companyId, categoryId, subCategoryId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
		//	logger.info("dataMap1------------------------------------------------------------------------------------");
			List<Object[]> cust = (List<Object[]>) dataMap.get("qtyList");
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			//logger.info(cust + "cust--------------------------------cust.size()-----");
			//logger.info(cust.size() + "cust.size()---------------------------------cust.size()-----");
			OrderDetailsVo odVo = null;
			for (Object[] cat : cust) {

				odVo = modelMapper.map(cat[0], OrderDetailsVo.class);
				odVo.setTotalQtyPurchased((long) cat[0]);
				odVo.setPurchaseProductId((long) cat[1]);

				Map<String, Object> dataMap1 = inputDao.getAllProducts(odVo.getPurchaseProductId(), 0, 0, 0, 0, 0, 0, 0,
						0, 100, 2, null, 0, 0, 0, 0, 0, 0, 0, null, null, 0, null, 0, 0);
				//logger.info(dataMap1
					//	+ "dataMap1------------------------------------------------------------------------------------");
				List<Product> products = (List<Product>) dataMap1.get("ProudctList");
				//logger.info(products
						///+ "products----------------------------------products*-------------------------------------------------");
				if ((int) dataMap1.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
					Map<Long, String> categoryMap = inputService.getAllCategoryNames();
					Map<Long, String> sub = inputService.getAllSubCategoryNames();
					ProductVo prVo = null;
					for (Product prod : products) {

						prVo = modelMapper.map(prod, ProductVo.class);
						odVo.setItemName(prVo.getProductName());
						odVo.setCategoryId(prVo.getCategoryId());
						odVo.setCategoryName(categoryMap.get(prod.getCategoryId()));
						odVo.setSubCategoryName(sub.get(prod.getSubCategoryId()));
						odVo.setSubCategoryId(prVo.getSubCategoryId());
					}
				}
				odVoList.add(odVo);

			}
			/*
			 * if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) { List<Product>
			 * products = (List<Product>) dataMap.get("ProudctList"); ProductVo prVo = null;
			 * for (Product prod : products) {
			 * 
			 * prVo = modelMapper.map(prod, ProductVo.class);
			 * odVo.setItemName(prVo.getProductName()); }
			 */
			returnMap.put("Report", odVoList);

			/*
			 * List<OrderDetails> orderlist = (List<OrderDetails>) dataMap.get(DATA);
			 * 
			 * long totalCounts = (long) dataMap.get("TotalCounts"); OrderDetailsVo ordervo
			 * = null; for (OrderDetails order : orderlist) { ordervo =
			 * modelMapper.map(order, OrderDetailsVo.class);
			 * 
			 * 
			 * orderVos.add(ordervo); } returnMap.put("Order", orderVos);
			 */
			/*
			 * if (orderId > 0) { returnMap.put("Order", orderVos.get(0)); List<Review>
			 * reviews = (List<Review>) dataMap.get("Reviews"); List<ReviewVo> reviewVos =
			 * new ArrayList<>(); ReviewVo revVo = null;
			 * 
			 * for (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class); //
			 * ordervo.setLastUpdatedTime(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); reviewVos.add(revVo); } returnMap.put("REVIEWS",
			 * reviewVos); }
			 */
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.totalRecords = (long) dataMap.get("TotalCounts");
			res.object = odVoList;

		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllOrdersDetails(long orderId, long orderListId, long customerId, long fromDate,
			long toDate, long sellerId, int firstIndex, int rowPerPage, long companyId) {
		BasicResponse res = null;
		long to = 0;
		long from = 0;
		List<OrderDetailsVo> orderVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		if (fromDate != 0) {
			from = TimeUtils.instance().getStartTimeOfGiveTime(fromDate);
			if (toDate != 0) {
				to = TimeUtils.instance().getEndTimeOfGiveTime(toDate);
			} else {
				to = TimeUtils.instance().getEndTimeOfGiveTime(fromDate);
			}
		}
		Map<String, Object> dataMap = reportDao.getAllOrdersDetails(orderId, orderListId, customerId, from, to,
				sellerId, firstIndex, rowPerPage, companyId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			List<OrderDetails> orderlist = (List<OrderDetails>) dataMap.get(DATA);
			long totalCounts = (long) dataMap.get("TotalCounts");
			OrderDetailsVo ordervo = null;
			for (OrderDetails order : orderlist) {
				ordervo = modelMapper.map(order, OrderDetailsVo.class);
				orderVos.add(ordervo);
			}
			returnMap.put("Order", orderVos);
			/*
			 * if (orderId > 0) { returnMap.put("Order", orderVos.get(0)); List<Review>
			 * reviews = (List<Review>) dataMap.get("Reviews"); List<ReviewVo> reviewVos =
			 * new ArrayList<>(); ReviewVo revVo = null;
			 * 
			 * for (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class); //
			 * ordervo.setLastUpdatedTime(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); reviewVos.add(revVo); } returnMap.put("REVIEWS",
			 * reviewVos); }
			 */
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = orderVos;
			// res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse getAllDetails(long userId, int role) {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = reportDao.getAllDetails(userId, role);
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			map.remove(ERROR_CODE);
			map.remove(ERROR_MESSAGE);
			map.remove(ERROR_MESSAGE_ARABIC);
			res.object = map;
		}
		return res;
	}

	@Override
	public BasicResponse updateOrder(long orderId, String paymentId, int paymentStatus) {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = reportDao.updateOrder(orderId, paymentId, paymentStatus);

		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			map.remove(ERROR_CODE);
			map.remove(ERROR_MESSAGE);
			map.remove(ERROR_MESSAGE_ARABIC);
			res.object = map;
		}
		return res;
	}

	@Override
	public BasicResponse getAllSalesReport(long storeId, long customerId, int firstIndex, int rowPerPage,
			long deliveryBoyId, long categoryId, long productId, long sellerId, long fromDate, long toDate,
			long subCategoryId, int transactionMode) {

		BasicResponse res = null;
		long to = 0;
		long from = 0;
		List<TransactionModel> tranVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		if (fromDate != 0) {
			from = TimeUtils.instance().getStartTimeOfGiveTime(fromDate);
			if (toDate != 0) {
				to = TimeUtils.instance().getEndTimeOfGiveTime(toDate);
			} else {
				to = TimeUtils.instance().getEndTimeOfGiveTime(fromDate);
			}
		}
		Map<String, Object> dataMap = reportDao.getAllSalesReport(storeId, customerId, firstIndex, rowPerPage,
				deliveryBoyId, categoryId, productId, sellerId, fromDate, toDate, subCategoryId, transactionMode);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) 
		{
			res = new BasicResponse();
			Map<Long, String> categoryMap = inputService.getAllCategoryNames();
			Map<Long, String> categoryMapAr = inputService.getAllCategoryNamesAr();
			Map<Long, String> customerMap = inputService.getAllCustomerName();
			Map<Long, String> customerMapAr = inputService.getAllCustomerNameAr();
			logger.info(customerMap + "customerMap--------------------------------------------------------customerMap");
			Map<Long, String> sub = inputService.getAllSubCategoryNames();
			Map<Long, String> subAr = inputService.getAllSubCategoryNamesAr();
			List<Object[]> tranlist = (List<Object[]>) dataMap.get("TranscList");
			logger.info(tranlist.size()
					+ "tranlist-------------------------------------------------------------------------99");

			TransactionModel trvo = null;

			for (Object[] tran : tranlist) {
				logger.info(transactionMode
						+ "transactionMode--------------------------------------^^^------------------------");
				logger.info(tran + "tran-------------------------------------------------------------------------399");
				trvo = modelMapper.map(tran[0], TransactionModel.class);
				if (transactionMode == SALE_REPORT_BY_PRODUCT) {
					trvo.setProductId((long) tran[0]);
					// trvo.setProductName(tran[1].toString());
					trvo.setSumOfOrderId((long) tran[1]);
					trvo.setTotalNetAmount((double) tran[2]);
					trvo.setProductName(tran[3].toString());
					trvo.setProductNameAr(tran[4].toString());
				} else if (transactionMode == SALE_REPORT_BY_CATEGORY) {
					trvo.setCategoryId((long) tran[0]);
					trvo.setCategoryName(categoryMap.get(trvo.getCategoryId()));
					trvo.setCategoryNameAr(categoryMapAr.get(trvo.getCategoryId()));

					trvo.setSumOfOrderId((long) tran[1]);
					trvo.setTotalNetAmount((double) tran[2]);
				} else if (transactionMode == SALE_REPORT_BY_SUBCATEGORY) {

					trvo.setSubCategoryId((long) tran[0]);
					trvo.setSubCategoryName(sub.get(trvo.getSubCategoryId()));
					trvo.setSubCategoryNameAr(subAr.get(trvo.getSubCategoryId()));
					trvo.setSumOfOrderId((long) tran[1]);
					trvo.setTotalNetAmount((double) tran[2]);
				}

				else if (transactionMode == SALE_REPORT_BY_CUSTOMER) {
					trvo.setCustomerId((long) tran[0]);
					logger.info(
							trvo.getCustomerId() + "____________________________________________________iddsssssss");
					trvo.setCustomerName(customerMap.get(trvo.getCustomerId()));
					trvo.setCustomerNameAr(customerMapAr.get(trvo.getCustomerId()));
					trvo.setSumOfOrderId((long) tran[1]);
					trvo.setTotalNetAmount((double) tran[2]);
				} else if (transactionMode == SALE_REPORT_BY_STORE) {
					trvo.setSellerId((long) tran[0]);

					trvo.setSumOfOrderId((long) tran[1]);
					trvo.setTotalNetAmount((double) tran[2]);
				}

				tranVos.add(trvo);
			}
			
			
			long totalRecords= (long) dataMap.get("total");
			res.totalRecords =totalRecords;
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
		    res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		    res.object = tranVos;
		    return res;
		}
		else
		{	
			logger.info("no data to show-------------------------------------");
		res = new BasicResponse();
		res.errorCode = -3;
		res.errorMessage = "NO DATA";
	    res.errorMessageArabic = "NO DATA";
	    
		}
	   		return res;
	}

	@Override
	public BasicResponse getPromocodeReport(int firstIndex, int rowPerPage, long fromDate, long toDate, long codeId,
			long userId, long promoterId) {
		BasicResponse res = null;
		double price = 0;
		long code = 0;
		List<GetPromocodeDetails> offVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		List<OrderVo> orderVos = new ArrayList<>();
		OrderVo ordervo = null;

		if (promoterId > 0) {
			Map<String, Object> dataMap = generalDao.getPromocodeDetails(0, promoterId, 1, 0, 0, 0, 1, null);
			if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

				List<PromocodeDetails> citylist = (List<PromocodeDetails>) dataMap.get("promoList");
				code = citylist.get(0).getCodeId();
				logger.info(code + "+++++++++++++++++++||+++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			}

		}
		Map<String, Object> dataMap = reportDao.getPromocodeReport(firstIndex, rowPerPage, fromDate, toDate, codeId,
				userId, promoterId, code);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Integer, String> orderStatus = orderService.getAllOrderStatus();
			List<Order> orderlist = (List<Order>) dataMap.get("promoList");
			// logger.info(CommonService.getJson(orderlist)+"orderlist------------|||||||||-----------------------------------");
			long totalCounts = (long) dataMap.get("TotalCounts");

			for (Order order : orderlist) {
				ordervo = new OrderVo();
				ordervo = modelMapper.map(order, OrderVo.class);
				ordervo.setOrderStatusName(orderStatus.get(order.getOrderstatus()));
				ordervo.setPromocodeId(order.getPromocodeId());
				OrderDetailsVo ovo = null;
				TransactionModel trvo = null;
				price = price + ordervo.getNetPrice();
				for (OrderDetails orDet : order.getOrderDetails()) {
					ovo = modelMapper.map(orDet, OrderDetailsVo.class);
					/// ovo.setStoreId(order.getStoreId());
					// ovo.setStoreName(orDet.getProduct().getShop().getStoreName());

					ordervo.getOrderDetailsVo().add(ovo);
				}

				userId = order.getCustomerId();

				CustomerDetails cust = generalService.getCustomerFromUserId(userId);
				if (cust != null) {
					ordervo.setCustomerName(cust.getCustomerName());
					ordervo.setAddress(cust.getAddress());
					ordervo.setPhoneNumber(cust.getPhoneNo());
					// ordervo.setAddressAr(cust.getAddressAr());
				}

				// ordervo.setPromoReportPrice(price);
				orderVos.add(ordervo);

				logger.info(ordervo.toString());
			}
			returnMap.put("PRICE", price);
			returnMap.put("ORDER", orderVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = returnMap;

			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse getAllSalesReport(long storeId, long customerId, int firstIndex, int rowPerPage,
			long deliveryBoyId, long categoryId, long productId, long sellerId, long fromDate, long toDate,
			long orderId, long subCategoryId, long transactionId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BasicResponse monthlyRevenueReport(int firstIndex, int rowPerPage, long fromDate, long toDate, long codeId) {
		logger.info("()" + "aCountQuery------------------------------------------------------");
		List<GetTransactionVo> reportModelList = new ArrayList<>();
		GetTransactionVo reportVo = null;
		BasicResponse res = null;
		Map<String, Object> dataMap = reportDao.monthlyRevenueReport(firstIndex, rowPerPage, fromDate, toDate, codeId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			List<Object[]> objList = (List<Object[]>) dataMap.get("TranscList");
			for (Object[] objct : objList) {
				reportVo = new GetTransactionVo();
				reportVo.setMonthlyTotal((double) objct[0]);
				try {
					reportVo.setMonthyDate(TimeUtils.instance().getMilSecFromDateFormatYear(String.valueOf(objct[1])));
					Calendar c = Calendar.getInstance();
					// Set time in milliseconds
					c.setTimeInMillis(reportVo.getMonthyDate());
					int mYear = c.get(Calendar.YEAR);
					int mMonth = c.get(Calendar.MONTH) + 1;
					logger.info(mMonth + "mMonth------------------------month------------------------");
					int mDay = c.get(Calendar.DAY_OF_MONTH);
					int hr = c.get(Calendar.HOUR);
					int min = c.get(Calendar.MINUTE);
					int sec = c.get(Calendar.SECOND);
					reportVo.setYear(mYear);
					reportVo.setMonth(mMonth);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*
				 * try {
				 * reportVo.setTime(TimeUtils.instance().getMilSecFromDateStr(String.valueOf(
				 * objct[2]))); } catch (Exception e) { e.printStackTrace(); }
				 */
				reportModelList.add(reportVo);
			}

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.object = reportModelList;
		}
		return res;
	}

	public BasicResponse yearlyRevenueReport(int firstIndex, int rowPerPage, long fromDate, long toDate, long codeId) {
		logger.info("()" + "aCountQuery------------------------------------------------------");
		List<GetTransactionVo> reportModelList = new ArrayList<>();
		GetTransactionVo reportVo = null;
		BasicResponse res = null;
		Map<String, Object> dataMap = reportDao.yearlyRevenueReport(firstIndex, rowPerPage, fromDate, toDate, codeId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			List<Object[]> objList = (List<Object[]>) dataMap.get("TranscList");
			for (Object[] objct : objList) {
				reportVo = new GetTransactionVo();
				reportVo.setMonthlyTotal((double) objct[0]);
				try {
					reportVo.setMonthyDate(TimeUtils.instance().getMilSecFromDateFormatYear(String.valueOf(objct[1])));
					Calendar c = Calendar.getInstance();
					// Set time in milliseconds
					c.setTimeInMillis(reportVo.getMonthyDate());
					int mYear = c.get(Calendar.YEAR);
					int mMonth = c.get(Calendar.MONTH);
					int week = c.get(Calendar.WEEK_OF_MONTH);
					logger.info(week + "week------------------------week------------------------");
					int mDay = c.get(Calendar.DAY_OF_MONTH);
					int hr = c.get(Calendar.HOUR);
					int min = c.get(Calendar.MINUTE);
					int sec = c.get(Calendar.SECOND);
					reportVo.setYear(mYear);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*
				 * try {
				 * reportVo.setTime(TimeUtils.instance().getMilSecFromDateStr(String.valueOf(
				 * objct[2]))); } catch (Exception e) { e.printStackTrace(); }
				 */
				reportModelList.add(reportVo);
			}

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.object = reportModelList;
		}
		return res;
	}

	public BasicResponse weeklyRevenueReport(int firstIndex, int rowPerPage, long fromDate, long toDate, long codeId) {
		logger.info("()" + "aCountQuery------------------------------------------------------");
		List<GetTransactionVo> reportModelList = new ArrayList<>();
		GetTransactionVo reportVo = null;
		BasicResponse res = null;
		Map<String, Object> dataMap = reportDao.weeklyRevenueReport(firstIndex, rowPerPage, fromDate, toDate, codeId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			List<Object[]> objList = (List<Object[]>) dataMap.get("TranscList");
			for (Object[] objct : objList) {
				reportVo = new GetTransactionVo();
				reportVo.setMonthlyTotal((double) objct[0]);
				try {
					reportVo.setMonthyDate(TimeUtils.instance().getMilSecFromDateFormatYear(String.valueOf(objct[1])));
					Calendar c = Calendar.getInstance();
					// Set time in milliseconds
					logger.info(reportVo.getMonthyDate()
							+ "reportVo.getMonthyDate()-----------------------------------------");
					c.setTimeInMillis(reportVo.getMonthyDate());
					int mYear = c.get(Calendar.YEAR);
					int mMonth = c.get(Calendar.MONTH) + 1;
					int week = c.get(Calendar.WEEK_OF_MONTH);
					logger.info(week + "week------------------------week------------------------");
					logger.info(mMonth + "mMonth-----------------------mMonth-----------------------mMonth-");
					reportVo.setYear(mYear);
					reportVo.setMonth(mMonth);
					reportVo.setWeek(week);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*
				 * try {
				 * reportVo.setTime(TimeUtils.instance().getMilSecFromDateStr(String.valueOf(
				 * objct[2]))); } catch (Exception e) { e.printStackTrace(); }
				 */
				reportModelList.add(reportVo);
			}

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.object = reportModelList;
		}
		return res;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public BasicResponse getAllAuctionFrom(long auctionId,long
	 * auctionDetaiilsId, long customerId, int firstIndex, int rowPerPage,int
	 * status[],long fromDate,long toDate) { BasicResponse res=new BasicResponse();
	 * long to=0; long from=0; if(fromDate !=0) { from
	 * =TimeUtils.instance().getStartTimeOfGiveTime(fromDate); if(toDate!=0) {
	 * to=TimeUtils.instance().getEndTimeOfGiveTime(toDate); }else {
	 * to=TimeUtils.instance().getEndTimeOfGiveTime(fromDate); } }
	 * Map<String,Object> map=reportDao.getAllAuctionBy(auctionId,
	 * auctionDetaiilsId, customerId, firstIndex, rowPerPage, status, from, to);
	 * res.errorCode=(int) map.get(ERROR_CODE); res.errorMessage=(String)
	 * map.get(ERROR_MESSAGE); res.errorMessageArabic=(String)
	 * map.get(ERROR_MESSAGE_ARABIC); if((int)map.get(ERROR_CODE) ==
	 * ERROR_CODE_NO_ERROR) { List<AuctionVo> vos=new ArrayList<>(); List<Auction>
	 * auction=(List<Auction>) map.get("AuctionList"); AuctionVo auctionVo=null;
	 * for(Auction auc:auction) { auctionVo=modelMapper.map(auc,AuctionVo.class);
	 * auctionVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(
	 * auc.getLastUpdatedTime())); vos.add(auctionVo); } if(auctionId > 0) {
	 * res.object=vos.get(0); }else {
	 * 
	 * res.object=vos; } } return res; }
	 * 
	 * 
	 * @Override public BasicResponse getAllAuctionDetailsBy(long auctionId, long
	 * auctionDetailsId, long customerId,int firstIndex,int rowPerPage,long
	 * fromDate,long toDate) { BasicResponse res=new BasicResponse(); long to=0;
	 * long from=0; if(fromDate !=0) { from
	 * =TimeUtils.instance().getStartTimeOfGiveTime(fromDate); if(toDate!=0) {
	 * to=TimeUtils.instance().getEndTimeOfGiveTime(toDate); }else {
	 * to=TimeUtils.instance().getEndTimeOfGiveTime(fromDate); } }
	 * Map<String,Object> map=reportDao.getAllAuctionDetails(auctionId, customerId,
	 * auctionDetailsId, firstIndex, rowPerPage, from, to); res.errorCode=(int)
	 * map.get(ERROR_CODE); res.errorMessage=(String) map.get(ERROR_MESSAGE);
	 * res.errorMessageArabic=(String) map.get(ERROR_MESSAGE_ARABIC);
	 * if((int)map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) { List<AuctionDetailsVo>
	 * vos=new ArrayList<>(); List<AuctionDetails>
	 * auctionDetails=(List<AuctionDetails>) map.get(DATA); AuctionDetailsVo
	 * auctionDetailsVo=null; for(AuctionDetails auc:auctionDetails) {
	 * auctionDetailsVo=modelMapper.map(auc, AuctionDetailsVo.class);
	 * auctionDetailsVo.setLastUpdatedTimeString(TimeUtils.instance().
	 * getDateStrFromMilSec(auc.getLastUpdatedTime())); vos.add(auctionDetailsVo); }
	 * if(auctionDetailsId > 0) { res.object=vos.get(0); }else { res.object=vos; } }
	 * return res; }
	 */

	/*
	 * @Override public BasicResponse getBookingDate(long customerId, int
	 * firstIndex, int rowPerPage,long auctionExpiryTime,long auctionTime,int
	 * auctionStatus) { BasicResponse basicResponse = null; if (auctionExpiryTime <
	 * 0 || auctionTime < 0) { basicResponse = new BasicResponse();
	 * basicResponse.errorCode = 9; basicResponse.errorMessage = "NULL VALUE";
	 * return basicResponse; }
	 * 
	 * Map<String, Object> map =
	 * reportDao.getAllBookings(customerId,firstIndex,rowPerPage,
	 * auctionExpiryTime,auctionTime,auctionStatus); //Map<Long, String> palaceMap =
	 * inputService.getAllPalaceMap(); //Map<Integer, String> sectionMap =
	 * inputService.getAllSectionMap(); //Map<Integer, String> statusMap =
	 * inputService.getAllStatusMap(); basicResponse = new BasicResponse();
	 * basicResponse.errorCode = (int) map.get(ERROR_CODE);
	 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
	 * if(basicResponse.errorCode == ERROR_CODE_NO_ERROR){ List<Auction>
	 * auction=(List<Auction>) map.get("DATA"); List<AuctionVo> vos=new
	 * ArrayList<>(); AuctionVo auctionVo=null; Auction auct=null;
	 * for(AuctionDetails aucDetails:auctionDetails) { auctionVo = new
	 * BookingDetailsVo(); auctionVo.bookingDateString =
	 * TimeUtils.instance().getDateStrFromMilSecAsShort(boDetails.getBookingDate());
	 * auctionVo.bookingId = boDetails.getBookingId(); auctionVo.customerId =
	 * boDetails.getCustomerId(); auctionVo.referenceNo =
	 * boDetails.getReferenceNo();
	 * auctionVo.reservedDate=TimeUtils.instance().getDateStrFromMilSec(boDetails.
	 * getCreatedDate()); auctionVo.price = boDetails.getPrice(); auctionVo.palaceId
	 * = boDetails.getPalaceId();
	 * auctionVo.statusName=statusMap.get(boDetails.getStatus());
	 * auctionVo.palaceName = palaceMap.get(boDetails.getPalaceId());
	 * auctionVo.sectionName=sectionMap.get(boDetails.getSection());
	 * auctionVo.status=boDetails.getStatus();
	 * 
	 * auct = modelMapper.map(auctionVo,Auction.class);
	 * if(auct.getAuctionStatus()==.)
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * if(bookVo.status == BOOKED || bookVo.status == EXPIRED) {
	 * bookVo.bookExpireDate=TimeUtils.instance().getDateStrFromMilSec(TimeUtils.
	 * instance().increaseDayBy(boDetails.getCreatedDate(), 1)); }else
	 * if(bookVo.status == CANCELED || bookVo.status == REJECTED) {
	 * bookVo.cancellationDate=TimeUtils.instance().getDateStrFromMilSec(boDetails.
	 * getCancelationDate()); }else if(bookVo.status == CONFIRMED) {
	 * bookVo.dateOfPayment=TimeUtils.instance().getDateStrFromMilSec(boDetails.
	 * getDateOfPayment()); } bookVo.userId = boDetails.getUserId(); CustomerDetails
	 * customer = userService.getCustomerFromUserId(boDetails.getCustomerId());
	 * if(customer != null) { bookVo.customerName = customer.getCustomerName();
	 * bookVo.phoneNo=customer.getPhoneNo(); bookVo.custEmail=customer.getEmailId();
	 * } vos.add(bookVo); } basicResponse.object=vos;
	 * basicResponse.totalRecords=(long) map.get("TotalCounts"); } return
	 * basicResponse; }
	 * 
	 * 
	 * 
	 */

	/*
	 * @Override public BasicResponse getSubscribeReportFromMode(long fromDate, long
	 * toDate,Map<String, String> dataMap, int mode, int status) { BasicResponse res
	 * = null; if (fromDate == 0 || mode == 0 || toDate < 0) { res = new
	 * BasicResponse(); res.errorCode = 3; res.errorMessage = MESSAGE_NULL_VALUE;
	 * return res; } toDate = TimeUtils.instance().getEndTimeOfGiveTime(toDate);
	 * logger.info(TimeUtils.instance().getDateStrFromMilSecAsShort(toDate));
	 * logger.info(TimeUtils.instance().getDateStrFromMilSecAsShort(fromDate));
	 * Object name[]=new Object[10]; StringBuffer str = new
	 * StringBuffer("from Subscribed s where "); if (mode ==
	 * REPORT_MODE_BETWEEN_TWO_DATE) {
	 * str.append("s.fromDate between :FDATE and :TDATE"); } else if (mode ==
	 * REPORT_MODE_DAY_SUBSCRIBED) {
	 * str.append("s.fromDate between :FDATE and :TDATE and s.SubscribeDays =:DATA"
	 * ); } else if (mode == REPORT_MODE_ONE_DATE_TO_ALL) {
	 * str.append("s.fromDate > :FDATE"); } else if (mode ==
	 * REPORT_MODE_PERIOD_TYPE) {
	 * str.append("s.fromDate between :FDATE and :TDATE and s.periodType =:DATA"); }
	 * else if (mode == REPORT_MODE_TIME) {
	 * str.append("s.fromDate between :FDATE and :TDATE and s.time =:DATA"); } else
	 * if (mode == REPORT_MODE_SERVICE) {
	 * str.append("s.fromDate between :FDATE and :TDATE and s.serviceId =:DATA"); }
	 * str.append(" and s.status =:STATUS"); Map<String, Object> map =
	 * reportDao.getSubscriptionDays(str, fromDate, toDate, name, mode, status); res
	 * = new BasicResponse(); res.errorCode = (int) map.get(ERROR_CODE);
	 * res.errorMessage = (String) map.get(ERROR_MESSAGE); if (res.errorCode ==
	 * ERROR_CODE_NO_ERROR) { Map<Integer, String> periodMap =
	 * generalService.getSalaryModes(); Map<Long, String> serviceMap =
	 * inputService.getAllServiceNames(); Map<Long, String> categoryMap =
	 * inputService.getAllCategoryNames(); List<FeatureVo> subscribedVos = new
	 * ArrayList<>(); List<FeaturesAndPhotosProduct> subscribeds =
	 * (List<FeaturesAndPhotosProduct>) map.get("reportData"); FeatureVo
	 * subscribedVo = null; for (FeaturesAndPhotosProduct sub : subscribeds) {
	 * subscribedVo = new FeatureVo(); subscribedVo.days = sub.getSubscribeDays();
	 * subscribedVo.userId = sub.getUserId(); subscribedVo.periodTypeValue =
	 * periodMap.get(sub.getPeriodType()); subscribedVo.periodType =
	 * sub.getPeriodType(); subscribedVo.serviceId = sub.getServiceId();
	 * subscribedVo.serviceName = serviceMap.get(sub.getServiceId()); if
	 * (sub.getStatus() == ACTIVE) { subscribedVo.status = true; } else {
	 * subscribedVo.status = false; } subscribedVo.subscribeId = sub.getServiceId();
	 * subscribedVo.categoryName=categoryMap.get(sub.getCategoryId());
	 * subscribedVo.time = sub.getTime(); subscribedVo.address=sub.getAddress();
	 * subscribedVo.createdDate=TimeUtils.instance().getDateStrFromMilSecAsShort(sub
	 * .getLastUpdatedTime()); logger.info(sub.getCustomerId()); CustomerDetails
	 * customer = generalService.getCustomerFromUserId(sub.getCustomerId());
	 * subscribedVo.customerName = customer.getCustomerName();
	 * subscribedVo.phoneNo=customer.getPhoneNo(); subscribedVos.add(subscribedVo);
	 * } res.object = subscribedVos; } return res; }
	 * 
	 * @Override public BasicResponse getBookingDetailsReportFromMode(long fromDate,
	 * long toDate,Map<String, String> dataMap, int mode, int status) {
	 * BasicResponse res = null; if (fromDate == 0 || mode == 0 || toDate < 0) { res
	 * = new BasicResponse(); res.errorCode = 3; res.errorMessage =
	 * MESSAGE_NULL_VALUE; return res; } toDate =
	 * TimeUtils.instance().getEndTimeOfGiveTime(toDate); logger.info(fromDate
	 * +"data diff"+toDate); Object name[]=new Object[10]; StringBuffer str = new
	 * StringBuffer("from BookingDetails bd where "); if (mode ==
	 * REPORT_MODE_BETWEEN_TWO_DATE) { str.
	 * append("bd.bookingDate between :FDATE and :TDATE and bd.status =:STATUS order by bd.lastUpdatedTime"
	 * ); } else if (mode == REPORT_MODE_ONE_DATE_TO_ALL) {
	 * str.append("bd.bookingDate > :FDATE"); } else if (mode == REPORT_MODE_TIME) {
	 * str.
	 * append("bd.bookingDate between :FDATE and :TDATE and bd.time =:DATA and bd.status =:STATUS order by bd.lastUpdatedTime"
	 * ); } else if (mode == REPORT_MODE_SERVICE) { str.
	 * append("bd.bookingDate between :FDATE and :TDATE and bd.serviceId =:DATA and bd.status =:STATUS order by bd.lastUpdatedTime"
	 * ); } else if (mode == REPORT_MODE_BETWEEN_TWO_DATE_WITHOUT_STATUS){
	 * str.append("bd.bookingDate between :FDATE and :TDATE"); } Map<String, Object>
	 * map = reportDao.getBookingDays(str, fromDate, toDate, name, mode, status);
	 * res = new BasicResponse(); res.errorCode = (int) map.get(ERROR_CODE);
	 * res.errorMessage = (String) map.get(ERROR_MESSAGE); if (res.errorCode ==
	 * ERROR_CODE_NO_ERROR) { Map<Long, String> serviceMap =
	 * inputService.getAllServiceNames(); Map<Long, String>
	 * categoryMap=inputService.getAllCategoryNames(); List<Shop> bookingDetails =
	 * (List<Shop>) map.get("reportData"); List<ShopVo> bookingDetailsVos = new
	 * ArrayList<>(); ShopVo bookVo = null; for (Shop boDetails : bookingDetails) {
	 * bookVo = new ShopVo(); bookVo.address = boDetails.getAddress();
	 * bookVo.bookingDate =
	 * TimeUtils.instance().getDateStrFromMilSecAsShort(boDetails.getBookingDate());
	 * bookVo.bookingId = boDetails.getBookingId(); bookVo.customerId =
	 * boDetails.getCustomerId(); bookVo.latitude = boDetails.getLatitude();
	 * bookVo.longitude = boDetails.getLongitude(); bookVo.serviceId =
	 * boDetails.getServiceId(); if(boDetails.getStatus() == BOOKED){
	 * bookVo.statusName=STRING_BOOKED; }else if(boDetails.getStatus() ==
	 * PROCESSING){ bookVo.statusName=STRING_PROCESSING; }else{
	 * bookVo.statusName=STRING_SERVICED; } bookVo.serviceName =
	 * serviceMap.get(boDetails.getServiceId());
	 * bookVo.categoryName=categoryMap.get(boDetails.getCategoryId());
	 * bookVo.status=boDetails.getStatus(); bookVo.time = boDetails.getTime();
	 * bookVo.userId = boDetails.getUserId(); CustomerDetails customer =
	 * generalService.getCustomerFromUserId(boDetails.getCustomerId());
	 * bookVo.customerName = customer.getCustomerName();
	 * bookVo.phoneNo=customer.getPhoneNo(); bookingDetailsVos.add(bookVo); }
	 * logger.info(bookingDetailsVos.size()); res.object = bookingDetailsVos; }
	 * return res; }
	 */

}
