package com.innoventiaProject.discount.service.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.innoventiaProject.dao.GeneralDao;
import com.innoventiaProject.dao.input.InputDao;
import com.innoventiaProject.dao.order.OrderDao;
import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.constants.OrderConst;
import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.discount.model.input.CityVo;
import com.innoventiaProject.discount.model.input.CountryVo;
import com.innoventiaProject.discount.model.input.ReturnItemsVo;
import com.innoventiaProject.discount.model.order.CardDetailsVo;
import com.innoventiaProject.discount.model.order.OrderDetailsVo;
import com.innoventiaProject.discount.model.order.OrderVo;
import com.innoventiaProject.discount.model.order.ShippingVo;
import com.innoventiaProject.discount.model.order.TaxVo;
import com.innoventiaProject.discount.service.GeneralService;
import com.innoventiaProject.discount.service.input.InputService;
import com.innoventiaProject.discount.utils.TimeUtils;
import com.innoventiaProject.entity.input.City;
import com.innoventiaProject.entity.input.Country;
import com.innoventiaProject.entity.input.ReturnItems;
import com.innoventiaProject.entity.input.SubCategory;
import com.innoventiaProject.entity.order.CardDetails;
import com.innoventiaProject.entity.order.Order;
import com.innoventiaProject.entity.order.OrderDetails;
import com.innoventiaProject.entity.order.ShippingDetails;
import com.innoventiaProject.entity.order.TaxManagement;
import com.innoventiaProject.entity.order.Transactions;
import com.innoventiaProject.entity.user.CustomerDetails;
import com.innoventiaProject.entity.user.PromocodeDetails;

@Transactional
@Service("orderService")
public class OrderServiceImpl implements OrderService, Constants {
	private static Logger logger = Logger.getLogger(OrderServiceImpl.class);
	OrderConst orderconstatnt;
	@Autowired
	private OrderDao orderDao;

	@Autowired
	private InputDao inputDao;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private InputService inputService;

	@Autowired
	private GeneralDao generalDao;
	@Autowired
	private GeneralService generalService;

	@Override
	public BasicResponse deleteShipping(long shippingId) {

		BasicResponse res = null;
		if (shippingId <= 0) {
			res = new BasicResponse();
			res.errorCode = 1;
			res.errorMessage = "shippingId is zero";
			return res;
		}
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> map = orderDao.deleteShipping(shippingId);
		res = new BasicResponse();
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return res;
	}

	@Override
	public BasicResponse deleteCard(long cardId) {
		BasicResponse res = null;
		if (cardId <= 0) {
			res = new BasicResponse();
			res.errorCode = 1;
			res.errorMessage = "cardId is zero";
			return res;
		}
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> map = orderDao.deleteCard(cardId);
		res = new BasicResponse();
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return res;
	}

	@Override
	public Map<Integer, String> getAllOrderStatus() {
		Map<Integer, String> orderStatusMap = new HashMap<Integer, String>();
		orderStatusMap.put(ORDERED, STRING_ORDERED);
		orderStatusMap.put(CONFIRMED, STRING_CONFIRMED);
		orderStatusMap.put(READY_TO_DELIVERY, STRING_READY_TO_DELIVERY);
		orderStatusMap.put(REJECTED, STRING_REJECTED);
		orderStatusMap.put(DELIVERED, STRING_DELIVERED);
		orderStatusMap.put(CANCELED, STRING_CANCELED);
		orderStatusMap.put(RETURN_REQUESTED, STRING_RETURN_REQUESTED);// 10
		orderStatusMap.put(RETURN_ACCEPTED, STRING_RETURN_ACCEPTED);
		orderStatusMap.put(RETURN_REJECTED, STRING_RETURN_REJECTED);
		orderStatusMap.put(RETURN_CANCELLED, STRING_RETURN_CANCELLED);
		orderStatusMap.put(RETURN_COMPLETED, STRING_RETURN_COMPLETED);
		return orderStatusMap;
	}

	@Override
	public Map<Integer, String> getAllOrderStatusAr() {
		Map<Integer, String> orderStatusMap = new HashMap<Integer, String>();
		orderStatusMap.put(ORDERED, STRING_ORDERED_AR);
		orderStatusMap.put(CONFIRMED, STRING_CONFIRMED_AR);
		orderStatusMap.put(READY_TO_DELIVERY, STRING_READY_TO_DELIVERY_AR);
		orderStatusMap.put(REJECTED, STRING_REJECTED_AR);
		orderStatusMap.put(DELIVERED, STRING_DELIVERED_AR);
		orderStatusMap.put(CANCELED, STRING_CANCELED_AR);
		orderStatusMap.put(RETURN_REQUESTED, STRING_RETURN_REQUESTED_AR);// 10
		orderStatusMap.put(RETURN_ACCEPTED, STRING_RETURN_ACCEPTED_AR);
		orderStatusMap.put(RETURN_REJECTED, STRING_RETURN_REJECTED_AR);
		orderStatusMap.put(RETURN_COMPLETED, STRING_RETURN_COMPLETED_AR);
		return orderStatusMap;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<Long, String[]> getPromocode(int status) {
	 * List<PromocodeDetails> placeList = null; Map<Long, String[]> allplaceMap =
	 * null; Map<String, Object> map = orderDao.getPromo(0, status); placeList =
	 * (List<PromocodeDetails>) map.get("CountryList"); if (placeList != null &&
	 * !placeList.isEmpty()) { allplaceMap = new HashMap<>(); for (PromocodeDetails
	 * c : placeList) { String[] str = { c.getPercentage()};
	 * allplaceMap.put(c.getCountryId(), str); }
	 * 
	 * } return allplaceMap; }
	 */

	/*
	 * @Override public BasicResponse addOrder(OrderVo orderVo) { List<OrderDetails>
	 * oList = null;
	 * 
	 * double totalAmt = 0; BasicResponse res = null; Map<Long, List<OrderDetails>>
	 * orMap = new HashMap<>(); Map<String, Object> tranMap = new
	 * HashMap<String,Object>(); Map<String, Object> onlineMap = new
	 * HashMap<String,Object>(); Map<Long, Order> orders = new HashMap<>();
	 * 
	 * Order order = null;
	 * 
	 * Transactions transaction=new Transactions() ; Transactions transaction1=new
	 * Transactions() ; modelMapper.getConfiguration()
	 * .setMatchingStrategy(MatchingStrategies.STRICT); for (OrderDetailsVo orVo :
	 * orderVo.getOrderDetailsVo()) {
	 * 
	 * if (orders.get(orVo.getSellerId()) == null) { logger.info(orVo.getSellerId()+
	 * "___________________________________________________"); order =
	 * modelMapper.map(orderVo, Order.class);
	 * order.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE
	 * )); order.setStatus(ACTIVE); order.setOrderstatus(ORDERED);
	 * 
	 * 
	 * order.setSellerId(orVo.getSellerId()); OrderDetails orderdet = null; oList =
	 * new ArrayList<>();
	 * 
	 * int noOfItems = 0; double netTax = 0; long item=0; for (OrderDetailsVo fVo :
	 * orderVo.getOrderDetailsVo()) { if (orVo.getSellerId() == fVo.getSellerId()) {
	 * orderdet = modelMapper.map(fVo, OrderDetails.class);
	 * orderdet.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); orderdet.setStatus(ACTIVE);
	 * orderdet.setOfferId(fVo.getOfferId());
	 * orderdet.setOperatingOfficerId(orderVo.getCustomerId());
	 * orderdet.setDeliveryDate(0); orderdet.setProduct(null); totalAmt +=
	 * fVo.getItemNetPrice(); logger.info(totalAmt+
	 * "totalAmt________________________________________________"); noOfItems +=
	 * fVo.getItemQuantity(); logger.info(noOfItems+
	 * "noOfItems________________________________________________"); netTax +=
	 * fVo.getTax(); oList.add(orderdet); } } logger.info(order.getCustomerId()+
	 * "order.getCustomerId()-----------------------------");
	 * 
	 * if(orderVo.getPromocode().length() > 0) { Map<String, Object> promoMap
	 * =generalDao.getPromocodeDetails(0,0, 1, 0, 0, 0, 0, orderVo.getPromocode());
	 * // orderVo.setCodeId((long) promoMap.get(0)); List<PromocodeDetails>
	 * data=(List<PromocodeDetails>) promoMap.get("promoList");
	 * orderVo.setCodeId(data.get(0).getCodeId());
	 * orderVo.setPercentage(data.get(0).getPercentage());
	 * orderVo.setFlatOffer(data.get(0).getFlatOffer());
	 * orderVo.setValidFrom(data.get(0).getValidFrom());
	 * orderVo.setValidTo(data.get(0).getValidTo());
	 * orderVo.setMinimumAmount(data.get(0).getMinimumAmount());
	 * logger.info(data.get(0).getCodeId()+
	 * "___________________________________________"); //orderVo.setCodeId((long)
	 * promoMap.get("promoList")); }
	 * 
	 * if(orderVo.getMinimumAmount() <= totalAmt) {
	 * 
	 * long currentTime=TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
	 * logger.info(currentTime+
	 * "currentTime________________________________________________12");
	 * if(currentTime >= orderVo.getValidFrom() && currentTime <=
	 * orderVo.getValidTo() ) { logger.info(currentTime+
	 * "currentTime________________________________________________12");
	 * 
	 * if(orderVo.getFlatOffer() != 0) { double
	 * total=totalAmt-orderVo.getFlatOffer();
	 * logger.info(total+"total________________________________________________10");
	 * order.setNetPrice(total); transaction.setNetAmount(total); } else
	 * if(orderVo.getPercentage() !=0) { double
	 * total=totalAmt-(totalAmt*orderVo.getPercentage()/100);
	 * logger.info(total+"total________________________________________________11");
	 * order.setNetPrice(total); transaction.setNetAmount(total); }
	 * 
	 * }
	 * 
	 * 
	 * }else { order.setNetPrice(totalAmt); transaction.setNetAmount(totalAmt); }
	 * 
	 * item=orVo.getItemQuantity();
	 * 
	 * //order.setTransaction(null); order.setTax(netTax);
	 * order.setCod(orderVo.getCod()); order.setOnline(orderVo.getOnline());
	 * order.setPromocodeId(orderVo.getCodeId()); order.setNumberOfItems(noOfItems);
	 * orders.put(orVo.getSellerId(), order); orMap.put(orVo.getSellerId(), oList);
	 * 
	 * } }
	 * 
	 * 
	 * logger.info(order.getCompanyId()+"________________________________");
	 * transaction.setBankOrCash(TRAN_BANK);
	 * transaction.setTransactionMode(TRAN_MODE_DEBIT_SALE); transaction.setCmd(1);
	 * transaction.setOperatingOfficer(order.getOperatingOfficerId());
	 * transaction.setToWhoId(order.getCustomerId());
	 * transaction.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); transaction.setReciptNo("1");
	 * transaction.setNote("sale of item"); transaction.setNetAmount(totalAmt);
	 * transaction.setInOrOut(CASH_IN); transaction.setIsCleared(NOT_CLEAR_TRAN);
	 * transaction.setStatus(ACTIVE);
	 * transaction.setCompanyId(order.getCompanyId());
	 * transaction.setPairedId(order.getOrderId());
	 * logger.info(transaction.getPairedId()+
	 * "prairedId----------------------------------------------------------------09"
	 * ); tranMap.put("ORDER_TRAN", transaction); //-added online
	 * 
	 * 
	 * 
	 * if(order.getCod() == 0) {
	 * 
	 * transaction1.setBankOrCash(TRAN_BANK); transaction1.setCmd(1);
	 * transaction1.setCompanyId(0); transaction1.setInOrOut(CASH_IN);
	 * transaction1.setDiscount(0);
	 * transaction1.setTransactionMode(TRAN_MODE_DEBIT_ONLINE_PAY);
	 * transaction1.setNetAmount(totalAmt);
	 * transaction1.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); transaction1.setReciptNo("1");
	 * transaction1.setNote("ONLINE PAY ");
	 * transaction1.setIsCleared(NOT_CLEAR_TRAN); transaction1.setStatus(ACTIVE);
	 * onlineMap.put("PAYMENT_TRAN", transaction1);
	 * 
	 * }else {
	 * 
	 * 
	 * 
	 * 
	 * transaction1.setBankOrCash(TRAN_BANK); transaction1.setCmd(1);
	 * transaction1.setCompanyId(0); transaction1.setInOrOut(CASH_IN);
	 * transaction1.setDiscount(0);
	 * transaction1.setTransactionMode(TRAN_MODE_DEBIT_CASH_PAY);
	 * transaction1.setNetAmount(totalAmt);
	 * transaction1.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); transaction1.setReciptNo("1");
	 * transaction1.setNote("CASH PAY"); transaction1.setIsCleared(NOT_CLEAR_TRAN);
	 * transaction1.setStatus(ACTIVE); onlineMap.put("PAYMENT_TRAN", transaction1);
	 * 
	 * }
	 * 
	 * 
	 * //-----------------
	 * 
	 * 
	 * Map<String, Object> map = orderDao.addOrder(orMap, orders,tranMap,onlineMap);
	 * res = new BasicResponse(); res.errorCode = (int) map.get(ERROR_CODE);
	 * res.errorMessage = (String) map.get(ERROR_MESSAGE); res.errorMessageArabic =
	 * (String) map.get(ERROR_MESSAGE_ARABIC);
	 * 
	 * if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) { res.object =
	 * map.get("ServiceList");
	 * 
	 * }
	 * 
	 * return res; }
	 */

	@Override
	public BasicResponse saveOrUpdateTax(TaxVo taxVo) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = null;
		TaxManagement tax = new TaxManagement();
		tax.setOperatingOfficerId(1);
		tax.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		tax = modelMapper.map(taxVo, TaxManagement.class);
		tax.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
//		tax.setStatus(ACTIVE);
		tax.setOperatingOfficerId(1);
		map = orderDao.saveOrUpdateTax(tax);
		basicResponse = new BasicResponse();
		basicResponse.setObject(map);
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);

		return basicResponse;
	}

	@Override
	public BasicResponse getTax(long countryId, long cityId, long taxId, int rowPerPage, int currentIndex, int stat) {
		BasicResponse res = null;

		List<TaxVo> taxVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = orderDao.getTax(countryId, cityId, taxId, rowPerPage, currentIndex, stat);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			Map<Long, String[]> countryMap = inputService.getAllCountry(1);
			Map<Long, String[]> cityMap = inputService.getAllCities(0, 1);
			List<TaxManagement> taxList = (List<TaxManagement>) dataMap.get("TaxList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			TaxVo taxVo = null;
			for (TaxManagement tax : taxList) {
				taxVo = modelMapper.map(tax, TaxVo.class);
				/*
				 * taxVo.setCountryName(countryMap.get(tax.getCountryId()));
				 * taxVo.setCityName(cityMap.get(taxVo.getCity()));
				 * taxVo.setCountryName(countryMap.get(taxVo.getCountry()));
				 */
				if (countryMap != null && countryMap.size() >0) {
					logger.info(countryMap+"----------------------------------------- COUNTRY MAP VALUE------");
					String names[] = countryMap.get(taxVo.getCountryId());
					taxVo.setCountryName(names[0]);
					taxVo.setCountryNameAr(names[1]);
				}
				if (cityMap != null && cityMap.size() >0) {
					String names[] = cityMap.get(taxVo.getCityId());
					taxVo.setCityName(names[0]);
					taxVo.setCityNameAr(names[1]);
				}
				taxVos.add(taxVo);
			}
			// prVo.setSubcategoryName(subcategoryMap.get(prod.getSubCategoryId()));

			returnMap.put("TaxManagement", taxVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = taxVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public Map<Long, String> getAllCountryNames() {

		List<Country> serviceList = null;
		Map<Long, String> allserviceMap = null;
		serviceList = (List<Country>) inputDao.getAllCountry(0, 0);
		if (serviceList != null && !serviceList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (Country item : serviceList) {

				allserviceMap.put(item.getCountryId(), item.getCountryName());
			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@Override
	public BasicResponse getAllShippingDetails(long customerId, long shippingId, int rowPerPage, int currentIndex,
			int stat) {
		BasicResponse res = null;

		List<ShippingVo> shipVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = orderDao.getAllShippingDetails(customerId, shippingId, rowPerPage, currentIndex,
				stat);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			List<ShippingDetails> shipList = (List<ShippingDetails>) dataMap.get("ShippingList");
			Map<Long, String[]> countryMap = inputService.getAllCountry(1);
			Map<Long, String[]> cityMap = inputService.getAllCities(0, 1);
			long totalCounts = (long) dataMap.get("TotalCounts");
			ShippingVo shipVo = null;
			CustomerDetails cust = generalService.getCustomerFromUserId(customerId);
			logger.info(cust + "cust________________________________________________________");
			for (ShippingDetails ship : shipList) {
				shipVo = modelMapper.map(ship, ShippingVo.class);

				if (cust != null) {
					shipVo.setCustomerName(cust.getCustomerName());
					shipVo.setCustomerNameAr(cust.getCustomerNameAr());
					shipVo.setEmailId(cust.getEmailId());

				}

				shipVos.add(shipVo);
			}

			returnMap.put("ShippingDetails", shipVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = shipVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;

	}

	@Override
	public BasicResponse getCardDetails(long cardId, long customerId, int currentIndex, int rowPerPage) {
		BasicResponse res = null;

		List<CardDetailsVo> cardVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = orderDao.getCardDetails(cardId, customerId, currentIndex, rowPerPage);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			List<CardDetails> cdList = (List<CardDetails>) dataMap.get("CardList");
			/*
			 * Map<Long, String[]> countryMap=inputService.getAllCountry(1); Map<Long,
			 * String[]> cityMap=inputService.getAllCities(0,1);
			 */
			long totalCounts = (long) dataMap.get("TotalCounts");
			CardDetailsVo cd = null;
			for (CardDetails ship : cdList) {
				cd = modelMapper.map(ship, CardDetailsVo.class);

				cardVos.add(cd);
			}
			returnMap.put("CardDetails", cardVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = returnMap;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse saveOrUpdateCountry(CountryVo countryVo) {

		BasicResponse basicResponse = null;
		Map<String, Object> map = null;
		// byte[] backToBytes = Base64.decodeBase64(countryVo.getFlag());
		Country country = modelMapper.map(countryVo, Country.class);
		// country.setCountryFlag(backToBytes);
		map = orderDao.saveOrUpdateCountry(country);
		basicResponse = new BasicResponse();
		basicResponse.setObject(map);
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		return basicResponse;

	}

	@Override
	public BasicResponse saveOrUpdateCity(CityVo cityVo) {

		BasicResponse basicResponse = null;
		Map<String, Object> map = null;

		City city = new City();
		city = modelMapper.map(cityVo, City.class);

		map = orderDao.saveOrUpdateCity(city);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		return basicResponse;

	}

	@Override
	public BasicResponse deletetax(long taxId) {
		BasicResponse res = null;
		if (taxId <= 0) {
			res = new BasicResponse();
			res.errorCode = 1;
			res.errorMessage = "taxId is zero";
			return res;
		}
		Map<String, Object> map = orderDao.deleteTax(taxId);
		res = new BasicResponse();
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return res;
	}
	
	@Override
	 public BasicResponse changeStatusOfTransaction(long orderId)
	 {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = null;
		Map<String, Object> tranMap = new HashMap<String, Object>();

		Order o = new Order();
		try {
			Transactions transactions = new Transactions();

			map = orderDao.changeStatusOfTransaction( orderId);
		} catch (Exception e) {
			res.errorCode = 3;
			res.errorMessage = e.getMessage();
			return res;
		}
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		if (map.get(ERROR_MESSAGE_ARABIC) != null) {
			res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	 }

	@Override
	public BasicResponse changeStatusOfOrder(int status, long orderId, long userId, List<Long> deleteOrderListIds,long entryId) {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = null;
		Map<String, Object> tranMap = new HashMap<String, Object>();

		Order o = new Order();
		try {
			Transactions transactions = new Transactions();

			map = orderDao.changeStatusOfOrder(status, orderId, userId, deleteOrderListIds, transactions, entryId);
		} catch (Exception e) {
			res.errorCode = 3;
			res.errorMessage = e.getMessage();
			return res;
		}
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		if (map.get(ERROR_MESSAGE_ARABIC) != null) {
			res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		}
		return res;

	}

	@Override
	public BasicResponse addShippingDetails(ShippingVo shippingVo) {

		BasicResponse basicResponse = null;
		Map<String, Object> map = null;
		ShippingDetails shippingDetails = new ShippingDetails();
		shippingDetails = modelMapper.map(shippingVo, ShippingDetails.class);
		shippingDetails.setCustomerDetails(null);
		shippingDetails.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		shippingDetails.setStatus(ACTIVE);
		shippingDetails.setCityId(shippingVo.getCityId());

		shippingDetails.setCountryId(shippingVo.getCountryId());
		map = orderDao.addShippingDetails(shippingDetails);
		basicResponse = new BasicResponse();
		basicResponse.setObject(map);
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;

	}

	@Override
	public BasicResponse addCardDetails(CardDetailsVo shippingVo) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = null;
		CardDetails shippingDetails = new CardDetails();
		shippingDetails = modelMapper.map(shippingVo, CardDetails.class);
		shippingDetails.setCustomerDetails(null);
		shippingDetails.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		shippingDetails.setStatus(ACTIVE);
		map = orderDao.addCardDetails(shippingDetails);
		basicResponse = new BasicResponse();
		basicResponse.setObject(map);
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;

	}

	/*
	 * @Override public BasicResponse changeStatusOfOrder(int status, long orderId,
	 * long userId, List<Long> deleteOrderListIds) { Map<String, Object> map =
	 * orderDao.changeStatusOfOrder(status, orderId, userId, deleteOrderListIds);
	 * BasicResponse res = new BasicResponse(); res.errorCode = (int)
	 * map.get(ERROR_CODE); res.errorMessage = (String) map.get(ERROR_MESSAGE); if
	 * (map.get(ERROR_MESSAGE_ARABIC) != null) { res.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); } return res; }
	 */

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public BasicResponse saveAuctionDetails(AuctionDetailsVo
	 * acDetailsVo) { BasicResponse res=null; AuctionDetails
	 * acAuctionDetails=modelMapper.map(acDetailsVo, AuctionDetails.class);
	 * acAuctionDetails.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); Map<String,Object>
	 * map=orderDao.saveAuctionDetails(acAuctionDetails); res=new BasicResponse();
	 * res.errorCode=(int) map.get(ERROR_CODE); res.errorMessage=(String)
	 * map.get(ERROR_MESSAGE); res.errorMessageArabic=(String)
	 * map.get(ERROR_MESSAGE_ARABIC); return res; }
	 */

	/*
	 * @Override public BasicResponse saveOrUpdateCart(CartAndFavouritesVo
	 * cartAndFavouritesVo) {
	 * 
	 * BasicResponse basicResponse = null; Map<String, Object> map = null;
	 * CartAndFavourites cartAndFavourites = new CartAndFavourites();
	 * cartAndFavourites = modelMapper.map(cartAndFavouritesVo,
	 * CartAndFavourites.class);
	 * 
	 * cartAndFavourites.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); map = orderDao.saveOrUpdateCart(cartAndFavourites);
	 * basicResponse = new BasicResponse(); basicResponse.setObject(map);
	 * basicResponse.errorCode = (int) map.get(ERROR_CODE);
	 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE); return
	 * basicResponse;
	 * 
	 * }
	 */

	/*
	 * @Override public BasicResponse deleteAuctionDetails(long auctionId, long
	 * auctionDetailsId) { BasicResponse res=null; if(auctionId <=0 ||
	 * auctionDetailsId <= 0) { res=new BasicResponse(); res.errorCode=1;
	 * res.errorMessage="auctionid or auctionDetailsid is zero"; return res; }
	 * 
	 * Map<String, Object> map =orderDao.deleteAuction(auctionId,auctionDetailsId);
	 * res=new BasicResponse(); res.errorCode=(int) map.get(ERROR_CODE);
	 * res.errorMessage=(String) map.get(ERROR_MESSAGE);
	 * res.errorMessageArabic=(String) map.get(ERROR_MESSAGE_ARABIC); return res; }
	 */

	/*
	 * @Override public BasicResponse changeStatusOfAuction(int status, long
	 * auctionId, long userId) { Map<String, Object> map =
	 * orderDao.changeStatusOfAuction(status, auctionId, userId); BasicResponse res
	 * = new BasicResponse(); res.errorCode = (int) map.get(ERROR_CODE);
	 * res.errorMessage = (String) map.get(ERROR_MESSAGE); if
	 * (map.get(ERROR_MESSAGE_ARABIC) != null) { res.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); } return res; }
	 */

	/*
	 * @Override public BasicResponse saveOrUpdateAuction(AuctionVo auctionvo) {
	 * 
	 * BasicResponse res = null; if (auctionvo == null) { res = new BasicResponse();
	 * res.errorCode = 9; res.errorMessage = MESSAGE_NULL_VALUE;
	 * res.errorMessageArabic = MESSAGE_NULL_VALUE_AR; return res; } Auction auction
	 * = modelMapper.map(auctionvo,Auction.class);
	 * auction.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); if (auctionvo.getAuctionId() <= 0) {
	 * auction.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); auction.setAuctionStatus(AUC_PRESENTED); } else {
	 * auction.setAuctionId(auctionvo.getAuctionId()); } auction.setStatus(ACTIVE);
	 * List<FeaturesAndPhotosAuction> fList = new ArrayList<>();
	 * FeaturesAndPhotosAuction fProduct = null; for (FeatureVo fVo :
	 * auctionvo.getFeatureAndPhotos()) { fProduct =
	 * modelMapper.map(fVo,FeaturesAndPhotosAuction.class);
	 * fProduct.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); fProduct.setStatus(ACTIVE); fList.add(fProduct); }
	 * Map<String, Object> map = orderDao.saveOrUpdateAuction(auction, fList); res =
	 * new BasicResponse(); res.errorCode = (int) map.get(ERROR_CODE);
	 * res.errorMessage = (String) map.get(ERROR_MESSAGE); res.errorMessageArabic =
	 * (String) map.get(ERROR_MESSAGE_ARABIC); if ((int) map.get(ERROR_CODE) ==
	 * ERROR_CODE_NO_ERROR) { res.object =map.get("ServiceList"); }
	 * 
	 * return res;
	 * 
	 * }
	 */
	@Override
	public BasicResponse addReturnProduct(List<ReturnItemsVo> returnItemsVos)

	{

		BasicResponse basicResponse = new BasicResponse();
		Map<String, Object> map = null;
		ReturnItems reItem = null;
		List<ReturnItems> returnItems = new ArrayList<>();
		try {
			for (ReturnItemsVo or : returnItemsVos) {
				reItem = modelMapper.map(or, ReturnItems.class);
				reItem.setStatus(RETURN_REQUESTED);
				reItem.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
				reItem.setTransactionId(0);
				returnItems.add(reItem);
			}

			map = orderDao.addReturnProduct(returnItems);
			basicResponse = new BasicResponse();
			basicResponse.errorCode = (int) map.get(ERROR_CODE);
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);

		}

		catch (Exception e) {
			e.printStackTrace();
		}

		return basicResponse;

	}

	/*
	 * @Override public BasicResponse getAllReturnProducts(long returnItemId,int
	 * rowPerPage,int currentIndex,long storeId) {Order o=new Order(); BasicResponse
	 * basicResponse = null; Order order=new Order(); Map<String, Object> map =
	 * orderDao.getAllReturnProducts(returnItemId,rowPerPage,currentIndex,storeId);
	 * //long totalCounts = (long) map.get("TotalCounts"); if ((int)
	 * map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) { basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = 2; basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); //basicResponse.totalRecords = totalCounts;
	 * return basicResponse; } basicResponse = new BasicResponse();
	 * basicResponse.errorCode = ERROR_CODE_NO_ERROR; basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); //basicResponse.totalRecords = totalCounts;
	 * 
	 * @SuppressWarnings("unchecked") List<Object[]> returnItemList =
	 * (List<Object[]>) map.get("ReturnItemList"); List<ReturnItemsVo>
	 * returnItemVoList = new ArrayList<>(); ReturnItemsVo returnItemVo = null; for
	 * (Object[] cat : returnItemList) { returnItemVo =
	 * modelMapper.map(cat[0],ReturnItemsVo.class);
	 * returnItemVo.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); returnItemVoList.add(returnItemVo); }
	 * 
	 * 
	 * if (returnItemId > 0) { basicResponse.object = returnItemVoList.get(0); }
	 * else { basicResponse.object = returnItemVoList; } return basicResponse;
	 * 
	 * }
	 */

	@Override
	public BasicResponse getShippingByOrder(long orderId) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = orderDao.getShippingByOrder(orderId);
		// long totalCounts = (long) map.get("TotalCounts");
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			// basicResponse.totalRecords = totalCounts;
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		// basicResponse.totalRecords = totalCounts;
		@SuppressWarnings("unchecked")
		List<ShippingDetails> orderList = (List<ShippingDetails>) map.get("OrderList");
		List<ShippingVo> orderVoList = new ArrayList<>();
		ShippingVo orderVo = null;
		for (ShippingDetails cat : orderList) {
			orderVo = modelMapper.map(cat, ShippingVo.class);
			// categoryVo.setCreatedDateString(TimeUtils.instance().getDateStrFromMilSec(cat.getCreatedDate()));
			// categoryVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(cat.getLastUpdatedTime()));
			// String base64String = Base64.encodeBase64String(cat.getCountryFlag());
			// countryVo.setFlag(base64String);
			// logger.info(countryVo.getFlag());
			orderVoList.add(orderVo);
		}
		if (orderId > 0) {
			basicResponse.object = orderVoList.get(0);
		} else {
			basicResponse.object = orderVoList;
		}
		return basicResponse;
	}

	@Override
	public BasicResponse getAllReturnProducts(long returnItemId, long orderListId, int rowPerPage, int currentIndex,
			long storeId, int stat) {
		BasicResponse basicResponse = null;
		long totalCounts = 0;
		Map<String, Object> map = orderDao.getAllReturnProducts(returnItemId, orderListId, rowPerPage, currentIndex,
				storeId, stat);
		totalCounts = (long) map.get("TotalCounts");
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			basicResponse.totalRecords = totalCounts;
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		basicResponse.totalRecords = totalCounts;

		List<ReturnItems> returnItemList = (List<ReturnItems>) map.get("ReturnItemList");
		List<ReturnItemsVo> returnItemVoList = new ArrayList<>();
		ReturnItemsVo returnItemVo = null;
		for (ReturnItems cat : returnItemList) {
			returnItemVo = modelMapper.map(cat, ReturnItemsVo.class);
			returnItemVo.setStoreId(cat.getOrderDetails().getProduct().getSellerId());
			returnItemVo.setCustomerId(cat.getOperatingOfficerId());
			// returnItemVo.setStoreName(cat.getOrderDetails().getProduct().getShop().getStoreName());
			returnItemVo.setOrderId(cat.getOrderDetails().getOrderId());// added
			CustomerDetails customer = generalService.getCustomerFromUserId(cat.getOperatingOfficerId());
			logger.info(cat.getOperatingOfficerId() + "cat.getOperatingOfficerId()");
			logger.info(customer + "customer-----------------------------------");
			if (customer != null) {
				returnItemVo.customerName = customer.getCustomerName();
				returnItemVo.phoneNo = customer.getPhoneNo();
				returnItemVo.custEmail = customer.getEmailId();
			}
			returnItemVoList.add(returnItemVo);
		}

		if (returnItemId > 0 || orderListId > 0) {
			basicResponse.object = returnItemVoList.get(0);
		} else {
			basicResponse.object = returnItemVoList;
		}
		return basicResponse;

	}

}
