package com.innoventiaProject.discount.service.input;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.innoventiaProject.dao.input.InputDao;
import com.innoventiaProject.dao.order.OrderDao;
import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.discount.model.input.AddVo;
import com.innoventiaProject.discount.model.input.AttributeShowVo;
import com.innoventiaProject.discount.model.input.AttributeVo;
import com.innoventiaProject.discount.model.input.CategoryVo;
import com.innoventiaProject.discount.model.input.CityVo;
import com.innoventiaProject.discount.model.input.CmsVo;
import com.innoventiaProject.discount.model.input.ConnectedProductsVo;
import com.innoventiaProject.discount.model.input.CountryVo;
import com.innoventiaProject.discount.model.input.FeatureCompareVo;
import com.innoventiaProject.discount.model.input.FeatureVo;
import com.innoventiaProject.discount.model.input.GetproductAttributeVo;
import com.innoventiaProject.discount.model.input.ManufactureVo;
import com.innoventiaProject.discount.model.input.OfferDetailsVo;
import com.innoventiaProject.discount.model.input.OfferVo;
import com.innoventiaProject.discount.model.input.ProductAttributeVo;
import com.innoventiaProject.discount.model.input.ProductVo;
import com.innoventiaProject.discount.model.input.ReviewVo;
import com.innoventiaProject.discount.model.input.SellerVo;
import com.innoventiaProject.discount.model.input.SubCategoryVo;
import com.innoventiaProject.discount.model.order.TaxVo;
import com.innoventiaProject.discount.service.GeneralService;
import com.innoventiaProject.discount.utils.TimeUtils;
import com.innoventiaProject.entity.input.Advertisement;
import com.innoventiaProject.entity.input.AttributeDetails;
import com.innoventiaProject.entity.input.Attributes;
import com.innoventiaProject.entity.input.Category;
import com.innoventiaProject.entity.input.City;
import com.innoventiaProject.entity.input.CmsMangement;
import com.innoventiaProject.entity.input.ConnectedProducts;
import com.innoventiaProject.entity.input.Country;
import com.innoventiaProject.entity.input.FeaturesAndPhotosProduct;
import com.innoventiaProject.entity.input.Manufacture;
import com.innoventiaProject.entity.input.Offer;
import com.innoventiaProject.entity.input.OfferDetails;
import com.innoventiaProject.entity.input.Product;
import com.innoventiaProject.entity.input.ProductAttributes;
import com.innoventiaProject.entity.input.Review;
import com.innoventiaProject.entity.input.SellerDetails;
import com.innoventiaProject.entity.input.SubCategory;
import com.innoventiaProject.entity.order.Order;
import com.innoventiaProject.entity.order.TaxManagement;
import com.innoventiaProject.entity.user.CustomerDetails;

@Transactional
@Service("inputService")
public class InputServiceImpl implements InputService, Constants {
	private static Logger logger = Logger.getLogger(InputServiceImpl.class);

	@Autowired
	private OrderDao orderDao;

	@Autowired
	private InputDao inputDao;

	@Autowired
	private GeneralService generalService;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public BasicResponse saveOrUpdateAdd(AddVo addVo) {
		BasicResponse basicResponse = new BasicResponse();
		Advertisement advertisement = modelMapper.map(addVo, Advertisement.class);
		advertisement.setModeId(addVo.getModeId());
		logger.info(addVo.getModeId() + "mode____________________________________________________");
		if (addVo.getAddId() <= 0) {
			advertisement.setAddStatus(ADD_STATED);
		}
		advertisement.setHeight(addVo.getHeight());
		advertisement.setStatus(ACTIVE);
		advertisement.setRank(addVo.getRank());
		advertisement.setArabicOrEnglish(addVo.getArabicOrEnglish());
		advertisement.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		Map<String, Object> map = inputDao.saveAddvertise(advertisement);
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllAdds(int addStatus, long addId, long storeUserId, long modeId,long arabicOrEnglish) {
		BasicResponse basicResponse = new BasicResponse();
		Map<String, Object> map = inputDao.getAllAdds(addStatus, storeUserId, addId, modeId, arabicOrEnglish);
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			List<Advertisement> aList = (List<Advertisement>) map.get(DATA);
			List<AddVo> addVos = new ArrayList<>();
			AddVo adVo = null;
			for (Advertisement add : aList) {
				adVo = modelMapper.map(add, AddVo.class);
				addVos.add(adVo);
			}
			if (addId > 0) {
				basicResponse.object = addVos.get(0);
			} else {
				basicResponse.object = addVos;
			}
		}
		return basicResponse;
	}

	@Override
	public BasicResponse changeReturnStatus(long returnItemId, int status) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = inputDao.changeReturnStatus(returnItemId, status);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;

	}

	@Override
	public BasicResponse saveProduct(ProductVo serviceVo) {
		Map<Long, String[]> attributeMap = getAllAttribute(1);
		logger.info(attributeMap + "attributeMap______________");
		BasicResponse res = null;
		if (serviceVo == null) {
			res = new BasicResponse();
			res.errorCode = 9;
			res.errorMessage = MESSAGE_NULL_VALUE;
			res.errorMessageArabic = MESSAGE_NULL_VALUE_AR;
			return res;
		}
		/* AttributeDetails atributeDetails=new AttributeDetails(); */
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Product product = modelMapper.map(serviceVo, Product.class);
		product.setSubCategoryId(serviceVo.getSubCategoryId());
		product.setSellerId(serviceVo.getSellerId());
		product.setShippingCharge(serviceVo.getShippingCharge());
		product.setFeatured(serviceVo.getFeatured());
		product.setRank(0);
		product.setCommission(serviceVo.getCommission());
		product.setMaximumPromoOffer(serviceVo.getMaximumPromoOffer());
		product.setPurchasePrice(serviceVo.getPurchasePrice());
		logger.info(product.getSellerId()
				+ "seller id_____________________________________________________________________");
		logger.info("4********************************************");
		product.setManufacturerId(serviceVo.getManufacturerId());
		// product.setConnectedProducts(null);
		product.setFeatureAndPhotos(null);

		product.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		if (serviceVo.getProductId() <= 0) {
			product.setShares(0);
			product.setNoOfReviews(0);
			product.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
			product.setNumberOfItemsRemaining(serviceVo.getProductQuantity());
		} else {
			product.setProductId(serviceVo.getProductId());
		}

		product.setStatus(serviceVo.getStatus());
		List<FeaturesAndPhotosProduct> fList = new ArrayList<>();
		FeaturesAndPhotosProduct fProduct = null;
		for (FeatureVo fVo : serviceVo.getFeatureOrPhotos()) {
			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			fProduct = modelMapper.map(fVo, FeaturesAndPhotosProduct.class);

//			fProduct.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
			fProduct.setStatus(ACTIVE);
			logger.info(fProduct.getProductId() + "++++++++++++++++++++++++++++++++");
			fList.add(fProduct);
		}
		Object[] obj = new Object[serviceVo.getProductAttributeVos().size()];
		logger.info(obj.length + "lenght***************************************************");
		List<ProductAttributes> paList = new ArrayList<>();
		List<AttributeDetails> paDeailsList = new ArrayList<>();
		ProductAttributes prodAtt = null;
		int i = 0;
		for (ProductAttributeVo prVo : serviceVo.getProductAttributeVos()) {
			AttributeDetails atributeDetails = new AttributeDetails();
			obj[i++] = prVo.getAttributeId();

			modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			prodAtt = modelMapper.map(prVo, ProductAttributes.class);
//			prodAtt.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
			prodAtt.setStatus(ACTIVE);
			paList.add(prodAtt);
			logger.info(prVo.getAttributeId()
					+ "prVo.getAttributeId()____________________________________prVo.getAttributeId()_____________________prVo.getAttributeId()______");
			logger.info(attributeMap.get(prVo.getAttributeId()) + "attributeMap.get(prVo.getAttributeId())");
			String[] names = attributeMap.get(prVo.getAttributeId());
			logger.info(names + "names____________________________________names_____________________names______");
			/*
			 * if(names!= null && names.length != 0) {
			 * logger.info(names+"4uuuuuuuuuuuuuuuuuuuuuuuuuu");
			 * atributeDetails.setAttributeName(names[0]);
			 * atributeDetails.setAttributeNameAr(names[1]);
			 * 
			 * 
			 * }
			 */

			atributeDetails.setAttributeId(prVo.getAttributeId());
			atributeDetails.setValue(prVo.getValue());
			atributeDetails.setValueAr(prVo.getValueAr());
			paDeailsList.add(atributeDetails);

		}
		logger.info(paDeailsList + "paDeailsList___________________________paDeailsList_________________");
		logger.info(paDeailsList.size() + "paDeailsList___________________________paDeailsList_________________");
		List<ConnectedProducts> cpList = null;
		if (serviceVo.getConnectedProductsVo() != null) {
			cpList = new ArrayList<>();
			ConnectedProducts cp = null;
			for (ConnectedProductsVo cpVo : serviceVo.getConnectedProductsVo()) {
				modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
				cp = modelMapper.map(cpVo, ConnectedProducts.class);

				cp.setModeId(fProduct.getModeId());
				cp.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
				cpList.add(cp);
			}
		} // ------------------added

		List<OfferDetails> offerList = null;
		if (serviceVo.getOfferDetailsVo() != null) {
			offerList = new ArrayList<>();
			OfferDetails cp = null;
			for (OfferDetailsVo ofVo : serviceVo.getOfferDetailsVo()) {
				modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
				cp = modelMapper.map(ofVo, OfferDetails.class);

				cp.setProductId(product.getProductId());
				cp.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
				offerList.add(cp);
			}
		}
		// added------------------------
		Map<String, Object> map = inputDao.saveProduct(product, fList, cpList, paList, obj, offerList, paDeailsList);
		res = new BasicResponse();
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			res.object = map.get("ServiceList");

		}

		return res;
	}

	@Override
	public Map<Long, String> getAllCountryAr(int status) {
		List<Country> placeList = null;
		Map<Long, String> allplaceMap = null;
		Map<String, Object> map = inputDao.getAllCountry(0, status);
		placeList = (List<Country>) map.get("CountryList");
		if (placeList != null && !placeList.isEmpty()) {
			allplaceMap = new HashMap<>();
			for (Country c : placeList) {
				// allplaceMap.put(c.getCountryId(), c.getCountryName());
				allplaceMap.put(c.getCountryId(), c.getCountryNameAr());
			}

		}
		return allplaceMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, String> getAllCitiesAr(long countryId, int status) {
		List<City> placeList = null;
		Map<Long, String> allplaceMap = null;
		Map<String, Object> map = inputDao.getAllCity(countryId, 0, "", 0, 0, status);
		placeList = (List<City>) map.get("CityList");
		if (placeList != null && !placeList.isEmpty()) {
			allplaceMap = new HashMap<>();
			for (City c : placeList) {
				// allplaceMap.put(c.getCityId(), c.getCityName());
				allplaceMap.put(c.getCityId(), c.getCityNameAr());
			}

		}
		return allplaceMap;
	}

	@Override
	public Map<Long, String> getAllCountry1(int status) {
		List<Country> placeList = null;
		Map<Long, String> allplaceMap = null;
		Map<String, Object> map = inputDao.getAllCountry(0, status);
		placeList = (List<Country>) map.get("CountryList");
		if (placeList != null && !placeList.isEmpty()) {
			allplaceMap = new HashMap<>();
			for (Country c : placeList) {
				allplaceMap.put(c.getCountryId(), c.getCountryName());
				// allplaceMap.put(c.getCountryId(), c.getCountryNameAr());
			}

		}
		return allplaceMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, String> getAllCities1(long countryId, int status) {
		List<City> placeList = null;
		Map<Long, String> allplaceMap = null;
		Map<String, Object> map = inputDao.getAllCity(countryId, 0, "", 0, 0, status);
		placeList = (List<City>) map.get("CityList");
		if (placeList != null && !placeList.isEmpty()) {
			allplaceMap = new HashMap<>();
			for (City c : placeList) {
				allplaceMap.put(c.getCityId(), c.getCityName());
				// allplaceMap.put(c.getCityId(), c.getCityNameAr());
			}

		}
		return allplaceMap;
	}

	@Override
	public BasicResponse saveOrUpdateCountry(CountryVo countryVo) {

		BasicResponse basicResponse = null;
		Map<String, Object> map = null;
		Country country = new Country();
		country = modelMapper.map(countryVo, Country.class);
		map = inputDao.saveOrUpdateCountry(country);
		basicResponse = new BasicResponse();
		basicResponse.setObject(map);
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);

		return basicResponse;

	}

	@Override
	public BasicResponse saveOrUpdateCity(CityVo cityVo) {

		BasicResponse basicResponse = null;
		Map<String, Object> map = null;
		City city = new City();
		city = modelMapper.map(cityVo, City.class);
		map = inputDao.saveOrUpdateCity(city);
		basicResponse = new BasicResponse();
		basicResponse.setObject(map);
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;

	}

	@Override
	public BasicResponse getAllDetails(long userId, long id, int prodOrShop) {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = inputDao.getAllDetails(userId, id, prodOrShop);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			res.object = map;
		}
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return res;
	}

	public BasicResponse updateStock(long productId, long productQuantity) {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = inputDao.updateStock(productId, productQuantity);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			res.object = map;
		}
		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return res;
	}

	@Override
	public Map<Long, String> getAllSubCategoryNames() {

		List<SubCategory> serviceList = null;
		Map<Long, String> allserviceMap = null;
		serviceList = inputDao.getAllSubCategoryList(0, 0, 0);
		if (serviceList != null && !serviceList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (SubCategory item : serviceList) {

				allserviceMap.put(item.getSubCategoryId(), item.getSubCategoryName());
			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@Override
	public Map<Long, String> getAllSubCategoryNamesAr() {

		List<SubCategory> serviceList = null;
		Map<Long, String> allserviceMap = null;
		serviceList = inputDao.getAllSubCategoryList(0, 0, 0);
		if (serviceList != null && !serviceList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (SubCategory item : serviceList) {
				// logger.info(item.getSubCategoryNameAr()+"arabic_____________1__________________");
				allserviceMap.put(item.getSubCategoryId(), item.getSubCategoryNameAr());
			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@Override
	public Map<Long, String> getAllOffers() {

		List<Offer> serviceList = null;
		Map<Long, String> allserviceMap = null;
		serviceList = inputDao.getAllOfferId(0);
		if (serviceList != null && !serviceList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (Offer item : serviceList) {

				allserviceMap.put(item.getOfferId(), item.getOfferName());
			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	public Map<Long, Long> getAllOffers1() {
		List<Offer> serviceList = null;
		Map<Long, Long> allserviceMap = null;
		serviceList = inputDao.getAllOfferId(0);
		if (serviceList != null && !serviceList.isEmpty()) {
			allserviceMap = new HashMap<Long, Long>();
			for (Offer item : serviceList) {

				allserviceMap.put(item.getOfferId(), item.getOfferPercentage());
			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@Override
	public Map<Long, String> getAllOffersAr() {

		List<Offer> serviceList = null;
		Map<Long, String> allserviceMap = null;
		serviceList = inputDao.getAllOfferId(0);
		if (serviceList != null && !serviceList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (Offer item : serviceList) {

				allserviceMap.put(item.getOfferId(), item.getOfferNameAr());
			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@Override
	public Map<Long, String> getallManufactures() {
		List<Manufacture> serviceList = null;
		Map<Long, String> allserviceMap = null;
		serviceList = (List<Manufacture>) inputDao.getallManufactureId(0);
		if (serviceList != null && !serviceList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (Manufacture item : serviceList) {

				allserviceMap.put(item.getManufacturerId(), item.getManufacturerName());
			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@Override
	public Map<Long, String> getallManufacturesAr() {
		List<Manufacture> serviceList = null;
		Map<Long, String> allserviceMap = null;
		serviceList = (List<Manufacture>) inputDao.getallManufactureId(0);
		if (serviceList != null && !serviceList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (Manufacture item : serviceList) {

				allserviceMap.put(item.getManufacturerId(), item.getManufacturerNameAr());
			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	/*
	 * @Override public Map<Long, String> getAllMainCategoryNames() {
	 * 
	 * List<MainCategory> serviceList = null; Map<Long, String> allserviceMap =
	 * null; serviceList = inputDao.getAllMainCategoryList(); if (serviceList !=
	 * null && !serviceList.isEmpty()) { allserviceMap = new HashMap<Long,
	 * String>(); for (MainCategory item : serviceList) {
	 * 
	 * allserviceMap.put(item.getMainCategoryId(), item.getMainCategoryName()); }
	 * return allserviceMap; } else { return null; } }
	 */

	@Override
	public Map<Long, String> getAllCategoryNames() {

		List<Category> categoryList = null;
		Map<Long, String> allserviceMap = null;
		categoryList = inputDao.getAllCategoryList(0);
		if (categoryList != null && !categoryList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (Category item : categoryList) {
				allserviceMap.put(item.getCategoryId(), item.getCategoryName());

			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@Override
	public Map<Long, String> getAllCustomerName() {

		List<CustomerDetails> categoryList = null;
		Map<Long, String> allserviceMap = null;
		categoryList = inputDao.getAllCustomerName(0);
		if (categoryList != null && !categoryList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (CustomerDetails item : categoryList) {
				allserviceMap.put(item.getUserId(), item.getCustomerName());

			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@Override
	public Map<Long, String> getAllCustomerNameAr() {
		List<CustomerDetails> categoryList = null;
		Map<Long, String> allserviceMap = null;
		categoryList = inputDao.getAllCustomerName(0);
		if (categoryList != null && !categoryList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (CustomerDetails item : categoryList) {
				allserviceMap.put(item.getUserId(), item.getCustomerNameAr());

			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	@Override
	public Map<Long, String> getAllCategoryNamesAr() {

		List<Category> categoryList = null;
		Map<Long, String> allserviceMap = null;
		categoryList = inputDao.getAllCategoryList(0);
		if (categoryList != null && !categoryList.isEmpty()) {
			allserviceMap = new HashMap<Long, String>();
			for (Category item : categoryList) {
				allserviceMap.put(item.getCategoryId(), item.getCategoryNameAr());

			}
			return allserviceMap;
		} else {
			return null;
		}
	}

	/*
	 * @Override public BasicResponse saveMainCategory(CategoryVo categoryVo) {
	 * BasicResponse basicResponse = null; if (categoryVo == null) { basicResponse =
	 * new BasicResponse(); basicResponse.errorCode = 4; basicResponse.errorMessage
	 * = MESSAGE_NULL_VALUE; return basicResponse; } MainCategory category = new
	 * MainCategory(); category = modelMapper.map(categoryVo,MainCategory.class);
	 * 
	 * //category.setMainCategoryName(categoryVo.mainCategoryName); if
	 * (category.getMainCategoryId() <= 0)
	 * category.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE)
	 * ); category.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); //if (categoryVo.discription != null) {
	 * //category.setDescription(categoryVo.discription); //} else {
	 * //category.setDescription(""); //} //category.setUserId(categoryVo.userId);
	 * //if (categoryVo.maincategoryId <= 0) {
	 * //category.setCreatedDate(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); //} else {
	 * //category.setMainCategoryId(categoryVo.maincategoryId); //}
	 * //category.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); //category.setStatus(categoryVo.status); Map<String,
	 * Object> map = inputDao.saveMainCategory(category); if ((int)
	 * map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) { basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = 2; basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); return basicResponse; } basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = ERROR_CODE_NO_ERROR;
	 * basicResponse.object = map.get("CategoryList"); basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); return basicResponse; }
	 */

	@Override
	public BasicResponse saveCategory(CategoryVo categoryVo) {
		BasicResponse basicResponse = null;
		if (categoryVo == null) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 4;
			basicResponse.errorMessage = MESSAGE_NULL_VALUE;
			basicResponse.errorMessageArabic = MESSAGE_NULL_VALUE_AR;
			return basicResponse;
		}
		Category category = modelMapper.map(categoryVo, Category.class);
		if (categoryVo.getCategoryId() <= 0) {
			//category.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		} else {
			category.setCategoryId(categoryVo.getCategoryId());
		}
		category.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		// category.setStatus(categoryVo.status);
		Map<String, Object> map = inputDao.saveCategory(category);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = (int) map.get(ERROR_CODE);
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.object = map.get("CategoryList");
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@Override
	public BasicResponse saveCms(CmsVo CMSvo) {
		BasicResponse basicResponse = null;
		if (CMSvo == null) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 4;
			basicResponse.errorMessage = MESSAGE_NULL_VALUE;
			basicResponse.errorMessageArabic = MESSAGE_NULL_VALUE_AR;
			return basicResponse;
		}
		CmsMangement category = modelMapper.map(CMSvo, CmsMangement.class);

		Map<String, Object> map = inputDao.saveCms(category);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = (int) map.get(ERROR_CODE);
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.object = map.get("CMS");
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@Override
	public BasicResponse svaeManufacture(ManufactureVo manufactureVo, int r)

	{
		BasicResponse basicResponse = null;
		if (manufactureVo == null) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 4;
			basicResponse.errorMessage = MESSAGE_NULL_VALUE;
			basicResponse.errorMessageArabic = MESSAGE_NULL_VALUE_AR;
			return basicResponse;
		}
		Manufacture manufacture = modelMapper.map(manufactureVo, Manufacture.class);
		manufacture.setBusinessType(manufactureVo.getBusinessType());
		if (manufactureVo.getManufacturerId() <= 0) {
			manufacture.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		} else {
			manufacture.setManufacturerId(manufactureVo.getManufacturerId());
		}
		manufacture.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		manufacture.setOperatingOfficer(r);
		logger.info(r + "__________________________________________________");
		// category.setStatus(categoryVo.status);
		Map<String, Object> map = inputDao.saveManufacture(manufacture);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = (int) map.get(ERROR_CODE);
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.object = map.get("CategoryList");
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@Override
	public BasicResponse updateCategory(CategoryVo categoryVo) {
		BasicResponse basicResponse = null;
		if (categoryVo == null) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 4;
			basicResponse.errorMessage = MESSAGE_NULL_VALUE;
			basicResponse.errorMessageArabic = MESSAGE_NULL_VALUE_AR;
			return basicResponse;
		}
		Category category = new Category();

		category = modelMapper.map(category, Category.class);
		// category.setCategoryId(categoryVo.categoryId);
		// category.setCategoryName(categoryVo.categoryName);
		// category.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		// category.setDescription(categoryVo.discription);
		//category.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		category.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		// category.setStatus(categoryVo.status);
		Map<String, Object> map = inputDao.saveCategory(category);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.object = map.get("CategoryList");
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	/*
	 * @Override public BasicResponse saveShop(StoreVo shopVo) { BasicResponse
	 * basicResponse = null;
	 * 
	 * Store shop = new Store(); shop = modelMapper.map(shopVo,Store.class);
	 * 
	 * 
	 * if (shopVo.getStoreId() <= 0) { shop.setShares(0);
	 * 
	 * shop.setNoOfReviews(0);
	 * shop.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE)); }
	 * else { //shop.setShopId(shopVo.shopId); }
	 * shop.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE)
	 * ); shop.setStatus(ACTIVE); shop.setLatitude(shopVo.latitude);
	 * shop.setLongitude(shopVo.longitude); shop.setCity(shopVo.city);
	 * shop.setDescription(shopVo.description); shop.setEmailId(shopVo.emailId);
	 * shop.setPhoneNumber(shopVo.phoneNo); shop.setPriceStarts(shopVo.priceStarts);
	 * shop.setStoreName(shopVo.storeName); shop.setMainCategoryId(shopVo.shopType);
	 * shop.setUserId(shopVo.userId); shop.setWebAddress(shopVo.webAddress);
	 * if(shopVo.getWorkingHours() == null) { shop.setWorkingHours("NA"); }
	 * List<FeaturesAndPhotosShop> fList = new ArrayList<>(); FeaturesAndPhotosShop
	 * fProduct = null;
	 * 
	 * 
	 * for (FeatureVo fVo : shopVo.getFeatureVo()) { fProduct =
	 * modelMapper.map(fVo,FeaturesAndPhotosShop.class); //fProduct = new
	 * FeaturesAndPhotosShop(); //fProduct.setData(fVo.data);
	 * fProduct.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); //fProduct.setModeId(fVo.modeType);
	 * fProduct.setStatus(ACTIVE); //if (fVo.extraData != null) {
	 * //fProduct.setExtraData(fVo.extraData); //}
	 * //fProduct.setExtraDataNum(fVo.extraDataNum);
	 * fProduct.setUserId(shopVo.getUserId());
	 * fProduct.setShopId(shopVo.getStoreId()); fList.add(fProduct); } LoginDetails
	 * login=new LoginDetails(); login.setCountry(shopVo.getCountryId());
	 * login.setCity(shopVo.getCityId()); login.setEmail(shopVo.getEmailId());
	 * login.setEmailVerified(VERIFIED_EMAIL);
	 * login.setLast_updated_time(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); login.setPassword(PREFIX_OWNER_ACTIVATION);
	 * login.setPhoneNo(shopVo.getPhoneNumber()); login.setRole(ROLL_ID_SHOP);
	 * login.setStatus(ACTIVE);
	 * 
	 * Map<String, Object> map = inputDao.saveShop(shop,fList); if ((int)
	 * map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) { basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = 2; basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); return basicResponse; } basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = ERROR_CODE_NO_ERROR;
	 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
	 * basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
	 * basicResponse.object=PREFIX_OWNER_ACTIVATION; return basicResponse; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllCategory(long categoryId, long mainCategoryId, int status, long companyId) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = inputDao.getAllCategories(categoryId, 0, status, companyId);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		List<Category> cateList = (List<Category>) map.get("CategoryList");
		List<CategoryVo> categoryVoList = new ArrayList<>();
		CategoryVo categoryVo = null;
		for (Category cat : cateList) {
			categoryVo = modelMapper.map(cat, CategoryVo.class);
			//categoryVo.setCreatedDateString(TimeUtils.instance().getDateStrFromMilSec(cat.getCreatedDate()));
			categoryVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(cat.getLastUpdatedTime()));
			/*
			 * if (cat.getSubCategorys().size() > 0) { categoryVo.setIsSub(1); } else {
			 * categoryVo.setIsSub(0); }
			 */
			categoryVo.setSubCategoryVos(new ArrayList<>());
			SubCategoryVo subCategoryVo = null;
			/*
			 * for (SubCategory sub : cat.getSubCategorys()) { subCategoryVo =
			 * modelMapper.map(sub, SubCategoryVo.class);
			 * categoryVo.getSubCategoryVos().add(subCategoryVo); }
			 */

			/*
			 * ProductVo prVo = null; for (Product pr : cat.g) { subCategoryVo =
			 * modelMapper.map(sub, SubCategoryVo.class);
			 * categoryVo.getSubCategoryVos().add(subCategoryVo); }
			 */
			categoryVoList.add(categoryVo);
		}

		if (categoryId > 0) {
			basicResponse.object = categoryVoList.get(0);
		} else {
			basicResponse.object = categoryVoList;
		}
		return basicResponse;
	}

	/*
	 * @Override public BasicResponse getAllCountry(long countryId,int status) {
	 * BasicResponse basicResponse = null; Map<String, Object> map =
	 * inputDao.getAllCountry(countryId,status); long totalCounts = (long)
	 * map.get("TotalCounts"); if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR)
	 * { basicResponse = new BasicResponse(); basicResponse.errorCode = 2;
	 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
	 * basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
	 * basicResponse.totalRecords = totalCounts; return basicResponse; }
	 * basicResponse = new BasicResponse(); basicResponse.errorCode =
	 * ERROR_CODE_NO_ERROR; basicResponse.errorMessage = (String)
	 * map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); basicResponse.totalRecords = totalCounts;
	 * 
	 * @SuppressWarnings("unchecked") List<Country> countryList = (List<Country>)
	 * map.get("CountryList"); List<CountryVo> countryVoList = new ArrayList<>();
	 * CountryVo countryVo = null; for (Country cat : countryList) { countryVo =
	 * modelMapper.map(cat, CountryVo.class); //
	 * categoryVo.setCreatedDateString(TimeUtils.instance().getDateStrFromMilSec(cat
	 * .getCreatedDate())); //
	 * categoryVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec
	 * (cat.getLastUpdatedTime())); // String base64String =
	 * Base64.encodeBase64String(cat.getCountryFlag()); //
	 * countryVo.setFlag(base64String); // logger.info(countryVo.getFlag());
	 * countryVoList.add(countryVo); } if (countryId > 0) { basicResponse.object =
	 * countryVoList.get(0); } else { basicResponse.object = countryVoList; } return
	 * basicResponse; }
	 */

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<Long, String> getAllCities(long countryId,int status) {
	 * List<City> placeList = null; Map<Long, String> allplaceMap = null;
	 * Map<String, Object> map = inputDao.getAllCity(countryId, 0, "", 0, 0,status);
	 * placeList = (List<City>) map.get("CityList"); if (placeList != null &&
	 * !placeList.isEmpty()) { allplaceMap = new HashMap<>(); for (City c :
	 * placeList) { allplaceMap.put(c.getCityId(), c.getCityName()); }
	 * 
	 * } return allplaceMap; }
	 */

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<Long,String> getAllCities(long countryId) { List<City>
	 * placeList = null; Map<Long, String> allplaceMap = null; Map<String, Object>
	 * map = inputDao.getAllCity(countryId, 0, "", 0, 0); placeList = (List<City>)
	 * map.get("CityList"); if (placeList != null && !placeList.isEmpty()) {
	 * allplaceMap=new HashMap<>(); for (City c : placeList) {
	 * allplaceMap.put(c.getCityId(), c.getCityName()); }
	 * 
	 * } return allplaceMap; }
	 * 
	 */

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllCategoryFromId(long categoryId) {
		BasicResponse basicResponse = null;
		int serviceMode = 0;
		if (categoryId < 0) {
			serviceMode = 1;
		}
		Map<String, Object> map = inputDao.getCategoryFromMainCategoryId(categoryId);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		List<CategoryVo> serviceVos = new ArrayList<>();
		List<Category> categories = (List<Category>) map.get("SubSubList");
		CategoryVo serVo = null;
		for (Category ca : categories) {
			serVo = modelMapper.map(ca, CategoryVo.class);
			serVo.setCreatedDateString(
					TimeUtils.instance().getDateStrFromMilSec(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE)));
			serviceVos.add(serVo);
		}
		if (serviceMode == 1) {
			basicResponse.object = serviceVos.get(0);
		} else {
			basicResponse.object = serviceVos;
		}
		return basicResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllProduct(long productId, long sellerId, long userId, long mainCategoryId, long categoryId,
			long subcategoryId, long countryId, long cityId, int firstIndex, int rowPerPage, int status,
			String productName, long companyId, long numberOfItemsRemaining, long offerId, long manufacturerId,
			double priceStarts, double priceEnds, long productAttributeId, String value, String productNameAr, int data,
			Map<Long, List<String>> attibutesSearch,int featured,int rank) 
	{
		BasicResponse res = null;
		TaxVo taxVo = null;
		OfferVo ofVo = null;
		List<ProductVo> productVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> dataMap = inputDao.getAllProducts(productId, sellerId, userId, mainCategoryId, categoryId,
				countryId, cityId, subcategoryId, firstIndex, rowPerPage, status, productName, companyId,
				numberOfItemsRemaining, offerId, manufacturerId, priceStarts, priceEnds, productAttributeId, value,
				productNameAr, data, attibutesSearch, featured, rank);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Long, String> subcategoryMap = getAllSubCategoryNames();
			Map<Long, String> subcategoryMapAr = getAllSubCategoryNamesAr();

			Map<Long, String> manufactureMap = getallManufactures();
			Map<Long, String> manufactureMapAr = getallManufacturesAr();
			Map<Long, String> categoryMap = getAllCategoryNames();
			Map<Long, String> sub = getAllSubCategoryNames();
			Map<Long, String> subAr = getAllSubCategoryNamesAr();
			Map<Long, String> categoryMapAr = getAllCategoryNamesAr();
			Map<Long, String[]> countryMap = getAllCountry(1);
			Map<Long, String[]> cityMap = getAllCities(0, 1);
			Map<Long, String[]> attributeMap = getAllAttribute(1);
			List<Product> products = (List<Product>) dataMap.get("ProudctList");
			logger.info("************"+products+"**********");

			Map<String, List<String>> NewMapArrayList = (Map<String, List<String>>) dataMap.get("AttributeValues");
			Map<String, List<String>> NewMapArrayListAr = (Map<String, List<String>>) dataMap.get("AttributeValuesAr");

			List<Manufacture> manu = (List<Manufacture>) dataMap.get("ManufactureList");
			List<Attributes> atributeDetails = (List<Attributes>) dataMap.get("AttributeDetails");
			Map<Attributes, List<AttributeShowVo>> newMapList = (Map<Attributes, List<AttributeShowVo>>) dataMap
					.get("NewMapAttriList");

			Set<AttributeDetails> AttributeList = (Set<AttributeDetails>) dataMap.get("AttributeList");

			long totalCounts = (long) dataMap.get("TotalCounts");

			returnMap.put("TOTALCOUNTS", totalCounts);
			ProductVo prVo = null;

			// added
			for (Product prod : products) {

				prVo = modelMapper.map(prod, ProductVo.class);
				prVo.setDescription(prod.getDescription());
				prVo.setDescriptionAr(prod.getDescriptionAr());
				prVo.setRatingPercentage(prod.getRatingPercentage());
				prVo.setShippingCharge(prod.getShippingCharge());
				prVo.setInstallationCharge(prod.getInstallationCharge());
				// prVo.setTaxPercentage(prod.getTaxPercentage());

				if (prVo.getTaxId() > 0 && prVo.getOfferId() > 0) 
				{
					Map<String, Object> taxMap = orderDao.getTax(0, 0, prVo.getTaxId(), 100, 0, 1);
					Map<String, Object> offerMap = inputDao.getAllOffer(prVo.getOfferId(), 0, 0, 1, null, null, 0, 100);
					List<TaxManagement> taxList = (List<TaxManagement>) taxMap.get("TaxList");
					List<Offer> offerList = (List<Offer>) offerMap.get("CityList");

					for (TaxManagement tax : taxList) 
					{
						taxVo = modelMapper.map(tax, TaxVo.class);
						prVo.setTaxPercentage(taxVo.getTaxPercentage());
						
						prVo.setTotalAmount(prVo.getPrice());

						for (Offer off : offerList) {
							ofVo = modelMapper.map(off, OfferVo.class);
							if (ofVo.getOfferRate() > 0) {
								prVo.setTotalAmount(prVo.getPrice());
								prVo.setTotalOfferAmount(prVo.getPrice() - ofVo.getOfferRate());
								double ofP = (double) (ofVo.getOfferRate() / prVo.getPrice() * 100);
								double roundOff = Math.round(ofP * 100.0) / 100.0;
								prVo.setOfferPercentage(roundOff);
								prVo.setOfferRatePercentage(roundOff);
							}
							if (ofVo.getOfferPercentage() > 0) {
								double p = ((double) ofVo.getOfferPercentage() / 100);
								logger.info(p + "p___________________________________________________________");
								double pOfferRate = prVo.getPrice() - (prVo.getPrice() * (Double.valueOf(p)));
								prVo.setTotalAmount(prVo.getPrice());
								prVo.setTotalOfferAmount(pOfferRate);
								
								prVo.setOfferPercentage(ofVo.getOfferPercentage());
								prVo.setOfferRatePercentage(ofVo.getOfferPercentage());
							}

						}
						

					}

				} else if (prVo.getTaxId() > 0) {
					Map<String, Object> dataMap3 = orderDao.getTax(0, 0, prVo.getTaxId(), 100, 0, 1);
					if ((int) dataMap3.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
						List<TaxManagement> taxList = (List<TaxManagement>) dataMap3.get("TaxList");

						for (TaxManagement tax : taxList) {
							taxVo = modelMapper.map(tax, TaxVo.class);
							prVo.setTaxPercentage(taxVo.getTaxPercentage());
							logger.info(prVo.getTaxPercentage());

							
							logger.info(prVo.getPrice());
							prVo.setTotalAmount(prVo.getPrice());
							prVo.setTotalOfferAmount(prVo.getPrice());
							prVo.setOfferPercentage(0);
							prVo.setOfferRatePercentage(0);
							
							

						}

					}

				} else if (prVo.getTaxId() == 0 && prVo.getOfferId() == 0) {
					prVo.setTotalAmount(prVo.getPrice());
					prVo.setTotalOfferAmount(prVo.getPrice());
					prVo.setOfferPercentage(0);
					prVo.setOfferRatePercentage(0);
					
					
				}else if(prVo.getOfferId() > 0)
				{
					Map<String, Object> offerMap1 = inputDao.getAllOffer(prVo.getOfferId(), 0, 0, 1, null, null, 0, 100);
					List<Offer> offerList = (List<Offer>) offerMap1.get("CityList");
					for (Offer off : offerList) {
						ofVo = modelMapper.map(off, OfferVo.class);
						if (ofVo.getOfferRate() > 0)
						{
							prVo.setTotalAmount(prVo.getPrice());
							prVo.setTotalOfferAmount(prVo.getPrice() - ofVo.getOfferRate());
							double ofP = (double) (ofVo.getOfferRate() / prVo.getPrice() * 100);
							prVo.setOfferPercentage(ofP);
							prVo.setOfferRatePercentage(ofP);
						}
						if (ofVo.getOfferPercentage() > 0) {
							double p = ((double) ofVo.getOfferPercentage() / 100);
							logger.info(p + "p___________________________________________________________");
							double pOfferRate = prVo.getPrice() - (prVo.getPrice() * (Double.valueOf(p)));
							prVo.setTotalAmount(prVo.getPrice());
							prVo.setTotalOfferAmount(pOfferRate);
							prVo.setOfferPercentage(ofVo.getOfferPercentage());
							prVo.setOfferRatePercentage(ofVo.getOfferPercentage());
						}

					}
					
				}

				FeatureVo fVo = null;
				prVo.setFeatureOrPhotos(new ArrayList<>());

				BasicResponse resp = getAllOfferProduct(prod.getOfferId());
				if (resp.getErrorCode() == ERROR_CODE_NO_ERROR) {
					if (prod.getOfferId() > 0) {
						List<OfferVo> exTimeList = (List<OfferVo>) resp.getObject();

						prVo.getOfferVos().addAll(exTimeList);
					}
				}
				List<FeaturesAndPhotosProduct> mainList = new ArrayList<FeaturesAndPhotosProduct>();
				FeatureCompareVo ratingCompare = new FeatureCompareVo(); 
				mainList.addAll(prod.getFeatureAndPhotos());
				boolean flag = true;
				  Collections.sort(mainList, ratingCompare); 
				  logger.info(mainList.toString());
				
				for (FeaturesAndPhotosProduct fea : mainList) 
			   {

					fVo = modelMapper.map(fea, FeatureVo.class);
					//Collections.sort(fVo); 
					logger.info(fVo.getModeId()+"modeId----------------");

					if (flag && fea.getModeId() == MODE_PHOTOS) 
					{

//						String image = fea.getData();
//						prVo.setImageName(image);

						flag = false;
					}
					fVo.setLastUpdatedTime(fea.getLastUpdatedTime());

					prVo.getFeatureOrPhotos().add(fVo);
				}

				ProductAttributeVo pVo = null;
				for (ProductAttributes pa : prod.getProudctAttributes()) 
				{
					pVo = modelMapper.map(pa, ProductAttributeVo.class);

					String[] names = attributeMap.get(pa.getAttributeId());
					if (names != null && names.length != 0) {
						pVo.setAttributeName(names[0]);
						pVo.setAttributeNameAr(names[1]);
					}
					prVo.getProductAttributeVos().add(pVo);
				}
				ConnectedProductsVo cVo = null;
				for (ConnectedProducts pa : prod.getConnectedProducts()) {
					cVo = modelMapper.map(pa, ConnectedProductsVo.class);
					// logger.info(pa.getAttributeId());
					String[] names = attributeMap.get(pa.getConnectedProductsId());
					if (names != null && names.length != 0) {
						pVo.setAttributeName(names[0]);
						pVo.setAttributeNameAr(names[1]);
					}
					prVo.getConnectedProductsVo().add(cVo);
				}

				prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(prod.getLastUpdatedTime()));
				prVo.setSellerAddress(prod.getSeller().getAddress());
				prVo.setPhone(prod.getSeller().getPhoneNumber());
				prVo.setContactName(prod.getSeller().getContactName());
				prVo.setTelephoneNo(prod.getSeller().getTelephoneNo());
				prVo.setSellerNameAr(prod.getSeller().getSellerNameAr());
				prVo.setCategoryName(categoryMap.get(prod.getCategoryId()));
				prVo.setCategoryNameAr(categoryMapAr.get(prod.getCategoryId()));
				prVo.setManufacturerName(manufactureMap.get(prod.getManufacturerId()));
				prVo.setManufacturerNameAr(manufactureMapAr.get(prod.getManufacturerId()));
				prVo.setBusinessType(manufactureMap.get(prod.getManufacturerId()));
				if (prod.getSubCategoryId() != 0) {
					prVo.setSubcategoryName(sub.get(prod.getSubCategoryId()));
					// logger.info(subAr.get(prod.getSubCategoryId())+"arabic!!!!!!!!!!!!!!!!!!!!_______________________1__________");
					prVo.setSubcategoryNameAr(subAr.get(prod.getSubCategoryId()));
				}
				prVo.setSellerName(prod.getSeller().getSellerName());
				// prVo.setCityName(cityMap.get(prod.getSeller().getCityId()));
				// prVo.setCountryName(countryMap.get(prod.getSeller().getCountryId()));
				prVo.setLatitude(prod.getSeller().getLatitude());
				prVo.setLongitude(prod.getSeller().getLongitude());
				/*if (countryMap != null) {
					String names[] = countryMap.get(prod.getSeller().getCountryId());
					prVo.setCountryName(names[0]);
					prVo.setCountryNameAr(names[1]);
				}
*/
				productVos.add(prVo);
			}
			returnMap.put("Product", productVos);
			if (productId > 0) {
				returnMap.put("Product", productVos.get(0));
				List<Review> reviews = (List<Review>) dataMap.get("Reviews");
				List<ReviewVo> reviewVos = new ArrayList<>();
				ReviewVo revVo = null;

				for (Review re : reviews) {
					revVo = modelMapper.map(re, ReviewVo.class);
					prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.getLastUpdatedTime()));
					reviewVos.add(revVo);
				}

				returnMap.put("REVIEWS", reviewVos);

			}

			returnMap.put("ManufactureList", manu);
			returnMap.put("AttributeList", AttributeList);
			returnMap.put("AttributeDetailsList", atributeDetails);
			returnMap.put("ListOfAttributeDetails", NewMapArrayList);
			returnMap.put("MewAttriCompleteDetails", newMapList);
			returnMap.put("AttributelistAR", NewMapArrayListAr);
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = returnMap;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getProductswithoutOffer(int rowPerPage, int firstIndex) {
		BasicResponse res = null;
		List<ProductVo> productVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> dataMap = inputDao.getProductswithoutOffer(rowPerPage, firstIndex);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Long, String> subcategoryMap = getAllSubCategoryNames();
			Map<Long, String> offerMap = getAllOffers();
			Map<Long, String> manufactureMap = getallManufactures();
			Map<Long, String> categoryMap = getAllCategoryNames();
			Map<Long, String[]> countryMap = getAllCountry(1);
			Map<Long, String[]> cityMap = getAllCities(0, 1);
			Map<Long, String[]> attributeMap = getAllAttribute(1);
			List<Product> products = (List<Product>) dataMap.get("ProudctList");
			long totalCounts = (long) dataMap.get("TotalCounts");

			returnMap.put("TOTALCOUNTS", totalCounts);
			ProductVo prVo = null;
			for (Product prod : products) {

				prVo = modelMapper.map(prod, ProductVo.class);
				prVo.setDescription(prod.getDescription());
				prVo.setDescriptionAr(prod.getDescriptionAr());
				FeatureVo fVo = null;

				prVo.setFeatureOrPhotos(new ArrayList<>());

				boolean flag = true;
				for (FeaturesAndPhotosProduct fea : prod.getFeatureAndPhotos()) {

					fVo = modelMapper.map(fea, FeatureVo.class);

					if (flag && fea.getModeId() == MODE_PHOTOS) {
						logger.info(fea.getData() + "image name-------------------------");
						String image = fea.getData();
						prVo.setImageName(image);
						// prVo.setProductImage(image);
						logger.info(prVo.getImageName()
								+ "get image name---------------------------------------------------");
						flag = false;
					}
					fVo.setLastUpdatedTime(fea.getLastUpdatedTime());

					prVo.getFeatureOrPhotos().add(fVo);
				}

				ProductAttributeVo pVo = null;
				for (ProductAttributes pa : prod.getProudctAttributes()) {
					pVo = modelMapper.map(pa, ProductAttributeVo.class);
					logger.info(pa.getAttributeId());
					String[] names = attributeMap.get(pa.getAttributeId());
					if (names != null && names.length != 0) {
						pVo.setAttributeName(names[0]);
						pVo.setAttributeNameAr(names[1]);
					}
					prVo.getProductAttributeVos().add(pVo);
				}
				ConnectedProductsVo cVo = null;
				/*
				 * for (ConnectedProducts pa : prod.getConnectedProducts()) { cVo =
				 * modelMapper.map(pa, ConnectedProductsVo.class);
				 * //logger.info(pa.getAttributeId()); String[]
				 * names=attributeMap.get(pa.getConnectedProductsId()); if(names!= null &&
				 * names.length != 0) { pVo.setAttributeName(names[0]);
				 * pVo.setAttributeNameAr(names[1]); } prVo.getConnectedProductsVo().add(cVo); }
				 */

				prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(prod.getLastUpdatedTime()));
				prVo.setSellerAddress(prod.getSeller().getAddress());
				prVo.setPhone(prod.getSeller().getPhoneNumber());
				prVo.setContactName(prod.getSeller().getContactName());
				prVo.setTelephoneNo(prod.getSeller().getTelephoneNo());

				prVo.setCategoryName(categoryMap.get(prod.getCategoryId()));
				prVo.setCategoryNameAr(categoryMap.get(prod.getCategoryId()));
				prVo.setSubcategoryName(subcategoryMap.get(prod.getSubCategoryId()));
				prVo.setSubcategoryNameAr(subcategoryMap.get(prod.getSubCategoryId()));
				prVo.setOfferName(offerMap.get(prod.getOfferId()));
				prVo.setOfferNameAr(offerMap.get(prod.getOfferId()));
				// prVo.setDescription(offerMap.get(prod.getOfferId()));
				// prVo.setDescriptionAr(offerMap.get(prod.getOfferId()));
				// prVo.setImageName(offerMap.get(prod.getOfferId()));
				prVo.setManufacturerName(manufactureMap.get(prod.getManufacturerId()));
				prVo.setBusinessType(manufactureMap.get(prod.getManufacturerId()));
				if (prod.getSubCategoryId() != 0) {
					prVo.setSubcategoryName(subcategoryMap.get(prod.getSubCategoryId()));
					prVo.setSubcategoryNameAr(subcategoryMap.get(prod.getSubCategoryId()));
				}
				prVo.setSellerName(prod.getSeller().getSellerName());
				// prVo.setCityName(cityMap.get(prod.getSeller().getCityId()));
				// prVo.setCountryName(countryMap.get(prod.getSeller().getCountryId()));
				prVo.setLatitude(prod.getSeller().getLatitude());
				prVo.setLongitude(prod.getSeller().getLongitude());
				if (countryMap != null) {
					String names[] = countryMap.get(prod.getSeller().getCountryId());
					prVo.setCountryName(names[0]);
					prVo.setCountryNameAr(names[1]);
				}
				if (cityMap != null) {
					String names[] = cityMap.get(prod.getSeller().getCityId());
					prVo.setCityName(names[0]);
					prVo.setCityNameAr(names[1]);
				}
				productVos.add(prVo);
			}
			returnMap.put("Product", productVos);
			/*
			 * if (productId > 0) { returnMap.put("Product", productVos.get(0));
			 * List<Review> reviews = (List<Review>) dataMap.get("Reviews"); List<ReviewVo>
			 * reviewVos = new ArrayList<>(); ReviewVo revVo = null; List<OfferDetails>
			 * offer = (List<OfferDetails>) dataMap.get("Offer"); List<OfferDetailsVo>
			 * offerDetailsVos = new ArrayList<>(); OfferDetailsVo offerDetailsVo =null; for
			 * (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class);
			 * prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); reviewVos.add(revVo); }
			 * 
			 * 
			 * returnMap.put("REVIEWS", reviewVos);
			 * 
			 * }
			 */
			/*
			 * if (productId > 0) { returnMap.put("Product", productVos.get(0));
			 * List<OfferDetails> offer = (List<OfferDetails>) dataMap.get("Offer");
			 * List<OfferDetailsVo> offerDetailsVos = new ArrayList<>(); OfferDetailsVo
			 * offerDetailsVo =null;
			 * 
			 * for (OfferDetails of : offer) { offerDetailsVo = modelMapper.map(of,
			 * OfferDetailsVo.class);
			 * //prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); offerDetailsVos.add(offerDetailsVo); }
			 * 
			 * //returnMap.put("REVIEWS", reviewVos); returnMap.put("OFFER",
			 * offerDetailsVos); }
			 */
			/*
			 * if (productId > 0) { returnMap.put("Product", productVos.get(0));
			 * List<ConnectedProducts> connectedProducts = (List<ConnectedProducts>)
			 * dataMap.get("ConnectedProducts"); List<ConnectedProductsVo> connectedVos =
			 * new ArrayList<>(); ConnectedProductsVo connectedVo = null;
			 * 
			 * for (ConnectedProducts re : connectedProducts) { connectedVo =
			 * modelMapper.map(re, ConnectedProductsVo.class);
			 * prVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); connectedVos.add(connectedVo); }
			 * returnMap.put("CONNECTEDPRODUCTS", connectedVos); }
			 */
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = returnMap;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;

	}

	/*
	 * @Override public BasicResponse getAllShops(long shopId, long categoryId, long
	 * subCategoryId, int firstIndex, int rowPerPage,long fromDate,long toDate) {
	 * BasicResponse res = null; List<StoreVo> shopVos = new ArrayList<>();
	 * Map<String, Object> returnMap = new HashMap<>(); //Map<Long, String>
	 * subcategoryMap = getAllSubCategoryNames(); // Map<Long, String>
	 * maincategoryMap = getAllMainCategoryNames(); //Map<Long, String> categoryMap
	 * = getAllCategoryNames(); Map<String, Object> dataMap =
	 * inputDao.getAllShops(shopId,0,categoryId,subCategoryId,firstIndex,rowPerPage,
	 * fromDate,toDate); if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
	 * List<Store> stores = (List<Store>) dataMap.get("ShopList"); long totalCounts
	 * = (long) dataMap.get("TotalCounts"); returnMap.put("TOTALCOUNTS",
	 * totalCounts); StoreVo strVo = null; for(Store store:stores) { strVo =
	 * modelMapper.map(store,StoreVo.class); FeatureVo fVo = null;
	 * strVo.setFeatureVo(new ArrayList<>());
	 * 
	 * for(FeaturesAndPhotosShop fea:store.getFeat) { fVo =
	 * modelMapper.map(fea,FeatureVo.class);
	 * fVo.setLastUpdatedTime(TimeUtils.instance().getDateStrFromMilSec(fea.
	 * getLastUpdatedTime())); strVo.getFeatureVo().add(fVo);
	 * 
	 * } strVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(
	 * store.getLastUpdatedTime())); shopVos.add(strVo); } returnMap.put("Store",
	 * shopVos); if (shopId > 0) { returnMap.put("Store", shopVos.get(0)); } res =
	 * new BasicResponse(); res.errorCode = (int) dataMap.get(ERROR_CODE);
	 * res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
	 * res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
	 * res.object = returnMap; } else { res = new BasicResponse(); res.errorCode =
	 * (int) dataMap.get(ERROR_CODE); res.errorMessage = (String)
	 * dataMap.get(ERROR_MESSAGE); res.errorMessageArabic = (String)
	 * dataMap.get(ERROR_MESSAGE_ARABIC); } return res; }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse addOffer(OfferVo offerVo) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = null;
		// byte[] backToBytes = Base64.decodeBase64(countryVo.getFlag());
		Offer offer = modelMapper.map(offerVo, Offer.class);
		/*
		 * offer.setOfferEnds("00"); offer.setOfferStarts("00");
		 */
		// country.setCountryFlag(backToBytes);
		offer.setOfferStarts(offerVo.getOfferStarts());
		offer.setOfferEnds(offerVo.getOfferEnds());
		/*
		 * if(offerVo.getOfferRate() > 0) { double offR=offerVo.getOfferRate(); long
		 * offerPercentage= }
		 */
		map = inputDao.addOffer(offer);
		basicResponse = new BasicResponse();
		// basicResponse.setObject(map);
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		return basicResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllReviews(long fatherId, long cityId, long countryId, long customerId, long sellerId,
			int currentIndex, int rowPerPage, long fromDate, long toDate) {
		double total = 0;
		double av = 0;
		BasicResponse basicResponse = null;
		Product p = new Product();
		long ratingPercentage = 0;
		List<ReviewVo> reviewVos = null;
		Map<String, Object> returnMap = null;
		Map<Long, String> customerMap = null;
		Map<Long, String[]> customernew = null;
		Map<String, Object> map1 = null;
		if (fatherId < 0) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 4;
			basicResponse.errorMessage = MESSAGE_NULL_VALUE;
			return basicResponse;
		}
		Map<String, Object> map = inputDao.getAllReviews(fatherId, cityId, countryId, customerId, sellerId,
				currentIndex, rowPerPage, fromDate, toDate);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			customerMap = generalService.getAllCustomerMap();
			customernew = generalService.getAllCustomerMap1();
			reviewVos = new ArrayList<>();
			List<Review> reviews = (List<Review>) map.get("ReviewList");
			long totalCounts = (long) map.get("TotalCounts");
			ReviewVo reVo = null;
			for (Review re : reviews) {
				reVo = new ReviewVo();
				reVo.setData(re.getData());
				reVo.setFatherId(re.getFatherId());
				reVo.setLikes(re.getLikes());
				reVo.setModeId(re.getModeId());
				reVo.setUserId(re.getUserId());
				reVo.setRating(re.getRating());
				reVo.setComments(re.getComments());
				total = total + reVo.getRating();
				reVo.setReviewId(re.getReviewId());
				reVo.setLastUpdatedTime(re.getLastUpdatedTime());
				reVo.setName(customerMap.get(re.getUserId()));
				av = total / totalCounts;
				// reVo.setTotalRating(av);
				long u = re.getUserId();
				CustomerDetails cust = generalService.getCustomerFromUserId(u);
				ratingPercentage = (long) ((av / 5) * 100);
				reVo.setRatingPercentage(ratingPercentage);

				if (cust != null) {
					reVo.setCustomerName(cust.getCustomerName());
					reVo.setCustomerNameAr(cust.getCustomerNameAr());

				}
				reviewVos.add(reVo);

			}

			p.setRating(av);
			logger.info(
					ratingPercentage + "ratingPercentage------------------------------------------------------------");
			p.setRatingPercentage(ratingPercentage);
			logger.info(
					p.getRatingPercentage() + "-----------rating percentage------------------------------------------");
			map1 = inputDao.updateProduct(av, ratingPercentage, reVo.getFatherId());
			if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
				basicResponse = new BasicResponse();
			}
			returnMap = new HashMap<>();
			returnMap.put("AVERAGERATING", av);
			returnMap.put("RATING PERCENTAGE", ratingPercentage);
			returnMap.put("REVIEW", reviewVos);
			basicResponse.errorCode = (int) map.get(ERROR_CODE);
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			basicResponse.object = returnMap;
			basicResponse.totalRecords = totalCounts;
		} else {
			returnMap = new HashMap<>();
			returnMap.put("AVERAGERATING", av);
			returnMap.put("RATING PERCENTAGE", ratingPercentage);
			basicResponse.errorCode = (int) map.get(ERROR_CODE);
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			basicResponse.object = returnMap;

		}
		return basicResponse;
	}

	@Override
	public Map<Long, String> getAllBusinessNames() {
		Map<Long, String> dataMap = new HashMap<>();
		dataMap.put((long) 1, "Furniture");
		dataMap.put((long) 2, "Fancy");
		dataMap.put((long) 3, "Clothing");
		dataMap.put((long) 4, "Pharamedicals");
		dataMap.put((long) 5, "Stationary");
		return dataMap;
	}

	@Override
	public Map<Integer, String> getAllStatusMap() {
		Map<Integer, String> rollMap = new HashMap<Integer, String>();
		rollMap.put(ORDERED, STRING_ORDERED);
		rollMap.put(CONFIRMED, STRING_CONFIRMED);
		rollMap.put(CANCELED, STRING_CANCELED);
		rollMap.put(READY_TO_DELIVERY, STRING_READY_TO_DELIVERY);
		rollMap.put(REJECTED, STRING_REJECTED);
		// rollMap.put(RETURN_REQUESTED, STRING_RETURN_REQUESTED);
		// rollMap.put(RETURN_ACCEPTED, STRING_RETURN_ACCEPTED);
		// rollMap.put(RETURN_REJECTED, STRING_RETURN_REJECTED);
		// rollMap.put(RETURN_CANCELLED, STRING_RETURN_CANCELLED);
		// rollMap.put(RETURN_COMPLETED, STRING_RETURN_COMPLETED);
		return rollMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse searchData(Map<Integer, String> data, int mode, int firstResult, int rowPerPage,
			double latitude, double longitude) {
		Map<String, Object> returnMap = null;
		BasicResponse basicResponse = null;
		List<ProductVo> productVos = null;
		TaxVo taxVo = null;
		OfferVo ofVo = null;
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> getMap = inputDao.searchData(data, mode, firstResult, rowPerPage, latitude, longitude);
		basicResponse = new BasicResponse();
		List<Product> products = (List<Product>) getMap.get("PRODUCTS");
		List<SellerDetails> stores = (List<SellerDetails>) getMap.get("SHOPS");
		List<Category> cat = (List<Category>) getMap.get("CATEGORY");
		List<SubCategory> subcat = (List<SubCategory>) getMap.get("SUBCATEGORY");

		if ((int) getMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			products = (List<Product>) getMap.get("PRODUCTS");

			stores = (List<SellerDetails>) getMap.get("SHOPS");
			List<Category> cat1 = (List<Category>) getMap.get("CATEGORY");
			List<SubCategory> subcat1 = (List<SubCategory>) getMap.get("SUBCATEGORY");

			// long totalCountShop = (long) getMap.get("TotalCountsShop");
			long totalCountProd = (long) getMap.get("TotalCountsProduct");
			// long totalCountCategory = (long) getMap.get("TotalCountsCat");
			// long totalCountSubCategory = (long) getMap.get("TotalCountsSubCat");

			// long totalCountShop = (long) getMap.get("TotalCountsShop");

			returnMap = new HashMap<>();
			returnMap.put("TOTALCOUNTPROD", totalCountProd);
			/*
			 * returnMap.put("TOTALCOUNTSHOP", totalCountShop);
			 * returnMap.put("TOTALCOUNTCATEGORY", totalCountCategory);
			 * returnMap.put("TOTALCOUNTSUB_CATEGORY", totalCountSubCategory);
			 */
			Map<Long, String> categoryMap = getAllCategoryNames();
			Map<Long, String> categoryMapAr = getAllCategoryNamesAr();
			Map<Long, String> subCategoryMap = getAllSubCategoryNames();
			Map<Long, String> subCategoryMapAr = getAllSubCategoryNamesAr();
			productVos = new ArrayList<>();
			ProductVo paVo = null;
			for (Product pal : products) {
				paVo = modelMapper.map(pal, ProductVo.class);
				if (paVo.getTaxId() > 0 && paVo.getOfferId() > 0) 
				{
					Map<String, Object> taxMap = orderDao.getTax(0, 0, paVo.getTaxId(), 100, 0, 1);
					Map<String, Object> offerMap = inputDao.getAllOffer(paVo.getOfferId(), 0, 0, 1, null, null, 0, 100);
					List<TaxManagement> taxList = (List<TaxManagement>) taxMap.get("TaxList");
					List<Offer> offerList = (List<Offer>) offerMap.get("CityList");

					for (TaxManagement tax : taxList) 
					{
						taxVo = modelMapper.map(tax, TaxVo.class);
						paVo.setTaxPercentage(taxVo.getTaxPercentage());
						
						paVo.setTotalAmount(paVo.getPrice());

						for (Offer off : offerList) {
							ofVo = modelMapper.map(off, OfferVo.class);
							if (ofVo.getOfferRate() > 0) {
								paVo.setTotalAmount(paVo.getPrice());
								paVo.setTotalOfferAmount(paVo.getPrice() - ofVo.getOfferRate());
								double ofP = (double) (ofVo.getOfferRate() / paVo.getPrice() * 100);
								double roundOff = Math.round(ofP * 100.0) / 100.0;
								paVo.setOfferPercentage(roundOff);
								paVo.setOfferRatePercentage(roundOff);
							}
							if (ofVo.getOfferPercentage() > 0) {
								double p = ((double) ofVo.getOfferPercentage() / 100);
								logger.info(p + "p___________________________________________________________");
								double pOfferRate = paVo.getPrice() - (paVo.getPrice() * (Double.valueOf(p)));
								paVo.setTotalAmount(paVo.getPrice());
								paVo.setTotalOfferAmount(pOfferRate);
								
								paVo.setOfferPercentage(ofVo.getOfferPercentage());
								paVo.setOfferRatePercentage(ofVo.getOfferPercentage());
							}

						}
						

					}

				} else if (paVo.getTaxId() > 0) {
					Map<String, Object> dataMap3 = orderDao.getTax(0, 0, paVo.getTaxId(), 100, 0, 1);
					if ((int) dataMap3.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
						List<TaxManagement> taxList = (List<TaxManagement>) dataMap3.get("TaxList");

						for (TaxManagement tax : taxList) {
							taxVo = modelMapper.map(tax, TaxVo.class);
							paVo.setTaxPercentage(taxVo.getTaxPercentage());
							logger.info(paVo.getTaxPercentage());

							
							logger.info(paVo.getPrice());
							paVo.setTotalAmount(paVo.getPrice());
							paVo.setTotalOfferAmount(paVo.getPrice());
							paVo.setOfferPercentage(0);
							paVo.setOfferRatePercentage(0);
							
							

						}

					}

				} else if (paVo.getTaxId() == 0 && paVo.getOfferId() == 0) {
					paVo.setTotalAmount(paVo.getPrice());
					paVo.setTotalOfferAmount(paVo.getPrice());
					paVo.setOfferPercentage(0);
					paVo.setOfferRatePercentage(0);
					
					
				}else if(paVo.getOfferId() > 0)
				{
					Map<String, Object> offerMap1 = inputDao.getAllOffer(paVo.getOfferId(), 0, 0, 1, null, null, 0, 100);
					List<Offer> offerList = (List<Offer>) offerMap1.get("CityList");
					for (Offer off : offerList) {
						ofVo = modelMapper.map(off, OfferVo.class);
						if (ofVo.getOfferRate() > 0)
						{
							paVo.setTotalAmount(paVo.getPrice());
							paVo.setTotalOfferAmount(paVo.getPrice() - ofVo.getOfferRate());
							double ofP = (double) (ofVo.getOfferRate() / paVo.getPrice() * 100);
							paVo.setOfferPercentage(ofP);
							paVo.setOfferRatePercentage(ofP);
						}
						if (ofVo.getOfferPercentage() > 0) {
							double p = ((double) ofVo.getOfferPercentage() / 100);
							logger.info(p + "p___________________________________________________________");
							double pOfferRate = paVo.getPrice() - (paVo.getPrice() * (Double.valueOf(p)));
							paVo.setTotalAmount(paVo.getPrice());
							paVo.setTotalOfferAmount(pOfferRate);
							paVo.setOfferPercentage(ofVo.getOfferPercentage());
							paVo.setOfferRatePercentage(ofVo.getOfferPercentage());
						}

					}
					
				}

				paVo.setCategoryName(categoryMap.get(pal.getCategoryId()));
				paVo.setCategoryNameAr(categoryMapAr.get(pal.getCategoryId()));
				if (pal.getSubCategoryId() != 0) {
					paVo.setSubcategoryName(subCategoryMap.get(pal.getSubCategoryId()));
					paVo.setSubcategoryNameAr(subCategoryMapAr.get(pal.getSubCategoryId()));
				}
				paVo.setCreatedDateString(TimeUtils.instance().getDateStrFromMilSec(pal.getCreatedDate()));
				FeatureVo fVo = null;
				for (FeaturesAndPhotosProduct fap : pal.getFeatureAndPhotos()) {
					fVo = modelMapper.map(fap, FeatureVo.class);
					if (fap.getModeId() == MODE_PHOTOS) {
						logger.info(fap.getData() + "image name-------------------------");
						String image = fap.getData();
						paVo.setImageName(image);
						// prVo.setProductImage(image);
						logger.info(paVo.getImageName()
								+ "get image name---------------------------------------------------");

					}
					fVo.setLastUpdatedTime(fap.getLastUpdatedTime());
					paVo.getFeatureOrPhotos().add(fVo);
				}
				ReviewVo ro = null;
				for (Review fap : pal.getReviews()) {
					if (fap.getStatus() == ACTIVE) {
						ro = modelMapper.map(fap, ReviewVo.class);
						ro.setDate(TimeUtils.instance().getDateStrFromMilSec(fap.getLastUpdatedTime()));
						paVo.getReviewVos().add(ro);
					}
				}
				productVos.add(paVo);
			}
			List<SellerVo> shopVos = new ArrayList<>();
			SellerVo strVo = null;
			for (SellerDetails store : stores) {
				strVo = modelMapper.map(store, SellerVo.class);
				FeatureVo fVo = null;

				strVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(store.getLastUpdatedTime()));
				shopVos.add(strVo);
			}

			List<CategoryVo> catVos = new ArrayList<>();
			CategoryVo catVo = null;

			for (Category category : cat1) {

				catVo = modelMapper.map(category, CategoryVo.class);

				// strVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(category.getLastUpdatedTime()));
				catVos.add(catVo);
			}

			List<SubCategoryVo> subCatVos = new ArrayList<>();
			SubCategoryVo subCatVo = null;

			for (SubCategory categorySub : subcat1) {

				subCatVo = modelMapper.map(categorySub, SubCategoryVo.class);

				// strVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(category.getLastUpdatedTime()));
				subCatVos.add(subCatVo);
			}

			returnMap.put("STORES", shopVos);
			returnMap.put("PRODUCTS", productVos);
			returnMap.put("CATEGORY", catVos);
			returnMap.put("SUBCATEGORY", subCatVos);
			basicResponse.object = returnMap;
			basicResponse.errorCode = (int) getMap.get(ERROR_CODE);
			basicResponse.errorMessage = (String) getMap.get(ERROR_MESSAGE);

			if (getMap.get(ERROR_MESSAGE_ARABIC) != null) {
				basicResponse.errorMessageArabic = (String) getMap.get(ERROR_MESSAGE_ARABIC);
			}

		} else {
			returnMap = new HashMap<>();
			// returnMap.put("TOTALCOUNTPROD", totalCountProd);
			// returnMap.put("TOTALCOUNTSHOP", totalCountShop);
			basicResponse.errorCode = (int) getMap.get(ERROR_CODE);
			basicResponse.errorMessage = (String) getMap.get(ERROR_MESSAGE);
			basicResponse.object = returnMap;
			if (getMap.get(ERROR_MESSAGE_ARABIC) != null) {
				basicResponse.errorMessageArabic = (String) getMap.get(ERROR_MESSAGE_ARABIC);
			}
		}
		return basicResponse;
	}

	// filter
	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse filterData(int mode, int firstResult, int rowPerPage, long categoryId) {
		Map<String, Object> returnMap = null;
		BasicResponse basicResponse = null;
		List<ProductVo> productVos = null;
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Map<String, Object> getMap = inputDao.filterData(mode, firstResult, rowPerPage, categoryId);
		basicResponse = new BasicResponse();
		List<Product> products = (List<Product>) getMap.get("PRODUCTS");

		if ((int) getMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			products = (List<Product>) getMap.get("PRODUCTS");
			long totalCountProd = (long) getMap.get("TotalCountsProduct");

			returnMap = new HashMap<>();
			returnMap.put("TOTALCOUNTPROD", totalCountProd);

			Map<Long, String> categoryMap = getAllCategoryNames();
			Map<Long, String> subCategoryMap = getAllSubCategoryNames();
			productVos = new ArrayList<>();
			ProductVo paVo = null;
			for (Product pal : products) {
				paVo = modelMapper.map(pal, ProductVo.class);
				paVo.setCategoryId(pal.getCategoryId());
				logger.info(paVo.getCategoryId() + "pavao cat");
				paVo.setProductId(pal.getProductId());
				logger.info(paVo.getProductId() + "pavao pro");
				paVo.setCategoryName(categoryMap.get(pal.getCategoryId()));
				if (pal.getSubCategoryId() != 0) {
					paVo.setSubcategoryName(subCategoryMap.get(pal.getSubCategoryId()));
				}
				paVo.setCreatedDateString(TimeUtils.instance().getDateStrFromMilSec(pal.getCreatedDate()));
				FeatureVo fVo = null;
				for (FeaturesAndPhotosProduct fap : pal.getFeatureAndPhotos()) {
					fVo = modelMapper.map(fap, FeatureVo.class);
					if (fap.getModeId() == MODE_PHOTOS) {
						logger.info(fap.getData() + "image name-------------------------");
						String image = fap.getData();
						paVo.setImageName(image);
						// prVo.setProductImage(image);
						logger.info(paVo.getImageName()
								+ "get image name---------------------------------------------------");

					}
					fVo.setLastUpdatedTime(fap.getLastUpdatedTime());
					paVo.getFeatureOrPhotos().add(fVo);
				}
				ReviewVo ro = null;
				for (Review fap : pal.getReviews()) {
					if (fap.getStatus() == ACTIVE) {
						ro = modelMapper.map(fap, ReviewVo.class);
						ro.setDate(TimeUtils.instance().getDateStrFromMilSec(fap.getLastUpdatedTime()));
						paVo.getReviewVos().add(ro);
					}
				}
				productVos.add(paVo);
			}
			List<SellerVo> shopVos = new ArrayList<>();
			SellerVo strVo = null;
			/*
			 * for (SellerDetails store : stores) { strVo = modelMapper.map(store,
			 * SellerVo.class); FeatureVo fVo = null;
			 * 
			 * strVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(
			 * store.getLastUpdatedTime())); shopVos.add(strVo); }
			 */

			List<CategoryVo> catVos = new ArrayList<>();
			CategoryVo catVo = null;

			/*
			 * for (Category category : cat1) {
			 * 
			 * catVo = modelMapper.map(category, CategoryVo.class);
			 * 
			 * //strVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(
			 * category.getLastUpdatedTime())); catVos.add(catVo); }
			 * 
			 * List<SubCategoryVo> subCatVos = new ArrayList<>(); SubCategoryVo subCatVo =
			 * null;
			 * 
			 * for (SubCategory categorySub : subcat1) {
			 * 
			 * subCatVo = modelMapper.map(categorySub, SubCategoryVo.class);
			 * 
			 * //strVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(
			 * category.getLastUpdatedTime())); subCatVos.add(subCatVo); }
			 */

			// returnMap.put("STORES", shopVos);
			returnMap.put("PRODUCTS", productVos);
			// returnMap.put("CATEGORY", catVos);
			// returnMap.put("SUBCATEGORY", subCatVos);

			basicResponse.object = returnMap;
			// basicResponse.totalRecords=(long) getMap.get("TotalCounts");
			basicResponse.errorCode = (int) getMap.get(ERROR_CODE);
			basicResponse.errorMessage = (String) getMap.get(ERROR_MESSAGE);

			if (getMap.get(ERROR_MESSAGE_ARABIC) != null) {
				basicResponse.errorMessageArabic = (String) getMap.get(ERROR_MESSAGE_ARABIC);
			}

		} else {
			returnMap = new HashMap<>();
			// returnMap.put("TOTALCOUNTPROD", totalCountProd);
			// returnMap.put("TOTALCOUNTSHOP", totalCountShop);
			basicResponse.errorCode = (int) getMap.get(ERROR_CODE);
			// basicResponse.totalRecords=(long) getMap.get("TotalCounts");
			basicResponse.errorMessage = (String) getMap.get(ERROR_MESSAGE);
			basicResponse.object = returnMap;
			if (getMap.get(ERROR_MESSAGE_ARABIC) != null) {
				basicResponse.errorMessageArabic = (String) getMap.get(ERROR_MESSAGE_ARABIC);
			}
		}
		return basicResponse;
	}

	/*
	 * @Override public BasicResponse getAllMainCategoryFromId() { BasicResponse
	 * basicResponse = null; List<MainCategory> mainCategories =
	 * inputDao.getAllMainCategoryList(); if (mainCategories != null &&
	 * !mainCategories.isEmpty()) { List<CategoryVo> mList = new ArrayList<>();
	 * CategoryVo cVo = null; MainCategory mainCat=new MainCategory(); mainCat =
	 * modelMapper.map(mainCat,MainCategory.class); cVo = new CategoryVo();
	 * 
	 * //cVo.maincategoryId = mainCat.getMainCategoryId(); //cVo.mainCategoryName =
	 * mainCat.getMainCategoryName(); mList.add(cVo);
	 * 
	 * basicResponse = new BasicResponse(); basicResponse.errorCode =
	 * ERROR_CODE_NO_ERROR; basicResponse.errorMessage = MESSAGE_SUCSESS;
	 * basicResponse.object = mList; return basicResponse; } else { basicResponse =
	 * new BasicResponse(); basicResponse.errorCode = 3; basicResponse.errorMessage
	 * = MESSAGE_NO_DATA_TO_SHOW; return basicResponse; } }
	 */

	@Override
	public BasicResponse getAllSubcategories(long mainCategoryId, long categoryId, long companyId) {
		BasicResponse basicResponse = null;

		List<SubCategory> subCategories = inputDao.getAllSubCategoryList(mainCategoryId, categoryId, companyId);

		if (subCategories != null && !subCategories.isEmpty()) {
			List<SubCategoryVo> mList = new ArrayList<>();
			SubCategoryVo cVo = null;
			for (SubCategory mainCat : subCategories) {
				// mainCat.setCategory(null);
				// TypeMap<SubCategory, CategoryVo>
				// typeMap=modelMapper.createTypeMap(SubCategory.class, CategoryVo.class);
				// typeMap.addMapping(SubCategory::getImageName, CategoryVo::setImageName);
				// modelMapper.getConfiguration().setImplicitMappingEnabled(false);
				/*
				 * modelMapper.addMappings(new PropertyMap<SubCategory, SubCategoryVo>() {
				 * 
				 * @Override protected void configure() { //
				 * map().setImageName(this.source.getImageName());
				 * map().setCatName(this.source.getCategory().getCategoryName()); } });
				 */
				cVo = modelMapper.map(mainCat, SubCategoryVo.class);
				cVo.setCatName(mainCat.getCategory().getCategoryName());
				cVo.setCatNameAr(mainCat.getCategory().getCategoryNameAr());
				mList.add(cVo);

			}

			basicResponse = new BasicResponse();
			basicResponse.errorCode = ERROR_CODE_NO_ERROR;
			basicResponse.errorMessage = MESSAGE_SUCSESS;
			basicResponse.errorMessageArabic = MESSAGE_SUCSESS_AR;

			basicResponse.object = mList;
			return basicResponse;
		} else {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 3;
			basicResponse.errorMessage = MESSAGE_NO_DATA_TO_SHOW;
			return basicResponse;
		}
	}

	@Override
	public BasicResponse saveSubCategory(SubCategoryVo subcategoryVo) {

		BasicResponse basicResponse = null;
		if (subcategoryVo == null) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 4;
			basicResponse.errorMessage = MESSAGE_NULL_VALUE;
			basicResponse.errorMessageArabic = MESSAGE_NULL_VALUE_AR;
			return basicResponse;
		}
		SubCategory category = modelMapper.map(subcategoryVo, SubCategory.class);
		category.setStatus(subcategoryVo.getStatus());
		category.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));

		Map<String, Object> map = inputDao.saveSubCategory(category);
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.object = map.get("CategoryList");
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	/*
	 * @Override public BasicResponse getAllCategoryFromMainCatgoryId(long
	 * mainCatId) { BasicResponse basicResponse = null; Map<String, Object> map =
	 * inputDao.getCategoryFromMainCategoryId(mainCatId); if ((int)
	 * map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) { basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = 2; basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); return basicResponse; } basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = ERROR_CODE_NO_ERROR;
	 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE); List<Category>
	 * cateList = (List<Category>) map.get("CategoryList"); List<CategoryVo>
	 * categoryVoList = new ArrayList<>(); CategoryVo categoryVo = null; Category
	 * cat=new Category();
	 * 
	 * cat = modelMapper.map(cat,Category.class);
	 * 
	 * 
	 * 
	 * if (mainCatId < 0) { basicResponse.object = categoryVoList.get(0); } else {
	 * basicResponse.object = categoryVoList; } return basicResponse; }
	 */

	/*
	 * @Override public BasicResponse getAllShopsByUserId(long userId, int
	 * firstIndex, int rowPerPage) { BasicResponse res = null; List<PalaceVo>
	 * shopVos = new ArrayList<>(); Map<String, Object> returnMap = new HashMap<>();
	 * Map<String, Object> dataMap = inputDao.getAllShopsByUserId(userId,
	 * firstIndex, rowPerPage); if ((int) dataMap.get(ERROR_CODE) ==
	 * ERROR_CODE_NO_ERROR) { // Map<Long, String> mainCategoryMap =
	 * getAllMainCategoryNames(); List<Store> shops = (List<Store>)
	 * dataMap.get("ShopList"); long totalCounts = (long)
	 * dataMap.get("TotalCounts"); returnMap.put("TOTALCOUNTS", totalCounts);
	 * PalaceVo shVo = null; Map<Long, String> subcategoryMap =
	 * getAllSubCategoryNames(); Map<Long, String> categoryMap =
	 * getAllCategoryNames();
	 * 
	 * Store prod=new Store(); prod = modelMapper.map(prod,Store.class);
	 * 
	 * shVo.createdDate =
	 * TimeUtils.instance().getDateStrFromMilSec(prod.getCreatedDate());
	 * 
	 * FeatureVo fVo = null; //logger.info(prod.getFeatureAndPhotos().size() +
	 * "size of fetures"); Map<Long, String> shopCategoryMap = new HashMap<>();
	 * FeaturesAndPhotosShop fap=new FeaturesAndPhotosShop();
	 * 
	 * res = new BasicResponse(); res.errorCode = (int) dataMap.get(ERROR_CODE);
	 * res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
	 * returnMap.put("SHOPS", shopVos); res.object = returnMap; } else { res = new
	 * BasicResponse(); res.errorCode = (int) dataMap.get(ERROR_CODE);
	 * res.errorMessage = (String) dataMap.get(ERROR_MESSAGE); } return res;
	 * 
	 * }
	 */

	// add category

	/*
	 * @Override public BasicResponse saveOrUpdateCategory(CategoryVo categoryVo) {
	 * BasicResponse basicResponse = new BasicResponse(); Map<String,Object> map =
	 * null; Category category=new Category(); category =
	 * modelMapper.map(categoryVo,Category.class);
	 * 
	 * category.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE)
	 * ); category.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE));
	 * 
	 * map = inputDao.saveOrUpdateCategory(category); basicResponse = new
	 * BasicResponse(); basicResponse.setObject(map); basicResponse.errorCode =
	 * (int) map.get(ERROR_CODE); basicResponse.errorMessage = (String)
	 * map.get(ERROR_MESSAGE); return basicResponse;
	 * 
	 * 
	 * }
	 */

	@Override
	public BasicResponse saveReview(ReviewVo reviewVo)

	{
		BasicResponse basicResponse = null;
		Map<String, Object> map = null;
		Review review = new Review();
		review = modelMapper.map(reviewVo, Review.class);

		review.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		review.setLikes(0);
		review.setModeId(MODE_REVIEW);

		review.setStatus(ACTIVE);

		map = inputDao.saveReview(review);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	public BasicResponse addComments(String comments, long reviewId) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = null;
		Review review = new Review();

		map = inputDao.addComments(comments, reviewId);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@Override
	public BasicResponse getAllProductsByUserId(long userId, int firstIndex, int rowPerPage) {

		return null;
	}

	/*
	 * @Override public BasicResponse getAllCity(long countryId,long cityId,String
	 * cityName, int firstIndex, int rowPerPage) {
	 * 
	 * BasicResponse res = null;
	 * 
	 * List<CityVo> cityVos = new ArrayList<>(); Map<String, Object> returnMap = new
	 * HashMap<>();
	 * 
	 * Map<String, Object> dataMap = inputDao.getAllCity(countryId, cityId,
	 * cityName, firstIndex, rowPerPage); if ((int) dataMap.get(ERROR_CODE) ==
	 * ERROR_CODE_NO_ERROR) {
	 * 
	 * @SuppressWarnings("unchecked") List<City> citylist = (List<City>)
	 * dataMap.get("CityList"); long totalCounts = (long)
	 * dataMap.get("TotalCounts"); CityVo cityvo = null; for (City city : citylist)
	 * { cityvo = modelMapper.map(city, CityVo.class); cityVos.add(cityvo); }
	 * returnMap.put("City", cityVos); if (orderId > 0) { returnMap.put("Order",
	 * orderVos.get(0)); List<Review> reviews = (List<Review>)
	 * dataMap.get("Reviews"); List<ReviewVo> reviewVos = new ArrayList<>();
	 * ReviewVo revVo = null;
	 * 
	 * for (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class); //
	 * ordervo.setLastUpdatedTime(TimeUtils.instance().getDateStrFromMilSec(re.
	 * getLastUpdatedTime())); reviewVos.add(revVo); } returnMap.put("REVIEWS",
	 * reviewVos); } res = new BasicResponse(); res.errorCode = (int)
	 * dataMap.get(ERROR_CODE); res.errorMessage = (String)
	 * dataMap.get(ERROR_MESSAGE); res.errorMessageArabic = (String)
	 * dataMap.get(ERROR_MESSAGE_ARABIC); res.object = cityVos; res.totalRecords =
	 * totalCounts; } else { res = new BasicResponse(); res.errorCode = (int)
	 * dataMap.get(ERROR_CODE); res.errorMessage = (String)
	 * dataMap.get(ERROR_MESSAGE); res.errorMessageArabic = (String)
	 * dataMap.get(ERROR_MESSAGE_ARABIC); } return res; }
	 */

	/*
	 * @Override public BasicResponse getAllCountry(long countryId) { BasicResponse
	 * basicResponse = null;
	 * 
	 * Map<String, Object> map = inputDao.getAllCountry(countryId); long totalCounts
	 * = (long) map.get("TotalCounts"); if ((int) map.get(ERROR_CODE) !=
	 * ERROR_CODE_NO_ERROR) { basicResponse = new BasicResponse();
	 * basicResponse.errorCode = 2; basicResponse.errorMessage = (String)
	 * map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); basicResponse.totalRecords = totalCounts;
	 * return basicResponse; } basicResponse = new BasicResponse();
	 * basicResponse.errorCode = ERROR_CODE_NO_ERROR; basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); basicResponse.totalRecords = totalCounts;
	 * 
	 * @SuppressWarnings("unchecked") List<Country> countryList = (List<Country>)
	 * map.get("CountryList"); List<CountryVo> countryVoList = new ArrayList<>();
	 * CountryVo countryVo = null; for (Country cat : countryList) { countryVo =
	 * modelMapper.map(cat,CountryVo.class);
	 * //categoryVo.setCreatedDateString(TimeUtils.instance().getDateStrFromMilSec(
	 * cat.getCreatedDate()));
	 * //categoryVo.setLastUpdatedTimeString(TimeUtils.instance().
	 * getDateStrFromMilSec(cat.getLastUpdatedTime()));
	 * countryVoList.add(countryVo); } if (countryId > 0) { basicResponse.object =
	 * countryVoList.get(0); } else { basicResponse.object = countryVoList; } return
	 * basicResponse; }
	 */

	/*
	 * @Override public BasicResponse saveRequestItem(RequestItemsVo requestItemsVo)
	 * { BasicResponse basicResponse = null; Map<String, Object> map = null;
	 * RequestItems requestItems = new RequestItems(); requestItems =
	 * modelMapper.map(requestItemsVo,RequestItems.class);
	 * requestItems.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); requestItems.setStatus(ACTIVE);
	 * requestItems.setCreatedTime(TimeUtils.instance().getCurrentTime(
	 * TAKE_TIME_SOURSE)); map = inputDao.saveRequestItem(requestItems);
	 * basicResponse = new BasicResponse(); basicResponse.errorCode = (int)
	 * map.get(ERROR_CODE); basicResponse.errorMessage = (String)
	 * map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic=(String)
	 * map.get(ERROR_MESSAGE_ARABIC); return basicResponse; }
	 * 
	 * 
	 * @Override public BasicResponse updateRequestItem(long requestId, int status)
	 * { BasicResponse basicResponse = null; Map<String, Object>
	 * map=inputDao.editRequestItem(requestId, status); basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = (int) map.get(ERROR_CODE);
	 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
	 * basicResponse.errorMessageArabic=(String) map.get(ERROR_MESSAGE_ARABIC);
	 * return basicResponse; }
	 * 
	 * 
	 * 
	 * @Override public BasicResponse getAllItemRequest(long requestItemId,long
	 * userId) { BasicResponse basicResponse = null; Map<String, Object> map =
	 * inputDao.getAllItemRequest(requestItemId,userId); if ((int)
	 * map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) { basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = 2; basicResponse.errorMessage =
	 * (String) map.get(ERROR_MESSAGE); basicResponse.errorMessageArabic = (String)
	 * map.get(ERROR_MESSAGE_ARABIC); return basicResponse; } basicResponse = new
	 * BasicResponse(); basicResponse.errorCode = ERROR_CODE_NO_ERROR;
	 * basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
	 * basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
	 * 
	 * @SuppressWarnings("unchecked") List<RequestItems> requestItemList =
	 * (List<RequestItems>) map.get("itemList"); List<RequestItemsVo>
	 * requestItemVoList = new ArrayList<>(); RequestItemsVo requestItemVo = null;
	 * for (RequestItems req : requestItemList) { requestItemVo =
	 * modelMapper.map(req,RequestItemsVo.class);
	 * 
	 * requestItemVoList.add(requestItemVo); } if (requestItemId > 0) {
	 * basicResponse.object = requestItemVoList.get(0); } else {
	 * basicResponse.object = requestItemVoList; } return basicResponse; }
	 */

	@Override
	public BasicResponse changeProductStatus(long productId, int status) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = inputDao.changeProductStatus(productId, status);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@Override
	public BasicResponse changeShipppingStatus(long shippingId, int status) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = inputDao.changeShipppingStatus(shippingId, status);
		basicResponse = new BasicResponse();
		basicResponse.errorCode = (int) map.get(ERROR_CODE);
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<Long, String> getAllCountry(int status) { List<Country>
	 * placeList = null; Map<Long, String> allplaceMap = null; Map<String, Object>
	 * map = inputDao.getAllCountry(0, status); placeList = (List<Country>)
	 * map.get("CountryList"); if (placeList != null && !placeList.isEmpty()) {
	 * allplaceMap = new HashMap<>(); for (Country c : placeList) {
	 * allplaceMap.put(c.getCountryId(), c.getCountryName()); }
	 * 
	 * } return allplaceMap; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, String[]> getAllCountry(int status) {
		List<Country> placeList = null;
		Map<Long, String[]> allplaceMap = null;
		Map<String, Object> map = inputDao.getAllCountry(0, status);
		placeList = (List<Country>) map.get("CountryList");
		if (placeList != null && !placeList.isEmpty()) {
			allplaceMap = new HashMap<>();
			for (Country c : placeList) {
				String[] str = { c.getCountryName(), c.getCountryNameAr() };
				allplaceMap.put(c.getCountryId(), str);
			}

		}
		return allplaceMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, String[]> getAllCities(long countryId, int status) {
		List<City> placeList = null;
		Map<Long, String[]> allplaceMap = null;
		Map<String, Object> map = inputDao.getAllCity(countryId, 0, "", 0, 0, status);
		placeList = (List<City>) map.get("CityList");
		if (placeList != null && !placeList.isEmpty()) {
			allplaceMap = new HashMap<>();
			for (City c : placeList) {
				String[] str = { c.getCityName(), c.getCityNameAr() };
				allplaceMap.put(c.getCityId(), str);
			}

		}
		return allplaceMap;
	}

	@Override
	public BasicResponse getAllCity(long countryId, long cityId, String cityName, int firstIndex, int rowPerPage,
			int status) {

		BasicResponse res = null;
		List<Country> placeList = null;
		List<CityVo> cityVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = inputDao.getAllCity(countryId, cityId, cityName, firstIndex, rowPerPage, status);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Long, String[]> countryMap = getAllCountry(1);
			@SuppressWarnings("unchecked")
			List<City> citylist = (List<City>) dataMap.get("CityList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			CityVo cityvo = null;
			CountryVo cVo = null;
			for (City city : citylist) {
				cityvo = modelMapper.map(city, CityVo.class);
				Map<String, Object> map = inputDao.getAllCountry(cityvo.getCountryId(), status);
				placeList = (List<Country>) map.get("CountryList");
				for (Country c : placeList) {
					cVo = modelMapper.map(c, CountryVo.class);
					cityvo.setCountryName(cVo.getCountryName());
				}

				cityVos.add(cityvo);
			}
			returnMap.put("City", cityVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = cityVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse getAllOffer(long offerId, long offerEnds, long offerStarts, int status, String offerName,
			String offerNameAr, int firstIndex, int rowPerPage) {
		BasicResponse res = null;

		List<OfferVo> offVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = inputDao.getAllOffer(offerId, offerEnds, offerStarts, status, offerName,
				offerNameAr, firstIndex, rowPerPage);

		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Long, String[]> countryMap = getAllCountry(1);

			@SuppressWarnings("unchecked")
			List<Offer> citylist = (List<Offer>) dataMap.get("CityList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			OfferVo ofVo = null;
			for (Offer off : citylist) {
				ofVo = modelMapper.map(off, OfferVo.class);
				ofVo.setOfferEnds(off.getOfferEnds());
				logger.info(off.getStatus() + "0000000000000000000000000000000000000");
				ofVo.setStatus(off.getStatus());
				offVos.add(ofVo);
			}
			returnMap.put("City", offVos);
			/*
			 * if (orderId > 0) { returnMap.put("Order", orderVos.get(0)); List<Review>
			 * reviews = (List<Review>) dataMap.get("Reviews"); List<ReviewVo> reviewVos =
			 * new ArrayList<>(); ReviewVo revVo = null;
			 * 
			 * for (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class); //
			 * ordervo.setLastUpdatedTime(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); reviewVos.add(revVo); } returnMap.put("REVIEWS",
			 * reviewVos); }
			 */
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = offVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse getAttributes(String attributeName, String attributeNameAr, int firstIndex, int rowPerPage,
			int status) {
		BasicResponse res = null;
		List<GetproductAttributeVo> getpVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = inputDao.getAttributes(attributeName, attributeNameAr, firstIndex, rowPerPage,
				status);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			@SuppressWarnings("unchecked")
			List<Attributes> attributeList = (List<Attributes>) dataMap.get("ATTRIBUTES");
			long totalCounts = (long) dataMap.get("TotalCounts");
			logger.info(totalCounts + "totalCounts---------------------------------------------------------2");
			GetproductAttributeVo getpVo = null;
			for (Attributes attributes : attributeList) {
				getpVo = modelMapper.map(attributes, GetproductAttributeVo.class);

				getpVos.add(getpVo);
			}
			returnMap.put("Attributes", getpVos);

			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = getpVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse getAllCms(long cmsId, int rowPerPage, int currentIndex) {
		BasicResponse res = null;

		List<CmsVo> offVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = inputDao.getAllCms(cmsId, currentIndex, rowPerPage);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {

			List<CmsMangement> citylist = (List<CmsMangement>) dataMap.get("CMSLIST");
			long totalCounts = (long) dataMap.get("TotalCounts");
			CmsVo cmcVo = null;
			for (CmsMangement off : citylist) {
				cmcVo = modelMapper.map(off, CmsVo.class);

				offVos.add(cmcVo);
			}
			returnMap.put("City", offVos);
			/*
			 * if (orderId > 0) { returnMap.put("Order", orderVos.get(0)); List<Review>
			 * reviews = (List<Review>) dataMap.get("Reviews"); List<ReviewVo> reviewVos =
			 * new ArrayList<>(); ReviewVo revVo = null;
			 * 
			 * for (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class); //
			 * ordervo.setLastUpdatedTime(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); reviewVos.add(revVo); } returnMap.put("REVIEWS",
			 * reviewVos); }
			 */
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = offVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse getAllOfferProduct(long offerId) {
		BasicResponse res = null;

		List<OfferVo> cityVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = inputDao.getAllOfferProduct(offerId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			Map<Long, String[]> countryMap = getAllCountry(1);
			@SuppressWarnings("unchecked")
			List<Offer> citylist = (List<Offer>) dataMap.get("CityList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			OfferVo cityvo = null;
			for (Offer city : citylist) {
				cityvo = modelMapper.map(city, OfferVo.class);

				cityVos.add(cityvo);
			}
			returnMap.put("City", cityVos);
			/*
			 * if (orderId > 0) { returnMap.put("Order", orderVos.get(0)); List<Review>
			 * reviews = (List<Review>) dataMap.get("Reviews"); List<ReviewVo> reviewVos =
			 * new ArrayList<>(); ReviewVo revVo = null;
			 * 
			 * for (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class); //
			 * ordervo.setLastUpdatedTime(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); reviewVos.add(revVo); } returnMap.put("REVIEWS",
			 * reviewVos); }
			 */
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = cityVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse getallManufacture(long manufactureId, String manufactureName, int status, int firstIndex,
			int rowPerPage, long companyId) {

		BasicResponse res = null;

		List<ManufactureVo> manufactureVos = new ArrayList<>();
		Map<String, Object> returnMap = new HashMap<>();

		Map<String, Object> dataMap = inputDao.getallManufacture(manufactureId, manufactureName, status, firstIndex,
				rowPerPage, companyId);
		if ((int) dataMap.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			// Map<Long, String[]> countryMap = getAllCountry(1);
			@SuppressWarnings("unchecked")
			List<Manufacture> ManufactureList = (List<Manufacture>) dataMap.get("ManufactureList");
			long totalCounts = (long) dataMap.get("TotalCounts");
			ManufactureVo manufactureVo = null;
			for (Manufacture manufacture : ManufactureList) {
				manufactureVo = modelMapper.map(manufacture, ManufactureVo.class);
				/*
				 * if (countryMap != null) { //String names[] =
				 * countryMap.get(manufacture.getCountryId());
				 * //cityvo.setCountryName(names[0]); //cityvo.setCountryNameAr(names[1]); }
				 */
				manufactureVos.add(manufactureVo);
			}
			returnMap.put("Manufacture", manufactureVos);
			/*
			 * if (orderId > 0) { returnMap.put("Order", orderVos.get(0)); List<Review>
			 * reviews = (List<Review>) dataMap.get("Reviews"); List<ReviewVo> reviewVos =
			 * new ArrayList<>(); ReviewVo revVo = null;
			 * 
			 * for (Review re : reviews) { revVo = modelMapper.map(re, ReviewVo.class); //
			 * ordervo.setLastUpdatedTime(TimeUtils.instance().getDateStrFromMilSec(re.
			 * getLastUpdatedTime())); reviewVos.add(revVo); } returnMap.put("REVIEWS",
			 * reviewVos); }
			 */
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
			res.object = manufactureVos;
			res.totalRecords = totalCounts;
		} else {
			res = new BasicResponse();
			res.errorCode = (int) dataMap.get(ERROR_CODE);
			res.errorMessage = (String) dataMap.get(ERROR_MESSAGE);
			res.errorMessageArabic = (String) dataMap.get(ERROR_MESSAGE_ARABIC);
		}
		return res;
	}

	@Override
	public BasicResponse readNotification(long userId)

	{
		BasicResponse basicResponse = null;
		Map<String, Object> map = inputDao.readNotification(userId);
		logger.info((int) map.get(ERROR_CODE)
				+ "(int) map.get(ERROR_CODE)________________________(int) map.get(ERROR_CODE)");
		if ((int) map.get(ERROR_CODE) == -3) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = "UPDATE FAILED";
			basicResponse.errorMessageArabic = "UPDATE FAILED";
			basicResponse.totalRecords = 0;
			return basicResponse;
		}
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			return basicResponse;

		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@Override
	public BasicResponse getAllCountry(long countryId, int status) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = inputDao.getAllCountry(countryId, status);
		if ((int) map.get(ERROR_CODE) == -3) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = MESSAGE_NO_DATA_TO_SHOW;
			basicResponse.errorMessageArabic = MESSAGE_NO_DATA_TO_SHOW_AR;
			basicResponse.totalRecords = 0;
			return basicResponse;
		}
		long totalCounts = (long) map.get("TotalCounts");
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			basicResponse.totalRecords = totalCounts;
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		basicResponse.totalRecords = totalCounts;
		@SuppressWarnings("unchecked")
		List<Country> countryList = (List<Country>) map.get("CountryList");
		List<CountryVo> countryVoList = new ArrayList<>();
		CountryVo countryVo = null;
		for (Country cat : countryList) {
			countryVo = modelMapper.map(cat, CountryVo.class);
			// categoryVo.setCreatedDateString(TimeUtils.instance().getDateStrFromMilSec(cat.getCreatedDate()));
			// categoryVo.setLastUpdatedTimeString(TimeUtils.instance().getDateStrFromMilSec(cat.getLastUpdatedTime()));
			// String base64String = Base64.encodeBase64String(cat.getCountryFlag());
			// countryVo.setFlag(base64String);
			// logger.info(countryVo.getFlag());
			countryVoList.add(countryVo);
		}
		if (countryId > 0) {
			basicResponse.object = countryVoList.get(0);
		} else {
			basicResponse.object = countryVoList;
		}
		return basicResponse;
	}

	@Override
	public BasicResponse saveOrUpdateAttribute(AttributeVo attributeVo) {
		BasicResponse basicResponse = new BasicResponse();
		Attributes attributes = modelMapper.map(attributeVo, Attributes.class);
		attributes.setCreatedDate(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
		Map<String, Object> map = inputDao.saveOrUpdateAttribute(attributes);
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		return basicResponse;
	}

	@Override
	public BasicResponse getAllAttribute(long attributeId, int status) {
		BasicResponse res = new BasicResponse();
		Map<String, Object> map = inputDao.getAllAttriubes(attributeId, status);

		res.errorCode = (int) map.get(ERROR_CODE);
		res.errorMessage = (String) map.get(ERROR_MESSAGE);
		res.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		if ((int) map.get(ERROR_CODE) == ERROR_CODE_NO_ERROR) {
			map.remove(ERROR_CODE);
			map.remove(ERROR_MESSAGE);
			map.remove(ERROR_MESSAGE_ARABIC);
			res.object = map.get(DATA);
		}
		return res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicResponse getAllAttributevalues(long attributeId, int status, int currentIndex, int rowPerPage) {
		BasicResponse basicResponse = null;
		Map<String, Object> map = inputDao.getAllAttributevalues(attributeId, status, currentIndex, rowPerPage);
		long totalCounts = (long) map.get("TotalCounts");
		if ((int) map.get(ERROR_CODE) != ERROR_CODE_NO_ERROR) {
			basicResponse = new BasicResponse();
			basicResponse.errorCode = 2;
			basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
			basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
			basicResponse.totalRecords = totalCounts;
			return basicResponse;
		}
		basicResponse = new BasicResponse();
		basicResponse.errorCode = ERROR_CODE_NO_ERROR;
		basicResponse.errorMessage = (String) map.get(ERROR_MESSAGE);
		basicResponse.errorMessageArabic = (String) map.get(ERROR_MESSAGE_ARABIC);
		basicResponse.totalRecords = totalCounts;
		@SuppressWarnings("unchecked")
		List<ProductAttributes> countryList = (List<ProductAttributes>) map.get("ProudctList");

		logger.info(countryList.size()
				+ "countryList----------------------------------------------------------------------------");
		List<GetproductAttributeVo> countryVoList = new ArrayList<>();
		GetproductAttributeVo countryVo = null;
		for (ProductAttributes cat : countryList) {
			countryVo = modelMapper.map(cat, GetproductAttributeVo.class);

			countryVoList.add(countryVo);
		}
		logger.info(countryVoList.size()
				+ "countryVoList-----------------------88-----------------------------------------------------");

		basicResponse.object = countryVoList;

		return basicResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, String[]> getAllAttribute(int status) {
		List<Attributes> attributesList = null;
		Map<Long, String[]> attributeMap = null;
		Map<String, Object> map = inputDao.getAllAttriubes(0, status);
		
		attributesList = (List<Attributes>) map.get(DATA);
		if (attributesList != null && !attributesList.isEmpty()) {
		
			attributeMap = new HashMap<>();
			String[] name = null;
			for (Attributes c : attributesList) {
				name = new String[2];
				name[0] = c.getAttributeName();
				name[1] = c.getAttributeNameAr();
				
				attributeMap.put(c.getAttributeId(), name);
			}

		}

		return attributeMap;
	}

}
