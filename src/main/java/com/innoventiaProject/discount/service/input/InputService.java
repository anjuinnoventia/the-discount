package com.innoventiaProject.discount.service.input;

import java.util.List;
import java.util.Map;

import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.discount.model.input.AddVo;
import com.innoventiaProject.discount.model.input.AttributeVo;
import com.innoventiaProject.discount.model.input.CategoryVo;
import com.innoventiaProject.discount.model.input.CityVo;
import com.innoventiaProject.discount.model.input.CmsVo;
import com.innoventiaProject.discount.model.input.CountryVo;
import com.innoventiaProject.discount.model.input.ManufactureVo;
import com.innoventiaProject.discount.model.input.OfferVo;
import com.innoventiaProject.discount.model.input.ProductVo;
import com.innoventiaProject.discount.model.input.ReviewVo;
import com.innoventiaProject.discount.model.input.SubCategoryVo;



public interface InputService {
	 
     public BasicResponse saveProduct(ProductVo serviceVo);
     public Map<Long,String> getAllCitiesAr(long countryId,int status);
 	public Map<Long, String> getAllCountryAr(int status);
 	 public Map<Long,String> getAllCities1(long countryId,int status);
  	public Map<Long, String> getAllCountry1(int status);
    // public BasicResponse getAllShopsBySellerId(long sellerId, int firstIndex, int rowPerPage);
     public BasicResponse saveCategory(CategoryVo categoryVo);
     public BasicResponse  saveCms(CmsVo CMSvo);
     public BasicResponse svaeManufacture(ManufactureVo manufactureVo,int r);
     public Map<Long, String[]> getAllCities(long countryId, int status);
     public Map<Long, String[]> getAllCountry(int status);
     public BasicResponse saveOrUpdateAdd(AddVo addVo);
     public BasicResponse changeReturnStatus(long returnItemId,int status);
     public BasicResponse getAllAdds(int addStatus,long addId,long storeUserId,long  modeId,long arabicOrEnglish);
     //public Map<Long,String> getAllCountry();
	 public BasicResponse saveSubCategory(SubCategoryVo categoryVo);
	 public BasicResponse addOffer(OfferVo offerVo);
	 public BasicResponse getAllReviews(long fatherId,long cityId,long countryId,long customerId,long sellerId,int currentIndex,int rowPerPage,long fromDate,long toDate);
//	 public BasicResponse saveMainCategory(CategoryVo categoryVo);
	 //public BasicResponse getAllReviews(long fatherId,int productOrshop,int firstIndex,int rowPerPage);
	 public BasicResponse updateCategory(CategoryVo categoryVo);
	 public BasicResponse getAllCategoryFromId(long categoryId);
	 public BasicResponse getAllProduct(long productId,long sellerId,long userId,long mainCategoryId,long categoryId,long subcategoryId,long countryId,long cityId,int firstIndex,int rowPerPage,int status,String productName,long companyId,long numberOfItemsRemaining,long offerId,long manufacturerId,double priceStarts,double priceEnds,long productAttributeId,String value,String productNameAr,int data,Map<Long, List<String>> attibutesSearch,int featured,int rank);
//	 public BasicResponse getAllMainCategoryFromId();
	 public BasicResponse getProductswithoutOffer(int rowPerPage,int firstIndex);
//	 public BasicResponse getAllCategoryFromMainCatgoryId(long mainCatId);
	 public BasicResponse getAllSubcategories(long mainCategoryId,long categoryId,long companyId);
	
	 public BasicResponse saveReview(ReviewVo reviewVo);
	 public BasicResponse  addComments(String comments,long reviewId);
	// public BasicResponse getOffer(long  offerId);
	 public BasicResponse changeProductStatus(long productId,int status);
	 public BasicResponse changeShipppingStatus(long shippingId,int status);
//	 public BasicResponse getAllItemRequest(long requestItemId,long userId);
	 public BasicResponse saveOrUpdateCountry(CountryVo countryVo);
	 public BasicResponse saveOrUpdateCity(CityVo cityVo);
	//public BasicResponse requestItems(RequestItemsVo requestItemsVo);
	// public BasicResponse getAllProduct(long productId,long sellerId,long userId,long mainCategoryId,long categoryId,long subcategoryId,long countryId,long cityId,int firstIndex,int rowPerPage);
	 public Map<Long, String> getAllSubCategoryNames();
	 public Map<Long, String> getAllSubCategoryNamesAr();
	 public Map<Long, String> getAllOffers();
	 public Map<Long, Long> getAllOffers1();
	 public Map<Long, String> getAllOffersAr();
	 public Map<Long, String> getallManufactures();
	 public Map<Long, String> getallManufacturesAr();
	// public Map<Long, String> getAllMainCategoryNames();
	public Map<Long, String> getAllCategoryNames();
	public Map<Long, String> getAllCustomerNameAr();
	public Map<Long, String> getAllCustomerName();
	public Map<Long, String> getAllCategoryNamesAr();
	public Map<Long, String> getAllBusinessNames();
   // public Map<Long,String> getAllCountry(int status);
//	public BasicResponse getAllCountry();
 //   public BasicResponse getAllStates();
//    public BasicResponse getAllCities();
	
 //   public BasicResponse saveOrUpdateCategory(CategoryVo categoryVo);
    
//	public BasicResponse getAllShops(long storeId,long userId,int firstIndex,int rowPerPage);
    
	// public BasicResponse saveShop(StoreVo shopVo);
	 public BasicResponse getAllCategory(long categoryId,long mainCategoryId,int status,long companyId);
   // public BasicResponse getAllProduct(long productId,long shopId,long userId,long mainCategoryId,long categoryId,long subcategoryId,int firstIndex,int rowPerPage);
    //public BasicResponse getAllShops(long shopId,long categoryId,long subcategoryId,int firstIndex,int rowPerPage,long fromDate,long toDate);
	
	 public BasicResponse getAllProductsByUserId(long userId,int firstIndex,int rowPerPage);
    
   
	//public BasicResponse saveLikeOrShare(LikeAndShareVO likesOrshares);
	
 //   public BasicResponse deleteReview(long reviewId,int subOrMain,long userId);
	
	 public BasicResponse getAllDetails(long userId,long id,int prodOrShop);
	 public BasicResponse updateStock(long productId,long productQuantity);

	public BasicResponse searchData(Map<Integer, String> data, int mode, int firstResult, int rowPerPage, double latitude,
			double longitude);
	public BasicResponse filterData( int mode, int firstResult, int rowPerPage,long categoryId);

	public Map<Integer, String> getAllStatusMap();
	/*public Map<Long,String> getAllCities(long countryId,int status);
    public Map<Long,String> getAllCountry(int status);*/
    public BasicResponse getAllCity(long countryId, long cityId,String cityName, int firstIndex, int rowPerPage,int status);
    
    public BasicResponse getAllOffer(long offerId,long offerEnds, long offerStarts,int status,String offerName,String offerNameAr,int firstIndex, int rowPerPage);
    public BasicResponse getAttributes(String attributeName,String attributeNameAr,int firstIndex, int rowPerPage,int status);
    public BasicResponse getAllCms(long cmsId,int rowPerPage,int currentIndex);
    public BasicResponse getallManufacture(long manufactureId, String manufactureName,int status, int firstIndex, int rowPerPage,long companyId);
    public BasicResponse getAllCountry(long countryId,int status);
    public BasicResponse  readNotification(long userId);
    public BasicResponse getAllOfferProduct(long offerId);
 //   public BasicResponse saveRequestItem(RequestItemsVo requestItemsVo);
    
  //  public BasicResponse updateRequestItem(long requestId,int status);
    public BasicResponse saveOrUpdateAttribute(AttributeVo attributeVo);
    public BasicResponse getAllAttribute(long attributeId,int status);
    public BasicResponse getAllAttributevalues(long attributeId,int status,int currentIndex,int rowPerPage);
    public Map<Long, String[]> getAllAttribute(int status);
   
}
