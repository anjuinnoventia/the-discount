package com.innoventiaProject.discount.service.account;

import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.discount.model.account.ExpenseVo;
import com.innoventiaProject.discount.model.account.IncomeExpenseHeadVo;
import com.innoventiaProject.discount.model.account.IncomeVo;

public interface AccountService {

	BasicResponse saveorUpdateIncomeExpenseHead(IncomeExpenseHeadVo incExpHedModel);

	BasicResponse getIncomeExpenseHead(int incomeOrExpense, long incomExpnsHeadId, int status);

	BasicResponse saveorUpdateIncome(IncomeVo incomeModel);

	BasicResponse saveorUpdateExpense(ExpenseVo expenseModel);

	BasicResponse getIncomes(int currentIndex, int rowPerPage, long incomeId, int status);

	BasicResponse getExpenses(int currentIndex, int rowPerPage, long expenseId, int status);

}
