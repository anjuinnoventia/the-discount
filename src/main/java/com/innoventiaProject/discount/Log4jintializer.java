package com.innoventiaProject.discount;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.PropertyConfigurator;

//@WebListener("log4jlistner")
public class Log4jintializer implements ServletContextListener{
	@Override
	public void contextInitialized(ServletContextEvent event) {
		// initialize log4j here
		ServletContext context = event.getServletContext();
		String fullPath = context.getRealPath("") + File.separator + "WEB-INF/log4j.xml";
		
		PropertyConfigurator.configure(fullPath);
		
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// TODO Auto-generated method stub
		
	}
}
