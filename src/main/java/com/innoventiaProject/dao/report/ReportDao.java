package com.innoventiaProject.dao.report;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.entity.order.Order;
import com.innoventiaProject.entity.order.OrderDetails;

public interface ReportDao {
  public Map<String, Object> getSubscriptionDays(StringBuffer str,long fromDate,long toDate,Object data[],int mode,int status);

  public Map<String, Object> getBookingDays(StringBuffer str, long fromDate, long toDate, Object data[], int mode, int status);
	public Map<String,Object> addOrder(Map<Long, List<OrderDetails>> orMap,Map<Long, Order> orders,Map<String, Object> tranMaps,Map<String, Object> onlineMap,Map<String, Object> commissinMap);
  //public Map<String, Object> getAllOrder(long orderId, long sellerId, long customerId,int status[], long fromDate, long toDate, long deliveryDate,long orderListId, int firstIndex, int rowPerPage,long companyId);
  
	public Map<String,Object> updateOrder(long orderId,String paymentId,int paymentStatus);
	
	
	
	
	public Map<String, Object> getAllOrder(long orderId,long storeId,long customerId, int firstIndex, int rowPerPage,int status[],long fromDate,long toDate,long deliveryDate,int  serviceOrProduct,long deliveryBoyId,long orderListId)throws IOException;
  //public Map<String,Object> getAllAuctionDetails(long auctionId,long customerId,long auctionDetaiilsId,int firstIndex,int rowPerPage,long fromDate,long toDate);
  public Map<String, Object> getAllAdd(int status[], int firstIndex, int rowPerPage,long companyId,long modeId,long modeIds[],long arabicOrEnglish,int stat);
  public Map<String, Object> getAllSalesReport(long storeId,long customerId,int firstIndex, int rowPerPage,long deliveryBoyId,long categoryId,long productId,long sellerId,long fromDate, long toDate,long subCategoryId,int modeType);
  public Map<String, Object> getAllOrdersDetails(long orderId,long orderListId,long customerId,long fromDate,long toDate,long sellerId,int firstIndex,int rowPerPage,long companyId);
//
  public Map<String,Object> getPromocodeReport(int firstIndex, int rowPerPage, long fromDate, long toDate, long codeId,long userId,long promoterId,long code);	
  public Map<String, Object> monthlyRevenueReport(int firstIndex, int rowPerPage, long fromDate, long toDate, long codeId); 
  public Map<String, Object> yearlyRevenueReport(int firstIndex, int rowPerPage, long fromDate, long toDate, long codeId);
  public Map<String, Object> bestPurchasedProducts(long orderId,long orderListId,long customerId,long fromDate,long toDate,long sellerId,int firstIndex,int rowPerPage,long companyId, long categoryId,long subCategoryId);
  public Map<String, Object>  weeklyRevenueReport(int firstIndex, int rowPerPage, long fromDate, long toDate, long codeId);
  //public Map<String,Object>  getAllAuctionBy(long auctionId,long auctionDetailsId,long customerId,int firstIndex, int rowPerPage,int status[],long fromDate,long toDate);
  public Map<String,Object> getAllProducts(long productId,long sellerId,long userId,long mainCategoryId,long categoryId,long subCategoryId,long countryId,long cityId,int firstIndex,int rowPerPage,int status,long companyId,long numberOfItemsRemaining,long offerId,long manufacturerId,double priceStarts,double priceEnds,long productAttributeId); 	
  public Map<String, Object> getAllDetails(long userId, int roleId);
 // public Map<String,Object> productPurchaseReport(int firstIndex,int rowPerPage); 
  public Map<String,Object> lowStockReport(int firstIndex,int rowPerPage,int status,long numberOfItemsRemaining,long numberStarts,long numberEnds);  
  public Map<String,Object>  commissionReport(int firstIndex,int rowPerPage,int status,long lastUpdatedTime,long orderId,long userId, double priceStarts,double priceEnds, long fromDate, long toDate,int transactionMode,int tMode[]);
  public  Map<String,Object> taxReport(int firstIndex,int rowPerPage,int status,long lastUpdatedTime,long orderId, double priceStarts,double priceEnds, long fromDate, long toDate,long taxId);
  public  Map<String,Object> transactionReport(int firstIndex,int rowPerPage,int status,long lastUpdatedTime,long orderId,long userId, double priceStarts,double priceEnds, long fromDate, long toDate,int transactionMode,int tMode[],int iNoR[],int bankOrCash[]);
  public  Map<String,Object>  incomeExpenceReport(int firstIndex,int rowPerPage,int status,long lastUpdatedTime,long orderId,long userId, double priceStarts,double priceEnds, long fromDate, long toDate,int transactionMode,int tMode[]);
  
  public Map<String,Object>  getAllTransBy(long mode,long fromDate,long toDate,long toWhoId,int cashType,int tranType,int currentIndex,int rowPerPage);
}
