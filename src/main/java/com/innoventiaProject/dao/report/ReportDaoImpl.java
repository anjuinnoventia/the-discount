package com.innoventiaProject.dao.report;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.DoubleType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.utils.TimeUtils;
import com.innoventiaProject.entity.input.Advertisement;
import com.innoventiaProject.entity.input.ConnectedProducts;
import com.innoventiaProject.entity.input.FeaturesAndPhotosProduct;
import com.innoventiaProject.entity.input.Product;
import com.innoventiaProject.entity.input.Review;
import com.innoventiaProject.entity.input.SellerDetails;
import com.innoventiaProject.entity.order.Order;
import com.innoventiaProject.entity.order.OrderDetails;
import com.innoventiaProject.entity.order.Transactions;
import com.innoventiaProject.entity.user.NotificationDetails;
import com.innoventiaProject.entity.user.PromocodeDetails;

@Repository("reportDao")
public class ReportDaoImpl implements ReportDao, Constants {
	private static final Logger logger = Logger.getLogger(ReportDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HibernateTemplate hibernateTemplate;

	protected Session session;

	private Session openDBsession() {
		Session session;
		session = sessionFactory.getCurrentSession();
		return session;
	}

	private void closeDBSession() {
		if (session.isOpen()) {
			session.flush();
			session.close();
		}
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> getAllOrder(long orderId, long
	 * sellerId, long customerId, int status[], long fromDate, long toDate, long
	 * deliveryDate, long orderListId, int firstIndex, int rowPerPage,long
	 * companyId) { Map<String, Object> map = null; List<Order> data = null;
	 * List<Review> reviewList = null; List<Transactions> transactionList =
	 * null; long totalCounts = 0; try { Session session = openDBsession();
	 * Criteria criteria = session.createCriteria(Order.class); Criteria
	 * criteria2 = criteria.createCriteria("orderDetails"); boolean flag =
	 * false; StringBuffer strBfr = new
	 * StringBuffer("select count(*) from Order o ");
	 * logger.info(strBfr+"strBfr-----------------------6---------------------")
	 * ; if (orderListId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" "); } else { strBfr.append(" and"); }
	 * strBfr.append(" inner join o.orderDetails od where od.orderListId=" +
	 * orderListId); criteria2.add(Restrictions.eq("orderListId", orderListId));
	 * }
	 * 
	 * 
	 * if (orderId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" o.orderId=" + orderId);
	 * criteria.add(Restrictions.eq("orderId", orderId));
	 * 
	 * }
	 * 
	 * if (deliveryDate > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" o.deliveryDate=" + deliveryDate);
	 * criteria.add(Restrictions.eq("deliveryDate", deliveryDate)); }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * if (fromDate > 0) { if (toDate == 0) { toDate =
	 * TimeUtils.instance().getEndTimeOfGiveTime(fromDate); } if (flag == false)
	 * { flag = true; strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" o.lastUpdatedTime between " + fromDate + " and " +
	 * toDate); criteria.add(Restrictions.between("lastUpdatedTime", fromDate,
	 * toDate)); } if (customerId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" o.customerId=" + customerId);
	 * criteria.add(Restrictions.eq("customerId", customerId)); }
	 * 
	 * 
	 * 
	 * List<Integer> statAry = null; if (status != null && status.length > 0) {
	 * if (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } Object[] objAr = new Object[status.length];
	 * statAry = new ArrayList<>(); for (int i = 0; i < status.length; i++) {
	 * objAr[i] = status[i]; statAry.add(status[i]); }
	 * criteria.add(Restrictions.in("orderstatus", objAr));
	 * 
	 * strBfr.append(" o.orderstatus in (:stat)"); } Query qry =
	 * session.createQuery(strBfr.toString());
	 * logger.info(qry+"qry-----------------------@@@---------------------");
	 * if(statAry != null)
	 * {logger.info("not null---------------------------------()");
	 * qry.setParameterList("stat", statAry); } totalCounts = (long)
	 * qry.uniqueResult(); logger.info(totalCounts+
	 * "totalCounts--------------------------------------------8");
	 * session.flush();
	 * criteria.setProjection(Projections.distinct(Projections.id())); if
	 * (rowPerPage > 0) { criteria.setMaxResults(rowPerPage);
	 * criteria.setFirstResult(firstIndex); }
	 * 
	 * 
	 * List<Long> ids = criteria.list(); session.flush();
	 * logger.info(firstIndex); if (ids != null && !ids.isEmpty()) { criteria =
	 * session.createCriteria(Order.class); criteria.add(Restrictions.in("id",
	 * ids)); criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	 * criteria.addOrder(Order.desc("lastUpdatedTime")); data = criteria.list();
	 * session.flush();
	 * 
	 * } if(data != null) { for(Order order:data) { logger.info(order.get); } }
	 * if (orderId > 0) { criteria = session.createCriteria(Transactions.class);
	 * criteria.add(Restrictions.eq("pairedId", orderId));
	 * 
	 * transactionList = criteria.list(); logger.info(transactionList.size() +
	 * "size of trans list"); session.flush(); }
	 * 
	 * } catch (HibernateException e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } logger.info(e.getMessage()); map = new
	 * HashMap<>(); map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE,
	 * e.getMessage()); return map; } if (data != null && !data.isEmpty()) { map
	 * = new HashMap<>(); map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC, "نجاح");
	 * map.put("OrderList", data); map.put("Reviews", reviewList);
	 * map.put("TotalCounts", totalCounts); return map;
	 * 
	 * } else { map = new HashMap<>(); map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS); return map; } }
	 */
	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String,Object> productPurchaseReport(int
	 * firstIndex,int rowPerPage) { Map<String, Object> map = null;
	 * List<Product> data = null; List<Review> reviewList = null;
	 * 
	 * List<ConnectedProducts> connectedList = null;
	 * 
	 * long totalCounts = 0; try { Session session = openDBsession(); Criteria
	 * criteria = session.createCriteria(OrderDetails.class);
	 * 
	 * boolean flag = false; StringBuffer strBfr = new
	 * StringBuffer("select count(productId) from OrderDetails GROUP BY productId "
	 * ); logger.info(strBfr+
	 * "strBfr-------------------------------------------------------------------------------"
	 * ); Query qry = session.createQuery(strBfr.toString()); //totalCounts =
	 * (long) qry.uniqueResult(); logger.info(totalCounts);
	 * criteria.setProjection(Projections.distinct(Projections.id())); if
	 * (rowPerPage > 0) { criteria.setMaxResults(rowPerPage);
	 * criteria.setFirstResult(firstIndex); } List<Long> ids = criteria.list();
	 * session.flush(); logger.info(ids+
	 * "ids---------------------------------------------------------------------"
	 * ); if (ids != null && !ids.isEmpty()) { criteria =
	 * session.createCriteria(OrderDetails.class);
	 * criteria.add(Restrictions.in("id", ids));
	 * //criteria.addOrder(Order.asc("numberOfItemsRemaining"));
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	 * 
	 * data = criteria.list(); session.flush(); }
	 * 
	 * 
	 * } catch (HibernateException e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } logger.info(e.getMessage()); map = new
	 * HashMap<>(); map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE,
	 * e.getMessage()); throw e; } if (data != null && !data.isEmpty()) { map =
	 * new HashMap<>(); map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
	 * map.put("ProudctList", data); map.put("Reviews", reviewList);
	 * map.put("ConnectedProducts", connectedList); map.put("TotalCounts", 0);
	 * return map;
	 * 
	 * } else { map = new HashMap<>(); map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR); return map; }
	 * }
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllProducts(long productId, long sellerId, long userId, long mainCategoryId,
			long categoryId, long subCategoryId, long countryId, long cityId, int firstIndex, int rowPerPage,
			int status, long companyId, long numberOfItemsRemaining, long offerId, long manufacturerId,
			double priceStarts, double priceEnds, long productAttributeId) {
		Map<String, Object> map = null;
		List<Product> data = null;
		List<Review> reviewList = null;

		List<ConnectedProducts> connectedList = null;

		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Product.class);
			Criteria criteria2 = criteria.createCriteria("seller");
			Criteria criteria3 = criteria.createCriteria("proudctAttributes");
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Product p");

			if (userId > 0) {
				flag = true;
				strBfr.append(" inner join p.seller s where s.userId=" + userId);
				criteria2.add(Restrictions.eq("userId", userId));
			}

			logger.info(
					strBfr + "strBfr-------------------------------------------------------------------------------");
			if (countryId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" inner join p.seller s where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" s.countryId=" + countryId);
				criteria2.add(Restrictions.eq("countryId", countryId));
			}
			if (cityId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" inner join p.seller s where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" s.cityId=" + cityId);
				criteria2.add(Restrictions.eq("cityId", cityId));
			}
			if (productId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.productId=" + productId);
				criteria.add(Restrictions.eq("productId", productId));
			}
			if (offerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.offerId=" + offerId);
				criteria.add(Restrictions.eq("offerId", offerId));
			}
			if (manufacturerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.manufacturerId=" + manufacturerId);
				criteria.add(Restrictions.eq("manufacturerId", manufacturerId));
			}
			if (sellerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.sellerId=" + sellerId);
				criteria.add(Restrictions.eq("sellerId", sellerId));
			}
			if (companyId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.companyId=" + companyId);
				criteria.add(Restrictions.eq("companyId", companyId));
			}
			if (numberOfItemsRemaining > 0) {
				long toDate = 0;
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.numberOfItemsRemaining between   " + toDate + " and " + numberOfItemsRemaining);
				criteria.add(Restrictions.between("numberOfItemsRemaining", toDate, numberOfItemsRemaining));
			}
			if (categoryId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.categoryId=" + categoryId);
				criteria.add(Restrictions.eq("categoryId", categoryId));
			}
			if (priceStarts > 0 && priceEnds > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.price between " + priceStarts + " and " + priceEnds);
				criteria.add(Restrictions.between("price", priceStarts, priceEnds));
			}
			if (subCategoryId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.subCategoryId=" + subCategoryId);
				criteria.add(Restrictions.eq("subCategoryId", subCategoryId));
			}
			/*
			 * if (productName != null && !productName.equals("")) {
			 * logger.info(productName +
			 * "proudctName_________________________________________"); if (flag
			 * == false) { flag = true; strBfr.append(" where");
			 * 
			 * } else { strBfr.append(" and"); }
			 * strBfr.append(" lower(p.productName) like '%" +
			 * productName.toLowerCase() + "%'"); logger.info(strBfr +
			 * "_______________________________________________");
			 * criteria.add(Restrictions.ilike("productName", "%" + productName
			 * + "%"));
			 * 
			 * }
			 */

			/*
			 * if (productName.equals("")) { if (flag == false) { flag = true;
			 * strBfr.append(" where"); } else { strBfr.append(" and"); }
			 * strBfr.append(" p.productName=" + productName);
			 * criteria.add(Restrictions.eq("productName", productName)); }
			 */

			if (status == 1 || status == 0) {
				if (flag == false) {
					strBfr.append(" where p.status=" + status);
				} else {
					strBfr.append(" and p.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}

			logger.info(strBfr.toString());
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			logger.info(totalCounts);
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			logger.info(ids);
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Product.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			if (productId > 0) {
				criteria = session.createCriteria(Review.class);
				criteria.add(Restrictions.eq("fatherId", productId));
				criteria.add(Restrictions.eq("productOrShop", PRODUCT));
				reviewList = criteria.list();
				logger.info(reviewList.size() + "size of review list");
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("ProudctList", data);
			map.put("Reviews", reviewList);
			map.put("ConnectedProducts", connectedList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> lowStockReport(int firstIndex, int rowPerPage, int status, long numberOfItemsRemaining,
			long numberStarts, long numberEnds) {
		Map<String, Object> map = null;
		List<Product> data = null;
		List<Review> reviewList = null;

		List<ConnectedProducts> connectedList = null;

		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Product.class);
			Criteria criteria2 = criteria.createCriteria("seller");
			Criteria criteria3 = criteria.createCriteria("proudctAttributes");
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Product p");

			if (numberStarts > 0 && numberEnds > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.numberOfItemsRemaining between " + numberStarts + " and " + numberEnds);
				criteria.add(Restrictions.between("numberOfItemsRemaining", numberStarts, numberEnds));
			}

			if (status == 1 || status == 0) {
				if (flag == false) {
					strBfr.append(" where p.status=" + status);
				} else {
					strBfr.append(" and p.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}

			logger.info(strBfr.toString());
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			logger.info(totalCounts);
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			logger.info(ids);
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Product.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.addOrder(org.hibernate.criterion.Order.asc("numberOfItemsRemaining"));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("ProudctList", data);
			map.put("Reviews", reviewList);
			map.put("ConnectedProducts", connectedList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> commissionReport(int firstIndex, int rowPerPage, int status, long lastUpdatedTime,
			long orderId, long userId, double priceStarts, double priceEnds, long fromDate, long toDate,
			int transactionMode, int tMode[]) {
		Map<String, Object> map = null;
		List<Transactions> data = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Transactions.class);
			if (lastUpdatedTime > 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(lastUpdatedTime);

				int mYear = calendar.get(Calendar.YEAR);
				int mMonth = calendar.get(Calendar.MONTH);
				int mDay = calendar.get(Calendar.DAY_OF_MONTH);
				logger.info(mYear + "mYear---------------------------------------------------1");
				logger.info(mMonth + "mMonth---------------------------------------------------2");
				logger.info(mDay + "mDay---------------------------------------------------3");
			}
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Transactions p");

			if (orderId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				logger.info(strBfr + "strBfr------------------------------------------");
				strBfr.append(" p.pairedId=" + orderId);
				criteria.add(Restrictions.eq("pairedId", orderId));

			}

			if (transactionMode == TRAN_MODE_DEBIT_CASH_PAY || transactionMode == TRAN_MODE_DEBIT_ONLINE_PAY
					|| transactionMode == TRAN_MODE_COMMISSION_PAY) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where p.transactionMode=" + transactionMode);
				} else {
					strBfr.append(" and p.transactionMode=" + transactionMode);
				}
				criteria.add(Restrictions.eq("transactionMode", transactionMode));
			}

			if (userId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				logger.info(strBfr + "strBfr--------------------------------------------");
				strBfr.append(" p.toWhoId=" + userId);
				criteria.add(Restrictions.eq("toWhoId", userId));

			}

			logger.info(
					strBfr + "strBfr-------------------------------------------------------------------------------");

			if (priceStarts > 0 && priceEnds > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.netAmount between " + priceEnds + " and " + priceEnds);
				criteria.add(Restrictions.between("netAmount", priceEnds, priceEnds));
			}

			if (fromDate > 0 && toDate > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.lastUpdatedTime between " + fromDate + " and " + toDate);
				criteria.add(Restrictions.between("netAmount", fromDate, toDate));
			}

			if (status == 1 || status == 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where p.status=" + status);
				} else {
					strBfr.append(" and p.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}

			List<Integer> statAry = null;
			if (tMode != null && tMode.length > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				Object[] objAr = new Object[tMode.length];
				statAry = new ArrayList<>();
				for (int i = 0; i < tMode.length; i++) {
					objAr[i] = tMode[i];

					logger.info(statAry + "not null---------------------------------()");
					statAry.add(tMode[i]);
				}
				criteria.add(Restrictions.in("transactionMode", objAr));

				strBfr.append(" p.transactionMode in (:stat)");
			}

			logger.info(strBfr.toString());
			Query qry = session.createQuery(strBfr.toString());
			qry.setParameterList("stat", statAry);
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			logger.info(totalCounts);
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			logger.info(ids);
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Transactions.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.addOrder(org.hibernate.criterion.Order.desc("lastUpdatedTime"));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("TranList", data);

			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> taxReport(int firstIndex, int rowPerPage, int status, long lastUpdatedTime, long orderId,
			double priceStarts, double priceEnds, long fromDate, long toDate, long taxId) {
		Map<String, Object> map = null;
		List<OrderDetails> data = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(OrderDetails.class);

			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from OrderDetails p");

			if (taxId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				logger.info(strBfr + "strBfr--------------------------------------------");
				strBfr.append(" p.taxId=" + taxId);
				criteria.add(Restrictions.eq("taxId", taxId));

			}

			logger.info(
					strBfr + "strBfr-------------------------------------------------------------------------------");

			if (priceStarts > 0 && priceEnds > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.netAmount between " + priceEnds + " and " + priceEnds);
				criteria.add(Restrictions.between("netAmount", priceEnds, priceEnds));
			}

			if (fromDate > 0 && toDate > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.lastUpdatedTime between " + fromDate + " and " + toDate);
				criteria.add(Restrictions.between("lastUpdatedTime", fromDate, toDate));
			}

			if (status > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where p.status=" + status);
				} else {
					strBfr.append(" and p.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}

			logger.info(strBfr.toString());
			Query qry = session.createQuery(strBfr.toString());

			totalCounts = (long) qry.uniqueResult();
			session.flush();
			logger.info(totalCounts);
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			logger.info(ids);
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(OrderDetails.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.addOrder(org.hibernate.criterion.Order.desc("lastUpdatedTime"));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("TaxList", data);

			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> incomeExpenceReport(int firstIndex, int rowPerPage, int status, long lastUpdatedTime,
			long orderId, long userId, double priceStarts, double priceEnds, long fromDate, long toDate,
			int transactionMode, int tMode[]) {
		Map<String, Object> map = null;
		List<Transactions> data = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Transactions.class);
			if (lastUpdatedTime > 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(lastUpdatedTime);

				int mYear = calendar.get(Calendar.YEAR);
				int mMonth = calendar.get(Calendar.MONTH);
				int mDay = calendar.get(Calendar.DAY_OF_MONTH);
				logger.info(mYear + "mYear---------------------------------------------------1");
				logger.info(mMonth + "mMonth---------------------------------------------------2");
				logger.info(mDay + "mDay---------------------------------------------------3");
			}
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Transactions p");

			if (orderId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				logger.info(strBfr + "strBfr------------------------------------------");
				strBfr.append(" p.pairedId=" + orderId);
				criteria.add(Restrictions.eq("pairedId", orderId));

			}

			if (transactionMode == TRAN_MODE_INCOME || transactionMode == TRAN_MODE_EXPENSE) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where p.transactionMode=" + transactionMode);
				} else {
					strBfr.append(" and p.transactionMode=" + transactionMode);
				}
				criteria.add(Restrictions.eq("transactionMode", transactionMode));
			}

			if (userId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				logger.info(strBfr + "strBfr--------------------------------------------");
				strBfr.append(" p.toWhoId=" + userId);
				criteria.add(Restrictions.eq("toWhoId", userId));

			}

			logger.info(
					strBfr + "strBfr-------------------------------------------------------------------------------");

			if (priceStarts > 0 && priceEnds > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.netAmount between " + priceEnds + " and " + priceEnds);
				criteria.add(Restrictions.between("netAmount", priceEnds, priceEnds));
			}

			if (fromDate > 0 && toDate > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.lastUpdatedTime between " + fromDate + " and " + toDate);
				criteria.add(Restrictions.between("netAmount", fromDate, toDate));
			}

			if (status == 1 || status == 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where p.status=" + status);
				} else {
					strBfr.append(" and p.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}

			List<Integer> statAry = null;
			if (tMode != null && tMode.length > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				Object[] objAr = new Object[tMode.length];
				statAry = new ArrayList<>();
				for (int i = 0; i < tMode.length; i++) {
					objAr[i] = tMode[i];

					logger.info(statAry + "not null---------------------------------()");
					statAry.add(tMode[i]);
				}
				criteria.add(Restrictions.in("transactionMode", objAr));

				strBfr.append(" p.transactionMode in (:stat)");
			}

			logger.info(strBfr.toString());
			Query qry = session.createQuery(strBfr.toString());
			qry.setParameterList("stat", statAry);
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			logger.info(totalCounts);
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			logger.info(ids);
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Transactions.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.addOrder(org.hibernate.criterion.Order.desc("lastUpdatedTime"));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("TranList", data);

			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> transactionReport(int firstIndex, int rowPerPage, int status, long lastUpdatedTime,
			long orderId, long userId, double priceStarts, double priceEnds, long fromDate, long toDate,
			int transactionMode, int tMode[], int iNoR[], int bankOrCash[]) {
		Map<String, Object> map = null;
		List<Transactions> data = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Transactions.class);
			if (lastUpdatedTime > 0) {
				Calendar calendar = Calendar.getInstance();
				calendar.setTimeInMillis(lastUpdatedTime);

				int mYear = calendar.get(Calendar.YEAR);
				int mMonth = calendar.get(Calendar.MONTH);
				int mDay = calendar.get(Calendar.DAY_OF_MONTH);
				logger.info(mYear + "mYear---------------------------------------------------1");
				logger.info(mMonth + "mMonth---------------------------------------------------2");
				logger.info(mDay + "mDay---------------------------------------------------3");
			}
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Transactions p");

			if (orderId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				logger.info(strBfr + "strBfr------------------------------------------");
				strBfr.append(" p.pairedId=" + orderId);
				criteria.add(Restrictions.eq("pairedId", orderId));

			}

			if (transactionMode == TRAN_MODE_DEBIT_CASH_PAY || transactionMode == TRAN_MODE_DEBIT_ONLINE_PAY
					|| transactionMode == TRAN_MODE_COMMISSION_PAY) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where p.transactionMode=" + transactionMode);
				} else {
					strBfr.append(" and p.transactionMode=" + transactionMode);
				}
				criteria.add(Restrictions.eq("transactionMode", transactionMode));
			}

			if (userId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				logger.info(strBfr + "strBfr--------------------------------------------");
				strBfr.append(" p.toWhoId=" + userId);
				criteria.add(Restrictions.eq("toWhoId", userId));

			}

			logger.info(
					strBfr + "strBfr-------------------------------------------------------------------------------");

			if (priceStarts > 0 && priceEnds > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.netAmount between " + priceEnds + " and " + priceEnds);
				criteria.add(Restrictions.between("netAmount", priceEnds, priceEnds));
			}

			if (fromDate > 0 && toDate > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.lastUpdatedTime between " + fromDate + " and " + toDate);
				criteria.add(Restrictions.between("lastUpdatedTime", fromDate, toDate));
			}

			if (status == 1 || status == 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where p.status=" + status);
				} else {
					strBfr.append(" and p.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}

			List<Integer> statAry = null;
			if (tMode != null && tMode.length > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				Object[] objAr = new Object[tMode.length];
				statAry = new ArrayList<>();
				for (int i = 0; i < tMode.length; i++) {
					objAr[i] = tMode[i];

					logger.info(statAry + "not null---------------------------------()");
					statAry.add(tMode[i]);
				}
				criteria.add(Restrictions.in("transactionMode", objAr));

				strBfr.append(" p.transactionMode in (:stat)");
			}

			List<Integer> inArray = null;
			if (iNoR != null && iNoR.length > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				Object[] objAr = new Object[iNoR.length];
				inArray = new ArrayList<>();
				for (int i = 0; i < iNoR.length; i++) {
					objAr[i] = iNoR[i];

					logger.info(inArray + "not null---------------------------------()");
					inArray.add(iNoR[i]);
				}
				criteria.add(Restrictions.in("inOrOut", objAr));

				strBfr.append(" p.inOrOut in (:inOrOut)");
			}

			List<Integer> bankOrCashArray = null;
			if (bankOrCash != null && bankOrCash.length > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				Object[] objAr = new Object[bankOrCash.length];
				bankOrCashArray = new ArrayList<>();
				for (int i = 0; i < bankOrCash.length; i++) {
					objAr[i] = bankOrCash[i];

					logger.info(bankOrCashArray + "not null---------------------------------()");
					bankOrCashArray.add(bankOrCash[i]);
				}
				criteria.add(Restrictions.in("bankOrCash", objAr));

				strBfr.append(" p.bankOrCash in (:bankOrCash)");
			}

			logger.info(strBfr.toString());
			Query qry = session.createQuery(strBfr.toString());
			qry.setParameterList("stat", statAry);
			qry.setParameterList("inOrOut", inArray);//
			qry.setParameterList("bankOrCash", bankOrCashArray);
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			logger.info(totalCounts);
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = (List<Long>) criteria.list();
			session.flush();
			logger.info(ids);
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Transactions.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.addOrder(org.hibernate.criterion.Order.desc("lastUpdatedTime"));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("TranList", data);

			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@Override
	public Map<String, Object> updateOrder(long orderId, String paymentId, int paymentStatus) {
		Map<String, Object> map = null;
		List<Order> dvList = null;
		try {

			Session session = openDBsession();
			DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(Order.class);
			detachedCriteria2.add(Restrictions.eq("orderId", orderId));
			dvList = (List<Order>) hibernateTemplate.findByCriteria(detachedCriteria2);
			if (dvList == null || dvList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "Order " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}
			Query qry2 = session.createQuery(
					"update Order set paymentId=:paymentId,paymentStatus=:paymentStatus where orderId=:orderId");
			qry2.setParameter("orderId", orderId);
			qry2.setParameter("paymentId", paymentId);
			qry2.setParameter("paymentStatus", paymentStatus);
			qry2.executeUpdate();

		}

		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		map.put("amount", dvList.get(0).getNetPrice());
		/*
		 * if (result <= 0) { map.put(ERROR_CODE, -1); map.put(ERROR_MESSAGE,
		 * MESSAGE_UPDATE_FAILED); map.put(ERROR_MESSAGE_ARABIC,
		 * MESSAGE_UPDATE_FAILED_AR); } else { map.put(ERROR_CODE,
		 * ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR); }
		 */
		return map;
	}

	@Override
	public Map<String, Object> addOrder(Map<Long, List<OrderDetails>> orMap, Map<Long, Order> orders,
			Map<String, Object> tranMaps, Map<String, Object> onlineMap, Map<String, Object> commissinMap) {
		Map<String, Object> map = new HashMap<String, Object>();
		String output = null;
		try {
			InputStream is;

			Session session = openDBsession();

			NotificationDetails notifyDetails = new NotificationDetails();
			Transactions saletran = (Transactions) tranMaps.get("ORDER_TRAN");

			String q31111 = " select count(*) from  Transactions where status=1 ";
			Query q4211 = session.createQuery(q31111);
			long count = (long) q4211.uniqueResult();

			if (count > 0)

			{

				String q3111 = " select MAX(lastUpdatedTime) from  Transactions  where status=1 and transactionMode=20";
				Query q421 = session.createQuery(q3111);
				long lastUpdatedTimemax1 = (long) q421.uniqueResult();

				String q3112 = " select t.reciptNo from  Transactions t where t.lastUpdatedTime =:lastUpdatedTime and t.status=1 and t.transactionMode=20 ";
				Query q411 = session.createQuery(q3112);
				q411.setParameter("lastUpdatedTime", lastUpdatedTimemax1);
				String reciptNo1 = (String) q411.uniqueResult();
				// logger.info(reciptNo1 + "reciptNo1");
				StringBuilder sb = new StringBuilder(reciptNo1);

				sb.delete(0, 3);
				String reciptNo2 = sb.toString();
				// logger.info(reciptNo2 +
				// "reciptNo2__________________reciptNo2_________________reciptNo2");
				long rec = Long.parseLong(reciptNo2);

				String recp = Long.toString(rec + 1);

				saletran.setReciptNo("PNT" + recp);
			} else {
				saletran.setReciptNo("PNT1");
			}

			///

			session.save(saletran);
			session.flush();
			if (onlineMap.size() > 0 && onlineMap != null) {
				Transactions paymetTran = (Transactions) onlineMap.get("PAYMENT_TRAN");
				paymetTran.setReciptNo(saletran.getReciptNo());
				paymetTran.setPairedId(saletran.getTransactionId());
				paymetTran.setToWhoId(saletran.getToWhoId());
				session.save(paymetTran);
				session.flush();
			}
			if (commissinMap.size() > 0 && commissinMap != null) {

				Transactions commisiionTran = (Transactions) commissinMap.get("commissinMap");
				commisiionTran.setReciptNo(saletran.getReciptNo());
				commisiionTran.setPairedId(saletran.getTransactionId());
				session.save(commisiionTran);
				session.flush();
			}

			for (Entry<Long, Order> entry : orders.entrySet()) {
				Order order = entry.getValue();
				order.setOrderDetails(null);
				order.setTransactionId(saletran.getTransactionId());
				session.flush();

				session.save(order);

				// -------------------------added
				String q7 = "update Transactions p set p.pairedId =:pairedId where p.transactionId =:transactionId ";
				Query q4 = session.createQuery(q7);

				q4.setParameter("pairedId", order.getOrderId());
				q4.setParameter("transactionId", saletran.getTransactionId());

				long r2 = q4.executeUpdate();

				session.flush();
				// ----------------

				List<OrderDetails> orDetails = orMap.get(entry.getValue().getSellerId());
				for (OrderDetails or : orDetails) {
					or.setOrderId(order.getOrderId());
					or.setOrder(null);

					long item = or.getItemQuantity();

					String q2 = "select p.numberOfItemsRemaining from  Product p  where p.productId =:productId ";
					Query q1 = session.createQuery(q2);

					q1.setParameter("productId", or.getProductId());
					long original = (long) q1.uniqueResult();

					long remain = original - item;
					String q = "update Product p set p.numberOfItemsRemaining =:numberOfItemsRemaining where p.productId =:productId ";
					Query q3 = session.createQuery(q);

					q3.setParameter("numberOfItemsRemaining", remain);
					q3.setParameter("productId", or.getProductId());

					long r = q3.executeUpdate();
					logger.info(r + "--------------------------------------------------------");
					session.save(or);
					session.flush();
				}

				notifyDetails.setUserId(order.getCustomerId());
				notifyDetails.setDescription("ORDERED SUCCESSFULLY");
				notifyDetails.setLastUpdatedTime(TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE));
				notifyDetails.setOrderStatus(ORDERED);
				notifyDetails.setReadStatus(0);
				session.save(notifyDetails);
				session.flush();

			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			throw e;
		} /*
			 * catch (IOException e) { // TODO Auto-generated catch block
			 * e.printStackTrace(); }
			 */
		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, "نجاح");
		map.put("order", orders);
		// map.put("output", output);
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllOrder(long orderId, long storeId, long customerId, int firstIndex, int rowPerPage,
			int status[], long fromDate, long toDate, long deliveryDate, int serviceOrProduct, long deliveryBoyId,
			long orderListId) throws IOException {
		Map<String, Object> map = null;
		List<Order> data = null;
		List<Review> reviewList = null;
		List<Transactions> transactionList = null;
		long totalCounts = 0;
		try {

			Session session = openDBsession();
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Order o");
			logger.info(strBfr + "strBfr-----------------------6---------------------");
			if (orderListId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" ");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" inner join o.orderDetails od where od.orderListId=" + orderListId);
			}

			if (orderId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.orderId=" + orderId);

			}
			if (storeId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.storeId=" + storeId);
			}
			if (deliveryDate > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.deliveryDate=" + deliveryDate);
			}

			if (serviceOrProduct > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.serviceOrProduct=" + serviceOrProduct);
			}

			if (deliveryBoyId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.deliveryBoyId=" + deliveryBoyId);
			}

			if (fromDate > 0) {
				if (toDate == 0) {
					toDate = TimeUtils.instance().getEndTimeOfGiveTime(fromDate);
				}
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.lastUpdatedTime between " + fromDate + " and " + toDate);
			}
			if (customerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.customerId=" + customerId);
			}

			List<Integer> statAry = null;
			if (status != null && status.length > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				statAry = new ArrayList<>();
				for (int i = 0; i < status.length; i++) {
					statAry.add(status[i]);
				}
				strBfr.append(" o.orderstatus in (:stat)");
			}
			Query qry = session.createQuery(strBfr.toString());
			if (status != null && status.length > 0) {
				qry.setParameterList("stat", statAry);
			}
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			strBfr.append(" order by o.lastUpdatedTime desc");
			strBfr.replace(7, 15, "o");
			logger.info(strBfr);
			qry = session.createQuery(strBfr.toString());
			qry.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			qry.setFirstResult(firstIndex);
			if (rowPerPage > 0) {
				qry.setMaxResults(rowPerPage);
			}
			if (status != null && status.length > 0) {
				qry.setParameterList("stat", statAry);
			}
			data = qry.list();
			session.flush();
			if (orderId > 0) {
				Criteria criteria = session.createCriteria(Transactions.class);
				criteria.add(Restrictions.eq("pairedId", orderId));

				transactionList = criteria.list();
				logger.info(transactionList.size() + "size of trans list");
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");
			map.put("OrderList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@Override
	public Map<String, Object> getAllAdd(int status[], int firstIndex, int rowPerPage, long companyId, long modeId,
			long modeIds[], long arabicOrEnglish, int stat) {
		Map<String, Object> map = null;
		List<Advertisement> data = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Advertisement.class);

			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Advertisement o");

			if (companyId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.companyId=" + companyId);
				criteria.add(Restrictions.eq("companyId", companyId));
			}

			if (modeId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.modeId=" + modeId);
				criteria.add(Restrictions.eq("modeId", modeId));
			}

			if (arabicOrEnglish == 1 || arabicOrEnglish == 0) {
				if (flag == false) {
					strBfr.append(" where o.arabicOrEnglish=" + arabicOrEnglish);
					flag = true;
				} else {
					strBfr.append(" and o.arabicOrEnglish=" + arabicOrEnglish);
				}
				criteria.add(Restrictions.eq("arabicOrEnglish", arabicOrEnglish));
			}

			if (stat == 1 || stat == 0) {
				if (flag == false) {
					strBfr.append(" where o.status=" + stat);
					flag = true;
				} else {
					strBfr.append(" and o.status=" + stat);
				}
				criteria.add(Restrictions.eq("status", stat));
			}

			List<Integer> statAry = null;
			if (status.length > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				Object[] objAr = new Object[status.length];
				statAry = new ArrayList<>();
				for (int i = 0; i < status.length; i++) {
					objAr[i] = status[i];
					statAry.add(status[i]);
				}

				criteria.add(Restrictions.in("addStatus", objAr));
				strBfr.append(" o.addStatus in (:status)");
			}

			List<Long> modeArray = null;
			if (modeIds.length > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				Object[] objAr = new Object[modeIds.length];
				modeArray = new ArrayList<>();
				for (int i = 0; i < modeIds.length; i++) {
					objAr[i] = modeIds[i];
					modeArray.add(modeIds[i]);
				}

				criteria.add(Restrictions.in("modeId", objAr));
				strBfr.append(" o.modeId in (:modeIds)");
			}

			Query qry = session.createQuery(strBfr.toString());
			qry.setParameterList("status", statAry);
			qry.setParameterList("modeIds", modeArray);

			if (flag == true) {
				strBfr.replace(36, strBfr.length(), "");
			}
			qry = session.createQuery(strBfr.toString());
			// qry.setParameterList("status", statAry);
			// qry.setParameterList("modeIds", modeArray);
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));

			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			logger.info(ids.size() + "ids___________________");
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Advertisement.class);
				criteria.add(Restrictions.in("id", ids));

				// criteria.addOrder(org.hibernate.criterion.Order.desc("lastUpdatedTime"));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");
			logger.info(data.size() + "-------------------------------009-09-09");
			map.put("AdvList", data);

			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@Override
	public Map<String, Object> getSubscriptionDays(StringBuffer str, long fromDate, long toDate, Object data[],
			int mode, int status) {
		Map<String, Object> map = null;
		List<FeaturesAndPhotosProduct> subscribeList = null;
		try {
			openDBsession();
			Query qry = session.createQuery(str.toString());
			if (mode == REPORT_MODE_ONE_DATE_TO_ALL) {
				qry.setParameter("FDATE", fromDate);
			} else if (mode == REPORT_MODE_BETWEEN_TWO_DATE) {
				qry.setParameter("FDATE", fromDate);
				qry.setParameter("TDATE", toDate);
			} else {
				qry.setParameter("FDATE", fromDate);
				qry.setParameter("TDATE", toDate);
				qry.setParameter("DATA", data[0]);
			}
			qry.setParameter("STATUS", status);
			subscribeList = qry.list();
			closeDBSession();
		} catch (Exception e) {
			closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (subscribeList == null || subscribeList.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -6);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put("reportData", subscribeList);
			return map;
		}
	}

	@Override
	public Map<String, Object> getBookingDays(StringBuffer str, long fromDate, long toDate, Object data[], int mode,
			int status) {
		Map<String, Object> map = null;
		List<SellerDetails> bookedList = null;
		try {
			openDBsession();
			Query qry = session.createQuery(str.toString());
			if (mode == REPORT_MODE_ONE_DATE_TO_ALL) {
				qry.setParameter("FDATE", fromDate);
				qry.setParameter("STATUS", status);
			} else if (mode == REPORT_MODE_BETWEEN_TWO_DATE) {
				qry.setParameter("FDATE", fromDate);
				qry.setParameter("TDATE", toDate);
				qry.setParameter("STATUS", status);
			} else if (mode == REPORT_MODE_BETWEEN_TWO_DATE_WITHOUT_STATUS) {
				qry.setParameter("FDATE", fromDate);
				qry.setParameter("TDATE", toDate);
			} else {
				qry.setParameter("FDATE", fromDate);
				qry.setParameter("TDATE", toDate);
				qry.setParameter("DATA", data[0]);
				qry.setParameter("STATUS", status);
			}
			bookedList = qry.list();
			logger.info(bookedList.size());
			closeDBSession();
		} catch (Exception e) {
			closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (bookedList == null || bookedList.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -6);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put("reportData", bookedList);
			return map;
		}
	}
	/*
	 * @Override public Map<String, Object> bestPurchasedProducts(long
	 * orderId,long orderListId,long customerId,long fromDate,long toDate,long
	 * sellerId,int firstIndex,int rowPerPage,long companyId, long
	 * categoryId,long subCategoryId)
	 * 
	 * { Map<String, Object> map = null; List<Object[]> obj =null;
	 * List<OrderDetails> orderDetails = null; long totalCounts = 0; List<Long>
	 * idCat = null; try { Session session = openDBsession(); boolean flag =
	 * false; Criteria criteria = session.createCriteria(OrderDetails.class);
	 * Criteria criteria1 = session.createCriteria(Product.class); StringBuffer
	 * strBfr2 = new
	 * StringBuffer("select count(*) as c from (select sum(od.item_quantity) as s ,od.product_id from ORDER_DETAILS od  "
	 * );
	 * 
	 * if (categoryId > 0) { if (flag == false) { flag = true;
	 * 
	 * strBfr2.append(" left join PRODUCT o on");
	 * 
	 * }
	 * 
	 * strBfr2.append("   o.CATEGORY_ID=" + categoryId);//111
	 * criteria1.add(Restrictions.eq("CATEGORY_ID", categoryId)); }
	 * 
	 * if (subCategoryId > 0) { if (flag == false) { flag = true;
	 * 
	 * strBfr2.append(" left join PRODUCT o on");
	 * 
	 * } else {
	 * 
	 * strBfr2.append(" left join PRODUCT o on"); }
	 * 
	 * strBfr2.append("   o.SUBCATEGORY_ID=" + subCategoryId);
	 * criteria1.add(Restrictions.eq("SUBCATEGORY_ID", subCategoryId)); } //98
	 * 
	 * strBfr2.
	 * append(" group by od.product_id order by sum(od.item_quantity) desc ) as a "
	 * ); SQLQuery q2=session.createSQLQuery(strBfr2.toString());
	 * q2.addScalar("c", LongType.INSTANCE); totalCounts = (long)
	 * q2.uniqueResult();
	 * criteria.setProjection(Projections.distinct(Projections.id())); idCat =
	 * criteria.list(); strBfr2.replace(0, 27, " ");
	 * 
	 * strBfr2.replace(132, 140, ""); if(categoryId >0 ) { strBfr2.replace(0,
	 * 27, " "); strBfr2.replace(173, 180, ""); }else if(subCategoryId > 0) {
	 * strBfr2.replace(0, 27, " "); strBfr2.replace(177, 184, ""); }else {
	 * strBfr2.replace(0, 27, " ");
	 * 
	 * strBfr2.replace(132, 140, ""); }
	 * 
	 * 
	 * SQLQuery q3=session.createSQLQuery(strBfr2.toString()); q3.addScalar("s",
	 * LongType.INSTANCE); q3.addScalar("product_id", LongType.INSTANCE);
	 * 
	 * logger.info(q3+
	 * "q3__________________________________________________________________q3")
	 * ; q3.setFirstResult(firstIndex); q3.setMaxResults(rowPerPage); obj =
	 * q3.list(); if (rowPerPage > 0) {
	 * 
	 * criteria.setFirstResult(firstIndex); criteria.setMaxResults(rowPerPage);
	 * } //criteria = session.createCriteria(Product.class);
	 * criteria1.add(Restrictions.in("id", idCat));
	 * criteria1.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	 * criteria1.addOrder(org.hibernate.criterion.Order.desc("lastUpdatedTime"))
	 * ; orderDetails = criteria1.list(); session.flush(); // }
	 * 
	 * } catch (HibernateException e) { logger.info(e.getMessage());
	 * e.printStackTrace(); throw e; } if (orderDetails != null &&
	 * !orderDetails.isEmpty()) { map = new HashMap<>(); map.put(ERROR_CODE, 0);
	 * map.put(ERROR_MESSAGE, MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC,
	 * MESSAGE_SUCSESS_AR); //map.put(DATA, orderDetails); map.put("qtyList",
	 * obj); map.put("TotalCounts", totalCounts); return map;
	 * 
	 * } else { map = new HashMap<>(); map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR); return map; }
	 * }
	 */

	@Override
	public Map<String, Object> bestPurchasedProducts(long orderId, long orderListId, long customerId, long fromDate,
			long toDate, long sellerId, int firstIndex, int rowPerPage, long companyId, long categoryId,
			long subCategoryId)

	{
		Map<String, Object> map = null;
		List<Object[]> obj = null;
		List<OrderDetails> orderDetails = null;
		long totalCounts = 0;
		List<Long> idCat = null;
		try {
			int t = 0;
			int n = 0;
			Session session = openDBsession();
			boolean flag = false;
			Criteria criteria = session.createCriteria(OrderDetails.class);
			Criteria criteria1 = session.createCriteria(Product.class);
			StringBuffer strBfr2 = new StringBuffer(
					"select count(*) as c from (select sum(od.item_quantity) as s ,od.product_id from ORDER_DETAILS od  ");// 70

			if (categoryId > 0) {

				n = 1;
				if (flag == false) {
					flag = true;

					strBfr2.append(" inner join PRODUCT o on o.product_id=od.product_id ");

				}

				strBfr2.append("  where  o.CATEGORY_ID=" + categoryId);
			}

			if (subCategoryId > 0) {
				n = 1;
				if (flag == false) {

					strBfr2.append(" inner join PRODUCT o on o.product_id=od.product_id ");
					strBfr2.append("  where  o.SUBCATEGORY_ID=" + subCategoryId);
				} else {
					t = 1;
					strBfr2.append("  and  o.SUBCATEGORY_ID=" + subCategoryId);
				}

			}

			if (fromDate > 0) 
			{
				if (flag == false) {
					// strBfr2.append("where");
				}
				n = 1;
				if (t == 0) {
					strBfr2.append("where");
				} else {
					strBfr2.append("and");
				}
				strBfr2.append(" od.LAST_UPDATED_TIME between " + fromDate + " and " + toDate);

			}
			// strBfr2.append(" ");
			// strBfr2.append(" group by od.product_id order by
			// sum(od.item_quantity) desc ) as a ");
			if (n > 0) {
				strBfr2.append(" and od.status=4 group by od.product_id order by sum(od.item_quantity) desc ) as a ");
			} else {
				strBfr2.append(" where od.status=4 group by od.product_id order by sum(od.item_quantity) desc ) as a ");
			}

			SQLQuery q2 = session.createSQLQuery(strBfr2.toString());
			q2.addScalar("c", LongType.INSTANCE);
			totalCounts = (long) q2.uniqueResult();

			if (categoryId > 0 && subCategoryId > 0) 
			{

				strBfr2.replace(0, 27, " ");
				// strBfr2.replace(234, 244, " ");
				strBfr2.replace(250, 260, " ");

			}

			else if (categoryId > 0) 
			{
				logger.info("cat");
				if(fromDate >0 )
				{
					logger.info("???????????????????????????????????????");
					strBfr2.replace(0, 27, " ");

					// strBfr2.replace(132, 140, "");
					strBfr2.replace(209, 222, "");
				}
				else
				{
				strBfr2.replace(0, 27, " ");
				// strBfr2.replace(208, 215, " ");
				strBfr2.replace(219, 233, " ");
				}
			} else if (subCategoryId > 0) {

				if (flag == false) {

					strBfr2.replace(0, 27, " ");
					strBfr2.replace(211, 218, " ");
				} else {

					strBfr2.replace(0, 27, " ");
					strBfr2.replace(263, 270, " ");

				}
			}

			else {
				if(fromDate >0 )
				{
					logger.info("???????????????????????????????????????");
					strBfr2.replace(0, 27, " ");

					// strBfr2.replace(132, 140, "");
					strBfr2.replace(209, 222, "");
				}
				else
				{
					logger.info("111111111111???????????????????????????????????????");
				strBfr2.replace(0, 27, " ");

				// strBfr2.replace(132, 140, "");
				strBfr2.replace(150, 158, "");
				}
			}

			SQLQuery q3 = session.createSQLQuery(strBfr2.toString());
			q3.addScalar("s", LongType.INSTANCE);
			q3.addScalar("product_id", LongType.INSTANCE);

			q3.setFirstResult(firstIndex);
			q3.setMaxResults(rowPerPage);
			logger.info(q3);
			obj = q3.list();
			logger.info(q3);
			if (rowPerPage > 0) {

				q3.setFirstResult(firstIndex);
				q3.setMaxResults(rowPerPage);
			}
			// criteria = session.createCriteria(Product.class);
			/*
			 * criteria1.add(Restrictions.in("id", idCat));
			 * criteria1.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			 * criteria1.addOrder(org.hibernate.criterion.Order.desc(
			 * "lastUpdatedTime")); orderDetails = criteria1.list();
			 * session.flush();
			 */
			// }

		} catch (HibernateException e) {
			logger.info(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		if (obj != null && !obj.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			// map.put(DATA, orderDetails);
			map.put("qtyList", obj);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@Override
	public Map<String, Object> getPromocodeReport(int firstIndex, int rowPerPage, long fromDate, long toDate,
			long codeId, long userId, long promoterId, long code) {
		Map<String, Object> map = null;
		List<PromocodeDetails> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			logger.info(code + "------------------------dao ----------------------code");
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Order.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Order o");
			if (codeId > 0) {
				flag = true;
				strBfr.append(" where o.promocodeId=" + codeId);
				criteria.add(Restrictions.eq("promocodeId", codeId));
			}
			/*
			 * if (status == 1 || status == 0) { if (flag == false) {
			 * strBfr.append(" where o.status=" + status); } else {
			 * strBfr.append(" and o.status=" + status); }
			 * criteria.add(Restrictions.eq("status", status)); }
			 */

			if (userId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.customerId=" + userId);
				criteria.add(Restrictions.eq("customerId", userId));
			}

			if (code > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and ");
				}
				strBfr.append(" o.promocodeId=" + code);
				criteria.add(Restrictions.eq("promocodeId", code));
			}

			if (fromDate > 0) {
				if (toDate == 0) {
					toDate = TimeUtils.instance().getEndTimeOfGiveTime(fromDate);
				}
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.lastUpdatedTime between " + fromDate + " and " + toDate);
				criteria.add(Restrictions.between("lastUpdatedTime", fromDate, toDate));
			}

			/*
			 * if (promocode != null && !promocode.equals("")) {
			 * logger.info(promocode +
			 * "promocode_________________________________________"); if (flag
			 * == false) { flag = true; strBfr.append(" and");
			 * 
			 * } else { strBfr.append(" and"); }
			 * strBfr.append(" lower(o.promocode) like '%" +
			 * promocode.toLowerCase() + "%'"); logger.info(strBfr +
			 * "_______________________________________________");
			 * criteria.add(Restrictions.ilike("promocode", "%" + promocode +
			 * "%"));
			 * 
			 * }
			 */

			Query qry = session.createQuery(strBfr.toString());
			logger.info(qry
					+ "qry---------------------------------------------------------------------------------------------");
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			logger.info(ids
					+ "ids----------------------------------------------------------------------------------------");
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Order.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");

			map.put("promoList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@Override
	public Map<String, Object> getAllOrdersDetails(long orderId, long orderListId, long customerId, long fromDate,
			long toDate, long sellerId, int firstIndex, int rowPerPage, long companyId) {
		Map<String, Object> map = null;
		List<OrderDetails> orderDetails = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			boolean flag = false;
			Criteria criteria = session.createCriteria(OrderDetails.class);

			StringBuffer strBfr = new StringBuffer("select count(*) from OrderDetails od");
			if (orderId > 0) {
				flag = true;
				strBfr.append(" where od.orderId=" + orderId);
				criteria.add(Restrictions.eq("orderId", orderId));
			}
			if (orderListId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" od.orderListId=" + orderListId);
				criteria.add(Restrictions.eq("orderListId", orderListId));
			}

			if (companyId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" od.companyId=" + companyId);
				criteria.add(Restrictions.eq("companyId", companyId));
			}

			if (customerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" orderId in (select orderId from OrderTable where customerId=" + customerId + ")");
				criteria.createAlias("order", "order");
				criteria.add(Restrictions.eq("order.customerId", customerId));
			}
			if (fromDate > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" od.lastUpdatedTime between " + fromDate + " and " + toDate);
				criteria.add(Restrictions.between("lastUpdatedTime", fromDate, toDate));
			}
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			// criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {

				criteria.setFirstResult(firstIndex);
				criteria.setMaxResults(rowPerPage);
			}
			/*
			 * List<Long> ids = criteria.list(); session.flush();
			 * session.clear(); if (ids != null && !ids.isEmpty()) { criteria =
			 * session.createCriteria(AuctionDetails.class);
			 * criteria.add(Restrictions.in("id", ids));
			 */
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			orderDetails = criteria.list();
			session.flush();
			// }

		} catch (HibernateException e) {
			logger.info(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		if (orderDetails != null && !orderDetails.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put(DATA, orderDetails);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@Override
	public Map<String, Object> getAllTransBy(long mode, long fromDate, long toDate, long toWhoId, int inOrOut,
			int tranType, int currentIndex, int rowPerPage) {
		Map<String, Object> map = null;
		List<Transactions> tranDetails = null;
		long totalCounts = 0;
		double totalAmt = 0;
		double debitAmt = 0;
		double codAmt = 0;
		double onlineAmt = 0;
		double returnAmt = 0;
		try {
			Session session = openDBsession();
			boolean flag = false;
			Criteria criteria = session.createCriteria(Transactions.class);
			StringBuffer strBfr = new StringBuffer(
					"select count(*),sum(t.netAmount),case when t.transactionMode=20 then sum(t.netAmount) end as debitAmt,case when t.transactionMode=21 then sum(t.netAmount) end as cashPay"
							+ ",case when t.transactionMode=22 then sum(t.netAmount) end as onlineAmt,case when t.transactionMode=250 then sum(t.netAmount) end as returnAmt from Transactions t");
			if (mode > 0) {
				flag = true;
				strBfr.append(" where t.transactionMode=" + mode);
				criteria.add(Restrictions.eq("transactionMode", mode));
			}
			if (inOrOut > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" t.inOrOut=" + inOrOut);
				criteria.add(Restrictions.eq("inOrOut", inOrOut));
			}
			if (tranType > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" t.bankOrCash=" + tranType);
				criteria.add(Restrictions.eq("bankOrCash", tranType));
			}
			if (toWhoId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" t.toWhoId=" + toWhoId);
				criteria.add(Restrictions.eq("toWhoId", toWhoId));
			}
			if (fromDate > 0) {
				if (toDate == 0) {
					toDate = TimeUtils.instance().getEndTimeOfGiveTime(fromDate);
				}
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.lastUpdatedTime between " + fromDate + " and " + toDate);
				criteria.add(Restrictions.between("lastUpdatedTime", fromDate, toDate));
			}
			Query qry = session.createQuery(strBfr.toString());
			List<Object[]> data = qry.list();
			session.flush();
			totalCounts = (long) data.get(0)[0];
			totalAmt = (double) data.get(0)[1];
			debitAmt = (double) data.get(0)[2];
			codAmt = (double) data.get(0)[3];
			onlineAmt = (double) data.get(0)[4];
			returnAmt = (double) data.get(0)[5];
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setFirstResult(currentIndex);

				criteria.setMaxResults(rowPerPage);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Transactions.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				criteria.addOrder(org.hibernate.criterion.Order.desc("lastUpdatedTime"));
				tranDetails = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			logger.info(e.getMessage());
			e.printStackTrace();
			throw e;
		}
		if (tranDetails != null && !tranDetails.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put(DATA, tranDetails);
			map.put(TOTAL_COUNT, totalCounts);
			map.put("TOTALAMOUNT", totalAmt);
			map.put("DEBITAMOUNT", debitAmt);
			map.put("CODAMOUNT", codAmt);
			map.put("ONLINEAMOUNT", onlineAmt);
			map.put("RETURNAMOUNT", returnAmt);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@Override
	public Map<String, Object> getAllDetails(long userId, int roleId) {
		Map<String, Object> map = new HashMap<>();
		try {
			logger.info(userId + "userId-------------------------------------------------------");
			Session session = openDBsession();
			long currentTime = TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
			StringBuffer strBfr = new StringBuffer();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(currentTime);
			long fromDate = TimeUtils.instance().getStartTimeOfGiveTime(currentTime);
			long toDate = TimeUtils.instance().getEndTimeOfGiveTime(currentTime);
			logger.info(fromDate + "fromDate---------------------------------------------------------");
			logger.info(toDate + "toDate---------------------------------------------------------");
			/*
			 * int mYear = calendar.get(Calendar.YEAR); int mMonth =
			 * calendar.get(Calendar.MONTH); int mDay =
			 * calendar.get(Calendar.DAY_OF_MONTH);
			 */
			if (roleId == ROLL_ID_ADMIN) {
				strBfr.append("select count(*) from Review where status=1");
				Query qry = session.createQuery(strBfr.toString());
				long count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_REVIEWS", count);

				/*
				 * qry = session.
				 * createQuery("select count(*) from Product where status=1");
				 * count = (long) qry.uniqueResult(); session.flush();
				 * map.put("NO_OF_PRODUCTS", count);
				 */

				qry = session.createQuery("select count(*) from SellerDetails where status=1");
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_STORES", count);
				qry = session.createQuery("select count(*) from CustomerDetails where status=1");
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_CUSTOMERS", count);

				qry = session
						.createQuery(" select count(*) from CustomerDetails where status=1 and createdDate  between  "
								+ fromDate + " and  " + toDate + " ");
				/*
				 * qry.setParameter("fromDate", fromDate);
				 * qry.setParameter("toDate", toDate);
				 */
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_NEW_CUSTOMERS", count);

				qry = session.createQuery(
						"select count(*) from Order where orderstatus=1 and status=1 and lastUpdatedTime  between  "
								+ fromDate + " and  " + toDate + " ");
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_NEW_ORDERS", count);

				qry = session.createQuery("select count(*) from Order where orderstatus=1 and status=1");
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_ORDERED", count);
				qry = session.createQuery("select count(*) from Order where orderstatus=2 and status=1");
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_CONFIRMED", count);
				qry = session.createQuery("select count(*) from Order where orderstatus=4 and status=1");
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_DELIVERED", count);
				/*
				 * qry = session.
				 * createQuery("select count(*) from BookingDetails where status=:stat"
				 * ); qry.setParameter("stat", BOOKED); count = (long)
				 * qry.uniqueResult();
				 */
				session.flush();
				map.put("NO_OF_NEW_BOOKING", count);
				// -------------
				qry = session.createQuery("select count(*) from City where  citusStatus=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_CITY", count);
				// -------------------------
				// -------------
				qry = session.createQuery("select count(*) from Country where  countryStatus=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_COUNTRY", count);
				// -------------------------
				// -------------
				qry = session.createQuery("select count(*) from Category where  status=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_CATEGORY", count);
				// -------------------------
				// -------------
				qry = session.createQuery("select count(*) from CustomerDetails where  status=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_CUSTOMERS", count);
				// -------------
				qry = session.createQuery("select count(*) from SubCategory where  status=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_SUB-CATEGORY", count);
				// -------------------------
				// -------------
				qry = session.createQuery("select count(*) from Manufacture where  status=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_MANUFACTURES", count);
				// -------------------------
				qry = session.createQuery("select count(*) from Product p where p.status=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_PRODUCTS", count);

			} else {
				strBfr.append(
						"select count(*) from Review r inner join r.product p inner join p.seller s where s.userId=:uId and r.status=1");
				Query qry = session.createQuery(strBfr.toString());
				qry.setParameter("uId", userId);
				long count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_REVIEWS", count);
				qry = session.createQuery(
						" select count(*) from Product p inner join p.seller s where s.userId=:uId and p.status=1");
				qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				logger.info(count + "count-------------------------------------------------------");
				session.flush();
				map.put("NO_OF_PRODUCTS", count);
				qry = session.createQuery("select count(*) from SellerDetails where userId=:uId and status=1");
				qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_SELLER", count);
				qry = session.createQuery("select count(*) from CustomerDetails where status=1");
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_CUSTOMERS", count);
				// -------------
				qry = session.createQuery("select count(*) from City where  citusStatus=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_CITY", count);
				// -------------------------
				// -------------
				qry = session.createQuery("select count(*) from Country where  countryStatus=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_COUNTRY", count);

				qry = session.createQuery(
						"select count(*) from Order where orderstatus=1 and status=1 and  lastUpdatedTime  between  "
								+ fromDate + " and  " + toDate + " ");
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_NEW_ORDERS", count);
				// -------------------------
				// -------------
				qry = session.createQuery("select count(*) from Category where  status=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_CATEGORY", count);
				// -------------------------
				qry = session
						.createQuery("select count(*) from CustomerDetails where status=1 and createdDate  between  "
								+ fromDate + " and  " + toDate + " ");
				/*
				 * qry.setParameter("fromDate", fromDate);
				 * qry.setParameter("toDate", toDate);
				 */
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_NEW_CUSTOMERS", count);
				// -------------
				qry = session.createQuery("select count(*) from SubCategory where  status=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_SUB-CATEGORY", count);
				// -------------------------
				// -------------
				qry = session.createQuery("select count(*) from Manufacture where  status=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_MANUFACTURES", count);
				// -------------------------
				qry = session.createQuery(
						"select count(*) from Order o inner join o.orderDetails od inner join od.product p inner join p.seller s where s.userId=:uId and o.orderstatus=1 and o.status=1");
				qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_ORDERED", count);
				qry = session.createQuery(
						"select count(*) from Order o inner join o.orderDetails od inner join od.product p inner join p.seller s where s.userId=:uId and o.orderstatus=2 and o.status=1");
				qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_CONFIRMED", count);
				// ---------------------------------------
				qry = session.createQuery(
						"select count(*) from Order o inner join o.orderDetails od inner join od.product p inner join p.seller s where s.userId=:uId and o.orderstatus=4 and o.status=1");
				qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_DELIVERED", count);
				// -------------------
				/*
				 * qry = session.
				 * createQuery("select count(*) from Order o inner join o.orderDetails od inner join od.product p inner join p.seller s where s.userId=:uId and o.orderstatus=4 and o.status=1"
				 * ); qry.setParameter("uId", userId); count = (long)
				 * qry.uniqueResult(); session.flush();
				 * map.put("NO_OF_DELIVERED", count);
				 */
				/*
				 * qry = session.
				 * createQuery("select count(*) from BookingDetails bd inner join bd.product p inner join p.shop s where s.userId=:uId and bd.status=:stat"
				 * ); qry.setParameter("uId", userId); qry.setParameter("stat",
				 * BOOKED); count = (long) qry.uniqueResult();
				 */
				session.flush();
				map.put("NO_OF_NEW_BOOKING", count);
				qry = session.createQuery("select count(*) from Product p where p.status=1");
				// qry.setParameter("uId", userId);
				count = (long) qry.uniqueResult();
				session.flush();
				map.put("NO_OF_PRODUCTS", count);
			}
		} catch (HibernateException e) {
			if (TESTING) {
				e.printStackTrace();
			}
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		return map;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> getAllSalesReport(long storeId,long
	 * customerId,int firstIndex, int rowPerPage,long deliveryBoyId,long
	 * categoryId,long productId,long sellerId,long fromDate, long toDate,long
	 * subCategoryId,int modeType)
	 * 
	 * { Map<String, Object> map = null; logger.info(modeType+
	 * "modeType-----------------------------------------------6--------------------------------------"
	 * );
	 * 
	 * long totalCounts = 0;
	 * 
	 * List<Object> rows; try { Session session = openDBsession(); StringBuffer
	 * strBfr = new
	 * StringBuffer("select count(*) from(select      from  Order o inner join o.transaction t inner join o.orderDetails od inner join od.product p where t.status=1"
	 * ); logger.info(strBfr+
	 * "strBfr---------------------------------------------------------------------------------------78"
	 * ); if (storeId > 0) { strBfr.append(" and o.storeId =" + storeId); } if
	 * (categoryId > 0) { strBfr.append(" and p.categoryId=" + categoryId); } if
	 * (customerId > 0) { strBfr.append(" and o.customerId=" + customerId); } if
	 * (deliveryBoyId > 0) { strBfr.append(" and o.deliveryBoyId=" +
	 * deliveryBoyId); } if (productId > 0) { strBfr.append(" and od.productId="
	 * + productId); } if (subCategoryId > 0) {
	 * strBfr.append(" and p.subCategoryId=" + subCategoryId); } if (fromDate >
	 * 0 || toDate > 0 ) {
	 * 
	 * strBfr.append(" and t.lastUpdatedTime between " + fromDate + " and " +
	 * toDate); }
	 * 
	 * 
	 * 
	 * if (modeType == SALE_REPORT_BY_CATEGORY) {
	 * strBfr.append(" group by p.categoryId"); } else if (modeType ==
	 * SALE_REPORT_BY_SUBCATEGORY) { strBfr.append(" group by p.subCategoryId");
	 * } else if (modeType == SALE_REPORT_BY_CUSTOMER) {
	 * strBfr.append(" group by o.customerId"); } else if (modeType ==
	 * SALE_REPORT_BY_DELIVERY) { strBfr.append(" group by o.deliveryBoyId"); }
	 * else if (modeType == SALE_REPORT_BY_PRODUCT) {
	 * strBfr.append(" group by p.productId"); } else if (modeType ==
	 * SALE_REPORT_BY_STORE) { strBfr.append(" group by o.sellerId"); } Query
	 * qry = session.createQuery(strBfr.toString()); logger.info(qry+
	 * "qry------------------------------------------------------------------------------------------"
	 * ); totalCounts = (long) qry.uniqueResult(); logger.info(totalCounts+
	 * "totalCounts---------------------------------------------totalCounts---------------------------------------------"
	 * ); session.flush();
	 * 
	 * switch (modeType) {
	 * 
	 * case SALE_REPORT_BY_CATEGORY: { strBfr.replace(7, 15,
	 * "p.categoryId,count(o.orderId),sum(t.netAmount)"); break; } case
	 * SALE_REPORT_BY_SUBCATEGORY: { strBfr.replace(2, 33,
	 * "p.subCategoryId,count(o.orderId),sum(t.netAmount)"); break; } case
	 * SALE_REPORT_BY_SUBCATEGORY: { strBfr.
	 * append("(select p.SUBCATEGORY_ID,count(o.order_Id),sum(t.net_Amount) from Order o inner join (Transactions t  inner join (ORDER_DETAILS od inner join PRODUCT p on p.PRODUCT_ID=od.PRODUCT_ID) on od.ORDER_ID=t.paired_id) on t.TRANSACTION_ID=o.TRANSACTION_ID GROUP BY p.SUBCATEGORY_ID) as a"
	 * ); break; }
	 * 
	 * case SALE_REPORT_BY_CUSTOMER: { strBfr.replace(7, 15,
	 * "o.customerId,count(o.orderId),sum(t.netAmount)"); break; } case
	 * SALE_REPORT_BY_DELIVERY: { strBfr.replace(7, 15,
	 * "o.deliveryBoyId,count(o.orderId),sum(t.netAmount)"); break; } case
	 * SALE_REPORT_BY_PRODUCT: { logger.
	 * info("inside product___________________*************___________________________"
	 * ); strBfr.replace(7, 15,
	 * "p.productId,p.productName,count(o.orderId),sum(t.netAmount)");
	 * 
	 * break; } case SALE_REPORT_BY_STORE: { strBfr.replace(7, 15,
	 * "p.storeId,count(o.orderId),sum(t.netAmount)"); break; } default:
	 * 
	 * break; }
	 * 
	 * strBfr.append(" ) as a"); Query qry =
	 * session.createQuery(strBfr.toString()); Query qry1 =
	 * session.createQuery(strBfr.toString()); logger.info(qry1+
	 * "qry1___________________*************___________________________qry1");
	 * List<Object> opbj= qry1.list(); totalCounts=opbj.size(); logger.info(qry+
	 * "qry---------------------------------------------------------------------------------------54"
	 * ); if (rowPerPage > 0) { qry.setFirstResult(firstIndex);
	 * qry.setMaxResults(rowPerPage); } rows = qry.list();
	 * logger.info(rows.size()+
	 * "rows.size()---------------------------------------------------------------------------------------44"
	 * ); session.flush(); } catch (HibernateException e) {
	 * 
	 * if (TESTING) { e.printStackTrace(); } logger.info(e.getMessage()); map =
	 * new HashMap<String, Object>(); map.put(ERROR_CODE, -2);
	 * map.put(ERROR_MESSAGE, e.getMessage()); return map; } if (rows != null &&
	 * !rows.isEmpty()) {
	 * 
	 * map = new HashMap<String, Object>(); map.put("TranscList", rows);
	 * //map.put(TOTAL_COUNT, totalCounts); map.put(ERROR_CODE, 0);
	 * map.put(ERROR_MESSAGE, "SUCCESS"); return map; } else { map = new
	 * HashMap<String, Object>(); map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS); return map; } }
	 * 
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllSalesReport(long storeId, long customerId, int firstIndex, int rowPerPage,
			long deliveryBoyId, long categoryId, long productId, long sellerId, long fromDate, long toDate,
			long subCategoryId, int modeType)

	{
		Map<String, Object> map = null;

		long totalCounts = 0;
		int t = 1;
		List<Object> rows;
		try {
			Session session = openDBsession();
			StringBuffer strBfr = new StringBuffer(
					"select count(*) as t from(select                                                               from ORDER_TABLE o inner join TRANSACTION t  on t.PAIRED_ID=o.order_id inner join ORDER_DETAILS od on t.PAIRED_ID=od.order_id  inner join PRODUCT p on p.product_id=od.product_id where t.status=1 and o.ORDER_STATUS=4 and t.transaction_mode=20 and od.STATUS=4");

			if (storeId > 0) {
				strBfr.append(" and o.seller_id =" + storeId);
			}
			if (categoryId > 0) {
				strBfr.append(" and p.category_id=" + categoryId);
			}
			if (customerId > 0) {
				strBfr.append(" and o.customer_id=" + customerId);
			}
			if (deliveryBoyId > 0) {
				strBfr.append(" and o.deliveryBoy_id=" + deliveryBoyId);
			}
			if (productId > 0) {
				strBfr.append(" and od.product_id=" + productId);
			}
			if (subCategoryId > 0) {
				strBfr.append(" and p.subCategory_id=" + subCategoryId);
			}
			if (fromDate > 0 || toDate > 0) {

				strBfr.append(" and t.LAST_UPDATED_TIME between " + fromDate + " and " + toDate);
			}

			if (modeType == SALE_REPORT_BY_CATEGORY) {
				strBfr.append(" group by p.category_id");
			} else if (modeType == SALE_REPORT_BY_SUBCATEGORY) {
				strBfr.append(" group by p.subCategory_id");
			} else if (modeType == SALE_REPORT_BY_CUSTOMER) {
				strBfr.append(" group by o.customer_id");
			} else if (modeType == SALE_REPORT_BY_DELIVERY) {
				strBfr.append(" group by o.deliveryBoyId");
			} else if (modeType == SALE_REPORT_BY_PRODUCT) {
				strBfr.append(" group by p.product_id");
			} else if (modeType == SALE_REPORT_BY_STORE) {

				strBfr.append(" group by p.seller_id");
			}

			switch (modeType) {

			case SALE_REPORT_BY_CATEGORY: {
				strBfr.replace(33, 36, "p.category_id as s,count(o.order_id) as c,sum(od.ITEM_NETPRICE) as n");
				break;
			}
			case SALE_REPORT_BY_SUBCATEGORY: {
				strBfr.replace(33, 36, "p.subCategory_id as s,count(o.order_id) as c,sum(od.ITEM_NETPRICE) as n");
				break;
			}

			case SALE_REPORT_BY_CUSTOMER: {
				strBfr.replace(33, 36, "o.CUSTOMER_ID as s,count(o.order_id) as c,sum(t.net_Amount) as n");
				break;
			}
			case SALE_REPORT_BY_DELIVERY: {
				strBfr.replace(33, 36, "o.deliveryBoy_id as s,count(o.order_id) as c,sum(t.net_Amount) as n");
				break;
			}
			case SALE_REPORT_BY_PRODUCT: {
				logger.info("inside product___________________*************___________________________");
				strBfr.replace(33, 36,
						"p.product_id as s,count(o.order_id) as c,sum(od.ITEM_NETPRICE)as n,p.product_name as pn,p.PRODUCT_NAME_ARABIC as pnr");

				break;
			}
			case SALE_REPORT_BY_STORE: {
				strBfr.replace(33, 36, "p.seller_id as s,count(o.order_id)as c,sum(t.net_Amount) as n");
				break;
			}
			default:

				break;
			}

			// -------------data

			StringBuffer sb1 = new StringBuffer(strBfr);

			strBfr.replace(0, 26, " ");

			SQLQuery qry1 = session.createSQLQuery(strBfr.toString());
			qry1.addScalar("s", LongType.INSTANCE);
			qry1.addScalar("c", LongType.INSTANCE);
			qry1.addScalar("n", DoubleType.INSTANCE);
			if (modeType == SALE_REPORT_BY_PRODUCT) {
				qry1.addScalar("pn", StringType.INSTANCE);
				qry1.addScalar("pnr", StringType.INSTANCE);
			}
			if (rowPerPage > 0) {
				qry1.setFirstResult(firstIndex);
				qry1.setMaxResults(rowPerPage);
			}
			logger.info(qry1 + " qry1qry1qry1q___________________qry1qry1qry1q_________________________ qry1qry1qry1q");
			rows = qry1.list();

			// ----------------data

			sb1.append(" ) as a ");

			SQLQuery q = session.createSQLQuery(sb1.toString());
			logger.info(q + "q___________________q_________________________ q");
			q.addScalar("t", LongType.INSTANCE);
			logger.info(q + "q___________________q_________________________ q");
			totalCounts = (long) q.uniqueResult();
			logger.info(totalCounts + "q___________________totalCountstotalCountsq_________________________ q");
			if (totalCounts == 0) {
				totalCounts = 0;
			}
		} catch (HibernateException e) {

			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<String, Object>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (rows != null && !rows.isEmpty()) {

			map = new HashMap<String, Object>();
			map.put("TranscList", rows);
			map.put("total", totalCounts);
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, "SUCCESS");
			return map;
		} else {
			map = new HashMap<String, Object>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@Override
	public Map<String, Object> monthlyRevenueReport(int firstIndex, int rowPerPage, long fromDate, long toDate,
			long codeId) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		List<Object[]> obj = null;

		StringBuffer strBfr = new StringBuffer(
				"SELECT sum(NETPRICE) as s ,date_trunc('month', to_timestamp(LAST_UPDATED_TIME/1000)) as t FROM ORDER_TABLE p   ");

		if (fromDate > 0 || toDate > 0) {

			strBfr.append(" where p.LAST_UPDATED_TIME between " + fromDate + " and " + toDate);
		} else {
			strBfr.append(" where p.LAST_UPDATED_TIME > 0");
		}

		strBfr.append("and p.ORDER_STATUS=4");
		strBfr.append(
				"GROUP BY date_trunc('month', to_timestamp(LAST_UPDATED_TIME/1000))ORDER BY date_trunc('month', to_timestamp(LAST_UPDATED_TIME/1000))  asc");

		SQLQuery qry1 = session.createSQLQuery(strBfr.toString());
		logger.info(qry1 + "monthly report query------------------------------------------------");
		obj = qry1.list();
		map = new HashMap<String, Object>();
		map.put("TranscList", obj);

		map.put(ERROR_CODE, 0);
		map.put(ERROR_MESSAGE, "SUCCESS");
		return map;

	}

	@Override
	public Map<String, Object> weeklyRevenueReport(int firstIndex, int rowPerPage, long fromDate, long toDate,
			long codeId) {
		long timeNow = TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);

		Map<String, Object> map = null;
		Session session = openDBsession();
		List<Object[]> obj = null;

		StringBuffer strBfr = new StringBuffer(
				"SELECT sum(net_amount) as s ,date_trunc('week', to_timestamp(last_updated_time/1000)) as t FROM TRANSACTION p   ");

		if (fromDate > 0 || toDate > 0) {

			strBfr.append(" where p.last_updated_time between " + fromDate + " and " + toDate);
		} else {
			strBfr.append(" where p.last_updated_time > 0");
		}

		strBfr.append("GROUP BY date_trunc('week', to_timestamp(last_updated_time/1000))");
		SQLQuery qry1 = session.createSQLQuery(strBfr.toString());
		obj = qry1.list();
		map = new HashMap<String, Object>();
		map.put("TranscList", obj);

		map.put(ERROR_CODE, 0);
		map.put(ERROR_MESSAGE, "SUCCESS");
		return map;
	}

	@Override
	public Map<String, Object> yearlyRevenueReport(int firstIndex, int rowPerPage, long fromDate, long toDate,
			long codeId)

	{
		Map<String, Object> map = null;
		Session session = openDBsession();
		List<Object[]> obj = null;
		/*
		 * SQLQuery aCountQuery = session.
		 * createSQLQuery("SELECT sum(net_amount) as s ,date_trunc('year', to_timestamp(last_updated_time/1000)) as t FROM TRANSACTION p where p.last_updated_time > 0   GROUP BY date_trunc('year', to_timestamp(last_updated_time/1000))"
		 * ); logger.info(aCountQuery+
		 * "aCountQuery------------------------------------------------------");
		 */

		StringBuffer strBfr = new StringBuffer(
				"SELECT sum(net_amount) as s ,date_trunc('year', to_timestamp(last_updated_time/1000)) as t FROM TRANSACTION p   ");

		if (fromDate > 0 || toDate > 0) {

			strBfr.append(" where p.last_updated_time between " + fromDate + " and " + toDate);
		} else {
			strBfr.append(" where p.last_updated_time > 0");
		}

		strBfr.append("GROUP BY date_trunc('year', to_timestamp(last_updated_time/1000))");
		SQLQuery qry1 = session.createSQLQuery(strBfr.toString());

		obj = qry1.list();
		map = new HashMap<String, Object>();
		map.put("TranscList", obj);

		map.put(ERROR_CODE, 0);
		map.put(ERROR_MESSAGE, "SUCCESS");
		return map;

	}

	/*
	 * @Override public Map<String, Object> getAllAuctionBy(long auctionId, long
	 * auctionDetailsId, long customerId,int firstIndex, int rowPerPage, int
	 * status[],long fromDate,long toDate) { Map<String, Object> map = null;
	 * List<Auction> data = null; List<Review> reviewList = null; long
	 * totalCounts = 0; try { Session session = openDBsession(); Criteria
	 * criteria = session.createCriteria(Auction.class); boolean flag = true;
	 * StringBuffer strBfr = new StringBuffer("select count(*) from Auction a");
	 * if (auctionId > 0) { flag = false; strBfr.append(" where a.auctionId=" +
	 * auctionId); criteria.add(Restrictions.eq("auctionId", auctionId)); } if
	 * (auctionDetailsId > 0) { if (flag == false) { flag = true;
	 * strBfr.append("inner join a.auctionDetails ad where"); } else {
	 * strBfr.append(" and"); }
	 * 
	 * strBfr.append(" ad.auctionDetailsId=" + auctionDetailsId); Criteria
	 * criteria2=criteria.createCriteria("auctionDetails");
	 * criteria2.add(Restrictions.eq("auctionDetailsId", auctionDetailsId)); }
	 * if(fromDate > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * 
	 * strBfr.append(" a.lastUpdatedTime between " + fromDate+" and "+toDate);
	 * criteria.add(Restrictions.between("lastUpdatedTime", fromDate, toDate));
	 * } if (customerId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * 
	 * strBfr.append(" a.userId=" + customerId);
	 * criteria.add(Restrictions.eq("userId", customerId)); } List<Integer>
	 * statAry = null; if (status.length > 0) { if (flag == false) { flag =
	 * true; strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * 
	 * Object[] objAr = new Object[status.length]; statAry = new ArrayList<>();
	 * for (int i = 0; i < status.length; i++) { objAr[i] = status[i];
	 * statAry.add(status[i]); } criteria.add(Restrictions.in("auctionStatus",
	 * objAr)); strBfr.append(" a.auctionStatus in (:stat)"); } if (flag ==
	 * true) { strBfr.replace(28, strBfr.length(), ""); } Query qry =
	 * session.createQuery(strBfr.toString()); totalCounts = (long)
	 * qry.uniqueResult(); session.flush();
	 * criteria.setProjection(Projections.distinct(Projections.id()));
	 * if(rowPerPage > 0) { criteria.setMaxResults(rowPerPage);
	 * criteria.setFirstResult(firstIndex); } List<Long> ids = criteria.list();
	 * session.flush(); session.clear(); if (ids != null && !ids.isEmpty()) {
	 * criteria = session.createCriteria(Auction.class);
	 * criteria.add(Restrictions.in("id", ids));
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); data =
	 * criteria.list(); session.flush(); }
	 * 
	 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
	 * criteria.add(Restrictions.eq("orderId", orderId));
	 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
	 * criteria.list();
	 * 
	 * session.flush(); }
	 * 
	 * // closeDBSession(); } catch (HibernateException e) { //
	 * closeDBSession(); if (TESTING) { e.printStackTrace(); }
	 * logger.info(e.getMessage()); map = new HashMap<>(); map.put(ERROR_CODE,
	 * -2); map.put(ERROR_MESSAGE, e.getMessage()); return map; } if (data !=
	 * null && !data.isEmpty()) { map = new HashMap<>(); map.put(ERROR_CODE, 0);
	 * map.put(ERROR_MESSAGE, MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC,
	 * MESSAGE_SUCSESS); map.put("AuctionList", data); map.put("Reviews",
	 * reviewList); map.put("TotalCounts", totalCounts); return map;
	 * 
	 * } else { map = new HashMap<>(); map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS); return map; } }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @Override public Map<String, Object> getAllAuctionDetails(long
	 * auctionId,long customerId,long auctionDetaiilsId,int firstIndex,int
	 * rowPerPage,long fromDate,long toDate) { Map<String, Object> map = null;
	 * List<AuctionDetails> auctionDetails = null; long totalCounts = 0; try {
	 * Session session = openDBsession(); boolean flag=false; Criteria criteria
	 * = session.createCriteria(AuctionDetails.class); StringBuffer strBfr = new
	 * StringBuffer("select count(*) from AuctionDetails ad"); if (auctionId >
	 * 0) { flag = true; strBfr.append(" where ad.auctionId=" + auctionId);
	 * criteria.add(Restrictions.eq("auctionId", auctionId)); } if
	 * (auctionDetaiilsId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" ad.auctionDetailsId=" + auctionDetaiilsId);
	 * criteria.add(Restrictions.eq("auctionDetailsId", auctionDetaiilsId)); }
	 * if (customerId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" ad.userId=" + customerId);
	 * criteria.add(Restrictions.eq("customerId", customerId)); } if (fromDate >
	 * 0) { if (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" o.lastUpdatedTime between " +
	 * fromDate+" and "+toDate);
	 * criteria.add(Restrictions.between("lastUpdatedTime", fromDate, toDate));
	 * } if (flag == true) { strBfr.replace(28, strBfr.length(), ""); } Query
	 * qry = session.createQuery(strBfr.toString()); totalCounts = (long)
	 * qry.uniqueResult(); session.flush();
	 * criteria.setProjection(Projections.distinct(Projections.id()));
	 * if(rowPerPage > 0) {
	 * 
	 * criteria.setFirstResult(firstIndex); criteria.setMaxResults(rowPerPage);
	 * } List<Long> ids = criteria.list(); session.flush(); session.clear(); if
	 * (ids != null && !ids.isEmpty()) { criteria =
	 * session.createCriteria(AuctionDetails.class);
	 * criteria.add(Restrictions.in("id", ids));
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
	 * auctionDetails = criteria.list(); session.flush(); }
	 * 
	 * } catch (HibernateException e) { logger.info(e.getMessage());
	 * e.printStackTrace(); throw e; } if (auctionDetails != null &&
	 * !auctionDetails.isEmpty()) { map = new HashMap<>(); map.put(ERROR_CODE,
	 * 0); map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR); map.put(DATA,
	 * auctionDetails); //map.put("TotalCounts", totalCounts); return map;
	 * 
	 * } else { map = new HashMap<>(); map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR); return map; }
	 * }
	 */

}
