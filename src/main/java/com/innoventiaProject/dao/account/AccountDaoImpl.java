package com.innoventiaProject.dao.account;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.entity.account.Expense;
import com.innoventiaProject.entity.account.Income;
import com.innoventiaProject.entity.account.IncomeExpenseHead;
import com.innoventiaProject.entity.order.Transactions;

@EnableTransactionManagement
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
@Repository("accountDao")
public class AccountDaoImpl implements AccountDao, Constants {

	private static Logger logger = Logger.getLogger(AccountDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HibernateTemplate hibernateTemplate;

	private Session openDBsession() {
		Session session;
		session = sessionFactory.getCurrentSession();
		return session;
	}

	@Override
	public Map<String, Object> saveorUpdateIncomeExpenseHead(IncomeExpenseHead head) {

		Map<String, Object> map = null;
		List<IncomeExpenseHead> headList = null;
		try {
			Session session = openDBsession();
			if (head.getIncomExpnsHeadId() <= 0) {
				Criteria criteria = session.createCriteria(IncomeExpenseHead.class);
				criteria.add(Restrictions.eq("incomeExpenseName", head.getIncomeExpenseName()));
				criteria.add(Restrictions.eq("incomeOrExpense", head.getIncomeOrExpense()));
				headList = criteria.list();
				if (!headList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -9);
					map.put(ERROR_MESSAGE, "income/expense head already exist");
					return map;
				} else {
					map = new HashMap<>();
					session.save(head);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, "income/expense head successfully added");
					return map;

				}
			} else {
				logger.info("_________id---------------" + head.getIncomExpnsHeadId());
				Criteria criteria = session.createCriteria(IncomeExpenseHead.class);
				criteria.add(Restrictions.eq("incomExpnsHeadId", head.getIncomExpnsHeadId()));
				headList = criteria.list();
				if (headList == null || headList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -9);
					map.put(ERROR_MESSAGE, "No such income/expense head");
					return map;
				} else {
					logger.info("__________merge---------------");
					session.merge(head);
					session.flush();
				}
			}
			/*
			 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
			 * MESSAGE_SUCCESS);
			 */
		}

		catch (Exception e) {
			e.printStackTrace();
			logger.debug(e);
			// brand = null;
			throw e;
		}
		// return map;

		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, "income/expense head updated successfully");
		// map.put("ProgressReport", progRport);
		return map;

	}

	@Override
	public Map<String, Object> getExpenseIncomeHead(int incomeOrExpense, long incomExpnsHeadId, int status) {

		BasicResponse res = new BasicResponse();
		res.errorCode = 0;
		res.errorMessage = "Success";
		Map<String, Object> expenseMap = null;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(IncomeExpenseHead.class);
			if (incomeOrExpense > 0) {
				criteria.add(Restrictions.eq("incomeOrExpense", incomeOrExpense));
			}
			if (status > 0) {
				criteria.add(Restrictions.eq("status", status));
			}
			if (incomExpnsHeadId > 0) {
				criteria.add(Restrictions.eq("incomExpnsHeadId", incomExpnsHeadId));
			}
			List<IncomeExpenseHead> headList = criteria.list();
			logger.info("problem with observelist" + headList.size());
			session.flush();
			if (headList == null || headList.isEmpty()) {
				expenseMap = new HashMap<>();
				expenseMap.put(ERROR_CODE, -1);
				expenseMap.put(ERROR_MESSAGE, "No  such income/expense head avialble");
				return expenseMap;
			} else {
				expenseMap = new HashMap<String, Object>();
				expenseMap.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				expenseMap.put(ERROR_MESSAGE, "Success");
				expenseMap.put("HeadList", headList);
				return expenseMap;
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			res.errorCode = 50;
			res.errorMessage = e.getMessage();
			throw e;
		}

	}

	@Override
	public Map<String, Object> saveorUpdateExpense(List<Expense> expenseList, List<Transactions> transList) {

		Map<String, Object> map = null;

		try {

			Session session = openDBsession();
			if (expenseList != null && expenseList.size() > 0) {
				for (int i = 0; i < expenseList.size(); i++) {

					Transactions trans = transList.get(i);
					Expense expense = expenseList.get(i);
					Criteria cr1 = session.createCriteria(Expense.class);
					cr1.add(Restrictions.eq("date_expense", expense.getDate_expense()));
					cr1.add(Restrictions.eq("expenseId", expense.getExpenseId()));
					Expense collectionDetails = (Expense) cr1.uniqueResult();
					if (collectionDetails != null) {

						String q31111 = " select count(*) from  Transactions";
						Query q4211 = session.createQuery(q31111);
						long count = (long) q4211.uniqueResult();

						/*if (count > 0)

						{

							String q3111 = " select MAX(lastUpdatedTime) from  Transactions where status=1";
							Query q421 = session.createQuery(q3111);
							long lastUpdatedTimemax1 = (long) q421.uniqueResult();

							String q3112 = " select t.reciptNo from  Transactions t where t.lastUpdatedTime =:lastUpdatedTime and t.status=1   ";
							Query q411 = session.createQuery(q3112);
							q411.setParameter("lastUpdatedTime", lastUpdatedTimemax1);
							String reciptNo1 = (String) q411.uniqueResult();
							logger.info(reciptNo1 + "reciptNo1");
							StringBuilder sb = new StringBuilder(reciptNo1);

							sb.delete(0, 3);
							String reciptNo2 = sb.toString();

							long rec = Long.parseLong(reciptNo2);

							String recp = Long.toString(rec + 1);

							trans.setReciptNo("PNT" + recp);
						} else {
							trans.setReciptNo("PNT1");
						}*/

						session.save(trans);
						session.flush();

					} else {
						logger.info("reciptNo2__________________reciptNo2_________________reciptNo2");
						String q31111 = " select count(*) from  Transactions";
						Query q4211 = session.createQuery(q31111);
						long count = (long) q4211.uniqueResult();

						/*if (count > 0)

						{
							logger.info("reciptNo2__________________reciptNo2_________________reciptNo2");
							String q3111 = " select MAX(lastUpdatedTime) from  Transactions where status=1";
							Query q421 = session.createQuery(q3111);
							long lastUpdatedTimemax1 = (long) q421.uniqueResult();

							String q3112 = " select t.reciptNo from  Transactions t where t.lastUpdatedTime =:lastUpdatedTime and t.status=1 ";
							Query q411 = session.createQuery(q3112);
							q411.setParameter("lastUpdatedTime", lastUpdatedTimemax1);
							String reciptNo1 = (String) q411.uniqueResult();
							logger.info(reciptNo1 + "reciptNo1");
							StringBuilder sb = new StringBuilder(reciptNo1);

							sb.delete(0, 3);
							String reciptNo2 = sb.toString();
							logger.info(reciptNo2 + "reciptNo2__________________reciptNo2_________________reciptNo2");
							long rec = Long.parseLong(reciptNo2);

							String recp = Long.toString(rec + 1);

							trans.setReciptNo("PNT" + recp);
						} else {
							trans.setReciptNo("PNT1");
						}*/
						session.save(trans);
						session.flush();
						expense.setTransactionId(trans.getTransactionId());
						session.save(expense);
						session.flush();

					}
				}
			}

			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, "Expense added successfully");

		} catch (Exception e) {
			e.printStackTrace();
			map = new HashMap<>();
			map.put(ERROR_CODE, -7);
			map.put(ERROR_MESSAGE, e.getMessage());
		}
		return map;

	}

	@Override
	public Map<String, Object> saveorUpdateIncome(List<Income> incomeList, List<Transactions> transList) {

		Map<String, Object> map = null;

		try {

			Session session = openDBsession();

			if (incomeList != null && incomeList.size() > 0) {
				for (int i = 0; i < incomeList.size(); i++) {
					Income income = incomeList.get(i);
					Transactions trans = transList.get(i);
					logger.info("reciptNo2__________________reciptNo2_________________reciptNo2");
					String q31111 = " select count(*) from  Transactions";
					Query q4211 = session.createQuery(q31111);
					long count = (long) q4211.uniqueResult();

				/*	if (count > 0)

					{
						logger.info("reciptNo2__________________reciptNo2_________________reciptNo2");
						String q3111 = " select MAX(lastUpdatedTime) from  Transactions where status=1";
						Query q421 = session.createQuery(q3111);
						long lastUpdatedTimemax1 = (long) q421.uniqueResult();
						logger.info(lastUpdatedTimemax1 + "lastUpdatedTimemax1");
						String q3112 = " select t.reciptNo from  Transactions t where t.lastUpdatedTime =:lastUpdatedTime and t.status=1  ";
						Query q411 = session.createQuery(q3112);
						q411.setParameter("lastUpdatedTime", lastUpdatedTimemax1);
						String reciptNo1 = (String) q411.uniqueResult();
						logger.info(reciptNo1 + "reciptNo1");
						StringBuilder sb = new StringBuilder(reciptNo1);

						sb.delete(0, 3);
						String reciptNo2 = sb.toString();
						logger.info(reciptNo2 + "reciptNo2__________________reciptNo2_________________reciptNo2");
						long rec = Long.parseLong(reciptNo2);

						String recp = Long.toString(rec + 1);

						trans.setReciptNo("PNT" + recp);
					} else {
						trans.setReciptNo("PNT1");
					}*/
					session.save(trans);
					session.flush();

					// } else {
					session.save(trans);
					session.flush();
					income.setTransactionId(trans.getTransactionId());
					session.save(income);
					session.flush();
					// trans.setPaired_id(feeCollection.getFeeCollectId());

					// }
				}
			}

			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, "Income added successfully");

		} catch (Exception e) {
			e.printStackTrace();
			map = new HashMap<>();
			map.put(ERROR_CODE, -7);
			map.put(ERROR_MESSAGE, e.getMessage());
		}
		return map;

	}

	
	@Override
	public Map<String, Object> getIncomes(int currentIndex, int rowPerPage, long incomeId, int status) {
		BasicResponse res = new BasicResponse();
		res.errorCode = 0;
		res.errorMessage = "Success";
		Map<String, Object> incomeMap = null;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Income.class);
			if (incomeId > 0) {
				criteria.add(Restrictions.eq("incomeId", incomeId));
			}
			if (status > 0) {
				criteria.add(Restrictions.eq("status", status));
			}
			criteria.setFirstResult(currentIndex);
			criteria.setMaxResults(rowPerPage);
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List<Income> feeList = criteria.list();
			logger.info("problem with observelist" + feeList.size());
			session.flush();

			Query aCountQuery = session.createQuery("select count(*) from Income");
			long totalRecords = (Long) aCountQuery.uniqueResult();

			incomeMap = new HashMap<String, Object>();
			incomeMap.put("IncomeList", feeList);

			incomeMap.put("TotalCount", totalRecords);

			session.flush();
		} catch (HibernateException e) {
			e.printStackTrace();
			res.errorCode = 50;
			res.errorMessage = e.getMessage();
			throw e;
		}
		return incomeMap;

	}

	@Override
	public Map<String, Object> getExpenses(int currentIndex, int rowPerPage, long expenseId, int status) {

		BasicResponse res = new BasicResponse();
		res.errorCode = 0;
		res.errorMessage = "Success";
		Map<String, Object> expenseMap = null;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Expense.class);
			if (expenseId > 0) {
				criteria.add(Restrictions.eq("expenseId", expenseId));
			}
			if (status > 0) {
				criteria.add(Restrictions.eq("status", status));
			}
			criteria.setFirstResult(currentIndex);
			criteria.setMaxResults(rowPerPage);
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List<Expense> expenseList = criteria.list();
			logger.info("problem with observelist" + expenseList.size());
			session.flush();

			Query aCountQuery = session.createQuery("select count(*) from Expense");
			long totalRecords = (Long) aCountQuery.uniqueResult();

			expenseMap = new HashMap<String, Object>();
			expenseMap.put("ExpenseList", expenseList);

			expenseMap.put("TotalCount", totalRecords);

			session.flush();
		} catch (HibernateException e) {
			e.printStackTrace();
			res.errorCode = 50;
			res.errorMessage = e.getMessage();
			throw e;
		}
		return expenseMap;

	}

	@Override
	public List<IncomeExpenseHead> getAllIncExpHeadNmaesList(long incomExpnsHeadId) {

		Session session = openDBsession();
		List<IncomeExpenseHead> itemnameList = null;
		try {
			Criteria criteria = session.createCriteria(IncomeExpenseHead.class);
			if (incomExpnsHeadId > 0) {
				criteria.add(Restrictions.eq("incomExpnsHeadId", incomExpnsHeadId));
			}
			itemnameList = criteria.list();
			session.flush();
		} catch (HibernateException e) {
			logger.error(e);
			if (TESTING) {
				e.printStackTrace();
			}
			throw e;
		}
		return itemnameList;

	}

}
