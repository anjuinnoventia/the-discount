package com.innoventiaProject.dao.input;

import java.util.List;
import java.util.Map;

import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.entity.input.Advertisement;
import com.innoventiaProject.entity.input.AttributeDetails;
import com.innoventiaProject.entity.input.Attributes;
import com.innoventiaProject.entity.input.Category;
import com.innoventiaProject.entity.input.City;
import com.innoventiaProject.entity.input.CmsMangement;
import com.innoventiaProject.entity.input.ConnectedProducts;
import com.innoventiaProject.entity.input.Country;
import com.innoventiaProject.entity.input.FeaturesAndPhotosProduct;
import com.innoventiaProject.entity.input.Manufacture;
import com.innoventiaProject.entity.input.Offer;
import com.innoventiaProject.entity.input.OfferDetails;
import com.innoventiaProject.entity.input.Product;
import com.innoventiaProject.entity.input.ProductAttributes;
import com.innoventiaProject.entity.input.Review;
import com.innoventiaProject.entity.input.SubCategory;
import com.innoventiaProject.entity.user.CustomerDetails;

public interface InputDao {

	public Map<String,Object> saveCategory(Category category);
	public Map<String,Object> saveCms(CmsMangement cms);
	public Map<String,Object> saveManufacture(Manufacture manufacture);
	 public Map<String,Object> getAllCountry(long countryId);
	 public Map<String,Object> getAllCategories(long categoryId,long mainCategoryId,int status,long companyId);
	 public Map<String,Object> getAllCity(long countryId,long cityId,String cityName, int firstIndex, int rowPerPage,int status);
	 public Map<String,Object>  getAllOffer(long offerId, long offerEnds, long offerStarts,int status,String offerName,String offerNameAr, int firstIndex, int rowPerPage);
	 public List<Offer>  getAllOfferId(long offerId);
	   public Map<String,Object> getAllCms(long cmsId,int rowPerPage,int currentIndex);
	 public Map<String,Object>  getAllOfferProduct(long offerId);
	 public  Map<String,Object> getAttributes(String attributeName,String attributeNameAr,int firstIndex, int rowPerPage,int status);
	 List<Manufacture>   getallManufactureId(long manufactureId);
	 public Map<String,Object> getallManufacture(long manufactureId, String manufactureName,int status, int firstIndex, int rowPerPage,long companyId);
	 
	 public Map<String,Object> getAllProducts(long productId,long sellerId,long userId,long mainCategoryId,long categoryId,
			 long countryId,long cityId,long subcategoryId,int firstIndex, int rowPerPage,int status,String  productName,long companyId,long numberOfItemsRemaining,long offerId,long manufacturerId,double priceStarts,double priceEnds,long productAttributeId,String value,String  productNameAr,int data,Map<Long, List<String>> attibutesSearch,int featured,int rank);  
	 public Map<String, Object> changeReturnStatus(long returnItemId, int status);
	 public Map<String,Object> getProductswithoutOffer(int rowPerPage,int firstIndex);
	 
	 public Map<String,Object> saveSubCategory(SubCategory category);
	public  Map<String,Object> addOffer(Offer offer);
	public Map<String, Object> saveProduct(Product service,List<FeaturesAndPhotosProduct> featuresAndPhotos,List<ConnectedProducts> connectedProducts,List<ProductAttributes> productAttributes,Object[] attributeIds,List<OfferDetails> offerDetails,List<AttributeDetails> atributeDetails);
	//public Map<String,Object> saveShop(Store shop,List<FeaturesAndPhotosShop> featuresAndPhotos);
	public Map<String,Object> saveReview(Review review);
	public Map<String,Object>  addComments(String comments,long reviewId);
	//  public Map<String,Object> getAllItemRequest(long requestItemId,long userId);
	//public Map<String, Object> getAllShopsBySellerId(long sellerId, int firstIndex, int rowPerPage);
	public  Map<String,Object> saveOrUpdateCity(City city);
	public  Map<String,Object> saveOrUpdateCountry(Country country);
	//public Map<String,Object> getAllProducts(long productId,long sellerId,long userId,long mainCategoryId,long categoryId,long subCategoryId,long countryId,long cityId,int firstIndex,int rowPerPage);
	 public Map<String,Object> getAllAdds(int addSatus,long storeUserId,long addId,long  modeId,long arabicOrEnglish);
	public Map<String, Object> changeProductStatus(long productId,int status);
	public Map<String, Object> changeShipppingStatus(long shippingId,int status);
	public Map<String, Object> updateProduct(double p,long ratingPercentage,long productId);
	//public Map<String,Object> requestItems(RequestItems requestItems);
	//public Map<String,Object> deleteReview(long reviewId,int mainOrSub,long userId);
	public Map<String, Object> getAllReviews(long fatherId,long cityId,long countryId,long customerId,long sellerId, int currentIndex, int rowPerPage,long fromDate,long toDate);
	//public Map<String,Object> getAllCategories(long categoryId,long mainCategoryId);
	//public Map<String,Object> getAllProducts(long productId,long shopId,long userId,long mainCategoryId,long categoryId,long subCategoryId,int firstIndex,int rowPerPage);
	//public Map<String,Object> getAllShops(long shopId,long mainCategory,long categoryId,long subCategoryId,int firstIndex,int rowPerPage,long fromDate,long toDate);
	 public Map<String,Object> getAllCountry(long countryId,int status);
	 public Map<String,Object>  readNotification(long userId);
	 
	public Map<String, Object> getAllProductsByUserId(long userId,int firstIndex,int rowPerPage);
	 public Map<String,Object> saveAddvertise(Advertisement advertisement);
	Map<String, Object> getCategoryFromMainCategoryId(long categoryId);
	public Map<String, Object> shareOrLike(StringBuffer str,Review likeOrShareReview);
	public Map<String,Object> getSubscribes(long subscribeId);
	public Map<String,Object> getBookingDetail(long bookingId,int status);
	List<Category> getAllCategoryList(long mainCategoryId);
	List<CustomerDetails> getAllCustomerName(long userId);
	List<SubCategory> getAllSubCategoryList(long mainCategoryId,long categoryId,long companyId);
//	List<MainCategory> getAllMainCategoryList();
	//public Map<String, Object> getAllReviews(long fatherId,int productOrShop,int firstIndex,int rowPerPage);
//	public Map<String, Object> saveMainCategory(MainCategory mainCategory);
	public Map<String, Object> getAllDetails(long userId,long id,int prodOrShop);
	public Map<String, Object> updateStock(long productId,long productQuantity);
	//public List<Store> getAllShopsByShopId(long shopId);

	public Map<String, Object> searchData(Map<Integer, String> data, int mode, int firstIndex, int rowPerPage, double latitude,
			double longitude);
	public Map<String, Object> filterData(int mode, int firstIndex, int rowPerPage,long categoryId);
  //  public Map<String, Object>  saveRequestItem(RequestItems requestItems);
 //   public Map<String, Object> editRequestItem(long requestItemId,int status);
    public Map<String,Object> saveOrUpdateAttribute(Attributes attributes);
    public Map<String,Object> getAllAttriubes(long attrbuteId,int status);
    public Map<String,Object> getAllAttributevalues(long attrbuteId,int status,int currentIndex,int rowPerPage);
    
	 
}
