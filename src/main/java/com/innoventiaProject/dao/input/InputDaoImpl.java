package com.innoventiaProject.dao.input;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.discount.model.input.AttributeShowVo;
import com.innoventiaProject.discount.model.input.ReviewVo;
import com.innoventiaProject.discount.utils.TimeUtils;
import com.innoventiaProject.entity.input.Advertisement;
import com.innoventiaProject.entity.input.AttributeDetails;
import com.innoventiaProject.entity.input.Attributes;
import com.innoventiaProject.entity.input.Category;
import com.innoventiaProject.entity.input.City;
import com.innoventiaProject.entity.input.CmsMangement;
import com.innoventiaProject.entity.input.ConnectedProducts;
import com.innoventiaProject.entity.input.Country;
import com.innoventiaProject.entity.input.FeaturesAndPhotosProduct;
import com.innoventiaProject.entity.input.Manufacture;
import com.innoventiaProject.entity.input.Offer;
import com.innoventiaProject.entity.input.OfferDetails;
import com.innoventiaProject.entity.input.Product;
import com.innoventiaProject.entity.input.ProductAttributes;
import com.innoventiaProject.entity.input.ReturnItems;
import com.innoventiaProject.entity.input.Review;
import com.innoventiaProject.entity.input.SellerDetails;
import com.innoventiaProject.entity.input.SubCategory;
import com.innoventiaProject.entity.order.OrderDetails;
import com.innoventiaProject.entity.order.ShippingDetails;
import com.innoventiaProject.entity.order.TaxManagement;
import com.innoventiaProject.entity.user.CustomerDetails;
import com.innoventiaProject.entity.user.LoginDetails;

@EnableTransactionManagement
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
@Repository("inputDao")
public class InputDaoImpl implements InputDao, Constants {
	private static Logger logger = Logger.getLogger(InputDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HibernateTemplate hibernateTemplate;

	private Session openDBsession() {
		Session session;
		session = sessionFactory.getCurrentSession();
		return session;
	}

	@Override
	public Map<String, Object> saveAddvertise(Advertisement advertisement) {
		Map<String, Object> map = new HashMap<>();
		try {
			Session session = openDBsession();
			if (advertisement.getAddId() > 0) {
				session.update(advertisement);
				session.flush();
				map.put(ERROR_MESSAGE, MESSAGE_UPDATED_SUCESSFULLY);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATED_SUCESSFULLY_AR);
			} else {
				session.save(advertisement);
				session.flush();
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			}
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			return map;
		} catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		}
	}

	/*
	 * private void closeDBSession() { if (session.isOpen()) { session.flush();
	 * session.close(); } }
	 */

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> saveMainCategory(MainCategory
	 * mainCategory) { Map<String, Object> map = null; List<MainCategory>
	 * categoryList = null; try { Session session = openDBsession();
	 * DetachedCriteria detachedCriteria =
	 * DetachedCriteria.forClass(MainCategory.class);
	 * detachedCriteria.add(Restrictions.eq("mainCategoryName",
	 * mainCategory.getMainCategoryName())); categoryList = (List<MainCategory>)
	 * hibernateTemplate.findByCriteria(detachedCriteria); map = new HashMap<>(); if
	 * (categoryList == null || categoryList.isEmpty() ||
	 * mainCategory.getMainCategoryId() > 0) { session.clear();
	 * session.saveOrUpdate(mainCategory); session.flush(); map.put(ERROR_CODE,
	 * ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE, MESSAGE_SUCSESS); } else {
	 * map.put(ERROR_CODE, -3); map.put(ERROR_MESSAGE, "DUBLICATE DATA"); } Criteria
	 * criteria = session.createCriteria(MainCategory.class); categoryList =
	 * criteria.list(); session.flush(); map.put("CategoryList", categoryList); //
	 * closeDBSession(); } catch (Exception e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } logger.info(e.getMessage()); map = new HashMap<>();
	 * map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE, e.getMessage()); return map;
	 * } return map; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllAdds(int addSatus, long storeUserId, long addId, long modeId,long arabicOrEnglish) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		try {
			/*long currentMill = TimeUtils.instance().getStartOfCurrentDay();
			Query qry = session.createQuery(
					"update from Advertisement set addStatus=3 where addExpirtyDate <= :date and addStatus=2");
			qry.setParameter("date", currentMill);
			int result = qry.executeUpdate();
			if (result < 0) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -7);
				map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
				return map;
			}*/
			Criteria criteria = session.createCriteria(Advertisement.class);
			if (addSatus > 0) {
				criteria.add(Restrictions.eq("addStatus", addSatus));
			}

			
			if (storeUserId > 0) {
				criteria.add(Restrictions.eq("sellerId", storeUserId));
			}
			if (modeId > 0) {
				criteria.add(Restrictions.eq("modeId", modeId));
			}
			if (addId > 0) {
				criteria.add(Restrictions.eq("addId", addId));
			}
			 
			 if (arabicOrEnglish == 1 || arabicOrEnglish == 0) {
					criteria.add(Restrictions.eq("arabicOrEnglish", arabicOrEnglish));
				}
			

			criteria.add(Restrictions.eq("status", ACTIVE));
			List<Advertisement> adList = criteria.list();
			session.flush();
			if (adList != null && !adList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				map.put(DATA, adList);
				return map;
			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -3);
				map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
				return map;
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveCategory(Category category) {
		Map<String, Object> map = null;
		List<Category> categoryList = null;
		try {
			Session session = openDBsession();
			if (category.getCategoryId() <= 0) {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Category.class);
				detachedCriteria.add(Restrictions.eq("categoryName", category.getCategoryName()));
				//detachedCriteria.add(Restrictions.eq("companyId", category.getCompanyId()));
				categoryList = (List<Category>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (categoryList.isEmpty()) {
					session.save(category);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­ ÙˆØ¸Ø§Ø¦Ù�");
					return map;
				} else {
					map.put(ERROR_CODE, -6);
					map.put(ERROR_MESSAGE, "Same category name already exist");
					map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­ ÙˆØ¸Ø§Ø¦Ù�");
					return map;
				}
			} else {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Category.class);
				detachedCriteria.add(Restrictions.eq("categoryId", category.getCategoryId()));
				//detachedCriteria.add(Restrictions.eq("rank", category.getRank()));
				categoryList = (List<Category>) hibernateTemplate.findByCriteria(detachedCriteria);
				if (categoryList == null || categoryList.isEmpty()) 
				{
					map = new HashMap<>();
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such category");
					map.put(ERROR_MESSAGE_ARABIC, "Ù„Ø§ ÙŠÙˆØ¬Ø¯ Ù…Ø«Ù„ Ù‡Ø°Ù‡ Ø§Ù„Ù�Ø¦Ø©");
					return map;
				} else {
					session.merge(category);
					session.flush();
				}
				
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			category = null;
			throw e;
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put("category", category);
		return map;

	}

	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveCms(CmsMangement cms) {
		Map<String, Object> map = null;
		List<CmsMangement> offerList = null;
		try {
			Session session = openDBsession();
			DetachedCriteria detachedCriteria = null;
			if (cms.getCmsId() <= 0) {
				detachedCriteria = DetachedCriteria.forClass(CmsMangement.class);

				offerList = (List<CmsMangement>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();

				if (offerList == null || offerList.isEmpty()) {
					session.save(cms);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ã™â€ Ã˜Â¬Ã˜Â§Ã˜Â­ Ã™Ë†Ã˜Â¸Ã˜Â§Ã˜Â¦Ã™ï¿½");
					return map;
				} else {
					map.put(ERROR_CODE, -3);
					map.put(ERROR_MESSAGE, "DUBLICATE DATA");
					map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â¨Ã™Å Ã˜Â§Ã™â€ Ã˜Â§Ã˜Âª Ã™â€¦Ã˜ÂªÃ™Æ’Ã˜Â±Ã˜Â±Ã˜Â©");
					return map;
				}
			} else {
				detachedCriteria = DetachedCriteria.forClass(CmsMangement.class);
				detachedCriteria.add(Restrictions.eq("cmsId", cms.getCmsId()));
				offerList = (List<CmsMangement>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (offerList == null || offerList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such offer");
					map.put(ERROR_MESSAGE_ARABIC,
							"Ã™â€žÃ˜Â§ Ã™Å Ã™Ë†Ã˜Â¬Ã˜Â¯ Ã˜Â¨Ã™â€žÃ˜Â¯ Ã™â€¦Ã™â€  Ã™â€¡Ã˜Â°Ã˜Â§ Ã˜Â§Ã™â€žÃ™â€šÃ˜Â¨Ã™Å Ã™â€ž");
					return map;
				} else {

					session.merge(cms);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ã™â€ Ã˜Â¬Ã˜Â§Ã˜Â­ Ã™Ë†Ã˜Â¸Ã˜Â§Ã˜Â¦Ã™ï¿½");
					map.put("CMS", cms);
					return map;
				}
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			cms = null;
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveManufacture(Manufacture manufacture)

	{
		Map<String, Object> map = null;
		List<Manufacture> manufactureList = null;
		try {
			Session session = openDBsession();
			if (manufacture.getManufacturerId() <= 0) {
				/*
				 * DetachedCriteria detachedCriteria =
				 * DetachedCriteria.forClass(Manufacture.class);
				 * detachedCriteria.add(Restrictions.eq("color", manufacture.getColor()));
				 * manufactureList = (List<Manufacture>)
				 * hibernateTemplate.findByCriteria(detachedCriteria); if
				 * (!manufactureList.isEmpty()) { map = new HashMap<>(); map.put(ERROR_CODE,
				 * -9); map.put(ERROR_MESSAGE, "Color already in server");
				 * map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­ ÙˆØ¸Ø§Ø¦Ù�"); return map; }
				 */
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Manufacture.class);
				detachedCriteria.add(Restrictions.eq("manufacturerName", manufacture.getManufacturerName()));
				detachedCriteria.add(Restrictions.eq("companyId", manufacture.getCompanyId()));
				manufactureList = (List<Manufacture>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (manufactureList.isEmpty()) {
					session.save(manufacture);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­ ÙˆØ¸Ø§Ø¦Ù�");
					return map;
				} else {
					map.put(ERROR_CODE, -6);
					map.put(ERROR_MESSAGE, "Same manufacturer name already exist");
					map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­ ÙˆØ¸Ø§Ø¦Ù�");
					return map;
				}
			} else {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Manufacture.class);
				detachedCriteria.add(Restrictions.eq("manufacturerId", manufacture.getManufacturerId()));
				manufactureList = (List<Manufacture>) hibernateTemplate.findByCriteria(detachedCriteria);
				if (manufactureList == null || manufactureList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such manufacturer");
					map.put(ERROR_MESSAGE_ARABIC, "Ù„Ø§ ÙŠÙˆØ¬Ø¯ Ù…Ø«Ù„ Ù‡Ø°Ù‡ Ø§Ù„Ù�Ø¦Ø©");
					return map;
				} else {
					session.merge(manufacture);
					session.flush();
				}
			}
		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			manufacture = null;
			throw e;
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put("manufacture", manufacture);
		return map;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> addOffer(Offer offer) {
		Map<String, Object> map = null;
		List<Offer> offerList = null;
		try {
			Session session = openDBsession();
			DetachedCriteria detachedCriteria = null;
			if (offer.getOfferId() <= 0) {
				detachedCriteria = DetachedCriteria.forClass(Offer.class);
				detachedCriteria.add(Restrictions.eq("offerName", offer.getOfferName()));
				detachedCriteria.add(Restrictions.eq("offerNameAr", offer.getOfferNameAr()));
				offerList = (List<Offer>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();

				if (offerList == null || offerList.isEmpty()) {
					session.save(offer);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ã™â€ Ã˜Â¬Ã˜Â§Ã˜Â­ Ã™Ë†Ã˜Â¸Ã˜Â§Ã˜Â¦Ã™ï¿½");
					return map;
				} else {
					map.put(ERROR_CODE, -3);
					map.put(ERROR_MESSAGE, "DUBLICATE DATA");
					map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â¨Ã™Å Ã˜Â§Ã™â€ Ã˜Â§Ã˜Âª Ã™â€¦Ã˜ÂªÃ™Æ’Ã˜Â±Ã˜Â±Ã˜Â©");
					return map;
				}
			} else {
				detachedCriteria = DetachedCriteria.forClass(Offer.class);
				detachedCriteria.add(Restrictions.eq("offerId", offer.getOfferId()));
				offerList = (List<Offer>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (offerList == null || offerList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such offer");
					map.put(ERROR_MESSAGE_ARABIC,
							"Ã™â€žÃ˜Â§ Ã™Å Ã™Ë†Ã˜Â¬Ã˜Â¯ Ã˜Â¨Ã™â€žÃ˜Â¯ Ã™â€¦Ã™â€  Ã™â€¡Ã˜Â°Ã˜Â§ Ã˜Â§Ã™â€žÃ™â€šÃ˜Â¨Ã™Å Ã™â€ž");
					return map;
				} else {
					offer.setOfferStarts(offer.getOfferStarts());
					offer.setOfferEnds(offer.getOfferEnds());
					logger.info(offer.getOfferEnds() + "offer.getOfferEnds()");
					session.merge(offer);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ã™â€ Ã˜Â¬Ã˜Â§Ã˜Â­ Ã™Ë†Ã˜Â¸Ã˜Â§Ã˜Â¦Ã™ï¿½");
					map.put("offer", offer);
					return map;
				}
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			offer = null;
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveProduct(Product product, List<FeaturesAndPhotosProduct> featuresAndPhotos,
			List<ConnectedProducts> connectedProducts, List<ProductAttributes> productAttributes, Object[] attributeIds,
			List<OfferDetails> offerDetails, List<AttributeDetails> atributeDetails)

	{

		double taxPercentage = 0.0;
		Map<String, Object> map = null;
		try {

			Session session = openDBsession();
			Query query = session.createQuery("from SellerDetails s where s.sellerId=?");
			query.setParameter(0, product.getSellerId());
			List<SellerDetails> sellers = query.list();
			session.flush();
			if (sellers == null || sellers.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "No such seller");
				map.put(ERROR_MESSAGE_ARABIC, "لا يوجد مثل هذا المخزن");
				return map;
			}
			query = session.createQuery("from Category c where c.categoryId=?");
			query.setParameter(0, product.getCategoryId());
			List<Category> category = query.list();
			if (category == null || category.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "No such category");
				map.put(ERROR_MESSAGE_ARABIC, "لا يوجد مثل هذا المخزن");
				return map;
			}
			// -----------added

			query = session.createQuery("select count(*) from Attributes a where a.attributeId in (:ids)");
			query.setParameterList("ids", attributeIds);
			long attributeCount = (long) query.uniqueResult();

			if (attributeCount != attributeIds.length) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "Some attribute are not available or duplicate attribute applied");
				map.put(ERROR_MESSAGE_ARABIC, "لا يوجد مثل هذا المخزن");
				return map;
			}
			if (product.getSubCategoryId() > 0) {
				query = session.createQuery("from SubCategory c where c.subCategoryId=?");
				query.setParameter(0, product.getSubCategoryId());
				List<SubCategory> subCategory = query.list();
				if (subCategory == null || subCategory.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -1);
					map.put(ERROR_MESSAGE, "No such sub category");
					map.put(ERROR_MESSAGE_ARABIC, "لا يوجد مثل هذا المخزن");
					return map;
				}
			}

			if (product.getTaxId() > 0) {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TaxManagement.class);
				detachedCriteria.add(Restrictions.eq("taxId", product.getTaxId()));
				List<TaxManagement> productList = (List<TaxManagement>) hibernateTemplate
						.findByCriteria(detachedCriteria);
				if (productList == null || productList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such tax added before");
					map.put(ERROR_MESSAGE_ARABIC, "لم تتم إضافة أي منتج من هذا القبيل قبل ");
					return map;
				} else {
					logger.info(product.getTaxId() + "taxId");
					query = session.createQuery("select t.taxPercentage from TaxManagement t where taxId=:taxId");
					query.setParameter("taxId", product.getTaxId());
					taxPercentage = (double) query.uniqueResult();

				}
			}

			if (product.getProductId() > 0) {
				DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Product.class);
				detachedCriteria.add(Restrictions.eq("productId", product.getProductId()));
				List<Product> productList = (List<Product>) hibernateTemplate.findByCriteria(detachedCriteria);
				if (productList == null || productList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such product added before");
					map.put(ERROR_MESSAGE_ARABIC, "لم تتم إضافة أي منتج من هذا القبيل قبل ");
					return map;
				}
				Query qry = session.createQuery("delete from FeaturesAndPhotosProduct fp where fp.productId=:prodId");
				qry.setParameter("prodId", product.getProductId());
				int result = qry.executeUpdate();
				if (result < 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "Features updation failed");
					map.put(ERROR_MESSAGE_ARABIC, "فشل تحديث الميزات");
					return map;
				}
				session.flush();
				qry = session.createQuery("delete from ProductAttributes pa where pa.productId=:prodId");
				qry.setParameter("prodId", product.getProductId());
				result = qry.executeUpdate();
				if (result < 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "Features updation failed");
					map.put(ERROR_MESSAGE_ARABIC, "Ù�Ø´Ù„ ØªØ­Ø¯ÙŠØ« Ø§Ù„Ù…ÙŠØ²Ø§Øª");
					return map;
				}

				product.setOfferDetails(null);// ------added
				product.setSeller(null);

				double total = product.getPrice();

				session.merge(product);
				session.flush();
			} else {

				product.setFeatureAndPhotos(null);
				product.setOfferDetails(null);

				double total = product.getPrice();

				product.setTotalOfferAmount(total);

				session.save(product);
				session.flush();

			}
			for (FeaturesAndPhotosProduct fePhotos : featuresAndPhotos) {
				fePhotos.setProductId(product.getProductId());
				fePhotos.setProduct(null);
				session.save(fePhotos);
				session.flush();
			}
			for (ProductAttributes prodAtt : productAttributes) {
				prodAtt.setProductId(product.getProductId());
				prodAtt.setProduct(null);
				session.save(prodAtt);
				session.flush();

			}

			for (AttributeDetails prodDetAtt : atributeDetails) {
				logger.info(prodDetAtt + "prodDetAtt__________________prodDetAtt");
				// added------------------------------

				Criteria criteria = session.createCriteria(AttributeDetails.class);

				criteria.add(Restrictions.eq("attributeId", prodDetAtt.getAttributeId()));

				criteria.add(Restrictions.eq("value", prodDetAtt.getValue()));

				criteria.add(Restrictions.eq("valueAr", prodDetAtt.getValueAr()));

				AttributeDetails user = (AttributeDetails) criteria.uniqueResult();

				if (user == null) {
					session.save(prodDetAtt);
					session.flush();
				}

				// added-------------------------------------

			}

			// --------------added
			for (OfferDetails prodAtt1 : offerDetails) {
				prodAtt1.setProductId(product.getProductId());
				prodAtt1.setProduct(null);
				session.save(prodAtt1);
				session.flush();
			}
			// -----------added
			if (connectedProducts != null) {
				for (ConnectedProducts ctdProducts : connectedProducts) {
					ctdProducts.setProduct(null);
					ctdProducts.setProductId(product.getProductId());
					// ctdProducts.setRelatedProductId(0);
					session.save(ctdProducts);
					session.flush();
					if (ctdProducts.getRelatedProductId() != 0) {

						Query qry = session.createQuery(
								"from ConnectedProducts cp where cp.productId=:prodId and cp.relatedProductId=:relProdId and cp.modeId=:mode");
						qry.setParameter("prodId", ctdProducts.getRelatedProductId());
						qry.setParameter("relProdId", ctdProducts.getProductId());
						qry.setParameter("mode", ctdProducts.getModeId());
						List<ConnectedProducts> coList = qry.list();
						if (coList == null || coList.isEmpty()) {
							ConnectedProducts cProducts = new ConnectedProducts();
							cProducts.setLastUpdatedTime(ctdProducts.getLastUpdatedTime());
							cProducts.setModeId(ctdProducts.getModeId());
							cProducts.setProductId(ctdProducts.getRelatedProductId());
							cProducts.setRelatedProductId(ctdProducts.getProductId());
							cProducts.setProduct(null);
							session.save(cProducts);
							session.flush();
						}
					}
				}
			}
			map = new HashMap<String, Object>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");
		} catch (Exception e) {
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		return map;
	}

	/*
	 * @Override public Map<String, Object> saveShop(Store shop,
	 * List<FeaturesAndPhotosShop> featuresAndPhotos) { Map<String, Object> map =
	 * null; List<Store> storeList = null; try { Session session = openDBsession();
	 * DetachedCriteria detachedCriteria =
	 * DetachedCriteria.forClass(SellerDetails.class);
	 * detachedCriteria.add(Restrictions.eq("userId", shop.getUserId()));
	 * List<SellerDetails> sellerList = (List<SellerDetails>) hibernateTemplate
	 * .findByCriteria(detachedCriteria); if (sellerList == null ||
	 * sellerList.isEmpty()) { map = new HashMap<>(); map.put(ERROR_CODE, -4);
	 * map.put(ERROR_MESSAGE, "No such seller id found");
	 * map.put(ERROR_MESSAGE_ARABIC, "لم تتم إضافة أي منتج من هذا القبيل من قبل");
	 * return map; }
	 * 
	 * if (shop.getStoreId() > 0) { detachedCriteria =
	 * DetachedCriteria.forClass(Store.class);
	 * detachedCriteria.add(Restrictions.eq("storeId", shop.getStoreId()));
	 * storeList = (List<Store>) hibernateTemplate.findByCriteria(detachedCriteria);
	 * if (storeList == null || storeList.isEmpty()) { map = new HashMap<>();
	 * map.put(ERROR_CODE, -4); map.put(ERROR_MESSAGE,
	 * "No such product added before"); map.put(ERROR_MESSAGE_ARABIC,
	 * "لم تتم إضافة أي منتج من هذا القبيل من قبل"); return map; }
	 * 
	 * Query qry = session.
	 * createQuery("delete from FeaturesAndPhotosShop fs where fs.shopId=:shId");
	 * qry.setParameter("shId", shop.getStoreId()); int result =
	 * qry.executeUpdate(); if (result < 0) { map = new HashMap<>();
	 * map.put(ERROR_CODE, -4); map.put(ERROR_MESSAGE, "Features updation failed");
	 * map.put(ERROR_MESSAGE_ARABIC, "فشل تحديث الميزات");
	 * 
	 * return map;
	 * 
	 * } session.flush(); session.merge(shop); session.flush(); }
	 * 
	 * else {
	 * 
	 * session.save(shop); session.flush(); } for (FeaturesAndPhotosShop fePhotos :
	 * featuresAndPhotos) { // logger.info("shop id is:" + shop.getShopId());
	 * fePhotos.setShopId(shop.getStoreId()); // logger.info(shop.getShopId()+
	 * "shopid"); session.save(fePhotos); session.flush(); } map = new
	 * HashMap<String, Object>(); map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
	 * map.put(ERROR_MESSAGE, MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC,
	 * "نجاح"); // closeDBSession(); }
	 * 
	 * catch (Exception e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } logger.info(e.getMessage()); map = new HashMap<>();
	 * map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE, e.getMessage()); throw e; }
	 * map = new HashMap<>(); map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
	 * map.put(ERROR_MESSAGE, MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC,
	 * "نجاح"); return map; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveReview(Review review) {
		Map<String, Object> map = null;
		long noOfReviews = 0;
		double avgRating = 0.0;
		try {
			Session session = openDBsession();
			Query query = session.createQuery("from LoginDetails lg where lg.userId=? and role=?");
			query.setParameter(0, review.getUserId());
			query.setParameter(1, ROLL_ID_CUSTOMER);
			List<LoginDetails> logins = query.list();
			if (logins == null || logins.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, MESSAGE_NO_SUCH_USER);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_SUCH_USER_AR);
				return map;
			}
			query = session.createQuery("from Product p where p.productId=? and status=?");
			query.setParameter(0, review.getFatherId());
			query.setParameter(1, ACTIVE);
			List<Product> prods = query.list();
			if (prods == null || prods.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -3);
				map.put(ERROR_MESSAGE, "No such product available");
				map.put(ERROR_MESSAGE_ARABIC,
						"Ã™â€žÃ˜Â§ Ã™Å Ã™Ë†Ã˜Â¬Ã˜Â¯ Ã™â€¦Ã˜Â«Ã™â€ž Ã™â€¡Ã˜Â°Ã˜Â§ Ã˜Â§Ã™â€žÃ˜Â¨Ã™â€ Ã˜Â¯ Ã˜Â§Ã™â€žÃ™â€¦Ã˜ÂªÃ˜Â§Ã˜Â­Ã˜Â©");
				return map;
			}
			Criteria criteria = session.createCriteria(Review.class);
			criteria.add(Restrictions.eq("userId", review.getUserId()));
			criteria.add(Restrictions.eq("status", ACTIVE));
			criteria.add(Restrictions.eq("productOrShop", review.getProductOrShop()));
			criteria.add(Restrictions.eq("fatherId", review.getFatherId()));
			List<Review> rev = (List<Review>) criteria.list();
			session.flush();
			if (rev == null || rev.isEmpty()) {
				review.setProduct(null);
				session.save(review);
				session.flush();
				Query qry = session.createQuery(
						"select avg(rev.rating) from Review rev where rev.productOrShop=:prodOrsh and rev.fatherId=:fatherId");
				qry.setParameter("prodOrsh", review.getProductOrShop());
				qry.setParameter("fatherId", review.getFatherId());
				avgRating = (double) qry.uniqueResult();
				session.flush();
				session.clear();
				qry = session.createQuery(
						"select count(*) from Review rev where rev.productOrShop=:prodOrsh and rev.fatherId=:fatherId");
				qry.setParameter("prodOrsh", review.getProductOrShop());
				qry.setParameter("fatherId", review.getFatherId());

				noOfReviews = (long) qry.uniqueResult();
				session.flush();
				session.clear();
				if (review.getProductOrShop() == PRODUCT) {
					qry = session.createQuery(
							"update from Product p set p.rating=:rat,p.noOfReviews=:noRev where p.productId=:id");
				} else {
					qry = session.createQuery(
							"update from Seller sh set sh.rating=:rat,sh.noOfReviews=:noRev where sh.sellerId=:id");
				}

				qry.setParameter("id", review.getFatherId());
				qry.setParameter("noRev", noOfReviews);
				qry.setParameter("rat", avgRating);

				logger.info(review.getFatherId() + ":fid" + noOfReviews + ":no of reviews" + avgRating + ":rating");
				int result = qry.executeUpdate();
				session.flush();
				logger.info(result);
				if (result <= 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -6);
					map.put(ERROR_MESSAGE, "Product " + MESSAGE_UPDATE_FAILED);
					map.put(ERROR_MESSAGE, "Ã™â€ Ã˜ÂªÃ˜Â§Ã˜Â¬ " + MESSAGE_UPDATE_FAILED_AR);
					return map;
				}
			} else {
				map = new HashMap<>();
				map.put(ERROR_CODE, -9);
				map.put(ERROR_MESSAGE, "Already reviewed you can edit or delete old reiview");
				map.put(ERROR_MESSAGE_ARABIC,
						"Ã˜ÂªÃ™â€¦Ã˜Âª Ã™â€¦Ã˜Â±Ã˜Â§Ã˜Â¬Ã˜Â¹Ã˜ÂªÃ™Æ’ Ã™â€¦Ã™â€  Ã™â€šÃ˜Â¨Ã™â€žÃ˜Å’ Ã™Å Ã™â€¦Ã™Æ’Ã™â€ Ã™Æ’ Ã˜ÂªÃ˜Â¹Ã˜Â¯Ã™Å Ã™â€ž Ã˜Â£Ã™Ë† Ã˜Â­Ã˜Â°Ã™ï¿½ Ã˜Â§Ã™â€žÃ™â€¦Ã˜Â±Ã˜Â§Ã˜Â¬Ã˜Â¹Ã˜Â© Ã˜Â§Ã™â€žÃ™â€šÃ˜Â¯Ã™Å Ã™â€¦Ã˜Â©");
				return map;
			}
			// closeDBSession();
		} catch (Exception e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		map.put("review", review);
		map.put("noOfReviews", noOfReviews);
		map.put("avgRatings", avgRating);

		return map;
	}

	/*
	 * @Override public Map<String, Object> saveSubReview(SubReviews review) {
	 * Map<String, Object> map = null; try { Session session = openDBsession();
	 * session.saveOrUpdate(review); session.flush(); // closeDBSession(); } catch
	 * (Exception e) { // closeDBSession(); if (TESTING) { e.printStackTrace(); }
	 * logger.info(e.getMessage()); map = new HashMap<>(); map.put(ERROR_CODE, -2);
	 * map.put(ERROR_MESSAGE, e.getMessage()); return map; } map = new HashMap<>();
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put("review", review); return map; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> addComments(String comments, long reviewId) {
		Map<String, Object> map = null;
		Session session = openDBsession();
		Query qry = session.createQuery("update from Review p set p.comments=:comments  where p.reviewId=:reviewId");
		qry.setParameter("reviewId", reviewId);
		qry.setParameter("comments", comments);
		int result = qry.executeUpdate();
		session.flush();
		logger.info(result);
		if (result <= 0) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -6);
			map.put(ERROR_MESSAGE, "Review " + MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE, "Ã™â€ Ã˜ÂªÃ˜Â§Ã˜Â¬ " + MESSAGE_UPDATE_FAILED_AR);
			return map;
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, -6);
		map.put(ERROR_MESSAGE, "success ");
		map.put(ERROR_MESSAGE, "success");
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllCategories(long categoryId, long mainCategoryId, int status, long companyId) {
		Map<String, Object> map = null;
		List<Category> data = null;

		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Category.class);
			Criteria criteria2 = session.createCriteria(Category.class);
			if (categoryId > 0) {
				criteria.add(Restrictions.eq("categoryId", categoryId));
			}

			if (companyId > 0) {
				criteria.add(Restrictions.eq("companyId", companyId));
			}

			if (mainCategoryId > 0) {
				criteria.add(Restrictions.eq("mainCategoryId", mainCategoryId));
			}
			if (status == 1 || status == 0) {
				criteria.add(Restrictions.eq("status", status));
			}
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			criteria.addOrder(Order.asc("rank"));
			data = criteria.list();
			session.flush();
			/*
			 * if (status == 2) { data = criteria2.list(); session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data == null || data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("CategoryList", data);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getCategoryFromMainCategoryId(long categoryId) {
		Map<String, Object> map = null;
		List<Product> serviceList = null;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Category.class);
			if (categoryId > 0) {
				criteria.add(Restrictions.eq("mainCategoryId", categoryId));
			}
			if (categoryId < 0) {
				categoryId = -categoryId;
				criteria.add(Restrictions.eq("categoryId", categoryId));
			}
			criteria.addOrder(Order.asc("rank"));
			serviceList = criteria.list();
			// closeDBSession();
		} catch (Exception e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (serviceList != null && !serviceList.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("CategoryList", serviceList);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -8);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> getAllProducts(long productId,long
	 * sellerId,long userId,long mainCategoryId,long categoryId, long countryId,long
	 * cityId,long subcategoryId,int firstIndex, int rowPerPage,int status,String
	 * productName,long companyId,long numberOfItemsRemaining,long offerId,long
	 * manufacturerId,double priceStarts,double priceEnds,long
	 * productAttributeId,String value,String productNameAr) { Map<String, Object>
	 * map = null; List<Product> data = null; List<Review> reviewList = null;
	 * 
	 * List<ConnectedProducts> connectedList = null;
	 * 
	 * long totalCounts = 0; try { Session session = openDBsession(); Criteria
	 * criteria = session.createCriteria(Product.class); Criteria criteria2 =
	 * criteria.createCriteria("seller"); Criteria criteria3 =
	 * criteria.createCriteria("proudctAttributes"); boolean flag = false;
	 * StringBuffer strBfr = new StringBuffer("select count(*) from Product p");
	 * 
	 * if (userId > 0) { flag = true;
	 * strBfr.append(" inner join p.seller s where s.userId=" + userId);
	 * criteria2.add(Restrictions.eq("userId", userId)); }
	 * 
	 * if (productAttributeId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" "); } else { strBfr.append(" and"); }
	 * strBfr.append(" inner join  p.proudctAttributes a where a.attributeId=" +
	 * productAttributeId ); strBfr.append(" and lower(a.value) like '%" +
	 * value.toLowerCase() + "%'"); criteria3.add(Restrictions.eq("attributeId",
	 * productAttributeId)); criteria3.add(Restrictions.ilike("value", "%" + value +
	 * "%")); }
	 * 
	 * 
	 * 
	 * logger.info(strBfr+
	 * "strBfr-------------------------------------------------------------------------------"
	 * ); if (countryId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" inner join p.seller s where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" s.countryId=" + countryId);
	 * criteria2.add(Restrictions.eq("countryId", countryId)); } if (cityId > 0) {
	 * if (flag == false) { flag = true;
	 * strBfr.append(" inner join p.seller s where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" s.cityId=" + cityId);
	 * criteria2.add(Restrictions.eq("cityId", cityId)); } if (productId > 0) { if
	 * (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" p.productId=" + productId);
	 * criteria.add(Restrictions.eq("productId", productId)); } if (offerId > 0) {
	 * if (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" p.offerId=" + offerId);
	 * criteria.add(Restrictions.eq("offerId", offerId)); } if (manufacturerId > 0)
	 * { if (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" p.manufacturerId=" +
	 * manufacturerId); criteria.add(Restrictions.eq("manufacturerId",
	 * manufacturerId)); } if (sellerId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" p.sellerId=" + sellerId);
	 * criteria.add(Restrictions.eq("sellerId", sellerId)); } if (companyId > 0) {
	 * if (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" p.companyId=" + companyId);
	 * criteria.add(Restrictions.eq("companyId", companyId)); } if
	 * (numberOfItemsRemaining > 0) { long toDate = 0; if (flag == false) { flag =
	 * true; strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" p.numberOfItemsRemaining between   " + toDate + " and " +
	 * numberOfItemsRemaining);
	 * criteria.add(Restrictions.between("numberOfItemsRemaining", toDate,
	 * numberOfItemsRemaining)); } if (categoryId > 0) { if (flag == false) { flag =
	 * true; strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" p.categoryId=" + categoryId);
	 * criteria.add(Restrictions.eq("categoryId", categoryId)); } if (priceStarts >
	 * 0 && priceEnds > 0 ) {
	 * 
	 * if (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" p.price between " + priceStarts +
	 * " and " + priceEnds); criteria.add(Restrictions.between("price", priceStarts,
	 * priceEnds)); } if (subcategoryId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" p.subCategoryId=" + subcategoryId);
	 * criteria.add(Restrictions.eq("subCategoryId", subcategoryId)); } if
	 * (productName != null && !productName.equals("")) { logger.info(productName +
	 * "proudctName_________________________________________"); if (flag == false) {
	 * flag = true; strBfr.append(" where");
	 * 
	 * } else { strBfr.append(" and"); }
	 * strBfr.append(" lower(p.productName) like '%" + productName.toLowerCase() +
	 * "%'"); logger.info(strBfr +
	 * "_______________________________________________");
	 * criteria.add(Restrictions.ilike("productName", "%" + productName + "%"));
	 * 
	 * }
	 * 
	 * if (productNameAr != null && !productNameAr.equals("")) {
	 * logger.info(productNameAr +
	 * "productNameAr_________________________________________"); if (flag == false)
	 * { flag = true; strBfr.append(" where");
	 * 
	 * } else { strBfr.append(" and"); }
	 * strBfr.append(" lower(p.productNameAr) like '%" + productNameAr.toLowerCase()
	 * + "%'"); logger.info(strBfr +
	 * "_______________________________________________");
	 * criteria.add(Restrictions.ilike("productNameAr", "%" + productNameAr + "%"));
	 * 
	 * }
	 * 
	 * 
	 * 
	 * if (productName.equals("")) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" p.productName=" + productName);
	 * criteria.add(Restrictions.eq("productName", productName)); }
	 * 
	 * 
	 * if (status == 1 || status == 0) { if (flag == false) {
	 * strBfr.append(" where p.status=" + status); } else {
	 * strBfr.append(" and p.status=" + status); }
	 * criteria.add(Restrictions.eq("status", status)); }
	 * 
	 * logger.info(strBfr.toString()); Query qry =
	 * session.createQuery(strBfr.toString()); totalCounts = (long)
	 * qry.uniqueResult(); session.flush(); logger.info(totalCounts);
	 * criteria.setProjection(Projections.distinct(Projections.id())); if
	 * (rowPerPage > 0) { criteria.setMaxResults(rowPerPage);
	 * criteria.setFirstResult(firstIndex); } List<Long> ids = criteria.list();
	 * session.flush(); logger.info(ids); if (ids != null && !ids.isEmpty()) {
	 * criteria = session.createCriteria(Product.class);
	 * criteria.add(Restrictions.in("id", ids));
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); data =
	 * criteria.list(); session.flush(); } if (productId > 0) { criteria =
	 * session.createCriteria(Review.class);
	 * criteria.add(Restrictions.eq("fatherId", productId));
	 * criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
	 * criteria.list(); logger.info(reviewList.size() + "size of review list");
	 * session.flush(); }
	 * 
	 * } catch (HibernateException e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } logger.info(e.getMessage()); map = new HashMap<>();
	 * map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE, e.getMessage()); throw e; }
	 * if (data != null && !data.isEmpty()) { map = new HashMap<>();
	 * map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR); map.put("ProudctList",
	 * data); map.put("Reviews", reviewList); map.put("ConnectedProducts",
	 * connectedList); map.put("TotalCounts", totalCounts); return map;
	 * 
	 * } else { map = new HashMap<>(); map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR); return map; } }
	 */

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> getAllProducts(long productId, long
	 * sellerId, long userId, long mainCategoryId, long categoryId, long countryId,
	 * long cityId, long subcategoryId, int firstIndex, int rowPerPage, int status,
	 * String productName, long companyId, long numberOfItemsRemaining, long
	 * offerId, long manufacturerId, double priceStarts, double priceEnds, long
	 * productAttributeId, String value, String productNameAr, int data1,Map<Long,
	 * List<String>> attibutesSearch) { Map<String, Object> map = null;
	 * List<Product> data = null; long attriId =0; List<String> AttriValues = null;
	 * List<Review> reviewList = null; List<Manufacture> manu = null;
	 * List<AttributeDetails> attributesList_new = null; List<Attributes>
	 * attributesListDetails = null; Map<String, List<AttributeShowVo>> newMap =
	 * null; Map<String, List<AttributeShowVo>> newMapAr =null; Map<Attributes,
	 * List<AttributeShowVo>> newMapList = null;
	 * 
	 * List<ConnectedProducts> connectedList = null; Set<AttributeDetails> uniqAttri
	 * = null; Set<Long> uniq = null; Set<Attributes> attriset = null; long
	 * totalCounts = 0; try {
	 * 
	 * 
	 * if(attibutesSearch.size() > 0) { Set<Long> keyset = attibutesSearch.keySet();
	 * } StringBuffer strBfr = new StringBuffer("select count(*) from Product p");
	 * 
	 * 
	 * 
	 * 
	 * 
	 * List<Long> ids = null; List<Long> ids1 = null; Session session =
	 * openDBsession(); Criteria criteria = session.createCriteria(Product.class);
	 * Criteria criteria2 = criteria.createCriteria("seller"); Criteria criteria3 =
	 * criteria.createCriteria("proudctAttributes"); boolean flag = false; boolean
	 * flag1 = false; boolean flag2 = false;
	 * 
	 * 
	 * if (userId > 0) { flag = true;
	 * strBfr.append(" inner join p.seller s where s.userId=" + userId);
	 * 
	 * criteria.add(Restrictions.eq("userId", userId)); } if(attibutesSearch != null
	 * && !attibutesSearch.isEmpty()) { Set<Long> keyset = attibutesSearch.keySet();
	 * 
	 * 
	 * boolean isFirst=true; for (Long key : keyset)
	 * 
	 * {
	 * System.out.println("key :"+key+"\nValue in form of list:"+attibutesSearch.get
	 * (key));
	 * 
	 * attriId=key; AttriValues=attibutesSearch.get(attriId); //
	 * logger.info(AttriValues+
	 * "AttriValues______________________AttriValues__________________AttriValues");
	 * 
	 * StringBuffer valueArray=new StringBuffer("("); for(String s:AttriValues) {
	 * valueArray.append("'"+s+"',"); } valueArray.replace(valueArray.length()-1,
	 * valueArray.length(), ")");
	 * 
	 * if (flag == false) { flag = true;
	 * strBfr.append(" inner join  p.proudctAttributes a where"); } else {
	 * strBfr.append(" "); } if(!isFirst) { strBfr.append(" or "); } isFirst=false;
	 * strBfr.append(" (a.attributeId=" +
	 * attriId+" and a.value in "+valueArray.toString()+")");
	 * criteria3.add(Restrictions.eq("attributeId", attriId));
	 * 
	 * 
	 * //logger.info(strBfr+
	 * "strBfr___________________strBfr___________________________________strBfr");
	 * 
	 * } //logger.info(strBfr+
	 * "strBfr_________________________strBfr_______________________________strBfr")
	 * ; } if (countryId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" inner join p.seller s where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" s.countryId=" + countryId);//2
	 * criteria.add(Restrictions.eq("countryId", countryId)); } if (cityId > 0) { if
	 * (flag == false) { flag = true; strBfr.append(" inner join p.seller s where");
	 * } else { strBfr.append(" and"); } strBfr.append(" s.cityId=" + cityId);//2
	 * criteria.add(Restrictions.eq("cityId", cityId)); } if (productId > 0) { if
	 * (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" p.productId=" +
	 * productId);//criteria criteria.add(Restrictions.eq("productId", productId));
	 * } if (offerId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" p.offerId=" + offerId);//criteria
	 * criteria.add(Restrictions.eq("offerId", offerId)); } if (manufacturerId > 0)
	 * { if (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" p.manufacturerId=" +
	 * manufacturerId);//criteria criteria.add(Restrictions.eq("manufacturerId",
	 * manufacturerId)); } if (sellerId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" p.sellerId=" + sellerId);//criteria
	 * criteria.add(Restrictions.eq("sellerId", sellerId)); } if (companyId > 0) {
	 * if (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" p.companyId=" +
	 * companyId);//criteria criteria.add(Restrictions.eq("companyId", companyId));
	 * } if (numberOfItemsRemaining > 0) { long toDate = 0; if (flag == false) {
	 * flag = true; strBfr.append(" where"); } else { strBfr.append(" and");
	 * }//criteria strBfr.append(" p.numberOfItemsRemaining between   " + toDate +
	 * " and " + numberOfItemsRemaining);
	 * criteria.add(Restrictions.between("numberOfItemsRemaining", toDate,
	 * numberOfItemsRemaining)); } if (categoryId > 0) { if (flag == false) { flag =
	 * true; strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" p.categoryId=" + categoryId);//criteria
	 * criteria.add(Restrictions.eq("categoryId", categoryId)); } if (priceStarts >
	 * 0 && priceEnds > 0) {
	 * 
	 * if (flag == false) { flag = true; strBfr.append(" where"); } else {
	 * strBfr.append(" and"); } strBfr.append(" p.totalOfferAmount between " +
	 * priceStarts + " and " + priceEnds);//criteria
	 * criteria.add(Restrictions.between("totalOfferAmount", priceStarts,
	 * priceEnds)); } if (subcategoryId > 0) { if (flag == false) { flag = true;
	 * strBfr.append(" where"); } else { strBfr.append(" and"); }
	 * strBfr.append(" p.subCategoryId=" + subcategoryId);//criteria
	 * criteria.add(Restrictions.eq("subCategoryId", subcategoryId)); } if
	 * (productName != null && !productName.equals("")) {
	 * 
	 * if (flag == false) { flag = true; strBfr.append(" where");
	 * 
	 * } else { strBfr.append(" and"); }
	 * strBfr.append(" lower(p.productName) like '%" + productName.toLowerCase() +
	 * "%'");//criteria
	 * 
	 * criteria.add(Restrictions.ilike("productName", "%" + productName + "%"));
	 * 
	 * }
	 * 
	 * if (productNameAr != null && !productNameAr.equals("")) {
	 * 
	 * if (flag == false) { flag = true; strBfr.append(" where");
	 * 
	 * } else { strBfr.append(" and"); }
	 * strBfr.append(" lower(p.productNameAr) like '%" + productNameAr.toLowerCase()
	 * + "%'");//criteria
	 * 
	 * criteria.add(Restrictions.ilike("productNameAr", "%" + productNameAr + "%"));
	 * 
	 * }
	 * 
	 * if (status == 1 || status == 0) { if (flag == false) {
	 * strBfr.append(" where p.status=" + status); } else {
	 * strBfr.append(" and p.status=" + status); }
	 * criteria.add(Restrictions.eq("status", status)); }
	 * 
	 * Query qry = session.createQuery(strBfr.toString()); totalCounts = (long)
	 * qry.uniqueResult(); session.flush();
	 * criteria.setProjection(Projections.distinct(Projections.id())); if
	 * (rowPerPage > 0) { criteria.setMaxResults(rowPerPage);
	 * criteria.setFirstResult(firstIndex); }
	 * 
	 * 
	 * 
	 * 
	 * if (data1 != 0) {
	 * 
	 * 
	 * 
	 * if (data1 == HIGH_TO_LOW) { strBfr.replace(6, 15, " p ");
	 * strBfr.append("  group by p.productId  order by p.totalOfferAmount desc ");
	 * 
	 * 
	 * } else if (data1 == LOW_TO_HIGH) { strBfr.replace(6, 15, " p ");
	 * strBfr.append(" group by p.productId  order by p.totalOfferAmount asc ");
	 * 
	 * 
	 * } else if (data1 == POPULARITY) {
	 * 
	 * strBfr.replace(6, 15, " p ");
	 * strBfr.append(" group by p.productId  order by p.ratingPercentage desc ");
	 * 
	 * 
	 * }
	 * 
	 * Query qry1 = session.createQuery(strBfr.toString());
	 * 
	 * //logger.info(qry1+
	 * "qry1****************************!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!**************************"
	 * ); ids = qry1.list(); totalCounts = (long) ids.size(); session.flush();
	 * session.flush();
	 * 
	 * if (ids != null && !ids.isEmpty()) {
	 * 
	 * data = qry1.list();
	 * 
	 * session.flush(); } }
	 * 
	 * 
	 * 
	 * 
	 * else { strBfr.append(" group by p.productId "); Query qry2 =
	 * session.createQuery(strBfr.toString());
	 * 
	 * ids = qry2.list(); totalCounts = (long) ids.size(); session.flush();
	 * logger.info(totalCounts+
	 * "ids__________________________________________ids___*(****************************)*((("
	 * ); strBfr.replace(6, 15, " p "); //strBfr.append(" group by p.productId ");
	 * Query qry1 = session.createQuery(strBfr.toString());
	 * 
	 * logger.info(qry1+
	 * "ids__________________________________________ids___*(****************************)*((("
	 * );
	 * 
	 * data = qry1.list();
	 * 
	 * session.flush();
	 * 
	 * if (ids != null && !ids.isEmpty()) { criteria =
	 * session.createCriteria(Product.class); criteria.add(Restrictions.in("id",
	 * ids)); criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); data =
	 * criteria.list(); session.flush(); }
	 * 
	 * }
	 * 
	 * 
	 * if (productId > 0) { criteria = session.createCriteria(Review.class);
	 * criteria.add(Restrictions.eq("fatherId", productId));
	 * criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
	 * criteria.list();
	 * 
	 * session.flush(); }
	 * 
	 * 
	 * 
	 * if(data != null && !data.isEmpty()) {
	 * 
	 * 
	 * 
	 * StringBuffer strBfr2 = new
	 * StringBuffer("select m.manufacturerId  from Product m where  productId in (select p.productId from Product p  "
	 * );
	 * 
	 * 
	 * 
	 * 
	 * if (userId > 0) { flag1 = true;
	 * strBfr2.append(" inner join p.seller s where s.userId=" + userId);
	 * 
	 * }
	 * 
	 * boolean isFirst1=true; for (Long key : keyset)
	 * 
	 * {
	 * System.out.println("key :"+key+"\nValue in form of list:"+attibutesSearch.get
	 * (key));
	 * 
	 * attriId=key; AttriValues=attibutesSearch.get(attriId);
	 * logger.info(AttriValues+
	 * "AttriValues______________________AttriValues__________________AttriValues");
	 * 
	 * StringBuffer valueArray=new StringBuffer("("); for(String s:AttriValues) {
	 * valueArray.append("'"+s+"',"); } valueArray.replace(valueArray.length()-1,
	 * valueArray.length(), ")");
	 * 
	 * if (flag == false) { flag = true;
	 * strBfr.append(" inner join  p.proudctAttributes a where"); } else {
	 * strBfr.append(" "); } if(!isFirst1) { strBfr.append(" or "); }
	 * isFirst1=false; strBfr.append(" (a.attributeId=" +
	 * attriId+" and a.value in "+valueArray.toString()+")");
	 * criteria3.add(Restrictions.eq("attributeId", attriId));
	 * 
	 * 
	 * logger.info(strBfr+
	 * "strBfr___________________strBfr___________________________________strBfr");
	 * 
	 * }
	 * 
	 * if (countryId > 0) { if (flag1 == false) { flag1 = true;
	 * strBfr2.append(" inner join p.seller s where"); } else {
	 * strBfr2.append(" and"); } strBfr2.append(" s.countryId=" + countryId);
	 * 
	 * } if (cityId > 0) { if (flag1 == false) { flag1 = true;
	 * strBfr2.append(" inner join p.seller s where"); } else {
	 * strBfr2.append(" and"); } strBfr2.append(" s.cityId=" + cityId);
	 * 
	 * } if (productId > 0) { if (flag1 == false) { flag1 = true;
	 * strBfr2.append(" where"); } else { strBfr2.append(" and"); }
	 * strBfr2.append(" p.productId=" + productId);
	 * 
	 * } if (offerId > 0) { if (flag1 == false) { flag1 = true;
	 * strBfr2.append(" where"); } else { strBfr2.append(" and"); }
	 * strBfr2.append(" p.offerId=" + offerId);
	 * 
	 * }
	 * 
	 * if (sellerId > 0) { if (flag1 == false) { flag1 = true;
	 * strBfr2.append(" where"); } else { strBfr2.append(" and"); }
	 * strBfr2.append(" p.sellerId=" + sellerId);
	 * 
	 * } if (companyId > 0) { if (flag1 == false) { flag1 = true;
	 * strBfr2.append(" where"); } else { strBfr2.append(" and"); }
	 * strBfr2.append(" p.companyId=" + companyId);
	 * 
	 * } if (numberOfItemsRemaining > 0) { long toDate = 0; if (flag1 == false) {
	 * flag1 = true; strBfr2.append(" where"); } else { strBfr2.append(" and"); }
	 * strBfr2.append(" p.numberOfItemsRemaining between   " + toDate + " and " +
	 * numberOfItemsRemaining);
	 * 
	 * } if (categoryId > 0) { if (flag1 == false) { flag1 = true;
	 * strBfr2.append(" where"); } else { strBfr2.append(" and"); }
	 * strBfr2.append(" p.categoryId=" + categoryId);
	 * 
	 * } if (priceStarts > 0 && priceEnds > 0) {
	 * 
	 * if (flag1 == false) { flag1 = true; strBfr2.append(" where"); } else {
	 * strBfr2.append(" and"); } strBfr2.append(" p.totalOfferAmount between " +
	 * priceStarts + " and " + priceEnds);
	 * 
	 * } if (subcategoryId > 0) { if (flag1 == false) { flag1 = true;
	 * strBfr2.append(" where"); } else { strBfr2.append(" and"); }
	 * strBfr2.append(" p.subCategoryId=" + subcategoryId);
	 * 
	 * } if (productName != null && !productName.equals("")) {
	 * 
	 * if (flag1 == false) { flag1 = true; strBfr2.append(" where");
	 * 
	 * } else { strBfr2.append(" and"); }
	 * strBfr2.append(" lower(p.productName) like '%" + productName.toLowerCase() +
	 * "%'");
	 * 
	 * 
	 * 
	 * }
	 * 
	 * if (productNameAr != null && !productNameAr.equals("")) {
	 * 
	 * if (flag1 == false) { flag1 = true; strBfr2.append(" where");
	 * 
	 * } else { strBfr2.append(" and"); }
	 * strBfr2.append(" lower(p.productNameAr) like '%" +
	 * productNameAr.toLowerCase() + "%'");
	 * 
	 * 
	 * 
	 * }
	 * 
	 * if (status == 1 || status == 0) { if (flag1 == false) {
	 * strBfr2.append(" where p.status=" + status); } else {
	 * strBfr2.append(" and p.status=" + status); }
	 * 
	 * }
	 * 
	 * 
	 * 
	 * strBfr2.append(" )  group by m.manufacturerId ");
	 * 
	 * Query manQuery = session.createQuery(strBfr2.toString());
	 * 
	 * List<Long> manuIds = (List<Long>) manQuery.list(); uniq = new
	 * HashSet<Long>(manuIds); StringBuffer strBfr21 = new
	 * StringBuffer("select m  from Manufacture m where manufacturerId in(:pid)  ");
	 * Query manQuery1 = session.createQuery(strBfr21.toString());
	 * manQuery1.setParameterList("pid", uniq); manu = (List<Manufacture>)
	 * manQuery1.list(); StringBuffer strBfr23 = new
	 * StringBuffer(" select a.attributeId from ProductAttributes a  where a.productId in (select p.productId from Product p  "
	 * );
	 * 
	 * 
	 * if (userId > 0) { flag2 = true;
	 * strBfr23.append(" inner join p.seller s where s.userId=" + userId);
	 * //criteria2.add(Restrictions.eq("userId", userId)); }
	 * 
	 * 
	 * 
	 * if (countryId > 0) { if (flag2 == false) { flag2 = true;
	 * strBfr23.append(" inner join p.seller s where"); } else {
	 * strBfr23.append(" and"); } strBfr23.append(" s.countryId=" + countryId);
	 * 
	 * } if (cityId > 0) { if (flag2 == false) { flag2 = true;
	 * strBfr23.append(" inner join p.seller s where"); } else {
	 * strBfr23.append(" and"); } strBfr23.append(" s.cityId=" + cityId);
	 * 
	 * } if (productId > 0) { if (flag2 == false) { flag2 = true;
	 * strBfr23.append(" where"); } else { strBfr23.append(" and"); }
	 * strBfr23.append(" p.productId=" + productId);
	 * 
	 * } if (offerId > 0) { if (flag2 == false) { flag2 = true;
	 * strBfr23.append(" where"); } else { strBfr23.append(" and"); }
	 * strBfr23.append(" p.offerId=" + offerId);
	 * 
	 * }
	 * 
	 * if (sellerId > 0) { if (flag2 == false) { flag2 = true;
	 * strBfr23.append(" where"); } else { strBfr23.append(" and"); }
	 * strBfr23.append(" p.sellerId=" + sellerId);
	 * 
	 * } if (companyId > 0) { if (flag2 == false) { flag2 = true;
	 * strBfr23.append(" where"); } else { strBfr23.append(" and"); }
	 * strBfr23.append(" p.companyId=" + companyId);
	 * 
	 * } if (numberOfItemsRemaining > 0) { long toDate = 0; if (flag2 == false) {
	 * flag2 = true; strBfr23.append(" where"); } else { strBfr23.append(" and"); }
	 * strBfr23.append(" p.numberOfItemsRemaining between   " + toDate + " and " +
	 * numberOfItemsRemaining);
	 * 
	 * } if (categoryId > 0) { if (flag2 == false) { flag2 = true;
	 * strBfr23.append(" where"); } else { strBfr23.append(" and"); }
	 * strBfr23.append(" p.categoryId=" + categoryId);
	 * 
	 * } if (priceStarts > 0 && priceEnds > 0) {
	 * 
	 * if (flag2 == false) { flag2 = true; strBfr23.append(" where"); } else {
	 * strBfr23.append(" and"); } strBfr23.append(" p.totalOfferAmount between " +
	 * priceStarts + " and " + priceEnds);
	 * 
	 * } if (subcategoryId > 0) { if (flag2 == false) { flag2 = true;
	 * strBfr23.append(" where"); } else { strBfr23.append(" and"); }
	 * strBfr23.append(" p.subCategoryId=" + subcategoryId);
	 * 
	 * } if (productName != null && !productName.equals("")) {
	 * 
	 * if (flag2 == false) { flag2 = true; strBfr23.append(" where");
	 * 
	 * } else { strBfr23.append(" and"); }
	 * strBfr23.append(" lower(p.productName) like '%" + productName.toLowerCase() +
	 * "%'");
	 * 
	 * 
	 * 
	 * }
	 * 
	 * if (productNameAr != null && !productNameAr.equals("")) {
	 * 
	 * if (flag2 == false) { flag2 = true; strBfr23.append(" where");
	 * 
	 * } else { strBfr23.append(" and"); }
	 * strBfr23.append(" lower(p.productNameAr) like '%" +
	 * productNameAr.toLowerCase() + "%'");
	 * 
	 * 
	 * 
	 * }
	 * 
	 * if (status == 1 || status == 0) { if (flag2 == false) {
	 * strBfr23.append(" where p.status=" + status); } else {
	 * strBfr23.append(" and p.status=" + status); }
	 * 
	 * }
	 * 
	 * strBfr23.append(" )  group by a.attributeId ");
	 * 
	 * 
	 * Query attribute = session.createQuery(strBfr23.toString());
	 * 
	 * //logger.info(attribute+
	 * "attribute___________________________________________789");
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * List<Long> attributesList = (List<Long>) attribute.list(); StringBuffer
	 * attriValue = new
	 * StringBuffer(" select a from AttributeDetails a  where a.attributeId in (:pid)"
	 * ); Query attributes = session.createQuery(attriValue.toString());
	 * attributes.setParameterList("pid", attributesList);
	 * 
	 * // logger.info(attributes+
	 * "attributes_______________________________attributes_______________________9847"
	 * ); attributesList_new = (List<AttributeDetails>) attributes.list(); uniqAttri
	 * = new HashSet<AttributeDetails>(attributesList_new); newMap = new
	 * HashMap<>(); newMapAr= new HashMap<>(); List<AttributeShowVo> values=null;
	 * List<AttributeShowVo> valuesAr=null; newMapList = new HashMap<>();
	 * StringBuffer attribute1 = new
	 * StringBuffer(" select a from Attributes a  where a.attributeId in (:pid)");
	 * Query attributeQuery = session.createQuery(attribute1.toString()); //
	 * logger.info(attribute1+
	 * "attribute1____________________________________________123");
	 * attributeQuery.setParameterList("pid", attributesList); attributesListDetails
	 * = (List<Attributes>) attributeQuery.list(); List<Attributes>
	 * attributesListDetails1=attributesListDetails; for (AttributeDetails a :
	 * attributesList_new) {
	 * 
	 * if (newMap.get(a.getAttributes().getAttributeName()) == null) { values=new
	 * ArrayList<>(); valuesAr=new ArrayList<>(); attributesListDetails1=new
	 * ArrayList<>(); values.add(new AttributeShowVo(a.getValue(),a));
	 * 
	 * newMap.put(a.getAttributes().getAttributeName(), values);
	 * 
	 * } else { values=newMap.get(a.getAttributes().getAttributeName());
	 * values.add(new AttributeShowVo(a.getValue(),a));
	 * newMap.put(a.getAttributes().getAttributeName(), values); }}}
	 * 
	 * } catch (HibernateException e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); }
	 * 
	 * map = new HashMap<>(); map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE,
	 * e.getMessage()); throw e; } if (data != null && !data.isEmpty()) { map = new
	 * HashMap<>(); map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR); map.put("ProudctList",
	 * data); map.put("Reviews", reviewList); map.put("ConnectedProducts",
	 * connectedList); map.put("TotalCounts", totalCounts);
	 * map.put("ManufactureList", manu); map.put("AttributeList", uniqAttri);
	 * 
	 * map.put("AttributeValues", newMap); map.put("AttributeValuesAr", newMapAr);
	 * map.put("AttributeDetails", attributesListDetails);
	 * map.put("NewMapAttriList", newMapList); return map;
	 * 
	 * } else { map = new HashMap<>(); map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR); return map; } }
	 * 
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllProducts(long productId, long sellerId, long userId, long mainCategoryId,
			long categoryId, long countryId, long cityId, long subcategoryId, int firstIndex, int rowPerPage,
			int status, String productName, long companyId, long numberOfItemsRemaining, long offerId,
			long manufacturerId, double priceStarts, double priceEnds, long productAttributeId, String value,
			String productNameAr, int data1, Map<Long, List<String>> attibutesSearch, int featured, int rank) {
		Map<String, Object> map = null;
		List<Product> data = null;
		long attriId = 0;
		List<String> AttriValues = null;
		List<Review> reviewList = null;
		List<Manufacture> manu = null;
		List<AttributeDetails> attributesList_new = null;
		List<Attributes> attributesListDetails = null;
		//Map<String, Set<AttributeShowVo>> newMap = null;
	Map<String, List<AttributeShowVo>> newMap = null;
		Map<String, List<AttributeShowVo>> newMapAr = null;
		Map<Attributes, List<AttributeShowVo>> newMapList = null;

		List<ConnectedProducts> connectedList = null;
		Set<AttributeDetails> uniqAttri = null;
		Set<Long> uniq = null;
		Set<Attributes> attriset = null;
		long totalCounts = 0;
		try {

			
			StringBuffer strBfr = new StringBuffer("select count(*) from Product p");

			List<Long> ids = null;
			List<Long> ids1 = null;
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Product.class);
			Criteria criteria2 = criteria.createCriteria("seller");
			Criteria criteria3 = criteria.createCriteria("proudctAttributes");
			boolean flag = false;
			boolean flag1 = false;
			boolean flag2 = false;

			if (userId > 0) {
				flag = true;
				strBfr.append(" inner join p.seller s where s.userId=" + userId);

				criteria.add(Restrictions.eq("userId", userId));
			}
			if (attibutesSearch != null && !attibutesSearch.isEmpty()) {
				Set<Long> keyset = attibutesSearch.keySet();

				boolean isFirst = true;
				for (Long key : keyset)

				{
					System.out.println("key :" + key + "\nValue in form of list:" + attibutesSearch.get(key));

					attriId = key;
					AttriValues = attibutesSearch.get(attriId);
					//logger.info(
							//AttriValues + "AttriValues______________________AttriValues__________________AttriValues");

					StringBuffer valueArray = new StringBuffer("(");
					for (String s : AttriValues) {
						valueArray.append("'" + s + "',");
					}
					valueArray.replace(valueArray.length() - 1, valueArray.length(), ")");

					if (flag == false) {
						flag = true;
						strBfr.append(" inner join  p.proudctAttributes a where");
					} else {
						strBfr.append(" ");
					}
					if (!isFirst) {
						strBfr.append(" or ");
					}
					isFirst = false;
					strBfr.append(" (a.attributeId=" + attriId + " and a.value in " + valueArray.toString() + ")");
					criteria3.add(Restrictions.eq("attributeId", attriId));

					//logger.info(strBfr + "strBfr___________________strBfr___________________________________strBfr");

				}
			//	logger.info(strBfr + "strBfr_________________________strBfr_______________________________strBfr");
			}
			if (countryId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" inner join p.seller s where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" s.countryId=" + countryId);// 2
				criteria.add(Restrictions.eq("countryId", countryId));
			}
			if (cityId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" inner join p.seller s where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" s.cityId=" + cityId);// 2
				criteria.add(Restrictions.eq("cityId", cityId));
			}
			if (productId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.productId=" + productId);// criteria
				criteria.add(Restrictions.eq("productId", productId));
			}
			// int featured,int rank

			if (featured > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.featured=" + featured);// criteria
				criteria.add(Restrictions.eq("featured", featured));
			}

			if (rank > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.rank=" + rank);// criteria
				criteria.add(Restrictions.eq("rank", rank));
			}

			if (offerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.offerId=" + offerId);// criteria
				criteria.add(Restrictions.eq("offerId", offerId));
			}
			if (manufacturerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.manufacturerId=" + manufacturerId);// criteria
				criteria.add(Restrictions.eq("manufacturerId", manufacturerId));
			}
			if (sellerId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.sellerId=" + sellerId);// criteria
				criteria.add(Restrictions.eq("sellerId", sellerId));
			}
			if (companyId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.companyId=" + companyId);// criteria
				criteria.add(Restrictions.eq("companyId", companyId));
			}
			if (numberOfItemsRemaining > 0) {
				long toDate = 0;
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				} // criteria
				strBfr.append(" p.numberOfItemsRemaining between   " + toDate + " and " + numberOfItemsRemaining);
				criteria.add(Restrictions.between("numberOfItemsRemaining", toDate, numberOfItemsRemaining));
			}
			if (categoryId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.categoryId=" + categoryId);// criteria
				criteria.add(Restrictions.eq("categoryId", categoryId));
			}
			if (priceStarts > 0 && priceEnds > 0) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.price between " + priceStarts + " and " + priceEnds);// criteria
				criteria.add(Restrictions.between("totalOfferAmount", priceStarts, priceEnds));
			}
			if (subcategoryId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" p.subCategoryId=" + subcategoryId);// criteria
				criteria.add(Restrictions.eq("subCategoryId", subcategoryId));
			}
			if (productName != null && !productName.equals("")) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");

				} else {
					strBfr.append(" and");
				}
				strBfr.append(" lower(p.productName) like '%" + productName.toLowerCase() + "%'");// criteria

				criteria.add(Restrictions.ilike("productName", "%" + productName + "%"));

			}

			if (productNameAr != null && !productNameAr.equals("")) {

				if (flag == false) {
					flag = true;
					strBfr.append(" where");

				} else {
					strBfr.append(" and");
				}
				strBfr.append(" lower(p.productNameAr) like '%" + productNameAr.toLowerCase() + "%'");// criteria

				criteria.add(Restrictions.ilike("productNameAr", "%" + productNameAr + "%"));

			}

			if (status == 1 || status == 0) {
				if (flag == false) {
					strBfr.append(" where p.status=" + status);
				} else {
					strBfr.append(" and p.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}

			Query qry = session.createQuery(strBfr.toString());

			totalCounts = (long) qry.uniqueResult();
			//logger.info(totalCounts + "totalCounts_________________________totalCounts____________________totalCounts");
			session.flush();

			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}


			if (data1 != 0) {

				if (data1 == HIGH_TO_LOW) {
					strBfr.replace(6, 15, " p ");
					strBfr.append("  group by p.productId  order by p.price desc ");

				} else if (data1 == LOW_TO_HIGH) {
					strBfr.replace(6, 15, " p ");
					strBfr.append(" group by p.productId  order by p.price asc ");

				} else if (data1 == POPULARITY) {

					strBfr.replace(6, 15, " p ");
					strBfr.append(" group by p.productId  order by p.ratingPercentage desc ");

				}

				Query qry1 = session.createQuery(strBfr.toString());

				//logger.info(qry1
					//	+ "qry1****************************!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!**************************");
				ids = qry1.list();
				//logger.info(
						//ids + "ids__________________________________________ids___*(****************************)*(((");
				session.flush();

				if (ids != null && !ids.isEmpty()) 
				{
					qry1.setMaxResults(rowPerPage);
					qry1.setFirstResult(firstIndex);

					data = qry1.list();

					session.flush();
				}
			}

			else {

				strBfr.replace(6, 15, " p ");
				Query qry1 = session.createQuery(strBfr.toString());
				qry1.setMaxResults(rowPerPage);
				qry1.setFirstResult(firstIndex);
				data = qry1.list();
				//logger.info(
					//	qry1 + "qry1___________________qry1____________________________________________________qry1");
				//logger.info(data
						//+ "data__________________________________________data___*(****************************)*(((");
				session.flush();

				

			}

			if (productId > 0) {
				criteria = session.createCriteria(Review.class);
				criteria.add(Restrictions.eq("fatherId", productId));
				criteria.add(Restrictions.eq("productOrShop", PRODUCT));
				reviewList = criteria.list();

				session.flush();
			}

			if (data != null && !data.isEmpty()) {

				StringBuffer strBfr2 = new StringBuffer(
						"select m.manufacturerId  from Product m where  productId in (select p.productId from Product p  ");

				if (userId > 0) {
					flag1 = true;
					strBfr2.append(" inner join p.seller s where s.userId=" + userId);

				}

				boolean isFirst1 = true;
				

				if (countryId > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" inner join p.seller s where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" s.countryId=" + countryId);

				}
				if (cityId > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" inner join p.seller s where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" s.cityId=" + cityId);

				}
				if (productId > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.productId=" + productId);

				}
				if (offerId > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.offerId=" + offerId);

				}

				if (sellerId > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.sellerId=" + sellerId);

				}

				if (featured > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.featured=" + featured);// criteria
					// criteria.add(Restrictions.eq("featured", featured));
				}

				if (rank > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.rank=" + rank);// criteria
					// criteria.add(Restrictions.eq("rank", rank));
				}

				if (companyId > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.companyId=" + companyId);

				}
				if (numberOfItemsRemaining > 0) {
					long toDate = 0;
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.numberOfItemsRemaining between   " + toDate + " and " + numberOfItemsRemaining);

				}
				if (categoryId > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.categoryId=" + categoryId);

				}
				if (priceStarts > 0 && priceEnds > 0) {

					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.price between " + priceStarts + " and " + priceEnds);

				}
				if (subcategoryId > 0) {
					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");
					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" p.subCategoryId=" + subcategoryId);

				}
				if (productName != null && !productName.equals("")) {

					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");

					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" lower(p.productName) like '%" + productName.toLowerCase() + "%'");

				}

				if (productNameAr != null && !productNameAr.equals("")) {

					if (flag1 == false) {
						flag1 = true;
						strBfr2.append(" where");

					} else {
						strBfr2.append(" and");
					}
					strBfr2.append(" lower(p.productNameAr) like '%" + productNameAr.toLowerCase() + "%'");

				}

				if (status == 1 || status == 0) {
					if (flag1 == false) {
						strBfr2.append(" where p.status=" + status);
					} else {
						strBfr2.append(" and p.status=" + status);
					}

				}

				strBfr2.append(" )  group by m.manufacturerId ");

				Query manQuery = session.createQuery(strBfr2.toString());

				List<Long> manuIds = (List<Long>) manQuery.list();
				uniq = new HashSet<Long>(manuIds);
				StringBuffer strBfr21 = new StringBuffer(
						"select m  from Manufacture m where manufacturerId in(:pid)  ");
				Query manQuery1 = session.createQuery(strBfr21.toString());
				manQuery1.setParameterList("pid", uniq);
				manu = (List<Manufacture>) manQuery1.list();
				StringBuffer strBfr23 = new StringBuffer(
						" select a.attributeId from ProductAttributes a  where a.productId in (select p.productId from Product p  ");

				if (userId > 0) {
					flag2 = true;
					strBfr23.append(" inner join p.seller s where s.userId=" + userId);
					// criteria2.add(Restrictions.eq("userId", userId));
				}

				if (countryId > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" inner join p.seller s where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" s.countryId=" + countryId);

				}
				if (cityId > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" inner join p.seller s where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" s.cityId=" + cityId);

				}
				if (productId > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.productId=" + productId);

				}

				if (featured > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.featured=" + featured);// criteria
					// criteria.add(Restrictions.eq("featured", featured));
				}

				if (rank > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.rank=" + rank);// criteria
					// criteria.add(Restrictions.eq("rank", rank));
				}

				if (offerId > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.offerId=" + offerId);

				}

				if (sellerId > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.sellerId=" + sellerId);

				}
				if (companyId > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.companyId=" + companyId);

				}
				if (numberOfItemsRemaining > 0) {
					long toDate = 0;
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.numberOfItemsRemaining between   " + toDate + " and " + numberOfItemsRemaining);

				}
				if (categoryId > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.categoryId=" + categoryId);

				}
				if (priceStarts > 0 && priceEnds > 0) {

					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.price between " + priceStarts + " and " + priceEnds);

				}
				if (subcategoryId > 0) {
					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");
					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" p.subCategoryId=" + subcategoryId);

				}
				if (productName != null && !productName.equals("")) {

					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");

					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" lower(p.productName) like '%" + productName.toLowerCase() + "%'");

				}

				if (productNameAr != null && !productNameAr.equals("")) {

					if (flag2 == false) {
						flag2 = true;
						strBfr23.append(" where");

					} else {
						strBfr23.append(" and");
					}
					strBfr23.append(" lower(p.productNameAr) like '%" + productNameAr.toLowerCase() + "%'");

				}

				if (status == 1 || status == 0) {
					if (flag2 == false) {
						strBfr23.append(" where p.status=" + status);
					} else {
						strBfr23.append(" and p.status=" + status);
					}

				}

				strBfr23.append(" )  group by a.attributeId ");

				Query attribute = session.createQuery(strBfr23.toString());

				

				List<Long> attributesList = (List<Long>) attribute.list();
				StringBuffer attriValue = new StringBuffer(
						" select a from AttributeDetails a  where a.attributeId in (:pid)");
				Query attributes = session.createQuery(attriValue.toString());
				attributes.setParameterList("pid", attributesList);

				
				attributesList_new = (List<AttributeDetails>) attributes.list();
				uniqAttri = new HashSet<AttributeDetails>(attributesList_new);
				newMap = new HashMap<>();
				newMapAr = new HashMap<>();
				List<AttributeShowVo> values = null;
				
				List<AttributeShowVo> valuesAr = null;
				newMapList = new HashMap<>();
				StringBuffer attribute1 = new StringBuffer(
						" select a from Attributes a  where a.attributeId in (:pid)");
				Query attributeQuery = session.createQuery(attribute1.toString());
				
				attributeQuery.setParameterList("pid", attributesList);
				attributesListDetails = (List<Attributes>) attributeQuery.list();
				List<Attributes> attributesListDetails1 = attributesListDetails;
				for (AttributeDetails a : uniqAttri)
				{

					if (newMap.get(a.getAttributes().getAttributeName()) == null) 
					{
						
						values = new ArrayList<>();
						
						valuesAr = new ArrayList<>();
						attributesListDetails1 = new ArrayList<>();
						values.add(new AttributeShowVo(a.getValue(), a.getValueAr(), a));
						
						Set<AttributeShowVo> hs = new HashSet<>();
						hs.addAll(values);
						values.clear();
						values.addAll(hs);
						
						//newMap.put(a.getAttributes().getAttributeName(), values);
						String s=a.getAttributes().getAttributeName();
						if (newMap.containsValue(s))
						{
							
							newMap.remove(a.getAttributes().getAttributeName(), values);
					     } 
						else 
						{
							
							newMap.put(a.getAttributes().getAttributeName(), values);
					          }
						

					} 
					else 
					{
					
						values = newMap.get(a.getAttributes().getAttributeName());
						values.add(new AttributeShowVo(a.getValue(), a.getValueAr(), a));
						Set<AttributeShowVo> hs = new HashSet<>();
						hs.addAll(values);
						values.clear();
						values.addAll(hs);
						String s=a.getAttributes().getAttributeName();
						if (newMap.containsValue(s))
						{
							
							newMap.remove(a.getAttributes().getAttributeName(), values);
					          } 
						else 
						{
							
							newMap.put(a.getAttributes().getAttributeName(), values);
					          }
						//newMap.put(a.getAttributes().getAttributeName(), values);
					}
				}
			}

		} catch (HibernateException e) {
			
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("ProudctList", data);
			map.put("Reviews", reviewList);
			map.put("ConnectedProducts", connectedList);
			map.put("TotalCounts", totalCounts);
			map.put("ManufactureList", manu);
			map.put("AttributeList", uniqAttri);

			map.put("AttributeValues", newMap);
			map.put("AttributeValuesAr", newMapAr);
			map.put("AttributeDetails", attributesListDetails);
			map.put("NewMapAttriList", newMapList);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> changeReturnStatus(long returnItemId, int status) {
		Map<String, Object> map = null;
		int result = 0;
		int result2 = 0;
		try {

			logger.info("returnItemId______________________________" + returnItemId);

			Session session = openDBsession();
			Criteria detachedCriteria = session.createCriteria(ReturnItems.class);
			detachedCriteria.add(Restrictions.eq("returnItemId", returnItemId));
			ReturnItems returnList = (ReturnItems) detachedCriteria.uniqueResult();
			if (returnList == null) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "returnItemId " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}
			boolean flag = true;
			switch (status) {
			case RETURN_ACCEPTED:
				if (returnList.getStatus() != RETURN_REQUESTED) {
					flag = false;
				}
				break;
			case RETURN_CANCELLED:
				if (returnList.getStatus() != RETURN_REQUESTED) {
					flag = false;
				}
				break;
			case RETURN_REJECTED:
				if (returnList.getStatus() != RETURN_REQUESTED) {
					flag = false;
				}
				break;
			case RETURN_COMPLETED:
				if (returnList.getStatus() != RETURN_ACCEPTED) {
					flag = false;
				}
				break;
			default:
				flag = false;
				break;
			}
			if (flag == false) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "Change to given status not allowed");
				map.put(ERROR_MESSAGE_ARABIC, "Ø·Ù„Ø¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}
			/*
			 * Query qry2 = session.
			 * createQuery(" update ReturnItems set status=:status where returnItemId=:returnItemId "
			 * ); qry2.setParameter("status", status); qry2.setParameter("returnItemId",
			 * returnItemId); result2 = qry2.executeUpdate();
			 */
			if (status == RETURN_ACCEPTED) {
				Query qry2 = session
						.createQuery(" update ReturnItems set status=:status where returnItemId=:returnItemId ");
				qry2.setParameter("status", status);
				qry2.setParameter("returnItemId", returnItemId);
				result2 = qry2.executeUpdate();

				Query qry3 = session
						.createQuery("select orderListId from ReturnItems where returnItemId=:returnItemId ");
				qry3.setParameter("returnItemId", returnItemId);
				long orderListId = (long) qry3.uniqueResult();
				/*
				 * Query qry4 = session.
				 * createQuery("select orderId from OrderDetails where orderListId=:orderListId "
				 * ); qry4.setParameter("orderListId", orderListId); long orderId = (long)
				 * qry4.uniqueResult();
				 */
				Criteria criteria = session.createCriteria(OrderDetails.class);
				criteria.add(Restrictions.eq("orderListId", orderListId));
				OrderDetails orderDetails = (OrderDetails) criteria.uniqueResult();
				orderDetails.setStatus(RETURN_ACCEPTED);
				session.merge(orderDetails);
				session.flush();
			} else if (status == RETURN_CANCELLED || status == RETURN_REJECTED) {
				Query qry3 = session.createQuery("from ReturnItems where returnItemId=:returnItemId ");
				qry3.setParameter("returnItemId", returnItemId);
				ReturnItems reItem = (ReturnItems) qry3.uniqueResult();
				session.flush();
				Query qry2 = session.createQuery(" delete from  ReturnItems  where returnItemId=:returnItemId ");
				qry2.setParameter("returnItemId", returnItemId);
				result2 = qry2.executeUpdate();
				session.flush();
				if (result2 <= 0) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -26);
					map.put(ERROR_MESSAGE, "Return item update failed");
					map.put(ERROR_MESSAGE_ARABIC, "نجاح");
					return map;
				}
				if (reItem.getOldOrderListId() == reItem.getOrderListId()) {
					Query qry5 = session
							.createQuery(" update OrderDetails set status=:status where orderListId=:orderListId ");
					qry5.setParameter("status", DELIVERED);
					qry5.setParameter("orderListId", reItem.getOrderListId());
					result2 = qry5.executeUpdate();
					session.flush();
					if (result2 <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -27);
						map.put(ERROR_MESSAGE, "Order item" + MESSAGE_UPDATE_FAILED);
						map.put(ERROR_MESSAGE_ARABIC, "نجاح");
						return map;
					}
				} else {
					Criteria criteria = session.createCriteria(OrderDetails.class);
					criteria.add(Restrictions.eq("orderListId", reItem.getOldOrderListId()));
					OrderDetails orderDetails = (OrderDetails) criteria.uniqueResult();
					session.flush();
					long qty = orderDetails.getItemQuantity() + reItem.getItemQuantity();
					orderDetails.setItemQuantity(qty);
					double netPrice = qty * orderDetails.getItemPrice();
					orderDetails.setItemNetPrice(netPrice);
					session.merge(orderDetails);
					session.flush();
					qry2 = session.createQuery(" delete from  OrderDetails  where orderListId=:orderLId ");
					qry2.setParameter("orderLId", reItem.getOrderListId());
					result2 = qry2.executeUpdate();
					session.flush();
					if (result2 <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -27);
						map.put(ERROR_MESSAGE, "Return order details update failed");
						map.put(ERROR_MESSAGE_ARABIC, "نجاح");
						return map;
					}
				}
			} else if (status == RETURN_COMPLETED) {

				Query qry3 = session
						.createQuery("select orderListId from ReturnItems where returnItemId=:returnItemId ");
				qry3.setParameter("returnItemId", returnItemId);
				long orderListId = (long) qry3.uniqueResult();
				OrderDetails orderDetails = null;
				if (orderListId > 0) {
					Criteria criteria = session.createCriteria(OrderDetails.class);
					criteria.add(Restrictions.eq("orderListId", orderListId));
					criteria.setMaxResults(1);
					orderDetails = (OrderDetails) criteria.list().get(0);
					if (orderDetails == null) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -21);
						map.put(ERROR_MESSAGE, "No such ordered product found");
						map.put(ERROR_MESSAGE_ARABIC, "نجاح");
						return map;
					}
				} else {
					map = new HashMap<>();
					map.put(ERROR_CODE, -22);
					map.put(ERROR_MESSAGE, "No such return");
					map.put(ERROR_MESSAGE_ARABIC, "نجاح");
					return map;
				}
				Query qry2 = session
						.createQuery(" update ReturnItems set status=:status where returnItemId=:returnItemId ");
				qry2.setParameter("status", RETURN_COMPLETED);
				qry2.setParameter("returnItemId", returnItemId);
				result2 = qry2.executeUpdate();
				Query qry = session.createQuery(
						"update OrderDetails od set od.itemNetPrice=od.itemNetPrice-:price,od.status=:stat where od.orderListId=:odId");
				qry.setParameter("odId", orderDetails.getOrderListId());
				qry.setParameter("price", orderDetails.getItemNetPrice());
				qry.setParameter("stat", RETURN_COMPLETED);
				result = qry.executeUpdate();
				if (result > 0) {

					String q = "select o.productId from OrderDetails o where o.orderListId=:orderListId";
					Query qry1 = session.createQuery(q);
					qry1.setParameter("orderListId", orderDetails.getOrderListId());
					long prod = (long) qry1.uniqueResult();

					logger.info(prod + "prod_________________________________________________________");

					qry2 = session.createQuery(
							"update Product p set p.numberOfItemsRemaining=p.numberOfItemsRemaining+:ite where p.productId=:odId");
					qry2.setParameter("ite", orderDetails.getItemQuantity());
					qry2.setParameter("odId", prod);
					int outcome = qry2.executeUpdate();
					logger.info(outcome
							+ "outcome-----------------------------------------------------------------------------");
					if (outcome <= 0) {
						map = new HashMap<>();
						map.put(ERROR_CODE, -4);
						map.put(ERROR_MESSAGE, "product update failed");
						map.put(ERROR_MESSAGE_ARABIC, "نجاح");
						return map;
					} else {
						map = new HashMap<>();
						map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
						map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
						map.put(ERROR_MESSAGE_ARABIC, "نجاح");
						return map;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		if (result2 <= 0) {
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
		} else {
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getProductswithoutOffer(int rowPerPage, int firstIndex) {
		Map<String, Object> map = null;
		List<Product> data = null;
		List<Review> reviewList = null;

		List<ConnectedProducts> connectedList = null;

		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Product.class);
			Criteria criteria2 = criteria.createCriteria("seller");
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer(" select count(*) from Product p where offerId=0 ");
			logger.info(strBfr.toString());
			long offer = 0;
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			logger.info(totalCounts);
			criteria = session.createCriteria(Product.class);
			criteria.add(Restrictions.eq("offerId", offer));
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}

			List<Long> ids = (List<Long>) criteria.list();
			logger.info(ids + "ids--------------------------------------------------------------------------------");
			session.flush();
			logger.info(ids);
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Product.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("ProudctList", data);
			map.put("Reviews", reviewList);
			map.put("ConnectedProducts", connectedList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@Override
	public Map<String, Object> shareOrLike(StringBuffer str, Review likeOrShareReview) {
		Map<String, Object> map = null;
		try {
			Session session = openDBsession();
			Query qry = session.createQuery(str.toString());
			int result = qry.executeUpdate();
			if (result <= 0) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
				return map;
			} else {
				session.saveOrUpdate(likeOrShareReview);
				session.flush();

			}
			// closeDBSession();
		} catch (Exception e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		map = new HashMap<>();
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubCategory> getAllSubCategoryList(long mainCategoryId, long categoryId, long companyId) {

		Session session = openDBsession();
		List<SubCategory> itemnameList = null;
		try {
			Criteria criteria = session.createCriteria(SubCategory.class);
			if (mainCategoryId > 0) {
				criteria.add(Restrictions.eq("mainCategoryId", mainCategoryId));
			}

			if (companyId > 0) {
				criteria.add(Restrictions.eq("companyId", companyId));
			}

			if (categoryId > 0) {
				criteria.add(Restrictions.eq("categoryId", categoryId));
			}
			itemnameList = criteria.list();
			session.flush();
		} catch (HibernateException e) {
			logger.error(e);
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			throw e;
		}
		// closeDBSession();
		return itemnameList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Offer> getAllOfferId(long offerId) {
		Session session = openDBsession();
		List<Offer> itemnameList = null;
		try {
			Criteria criteria = session.createCriteria(Offer.class);
			if (offerId > 0) {
				criteria.add(Restrictions.eq("offerId", offerId));
			}

			itemnameList = criteria.list();
			session.flush();
		} catch (HibernateException e) {
			logger.error(e);
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			throw e;
		}
		// closeDBSession();
		return itemnameList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Manufacture> getallManufactureId(long manufactureId) {
		Session session = openDBsession();
		List<Manufacture> itemnameList = null;
		try {
			Criteria criteria = session.createCriteria(Manufacture.class);
			if (manufactureId > 0) {
				criteria.add(Restrictions.eq("manufactureId", manufactureId));
			}

			itemnameList = criteria.list();
			session.flush();
		} catch (HibernateException e) {
			logger.error(e);
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			throw e;
		}
		// closeDBSession();
		return itemnameList;
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public List<MainCategory> getAllMainCategoryList() {
	 * 
	 * Session session = openDBsession(); List<MainCategory> itemnameList = null;
	 * try { Criteria criteria = session.createCriteria(MainCategory.class);
	 * itemnameList = criteria.list(); session.flush(); } catch (HibernateException
	 * e) { logger.error(e); // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } return null; } // closeDBSession(); return
	 * itemnameList; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> getAllCategoryList(long categoryId) {

		Session session = openDBsession();
		List<Category> itemnameList = null;
		try {
			Criteria criteria = session.createCriteria(Category.class);
			if (categoryId > 0) {
				criteria.add(Restrictions.eq("", categoryId));
			}
			itemnameList = criteria.list();
			session.flush();
		} catch (HibernateException e) {
			logger.error(e);
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			throw e;
		}
		// closeDBSession();
		return itemnameList;
	}

	@SuppressWarnings("unchecked")
	@Override

	public List<CustomerDetails> getAllCustomerName(long userId) {
		Session session = openDBsession();
		List<CustomerDetails> itemnameList = null;
		try {
			Criteria criteria = session.createCriteria(CustomerDetails.class);
			if (userId > 0) {
				criteria.add(Restrictions.eq("", userId));
			}
			itemnameList = criteria.list();
			session.flush();
		} catch (HibernateException e) {
			logger.error(e);
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			throw e;
		}
		return itemnameList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getSubscribes(long subscribeId) {
		Map<String, Object> map = null;
		DetachedCriteria detachedcriteria = DetachedCriteria.forClass(FeaturesAndPhotosProduct.class);
		if (subscribeId > 0) {
			detachedcriteria.add(Restrictions.eq("subScribeId", subscribeId));
		}
		List<FeaturesAndPhotosProduct> data = (List<FeaturesAndPhotosProduct>) hibernateTemplate
				.findByCriteria(detachedcriteria);
		if (data == null || data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put("DATA", data);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getBookingDetail(long bookingId, int status) {
		Map<String, Object> map = null;
		DetachedCriteria detachedcriteria = DetachedCriteria.forClass(SellerDetails.class);
		if (bookingId > 0) {
			detachedcriteria.add(Restrictions.eq("bookingId", bookingId));
		}
		if (status > 0) {
			detachedcriteria.add(Restrictions.eqOrIsNull("status", status));
		}
		List<SellerDetails> data = (List<SellerDetails>) hibernateTemplate.findByCriteria(detachedcriteria);
		if (data == null || data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put("DATA", data);
			return map;
		}
	}

	/*
	 * @SuppressWarnings("unchecked")
	 * 
	 * @Override public Map<String, Object> getAllShops(long shopId, long
	 * mainCategory, long categoryId, long subCategoryId, int firstIndex, int
	 * rowPerPage, long fromDate, long toDate) { Map<String, Object> map = null;
	 * List<Product> data = null; long totalCounts = 0; List<Review> reviewList =
	 * null; try { Session session = openDBsession(); Criteria criteria =
	 * session.createCriteria(Store.class); Criteria criteriaprod = null; boolean
	 * flag = false; StringBuffer strBfr = new
	 * StringBuffer("select count(*) from Store s");
	 * 
	 * if (mainCategory > 0) { logger.info("size is " + mainCategory); flag = false;
	 * strBfr.append(" s.mainCategoryId=" + mainCategory);
	 * criteria.add(Restrictions.eq("mainCategoryId", mainCategory)); }
	 * 
	 * if (shopId > 0) { flag = true; strBfr.append(" where s.storeId=" + shopId);
	 * criteria.add(Restrictions.eq("storeId", shopId)); } if (categoryId > 0) { if
	 * (flag == false) { strBfr.append(" where"); flag = true; } else {
	 * strBfr.append(" and"); } if(criteriaprod == null) { criteriaprod =
	 * criteria.createCriteria("products"); } strBfr.
	 * append(" s.storeId in (select p.storeId from Product p where p.categoryId=" +
	 * subCategoryId + ")"); criteriaprod.add(Restrictions.eq("categoryId",
	 * categoryId)); }
	 * 
	 * if (subCategoryId > 0) { if (flag == false) { strBfr.append(" where"); flag =
	 * true; } else { strBfr.append(" and"); } if(criteriaprod == null) {
	 * criteriaprod = criteria.createCriteria("products"); } strBfr.append(
	 * " s.storeId in (select p.storeId from Product p where p.subCategoryId=" +
	 * subCategoryId + ")"); criteriaprod.add(Restrictions.eq("subCategoryId",
	 * subCategoryId)); }
	 * 
	 * Query qry = session.createQuery(strBfr.toString());
	 * 
	 * 
	 * * if (categoryId > 0) { qry.setParameter("catId",
	 * String.valueOf(categoryId)); qry.setParameter("mode", MODE_CATEGORIES); }
	 * 
	 * totalCounts = (long) qry.uniqueResult(); session.flush();
	 * criteria.setProjection(Projections.distinct(Projections.id())); if(rowPerPage
	 * > 0) { criteria.setMaxResults(rowPerPage);
	 * criteria.setFirstResult(firstIndex); } List<Long> ids = criteria.list();
	 * session.flush(); logger.info("size is " + ids.size()); if (ids != null &&
	 * !ids.isEmpty()) { criteria = session.createCriteria(Store.class);
	 * criteria.add(Restrictions.in("id", ids));
	 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); data =
	 * criteria.list(); session.flush(); } if (shopId > 0) { criteria =
	 * session.createCriteria(Review.class);
	 * criteria.add(Restrictions.eq("fatherId", shopId));
	 * criteria.add(Restrictions.eq("productOrShop", SHOP)); reviewList =
	 * criteria.list(); session.flush(); } // closeDBSession(); } catch
	 * (HibernateException e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } logger.info(e.getMessage()); map = new HashMap<>();
	 * map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE, e.getMessage()); throw e; }
	 * if (data != null && !data.isEmpty()) { map = new HashMap<>();
	 * map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR); map.put("ShopList", data);
	 * map.put("TotalCounts", totalCounts); if (shopId > 0) { map.put("reviews",
	 * reviewList); } return map; } else {
	 * 
	 * map = new HashMap<>(); map.put(ERROR_CODE, -3); map.put(ERROR_MESSAGE,
	 * MESSAGE_NO_DATA_TO_SHOW); map.put(ERROR_MESSAGE_ARABIC,
	 * MESSAGE_NO_DATA_TO_SHOW_AR); return map; } }
	 */

	/*
	 * @Override public Map<String, Object> getAllReviews(long fatherId, int
	 * productOrShop, int firstIndex, int rowPerPage) { Map<String, Object> map =
	 * null; List<Review> reviewList = null; long totalCounts = 0; try { Session
	 * session = openDBsession(); Criteria criteria =
	 * session.createCriteria(Review.class); criteria.add(Restrictions.eq("status",
	 * ACTIVE)); if (fatherId > 0) { criteria.add(Restrictions.eq("fatherId",
	 * fatherId)); } criteria.add(Restrictions.eq("productOrShop", productOrShop));
	 * criteria.setFirstResult(firstIndex); criteria.setMaxResults(rowPerPage);
	 * reviewList = criteria.list(); session.flush(); logger.info("size of llist " +
	 * reviewList.size()); StringBuffer strBfr = new
	 * StringBuffer("select count(*) from Review r where r.productOrShop=:POS"); if
	 * (fatherId > 0) { strBfr.append(" and r.fatherId=:FID"); } Query qryTotal =
	 * session.createQuery(strBfr.toString()); if (fatherId > 0) {
	 * qryTotal.setParameter("FID", fatherId); } qryTotal.setParameter("POS",
	 * productOrShop); totalCounts = (long) qryTotal.uniqueResult();
	 * session.flush(); // closeDBSession(); } catch (HibernateException e) { //
	 * closeDBSession(); if (TESTING) { e.printStackTrace(); }
	 * logger.info(e.getMessage()); map = new HashMap<>(); map.put(ERROR_CODE, -2);
	 * map.put(ERROR_MESSAGE, e.getMessage()); throw e; } if (reviewList == null ||
	 * reviewList.isEmpty()) { map = new HashMap<>(); map.put(ERROR_CODE, -3);
	 * map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR); return map; } else
	 * { map = new HashMap<>(); map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE,
	 * "SUCCESS"); map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­"); map.put("ReviewList",
	 * reviewList); map.put("TotalCounts", totalCounts); return map; } }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllReviews(long fatherId, long cityId, long countryId, long customerId, long sellerId,
			int currentIndex, int rowPerPage, long fromDate, long toDate) {
		Map<String, Object> map = null;
		List<Review> reviewList = null;

		Review review = new Review();

		long totalCounts = 0;
		try {
			boolean isSub = false;
			boolean flag = false;
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Review.class);
			Criteria criteria2 = null;
			StringBuffer sBuffer = new StringBuffer("from Review r");
			criteria.add(Restrictions.eq("status", ACTIVE));

			if (fatherId > 0) {
				if (!flag) {
					sBuffer.append(" where");
				} else {
					sBuffer.append(" and");
				}
				criteria.add(Restrictions.eq("fatherId", fatherId));
			}
			if (fromDate > 0) {
				criteria.add(Restrictions.between("lastUpdatedTime", fromDate, toDate));
			}
			if (cityId > 0) {
				if (isSub == false) {
					criteria2 = criteria.createCriteria("product");
				}
			}
			criteria.add(Restrictions.eq("status", ACTIVE));
			criteria.setProjection(Projections.distinct(Projections.id()));
			criteria.setMaxResults(rowPerPage);
			criteria.setFirstResult(currentIndex);
			List<Long> ids = criteria.list();
			List<Integer> id = criteria.list();

			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Review.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				reviewList = criteria.list();
				session.flush();
			}
			StringBuffer strBfr = new StringBuffer("select count(*) from Review");
			if (fatherId > 0) {
				strBfr.append(" where fatherId=:FID");
			}
			Query qryTotal = session.createQuery(strBfr.toString());
			if (fatherId > 0) {
				qryTotal.setParameter("FID", fatherId);
			}
			totalCounts = (long) qryTotal.uniqueResult();
			session.flush();
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (reviewList == null || reviewList.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "نجاح");
			map.put("ReviewList", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.innoventiaProject.dao.input.InputDao#saveSubCategory(com.hidile.
	 * jewelry.entity.input.SubCategory)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveSubCategory(SubCategory category) {
		logger.info(category.getSubCategoryName());
		Map<String, Object> map = null;
		List<SubCategory> subcategoryList = null;
		try {
			Session session = openDBsession();
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Category.class);
			detachedCriteria.add(Restrictions.eq("categoryId", category.getCategoryId()));
			List<Category> categoryList = (List<Category>) hibernateTemplate.findByCriteria(detachedCriteria);
			if (categoryList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -9);
				map.put(ERROR_MESSAGE, "No such category available");
				map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­ ÙˆØ¸Ø§Ø¦Ù�");
				return map;
			}
			
			if (category.getSubCategoryId() <= 0) {
				 detachedCriteria = DetachedCriteria.forClass(SubCategory.class);
				detachedCriteria.add(Restrictions.eq("rank", category.getRank()));
				detachedCriteria.add(Restrictions.eq("subCategoryName", category.getSubCategoryName()));
				subcategoryList = (List<SubCategory>) hibernateTemplate.findByCriteria(detachedCriteria);
				if (!subcategoryList.isEmpty()) {
					map = new HashMap<>();
					map.put(ERROR_CODE, -9);
					map.put(ERROR_MESSAGE, "rank already in server");
					map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­ ÙˆØ¸Ø§Ø¦Ù�");
					return map;
				}
			
			}
			
			
			if (category.getSubCategoryId() <= 0) {
				detachedCriteria = DetachedCriteria.forClass(SubCategory.class);
				Criterion criterion1 = Restrictions.eq("subCategoryNameAr", category.getSubCategoryNameAr());
				Criterion criterion2 = Restrictions.eq("subCategoryName", category.getSubCategoryName());
				Criterion criterion3 = Restrictions.eq("categoryId", category.getCategoryId());
				detachedCriteria.add(Restrictions.eq("companyId", category.getCompanyId()));

				detachedCriteria.add(Restrictions.and(criterion1, criterion2, criterion3));
				subcategoryList = (List<SubCategory>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (subcategoryList == null || subcategoryList.isEmpty()) {
					category.setCategory(null);
					session.save(category);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­");
					return map;
				} else {
					map.put(ERROR_CODE, -7);
					map.put(ERROR_MESSAGE, "Sub category name already exist");
					map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­");
					return map;
				}

			} else {
				detachedCriteria = DetachedCriteria.forClass(SubCategory.class);
				detachedCriteria.add(Restrictions.eq("subCategoryId", category.getSubCategoryId()));
				subcategoryList = (List<SubCategory>) hibernateTemplate.findByCriteria(detachedCriteria);
				map = new HashMap<>();
				if (subcategoryList == null || subcategoryList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such subcategory");
					map.put(ERROR_MESSAGE_ARABIC, "Ù„Ø§ ÙŠÙˆØ¬Ø¯ Ù…Ø«Ù„ Ù‡Ø°Ù‡ Ø§Ù„Ù�Ø¦Ø©");
					return map;
				} else {
					category.setCategory(null);
					session.merge(category);
					session.flush();
					map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
					map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
					map.put(ERROR_MESSAGE_ARABIC, "Ø¬Ø§Ø­");
					map.put("category", category);
					return map;
				}
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			category = null;
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllProductsByUserId(long userId, int firstIndex, int rowPerPage) {
		Map<String, Object> map = null;
		List<Product> data = null;
		long totalCounts = 0;

		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Product.class);
			criteria.add(Restrictions.eq("userId", userId));
			criteria.setProjection(Projections.distinct(Projections.id()));
			criteria.setMaxResults(rowPerPage);
			criteria.setFirstResult(firstIndex);
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Product.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			Query totalQry = session.createQuery("select count(*) from Product p where p.userId=" + userId);
			totalCounts = (long) totalQry.uniqueResult();
			// logger.info(data.size());

			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("ProductList", data);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> readNotification(long userId) {
		Session session = openDBsession();
		Map<String, Object> map = null;
		long currentTime = TimeUtils.instance().getCurrentTime(TAKE_TIME_SOURSE);
		Query qry = session.createQuery(
				"update  NotificationDetails nd  set nd.readStatus=1  where nd.userId=:userId and nd.lastUpdatedTime <=:currentTime");
		qry.setParameter("userId", userId);
		qry.setParameter("currentTime", currentTime);
		int r = qry.executeUpdate();
		if (r > 0) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, "UPDATE FAILED");
			map.put(ERROR_MESSAGE_ARABIC, "UPDATE FAILED");

		}
		logger.info(map + "map______________________map___________________________________map___________________");
		return map;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllCountry(long countryId, int status) {
		long totalCounts = 0;
		Map<String, Object> map = null;
		List<Country> data = null;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Country.class);
			if (countryId > 0) {
				criteria.add(Restrictions.eq("countryId", countryId));
			}
			String countArry = "";
			if (status == 0 || status == 1) {
				criteria.add(Restrictions.eq("countryStatus", status));
				countArry = " where o.countryStatus=" + status;
			}
			data = criteria.list();
			session.flush();
			StringBuffer strBfr = new StringBuffer("select count(*) from Country o" + countArry);
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();

		} catch (HibernateException e) {

			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data == null || data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("CountryList", data);
			map.put("TotalCounts", totalCounts);
			return map;
		}
	}

	/*
	 * @Override public Map<String, Object> deleteReview(long reviewId, int
	 * mainOrSub, long userId) { Map<String, Object> map = null; long noOfReviews =
	 * 0; double avgRating = 0.0; try { Session session = openDBsession(); if
	 * (mainOrSub == 0) { Criteria criteria = session.createCriteria(Review.class);
	 * criteria.add(Restrictions.eq("reviewId", reviewId));
	 * criteria.add(Restrictions.eq("userId", userId)); List<Review> reviewList =
	 * criteria.list(); session.flush(); if (reviewList != null ||
	 * !reviewList.isEmpty()) { Review review = reviewList.get(0);
	 * review.setStatus(INACTIVE); session.saveOrUpdate(review); session.flush(); if
	 * (reviewList.size() > 1) { Query qry = session.createQuery(
	 * "select avg(rev.rating) from Review rev where rev.productOrShop=:prodOrsh and rev.fatherId=:fathId and rev.status=1"
	 * ); qry.setParameter("prodOrsh", review.getProductOrShop());
	 * qry.setParameter("fathId", review.getFatherId()); avgRating = (double)
	 * qry.uniqueResult(); session.flush(); session.clear(); qry =
	 * session.createQuery(
	 * "select count(*) from Review rev where rev.productOrShop=:prodOrsh and rev.fatherId=:fathId and rev.status=1"
	 * ); qry.setParameter("prodOrsh", review.getProductOrShop());
	 * qry.setParameter("fathId", review.getFatherId()); noOfReviews = (long)
	 * qry.uniqueResult(); session.flush(); session.clear(); } else { avgRating = 0;
	 * noOfReviews = 0; } Query qry = null; if (review.getProductOrShop() ==
	 * PRODUCT) { qry = session.createQuery(
	 * "update from Product p set p.rating=:rat,p.noOfReviews=:noRev where p.productId=:id"
	 * ); } else { qry = session.createQuery(
	 * "update from Shop sh set sh.rating=:rat,sh.noOfReviews=:noRev where sh.shopId=:id"
	 * ); } qry.setParameter("id", review.getFatherId()); qry.setParameter("noRev",
	 * noOfReviews); qry.setParameter("rat", avgRating);
	 * logger.info(review.getFatherId() + ":fid" + noOfReviews + ":no of reviews" +
	 * avgRating + ":rating"); int result = qry.executeUpdate(); session.flush();
	 * logger.info(result); if (result <= 0) { map = new HashMap<>();
	 * map.put(ERROR_CODE, -6); map.put(ERROR_MESSAGE, MESSAGE_SOME_ERROR_OCCURED);
	 * return map; } } else { map = new HashMap<>(); map.put(ERROR_CODE, -34);
	 * map.put(ERROR_MESSAGE, "No such Review or you have no permision to delete");
	 * return map; } } else { Criteria criteria =
	 * session.createCriteria(SubReviews.class);
	 * criteria.add(Restrictions.eq("subreviewId", reviewId));
	 * criteria.add(Restrictions.eq("userId", userId)); List<SubReviews> subReviews
	 * = criteria.list(); if (subReviews != null || !subReviews.isEmpty()) {
	 * SubReviews subReview = (SubReviews) subReviews.get(0); session.flush();
	 * subReview.setStatus(INACTIVE); session.saveOrUpdate(subReview);
	 * session.flush(); } else { map = new HashMap<>(); map.put(ERROR_CODE, -34);
	 * map.put(ERROR_MESSAGE, "No such Review or you have no permision to delete");
	 * return map; } } // closeDBSession(); } catch (Exception e) { //
	 * closeDBSession(); if (TESTING) { e.printStackTrace(); }
	 * logger.info(e.getMessage()); map = new HashMap<>(); map.put(ERROR_CODE, -2);
	 * map.put(ERROR_MESSAGE, e.getMessage()); return map; } map = new HashMap<>();
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put("noOfReviews", noOfReviews); map.put("avgRatings",
	 * avgRating);
	 * 
	 * return map; }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> updateStock(long productId, long productQuantity) {
		Map<String, Object> map = new HashMap<>();
		Session session = openDBsession();

		DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Product.class);
		detachedCriteria.add(Restrictions.eq("productId", productId));

		List<Product> categoryList = (List<Product>) hibernateTemplate.findByCriteria(detachedCriteria);
		if (categoryList.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -9);
			map.put(ERROR_MESSAGE, "product not found");
			map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­ ÙˆØ¸Ø§Ø¦Ù�");
			return map;
		} else {
			Query qry2 = session.createQuery("select p.productQuantity from  Product p where p.productId =:productId");
			qry2.setParameter("productId", productId);
			long qty = (long) qry2.uniqueResult();
			long totalQty = qty + productQuantity;

			Query qry3 = session
					.createQuery("select p.numberOfItemsRemaining from  Product p where p.productId =:productId");
			qry3.setParameter("productId", productId);
			long remainingQty = (long) qry3.uniqueResult();
			long totalRem = remainingQty + productQuantity;

			logger.info(totalQty);
			logger.info(totalRem);
			logger.info(productId);

			Query qry = session.createQuery(
					"update Product p set p.productQuantity =:totalQty , p.numberOfItemsRemaining =:totalRem where p.productId =:productId ");
			qry.setParameter("totalQty", totalQty);
			qry.setParameter("totalRem", totalRem);
			qry.setParameter("productId", productId);

			long res = (long) qry.executeUpdate();

			if (res > 0) {
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				return map;
			}
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllDetails(long userId, long id, int prodOrShop) {
		Map<String, Object> map = new HashMap<>();
		long noOfReviews = 0;
		double avgRating = 0.0;
		try {
			Session session = openDBsession();
			if (userId != 0) {
				StringBuffer strBfr = new StringBuffer();
				if (prodOrShop == PRODUCT) {
					strBfr.append(
							"select Avg(rev.rating) average,count(*) count from review_and_likes rev INNER JOIN product p ON rev.father_id=p.product_id and rev.status=1");
				} else {
					strBfr.append(
							"select Avg(rev.rating) average,count(*) count from review_and_likes rev INNER JOIN sellerDetails s ON rev.father_id=s.seller_id and rev.status=1");
				}
				SQLQuery qry = session.createSQLQuery(strBfr.toString()).addScalar("average", StandardBasicTypes.DOUBLE)
						.addScalar("count", StandardBasicTypes.LONG);
				List<Object[]> data = qry.list();
				Object result[] = (Object[]) data.get(0);
				avgRating = (double) result[0];
				noOfReviews = (long) result[1];
				session.flush();
				session.clear();
				map.put("TOTAL_REVIEW_OF_USER", avgRating);
				map.put("TOTAL_AVG_RATING", noOfReviews);
			}
			if (id > 0) {
				Query qry = session.createQuery(
						"select avg(rev.rating) from Review rev where rev.productOrShop=:prodOrsh and rev.fatherId=:fathId and rev.status=1");
				qry.setParameter("prodOrsh", prodOrShop);
				qry.setParameter("fathId", id);
				avgRating = (double) qry.uniqueResult();
				session.flush();
				session.clear();
				qry = session.createQuery(
						"select count(*) from Review rev where rev.productOrShop=:prodOrsh and rev.fatherId=:fathId and rev.status=1");
				qry.setParameter("prodOrsh", prodOrShop);
				qry.setParameter("fathId", id);
				noOfReviews = (long) qry.uniqueResult();
				session.flush();
				session.clear();
				map.put("NO_OF_REVIEW", noOfReviews);
				map.put("AVG_RATING", avgRating);
			}
		} catch (HibernateException e) {
			if (TESTING) {
				e.printStackTrace();
			}
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		return map;
	}

	/*
	 * @Override public List<Seller> getAllShopsByShopId(long shopId) { List<Seller>
	 * shpList = null; try { Session session = openDBsession(); Criteria cr1 =
	 * session.createCriteria(Seller.class); cr1.add(Restrictions.eq("shopId",
	 * shopId)); shpList = cr1.list();
	 * 
	 * } catch (Exception e) { e.printStackTrace(); } return shpList; }
	 */

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> searchData(Map<Integer, String> data, int mode, int firstIndex, int rowPerPage,
			double latitude, double longitude) {
		Map<String, Object> returnMap = null;
		List<Product> paList = new ArrayList<>();
		List<SellerDetails> storeList = new ArrayList<>();
		List<Category> catList = new ArrayList<>();
		List<SubCategory> subList = new ArrayList<>();

		List<Long> ids = null;
		List<Long> idCat = null;
		List<Long> idSub = null;
		boolean flag = false;
		int f = 0;
		try {
			double maxLat = 0;
			double minLat = 0;
			double maxLon = 0;
			double minLon = 0;
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Product.class);
			Criteria criteria1 = criteria.createCriteria("seller");
			Criteria criteriaShop = session.createCriteria(SellerDetails.class);
			Criteria criteriaCat = session.createCriteria(Category.class);
			Criteria criteriaSub = session.createCriteria(SubCategory.class);
			StringBuffer strBffr = new StringBuffer("select count(*) from Product ");
			/*
			 * StringBuffer strBffrShop = new
			 * StringBuffer("select count(*) from SellerDetails s "); StringBuffer
			 * strBffrCat = new StringBuffer("select count(*) from  Category c ");
			 * StringBuffer strBffrSub = new
			 * StringBuffer("select count(*) from  SubCategory sc ");
			 */

			// --------------------------------------------------------------------------------------------------------------------------------------------------------
			if (data.get(SEARCH_BY_COUNTRY) != null) {
				String value = data.get(SEARCH_BY_COUNTRY);
				if (flag == false) {
					flag = true;
					strBffr.append(" p left join p.seller s where");
					// strBffrShop.append(" where");
				} else {
					strBffr.append(" and ");
					// strBffrShop.append(" and");
				}
				if (!value.equals(""))
					criteria1.add(Restrictions.eq("countryId", Long.valueOf(value)));
				strBffr.append(" s.countryId=" + value);
				criteriaShop.add(Restrictions.eq("countryId", Long.valueOf(value)));
				// strBffrShop.append(" s.countryId=" + value);
				logger.info(value);
			}

			// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			if (data.get(SEARCH_BY_PRODUCT_OR_SERVICE) != null) {
				String value = data.get(SEARCH_BY_PRODUCT_OR_SERVICE);
				if (flag == false) {
					flag = true;
					strBffr.append(" p left join p.seller s where");
				} else {
					strBffr.append(" and");
				}
				if (value.equals("P")) {
					criteria.add(Restrictions.eq("serviceOrProduct", PRODUCT));
					strBffr.append(" p.serviceOrProduct=" + PRODUCT);
				} else {
					criteria.add(Restrictions.eq("serviceOrProduct", SERVICE));
					strBffr.append(" p.serviceOrProduct=" + SERVICE);
				}
			}

			// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

			if (data.get(SEARCH_BY_CITY) != null) {
				String value = data.get(SEARCH_BY_CITY);
				if (flag == false) {
					flag = true;
					strBffr.append(" p left join p.seller s where");
					// strBffrShop.append(" where");
				} else {
					strBffr.append(" and");
					// strBffrShop.append(" and");
				}
				if (!value.equals("")) {
					criteria1.add(Restrictions.eq("cityId", Long.valueOf(value)));
					strBffr.append(" s.cityId=" + value);
					criteriaShop.add(Restrictions.eq("cityId", Long.valueOf(value)));
					// strBffrShop.append(" s.cityId=" + value);
				}
			}
			if (longitude != 0 && latitude != 0) {
				double R = 3960; // earth's mean radius
				double rad = 5000;
				// first-cut bounding box (in degrees)
				maxLat = latitude + rad2deg(rad / R);
				minLat = latitude - rad2deg(rad / R);
				// compensate for degrees longitude getting smaller with increasing latitude
				maxLon = longitude + rad2deg(rad / R / Math.cos(deg2rad(latitude)));
				minLon = longitude - rad2deg(rad / R / Math.cos(deg2rad(latitude)));
				criteria1.add(Restrictions.sqlRestriction(" LONGITUDE BETWEEN " + minLon + " AND " + maxLon
						+ " AND LATITUDE BETWEEN " + minLat + " AND " + maxLat));
				criteriaShop.add(Restrictions.sqlRestriction(" LONGITUDE BETWEEN " + minLon + " AND " + maxLon
						+ " AND LATITUDE BETWEEN " + minLat + " AND " + maxLat));
			}

			// ----------------------------------------------------------------------------------------------------------------------

			if (data.get(SEARCH_BY_NAME) != null) {
				String value = data.get(SEARCH_BY_NAME);
				if (!value.equals("")) {
					if (flag == false) {
						flag = true;
						strBffr.append(" p where ");
						/*
						 * strBffrShop.append(" where "); strBffrCat.append(" where");
						 * strBffrSub.append(" where");
						 */
					} else {
						strBffr.append(" and");
						/*
						 * strBffrShop.append(" and"); strBffrCat.append(" and ");
						 * strBffrSub.append(" and ");
						 */
					}
					if (mode == ENGLISH) {
						// criteria.add(Restrictions.ilike("palaceName", "'%" + value + "%'"));
						criteria.add(Restrictions.sqlRestriction(" PRODUCT_NAME ilike '%" + value + "%' "));
						strBffr.append(" lower(p.productName) like '%" + value.toLowerCase() + "%' ");

						criteriaCat.add(Restrictions.sqlRestriction(" CATEGORY_NAME ilike '%" + value + "%' "));
						// strBffrCat.append(" lower(c.categoryName) like '%" + value.toLowerCase() +
						// "%' ");

						criteriaSub.add(Restrictions.sqlRestriction(" SUBCATEGORY_NAME ilike '%" + value + "%' "));
						// strBffrSub.append(" lower(sc.subCategoryName) like '%" + value.toLowerCase()
						// + "%' ");

						criteriaShop.add(Restrictions.sqlRestriction(" SELLER_NAME ilike '%" + value + "%' "));
						// strBffrShop.append(" lower(s.sellerName) like '%" + value.toLowerCase() + "%'
						// ");

					} else

					{
						criteria.add(Restrictions.sqlRestriction(" PRODUCT_NAME_ARABIC ilike '%" + value + "%' "));
						strBffr.append(" lower(p.productNameAr) like '%" + value.toLowerCase() + "%' ");
						criteriaShop.add(Restrictions.sqlRestriction(" STORE_NAME_AR ilike '%" + value + "%' "));
						// strBffrShop.append(" lower(s.sellerNameAr) like '%" + value.toLowerCase() +
						// "%' ");
						criteriaSub.add(Restrictions.sqlRestriction(" SUBCATEGORY_NAME_AR ilike '%" + value + "%' "));
						// strBffrSub.append(" lower(sc.subCategoryNameAr) like '%" +
						// value.toLowerCase() + "%' ");
						criteria.add(Restrictions.sqlRestriction(" CATEGORY_NAME_AR ilike '%" + value + "%' "));
						// strBffrCat.append(" lower(c.categoryNameAr) like '%" + value.toLowerCase() +
						// "%' ");

					}
				}
			}
			// ------------------------------------------------------------------------------------------------------------------------------

			criteria.add(Restrictions.eq("status", ACTIVE));
			Query qry = session.createQuery(strBffr.toString());
			logger.info(qry + "qry______________________________________________________qry");

			long count = (long) qry.uniqueResult();
			criteria.setProjection(Projections.distinct(Projections.id()));

			// --------------------------------------------------------------------------------------------------------------------------------
			/*
			 * Query qry2 = session.createQuery(strBffrCat.toString()); long countCat =
			 * (long) qry2.uniqueResult(); logger.info(countCat +
			 * "countCat______________________________________________________");
			 */
			criteriaCat.setProjection(Projections.distinct(Projections.id()));
			idCat = criteriaCat.list();
			logger.info(idCat + "idCat--------------------------------------------------------------------");
			session.flush();

			// --------------------------------------------------------------------------------------------------------------------------------

			// --------------------------------------------------------------------------------------------------------------------------------
			/*
			 * Query qry3 = session.createQuery(strBffrSub.toString()); long countSub =
			 * (long) qry3.uniqueResult(); logger.info(strBffrSub +
			 * "strBffrSub______________________________________________________");
			 */
			criteriaSub.setProjection(Projections.distinct(Projections.id()));
			idSub = criteriaSub.list();
			logger.info(
					idSub + "idSub----------------------^^^^^^^^^^^^----------------------------------------------");
			session.flush();

			// --------------------------------------------------------------------------------------------------------------------------------

			/*
			 * Query qryShop = session.createQuery(strBffrShop.toString()); long countShop =
			 * (long) qryShop.uniqueResult();
			 */
			criteria.setProjection(Projections.distinct(Projections.id()));
			ids = criteria.list();
			criteriaShop.setProjection(Projections.distinct(Projections.id()));
			List<Long> idShops = criteriaShop.list();
			session.flush();

			// --------------------------------------------------------------------------------------------------------------------------------

			if (ids != null || idShops != null || idCat != null || idSub != null) {
				StringBuffer stringBuff = null;
				// if (ids != null && !ids.isEmpty()) {
				logger.info(ids + "idSub-------+++++++-----------------");
				flag = false;
				stringBuff = new StringBuffer("select p from Product p");
				// }
				/*
				 * if (idShops != null && !idShops.isEmpty()) { strBffrShop = new
				 * StringBuffer(" select s from SellerDetails s  "); }
				 */
				/*
				 * if (idSub != null && !idSub.isEmpty()) { logger.info(idSub +
				 * "idSub-------+++++++-----------------"); stringBuff3 = new
				 * StringBuffer(" select sc from SubCategory sc  ");
				 * 
				 * } if (idCat != null && !idCat.isEmpty()) { logger.info(idCat +
				 * "idCat-------+++++++-----------------"); stringBuff2 = new
				 * StringBuffer(" select c from Category c  ");
				 * 
				 * }
				 */

				// ---------------------------------------------------------------------------------------------------------------------------------
				/*
				 * if (!flag) { if (ids != null && !ids.isEmpty()) { logger.info(
				 * "---------------------------------------------------------------------");
				 * 
				 * stringBuff.append("  where p.productId in (:ids) and p.status=1 "); } if
				 * (idShops != null && !idShops.isEmpty()) {
				 * 
				 * strBffrShop.append("  where s.sellerId in (:idShops) and s.status=1  "); }
				 * 
				 * 
				 * if (idCat != null && !idCat.isEmpty()) { stringBuff2.
				 * append("  where c.categoryId in (:idCat) and c.status=1 order by "); }
				 * 
				 * 
				 * if (idCat != null && !idCat.isEmpty()) {
				 * 
				 * stringBuff.append("  and p.categoryId in (:idCat) and p.status=1  "); }
				 * 
				 * if (idSub != null && !idSub.isEmpty()) {
				 * 
				 * stringBuff.append("  and p.subCategoryId in (:idSub) and p.status=1  "); }
				 * 
				 * flag = true; } else {
				 */
				boolean subOnly = false;// added
				boolean catOnly = false;
				boolean flagId = false;
				boolean flagCat = false;
				boolean flagSubCat = false;
				if (ids != null && !ids.isEmpty()) {

					flagCat = true;// added
					flagId = true;
					stringBuff.append("  where p.productId in (:ids) and p.status=1 ");
				}
				/*
				 * if (idShops != null && !idShops.isEmpty()) {
				 * 
				 * strBffrShop.append(" where s.sellerId in (:idShops) and s.status=1 "); }
				 */
				if (idCat != null && !idCat.isEmpty()) {
					if (!flagId) {
						stringBuff.append(" where");
						catOnly = true;
					} else {
						stringBuff.append(" or");

					}
					flagCat = true;

					flagId = false;

					stringBuff.append("  p.categoryId in (:idCat) and p.status=1  ");

				}
				if (idSub != null && !idSub.isEmpty()) {
					if (!flagCat) {
						// stringBuff.append(" where");//first
						stringBuff.append(" where ");
						subOnly = true;
					} else {
						stringBuff.append(" and");
						subOnly = false;

					}
					flagSubCat = true;
					// added
					flagId = true;
					logger.info(idSub + "idSub-------+++++++-----------------");
					stringBuff.append(" p.subCategoryId in (:idSub) and p.status=1 ");

				}

				// }
				boolean isFirstOne = true;

				// --------------------------------------------------------------------------------------------------------------------------------

				if (data.get(SEARCH_BY_REVIEWS) != null) {
					// criteria.addOrder(Order.desc("rating"));
					stringBuff.append(" order by p.noOfReviews desc ");
					// strBffrShop.append(" order by s.noOfReviews desc ");
					isFirstOne = false;
				}

				// -------------------------------------------------------------------------------------------------------------------------------

				if (data.get(SEARCH_BY_RATING) != null) {
					// criteria.addOrder(Order.desc("rating"));
					if (isFirstOne) {
						stringBuff.append(",");
						// strBffrShop.append(",");
						isFirstOne = false;
					}
					stringBuff.append("order by p.rating desc");
				}

				// -------------------------------------------------------------------------------------------------------------------------------
				if (data.get(SEARCH_BY_RANK) != null) {
					if (isFirstOne) {
						stringBuff.append(",");
						// strBffrShop.append(",");
						isFirstOne = false;
					}
					stringBuff.append("order by p.rank asc");
				}
				if (longitude > 0 && latitude > 0) {
					if (isFirstOne) {
						stringBuff.append(",");
						// strBffrShop.append(",");
						isFirstOne = false;
					}
					stringBuff.append(
							"(POW((p.longitude-" + longitude + "),2) + POW((p.latitude-" + latitude + "),2)) asc");
				}

				// ---------------------------------------------------------------------------------------------------------------------------------
				if (data.get(SEARCH_BY_PRICE) != null) {
					if (isFirstOne) {
						stringBuff.append(",");
						isFirstOne = false;
					}
					String ascOrDes = data.get(SEARCH_BY_PRICE);
					logger.info(ascOrDes);
					if (ascOrDes == ASC || ascOrDes == DESC) {
						stringBuff.append(" order by p.price " + ascOrDes);
					} else {
						returnMap = new HashMap<>();
						returnMap.put(ERROR_CODE, -9);
						returnMap.put(ERROR_MESSAGE, "Price format ascending or descending not provide correctly");
						returnMap.put(ERROR_MESSAGE_ARABIC,
								"Ã˜ÂªÃ™â€ Ã˜Â³Ã™Å Ã™â€š Ã˜Â§Ã™â€žÃ˜Â³Ã˜Â¹Ã˜Â± Ã˜ÂªÃ˜ÂµÃ˜Â§Ã˜Â¹Ã˜Â¯Ã™Å  Ã˜Â£Ã™Ë† Ã˜ÂªÃ™â€ Ã˜Â§Ã˜Â²Ã™â€žÃ™Å  Ã™â€žÃ˜Â§ Ã˜ÂªÃ™Ë†Ã™ï¿½Ã˜Â± Ã˜Â¨Ã˜Â´Ã™Æ’Ã™â€ž Ã˜ÂµÃ˜Â­Ã™Å Ã˜Â­");
						return returnMap;
					}

				}
				// --------------------------------------------------------------------------------------------------------------------------------
				Query qrQuery = null;
				if (ids != null && !ids.isEmpty() || idCat != null && !idCat.isEmpty()
						|| idSub != null && !idSub.isEmpty()) {
					qrQuery = session.createQuery(stringBuff.toString());// added-------------------

					logger.info(flagId + "flagId");
					logger.info(flagSubCat + "flagSubCat");
					logger.info(flagCat + "flagCat");

					logger.info(idSub);
					logger.info(idCat);
					logger.info(ids);

					if (subOnly == true) {
						qrQuery.setParameterList("idSub", idSub);
					} else if (subOnly == true) {
						qrQuery.setParameterList("idCat", idCat);
					}

					else if (flagId == true && flagSubCat == false && flagCat == true) {
						logger.info("-______________________________________________________________mohan1");
						qrQuery.setParameterList("ids", ids);
						/*
						 * qrQuery.setParameterList("idSub", idSub); qrQuery.setParameterList("idCat",
						 * idCat);
						 */
					}

					else if (flagSubCat) {
						logger.info(
								idSub + "-______________________________________________________________mohan41");
						logger.info(ids + "-______________________________________________________________mohan41");
						logger.info(
								idCat + "-______________________________________________________________mohan41");

						if (flagCat == false && flagId == false && flagSubCat == true) {
							logger.info("-______________________________________________________________mohan41");
							qrQuery.setParameterList("idSub", idSub);
							qrQuery.setParameterList("ids", ids);
						}

						// add
						else if (flagId == true && flagSubCat == true && flagCat == false) {
							qrQuery.setParameterList("idSub", idSub);

							qrQuery.setParameterList("ids", ids);
						}
						// add
						else if (idSub.size() > 0 && idCat.size() > 0 && ids.size() > 0) {

							logger.info("-______________________________________________________________mohan2");
							qrQuery.setParameterList("idSub", idSub);

							qrQuery.setParameterList("ids", ids);

							qrQuery.setParameterList("idCat", idCat);

						} /*
							 * else if(idSub.size() >0 && idCat.size()>0 &&ids.size() == 0 ) {
							 * qrQuery.setParameterList("idSub", idSub); qrQuery.setParameterList("idCat",
							 * idCat); }
							 */

						else {
							qrQuery.setParameterList("idSub", idSub);

							qrQuery.setParameterList("ids", ids);
						}

					} else if (flagCat) {
						logger.info("-______________________________________________________________mohan3");

						qrQuery.setParameterList("idCat", idCat);
						qrQuery.setParameterList("idSub", idSub);
					} else if (flagId == true && flagSubCat == true && flagCat == true) {
						qrQuery.setParameterList("idSub", idSub);

						qrQuery.setParameterList("ids", ids);
					}

					/// added
					/*
					 * else if(flagCat == false && flagId == false && flagSubCat == true ) {
					 * logger.info(
					 * "-______________________________________________________________mohan4");
					 * qrQuery.setParameterList("idCat", idCat); qrQuery.setParameterList("ids",
					 * ids);
					 * 
					 * }
					 */

					if (rowPerPage > 0) {
						qrQuery.setFirstResult(firstIndex);
						qrQuery.setMaxResults(rowPerPage);
					}
					logger.info(qrQuery + "qrQuery------------------------||_______________________________________");
					paList = qrQuery.list();
					logger.info(paList + "paList------------------------||_______________________________________");
				}

				// ----------------------------------------------------------------------------------------------------------------------------------
				/*
				 * if (idShops != null && !idShops.isEmpty()) { Query shopQuery =
				 * session.createQuery(strBffrShop.toString());
				 * 
				 * shopQuery.setParameterList("idShops", idShops);
				 * 
				 * if (rowPerPage > 0) { shopQuery.setFirstResult(firstIndex);
				 * shopQuery.setMaxResults(rowPerPage); }
				 * 
				 * storeList = shopQuery.list(); session.flush(); }
				 */

				// ----------------------------------------------------------------------------------------------------------------------------------
				/*
				 * Query catQuery2 = null; if (idCat != null && !idCat.isEmpty()) { logger.info(
				 * stringBuff2 +
				 * "stringBuff2_______________------------______________-------------------");
				 * catQuery2 = session.createQuery(stringBuff2.toString()); logger.info( idCat +
				 * "idcat========================================================================"
				 * ); catQuery2.setParameterList("idCat", idCat); if (rowPerPage > 0) {
				 * catQuery2.setFirstResult(firstIndex); catQuery2.setMaxResults(rowPerPage); }
				 * logger.info(catQuery2 + "catQuery-----____________---------------"); catList
				 * = catQuery2.list(); session.flush(); }
				 */
				// -------------------
				/*
				 * Query catQuery3 = null; logger.info(idSub +
				 * "idSub---------------------@@@@--------------------"); if (idSub != null &&
				 * !idSub.isEmpty()) { logger.info(stringBuff3 +
				 * "stringBuff3-----------------------------------------"); logger.info(idSub +
				 * "idSub-----------------------------------------"); catQuery3 =
				 * session.createQuery(stringBuff3.toString());
				 * catQuery3.setParameterList("idSub", idSub);
				 * 
				 * if (rowPerPage > 0) { catQuery3.setFirstResult(firstIndex);
				 * catQuery3.setMaxResults(rowPerPage); } logger.info(catQuery3 +
				 * "catQuery3-----____________---------------"); subList = catQuery3.list();
				 * 
				 * session.flush(); }
				 */
				// ----------------------------------
				/*
				 * logger.info(subList +
				 * "subList____********************************___________-----------------------___________________"
				 * ); logger.info(paList +
				 * "paList____********************************___________-----------------------___________________"
				 * ); logger.info(catList +
				 * " catList______________**************************-------------------------------------"
				 * );
				 */

			}
			if (paList != null && !paList.isEmpty() || storeList != null && !storeList.isEmpty()
					|| subList != null && !subList.isEmpty() || catList != null && !catList.isEmpty())

			{

				returnMap = new HashMap<>();
				returnMap.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				returnMap.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				returnMap.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				returnMap.put("PRODUCTS", paList);
				returnMap.put("SHOPS", storeList);
				returnMap.put("CATEGORY", catList);
				returnMap.put("SUBCATEGORY", subList);
				returnMap.put("TotalCountsProduct", count);
				// returnMap.put("TotalCountsShop", countShop);
				// returnMap.put("TotalCountsCat", countCat);
				// returnMap.put("TotalCountsSubCat", countSub);
				returnMap.put("idCat", idCat);

			}

			else {

				returnMap = new HashMap<>();
				returnMap.put("TotalCountsProduct", count);
				// returnMap.put("TotalCountsShop", countShop);
				// returnMap.put("TotalCountsCat", countCat);
				// returnMap.put("TotalCountsSubCat", countSub);
				returnMap.put(ERROR_CODE, -1);
				returnMap.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
				returnMap.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);

			}
			return returnMap;
		} catch (Exception e) {
			if (TESTING) {
				e.printStackTrace();
			}
			returnMap = new HashMap<>();
			returnMap.put(ERROR_CODE, -4);
			returnMap.put(ERROR_MESSAGE, e.getMessage());
			return returnMap;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> filterData(int mode, int firstIndex, int rowPerPage, long categoryId) {
		Map<String, Object> returnMap = null;
		List<Product> paList = new ArrayList<>();
		long count1 = 0;

		List<Long> idCat = null;

		boolean flag = false;
		try {

			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Product.class);
			StringBuffer strBffr = new StringBuffer("select count(*) from Product p  ");
			criteria.add(Restrictions.eq("status", ACTIVE));
			Query qry = session.createQuery(strBffr.toString());
			logger.info(qry + "qry^^^^^^^^^^^^&&^^^^^^^^^^^^^^^^^^^^^^^");
			long count = (long) qry.uniqueResult();
			criteria.setProjection(Projections.distinct(Projections.id()));
			idCat = criteria.list();
			logger.info(idCat + "idCat--------------------------------------------------------------------");
			session.flush();

			if (categoryId > 0) {

				strBffr.append(" where");

				strBffr.append(" p.categoryId=" + categoryId);
				criteria.add(Restrictions.eq("categoryId", categoryId));
			}
			if (idCat != null) {
				StringBuffer stringBuff = null;

				if (idCat != null && !idCat.isEmpty()) {
					stringBuff = new StringBuffer("select count(*)  from Product p");
				}

				if (!flag) {
					if (idCat != null && !idCat.isEmpty()) {

						stringBuff.append("  where p.productId in (:idCat) and p.status=1 ");
						if (categoryId > 0) {
							stringBuff.append("  and  p.categoryId=" + categoryId);
							criteria.add(Restrictions.eq("categoryId", categoryId));
						}

					}

					flag = true;
				} else {
					if (idCat != null && !idCat.isEmpty()) {

						stringBuff.append("  where p.productId in (:idCat) and p.status=1 ");
						if (categoryId > 0) {
							stringBuff.append("  and  p.categoryId=" + categoryId);
							criteria.add(Restrictions.eq("categoryId", categoryId));
						}

					}

				}
				if (mode == HIGH_TO_LOW) {
					if (idCat != null && !idCat.isEmpty()) {
						stringBuff.append("group by p.productId  order by p.price desc ");

					}
				} else if (mode == LOW_TO_HIGH) {
					if (idCat != null && !idCat.isEmpty()) {
						stringBuff.append("group by p.productId  order by p.price asc ");

					}
				} else if (mode == POPULARITY) {
					//
					if (idCat != null && !idCat.isEmpty()) {
						stringBuff.append("group by p.productId  order by p.ratingPercentage desc ");

					}

				}
				Query qrQuery1 = session.createQuery(stringBuff.toString());

				qrQuery1.setParameterList("idCat", idCat);

				Query qrQuery = null;
				if (idCat != null && !idCat.isEmpty()) {

					stringBuff.replace(7, 15, "p");

					qrQuery = session.createQuery(stringBuff.toString());

					qrQuery.setParameterList("idCat", idCat);
					if (rowPerPage > 0) {
						qrQuery.setFirstResult(firstIndex);
						qrQuery.setMaxResults(rowPerPage);
					}

					paList = qrQuery.list();

					/*
					 * if(mode == HIGH_TO_LOW) { if (idCat != null && !idCat.isEmpty()) { criteria =
					 * session.createCriteria(Product.class); criteria.add(Restrictions.in("id",
					 * idCat)); criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					 * criteria.setFirstResult(firstIndex); criteria.setMaxResults(rowPerPage);
					 * criteria.addOrder(Order.desc("price")); paList = criteria.list();
					 * session.flush();
					 * 
					 * } }else if(mode == LOW_TO_HIGH) { if (idCat != null && !idCat.isEmpty()) {
					 * criteria = session.createCriteria(Product.class);
					 * criteria.add(Restrictions.in("id", idCat));
					 * criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					 * criteria.setFirstResult(firstIndex); criteria.setMaxResults(rowPerPage);
					 * criteria.addOrder(Order.asc("price")); paList = criteria.list();
					 * session.flush();
					 * 
					 * }
					 * 
					 * 
					 * 
					 * 
					 * 
					 * 
					 * //criteria.addOrder(Order.asc("price")); }else if(mode == POPULARITY) { // if
					 * (idCat != null && !idCat.isEmpty()) { criteria =
					 * session.createCriteria(Product.class); criteria.add(Restrictions.in("id",
					 * idCat)); criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					 * criteria.setFirstResult(firstIndex); criteria.setMaxResults(rowPerPage);
					 * criteria.addOrder(org.hibernate.criterion.Order.desc("ratingPercentage"));
					 * paList = criteria.list(); session.flush();
					 * 
					 * }
					 * 
					 * }
					 */
					criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

				}

			}

			if (paList != null && !paList.isEmpty())

			{

				returnMap = new HashMap<>();
				returnMap.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				returnMap.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				returnMap.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
				returnMap.put("PRODUCTS", paList);

				returnMap.put("TotalCountsProduct", count);
				// returnMap.put("TotalCounts", paList.size());
				returnMap.put("idCat", idCat);

			}

			else {

				returnMap = new HashMap<>();
				returnMap.put("TotalCountsProduct", count);
				// returnMap.put("TotalCounts", count1);
				returnMap.put(ERROR_CODE, -1);
				returnMap.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
				returnMap.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);

			}
			return returnMap;
		} catch (Exception e) {
			if (TESTING) {
				e.printStackTrace();
			}
			returnMap = new HashMap<>();
			returnMap.put(ERROR_CODE, -4);
			returnMap.put(ERROR_MESSAGE, e.getMessage());
			return returnMap;
		}
	}

	/* The function to convert radians into decimal */
	private double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}

	/* The function to convert decimal into radians */
	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/*
	 * @Override public Map<String, Object> getAllShopsBySellerId(long sellerId, int
	 * firstIndex, int rowPerPage) { Map<String, Object> map = null; List<Store>
	 * data = null; long totalCounts = 0; try { Session session = openDBsession();
	 * Criteria criteria = session.createCriteria(Store.class);
	 * criteria.add(Restrictions.eq("userId", sellerId));
	 * criteria.setProjection(Projections.distinct(Projections.id()));
	 * criteria.setMaxResults(rowPerPage); criteria.setFirstResult(firstIndex);
	 * List<Long>ids = criteria.list(); session.flush();
	 * 
	 * if (ids != null && !ids.isEmpty()) { criteria =
	 * session.createCriteria(Store.class); criteria.add(Restrictions.in("id",
	 * ids)); criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); data =
	 * criteria.list();
	 * 
	 * session.flush(); } session.flush(); Query totalQry =
	 * session.createQuery("select count(*) from Store"); totalCounts = (long)
	 * totalQry.uniqueResult(); session.flush(); // closeDBSession(); } catch
	 * (HibernateException e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } logger.info(e.getMessage()); map = new HashMap<>();
	 * map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE, e.getMessage()); throw e; }
	 * if (data != null && !data.isEmpty()) { map = new HashMap<>();
	 * map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR); map.put("ShopList", data);
	 * map.put("TotalCounts", totalCounts); return map; } else { map = new
	 * HashMap<>(); map.put(ERROR_CODE, -3); map.put(ERROR_MESSAGE,
	 * MESSAGE_NO_DATA_TO_SHOW); map.put(ERROR_MESSAGE_ARABIC,
	 * MESSAGE_NO_DATA_TO_SHOW_AR); return map; } }
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveOrUpdateCountry(Country country)

	{

		Map<String, Object> map = null;
		List<Country> countryList = null;
		try {
			Session session = openDBsession();
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(Country.class);
			detachedCriteria.add(Restrictions.eq("countryName", country.getCountryName()));
			countryList = (List<Country>) hibernateTemplate.findByCriteria(detachedCriteria);
			map = new HashMap<>();

			if (countryList == null || countryList.isEmpty()) {
				session.save(country);
				session.flush();
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, "نجاح");
				return map;
			}

			else if (country.getCountryId() > 0) {
				if (country.getCountryName().equals(countryList.get(0).getCountryName())) {
					map.put(ERROR_CODE, -3);
					map.put(ERROR_MESSAGE, "DUBLICATE DATA");
					map.put(ERROR_MESSAGE_ARABIC, "بيانات متكررة");
					return map;
				}
				detachedCriteria = DetachedCriteria.forClass(Country.class);
				detachedCriteria.add(Restrictions.eq("countryId", country.getCountryId()));
				countryList = (List<Country>) hibernateTemplate.findByCriteria(detachedCriteria);
				if (countryList == null || countryList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such country");
					map.put(ERROR_MESSAGE_ARABIC, "لا يوجد بلد من هذا القبيل");
					return map;
				} else {
					session.update(country);
					session.flush();
				}
			} else {
				map.put(ERROR_CODE, -3);
				map.put(ERROR_MESSAGE, "DUBLICATE DATA");
				map.put(ERROR_MESSAGE_ARABIC, "بيانات متكررة");
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			country = null;
			throw e;
		}

		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, "نجاح");
		map.put("country", country);
		return map;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> saveOrUpdateCity(City city)

	{

		Map<String, Object> map = null;
		List<City> cityList = null;
		try {
			Session session = openDBsession();
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass(City.class);
			detachedCriteria.add(Restrictions.eq("cityName", city.getCityName()));
			cityList = (List<City>) hibernateTemplate.findByCriteria(detachedCriteria);

			map = new HashMap<>();
			if (cityList == null || cityList.isEmpty()) {
				//logger.info(city.getCountryId() + "++++++++++++++++++++++++++++++++");
				//city.setCountry(null);
				session.save(city);
				session.flush();
				map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, "نجاح");
				return map;
			} else if (city.getCityId() > 0) {
				if (city.getCityName().equals(cityList.get(0).getCityName())) {
					map.put(ERROR_CODE, -3);
					map.put(ERROR_MESSAGE, "DUBLICATE DATA");
					map.put(ERROR_MESSAGE_ARABIC, "بيانات متكررة");
					return map;
				}
				detachedCriteria = DetachedCriteria.forClass(City.class);
				detachedCriteria.add(Restrictions.eq("cityId", city.getCityId()));
				cityList = (List<City>) hibernateTemplate.findByCriteria(detachedCriteria);
				if (cityList == null || cityList.isEmpty()) {
					map.put(ERROR_CODE, -4);
					map.put(ERROR_MESSAGE, "No such city");
					map.put(ERROR_MESSAGE_ARABIC, "لا يوجد مثل هذه المدينة");
					return map;
				} else {
					session.update(city);
					session.flush();
				}
			} else {
				map.put(ERROR_CODE, -3);
				map.put(ERROR_MESSAGE, "DUBLICATE DATA");
				map.put(ERROR_MESSAGE_ARABIC, "بيانات متكررة");
			}

		} catch (HibernateException e) {
			e.printStackTrace();
			logger.debug(e);
			city = null;
			throw e;
		}

		map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
		map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
		map.put(ERROR_MESSAGE_ARABIC, "نجاح");
		map.put("city", city);
		return map;

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllCity(long countryId, long cityId, String cityName, int firstIndex, int rowPerPage,
			int status)

	{
		Map<String, Object> map = null;
		List<Country> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(City.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from City o");
			if (countryId > 0) {
				flag = true;
				strBfr.append(" where o.countryId=" + countryId);
				criteria.add(Restrictions.eq("countryId", countryId));
			}
			if (cityId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.cityId=" + cityId);
				criteria.add(Restrictions.eq("cityId", cityId));
			}

			if (cityName != null && !cityName.equals("")) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append("  lower(o.cityName) like '%" + cityName.toLowerCase() + "%'");
				criteria.add(Restrictions.ilike("cityName", "'%" + cityName + "%'"));
			}
			if (status == 1 || status == 0) {
				if (flag == false) {
					strBfr.append(" where o.citusStatus=" + status);
				} else {
					strBfr.append(" and o.citusStatus=" + status);
				}
				criteria.add(Restrictions.eq("citusStatus", status));
			}
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(City.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­");

			map.put("CityList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllOfferProduct(long offerId) {
		Map<String, Object> map = null;
		List<Offer> data = null;
		List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Offer.class);

			StringBuffer strBfr = new StringBuffer("select count(*) from Offer o");
			if (offerId > 0) {

				strBfr.append(" where o.offerId=" + offerId);
				criteria.add(Restrictions.eq("offerId", offerId));
			}

			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));

			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Offer.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			/*
			 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
			 * criteria.add(Restrictions.eq("orderId", orderId));
			 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
			 * criteria.list();
			 * 
			 * session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­");

			map.put("CityList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllOffer(long offerId, long offerEnds, long offerStarts, int status, String offerName,
			String offerNameAr, int firstIndex, int rowPerPage) {
		Map<String, Object> map = null;
		List<Country> data = null;
		List<Review> reviewList = null;
		List<Long> idSub = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			long currentMill = TimeUtils.instance().getStartOfCurrentDay();
			logger.info(currentMill + "currentMill*******************************************");
			Query qry1 = session.createQuery(
					"update Offer set status=0,offerRate=:offerRate,offerPercentage=:offerPercentage where offerEnds <= :date ");
			qry1.setParameter("date", currentMill);
			qry1.setParameter("offerRate", 0.0);
			qry1.setParameter("offerPercentage", 0L);
			//logger.info(qry1 + "qry1_____________________________))))))))))))))))))))))))))))))))))");
			double result = (double) qry1.executeUpdate();
			//logger.info(result + "result_______________________________");
			if (result < 0) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -7);
				map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
				return map;
			} else {
				Query qry2 = session.createQuery("select o.offerId from  Offer o where status=0");
				idSub = qry2.list();
				if (idSub.size() > 0) {
					Query qry3 = session
							.createQuery("update OfferDetails o set o.status=0  where o.offerId in (:idSub) ");
					qry3.setParameterList("idSub", idSub);
					int result1 = qry3.executeUpdate();

					Query qry4 = session.createQuery("update Product o set o.offerId=0  where o.offerId in (:idSub) ");
					qry4.setParameterList("idSub", idSub);
					int result4 = qry4.executeUpdate();
				//	logger.info(result4 + "result4*******************************************");
				}
			}

			Criteria criteria = session.createCriteria(Offer.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Offer o");
			if (offerId > 0) {
				flag = true;
				strBfr.append(" where o.offerId=" + offerId);
				criteria.add(Restrictions.eq("offerId", offerId));
			}

			if (offerName != null && !offerName.equals("")) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append("  lower(o.offerName) like '%" + offerName.toLowerCase() + "%'");
				criteria.add(Restrictions.ilike("offerName", "'%" + offerName + "%'"));
			}

			if (offerNameAr != null && !offerNameAr.equals("")) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append("  lower(o.offerNameAr) like '%" + offerNameAr.toLowerCase() + "%'");
				criteria.add(Restrictions.ilike("offerNameAr", "'%" + offerNameAr + "%'"));
			}

			if (status == 1 || status == 0) {
				if (flag == false) {
					strBfr.append(" where o.status=" + status);
				} else {
					strBfr.append(" and o.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}
			//logger.info(strBfr + "strBfr_____________________________))))))))))))))))))))))))))))))))))");
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			//logger.info(ids + "ids____________________ids____________________________ids");
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Offer.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­");

			map.put("CityList", data);
			map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllCms(long cmsId, int rowPerPage, int currentIndex) {
		Map<String, Object> map = null;
		List<CmsMangement> data = null;
		// List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(CmsMangement.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from CmsMangement o");
			if (cmsId > 0) {
				flag = true;
				strBfr.append(" where o.cmsId=" + cmsId);
				criteria.add(Restrictions.eq("cmsId", cmsId));
			}

			/*
			 * if (companyId > 0) { if (flag == false) { flag = true;
			 * strBfr.append(" where"); } else { strBfr.append(" and"); }
			 * strBfr.append(" o.companyId=" + companyId);
			 * criteria.add(Restrictions.eq("companyId", companyId)); }
			 * 
			 * if (!manufactureName.equals("")) { if (flag == false) { flag = true;
			 * strBfr.append(" where"); } else { strBfr.append(" and"); }
			 * strBfr.append("  lower(o.manufactureName) like '%" +
			 * manufactureName.toLowerCase() + "%'");
			 * criteria.add(Restrictions.ilike("manufactureName", "'%" + manufactureName +
			 * "%'")); } if (status == 1 || status == 0) { if (flag == false) {
			 * strBfr.append(" where o.status=" + status); } else {
			 * strBfr.append(" and o.status=" + status); }
			 * criteria.add(Restrictions.eq("status", status)); }
			 */
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(currentIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(CmsMangement.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			/*
			 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
			 * criteria.add(Restrictions.eq("orderId", orderId));
			 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
			 * criteria.list();
			 * 
			 * session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­");

			map.put("CMSLIST", data);
			// map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAttributes(String attributeName, String attributeNameAr, int firstIndex,
			int rowPerPage, int status) {
		Map<String, Object> map = null;
		List<Attributes> data = null;

		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Attributes.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Attributes o ");

			if (!attributeName.equals("") && !attributeName.equals("")) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append("  lower(o.attributeName) like '%" + attributeName.toLowerCase() + "%'");
				criteria.add(Restrictions.ilike("attributeName", "%" + attributeName + "%"));

			}
			if (!attributeNameAr.equals("") && !attributeNameAr.equals("")) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append("  lower(o.attributeNameAr) like '%" + attributeNameAr.toLowerCase() + "%'");
				criteria.add(Restrictions.ilike("attributeNameAr", "%" + attributeNameAr + "%"));
			}
			if (status == 1 || status == 0) {
				if (flag == false) {
					strBfr.append(" where o.status=" + status);
				} else {
					strBfr.append(" and o.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}
			Query qry = session.createQuery(strBfr.toString());
			logger.info(qry + "qry---------------------------------------------------------8");
			totalCounts = (long) qry.uniqueResult();
			logger.info(totalCounts + "totalCounts-------------------------------------------12");
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
				logger.info(rowPerPage + "   " + firstIndex);
			}

			List<Long> ids = criteria.list();

			logger.info(ids + "ids---------------------------------------------------------18");

			session.flush();
			session.clear();

			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Attributes.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		logger.info(data + "data---------------------------------------------------------8");
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­");
			map.put("ATTRIBUTES", data);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getallManufacture(long manufactureId, String manufactureName, int status, int firstIndex,
			int rowPerPage, long companyId) {
		Map<String, Object> map = null;
		List<Manufacture> data = null;
		// List<Review> reviewList = null;
		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Manufacture.class);
			boolean flag = false;
			StringBuffer strBfr = new StringBuffer("select count(*) from Manufacture o");
			if (manufactureId > 0) {
				flag = true;
				strBfr.append(" where o.manufactureId=" + manufactureId);
				criteria.add(Restrictions.eq("manufactureId", manufactureId));
			}

			if (companyId > 0) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append(" o.companyId=" + companyId);
				criteria.add(Restrictions.eq("companyId", companyId));
			}

			if (manufactureName != null && !manufactureName.equals("")) {
				if (flag == false) {
					flag = true;
					strBfr.append(" where");
				} else {
					strBfr.append(" and");
				}
				strBfr.append("  lower(o.manufactureName) like '%" + manufactureName.toLowerCase() + "%'");
				criteria.add(Restrictions.ilike("manufactureName", "'%" + manufactureName + "%'"));
			}
			if (status == 1 || status == 0) {
				if (flag == false) {
					strBfr.append(" where o.status=" + status);
				} else {
					strBfr.append(" and o.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(firstIndex);
			}
			List<Long> ids = criteria.list();
			session.flush();
			session.clear();
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(Manufacture.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}
			/*
			 * if (orderId > 0) { criteria = session.createCriteria(Review.class);
			 * criteria.add(Restrictions.eq("orderId", orderId));
			 * //criteria.add(Restrictions.eq("productOrShop", PRODUCT)); reviewList =
			 * criteria.list();
			 * 
			 * session.flush(); }
			 */
			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			return map;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, "Ù†Ø¬Ø§Ø­");

			map.put("ManufactureList", data);
			// map.put("Reviews", reviewList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllCountry(long countryId) {
		long totalCounts = 0;

		Map<String, Object> map = null;
		List<Country> data = null;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Country.class);
			if (countryId > 0) {
				criteria.add(Restrictions.eq("countryId", countryId));
			}

			data = criteria.list();
			session.flush();
			StringBuffer strBfr = new StringBuffer("select count(*) from Country o");
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();

			// closeDBSession();
		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data == null || data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		} else {

			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("CountryList", data);
			map.put("TotalCounts", totalCounts);
			return map;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> changeProductStatus(long productId, int status) {
		Map<String, Object> map = null;
		int result = 0;
		try {

			Session session = openDBsession();

			DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(Product.class);
			detachedCriteria2.add(Restrictions.eq("productId", productId));
			List<Product> dvList = (List<Product>) hibernateTemplate.findByCriteria(detachedCriteria2);
			if (dvList == null || dvList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "productId " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}
			Query qry2 = session.createQuery("update Product set status=:status where productId=:productId");
			qry2.setParameter("status", status);
			qry2.setParameter("productId", productId);
			result = qry2.executeUpdate();

		}

		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		if (result <= 0) {
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
		} else {
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		}
		return map;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> changeShipppingStatus(long shippingId, int status) {
		Map<String, Object> map = null;
		int result = 0;
		try {

			Session session = openDBsession();

			DetachedCriteria detachedCriteria2 = DetachedCriteria.forClass(ShippingDetails.class);
			detachedCriteria2.add(Restrictions.eq("shippingId", shippingId));
			List<Product> dvList = (List<Product>) hibernateTemplate.findByCriteria(detachedCriteria2);
			if (dvList == null || dvList.isEmpty()) {
				map = new HashMap<>();
				map.put(ERROR_CODE, -1);
				map.put(ERROR_MESSAGE, "shippingId " + MESSAGE_NOT_FOUND);
				map.put(ERROR_MESSAGE_ARABIC, "Ã˜Â·Ã™â€žÃ˜Â¨" + MESSAGE_NOT_FOUND_AR);
				return map;
			}
			Query qry2 = session.createQuery("update ShippingDetails set status=:status where shippingId=:shippingId");
			qry2.setParameter("status", status);
			qry2.setParameter("shippingId", shippingId);
			result = qry2.executeUpdate();

		}

		catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		map = new HashMap<>();
		if (result <= 0) {
			map.put(ERROR_CODE, -1);
			map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATE_FAILED_AR);
		} else {
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
		}
		return map;
	}

	/*
	 * @Override public Map<String, Object> saveRequestItem(RequestItems
	 * requestItems) { Map<String, Object> map=null; >>>>>>>
	 * 630176607cf87dd1abc60984c40e23a091569e52 try { Session session =
	 * openDBsession(); Criteria criteria = session.createCriteria(Store.class);
	 * criteria.add(Restrictions.eq("sellerId", sellerId));
	 * criteria.setProjection(Projections.distinct(Projections.id()));
	 * criteria.setMaxResults(rowPerPage); criteria.setFirstResult(firstIndex);
	 * List<Long> ids = criteria.list(); session.flush();
	 * 
	 * if (ids != null && !ids.isEmpty()) { criteria =
	 * session.createCriteria(Store.class); criteria.add(Restrictions.in("id",
	 * ids)); criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY); data =
	 * criteria.list();
	 * 
	 * session.flush(); } session.flush(); Query totalQry =
	 * session.createQuery("select count(*) from Store"); totalCounts = (long)
	 * totalQry.uniqueResult(); session.flush(); // closeDBSession(); } catch
	 * (HibernateException e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } logger.info(e.getMessage()); map = new HashMap<>();
	 * map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE, e.getMessage()); throw e; }
	 * if (data != null && !data.isEmpty()) { map = new HashMap<>();
	 * map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR); map.put("ShopList", data);
	 * map.put("TotalCounts", totalCounts); return map; } else { map = new
	 * HashMap<>(); map.put(ERROR_CODE, -3); map.put(ERROR_MESSAGE,
	 * MESSAGE_NO_DATA_TO_SHOW); map.put(ERROR_MESSAGE_ARABIC,
	 * MESSAGE_NO_DATA_TO_SHOW_AR); return map; } }
	 * 
	 * /*
	 * 
	 * @Override public Map<String, Object> saveRequestItem(RequestItems
	 * requestItems) { Map<String, Object> map=null; try { Session
	 * session=openDBsession(); session.save(requestItems); session.flush(); } catch
	 * (HibernateException e) { e.printStackTrace(); throw e; } map=new HashMap<>();
	 * map.put(ERROR_CODE, ERROR_CODE_NO_ERROR); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR); return
	 * map; }
	 * 
	 * @Override public Map<String, Object> editRequestItem(long requestItemId, int
	 * status) { Map<String, Object> map=null; int result=0; try { Session
	 * session=openDBsession(); Query qry=session.
	 * createQuery("update RequestItems set status=:stat where requestItemId=:id");
	 * qry.setParameter("stat", status); qry.setParameter("id", requestItemId);
	 * result = qry.executeUpdate(); } catch (Exception e) { e.printStackTrace();
	 * throw e; } map=new HashMap<>(); if(result <= 0) { map.put(ERROR_CODE, -1);
	 * map.put(ERROR_MESSAGE, MESSAGE_UPDATE_FAILED); map.put(ERROR_MESSAGE_ARABIC,
	 * MESSAGE_UPDATE_FAILED_AR); }else { map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
	 * map.put(ERROR_MESSAGE, MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC,
	 * MESSAGE_SUCSESS_AR); } return map; }
	 * 
	 * 
	 * public Map<String,Object> getAllItemRequest(long requestItemId,long userId) {
	 * Map<String, Object> map = null; List<RequestItems> data = null; try { Session
	 * session = openDBsession();
	 * 
	 * Criteria criteria = session.createCriteria(RequestItems.class); if
	 * (requestItemId > 0) { criteria.add(Restrictions.eq("requestItemId",
	 * requestItemId)); }
	 * 
	 * data = criteria.list(); session.flush(); // closeDBSession(); } catch
	 * (HibernateException e) { // closeDBSession(); if (TESTING) {
	 * e.printStackTrace(); } logger.info(e.getMessage()); map = new HashMap<>();
	 * map.put(ERROR_CODE, -2); map.put(ERROR_MESSAGE, e.getMessage()); throw e; }
	 * if (data == null || data.isEmpty()) { map = new HashMap<>();
	 * map.put(ERROR_CODE, -3); map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
	 * map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR); return map; } else
	 * { map = new HashMap<>(); map.put(ERROR_CODE, 0); map.put(ERROR_MESSAGE,
	 * MESSAGE_SUCSESS); map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
	 * map.put("itemList", data);
	 * 
	 * return map; }
	 * 
	 * }
	 */

	@Override
	public Map<String, Object> saveOrUpdateAttribute(Attributes attributes) {
		Map<String, Object> map = new HashMap<>();
		try {
			Session session = openDBsession();
			if (attributes.getAttributeId() > 0) {
				session.update(attributes);
				session.flush();
				map.put(ERROR_MESSAGE, MESSAGE_UPDATED_SUCESSFULLY);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_UPDATED_SUCESSFULLY_AR);
			} else {
				session.save(attributes);
				session.flush();
				map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
				map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			}
			map.put(ERROR_CODE, ERROR_CODE_NO_ERROR);
			return map;
		} catch (HibernateException e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getAllAttriubes(long attributeId, int status) {
		Map<String, Object> map = null;
		List<Attributes> data = null;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(Attributes.class);
			if (attributeId > 0) {
				criteria.add(Restrictions.eq("attributeId", attributeId));
			}
			if (status == 0 || status == 1) {
				criteria.add(Restrictions.eq("status", status));
			}
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			data = criteria.list();
			session.flush();
		} catch (HibernateException e) {
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data == null || data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put(DATA, data);
			return map;
		}
	}

	@Override
	public Map<String, Object> getAllAttributevalues(long attributeId, int status, int currentIndex, int rowPerPage) {
		Map<String, Object> map = null;
		List<ProductAttributes> data = null;
		List<Review> reviewList = null;

		List<ConnectedProducts> connectedList = null;

		long totalCounts = 0;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(ProductAttributes.class);

			boolean flag = false;
			StringBuffer strBfr = new StringBuffer(" select count(*) from ProductAttributes o  ");
			if (attributeId > 0) {
				flag = true;
				strBfr.append(" where o.attributeId=" + attributeId);
				criteria.add(Restrictions.eq("attributeId", attributeId));
			}

			if (status == 1 || status == 0) {
				if (flag == false) {
					strBfr.append(" where o.status=" + status);
				} else {
					strBfr.append(" and o.status=" + status);
				}
				criteria.add(Restrictions.eq("status", status));
			}
			logger.info(strBfr.toString());
			long offer = 0;
			Query qry = session.createQuery(strBfr.toString());
			totalCounts = (long) qry.uniqueResult();
			session.flush();
			logger.info(totalCounts);
			criteria = session.createCriteria(ProductAttributes.class);
			criteria.add(Restrictions.eq("attributeId", attributeId));
			criteria.setProjection(Projections.distinct(Projections.id()));
			if (rowPerPage > 0) {
				criteria.setMaxResults(rowPerPage);
				criteria.setFirstResult(currentIndex);
			}

			List<Long> ids = (List<Long>) criteria.list();
			logger.info(ids + "ids--------------------------------------------------------------------------------");
			session.flush();
			logger.info(ids);
			if (ids != null && !ids.isEmpty()) {
				criteria = session.createCriteria(ProductAttributes.class);
				criteria.add(Restrictions.in("id", ids));
				criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
				data = criteria.list();
				session.flush();
			}

		} catch (HibernateException e) {
			// closeDBSession();
			if (TESTING) {
				e.printStackTrace();
			}
			logger.info(e.getMessage());
			map = new HashMap<>();
			map.put(ERROR_CODE, -2);
			map.put(ERROR_MESSAGE, e.getMessage());
			throw e;
		}
		if (data != null && !data.isEmpty()) {
			map = new HashMap<>();
			map.put(ERROR_CODE, 0);
			map.put(ERROR_MESSAGE, MESSAGE_SUCSESS);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_SUCSESS_AR);
			map.put("ProudctList", data);
			map.put("Reviews", reviewList);
			map.put("ConnectedProducts", connectedList);
			map.put("TotalCounts", totalCounts);
			return map;

		} else {
			map = new HashMap<>();
			map.put(ERROR_CODE, -3);
			map.put(ERROR_MESSAGE, MESSAGE_NO_DATA_TO_SHOW);
			map.put(ERROR_MESSAGE_ARABIC, MESSAGE_NO_DATA_TO_SHOW_AR);
			return map;
		}
	}

	@Override
	public Map<String, Object> updateProduct(double p, long ratingPercentage, long productId) {
		Map<String, Object> newmap = null;
		logger.info("-" + "__________________");
		Session session = openDBsession();
		Query qry2 = session.createQuery(
				"update Product set rating=:p , ratingPercentage=:ratingPercentage  where productId=:productId");
		qry2.setParameter("p", p);
		qry2.setParameter("ratingPercentage", ratingPercentage);
		qry2.setParameter("productId", productId);
		int result = qry2.executeUpdate();
		if (result > 0) {
			newmap = new HashMap<>();
			newmap.put(ERROR_CODE, 0);
			newmap.put(ERROR_MESSAGE, MESSAGE_SUCSESS);

		}
		return newmap;

	}
}
