/**
 * 
 */
//
package com.innoventiaProject.ctrl.filter.corsfilter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/**
 * @author jiji
 * @email jiji@sociohub.in
 */
public class CorsFilter implements Filter {
	
	 @Override
	    public void init(FilterConfig filterConfig) throws ServletException {

	    }


	    @Override
	    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
	        System.out.println("Adding Access Control Response Headers");
	        HttpServletResponse response = (HttpServletResponse) servletResponse;
	        response.setHeader("Access-Control-Allow-Origin", "*");
	        response.setHeader("Access-Control-Allow-Credentials", "true");
	        response.setHeader("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS");
	        response.setHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers,user-Token,Bearer");
	        filterChain.doFilter(servletRequest, servletResponse);
	        
	        
	    }
	    @Override
	    public void destroy() {

	    }

}
