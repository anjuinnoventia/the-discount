package com.innoventiaProject.ctrl.input;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.amazonaws.util.IOUtils;
import com.amazonaws.util.json.JSONException;
import com.amazonaws.util.json.JSONObject;
import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.model.BasicResponse;
import com.innoventiaProject.discount.service.CommonService;
import com.innoventiaProject.discount.service.order.OrderService;
import com.innoventiaProject.discount.service.report.ReportService;
import com.innoventiaProject.entity.order.Transactions;

@Controller
public class GeneralWebCtrl extends CommonService implements Constants{
	private static final Logger logger = Logger.getLogger(GeneralWebCtrl.class);

//	@Autowired
//	private GeneralService generalService;
	
	@Autowired
	ReportService reportService;
	
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView greeting() {
        return new ModelAndView("index");
    }
	
	@RequestMapping(value = "/payment/check", method = RequestMethod.GET)
	   public RedirectView paymentcheck(@RequestParam("id") String Id,@RequestParam("type") long type,@RequestParam("orderId") long orderId,@RequestParam("phoneOrWeb") long phoneOrWeb,@RequestParam("direct") long direct) throws IOException, JSONException{
		BasicResponse res=null;
		BasicResponse res1=null;
		double amount=0;
	        String urlString="https://test.oppwa.com/v1/checkouts/"+Id+"/payment" +
	                "?authentication.userId=8a82941865fc22f80166071f8e710f2b" +
	                "&authentication.password=yrkE5s3FRF" +"&authentication.entityId=8a82941865fc22f80166071ff3fa0f2f";
	            URL url = new URL(urlString);

	            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
	            conn.setRequestMethod("GET");
	            int responseCode = conn.getResponseCode();
	            InputStream is;

	            if (responseCode >= 400) is = conn.getErrorStream();
	            else is = conn.getInputStream();

	            String result=IOUtils.toString(is);
	            logger.info(result);
	            JSONObject jsonObj = new JSONObject(result);
	            JSONObject jsonObj2=(JSONObject) jsonObj.get("result");
	            String sendData;
	logger.info(jsonObj2.get("code")+"error codeeee paymernrt----------------________________");           
	            
	
	
	
	if(jsonObj2.get("code").equals("000.100.110"))
	            {
	            	res = reportService.updateOrder(orderId, Id, PAYMENT_SUCCESS);
	            	
	                sendData="paysuccess";
	                res1= orderService.changeStatusOfTransaction(orderId);  
	                logger.info(res1);
	                
	                
	            }else {
	            	res = reportService.updateOrder(orderId, Id, PAYMENT_FAILED);
	                sendData="payfailed";
	            }
	            RedirectView redirectView = new RedirectView();
	            if(phoneOrWeb==PHONE) 
	            {
	            
	            redirectView.setUrl("/app/payment?status="+sendData);
	            }else if(phoneOrWeb==WEB)
	            {
	            	
	 	            redirectView.setUrl("http://pentalsaudi.com/app/"+ sendData +"?key="+Id+"&type="+type+"&orderId="+orderId+"&direct="+direct);
	            }
	            return redirectView;
	   }
	
	
	
	
	@RequestMapping(value = "/app/payment", method = RequestMethod.GET)
public BasicResponse successPhone(@RequestParam("status") String status,HttpServletRequest req) throws IOException {
		
		
		BasicResponse res=new BasicResponse();
		res.setErrorCode(0);
		res.setErrorMessage(MESSAGE_SUCCESS);
		
		 return res;
	}
	
	
	
	
	
	
	
	
	
	
	/*@RequestMapping(value = GET_VIEW_LOGIN_SCREEN, method = RequestMethod.GET)
    public ModelAndView getLogin() {
		//ModelAndView modelView=;
    //	modelView.addObject("FIRST_URL", INITIAL_URL);
        return new ModelAndView(VIEW_LOGIN_SCREEN);
    }
	
	@RequestMapping(value = SAVE_DEFAULT_ADMIN, method = RequestMethod.GET)
    public ModelAndView addDefaultUser(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException  {
    	logger.info("get here the data");
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	Map<String, Object> map=new HashMap<>();
    	BasicResponse res=generalService.authenticate("admin", "password");
    	if(res.errorCode != ERROR_CODE_NO_ERROR){
    		AddEmployeeVo addEmp=new AddEmployeeVo();
    		addEmp.address="NA";
    		addEmp.email="sa@k.in";
    		addEmp.name="admin";
    		addEmp.password="password";
    		addEmp.phoneNo="NA";
    		addEmp.status=true;
    		addEmp.rollId=50;
    		res=generalService.saveUser(addEmp);
    	}
    	logger.info(res.getErrorCode());
    	resp.sendRedirect(GENERAL_FIRST_URL);
        return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    }
	
	@RequestMapping(value = GET_VIEW_HOME, method = RequestMethod.GET)
    public ModelAndView callhome(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	logger.info("get here the data");
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	Map<String, Object> map=new HashMap<>();
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(GENERAL_FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
   		 return new ModelAndView(VIEW_HOME_SCREEN, map);
    }
	
	 @RequestMapping(value = GET_VIEW_ADD_USER, method = RequestMethod.GET)
	    public ModelAndView addUser(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
	    	Map<String, Object> map=new HashMap<>();
	    	model.addAttribute("FIRST_URL", INITIAL_URL);
	    	if(getLogedUserFromSession(req) == null){
	    		resp.sendRedirect(GENERAL_FIRST_URL);
	    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
	    	}
	    	if(getLogedUserFromSession(req).roleId == ROLL_ID_ADMIN){  
	      		 return new ModelAndView(VIEW_ADD_USER, map);
	      	     }else{
	      	    	resp.sendRedirect(GENERAL_FIRST_URL+"/home");
	      	    	 return new ModelAndView(VIEW_HOME_SCREEN, map);
	      	     }
	    }
	    
	    @RequestMapping(value = GET_VIEW_USERS, method = RequestMethod.GET)
	    public ModelAndView getUserList(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
	    	Map<String, Object> map=new HashMap<>();
	    	model.addAttribute("FIRST_URL", INITIAL_URL);
	    	if(getLogedUserFromSession(req) == null){
	    		resp.sendRedirect(GENERAL_FIRST_URL);
	    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
	    	}
	    	if(getLogedUserFromSession(req).roleId == ROLL_ID_ADMIN){  
	    	return new ModelAndView(VIEW_USERLIST, map);
	    	}else{
	    		resp.sendRedirect(GENERAL_FIRST_URL+"/home");
	    		return new ModelAndView(VIEW_HOME_SCREEN, map);
     	     
	    	}
	    }
	    
	    @RequestMapping(value = GET_VIEW_CUSTOMERS, method = RequestMethod.GET)
	    public ModelAndView getCustomerList(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
	    	Map<String, Object> map=new HashMap<>();
	    	model.addAttribute("FIRST_URL", INITIAL_URL);
	    	if(getLogedUserFromSession(req) == null){
	    		resp.sendRedirect(GENERAL_FIRST_URL);
	    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
	    	}
	    	return new ModelAndView(VIEW_CUSTOMERLIST, map);
	    }
	
	
	@RequestMapping(value = "/kerashop", method = RequestMethod.GET)
	public ModelAndView enterToApplication(@RequestParam("username") String user,HttpServletRequest req,HttpServletResponse resp) {
		//BasicResponse res = generalService.authenticate(addUser.customerName, addUser.phoneNumber);
	//	logger.info(res.errorCode);

		if (!user.equals("")) {

			LoggedSession session = new LoggedSession();
			session.userName=user;
			initSession(req, session);
			return new ModelAndView("index");
		} else {
			return new ModelAndView("errorpage");

		}

	}
	*/
	
	
	
}
