package com.innoventiaProject.ctrl.input;
/*package com.innoventiaProject.ctrl.input;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.innoventiaProject.discount.constants.Constants;
import com.innoventiaProject.discount.service.CommonService;

@Controller
@RequestMapping("/webinput")
public class WebViewInputCtrl extends CommonService implements Constants{
	
	
    @RequestMapping(value = GET_VIEW_ADD_CATEGORY, method = RequestMethod.GET)
    public ModelAndView addCategory(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	Map<String, Object> map=new HashMap<>();
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
    	return new ModelAndView(VIEW_ADD_CATEGORY, map);
    }
    
    @RequestMapping(value = GET_VIEW_ADD_SERVICE, method = RequestMethod.GET)
    public ModelAndView addService(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	Map<String, Object> map=new HashMap<>();
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
    	return new ModelAndView(VIEW_ADD_SERVICE, map);
    }
    
    @RequestMapping(value = GET_VIEW_CATEGORIES, method = RequestMethod.GET)
    public ModelAndView getcategory(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	Map<String, Object> map=new HashMap<>();
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
    	return new ModelAndView(VIEW_CATEGORIES, map);
    }
    
    
    
    @RequestMapping(value = GET_VIEW_CUSTOMERS, method = RequestMethod.GET)
    public ModelAndView getCustomers(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	Map<String, Object> map=new HashMap<>();
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
    	return new ModelAndView(VIEW_CUSTOMERLIST, map);
    }
    
    
    @RequestMapping(value =GET_VIEW_SERVICES, method = RequestMethod.GET)
    public ModelAndView getService(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	Map<String, Object> map=new HashMap<>();
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
    	return new ModelAndView(VIEW_SERVICES, map);
    }
    
    
    @RequestMapping(value =GET_VIEW_BOOKING_DETAILS, method = RequestMethod.GET)
    public ModelAndView getBookingDetails(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	Map<String, Object> map=new HashMap<>();
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
    	return new ModelAndView(VIEW_BOOKING_DETAILS, map);
    }
    
    @RequestMapping(value =GET_VIEW_SUBSCRIPTIONS_DETAILS, method = RequestMethod.GET)
    public ModelAndView getSubscriptionDetails(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException{
    	Map<String, Object> map=new HashMap<>();
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
    	return new ModelAndView(VIEW_SUBSCRIPTION, map);
    }
    
    
    @RequestMapping(value =GET_VIEW_PERIOD_BOOKING, method = RequestMethod.GET)
    public ModelAndView getMonthlyBookingDetails(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	Map<String, Object> map=new HashMap<>();
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
    	return new ModelAndView(VIEW_BOOKING_REPORT, map);
    }
    
    
    
    @RequestMapping(value = GET_VIEW_BILL, method = RequestMethod.GET)
    public ModelAndView getBillView(HttpServletRequest req, HttpServletResponse resp,ModelMap model) throws ServletException, IOException {
    	Map<String, Object> map=new HashMap<>();
    	model.addAttribute("FIRST_URL", INITIAL_URL);
    	if(getLogedUserFromSession(req) == null){
    		resp.sendRedirect(FIRST_URL);
    		return new ModelAndView(VIEW_LOGIN_SCREEN, map);
    	}
    	return new ModelAndView(VIEW_BILL_VIEW, map);
    }
    
    
}*/