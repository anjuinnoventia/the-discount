package com.innoventiaProject.entity.account;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "INCOMES",uniqueConstraints ={@UniqueConstraint(columnNames ="INCOME_ID")})
public class Income implements Serializable{
	
	private static final long serialVersionUID = 2694261013002519012L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "INCOME_ID", unique = true, nullable = false)
	private long incomeId;

	@Column(name = "BUSINESS_TYPE", nullable = false)
	private int business_type;
	
	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long last_updated_time;
	
	
	@Column(name = "ISPAYABLE", nullable = false)
	private int ispayable;
	
	@Column(name ="CMD",unique =false,nullable = true)
	private long cmd;
	
	@Column(name = "STATUS", nullable = false)
	private int status;
	
	@Column(name = "OPRATING_OFFICERID", unique = false, nullable = true)
	private long operatingOfficer;
	
	@Column(name = "AMOUNT", unique = false, nullable = false)
	private double amount;
	
	@Column(name = "TRANSACTION_ID", unique = false, nullable = false)
	private long transactionId;
	
	@Column(name = "INCOM_EXPNSE_ID", unique = false, nullable = false)
	private long incomExpnsHeadId;
	
	@Column(name = "ENTERED_DATE", unique = false, nullable = false)
	private long entered_date;
	
	@Column(name = "BILL_NO", unique = false, nullable = false)
	private long bill_no;
	
	@Column(name = "DATE_INCOME", unique = false, nullable = false)
	private long date_income;
	
	@Column(name = "TRAN_STATUS",unique = false,nullable = true)
	private int tran_status;
	
	@Column(name = "USER_ID", unique = false, nullable = false)
	private long userId;
	
	

	public long getIncomeId() {
		return incomeId;
	}

	public void setIncomeId(long incomeId) {
		this.incomeId = incomeId;
	}

	public int getBusiness_type() {
		return business_type;
	}

	public void setBusiness_type(int business_type) {
		this.business_type = business_type;
	}

	public long getLast_updated_time() {
		return last_updated_time;
	}

	public void setLast_updated_time(long last_updated_time) {
		this.last_updated_time = last_updated_time;
	}

	public int getIspayable() {
		return ispayable;
	}

	public void setIspayable(int ispayable) {
		this.ispayable = ispayable;
	}

	public long getCmd() {
		return cmd;
	}

	public void setCmd(long cmd) {
		this.cmd = cmd;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getOperatingOfficer() {
		return operatingOfficer;
	}

	public void setOperatingOfficer(long operatingOfficer) {
		this.operatingOfficer = operatingOfficer;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public long getIncomExpnsHeadId() {
		return incomExpnsHeadId;
	}

	public void setIncomExpnsHeadId(long incomExpnsHeadId) {
		this.incomExpnsHeadId = incomExpnsHeadId;
	}

	public long getEntered_date() {
		return entered_date;
	}

	public void setEntered_date(long entered_date) {
		this.entered_date = entered_date;
	}

	public long getBill_no() {
		return bill_no;
	}

	public void setBill_no(long bill_no) {
		this.bill_no = bill_no;
	}

	public long getDate_income() {
		return date_income;
	}

	public void setDate_income(long date_income) {
		this.date_income = date_income;
	}

	public int getTran_status() {
		return tran_status;
	}

	public void setTran_status(int tran_status) {
		this.tran_status = tran_status;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	
	

}
