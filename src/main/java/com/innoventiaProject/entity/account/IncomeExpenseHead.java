package com.innoventiaProject.entity.account;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "INCOME_EXPENSE_HEAD",uniqueConstraints ={@UniqueConstraint(columnNames ="INCOM_EXPNSE_ID")})
public class IncomeExpenseHead implements Serializable{

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "INCOM_EXPNSE_ID", unique = true, nullable = false)
	private long incomExpnsHeadId;

	@Column(name = "INCOME_NAME", unique = false, nullable = false)
	private String incomeExpenseName;
	
	@Column(name = "BUSINESS_TYPE", nullable = false)
	private int business_type;
	
	@Column(name = "LAST_UPDATED_TIME", nullable = false)
	private long last_updated_time;
	
	@Column(name = "OPRATING_OFFICERID", unique = false, nullable = true)
	private long operatingOfficer;
	
	@Column(name = "YEAR", unique = false, nullable = true)
	private String year;
	
	@Column(name = "INCOME_OR_EXPENSE", nullable = false)
	private int incomeOrExpense;
	
	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;
	
	

	public long getIncomExpnsHeadId() {
		return incomExpnsHeadId;
	}

	public void setIncomExpnsHeadId(long incomExpnsHeadId) {
		this.incomExpnsHeadId = incomExpnsHeadId;
	}


	public String getIncomeExpenseName() {
		return incomeExpenseName;
	}

	public void setIncomeExpenseName(String incomeExpenseName) {
		this.incomeExpenseName = incomeExpenseName;
	}

	public int getBusiness_type() {
		return business_type;
	}

	public void setBusiness_type(int business_type) {
		this.business_type = business_type;
	}

	public long getLast_updated_time() {
		return last_updated_time;
	}

	public void setLast_updated_time(long last_updated_time) {
		this.last_updated_time = last_updated_time;
	}

	public long getOperatingOfficer() {
		return operatingOfficer;
	}

	public void setOperatingOfficer(long operatingOfficer) {
		this.operatingOfficer = operatingOfficer;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public int getIncomeOrExpense() {
		return incomeOrExpense;
	}

	public void setIncomeOrExpense(int incomeOrExpense) {
		this.incomeOrExpense = incomeOrExpense;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	

}
