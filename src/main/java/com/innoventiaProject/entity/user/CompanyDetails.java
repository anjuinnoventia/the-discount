package com.innoventiaProject.entity.user;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.innoventiaProject.entity.input.SubCategory;
import com.innoventiaProject.entity.order.ShippingDetails;

@Entity
@Table(name = "COMPANY_DETAILS",uniqueConstraints ={@UniqueConstraint(columnNames = "COMPANY_ID")})
public class CompanyDetails implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "COMPANY_ID", unique = true, nullable = false)
	private long companyId;

	@Column(name = "COMPANY_NAME", unique = false, nullable = false)
	private String companyName;
	
	@Column(name = "COMPANY_ADDRESS", unique = false, nullable = false)
	private String companyAddress;
	
	@Column(name = "CITY_ID", unique = false, nullable = false)
	private int cityId;
	
	@Column(name = "COUNTRY_ID", unique = false, nullable = false)
	private int countryId;
	
	@Column(name = "USER_ID", unique = false, nullable = false)
	private long userId;
	
	@Column(name = "PINCODE", unique = false, nullable = false)
	private String pincode;
	
	@Column(name = "FIRST_NAME", unique = false, nullable = false)
	private String firstName;
	
	@Column(name = "LAST_NAME", unique = false, nullable = false)
	private String lastName;
	
	@Column(name = "PHONE_NUMBER", unique = false, nullable = false)
	private String phone;
	
	@Column(name = "MOBILE", unique = false, nullable = false)
	private String mobile;
	
	@Column(name = "EMAIL", unique = false, nullable = false)
	private String email;
	
	@Column(name = "WEBSITE", unique = false, nullable = false)
	private String website;
	
	@Column(name = "GSTIN", unique = false, nullable = false)
	private String gstin;
	
	@Column(name = "PAN", unique = false, nullable = false)
	private String pan;

	@Column(name = "COMPANY_TYPE", unique = false, nullable = false)
	private String companyType;
	
	@Column(name = "YEAR", unique = false, nullable =true )
	private String year;
	
	@Column(name = "LOGO_URL", unique = false, nullable = true)
	private String logo;

	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;

	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getCityId() {
		return cityId;
	}

	public void setCityId(int cityId) {
		this.cityId = cityId;
	}

	public int getCountryId() {
		return countryId;
	}

	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getGstin() {
		return gstin;
	}

	public void setGstin(String gstin) {
		this.gstin = gstin;
	}

	public String getPan() {
		return pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getCompanyType() {
		return companyType;
	}

	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

		
	
	
	
	
	
	
	
}
