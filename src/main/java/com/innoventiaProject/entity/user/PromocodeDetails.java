package com.innoventiaProject.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "PROMOCODE_DETAILS",uniqueConstraints ={@UniqueConstraint(columnNames = "CODE_ID")})
public class PromocodeDetails implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CODE_ID", unique = true, nullable = false)
	private long codeId;

	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	
	@Column(name = "PROMOCODE", unique = false, nullable = false)
	private String promocode;
	

	@Column(name = "MINIMUM_AMOUNT", unique = false, nullable = true)
	private double minimumAmount;
	

	@Column(name = "FLAT_OFFER", unique = false, nullable = false)
	private double flatOffer;
	

	@Column(name = "PERCENTAGE", unique = false, nullable = false)
	private long percentage;


	@Column(name = "VALID_FROM", unique = false, nullable = true)
    private long validFrom;
	
	@Column(name = "VALID_TO", unique = false, nullable = true)
	private long validTo;
	
	
	@Column(name ="STATUS",unique =false,nullable =true)
	private int status;
	
	
	public double getMinimumAmount() {
		return minimumAmount;
	}


	public void setMinimumAmount(double minimumAmount) {
		this.minimumAmount = minimumAmount;
	}


	public long getValidFrom() {
		return validFrom;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public void setValidFrom(long validFrom) {
		this.validFrom = validFrom;
	}


	public long getValidTo() {
		return validTo;
	}


	public void setValidTo(long validTo) {
		this.validTo = validTo;
	}


	public long getCodeId() {
		return codeId;
	}


	public void setCodeId(long codeId) {
		this.codeId = codeId;
	}


	public long getUserId() {
		return userId;
	}


	public void setUserId(long userId) {
		this.userId = userId;
	}


	public String getPromocode() {
		return promocode;
	}


	public void setPromocode(String promocode) {
		this.promocode = promocode;
	}


	public double getFlatOffer() {
		return flatOffer;
	}


	public void setFlatOffer(double flatOffer) {
		this.flatOffer = flatOffer;
	}


	public long getPercentage() {
		return percentage;
	}


	public void setPercentage(long percentage) {
		this.percentage = percentage;
	}
	
	
	
	
	
}
