package com.innoventiaProject.entity.user;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "DELIVERY_BOY",uniqueConstraints ={@UniqueConstraint(columnNames = "DELIVERY_BOY_ID")})
public class DeliveryBoy implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DELIVERY_BOY_ID",unique = true,nullable = false)
	private long deliveryBoyId;
	
	@Column(name ="DELIVERY_BOY_NAME",unique =false,nullable =false)
	private String deliveryBoyName;
	
	@Column(name ="ADDRESS",unique =false,nullable =false)
	private String address;
	
	@Column(name ="PHONE_NUMBER",unique =false,nullable =false)
	private String phoneNumber;
	
	@Column(name = "LAST_UPDATED_TIME",unique = false,nullable = false)
	private long lastUpdatedTime;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name = "PROFILE_IMAGE",unique = false,nullable = false)
	private String profileImage;
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	@Column(name = "EMAIL_ID",unique = false,nullable = false)
	private String emailId;
	
	@Column(name = "LATITUDE", unique = false, nullable = false)
	private double latitude;

	@Column(name = "LONGITUDE", unique = false, nullable = false)
	private double longitude;
	

	@Column(name = "IS_AVAILABLE", unique = false, nullable = false)
	private int isAvailable;
	

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getDeliveryBoyId() {
		return deliveryBoyId;
	}

	public void setDeliveryBoyId(long deliveryBoyId) {
		this.deliveryBoyId = deliveryBoyId;
	}

	public String getDeliveryBoyName() {
		return deliveryBoyName;
	}

	public void setDeliveryBoyName(String deliveryBoyName) {
		this.deliveryBoyName = deliveryBoyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(int isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	
	 
	
	
}
