package com.innoventiaProject.entity.user;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.innoventiaProject.entity.input.SubCategory;
import com.innoventiaProject.entity.order.CardDetails;
import com.innoventiaProject.entity.order.ShippingDetails;

@Entity
@Table(name = "CS_CUSTOMER_DETAILS",uniqueConstraints ={@UniqueConstraint(columnNames = "CUSTOMER_ID")})
public class CustomerDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CUSTOMER_ID",unique = true,nullable = false)
	private long customerId;
	
	@Column(name = "CREATED_DATE",unique = false,nullable = false)
	private long createdDate;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name ="IS_SPECIAL",unique =false,nullable =true)
	private int isSpecial;
	
	@Column(name = "CUSTOMER_NAME",unique = false,nullable = false)
	private String customerName;
	
	@Column(name = "CUSTOMER_NAME_Ar",unique = false,nullable = true)
	private String customerNameAr;
	
	@Column(name = "ADDRESS",unique = false,nullable = true)
	private String address;
	
	@Column(name = "ADDRESS_AR",unique = false,nullable = true)
	private String addressAr;
	
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
    @Column(name = "PHONE_NO",unique = false,nullable = false)
	private String phoneNo;
	
	@Column(name = "DEVICE_ID",unique = false,nullable = false)
	private String deviceId;
	
	@Column(name = "EMAIL_ID",unique = false,nullable = false)
	private String emailId;
	
	@Column(name ="LATITUDE",unique =false,nullable =false)
	private double latitude;
	
	@Column(name ="LONGITUDE",unique =false,nullable =false)
	private double longitude;
	
	@Column(name = "PROFILE_IMAGE",unique = false,nullable = false)
	private String profileImage;
	
	
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	@ColumnDefault("'00'")
	@Column(name ="ZIP_CODE",unique =false,nullable =true)
	private String zipCode;
	

	@ColumnDefault("'00'")
	@Column(name ="VALIDATE",unique =false,nullable =false)
	private int validate;
	
	
	

	@Column(name = "COUNTRY_CODE",unique = false,nullable = true)
	private int countryCode;

	public int getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(int countryCode) {
		this.countryCode = countryCode;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy="customerDetails" ,cascade = CascadeType.ALL)
	private Set<ShippingDetails> shippingDetails;

	@OneToMany(fetch = FetchType.EAGER, mappedBy="customerDetails" ,cascade = CascadeType.ALL)
	private Set<CardDetails> cardDetails;
	
	
	
	
	
	
	
	
	/*
	public int getIsSpecial() {
		return isSpecial;
	}

	public void setIsSpecial(int isSpecial) {
		this.isSpecial = isSpecial;
	}
*/
	public int getValidate() {
		return validate;
	}

	public void setValidate(int validate) {
		this.validate = validate;
	}

	public Set<CardDetails> getCardDetails() {
		return cardDetails;
	}

	public void setCardDetails(Set<CardDetails> cardDetails) {
		this.cardDetails = cardDetails;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddressAr() {
		return addressAr;
	}

	public void setAddressAr(String addressAr) {
		this.addressAr = addressAr;
	}

	public String getCustomerNameAr() {
		return customerNameAr;
	}

	public void setCustomerNameAr(String customerNameAr) {
		this.customerNameAr = customerNameAr;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public Set<ShippingDetails> getShippingDetails() {
		return shippingDetails;
	}

	public void setShippingDetails(Set<ShippingDetails> shippingDetails) {
		this.shippingDetails = shippingDetails;
	}

	public String getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
	
	
}
