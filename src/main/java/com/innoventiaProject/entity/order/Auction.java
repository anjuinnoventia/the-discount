package com.innoventiaProject.entity.order;
/*package com.innoventiaProject.entity.order;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.innoventiaProject.entity.input.FeaturesAndPhotosProduct;
@Entity
@Table(name = "AUCTION",uniqueConstraints ={@UniqueConstraint(columnNames = "AUCTION_ID")})
public class Auction implements Serializable 
{
	*//**
	 * 
	 *//*
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AUCTION_ID",unique = true,nullable = false)
	private long auctionId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="OPERATING_OFFICER_ID",unique =false,nullable =false)
	private long operatingOfficerId;
	
	@Column(name ="AUCTION_STATUS",unique =false,nullable =false)
	private int auctionStatus;
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;

	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name = "ITEM_NAME",unique = false,nullable = false)
	private String itemName;
	
	@Column(name = "ITEM_NETPRICE",unique = false,nullable = false)
	private double itemNetPrice;
	
	@Column(name = "BASIC_BID_AMOUNT",unique = false,nullable = false)
	private double basicBidAmt;
	
	@Column(name = "MAX_BID_AMT",unique = false,nullable = false)
	private double maxBidAmt;
	
	@Column(name ="AUCTION_DATE",unique =false,nullable =false)
	private long auctionDate;
	
	@Column(name ="AUCTION_TIME",unique =false,nullable =false)
	private long auctionTime;
	
	@Column(name ="NO_OF_BIDS",unique =false,nullable =false)
	private int noOfBids;
	
	@Column(name ="AUCTION_EXPIRY_TIME",unique =false,nullable =false)
	private long auctionExpiryTime;
	
	@Column(name ="LAST_BID_AMOUNT",unique =false,nullable =false)
	private double lastBidAmt;
	
	@Column(name ="COUNTRY_ID",unique =false,nullable =true)
	private long countryId;
	
	@Column(name ="CITY_ID",unique =false,nullable =true)
	private long cityId;
	
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="auction" ,cascade =   CascadeType.ALL)
	private Set<AuctionDetails> auctionDetails;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="auction" ,cascade = CascadeType.ALL)
	private Set<FeaturesAndPhotosAuction> featureAndPhotos;
	

	public long getUserId() {
		return userId;
	}



	public void setUserId(long userId) {
		this.userId = userId;
	}



	public int getNoOfBids() {
		return noOfBids;
	}
	
	

	public long getCountryId() {
		return countryId;
	}



	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}



	public long getCityId() {
		return cityId;
	}



	public void setCityId(long cityId) {
		this.cityId = cityId;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}



	public Set<AuctionDetails> getAuctionDetails() {
		return auctionDetails;
	}



	public void setAuctionDetails(Set<AuctionDetails> auctionDetails) {
		this.auctionDetails = auctionDetails;
	}



	public void setNoOfBids(int noOfBids) {
		this.noOfBids = noOfBids;
	}

	

	public double getLastBidAmt() {
		return lastBidAmt;
	}

	public void setLastBidAmt(double lastBidAmt) {
		this.lastBidAmt = lastBidAmt;
	}

	public double getBasicBidAmt() {
		return basicBidAmt;
	}

	public void setBasicBidAmt(double basicBidAmt) {
		this.basicBidAmt = basicBidAmt;
	}

	public double getMaxBidAmt() {
		return maxBidAmt;
	}

	public void setMaxBidAmt(double maxBidAmt) {
		this.maxBidAmt = maxBidAmt;
	}

	public long getAuctionId() {
		return auctionId;
	}

	public void setAuctionId(long auctionId) {
		this.auctionId = auctionId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}


	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}


	public long getAuctionTime() {
		return auctionTime;
	}

	public void setAuctionTime(long auctionTime) {
		this.auctionTime = auctionTime;
	}

	public long getAuctionExpiryTime() {
		return auctionExpiryTime;
	}

	public void setAuctionExpiryTime(long auctionExpiryTime) {
		this.auctionExpiryTime = auctionExpiryTime;
	}

	public int getAuctionStatus() {
		return auctionStatus;
	}

	public void setAuctionStatus(int auctionStatus) {
		this.auctionStatus = auctionStatus;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getItemNetPrice() {
		return itemNetPrice;
	}

	public void setItemNetPrice(double itemNetPrice) {
		this.itemNetPrice = itemNetPrice;
	}

	public long getAuctionDate() {
		return auctionDate;
	}

	public void setAuctionDate(long auctionDate) {
		this.auctionDate = auctionDate;
	}
	
	
	
	
	
	
}
*/