package com.innoventiaProject.entity.order;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "TAX_TABLE", uniqueConstraints = { @UniqueConstraint(columnNames = "TAX_ID") })
public class TaxManagement implements Serializable 
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TAX_ID", unique = true, nullable = false)
	private long taxId;
	
	@Column(name = "TAX_NAME", unique = false, nullable = false)
	private String taxName;
	
	@Column(name = "TAX_NAME_AR", unique = false, nullable = true)
	private String taxNameAr;
	
	
	@Column(name = "TAX_PERCENTAGE", unique = false, nullable = false)
	private Double taxPercentage;
	
	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;

	@Column(name = "OPERATING_OFFICER_ID", unique = false, nullable = false)
	private long operatingOfficerId;
	
	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;

	@Column(name ="COUNTRY_ID",unique =false,nullable =true)
	private long countryId;
	
	@Column(name ="CITY_ID",unique =false,nullable =true)
	private long cityId;
	
	
	

	

	public String getTaxNameAr() {
		return taxNameAr;
	}

	public void setTaxNameAr(String taxNameAr) {
		this.taxNameAr = taxNameAr;
	}

	public Double getTaxPercentage() {
		return taxPercentage;
	}

	public void setTaxPercentage(Double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}

	public long getTaxId() {
		return taxId;
	}

	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
	
	
	
	
	
}
