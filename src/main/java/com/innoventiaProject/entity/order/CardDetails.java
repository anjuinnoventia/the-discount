package com.innoventiaProject.entity.order;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.innoventiaProject.entity.user.CustomerDetails;

@Entity
@Table(name = "CARD_TABLE", uniqueConstraints = { @UniqueConstraint(columnNames = "CARD_ID") })
public class CardDetails implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CARD_ID", unique = true, nullable = false)
	private long cardId;
	
	@Column(name = "CARD_NAME", unique = false, nullable = false)
	private String cardName;
	
	@Column(name = "CARD_NUMBER", unique = false, nullable = false)
	private String cardNumber;
	
	@Column(name = "BANK_NAME", unique = false, nullable = false)
	private String bankName;
	
	@Column(name = "CUSTOMER_ID", unique = false, nullable = false)
	private long customerId;

	
	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;
	
	@Column(name = "EXPIRY_DATE", unique = false, nullable = false)
	private long expiryDate;
	
	
	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "CUSTOMER_ID", nullable = true, insertable = false, updatable = false)
	private CustomerDetails customerDetails;
	
	
	
	
	

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(long customerId) {
		this.customerId = customerId;
	}

	public CustomerDetails getCustomerDetails() {
		return customerDetails;
	}

	public void setCustomerDetails(CustomerDetails customerDetails) {
		this.customerDetails = customerDetails;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(long expiryDate) {
		this.expiryDate = expiryDate;
	}
	



}
