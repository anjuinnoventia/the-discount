package com.innoventiaProject.entity.order;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

import com.innoventiaProject.entity.input.Product;
import com.innoventiaProject.entity.order.Order;
@Entity
@Table(name = "ORDER_DETAILS",uniqueConstraints ={@UniqueConstraint(columnNames = "ORDER_LIST_ID")})
public class OrderDetails implements Serializable
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="ORDER_LIST_ID",unique =true,nullable =false)
	private long orderListId;
	
	@Column(name ="ORDER_ID",unique =false,nullable =false)
	private long orderId;
		
	@Column(name = "ITEM_QUANTITY",unique = false,nullable = false)
	private long itemQuantity;
	
	@Column(name = "ITEM_NAME",unique = false,nullable = false)
	private String itemName;
	
	@Column(name = "ITEM_NAME_AR",unique = false,nullable = true)
	private String itemNameAr;
	
	
	@Column(name = "ITEM_PRICE",unique = false,nullable = false)
	private double itemPrice;
	
	@Column(name = "ITEM_PURCHASE_PRICE",unique = false,nullable = 	true)
	private double itemPurchasePrice;
	
	@Column(name ="PRODUCT_ID",unique =false,nullable =false)
	private long productId;
	
	@Column(name ="TAX_ID",unique =false,nullable =true)
	private long taxId;
	
	@Column(name = "TOTAL_AMOUNT_INCLUDE_OFFER", unique = false, nullable = true)
	private double totalAmountIncludingOffer;

	@Column(name ="OFFER_ID",unique =false,nullable =false)
	private long offerId;
	
	@Column(name = "ITEM_NETPRICE",unique = false,nullable = false)
	private double itemNetPrice;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="OPERATING_OFFICER_ID",unique =false,nullable =false)
	private long operatingOfficerId;
	
	@Column(name ="DESCRIPTION",unique =false,nullable =false)
	private String description;
	
	@Column(name ="RETURN_TIME",unique =false,nullable =false)
	private long returnTime;
	
	@Column(name ="IMAGE_NAME",unique =false,nullable =true)
	private String imageName;
	
	
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
     
	@Column(name = "DELIVERY_DATE",unique = false,nullable = false)
	private long deliveryDate;
	
	@Column(name = "CANCELLATION_DATE", unique = false, nullable = true)
	private long cancellationDate;
	
	@Column(name = "TAX", unique = false, nullable = false)
	private double tax;
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="ORDER_ID", nullable = true, insertable = false, updatable = false)
	private Order order;

	@ManyToOne( fetch = FetchType.EAGER)  
	@JoinColumn(name="PRODUCT_ID", nullable = true, insertable = false, updatable = false)
	private Product product;
	
	@Column(name = "SHIPPING_CHARGE", unique = false, nullable = true)
	private double shippingCharge;
	
	@ColumnDefault("'00'")
	@Column(name = "INSTALLATION_CHARGE", unique = false, nullable = true)
	private double installationCharge;
	
	
	
	

	public double getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(double installationCharge) {
		this.installationCharge = installationCharge;
	}

	public long getCancellationDate() {
		return cancellationDate;
	}

	public void setCancellationDate(long cancellationDate) {
		this.cancellationDate = cancellationDate;
	}

	public String getItemNameAr() {
		return itemNameAr;
	}

	public void setItemNameAr(String itemNameAr) {
		this.itemNameAr = itemNameAr;
	}

	public double getItemPurchasePrice() {
		return itemPurchasePrice;
	}

	public void setItemPurchasePrice(double itemPurchasePrice) {
		this.itemPurchasePrice = itemPurchasePrice;
	}

	public double getShippingCharge() {
		return shippingCharge;
	}

	public void setShippingCharge(double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}

	public long getOrderListId() {
		return orderListId;
	}

	public void setOrderListId(long orderListId) {
		this.orderListId = orderListId;
	}

	public long getOrderId() {
		return orderId;
	}

	public void setOrderId(long orderId) {
		this.orderId = orderId;
	}

	public long getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(long itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public double getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(double itemPrice) {
		this.itemPrice = itemPrice;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getTaxId() {
		return taxId;
	}

	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}

	public double getTotalAmountIncludingOffer() {
		return totalAmountIncludingOffer;
	}

	public void setTotalAmountIncludingOffer(double totalAmountIncludingOffer) {
		this.totalAmountIncludingOffer = totalAmountIncludingOffer;
	}

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	public double getItemNetPrice() {
		return itemNetPrice;
	}

	public void setItemNetPrice(double itemNetPrice) {
		this.itemNetPrice = itemNetPrice;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getReturnTime() {
		return returnTime;
	}

	public void setReturnTime(long returnTime) {
		this.returnTime = returnTime;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(long deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	

	
	
	
	
	
	
}

