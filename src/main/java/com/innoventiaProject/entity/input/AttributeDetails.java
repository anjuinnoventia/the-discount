package com.innoventiaProject.entity.input;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "ATTRIBUTE_DETAILS",uniqueConstraints ={@UniqueConstraint(columnNames = "ATTRIBUTE_DETAIL_ID")})
public class AttributeDetails 
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ATTRIBUTE_DETAIL_ID",unique = true,nullable = false)
	private long attributeDetailsId;	
	
	
	@Column(name = "VALUE",unique = false,nullable = false)
	private String value;
	
	@Column(name = "VALUE_AR",unique = false,nullable = true)
	private String valueAr;
	
	@Column(name = "ATTRIBUTE_ID",unique = false,nullable = true)
	private long attributeId;
	
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="ATTRIBUTE_ID", nullable = true, insertable = false, updatable = false)
	private Attributes attributes;
	
	
	

	public Attributes getAttributes() {
		return attributes;
	}

	public void setAttributes(Attributes attributes) {
		this.attributes = attributes;
	}


	public long getAttributeDetailsId() {
		return attributeDetailsId;
	}

	public void setAttributeDetailsId(long attributeDetailsId) {
		this.attributeDetailsId = attributeDetailsId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getValueAr() {
		return valueAr;
	}

	public void setValueAr(String valueAr) {
		this.valueAr = valueAr;
	}

	public long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}

	

	
	
	
	
	
	
	
	
	
	
	
}
