package com.innoventiaProject.entity.input;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;





@Entity
@Table(name = "SELLERDETAILS",uniqueConstraints ={@UniqueConstraint(columnNames = "SELLER_ID")})
public class SellerDetails implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SELLER_ID",unique = true,nullable = false)
	private long sellerId;
	
	@Column(name ="SELLER_NAME",unique =false,nullable =false)
	private String sellerName;
	
	@Column(name ="SELLER_NAME_AR",unique =false,nullable =true)
	private String sellerNameAr;
	
	@Column(name ="OPERATING_OFFICER_ID",unique =false,nullable =false)
	private long operatingOfficerId;
	
	@Column(name = "PROFILE_IMAGE",unique = false,nullable = false)
	private String profileImage;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name = "CREATED_DATE",unique = false,nullable = false)
	private long createdDate;
	
	@Column(name = "LAST_UPDATED_TIME",unique = false,nullable = false)
	private long lastUpdatedTime;
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	@Column(name ="PHONE_NUMBER",unique =false,nullable =false)
	private String phoneNumber;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name ="DESCRIPTION",unique =false,nullable =false)
	private String description;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name ="DESCRIPTION_AR",unique =false,nullable =false)
	private String descriptionAr;
	
	
	
	
	@Column(name ="EMAIL_ID",unique =false,nullable =false)
	private String emailId;
	
	@Column(name ="WEB_ADDRESS",unique =false,nullable =false)
	private String webAddress;
	
	@Column(name = "ADDRESS",unique = false,nullable = false)
	private String address;
	

	@Column(name = "ADDRESS_AR",unique = false,nullable = true)
	private String addressAr;
	
	@Column(name = "MAINCATEGORY_ID",unique = false,nullable = false)
	private long mainCategoryId;
	
	@Column(name ="RATING",unique =false,nullable =false)
	private double rating;
	
	@Column(name ="RANK",unique =false,nullable =false)
	private int  rank;
	
	@Column(name ="SHARES",unique =false,nullable =false)
	private long shares;
	
	@Column(name ="PRICE_STARTS",unique =false,nullable =false)
	private double priceStarts;

	@Column(name ="WORKING_HOURS",unique =false,nullable =false)
	private String workingHours;
	
	@Column(name ="DELIVARY_STATUS",unique =false,nullable =false)
	private int delivaryStatus;
	
	@Column(name ="LATITUDE",unique =false,nullable =false)
	private double latitude;
	
	@Column(name ="LONGITUDE",unique =false,nullable =false)
	private double longitude;
	
	@Column(name ="NO_OF_REVIEWS",unique =false,nullable =false)
	private long noOfReviews;
	
	@Column(name ="SERVICE_OR_PRODUCT",unique =false,nullable =false)
	private int  serviceOrProduct;

	@Column(name ="IS_COD_PAYMENT",unique =false,nullable =false)
	private int  isCod;
	
	@Column(name ="IS_ONLINE_PAYMENT",unique =false,nullable =false)
	private int  isOnline;
    
	
	@Column(name ="COUNTRY_ID",unique =false,nullable =false)
	private long countryId;
	
	@Column(name ="CITY_ID",unique =false,nullable =false)
	private long cityId;
	
	@Column(name ="CONTACT_NAME",unique =false,nullable =true)
	private String contactName;
	
	@Column(name ="TELEPHONE_NO",unique =false,nullable =true)
	private String telephoneNo;
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	
	
	
	
	
	
	
	/*@OneToMany(fetch = FetchType.EAGER, mappedBy="seller" ,cascade = CascadeType.ALL)
	private Set<Product> products;
	*/



/*	public Set<Product> getProducts() {
		return products;
	}



	public void setProducts(Set<Product> products) {
		this.products = products;
	}*/





	public String getAddressAr() {
		return addressAr;
	}



	public void setAddressAr(String addressAr) {
		this.addressAr = addressAr;
	}



	public String getSellerNameAr() {
		return sellerNameAr;
	}



	public void setSellerNameAr(String sellerNameAr) {
		this.sellerNameAr = sellerNameAr;
	}



	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}



	public long getCompanyId() {
		return companyId;
	}



	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}



	public String getContactName() {
		return contactName;
	}



	public void setContactName(String contactName) {
		this.contactName = contactName;
	}



	public String getTelephoneNo() {
		return telephoneNo;
	}



	public void setTelephoneNo(String telephoneNo) {
		this.telephoneNo = telephoneNo;
	}



	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}



	public long getCityId() {
		return cityId;
	}



	public String getProfileImage() {
		return profileImage;
	}



	public void setProfileImage(String profileImage) {
		this.profileImage = profileImage;
	}



	public void setCityId(long cityId) {
		this.cityId = cityId;
	}



	public int getIsCod() {
		return isCod;
	}



	public void setIsCod(int isCod) {
		this.isCod = isCod;
	}



	public int getIsOnline() {
		return isOnline;
	}



	public void setIsOnline(int isOnline) {
		this.isOnline = isOnline;
	}





	public String getDescriptionAr() {
		return descriptionAr;
	}



	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}



	public int getRank() {
		return rank;
	}



	public void setRank(int rank) {
		this.rank = rank;
	}




	public int getServiceOrProduct() {
		return serviceOrProduct;
	}



	public void setServiceOrProduct(int serviceOrProduct) {
		this.serviceOrProduct = serviceOrProduct;
	}



	public long getCountryId() {
		return countryId;
	}



	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}



	public long getNoOfReviews() {
		return noOfReviews;
	}



	public void setNoOfReviews(long noOfReviews) {
		this.noOfReviews = noOfReviews;
	}



	public double getLatitude() {
		return latitude;
	}



	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}



	public double getLongitude() {
		return longitude;
	}



	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	public String getEmailId() {
		return emailId;
	}
	



	/*public Set<FeaturesAndPhotosShop> getFeatureAndPhotos() {
		return featureAndPhotos;
	}



	public void setFeatureAndPhotos(Set<FeaturesAndPhotosShop> featureAndPhotos) {
		this.featureAndPhotos = featureAndPhotos;
	}
*/


	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	/*public Set<FeaturesAndPhotosShop> getFeatureAndPhotos() {
		return featureAndPhotos;
	}

	public void setFeatureAndPhotos(Set<FeaturesAndPhotosShop> featureAndPhotos) {
		this.featureAndPhotos = featureAndPhotos;
	}*/

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	

	



	public int getStatus() {
		return status;
	}



	public void setStatus(int status) {
		this.status = status;
	}



	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
 


	

	public long getSellerId() {
		return sellerId;
	}



	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}



	public String getSellerName() {
		return sellerName;
	}



	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}



	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

    

	public long getMainCategoryId() {
		return mainCategoryId;
	}



	public void setMainCategoryId(long mainCategoryId) {
		this.mainCategoryId = mainCategoryId;
	}



	
	public long getShares() {
		return shares;
	}

	public void setShares(long shares) {
		this.shares = shares;
	}

	public double getPriceStarts() {
		return priceStarts;
	}

	public void setPriceStarts(double priceStarts) {
		this.priceStarts = priceStarts;
	}

	public String getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}

	public int getDelivaryStatus() {
		return delivaryStatus;
	}

	public void setDelivaryStatus(int delivaryStatus) {
		this.delivaryStatus = delivaryStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}
	
	
	

}
