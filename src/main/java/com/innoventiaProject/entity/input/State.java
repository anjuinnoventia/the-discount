package com.innoventiaProject.entity.input;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "STATE",uniqueConstraints ={@UniqueConstraint(columnNames = "STATE_ID")})
public class State implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "STATE_ID",unique = true,nullable = false)
	private long stateId;
	
	@Column(name = "STATE_NAME",unique = false,nullable = true)
	private String stateName;
	
	@Column(name = "STATE_NAME_AR",unique = false,nullable = true)
	private String stateNameAr;
	
	@Column(name = "STATUS",unique = false,nullable = true)
	private int status;
	
	@Column(name = "COUNTRY_ID",unique = false,nullable = true)
	private long countryId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =true)
	private long lastUpdatedTime;
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="COUNTRY_ID", nullable = true, insertable = false, updatable = false)
	private Country country;

	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateNameAr() {
		return stateNameAr;
	}

	public void setStateNameAr(String stateNameAr) {
		this.stateNameAr = stateNameAr;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	
	
	
	
	
	
}
