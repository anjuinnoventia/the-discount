package com.innoventiaProject.entity.input;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "REGION",uniqueConstraints ={@UniqueConstraint(columnNames = "REGION_ID")})
public class Region implements Serializable  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REGION_ID",unique = true,nullable = false)
	private long regionId;
	
	@Column(name = "REGION_NAME",unique = false,nullable = true)
	private String cityName;
	
	@Column(name = "REGION_AR",unique = false,nullable = true)
	private String cityNameAr;
	
	@Column(name = "STATUS",unique = false,nullable = true)
	private int status;
	
	@Column(name = "CITY_ID",unique = false,nullable = false)
	private long cityId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =true)
	private long lastUpdatedTime;
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="CITY_ID", nullable = true, insertable = false, updatable = false)
	private City city;

	public long getRegionId() {
		return regionId;
	}

	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCityNameAr() {
		return cityNameAr;
	}

	public void setCityNameAr(String cityNameAr) {
		this.cityNameAr = cityNameAr;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}
	
	
	
	
	
}
