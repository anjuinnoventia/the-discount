package com.innoventiaProject.entity.input;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;







@Entity
@Table(name = "CS_FEATURE_AND_PHOTOS_PRODUCT",uniqueConstraints ={@UniqueConstraint(columnNames = "FEATURE_PHOTO_ID")})
public class FeaturesAndPhotosProduct implements Serializable {

	public FeaturesAndPhotosProduct(){
		
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1845104054654124444L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FEATURE_PHOTO_ID",unique = true,nullable = false)
	private long featurePhotoId;
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	@Column(name = "MODE_ID",unique = false,nullable = false)
	private int modeId;
	
	@Column(name ="DATA",unique =false,nullable =true)
	private String data;
	
	@Column(name ="EXTRA_DATA",unique =false,nullable =true)
	private String extraData;
	
	@Column(name ="EXTRA_DATA_NUM",unique =false,nullable =true)
	private double extraDataNum;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="PRODUCT_ID",unique =false,nullable =false)
	private long productId;
	
	@Column(name = "TAX_ID", unique = false, nullable = false)
	private long taxId;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	@Column(name = "ITEM_DIMEANSION", unique = false, nullable = false)
	private int itemDimeansion;
	
	@Column(name = "TYPE_OF_FEATURE", unique = false, nullable = false)
	private int typeOfFeature;
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="PRODUCT_ID", nullable = true, insertable = false, updatable = false)
	private Product product;
	
	
	
	
	
	

	public int getItemDimeansion() {
		return itemDimeansion;
	}

	public void setItemDimeansion(int itemDimeansion) {
		this.itemDimeansion = itemDimeansion;
	}

	public int getTypeOfFeature() {
		return typeOfFeature;
	}

	public void setTypeOfFeature(int typeOfFeature) {
		this.typeOfFeature = typeOfFeature;
	}

	public long getTaxId() {
		return taxId;
	}

	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getFeaturePhotoId() {
		return featurePhotoId;
	}

	public void setFeaturePhotoId(long featurePhotoId) {
		this.featurePhotoId = featurePhotoId;
	}

	public int getModeId() {
		return modeId;
	}

	public void setModeId(int modeId) {
		this.modeId = modeId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getExtraData() {
		return extraData;
	}

	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}

	public double getExtraDataNum() {
		return extraDataNum;
	}

	public void setExtraDataNum(double extraDataNum) {
		this.extraDataNum = extraDataNum;
	}

	
	
	

	
	
	
	
	
	
	


	
	


}
