package com.innoventiaProject.entity.input;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "CONNECTED_PRODUCTS", uniqueConstraints = { @UniqueConstraint(columnNames = "CONNECTED_PRODUCTS_ID") })
public class ConnectedProducts implements Serializable
{

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CONNECTED_PRODUCTS_ID",unique = true,nullable = false)
	private long connectedProductsId;
	
	@Column(name ="PRODUCT_ID",unique =false,nullable =false)
	private long productId;
	
	@Column(name ="RELATED_PRODUCT_ID",unique =false,nullable =false)
	private long relatedProductId;
	
	@Column(name = "MODE_ID",unique = false,nullable = false)
	private int modeId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;

	@ManyToOne( fetch = FetchType.EAGER)  
	@JoinColumn(name="PRODUCT_ID", nullable = true, insertable = false, updatable = false)
	private Product product;

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public long getConnectedProductsId() {
		return connectedProductsId;
	}

	public void setConnectedProductsId(long connectedProductsId) {
		this.connectedProductsId = connectedProductsId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public int getModeId() {
		return modeId;
	}

	public void setModeId(int modeId) {
		this.modeId = modeId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public long getRelatedProductId() {
		return relatedProductId;
	}

	public void setRelatedProductId(long relatedProductId) {
		this.relatedProductId = relatedProductId;
	}
	
	
	
	
	
	
}
