package com.innoventiaProject.entity.input;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "OFFER", uniqueConstraints = { @UniqueConstraint(columnNames = "OFFER_ID") })
public class Offer implements Serializable 
{
	private static final long serialVersionUID = 4398790091677292595L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "OFFER_ID", unique = true, nullable = false)
	private long offerId;

	@Column(name = "OFFER_NAME", unique = false, nullable = false)
	private String offerName;
	
	@Column(name = "OFFER_NAME_AR", unique = false, nullable = false)
	private String offerNameAr;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DESCRIPTION", unique = false, nullable = true)
	private String description;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DESCRIPTION_AR", unique = false, nullable = true)
	private String descriptionAr;
	
	@Column(name = "ImageName",unique = false,nullable = true)
	private String imageName;
	
	
	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = true)
	private long lastUpdatedTime;
	
	@Column(name = "STATUS", unique = false, nullable = true)
	private int status;
	
	
	@Column(name ="OPERATING_OFFICER_ID",unique =false,nullable =true)
	private long operatingOfficerId;
	
	@Column(name = "OFFER_STARTS", unique = false, nullable = true)
	private long offerStarts;
	
	@Column(name = "OFFER_ENDS", unique = false, nullable = true)
	private long offerEnds;
	
	
	@Column(name = "OFFER_RATE", unique = false, nullable = false)
	private double offerRate;
	

	@Column(name = "OFFER_PERCENTAGE", unique = false, nullable = false)
	private long offerPercentage;
	
	
	
	
	

	public long getOfferStarts() {
		return offerStarts;
	}

	public void setOfferStarts(long offerStarts) {
		this.offerStarts = offerStarts;
	}

	public long getOfferEnds() {
		return offerEnds;
	}

	public void setOfferEnds(long offerEnds) {
		this.offerEnds = offerEnds;
	}

	public double getOfferRate() {
		return offerRate;
	}

	public void setOfferRate(double offerRate) {
		this.offerRate = offerRate;
	}

	public long getOfferPercentage() {
		return offerPercentage;
	}

	public void setOfferPercentage(long offerPercentage) {
		this.offerPercentage = offerPercentage;
	}

	
	

	

	public long getOperatingOfficerId() {
		return operatingOfficerId;
	}

	public void setOperatingOfficerId(long operatingOfficerId) {
		this.operatingOfficerId = operatingOfficerId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionAr() {
		return descriptionAr;
	}

	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}

	/*public long getPercentage() {
		return percentage;
	}

	public void setPercentage(long percentage) {
		this.percentage = percentage;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}*/

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	public String getOfferName() {
		return offerName;
	}

	public void setOfferName(String offerName) {
		this.offerName = offerName;
	}

	public String getOfferNameAr() {
		return offerNameAr;
	}

	public void setOfferNameAr(String offerNameAr) {
		this.offerNameAr = offerNameAr;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	

}
