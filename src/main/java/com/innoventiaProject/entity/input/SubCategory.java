package com.innoventiaProject.entity.input;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "CS_SUBCATEGORY",uniqueConstraints ={@UniqueConstraint(columnNames = "SUBCATEGORY_ID")})
public class SubCategory implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SUBCATEGORY_ID",unique = true,nullable = false)
	private long subCategoryId;
	
	@Column(name = "SUBCATEGORY_NAME",unique = false,nullable = false)
	private String subCategoryName;
	
	@Column(name = "SUBCATEGORY_NAME_ARABIC",unique = false,nullable = false)
	private String subCategoryNameAr;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	@Column(name = "CREATED_DATE",unique = false,nullable = false)
	private long createdDate;
	
	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	
	@Column(name = "DISCRIPTION",unique = false,nullable = false)
	private String discription;
	
	@Column(name = "ImageName",unique = false,nullable = false)
	private String imageName;
	
	@Column(name = "CATEGORY_ID",unique = false,nullable = false)
	private long categoryId;
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="CATEGORY_ID", nullable = true, insertable = false, updatable = false)
	private Category category;
	
	@Column(name ="RANK",unique =false,nullable =false)
	private int  rank;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DiSCRIPTION_AR",unique = false,nullable = true)
	private String discriptionAr;
	
	
    
	/*public String getIconImage() {
		return iconImage;
	}

	public void setIconImage(String iconImage) {
		this.iconImage = iconImage;
	}*/

	public String getDiscriptionAr() {
		return discriptionAr;
	}

	public void setDiscriptionAr(String discriptionAr) {
		this.discriptionAr = discriptionAr;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public String getSubCategoryNameAr() {
		return subCategoryNameAr;
	}

	public void setSubCategoryNameAr(String subCategoryNameAr) {
		this.subCategoryNameAr = subCategoryNameAr;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	@JsonIgnore
	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getSubCategoryName() {
		return subCategoryName;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getDiscription() {
		return discription;
	}

	public void setDiscription(String description) {
		this.discription = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public long getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	
	
}
