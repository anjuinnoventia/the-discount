package com.innoventiaProject.entity.input;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "CATEGORY",uniqueConstraints ={@UniqueConstraint(columnNames = "CATEGORY_ID")})
public class Category implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CATEGORY_ID",unique = true,nullable = false)
	private long categoryId;
	
	@Column(name = "CATEGORY_NAME",unique = false,nullable = true)
	private String categoryName;
	
	@Column(name = "CATEGORY_NAME_ARABIC",unique = false,nullable = true)
	private String categoryNameAr;
	
	@Column(name ="STATUS",unique =false,nullable =true)
	private int status;
	
	@Column(name = "USER_ID",unique = false,nullable = true)
	private long userId;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =true)
	private long lastUpdatedTime;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DiSCRIPTION",unique = false,nullable = true)
	private String discription;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DiSCRIPTION_AR",unique = false,nullable = true)
	private String discriptionAr;
	
	
	@Column(name = "ImageName",unique = false,nullable = true)
	private String imageName;


	public long getCategoryId() {
		return categoryId;
	}


	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}


	public String getCategoryName() {
		return categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public String getCategoryNameAr() {
		return categoryNameAr;
	}


	public void setCategoryNameAr(String categoryNameAr) {
		this.categoryNameAr = categoryNameAr;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public long getUserId() {
		return userId;
	}


	public void setUserId(long userId) {
		this.userId = userId;
	}


	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}


	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}


	public String getDiscription() {
		return discription;
	}


	public void setDiscription(String discription) {
		this.discription = discription;
	}


	public String getDiscriptionAr() {
		return discriptionAr;
	}


	public void setDiscriptionAr(String discriptionAr) {
		this.discriptionAr = discriptionAr;
	}


	public String getImageName() {
		return imageName;
	}


	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	

	
	
	

}

