package com.innoventiaProject.entity.input;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "OFFER_DETAILS", uniqueConstraints = { @UniqueConstraint(columnNames = "OFFER_DETAILS_ID") })
public class OfferDetails implements Serializable 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "OFFER_DETAILS_ID", unique = true, nullable = false)
	private long offerDetailsId;

	@Column(name ="PRODUCT_ID",unique =false,nullable =true)
	private long productId;
	
	
	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;
	
	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;
	
	
	
	
	@Column(name = "OFFER_ID", unique = false, nullable = true)
	private long offerId;
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="PRODUCT_ID", nullable = true, insertable = false, updatable = false)
	private Product product;

	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="OFFER_ID", nullable = true, insertable = false, updatable = false)
	private Offer offer;
	
	
	

	public Offer getOffer() {
		return offer;
	}

	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	
	

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	public long getOfferDetailsId() {
		return offerDetailsId;
	}

	public void setOfferDetailsId(long offerDetailsId) {
		this.offerDetailsId = offerDetailsId;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	
	
	
	
	
	
}
