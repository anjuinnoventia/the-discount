package com.innoventiaProject.entity.input;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "PRODUCT", uniqueConstraints = { @UniqueConstraint(columnNames = "PRODUCT_ID") })
public class Product implements Serializable 
{

	private static final long serialVersionUID = 4398790091677292595L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PRODUCT_ID", unique = true, nullable = false)
	private long productId;

	@Column(name = "PRODUCT_NAME", unique = false, nullable = false)
	private String productName;

	@Column(name = "PRODUCT_NAME_ARABIC", unique = false, nullable = false)
	private String productNameAr;

	@Column(name = "PRODUCT_QUANTITY", unique = false, nullable = false)
	private long productQuantity;

	@Column(name = "SELLER_ID",unique = false,nullable = false)
	private long sellerId;
	
	@Column(name = "CREATED_DATE", unique = false, nullable = false)
	private long createdDate;

	@Column(name = "LAST_UPDATED_TIME", unique = false, nullable = false)
	private long lastUpdatedTime;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DESCRIPTION", unique = false, nullable = false)
	private String description;
	
	@Column(name = "SHIPPING_CHARGE", unique = false, nullable = false)
	private double shippingCharge;

	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "DESCRIPTION_ARABIC", unique = false, nullable = false)
	private String descriptionAr;

	@Column(name = "CATEGORY_ID", unique = false, nullable = false)
	private long categoryId;

	@Column(name = "SUBCATEGORY_ID", unique = false, nullable = false)
	private long subCategoryId;

	@Column(name = "MANUFACTURER_ID", unique = false, nullable = false)
	private long manufacturerId;

	@Column(name = "USER_ID", unique = false, nullable = false)
	private long userId;

	@Column(name = "PRICE", unique = false, nullable = false)
	private double price;

	/*@Column(name = "TAX_PERCENTAGE", unique = false, nullable = false)
	private double taxPercentage;*/
	
	
	@Column(name = "TOTAL_AMOUNT_INCLUDE_OFFER", unique = false, nullable = true)
	private double totalOfferAmount;
	
	@Column(name = "TAX_ID", unique = false, nullable = true)
	private long taxId;
	
	/*@Column(name = "OFFER_RATE", unique = false, nullable = false)
	private double offerRate;*/

	@Column(name = "RANK", unique = false, nullable = false)
	private int rank;
	
	@Column(name = "FEATURED", unique = false, nullable = true)
	private int featured;
	
	
	@Column(name = "Rating", unique = false, nullable = false)
	private double rating;
	

	@Column(name = "RATING_PERCENTAGE", unique = false, nullable = true)
	private long ratingPercentage;

	@Column(name = "SHARES", unique = false, nullable = false)
	private long shares;

	@Column(name = "STATUS", unique = false, nullable = false)
	private int status;

	@Column(name = "DELIVERY_STATUS", unique = false, nullable = true)
	private int delivaryStatus;
	
	@Column(name = "NO_OF_REVIEWS", unique = false, nullable = false)
	private long noOfReviews;

	@Column(name = "OFFER_ID", unique = false, nullable = false)
	private long offerId;

	@Column(name = "NUMBER_OF_ITEMS", unique = false, nullable = false)
	private long numberOfItems;

	@Column(name = "NUMBER_OF_ITEMS_REMAINING", unique = false, nullable = false)
	private long numberOfItemsRemaining;

	@Column(name = "SERVICE_OR_PRODUCT", unique = false, nullable = false)
	private int serviceOrProduct;
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name ="RETURN_POLICY",unique =false,nullable =true)
	private String returnPolicy;
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name ="RETURN_POLICY_AR",unique =false,nullable =true)
	private String returnPolicyAr;
	
	@Column(name = "IS_OUT_OF_STOCK", unique = false, nullable = false)
	private int isOutOfStock;
	
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	@ColumnDefault("'00'")
	@Column(name = "IMAGE_NAME", unique = false, nullable = true)
	private String imageName;

	
	
	/*@Column(name = "TOTAL_AMOUNT_WITH_SHIPPING", unique = false, nullable = true)
	private double totalAmountIncludingShipping;*/
	

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SELLER_ID", nullable = true, insertable = false, updatable = false)
	private SellerDetails seller;
	
	
	@Column(name ="WARRANTY",unique =false,nullable =true)
	private String wrranty;
	
	@Column(name ="WARRANTY_AR",unique =false,nullable =true)
	private String wrrantyAr;
	
	
	@Column(name ="INSTALLATION_CHARGE",unique =false,nullable =true)
	private double installationCharge;
	
	
	@Column(name ="OFFER_BY_PROMOCODE",unique =false,nullable =true)
	private double offerByPromocode;
	
	
	@Column(name ="OFFER_TO_PROMOTER",unique =false,nullable =true)
	private double offerToPromoter;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<FeaturesAndPhotosProduct> featureAndPhotos;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<ConnectedProducts> connectedProducts;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<Review> reviews;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<ProductAttributes> proudctAttributes;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
	private Set<OfferDetails> offerDetails;
	
	
	@ColumnDefault("'00'")
	@Column(name = "PURCHASE_PRICE", unique = false, nullable = true)
	private double purchasePrice;
	
	@ColumnDefault("'00'")
	@Column(name = "COMMISSION", unique = false, nullable = true)
	private double commission;
	
	@ColumnDefault("'00'")
	@Column(name = "MAXIMUM_PROMO_OFFER", unique = false, nullable = true)
	private double maximumPromoOffer;
	
	
	
	
	
	
	

	public double getCommission() {
		return commission;
	}

	public void setCommission(double commission) {
		this.commission = commission;
	}

	public double getMaximumPromoOffer() {
		return maximumPromoOffer;
	}

	public void setMaximumPromoOffer(double maximumPromoOffer) {
		this.maximumPromoOffer = maximumPromoOffer;
	}

	public int getFeatured() {
		return featured;
	}

	public void setFeatured(int featured) {
		this.featured = featured;
	}

	public double getOfferByPromocode() {
		return offerByPromocode;
	}

	public void setOfferByPromocode(double offerByPromocode) {
		this.offerByPromocode = offerByPromocode;
	}

	public double getOfferToPromoter() {
		return offerToPromoter;
	}

	public void setOfferToPromoter(double offerToPromoter) {
		this.offerToPromoter = offerToPromoter;
	}

	public String getWrrantyAr() {
		return wrrantyAr;
	}

	public void setWrrantyAr(String wrrantyAr) {
		this.wrrantyAr = wrrantyAr;
	}

	public double getInstallationCharge() {
		return installationCharge;
	}

	public void setInstallationCharge(double installationCharge) {
		this.installationCharge = installationCharge;
	}

	public String getWrranty() {
		return wrranty;
	}

	public void setWrranty(String wrranty) {
		this.wrranty = wrranty;
	}
/*
	public double getTotalAmountIncludingShipping() {
		return totalAmountIncludingShipping;
	}

	public void setTotalAmountIncludingShipping(double totalAmountIncludingShipping) {
		this.totalAmountIncludingShipping = totalAmountIncludingShipping;
	}*/

	
	
	
	
	

	public Set<ConnectedProducts> getConnectedProducts() {
		return connectedProducts;
	}

	public void setConnectedProducts(Set<ConnectedProducts> connectedProducts) {
		this.connectedProducts = connectedProducts;
	}

	

	public double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public long getRatingPercentage() {
		return ratingPercentage;
	}

	public void setRatingPercentage(long ratingPercentage) {
		this.ratingPercentage = ratingPercentage;
	}

	public double getTotalOfferAmount() {
		return totalOfferAmount;
	}

	public void setTotalOfferAmount(double totalOfferAmount) {
		this.totalOfferAmount = totalOfferAmount;
	}

	/*public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}*/

	public long getTaxId() {
		return taxId;
	}

	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}

	public int getDelivaryStatus() {
		return delivaryStatus;
	}

	public void setDelivaryStatus(int delivaryStatus) {
		this.delivaryStatus = delivaryStatus;
	}

	public Set<OfferDetails> getOfferDetails() {
		return offerDetails;
	}

	public void setOfferDetails(Set<OfferDetails> offerDetails) {
		this.offerDetails = offerDetails;
	}

	public String getReturnPolicy() {
		return returnPolicy;
	}

	public void setReturnPolicy(String returnPolicy) {
		this.returnPolicy = returnPolicy;
	}

	public String getReturnPolicyAr() {
		return returnPolicyAr;
	}

	public void setReturnPolicyAr(String returnPolicyAr) {
		this.returnPolicyAr = returnPolicyAr;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public Set<ProductAttributes> getProudctAttributes() {
		return proudctAttributes;
	}

	public void setProudctAttributes(Set<ProductAttributes> proudctAttributes) {
		this.proudctAttributes = proudctAttributes;
	}

	public double getShippingCharge() {
		return shippingCharge;
	}

	public void setShippingCharge(double shippingCharge) {
		this.shippingCharge = shippingCharge;
	}

	/*public Set<ConnectedProducts> getConnectedProducts() {
		return connectedProducts;
	}

	public void setConnectedProducts(Set<ConnectedProducts> connectedProducts) {
		this.connectedProducts = connectedProducts;
	}*/

	/*public double getOfferRate() {
		return offerRate;
	}

	public void setOfferRate(double offerRate) {
		this.offerRate = offerRate;
	}*/

	public int getServiceOrProduct() {
		return serviceOrProduct;
	}

	public void setServiceOrProduct(int serviceOrProduct) {
		this.serviceOrProduct = serviceOrProduct;
	}

	public long getNumberOfItems() {
		return numberOfItems;
	}

	public void setNumberOfItems(long numberOfItems) {
		this.numberOfItems = numberOfItems;
	}

	public long getNumberOfItemsRemaining() {
		return numberOfItemsRemaining;
	}

	public void setNumberOfItemsRemaining(long numberOfItemsRemaining) {
		this.numberOfItemsRemaining = numberOfItemsRemaining;
	}

	public int getIsOutOfStock() {
		return isOutOfStock;
	}

	public void setIsOutOfStock(int isOutOfStock) {
		this.isOutOfStock = isOutOfStock;
	}

	public Set<Review> getReviews() {
		return reviews;
	}

	public void setReviews(Set<Review> reviews) {
		this.reviews = reviews;
	}

	public long getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(long productQuantity) {
		this.productQuantity = productQuantity;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Set<FeaturesAndPhotosProduct> getFeatureAndPhotos() {
		return featureAndPhotos;
	}

	public void setFeatureAndPhotos(Set<FeaturesAndPhotosProduct> featureAndPhotos) {
		this.featureAndPhotos = featureAndPhotos;
	}

	


	public SellerDetails getSeller() {
		return seller;
	}

	public void setSeller(SellerDetails seller) {
		this.seller = seller;
	}

	public long getOfferId() {
		return offerId;
	}

	public void setOfferId(long offerId) {
		this.offerId = offerId;
	}

	public long getNoOfReviews() {
		return noOfReviews;
	}

	public void setNoOfReviews(long noOfReviews) {
		this.noOfReviews = noOfReviews;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public long getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(long subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public long getSellerId() {
		return sellerId;
	}

	public void setSellerId(long sellerId) {
		this.sellerId = sellerId;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}

	public long getShares() {
		return shares;
	}

	public void setShares(long shares) {
		this.shares = shares;
	}


	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getProductNameAr() {
		return productNameAr;
	}

	public void setProductNameAr(String productNameAr) {
		this.productNameAr = productNameAr;
	}

	public String getDescriptionAr() {
		return descriptionAr;
	}

	public void setDescriptionAr(String descriptionAr) {
		this.descriptionAr = descriptionAr;
	}

	public long getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	/*public double getTaxPercentage() {
		return taxPercentage;
	}

	public void setTaxPercentage(double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}*/

	

	
	
	
	
	
	

}
