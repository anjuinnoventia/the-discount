package com.innoventiaProject.entity.input;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.ColumnDefault;

@Entity
@Table(name = "REVIEW_AND_LIKES",uniqueConstraints ={@UniqueConstraint(columnNames = "REVIEW_ID")})
public class Review implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "REVIEW_ID",unique = true,nullable = false)
	private long reviewId;

	@Column(name = "USER_ID",unique = false,nullable = false)
	private long userId;
	
	@Column(name = "MODE_ID",unique = false,nullable = false)
	private int modeId;
	
	@Column(name ="DATA",unique =false,nullable =true)
	private String data;
	
	@Column(name ="EXTRA_DATA",unique =false,nullable =true)
	private String extraData;
	
	@Column(name ="LAST_UPDATED_TIME",unique =false,nullable =false)
	private long lastUpdatedTime;
	
	@Column(name ="FATHER_ID",unique =false,nullable =false)
	private long fatherId;

	
	@Column(name ="PRODUCT_OR_SHOP",unique =false,nullable =false)
	private int productOrShop;
	
	@Column(name ="LIKES",unique =false,nullable =false)
	private long likes;
	
	@Column(name ="RATING",unique =false,nullable =false)
	private int rating;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	@ColumnDefault("'00'")
	@Column(name = "COMPANY_ID", unique = false, nullable = true)
	private long companyId;
	
	
	
	
	
	@Lob
    @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
	@Column(name = "COMMENTS", unique = false, nullable = true)
	private String comments;
	
	
	@ManyToOne( fetch = FetchType.EAGER, cascade = CascadeType.ALL)  
	@JoinColumn(name="PRODUCT_ID", nullable = true, insertable = false, updatable = false)
	private Product product;
	
	

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getReviewId() {
		return reviewId;
	}

	public void setReviewId(long reviewId) {
		this.reviewId = reviewId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getModeId() {
		return modeId;
	}

	public void setModeId(int modeId) {
		this.modeId = modeId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}


	public int getProductOrShop() {
		return productOrShop;
	}

	public void setProductOrShop(int productOrShop) {
		this.productOrShop = productOrShop;
	}

	public long getLikes() {
		return likes;
	}

	public void setLikes(long likes) {
		this.likes = likes;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getExtraData() {
		return extraData;
	}

	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}



	public long getFatherId() {
		return fatherId;
	}

	public void setFatherId(long fatherId) {
		this.fatherId = fatherId;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
    
}
