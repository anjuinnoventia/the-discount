package com.innoventiaProject.entity.input;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "ATTRIBUTES",uniqueConstraints ={@UniqueConstraint(columnNames = "ATTRIBUTE_ID")})
public class Attributes implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ATTRIBUTE_ID",unique = true,nullable = false)
	private long attributeId;
	
	@Column(name = "ATTRIBUTE_NAME",unique = false,nullable = false)
	private String attributeName;
	
	@Column(name = "ATTRIBUTE_NAME_AR",unique = false,nullable = false)
	private String attributeNameAr;
	
	@Column(name = "CREATED_DATE",unique = false,nullable = false)
	private long createdDate;
	
	@Column(name ="STATUS",unique =false,nullable =false)
	private int status;
	
	
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "attributes", cascade = CascadeType.ALL)
	private Set<AttributeDetails> attributeDetails;
	
	
	

	public long getAttributeId() {
		return attributeId;
	}

	public void setAttributeId(long attributeId) {
		this.attributeId = attributeId;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeNameAr() {
		return attributeNameAr;
	}

	public void setAttributeNameAr(String attributeNameAr) {
		this.attributeNameAr = attributeNameAr;
	}

	public long getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(long createdDate) {
		this.createdDate = createdDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
